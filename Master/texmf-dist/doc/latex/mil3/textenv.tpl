% Sample file: textenv.tpl template file

% 3.1 List environments

% 3.1.1 Numbered lists: enumerate

\noindent This space has the following properties:
\begin{enumerate}
   \item Grade 2 Cantor\label{Cantor}
   \item Half-smooth Hausdorff\label{Hausdorff}
   \item Metrizably smooth\label{smooth}
\end{enumerate}
Therefore, we can apply the Main Theorem \ldots

% 3.1.2 Bulleted lists: itemize

\noindent In this lecture, we set out to accomplish a variety 
of goals:
\begin{itemize}
   \item To introduce the concept of smooth functions
   \item To show their usefulness in the differentiation 
      of Howard-type functions
   \item To point out the efficacy of using smooth functions 
      in Advanced Calculus courses
\end{itemize}

% 3.1.3 Captioned lists: description

In this introduction, we outline the history of this concept.  
The main contributors were:
\begin{description}
   \item[J. Perelman,] the first to introduce smooth functions.
   \item[T. Kov\'acs,] who showed their usefulness in the 
      differentiation of Howard-type functions. 
   \item[A.P. Fein,] the main advocate of using smooth 
      functions in Advanced Calculus courses. 
\end{description}
In addition to these mathematicians \ldots

% 3.1.4 Rule and combinations

\begin{enumerate}
   \item First item of Level 1. 
   \begin{enumerate}
      \item First item of Level 2.
      \begin{enumerate}
         \item First item of Level 3.
         \begin{enumerate}
            \item First item of Level 4.\label{level4}
            \item Second item of Level 4.
          \end{enumerate}
          \item Second item of Level 3.
       \end{enumerate}
       \item Second item of Level 2.
   \end{enumerate}
   \item Second item of Level 1.
\end{enumerate}
Referencing the first item of Level 4: \ref{level4}

\begin{enumerate}
   \item First item of Level 1. 
   \begin{itemize}
      \item First item of Level 2.
      \begin{enumerate}
         \item First item of Level 3.
         \begin{itemize}
            \item First item of Level 4.\label{enums}
            \item Second item of Level 4.
          \end{itemize}
          \item Second item of Level 3.
       \end{enumerate}
       \item Second item of Level 2.
   \end{itemize}
   \item Second item of Level 1.
\end{enumerate}
Referencing the first item of Level 4: \ref{enums}

% 3.2 Tabbing environment

\begin{tabbing}
   Print\= Time\\
   \>Block\=[\{timing\},\\
   \>\>timing = Timing[expr];\\
   (careful with initalization)\kill
   \>\>Print[ timing[[1]] ];\\
   \>]\\
   End[\,]
\end{tabbing}

\begin{tabbing}
   \hspace*{.25in}\=\hspace{2ex}\=\hspace{2ex}\=\hspace{2ex}\kill
   \>  $k := 1$  \\
   \>  $l_k := 0$; $r_k := 1$  \\
   \>  \texttt{loop}  \\
   \>  \> $m_k := (l_k + r_k)/2$  \\
   \>  \> \texttt{if} $w < m_k$ \texttt{then}  \\
   \>  \>  \> $b_k := 0$; $r_k := m_k$  \\
   \>  \> \texttt{else if} $w > m_k$ \texttt{then}  \\
   \>  \>  \> $b_k := 1$; $l_k := m_k$  \\
   \>  \> \texttt{end if}  \\
   \>  \> $k := k + 1$  \\
   \>  \texttt{end loop}
\end{tabbing}

\begin{tabbing}
   This is short.\=\\
   This is much longer, \> and jumps back.
\end{tabbing}

% 3.3 Miscellaneous displayed text environments

\begin{quote}
   It's not that I'm afraid to die.  I just don't want to be 
   there when it happens. 
   \emph{Woody Allen}

   Literature is news that STAYS news.  \emph{Ezra Pound}
\end{quote}

\begin{quotation}
   KATH: Can he be present at the birth of his child?

   ED: It's all any reasonable child can expect if the dad 
   is present at the conception.
   \begin{flushright}
      \emph{Joe Orton}
   \end{flushright}
\end{quotation}

\begin{verse}
   I think that I shall never see\\
   A poem as lovely as a tree.

   Poems are made by fools like me,\\
   But only God can make a tree.

   \begin{flushright}
      \emph{Joyce Kilmer}
   \end{flushright}
\end{verse}

\begin{verbatim} 
Formula (2) in section 3 should be typed as follows:
\begin{equation}
   D^{\langle 2 \rangle} = \{\, \langle x_0, x_1 \rangle
    \mid x_0, x_1 \in D,\ x_0 = 0 \Rightarrow x_1 = 0 \,\}. 
\end{equation}
Please make the corrections.
\end{verbatim} 

Some European e-mail addresses contain \texttt{\%}; 
recall that you have to type \verb+\%+ to get \texttt{\%}.

% 3.4 Proclamations (theorem-like structures)

\begin{definition}\label{D:prime}
   \begin{enumerate} 
      \item $u$ is \emph{meet-irreducible} if 
         $u = x \wedge y$ implies that 
         $u = x$ or $u = x$.\label{mi1}
      \item $u$ is \emph{meet-irreducible} if 
         $u = x \wedge y$ implies that 
         $u = x$ or $u = x$.\label{mi2}
      \item $u$ is \emph{completely join-irreducible} if 
         $u = \bigvee X$ implies that $u \in X$.
          \label{mi3} 
   \end{enumerate}
\end{definition}

% Consecutive numbering

\newtheorem{lemma}{Lemma}
\newtheorem{proposition}[lemma]{Proposition}

% Numbering within a section

\newtheorem{lemma}{Lemma}[section]

\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}[lemma]{Proposition}

% 3.4.1 The full syntax

\newtheorem}{envname}[procCounter]{Name}[secCounter]

% 3.4.2 Proclamations with style

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem*{main}{Main Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{remark}
\newtheorem*{notation}{Notation}

% Five examples

% Example 1

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}

Example 2 
 
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{corollary}[theorem]{Corollary}
 
Example 3

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{proposition}{Proposition}[section]
\newtheorem{lemma}[proposition]{Lemma}
\newtheorem{definition}{Definition}

\theoremstyle{definition}
\newtheorem*{notation}{Notation}

Example 4

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem*{main}{Main Theorem}
\newtheorem{definition}{Definition}[section]
\newtheorem{lemma}[definition]{Lemma}

\theoremstyle{definition}
\newtheorem*{Rule}{Rule}

Example 5

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem*{main}{Main Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{remark}
\newtheorem*{notation}{Notation}

Exercises

\theoremstyle{definition}
\newtheorem{xca}{Exercise}
 
\begin{xca}
Prove that if $\inf H$ exists for all nonempty subsets $H$ of a 
poset $P$, then $\sup \varnothing$ also exists in~$P$.
\end{xca}

\begin{xcb}{Exercises}
\begin{enumerate}
\item A finite lattice $L$ is modular if{f} it does not
contain a pentagon.\label{E:pentagon}
\item Can the numbers of covering pairs in
Exercise~\ref{E:pentagon} be increased?\label{E:increased}
\end{enumerate}
\end{xcb}

% 3.5 Proof environment

\begin{proof} 
   This is the proof, delimited by the q.e.d. symbol.
\end{proof} 

\begin{proof}[Necessity]
   This is the proof of necessity.
\end{proof} 

\begin{proof}
Now the proof follows from the equation
\[
   a^2 = b^2 + c^2.\qedhere
\]
\end{proof}


% 3.7 Tabular environment

\begin{tabular}{ | l | r | r | r | }
   \hline
   Name     & 1     & 2     & 3   \\ \hline
   Peter    &  2.45 & 34.12 & 1.00\\ \hline
   John     &  0.00 & 12.89 & 3.71\\ \hline
   David    &  2.00 & 1.85  & 0.71\\ \hline
\end{tabular}

\begin{table}
   \begin{center}
      \begin{tabular}{ || l | r | r | r || }
         \hline
         Name     & 1     & 2     & 3   \\ \hline
         Peter    &  2.45 & 34.12 & 1.00\\ \hline
         John     &  0.00 & 12.89 & 3.71\\ \hline
         David    &  2.00 & 1.85  & 0.71\\ \hline
      \end{tabular}
      \caption{Tabular table}\label{Ta:first}
   \end{center}
\end{table}

\begin{tabular}{r||l|l|l|l|l|l|l|l|l|l|} \hline \hline
 & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9\\\hline

0& \symbol{0} &\symbol{1}&\symbol{2}&\symbol{3}&
\symbol{4}&\symbol{5}&\symbol{6}&\symbol{7}&
\symbol{8}&\symbol{9}\\ \hline

.........

120& \symbol{120} &\symbol{121}&\symbol{122}&\symbol{123}&
\symbol{124}&\symbol{125}&\symbol{126}&\symbol{127} & & \\ \hline
\end{tabular}

% More column formatting commands

\begin{tabular}{r @{.} l}
   3&78\\
   4&261\\
   4
\end{tabular}

\begin{tabular}{ || p{1in} | r | r | r || }\hline
   Name     &  1    & 2     & 3   \\ \hline
   Peter    &  2.45 & 34.12 & 1.00\\ \hline
   John     &  0.00 & 12.89 & 3.71\\ \hline
   David    &  2.00 & 1.85  & 0.71\\ \hline
\end{tabular}

% Refinements

\begin{table}
   \begin{center}
      \begin{tabular}{ || l | r | r | r || } \hline
         Name    & 1    & 2     & 3\\ \hline
         Peter   & 2.45 & 34.12 & 1.00\\ \hline
         John    & \multicolumn{3}{c ||}{\emph{absent}}\\ \hline
         David   & 2.00 & 1.85  & 0.71\\ \hline
      \end{tabular}
      \caption{Floating table with 
               $\backslash$\texttt{multicolumn}}
      \label{Ta:cline}
   \end{center}
\end{table}

\begin{table}
   \begin{center}
      \begin{tabular}{ || c  c  | c | r || } \hline 
         Name  & Month & Week & Amount\\ \hline
         Peter & Jan.  & 1    & 1.00\\ \cline{3-4}
               &       & 2    & 12.78\\ \cline{3-4}
               &       & 3    & 0.71\\ \cline{3-4}
               &       & 4    & 15.00\\ \cline{2-4}
               & \multicolumn{2}{| l}{Total: } & 29.49\\  \hline
         John  & Jan.  & 1    & 12.01\\ \cline{3-4}
               &       & 2    & 3.10\\ \cline{3-4}
               &       & 3    & 10.10\\ \cline{3-4}
               &       & 4    & 0.00\\ \cline{2-4}
               & \multicolumn{2}{| l}{Total: } & 25.21\\  \hline
           \multicolumn{3}{|| l}{Grand Total:} & 54.70\\ \hline
      \end{tabular}
      \caption{Floating table with 
               $\backslash$\texttt{multicolumn} and 
               $\backslash$\texttt{cline}}
      \label{Ta:multicol}
   \end{center}
\end{table}

\begin{center}
   \begin{tabular}{ | r | c | c | } \hline
      & \textbf{Area}  & \textbf{Students}\\ \hline
      \textbf{5th Grade}: & 63.4 \mbox{m$^{2}$} & 22\\ \hline
      \textbf{6th Grade}: & 62.0 \mbox{m$^{2}$} & 19 \\ \hline
      \textbf{Overall}: & 62.6 \mbox{m$^{2}$} & 20\\ \hline
   \end{tabular}
\end{center}

% Style and size environments

\begin{flushright}
   The \begin{bfseries}simplest\end{bfseries} 
   text environments set the
   printing style and size.\\
   The commands and the environments have similar names. 

\end{flushright}

% 9.6 Custom lists

% 9.6.2 The list environment

\noindent Here are the most important \LaTeX\ rules about 
spaces in text, sentences, and paragraphs: 
\begin{list}{$\diamondsuit$}{\setlength{\leftmargin}{.5in}
                             \setlength{\rightmargin}{.5in}}
   \item \textbf{Rule 1:} Two or more spaces in text are 
    the same as one.
   \item \textbf{Rule 2:} A blank line (that is, two end-of-line 
    characters separated only by blanks and tabs) indicates  
    the end of a paragraph.
\end{list}
Rules 1 and~2 make typing and copying very convenient.

\noindent Here are the most important \LaTeX\ rules about 
spaces in text, sentences, and paragraphs: 
\begin{list}{}{\setlength{\leftmargin}{.5in}
               \setlength{\rightmargin}{.5in}}
   \item[\textbf{Rule 1:}] Two or more spaces in text are 
    the same as one.
   \item[\textbf{Rule 2:}] A blank line (that is, two 
    end-of-line characters separated only by blanks and tabs)
    indicates the end of a paragraph.
\end{list}
Rules 1 and~2 make typing and copying very convenient.

% Using counters

Here are the most important \LaTeX\ rules about spaces in text, 
sentences, and paragraphs: 
\newcounter{spacerule}
\begin{list}{\textbf{Rule \arabic{spacerule}:}}
            {\setlength{\leftmargin}{.5in}
             \setlength{\rightmargin}{.5in}
             \usecounter{spacerule}}
\item Two or more spaces in text are 
the same as one.
\item A blank line (that is, two end-of-line 
characters separated only by blanks and tabs) indicates the end 
of a paragraph.
 \end{list}
Rules~1 and 2 make typing and copying very convenient.

% 9.6.3 Two complete examples

% Example 1

Here are the most important \LaTeX\ rules about spaces in text, 
sentences, and paragraphs: 
\newcounter{spacerule}
\begin{list}{\upshape \bfseries Rule \arabic{spacerule}:}
            {\setlength{\leftmargin}{1.5in}
             \setlength{\rightmargin}{.6in}
             \setlength{\labelwidth}{1.0in}
             \setlength{\labelsep}{.2in}
             \setlength{\parsep}{0.5ex plus 0.2ex minus 0.1ex}
             \setlength{\itemsep}{0ex plus 0.2ex minus 0ex}
             \usecounter{spacerule}
             \itshape}
\item Two or more spaces in text are the same as one.
\item A blank line (that is, two end-of-line characters 
separated only by blanks and tabs) indicates the end  
of a paragraph.
\end{list}
Rules~1 and 2 make typing and copying very convenient.

\newenvironment{myrules}
   {\begin{list}
      {\upshape \bfseries Rule \arabic{spacerule}:}
      {\setlength{\leftmargin}{1.5in}
      \setlength{\rightmargin}{.6in}
      \setlength{\labelwidth}{1.0in}
      \setlength{\labelsep}{.2in}
      \setlength{\parsep}{0.5ex plus 0.2ex minus 0.1ex}
      \setlength{\itemsep}{0ex plus 0.2ex minus 0ex}
      \usecounter{spacerule}
      \itshape} }
   {\end{list}}

% Example 2

\begin{list}{}
   {\setlength{\leftmargin}{30pt}
   \setlength{\rightmargin}{0pt}
   \setlength{\itemindent}{14pt}
   \setlength{\labelwidth}{40pt}
   \setlength{\labelsep}{5pt}
   \setlength{\parsep}{0.5ex plus 0.2ex minus 0.1ex}
   \setlength{\itemsep}{0ex plus 0.2ex minus 0ex}}
   \item[\textbf{sentence}\hfill] is a group of words terminated 
       by a period, exclamation point, or question mark.  
   \item[\textbf{paragraph}\hfill] is a group of sentences 
       terminated by a blank line or by the \com{par} command.
\end{list}

% 9.7 Custom formats

\input tmplatex.ltx
\documentclass{article}
\usepackage{amssymb,latexsym,amsmath}
\dump
\end{verbatim}
On the other hand, the
\doc{sampartu.tex}\amssamplefileindex{sampartu.tex} article is a
little more complex:
\begin{verbatim}
\input tmplatex.ltx
\documentclass{amsart}
\usepackage{amssymb,latexsym}
\usepackage{lattice}

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\theoremstyle{remark}
\newtheorem*{notation}{Notation}

\numberwithin{equation}{section}

\newcommand{\Prodm}[2]{\gP(\,#1\mid#2\,)}
   % product with a middle
\newcommand{\Prodsm}[2]{\gP^{*}(\,#1\mid#2\,)}
   % product * with a middle
\newcommand{\vct}[2]{\vv<\dots,0,\dots,\overset{#1}{#2},% 
\dots,0,\dots>}% special vector
\newcommand{\fp}{\F{p}}% Fraktur p
\newcommand{\Ds}{D^{\langle2\rangle}}
\dump
