% Sample file: multline.tpl 
% multiline math formula template file


% Section 5.2 Gathering formulas

\begin{gather} 
   x_{1} x_{2} + x_{1}^{2} x_{2}^{2} + x_{3}, \label{E:mm1.1}\\
   x_{1} x_{3} + x_{1}^{2} x_{3}^{2} + x_{2}, \label{E:mm1.2}\\
   x_{1} x_{2} x_{3}. \label{E:mm1.3}
\end{gather}

% 5.3 Splitting a long formula

\begin{multline}\label{E:mm2}
   (x_{1} x_{2} x_{3} x_{4} x_{5} x_{6})^{2}\\ 
   + (y_{1} y_{2} y_{3} y_{4} y_{5} +y_{1} y_{3} y_{4} y_{5} y_{6}
   + y_{1} y_{2} y_{4} y_{5} y_{6} 
   + y_{1} y_{2} y_{3} y_{5} y_{6})^{2}\\
   + (z_{1} z_{2} z_{3} z_{4} z_{5} +z_{1} z_{3} z_{4} z_{5} z_{6}
   + z_{1} z_{2} z_{4} z_{5} z_{6} 
   + z_{1} z_{2} z_{3} z_{5} z_{6})^{2}\\
   + (u_{1} u_{2} u_{3} u_{4} + u_{1} u_{2} u_{3} u_{5} + 
   u_{1} u_{2} u_{4} u_{5} + u_{1} u_{3} u_{4} u_{5})^{2}
\end{multline}

\begin{multline*}
   (x_{1} x_{2} x_{3} x_{4} x_{5} x_{6})^{2}\\ 
   + (x_{1} x_{2} x_{3} x_{4} x_{5} 
    + x_{1} x_{3} x_{4} x_{5} x_{6}
    + x_{1} x_{2} x_{4} x_{5} x_{6} 
    + x_{1} x_{2} x_{3} x_{5} x_{6})^{2}\\
   + (x_{1} x_{2} x_{3} x_{4} + x_{1} x_{2} x_{3} x_{5} 
    + x_{1} x_{2} x_{4} x_{5} + x_{1} x_{3} x_{4} x_{5})^{2}
\end{multline*}
\begin{setlength}{\multlinegap}{0pt} 
   \begin{multline*}
      (x_{1} x_{2} x_{3} x_{4} x_{5} x_{6})^{2}\\ 
      + (x_{1} x_{2} x_{3} x_{4} x_{5}
       + x_{1} x_{3} x_{4} x_{5} x_{6}
       + x_{1} x_{2} x_{4} x_{5} x_{6} 
       + x_{1} x_{2} x_{3} x_{5} x_{6})^{2}\\
      + (x_{1} x_{2} x_{3} x_{4} + x_{1} x_{2} x_{3} x_{5} 
       + x_{1} x_{2} x_{4} x_{5} + x_{1} x_{3} x_{4} x_{5})^{2}
   \end{multline*}
\end{setlength}

\begin{multline*}
   (x_{1} x_{2} x_{3} x_{4} x_{5} x_{6})^{2}\\ 
   \shoveleft{+ (x_{1} x_{2} x_{3} x_{4} x_{5} 
      + x_{1} x_{3} x_{4} x_{5} x_{6} 
      + x_{1} x_{2} x_{4} x_{5} x_{6} 
      + x_{1} x_{2} x_{3} x_{5} x_{6})^{2}}\\
   + (x_{1} x_{2} x_{3} x_{4} + x_{1} x_{2} x_{3} x_{5} 
   + x_{1} x_{2} x_{4} x_{5} + x_{1} x_{3} x_{4} x_{5})^{2}
\end{multline*}

% 5.4.3 Group numbering

\begin{gather} 
   x_{1} x_{2} + x_{1}^{2} x_{2}^{2} + x_{3},\label{E:mm1}     \\
   x_{1} x_{3} + x_{1}^{2} x_{3}^{2} + x_{2},\tag{\ref{E:mm1}a}\\
   x_{1} x_{2} x_{3};\tag{\ref{E:mm1}b}
\end{gather}

\begin{subequations}\label{E:gp}
   \begin{gather} 
      x_{1} x_{2} + x_{1}^{2} x_{2}^{2} + x_{3},\label{E:gp1}\\
      x_{1} x_{3} + x_{1}^{2} x_{3}^{2} + x_{2},\label{E:gp2}\\
      x_{1} x_{2} x_{3},\label{E:gp3}
   \end{gather}
\end{subequations}

 % 5.5 Aligned columns

\begin{align}\label{E:mm3}
   f(x) &= x + yz        & g(x) &= x + y + z\\
   h(x) &= xy + xz + yz  & k(x) &= (x + y)(x + z)(y + z)
    \notag
\end{align}

% 5.5.1 An align variant

\begin{flalign}\label{E:mm3fl}
   f(x) &= x + yz       & g(x) &= x + y + z\\
   h(x) &= xy + xz + yz & k(x) &= (x + y)(x + z)(y + z)
    \notag
\end{flalign}

% 5.5.2 eqnarray, the ancestor of align

\begin{eqnarray}
   x & = & 17y \\
   y & > & a + b + c
\end{eqnarray}

\begin{align}
   x  & =  17y \\
   y  & >  a + b + c
\end{align}

% 5.5.3 The subformula rule revisited

\begin{align} 
   x_{1} + y_{1} + \left( \sum_{i < 5} \binom{5}{i} 
            &+ a^{2} \right)^{2}\\
    \left( \sum_{i < 5} \binom{5}{i} + \alpha^{2} \right)^{2}
\end{align}

\begin{align*} 
   &x_{1} + y_{1} + \left( \sum_{i < 5} \binom{5}{i} 
      + a^{2} \right)^{2}\\
   &\phantom{x_{1} + y_{1} + {}}
      \left( \sum_{i < 5} \binom{5}{i} + \alpha^{2} \right)^{2}
\end{align*}

% 5.5.4  The alignat environment

\begin{alignat}{2}\label{E:mm3A}
   f(x) &= x + yz       & g(x) &= x + y + z\\
   h(x) &= xy + xz + yz & k(x) &= (x + y)(x + z)(y + z)
    \notag
\end{alignat}

\begin{alignat}{2}\label{E:mm3B}
   f(x) &= x + yz              & g(x) &= x + y + z\\
   h(x) &= xy + xz + yz \qquad & k(x) &= (x + y)(x + z)(y + z)
    \notag

\begin{alignat}{2}\label{E:mm4} 
   x &= x \wedge (y \vee z) & &\quad\text{(by distributivity)}\\
     &= (x \wedge y) \vee (x \wedge z) & & 
      \quad\text{(by condition (M))}\notag\\
     &= y \vee z \notag
\end{alignat}

\begin{alignat}{2}
   (A + B C)x &+{} &C      &y = 0,\\
           Ex &+{} &(F + G)&y = 23.
\end{alignat}

\begin{alignat}{4}
  a_{11}x_1 &+ a_{12}x_2 &&+ a_{13}x_3 &&            &&= y_1,\\
  a_{21}x_1 &+ a_{22}x_2 &&            &&+ a_{24}x_4 &&= y_2,\\
  a_{31}x_1 &            &&+ a_{33}x_3 &&+ a_{34}x_4 &&= y_3.
\end{alignat}

% 5.5.5 Intertext

\begin{align}\label{E:mm5}
      h(x) &= \int \left( 
                         \frac{ f(x) + g(x) }
                              {1 + f^{2}(x)} +
                         \frac{1 + f(x)g(x)}
                              { \sqrt{1 - \sin x} }
                   \right) \, dx\\ 
   \intertext{The reader may find the following form easier to
    read:}
      &= \int \frac{1 + f(x)}
                   {1 + g(x)} 
         \, dx - 2 \arctan(x - 2) \notag
\end{align} 

\begin{align*}
   f(x) &= x + yz & \qquad g(x) &= x + y + z \\
   \intertext{The reader also may find the following 
    polynomials useful:}
   h(x) &= xy + xz + yz 
                  & \qquad k(x) &= (x + y)(x + z)(y + z)
\end{align*}

% 5.6 Aligned subsidiary math environments

% 5.6.1 Subsidiary variants of aligned math environments

\[
   \begin{aligned}
      x &= 3 + \mathbf{p} + \alpha \\
      y &= 4 + \mathbf{q}\\
      z &= 5 + \mathbf{r} \\
      u &=6 + \mathbf{s}
   \end{aligned}
   \text{\qquad using\qquad}
   \begin{gathered}
      \mathbf{p} = 5 + a + \alpha \\
      \mathbf{q} = 12 \\
      \mathbf{r} = 13 \\
      \mathbf{s} = 11 + d
   \end{gathered}
\]

\begin{align}\label{E:mm5}
      h(x) &= \int \left( 
                         \frac{ f(x) + g(x) }
                              {1 + f^{2}(x)} +
                         \frac{1 + f(x)g(x)}
                              { \sqrt{1 - \sin x} }
                   \right) \, dx\\ 
   \intertext{The reader may find the following form easier to
    read:}
      &= \int \frac{1 + f(x)}
                   {1 + g(x)} 
         \, dx - 2 \arctan(x - 2) \notag
\end{align} 

\begin{equation}\label{E:mm6}
   \begin{aligned} 
      h(x) &= \int \left( 
                         \frac{ f(x) + g(x) }
                              { 1 + f^{2}(x) } +
                         \frac{ 1 + f(x)g(x) }
                              { \sqrt{1 - \sin x} } 
                   \right) \, dx\\ 
           &= \int \frac{ 1 + f(x) }
                        { 1 + g(x) } \, dx - 2 \arctan (x - 2) 
   \end{aligned}  
\end{equation}

\[
   \begin{aligned}[b]
      x &= 3 + \mathbf{p} + \alpha \\
      y &= 4 + \mathbf{q}\\
      z &= 5 + \mathbf{r} \\
      u &=6 + \mathbf{s}
   \end{aligned}
   \text{\qquad using\qquad}
   \begin{gathered}[b]
      \mathbf{p} = 5 + a + \alpha \\
      \mathbf{q} = 12 \\
      \mathbf{r} = 13 \\
      \mathbf{s} = 11 + d
   \end{gathered}
\]

% 5.6.2 Split

\begin{equation}\label{E:mm7}
   \begin{split}
       (x_{1}x_{2}&x_{3}x_{4}x_{5}x_{6})^{2}\\ 
                  &+ (x_{1}x_{2}x_{3}x_{4}x_{5} 
                   + x_{1}x_{3}x_{4}x_{5}x_{6} 
                   + x_{1}x_{2}x_{4}x_{5}x_{6} 
                   + x_{1}x_{2}x_{3}x_{5}x_{6})^{2}
   \end{split}
\end{equation}

\begin{align}\label{E:mm8}
   \begin{split}
      f &= (x_{1} x_{2} x_{3} x_{4} x_{5} x_{6})^{2}\\ 
        &= (x_{1} x_{2} x_{3} x_{4} x_{5} 
          + x_{1} x_{3} x_{4} x_{5} x_{6}
          + x_{1} x_{2} x_{4} x_{5} x_{6}
          + x_{1} x_{2} x_{3} x_{5} x_{6})^{2},
   \end{split}\\
      g &= y_{1} y_{2} y_{3}.\label{E:mm9}
\end{align}

\begin{gather}\label{E:mm10} 
   \begin{split}
      f &= (x_{1} x_{2} x_{3} x_{4} x_{5} x_{6})^{2}\\ 
        &= (x_{1} x_{2} x_{3} x_{4} x_{5} 
           + x_{1} x_{3} x_{4} x_{5} x_{6}
           + x_{1} x_{2} x_{4} x_{5} x_{6}
           + x_{1} x_{2} x_{3} x_{5} x_{6})^{2}\\
        &= (x_{1} x_{2} x_{3} x_{4} 
           + x_{1} x_{2} x_{3} x_{5}
           + x_{1} x_{2} x_{4} x_{5}
           + x_{1} x_{3} x_{4} x_{5})^{2}
   \end{split}\\
   \begin{align*}
      g &= y_{1} y_{2} y_{3}\\
      h &= z_{1}^{2} z_{2}^{2} z_{3}^{2} z_{4}^{2}
   \end{align*}
\end{gather}

% 5.7 Adjusted columns

\begin{equation*}
   \left(
      \begin{matrix}
         a + b + c & uv & x - y & 27\\
         a + b & u + v & z & 1340
      \end{matrix}
   \right) = 
   \left(
      \begin{matrix}
         1 & 100 & 115 & 27\\
         201 & 0 & 1 & 1340
      \end{matrix}
   \right) 
\end{equation*}

\begin{equation*}
   \left(
      \begin{array}{cccr}
         a + b + c & uv & x - y & 27\\
         a + b & u + v & z & 1340
      \end{array}
   \right) = 
   \left(
      \begin{array}{rrrr}
         1 & 100 & 115 & 27\\
         201 & 0 & 1 & 1340
      \end{array}
   \right) 
\end{equation*}

\begin{equation}\label{E:mm11} 
   f(x) =
   \begin{cases}
      -x^{2}, &\text{\CMR if $x < 0$;}                \\
      \alpha + x,  &\text{\CMR if $ 0 \leq x \leq 1$;}\\
      x^{2},  &\text{\CMR otherwise.}
   \end{cases} 
\end{equation}

% 5.7.1 Matrices

\begin{equation*}
   \left(
   \begin{matrix}
      a + b + c & uv    & x - y & 27  \\
      a + b     & u + v & z     & 1340
   \end{matrix}
   \right) = 
   \left(
   \begin{matrix}
      1   & 100 & 115 & 27  \\
      201 & 0   & 1   & 1340
   \end{matrix}
   \right) 
\end{equation*}

\begin{matrix}
   a + b + c & uv    & x - y & 27 \\
   a + b     & u + v & z     & 134
\end{matrix}

\begin{equation}\label{E:mm12}
   \setcounter{MaxMatrixCols}{12}
   \begin{matrix}
      1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12\\
      1 & 2 & 3 & \hdotsfor{7}               & 11 & 12
   \end{matrix}
\end{equation}

\begin{equation}\label{E:mm12dupl}
   \setcounter{MaxMatrixCols}{12}
   \begin{matrix}
      1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12\\
      1 & 2 & 3 & \hdotsfor[3]{7}            & 11 & 12
   \end{matrix}
\end{equation}

% Matrix variants

\begin{alignat*}{3}
   &\  
   \begin{matrix} 
      a + b + c & uv\\
      a + b & c + d
   \end{matrix}
   \qquad
   & &
   \begin{pmatrix} 
      a + b + c & uv\\
      a + b & c + d
   \end{pmatrix}
   \qquad
   & &
   \begin{bmatrix}
      a + b + c & uv\\
      a + b & c + d
   \end{bmatrix}
   \\
   &
   \begin{vmatrix} 
      a + b + c & uv\\
      a + b & c + d
   \end{vmatrix}
   \qquad
   & &
   \begin{Vmatrix}
      a + b + c & uv\\
      a + b & c + d
   \end{Vmatrix}
   \qquad
   & &
   \begin{Bmatrix}
      a + b + c & uv\\
      a + b & c + d
   \end{Bmatrix}
\end{alignat*}

\begin{equation*}
   \left(
   \begin{matrix}
      1      &    0   & \dots   & 0     \\
      0      &    1   & \dots   & 0     \\
      \vdots & \vdots & \ddots  & \vdots\\
      0      &    0   & \dots   & 1
   \end{matrix}
   \right]
\end{equation*}
\end{verbatim}
which produces
\begin{equation*}
   \left(
      \begin{matrix}
         1      &    0   & \dots   & 0     \\
         0      &    1   & \dots   & 0     \\
         \vdots & \vdots & \ddots  & \vdots\\
         0      &    0   & \dots   & 1
      \end{matrix}
   \right]
\end{equation*}

% Small matrix

$\begin{pmatrix} 
   a + b + c & uv\\
   a + b     & c + d 
\end{pmatrix}$

$\left(
\begin{smallmatrix} 
   a + b + c & uv \\ 
   a + b     & c + d 
\end{smallmatrix}
\right)$

% 5.7.2 Arrays

\begin{equation*}
   \left(
   \begin{array}{cccc}
      a + b + c & uv    & x - y & 27 \\
      a + b     & u + v & z     & 134
   \end{array}
   \right)
\end{equation*}

% 5.7.3 Cases

\begin{equation} 
   f(x)=
   \begin{cases}
      -x^{2},       &\text{if $x < 0$;}\\
      \alpha + x,   &\text{if $0 \leq x \leq 1$;}\\
      x^{2},        &\text{otherwise.}
   \end{cases}
\end{equation}

% 5.8 Commutative diagrams

\[
   \begin{CD}
      A       @>>>      B   \\
      @VVV              @VVV\\
      C       @=        D
   \end{CD}
\]

\[
   \begin{CD}
      \mathbb{C} @>H_{1}>> \mathbb{C} @>H_{2}>> \mathbb{C}  \\
      @VP_{c,3}VV  @VP_{\bar{c},3}VV  @VVP_{-c,3}V  \\
      \mathbb{C} @>H_{1}>> \mathbb{C} @>H_{2}>> \mathbb{C}
   \end{CD}
\]

\[
   \begin{CD}
      A       @>\log>>     B       @>>\text{bottom}>     C      
              @=           D       @<<<                  E       
              @<<<         F\\
      @V\text{one-one}VV   @.      @AA\text{onto}A       @|\\
      X       @=           Y       @>>>                  Z      
              @>>>         U\\
      @A\beta AA           @AA\gamma A      @VVV         @VVV\\
      D       @>\alpha>>   E       @>>>                  H      
              @.           I\\
   \end{CD}
\]

% 5.9 Pagebreak

{\allowdisplaybreaks
\begin{align}\label{E:mm13}
   a &= b + c,\\
   d &= e + f,\\
   x &= y + z,\\
   u &= v + w. 
\end{align}
}% end of \allowdisplaybreaks
