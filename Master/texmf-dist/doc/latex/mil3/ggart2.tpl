% Sample file: ggart2.tpl for two authors
% Typeset with LaTeX format

\documentclass{article}
\usepackage{latexsym}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{definition}{Definition}
\newtheorem{corollary}{Corollary}
\newtheorem{notation}{Notation}

\begin{document}
\title{titleline1\\ 
       titleline2}
\author{George Gr\"{a}tzer\thanks{Research supported by the
                                  NSERC of Canada.}\\
   University of Manitoba\\ 
   Department of Mathematics\\
   Winnipeg, MB R3T 2N2\\
   Canada
   \and
   name2\thanks{support2}\\
   address2line1\\
   address2line2\\
   address2line3}
\date{date}
\maketitle

\begin{abstract}
   abstract
\end{abstract}

\begin{thebibliography}{99}
   bibliographic entries
\end{thebibliography}
\end{document}

Papers:

\bibitem{xxx}
   author, \emph{title}, journal \textbf{volume} 
    (year), pages.

Books:

\bibitem{xxx}
   author, \emph{title}, publisher, address, year.

\bibitem{xxx}
   author, \emph{title}, series, vol.~volume, 
    publisher, address, edition, date.

\bibitem{xxx}
   editor, ed., \emph{title}, publisher, address, year.

Papers in books:

\bibitem{xxx}
   author, \emph{title}, book title, publisher, 
    year, pp~pages.

\bibitem{xxx}
   author, \emph{title}, book title (editor, ed.), 
    vol.~volume, publisher, publisher address, date, 
    pp.~pages.

Theses:

\bibitem{xxx}
   author, \emph{title}, Ph.D. thesis, university, year.

Tech reports:

\bibitem{xxx}
   author, \emph{title}, tech. report, university, year.

Research notes:

\bibitem{xxx}
   author, \emph{title}, Research Note number,  
    university, location, date, research paper in
    preparation. 

Conference proceedings:

\bibitem{xxx}
   author, \emph{title}, conference title (location, 
    year).

\bibitem{xxx}
   author, \emph{title}, conference title, year
    (editor, ed.), vol.~volume, publisher, address, 
    pp.~pages. 
  
Abstracts:

\bibitem{xxx}
   author, \emph{title}, Abstract: journal, volume, 
    year.