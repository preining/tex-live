%%% Portions Copyright 1993 1994 1995 1996 1997
%% The LaTeX3 Project and any individual authors listed elsewhere
%% in this file.
%% 
%% Compilation and portions copyright 1998 Springer-Verlag New York, Inc.
%% 
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{svsing6}[1998/11/08 v1.0 Springer-Verlag LaTeX document class]
\typeout{Note that this class will not work in compatibility mode.^^J
  If you wish to use LaTeX 2.09, using svsing6.sty}
\typeout{Springer-Verlag New York strongly recommends and prefers the use^^J
  of LaTeX 2.09!}
\errorcontextlines\z@
\showboxdepth\z@
\showboxbreadth\z@
\let\wlog\@gobble
\def\xx#1{{\gdef#1{{}}}}
\DeclareOption{draft}{\setlength\overfullrule{5\p@}}
\DeclareOption{final}{\setlength\overfullrule{\z@}}
\DeclareOption{openright}{\let\chapclear\cleardoublepage}
\DeclareOption{openany}{\let\chapclear\clearpage}
\ExecuteOptions{final,openany}
\ProcessOptions
\ExecuteOptions{final,openright}
\ProcessOptions
%
%		Font size stuff
%
\def\setverticalskips{\abovedisplayskip\medskipamount
  \belowdisplayskip\medskipamount
  \abovedisplayshortskip\medskipamount
  \belowdisplayshortskip\medskipamount
  \topsep\medskipamount
  \itemsep\medskipamount}
\renewcommand\normalsize{%
  \@setfontsize\normalsize{10}{12}%
  \setlength\smallskipamount{3\p@\@plus1.5\p@\@minus.75\p@}%
  \setlength\medskipamount{6\p@\@plus1.8\p@\@minus.9\p@}%
  \setlength\bigskipamount{12\p@\@plus2.4\p@\@minus1.2\p@}%
  \setverticalskips
  \let\@listi\@listI}
\newcommand\small{%
  \@setfontsize\small{9}{11}%
  \setlength\smallskipamount{2.75\p@\@plus1.375\p@\@minus.6825\p@}%
  \setlength\medskipamount{5.5\p@\@plus1.65\p@\@minus.825\p@}%
  \setlength\bigskipamount{11\p@\@plus2.2\p@\@minus1.1\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
\newcommand\footnotesize{%
  \@setfontsize\footnotesize{8}{10}%
  \setlength\smallskipamount{2.5\p@\@plus1.25\p@\@minus.625\p@}%
  \setlength\medskipamount{5\p@\@plus1.6\p@\@minus.8\p@}%
  \setlength\bigskipamount{10\p@\@plus\tw@\p@\@minus\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
\newcommand\scriptsize{\@setfontsize\scriptsize{7}{8}%
  \setlength\smallskipamount{\tw@\p@\@plus\p@\@minus.5\p@}%
  \setlength\medskipamount{4\p@\@plus1.2\p@\@minus.6\p@}%
  \setlength\bigskipamount{8\p@\@plus1.6\p@\@minus.8\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
\newcommand\tiny{\@setfontsize\tiny{5}{6}%
  \setlength\smallskipamount{1.5\p@\@plus.75\p@\@minus.375\p@}%
  \setlength\medskipamount{\thr@@\p@\@plus.9\p@\@minus.45\p@}%
  \setlength\bigskipamount{6\p@\@plus1.2\p@\@minus.6\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
\newcommand\large{\@setfontsize\large{12}{14}%
  \setlength\smallskipamount{3.5\p@\@plus1.75\p@\@minus.875\p@}%
  \setlength\medskipamount{7\p@\@plus2.1\p@\@minus1.05\p@}%
  \setlength\bigskipamount{14\p@\@plus2.8\p@\@minus1.4\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
\newcommand\Large{\@setfontsize\Large{14}{18}%
  \setlength\smallskipamount{4.5\p@\@plus2.25\p@\@minus1.125\p@}%
  \setlength\medskipamount{9\p@\@plus2.7\p@\@minus1.35\p@}%
  \setlength\bigskipamount{18\p@\@plus3.6\p@\@minus1.8\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
\newcommand\LARGE{\@setfontsize\LARGE{17}{22}%
  \setlength\smallskipamount{5.5\p@\@plus2.75\p@\@minus1.375\p@}%
  \setlength\medskipamount{11\p@\@plus3.3\p@\@minus1.65\p@}%
  \setlength\bigskipamount{22\p@\@plus4.4\p@\@minus2.2\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
\newcommand\huge{\@setfontsize\huge{20}{25}%
  \setlength\smallskipamount{6.25\p@\@plus3.125\p@\@minus1.0625\p@}%
  \setlength\medskipamount{12.5\p@\@plus3.75\p@\@minus1.375\p@}%
  \setlength\bigskipamount{25\p@\@plus5\p@\@minus2.5\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
\newcommand\Huge{\@setfontsize\Huge{25}{30}%
  \setlength\smallskipamount{7.5\p@\@plus3.75\p@\@minus1.875\p@}%
  \setlength\medskipamount{15\p@\@plus4.5\p@\@minus1.25\p@}%
  \setlength\bigskipamount{30\p@\@plus6\p@\@minus\thr@@\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
%8/10
\def\viiiONx{\@setfontsize\@footnotesize{8}{10}%
  \setlength\smallskipamount{2.5\p@\@plus1.25\p@\@minus.625\p@}%
  \setlength\medskipamount{5\p@\@plus1.6\p@\@minus.8\p@}%
  \setlength\bigskipamount{10\p@\@plus\tw@\p@\@minus\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
%9/11
\def\ixONxi{\@setfontsize\smallsize{9}{11}%
  \setlength\smallskipamount{2.75\p@\@plus1.375\p@\@minus.6825\p@}%
  \setlength\medskipamount{5.5\p@\@plus1.65\p@\@minus.825\p@}%
  \setlength\bigskipamount{11\p@\@plus2.2\p@\@minus1.1\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
%10/12
\def\xONxii{\@setfontsize\normalsize{10}{12}%
  \setlength\smallskipamount{3\p@\@plus1.5\p@\@minus.75\p@}%
  \setlength\medskipamount{6\p@\@plus1.8\p@\@minus.9\p@}%
  \setlength\bigskipamount{12\p@\@plus2.4\p@\@minus1.2\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
%12/14
\def\xiiONxiv{\@setfontsize\large{12}{14}%
  \setlength\smallskipamount{3.5\p@\@plus1.75\p@\@minus.875\p@}%
  \setlength\medskipamount{7\p@\@plus2.1\p@\@minus1.05\p@}%
  \setlength\bigskipamount{14\p@\@plus2.8\p@\@minus1.4\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
%14/14
\def\xivONxiv{\@setfontsize\Large{14}{14}%
  \setlength\smallskipamount{3.5\p@\@plus1.75\p@\@minus.875\p@}%
  \setlength\medskipamount{7\p@\@plus2.1\p@\@minus1.05\p@}%
  \setlength\bigskipamount{14\p@\@plus2.8\p@\@minus1.4\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
%14/16
\def\xivONxvi{\@setfontsize\Large{14}{16}%
  \setlength\smallskipamount{4\p@\@plus\tw@\p@\@minus\p@}%
  \setlength\medskipamount{8\p@\@plus2.4\p@\@minus1.2\p@}%
  \setlength\bigskipamount{16\p@\@plus3.2\p@\@minus1.6\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
%20/20
\def\xxONxx{\@setfontsize\huge{20}{20}%
  \setlength\smallskipamount{5\p@\@plus2.5\p@\@minus1.25\p@}%
  \setlength\medskipamount{10\p@\@plus\thr@@\p@\@minus1.5\p@}%
  \setlength\bigskipamount{20\p@\@plus4\p@\@minus\tw@\p@}%
  \setverticalskips
  \def\@listi{\leftmargin\leftmargini\parsep\z@}}
%
%		List environments, etc. I
%
\def\@listi  {\leftmargin\leftmargini
              \parsep\z@}
\def\@listii {\leftmargin\leftmarginii
              \labelwidth\leftmarginii
              \advance\labelwidth-\labelsep
              \topsep    4\p@ \@plus2\p@ \@minus\p@
              \parsep\z@
              \itemsep   \parsep}
\def\@listiii{\leftmargin\leftmarginiii
              \labelwidth\leftmarginiii
              \advance\labelwidth-\labelsep
              \topsep    2\p@ \@plus\p@\@minus\p@
              \parsep    \z@
              \partopsep\z@
              \itemsep   \topsep}
\def\@listiv {\leftmargin\leftmarginiv
              \labelwidth\leftmarginiv
              \advance\labelwidth-\labelsep}
\def\@listv  {\leftmargin\leftmarginv
              \labelwidth\leftmarginv
              \advance\labelwidth-\labelsep}
\def\@listvi {\leftmargin\leftmarginvi
              \labelwidth\leftmarginvi
              \advance\labelwidth-\labelsep}
\let\@listI\@listi
%
\normalsize
\setlength\parindent{\@ne em}
%
%		Various registers
%
\setlength\headheight{9\p@}
\setlength\headsep{15\p@}
\setlength\topskip{10\p@}
\setlength\footskip{16\p@}
\setlength\maxdepth{.5\topskip}
\textwidth324\p@
\@tempdima\baselineskip
\setlength\textheight{44\baselineskip}
\addtolength\textheight{\topskip}
\setlength\hfuzz{.5\p@}
\setlength\marginparsep{11\p@}
\setlength\marginparpush{5\p@}
\paperwidth8.5in
\setlength\@tempdima        {\paperwidth}
\addtolength\@tempdima      {-\textwidth}
\setlength\oddsidemargin    {.5\@tempdima}
\addtolength\oddsidemargin  {-1in}
\@settopoint\oddsidemargin
\evensidemargin\oddsidemargin
\setlength\marginparwidth   {.5\@tempdima}
\addtolength\marginparwidth {-\marginparsep}
\addtolength\marginparwidth {-0.4in}
\ifdim \marginparwidth >2in  \setlength\marginparwidth{2in}  \fi
\@settopoint\marginparwidth
\paperheight11in
\setlength\topmargin{\paperheight}
\addtolength\topmargin{-2in}
\addtolength\topmargin{-\headheight}
\addtolength\topmargin{-\headsep}
\addtolength\topmargin{-\textheight}
\addtolength\topmargin{-\footskip}     % this might be wrong!
\addtolength\topmargin{-.5\topmargin}
\@settopoint\topmargin
\setlength\footnotesep{7\p@}
\setlength{\skip\footins}{18.5\p@\@plus6\p@}
\setlength\lineskip{\p@}
\setlength\normallineskip{\p@}
\renewcommand\baselinestretch{}
\setlength\parskip{\z@skip}
%	Penalties
\clubpenalty \@M
\widowpenalty \@M
\tolerance 1750
\hbadness 1750
\@lowpenalty   51
\@medpenalty  151
\@highpenalty 301
\@beginparpenalty -\@lowpenalty
\@endparpenalty   -\@lowpenalty
\@itempenalty     -\@lowpenalty
%	Demerits
\finalhyphendemerits100000000
\doublehyphendemerits80000000
%
%		Float parameters
%
\setlength\floatsep    {12\p@\@plus2.4\p@\@minus1.2\p@}
\setlength\textfloatsep{12\p@\@plus4.8\p@}
\setlength\intextsep   {12\p@\@plus2.4\p@\@minus1.2\p@}
\setlength\@fptop{0\p@ \@plus 1fil}
\setlength\@fpsep{8\p@ \@plus 2fil}
\setlength\@fpbot{0\p@ \@plus 1fil}
\setcounter{topnumber}{3}
\renewcommand\topfraction{1}
\setcounter{bottomnumber}{2}
\renewcommand\bottomfraction{1}
\setcounter{totalnumber}{6}
\renewcommand\textfraction{0.085502}
\renewcommand\floatpagefraction{.9}
%
%		More registers
%
\setlength\partopsep{\z@skip}
\setlength\arraycolsep{5\p@}
\setlength\tabcolsep{6\p@}
\setlength\arrayrulewidth{.5\p@}
\setlength\doublerulesep{2\p@}
\setlength\tabbingsep{\labelsep}
\skip\@mpfootins \skip\footins
\setlength\fboxsep{\thr@@\p@}
\setlength\fboxrule{.4\p@}
\setlength\columnsep{12\p@}
\setlength\columnseprule{\z@}
%
%		List environments, etc. II
%
\setlength\leftmargini   {2.5em}
\leftmargin  \leftmargini
\setlength\leftmarginii  {2.2em}
\setlength\leftmarginiii {1.87em}
\setlength\leftmarginiv  {1.7em}
\setlength\leftmarginv   {1em}
\setlength\leftmarginvi  {1em}
\setlength\labelsep      {.5em}
\setlength\labelwidth    {\leftmargini}
\addtolength\labelwidth  {-\labelsep}
\@beginparpenalty-\@lowpenalty
\@endparpenalty  -\@lowpenalty
\@itempenalty    -\@lowpenalty
\renewcommand\theenumi{\@arabic\c@enumi}
\renewcommand\theenumii{\@alph\c@enumii}
\renewcommand\theenumiii{\@roman\c@enumiii}
\renewcommand\theenumiv{\@Alph\c@enumiv}
\newcommand\labelenumi{\theenumi.}
\newcommand\labelenumii{(\theenumii)}
\newcommand\labelenumiii{\theenumiii.}
\newcommand\labelenumiv{\theenumiv.}
\renewcommand\p@enumii{\theenumi}
\renewcommand\p@enumiii{\theenumi(\theenumii)}
\renewcommand\p@enumiv{\p@enumiii\theenumiii}
\newcommand\labelitemi{\textbullet}
\newcommand\labelitemii{\normalfont\bfseries \textendash}
\newcommand\labelitemiii{\textasteriskcentered}
\newcommand\labelitemiv{\textperiodcentered}
\newenvironment{description}
               {\list{}{\labelwidth\z@ \itemindent-\leftmargin
                        \let\makelabel\descriptionlabel}}
               {\endlist}
\newcommand*\descriptionlabel[1]{\hspace\labelsep
                                \normalfont\bfseries #1}
\newcommand\abstractname{Abstract}
\newenvironment{abstract}{\vskip21\p@
  \rightskip1pc\leftskip1pc\parindent\z@
  \small
  \begin{center}%
    {\uppercase{\abstractname}\vspace{-.5em}\vspace{\z@}}%
  \end{center}%
  \quotation}
{\endquotation}
\newenvironment{verse}
               {\let\\\@centercr
                \list{}{\itemsep      \z@
                        \itemindent   -1.5em%
                        \listparindent\itemindent
                        \rightmargin  \leftmargin
                        \advance\leftmargin 1.5em}%
                \item\relax}
               {\endlist}
\newenvironment{quotation}
               {\list{}{\listparindent 1.5em%
                        \itemindent    \listparindent
                        \rightmargin   \leftmargin
                        \parsep        \z@ \@plus\p@}%
                \item\relax}
               {\endlist}
\newenvironment{quote}
               {\list{}{\rightmargin\leftmargin}%
                \item\relax}
               {\endlist}
%
%		make title
%
\newcommand\maketitle{\par
  \begingroup
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\parindent 1em\noindent\hb@xt@1.8em{%
                \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
    \newpage
    \global\@topnum\z@   % Prevents figures from going at top of page.
    \@maketitle
    \thispagestyle{empty}\@thanks
  \endgroup
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}
\def\@maketitle{%
  \newpage
  \null
  \vskip 2em%
  \begin{center}%
  \let \footnote \thanks
    {\LARGE \@title \par}%
    \vskip 1.5em%
    {\large
      \lineskip .5em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
    \vskip 1em%
    {\large \@date}%
  \end{center}%
  \par
  \vskip 1.5em}
\newenvironment{titlepage}{\newpage\thispagestyle{empty}\setcounter{page}%
  \@ne}{\newpage}
%
%		Output stuff
%
\def\ps@headings{%
  \let\@mkboth\markboth
  \let\@oddfoot\@empty\let\@evenfoot\@empty
  \def\@oddhead{\small\upshape\hfill\rightmark\hskip\tw@ em\thepage}%
  \def\@evenhead{\small\upshape\thepage\hskip\tw@ em\leftmark\hfill}%
  \def\chaptermark##1{\markbothsame{\ifnum\c@secnumdepth>\m@ne\@chapapp
    \thechapter.\kern6\p@\fi##1}}%
  \def\sectionmark##1{\markright{\ifnum\c@secnumdepth>\z@\thesection.\kern6\p@
    \fi##1}}}
\pagestyle{headings}
\def\ps@index{%
  \let\@mkboth\markboth
  \let\@oddfoot\@empty\let\@evenfoot\@empty
  \def\@oddhead{\hfill\small\upshape\indexname\hskip\tw@ em\thepage}%
  \def\@evenhead{\small\upshape\thepage\hskip\tw@ em\indexname\hfill}}
\def\clap#1{\hbox to\z@{\hss#1\hss}}
\newdimen\@crosshairrule \@crosshairrule.2\p@
\def\@crosshairs{\vbox to\z@{\hsize\z@\baselineskip\z@\lineskip\z@
  \vss
  \clap{\vrule height .125in width \@crosshairrule depth\z@}
  \clap{\vrule width .25in height \@crosshairrule depth\z@
    \rlap{\vbox to\z@{\vss\hbox{This is page \thepage\strut}%
      \hbox{Printer: Opaque this\strut}\vss}}}
  \clap{\vrule height .125in width \@crosshairrule depth\z@}
  \vss}}
\def\ps@empty{\def\@oddhead{\hfill\raise\headheight\@crosshairs}%
  \let\@evenhead\@oddhead
  \def\@evenfoot{}\let\@oddfoot\@evenfoot}
\mark{{}{}}
\def\markbothsame#1{\markboth{#1}{#1}}
\def\clearpage{\par\vfill\penalty-\@M\write\m@ne{}\vbox{}\penalty-\@Mi}
\def\newpage{\par\vfill\penalty-\@M}
\def\cleardoublepage{\clearpage\ifodd\c@page\else\typeout{Page \the\c@page
  \space is blank}\hbox{}\thispagestyle{empty}\newpage\fi}
\def\@makecol{\ifvoid\footins \setbox\@outputbox\box\@cclv
   \else\setbox\@outputbox
     \vbox{\unvbox\@cclv\vskip\skip\footins\footnoterule\unvbox\footins}\fi
     \xdef\@freelist{\@freelist\@midlist}\gdef\@midlist{}\@combinefloats
     \setbox\@outputbox\vbox to\@colht{\boxmaxdepth\maxdepth
        \@texttop
        \unvbox\@outputbox
        \@textbottom}\global\maxdepth\@maxdepth}
%
%		Front matter
%
\setcounter{secnumdepth}{2}
\setcounter{tocdepth}{2}
\newdimen\@pnumwidth
\setbox\z@\hbox{000\quad}
\@pnumwidth\wd\z@
\newcommand\@tocrmarg{2.55em}
\newcommand\@dotsep{4.5}
\newcommand*\l@part[2]{%
  \ifnum \c@tocdepth >-2\relax
    \addpenalty{-\@highpenalty}%
    \addvspace{2.25em \@plus\p@}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      {\leavevmode
       \large \bfseries #1\hfil \hb@xt@\@pnumwidth{\hss #2}}\par
       \nobreak
         \global\@nobreaktrue
         \everypar{\global\@nobreakfalse\everypar{}}%
    \endgroup
  \fi}
\newcommand*\l@chapter[2]{%
  \ifnum \c@tocdepth >\m@ne
    \addpenalty{-\@highpenalty}%
    \vskip 1.0em \@plus\p@
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
      \penalty\@highpenalty
    \endgroup
  \fi}
\def\l@section{\@dottedtocline{1}{1.5em}{2.8em}}
\def\l@subsection{\@dottedtocline{2}{4.3em}{3.2em}}
\def\l@subsubsection{\@dottedtocline{3}{7.5em}{4.1em}}
\def\l@paragraph{\@dottedtocline{4}{11.1em}{5em}}
\def\l@subparagraph{\@dottedtocline{5}{12.5em}{6em}}
\def\l@figure{\@dottedtocline{1}{1.5em}{2.3em}}
\let\l@table\l@figure
\newcommand\contentsname{Contents}
\newcommand\listfigurename{List of Figures}
\newcommand\listtablename{List of Tables}
\newcommand\tableofcontents{\chapter*{\contentsname}\markbothsame
  {\contentsname}\@starttoc{toc}}
\newcommand\listoffigures{\chapter*{\listfigurename}\markbothsame
  {\listfigurename}\@starttoc{lof}}
\newcommand\listoftables{\chapter*{\listtablename}\markbothsame
  {\listtablename}\@starttoc{lot}}
%
%		Back matter
%
\newdimen\bibindent
\setlength\bibindent{1.5em}
\newcommand\newblock{\unskip\ \ignorespaces}
\newcommand\bibname{References}
\newenvironment{thebibliography}[1]%
  {\chapter*{\bibname}\markbothsame{\bibname}%
  \addcontentsline{toc}{chapter}{\bibname}%
  \small
  \itemsep\smallskipamount
  \list
    {[\arabic{enumiv}]}{\settowidth\labelwidth{[#1]}%
      \leftmargin\labelwidth
      \advance\leftmargin\labelsep
      \usecounter{enumiv}%
      \let\p@enumiv\@empty
      \renewcommand\theenumiv{\@arabic\c@enumiv}}}
  {\def\@noitemerr{\@latex@warning{Empty `thebibliography' environment}}%
    \endlist}
\newcommand\indexname{Index}
\def\thebaseindex{\cleardoublepage\pagestyle{index}\columnseprule\z@
  \columnsep18\p@\twocolumn[\@makeschapterhead{\indexname}]
  \addcontentsline{toc}{chapter}{\indexname}%
  \@mkboth{\indexname}{\indexname}\thispagestyle{empty}%
  \small\parindent\z@
  \rightskip\z@ plus6em
  \parskip\z@\@plus.3\p@
  \let\item\@idxitem}
\let\theindex\thebaseindex
\def\theauthorindex{\def\indexname{Author Index}\thebaseindex}
\def\thesubjectindex{\def\indexname{Subject Index}\thebaseindex}
\def\thesymbolindex{\def\indexname{Symbol Index}\thebaseindex}
\newcommand\@idxitem{\par\hangindent\thr@@ em}
\newcommand\subitem{\@idxitem\hspace*{\@ne em}}
\newcommand\subsubitem{\@idxitem\hspace*{\tw@ em}}
\newcommand\indexspace{\par\vskip11\p@ plus2.2\p@ minus1.1\p@}
\def\endtheindex{\clearpage\onecolumn}
\let\endthesymbolindex\endtheindex
\let\endtheauthorindex\endtheindex
\let\endthesubhectindex\endtheindex
%
%		heads
%
\newcommand\partname{Part}
\newcommand\chaptername{}
\newcommand\appendixname{Appendix}
\newcommand\@chapapp{\chaptername}
\newcounter{part}
\newcounter{chapter}
\newcounter{section}[chapter]
\newcounter{subsection}[section]
\newcounter{subsubsection}[subsection]
\newcounter{paragraph}[subsubsection]
\newcounter{subparagraph}[paragraph]
\renewcommand\thepart         {\@Roman\c@part}
\renewcommand\thechapter      {\@arabic\c@chapter}
\renewcommand\thesection      {\thechapter.\@arabic\c@section}
\renewcommand\thesubsection   {\thesection.\@arabic\c@subsection}
\renewcommand\thesubsubsection{\thesubsection .\@arabic\c@subsubsection}
\renewcommand\theparagraph    {\thesubsubsection.\@arabic\c@paragraph}
\renewcommand\thesubparagraph {\theparagraph.\@arabic\c@subparagraph}
\newcommand\part{\cleardoublepage\thispagestyle{empty}\null\vfil\secdef\@part
  \@spart}
\def\@part[#1]#2{\ifnum\c@secnumdepth>-\tw@\refstepcounter{part}%
  \addcontentsline{toc}{part}{\thepart\hspace{1em}#1}\else\addcontentsline
  {toc}{part}{#1}\fi\markboth{}{}{\centering\interlinepenalty\@M\normalfont
  \ifnum\c@secnumdepth>-2\relax\huge\bfseries\partname~\thepart\par\vskip20\p@
  \fi\Huge\bfseries#2\par}\@endpart}
\def\@spart#1{{\centering\interlinepenalty\@M\normalfont\Huge\bfseries#1\par}%
  \@endpart}
\def\@endpart{\vfil\newpage\null\thispagestyle{empty}\newpage}
\font\chnum cmr10 scaled \magstep5
\def\chapter{\chapclear\thispagestyle{empty}\global\@topnum\z@
  \@afterindentfalse\secdef\@chapter\@schapter}
\def\@chapter[#1]#2{\ifnum\c@secnumdepth>\m@ne\refstepcounter{chapter}%
  \typeout{\@chapapp\space\thechapter.}\addcontentsline{toc}{chapter}%
  {\protect\numberline{\thechapter}#1}\else\addcontentsline{toc}{chapter}{#1}%
  \fi\chaptermark{#1}\addtocontents{lof}{\protect\addvspace{10\p@}}%
  \addtocontents{lot}{\protect\addvspace{10\p@}}\@makechapterhead{#2}%
  \@afterheading}
\def\@schapter#1{\@makeschapterhead{#1}\@afterheading}
\def\@makechapterhead#1{\vbox to238\p@{{\strut\vskip20\p@\raggedright\xxONxx
  \ifnum\c@secnumdepth>\m@ne{\chnum\@chapapp{}\thechapter}\fi\par\vskip10\p@
  #1\par\vfil}}}
\def\@makeschapterhead#1{\vbox to238\p@{{\strut\vskip50.5\p@\raggedright
  \xxONxx#1\par\vfil}}}
\newcommand\appendix{\par\setcounter{chapter}{0}\setcounter{section}{0}%
  \renewcommand\@chapapp{\appendixname}\renewcommand\thechapter{\@Alph
  \c@chapter}}
\def\section{\@startsection{section}{\@ne}{\z@}{-24\p@\@plus-6\p@\@minus
  -\thr@@\p@}{12\p@}{\xivONxvi\raggedright}}
\def\subsection{\@startsection{subsection}{\tw@}{\z@}{-18\p@\@plus-4.8\p@
  \@minus-2.4\p@}{6\p@}{\xiiONxiv\itshape\raggedright}}
\def\subsubsection{\@startsection{subsubsection}{\thr@@}{\z@}{-12\p@\@plus
  -4.2\p@\@minus-2.1\p@}{6\p@}{\xONxii\rm\raggedright}}
\def\paragraph{\@startsection{paragraph}{4}{\z@}{-8\p@\@plus-3.8\p@\@minus
  -1.7\p@}{6\p@}{\xONxii\itshape\raggedright}}
\def\subparagraph#1{\@startsection{subparagraph}{5}{\z@}{-6\p@\@plus-1.8\p@
  \@minus-.9\p@}{-.5em}{\xONxii\itshape}{#1.}}
\def\acknowledgments{\@startsection{subparagraph}{6}{\z@}{-24\p@\@plus-3.6\p@
  \@minus-1.8\p@}{-.5em}{\xONxii\itshape}*{Acknowledgments:\/}}
%
%	equations
%
\@addtoreset{equation}{chapter}
\renewcommand\theequation{\ifnum\c@chapter>\z@\thechapter.\fi\@arabic
  \c@equation}
%
%	feetnote
%
\@addtoreset{footnote}{chapter}
\newcommand\@makefntext[1]{\parindent\@ne em\noindent\hb@xt@1.8em{\hss
\renewcommand\footnoterule{\kern-6.5\p@\hrule\@width5pc height.5\p@\vskip6\p@}
  \@makefnmark}#1}
%
%		Figures and Tables
%
\newcommand\figurename{Figure}
\newcounter{figure}[chapter]
\renewcommand\thefigure{\ifnum\c@chapter>\z@\thechapter.\fi\@arabic\c@figure}
\def\fps@figure{tbp}
\def\ftype@figure{1}
\def\ext@figure{lof}
\def\fnum@figure{\small\uppercase{\figurename}~\thefigure}
\newenvironment{figure}{\@float{figure}}{\end@float}
\newenvironment{figure*}{\@float{figure}}{\end@float}
\newenvironment{table*}{\@float{table}}{\end@float}
\newcommand\tablename{Table}
\newcounter{table}[chapter]
\renewcommand\thetable{\ifnum\c@chapter>\z@\thechapter.\fi\@arabic\c@table}
\def\fps@table{tbp}
\def\ftype@table{2}
\def\ext@table{lot}
\def\fnum@table{\small\uppercase{\tablename}~\thetable}
\newenvironment{table}{\@float{table}}{\end@float}
\newlength\abovecaptionskip
\newlength\belowcaptionskip
\setlength\abovecaptionskip{6\p@}
\setlength\belowcaptionskip{\z@skip}
\long\def\@makecaption#1#2{{%
  \vskip\abovecaptionskip
  \small
  \sbox\@tempboxa{#1. #2}%
  \ifdim\wd\@tempboxa>\hsize
    \unhbox\@tempboxa\hfil\par
  \else
    \global\@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}}
%
%		Font commands
%
\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
\DeclareOldFontCommand{\sl}{\normalfont\slshape}{\@nomath\sl}
\DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}
\DeclareRobustCommand*\cal{\@fontswitch\relax\mathcal}
\DeclareRobustCommand*\mit{\@fontswitch\relax\mathnormal}
%
%		In case book.sty has been used
%
\newif\if@mainmatter \@mainmattertrue
\newcommand\frontmatter{\cleardoublepage\@mainmatterfalse\pagenumbering
  {roman}}
\newcommand\mainmatter{\cleardoublepage\@mainmattertrue\pagenumbering{arabic}}
\newcommand\backmatter{\cleardoublepage\@mainmatterfalse}
%
%		Last words ...
%
\def\today{\ifcase\month\or
  January\or February\or March\or April\or May\or June\or
  July\or August\or September\or October\or November\or December\fi
  \space\number\day, \number\year}
\catcode`\^^Z 10
\@twosidetrue
\pagenumbering{roman}
\flushbottom
\onecolumn
\frenchspacing
\endinput
