#!/bin/sh
#
# Ein Beispiel der DANTE-Edition
#
## Beispiel 12-04-4 auf Seite 620.
#
# Copyright (C) 2012 Herbert Voss
#
# It may be distributed and/or modified under the conditions
# of the LaTeX Project Public License, either version 1.3
# of this license or (at your option) any later version.
#
# See http://www.latex-project.org/lppl.txt for details.
#
# Running fc-list on Linux
#START

#STOP
#
#CODE->
fc-list : family | sort | head
#<-CODE
