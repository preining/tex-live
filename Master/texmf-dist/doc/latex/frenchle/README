﻿% Here is a the light version of the multilingual package FrenchPro for LaTeX.
% Copyright notice is LPPL, Copyright Bernard GAULLE, 2000-2006,
% eFrench 2011-2012 (efrench@lists.tuxfamily.org). 
%
%                                        <efrench AT lists.tuxfamily.org>%                                                
%%%%%%%%%%%% English %%%%%%%%%
% Documentation is available here:  http://www.efrench.org/doc/frenchle.pdf
%
% You can find here 5 LaTeX source files to move to your LaTeX installation
% at the same place where you normally put any other LaTeX package/extension.
% frenchle.sty which is the main package 
% to be called just by \usepackage{frenchle} 
% or via babelfr (file babelfr.sty provided) with:
%   frenchle.ldf which is called when you type \usepackage[frenchle]{babelfr}
%   french.ldf which is called when you type   \usepackage[french]{babel} or
%                     better when you type in: \documentcalss[french]{class}
%                                         and: \usepackage{babelfr}
%
% Notice that french.ldf is no more called by babel since 2003 (THANKS!); you
% can't code \usepackage[french]{babel} as it was possible since the origine
% of Babel; this is the reason why babelfr was developped.
%
% frenchle package will do its best to typeset your document according to the
% french typographic rules. You have no command to type in (few commands
% are available in this light version) but macrotypography (punctuation, 
% foot notes) is in action, as well as french layout (figures, tables, lists,
% etc.), obviously frenh translation of standard titles and classes
% changes (book, letter); but not hyphenation, abbreviations, illuminated
% letters (lettrines), nor complete french guillemets. All that stuff and 
% other features are available with the professional version which gives you
% the ability of customization and allows you to switch on/off any
% feature depending of your needs. If you want more informations about the 
% "E" version (eFrench) look at http://www.efrench.org or 
% on any ctan. Don't hesitate to ask us at efrench@lists.tuxfamily.org. 
%
% Enjoy.                                              --rj update 2012-05-30

%%%%%%%%%% francais %%%%%%%%%%%
%% Vous trouverez la documentation de frenchle dans le fichier frenchle.pdf
%%
%% Vous trouverez ici, essentiellement 5 fichiers source LaTeX :
%% babelfr.sty  l'extension a appeler pour utiliser frenchle avec Babel et
%% frenchle.ldf    l'intermediaire pour coder \usepackage[frenchle]{babelfr} 
%% french.ldf      ceci pour pouvoir coder    \usepackage[french]{babelfr}
%%                                            (ce qui est le meilleur choix)
%% frenchle.sty le fichier style principal deefrench@lists.tuxfamily.org l'extension frenchle ; on peut
%%              l'appeler seule en codant    \usepackage{frenchle} 
%% frenchle.cfg qui permet de personnaliser votre utilisation de frenchle.
%%
%% A noter que french.ldf n'est plus appele par babel depuis fin 2003 ce qui
%% ne permet donc plus de coder \usepackage[french]{babel} comme cela etait
%% possible depuis l'origine de Babel ; MERCI ! d'ou l'utilisation de babelfr.
%%
%% Vous devez deposer ces fichiers dans /texmf/tex/latex/frenchle/
%% ET ne pas oublier de faire un "texhash" ensuite (ou "mktexlsr")
%% pour que ces fichiers figurent dans la base des fichiers TeX !
%% Si vous utilisez la commande latex standard il suffit que vous ayez
%% french dans language.dat pour disposer des cesures françaises, ensuite
%% frenchle fera le maximum pour composer le document en respectant les
%% regles typographiques usuelles a l'Imprimerie nationale. Vous n'avez
%% aucune commande a entrer (peu de commandes sont accessibles dans cette
%% version allegee) mais la macro-typographie sera active (ponctuation,
%% notes de bas de page) tout comme la mise en page globale (figures, tables,
%% listes, etc.), avec bien sur la traduction des differents labels et
%% la prise en compte des classes de documents ; mais pas la coupure de mots
%% (effective normalement au niveau du format "frlatex"), ni les 
%% abreviations, ni les lettrines, ni le traitement complet des guillemets
%% a la francaise qui sont des dispositifs de la version professionnelle
%% eFrench qui offre par ailleurs de grandes possibilites de 
%% personnalisation et la capacite d'activer ou neutraliser chaque 
%% dispositif selon les besoins. Si vous  voulez plus d'informations au 
%% sujet de la version "E" voyez sous http://www.efrench.org  
%% ou sur CTAN (sous language/french/efrench). 
%% Ne pas hesiter a nous contacter sous efrench@lists.tuxfamily.org.
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

