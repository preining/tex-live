\documentclass{article}
\usepackage{ot-tableau}
\usepackage{fullpage,rotating}
\title{The \textsf{ot-tableau} package}
\author{Adam Baker}

\def\tabl{\textsf{ot-tableau}}

\begin{document}
\maketitle

\section{Introduction}
The \tabl\ package makes it easy to create beautiful optimality-theoretic tableaux. The \LaTeX\ source is visually very similar to a formatted tableau, which makes working with the source code painless (well, less painful). A variety of stylistic variants can be modified to suit personal taste.

\begin{center}
	\begin{tableau}{c:c|c}
	\inp{\ips{stap}}	\const{*Complex}	\const{Anchor-IO}	\const{Contiguity-IO}
	\cand{stap}		\vio{*!}		\vio{}			\vio{}
	\cand[\HandRight]{sap}	\vio{}			\vio{}			\vio{*}
	\cand{tap}		\vio{}			\vio{*!}		\vio{}
	\end{tableau}
\end{center}

\begin{verbatim}
\begin{tableau}{c:c|c}
\inp{\ips{stap}}        \const{*Complex} \const{Anchor-IO} \const{Contiguity-IO}
\cand{stap}             \vio{*!}         \vio{}            \vio{}
\cand[\HandRight]{sap}  \vio{}           \vio{}            \vio{*}
\cand{tap}              \vio{}           \vio{*!}          \vio{}
\end{tableau}
\end{verbatim}
Pertinent features:
\begin{itemize}
\item The package introduces the \verb+tableau+ environment.
\item Indicate solid or dashed lines between constraints with \verb+\begin{tableau}{c:c|c}+. A solid line is indicated by a pipe, a dashed line with a colon.
\item The input is specified with the \verb+\inp+ command. (Here the \verb+\ips+ macro is being used to render the text using TIPA and put it within slashes.)
\item Indicate the constraints with the \verb+\const+ command.
\item Add a candidate with the \verb+\cand+ command. An optional argument can be used to annotate the candidate (like with \HandRight, \verb+\HandRight+)
\item Violations are indicated with the \verb+\vio+ macro. You need to include these commands even when there are no violations.
\item Use of whitespace is optional. Keeping the columns aligned in the source code, though, makes the tableau much easier to edit.
\end{itemize}

\noindent Very large tableaux are not much more difficult---for instance in Table~\ref{large_example}.

\begin{sidewaystable}
\ShadingOn
\begin{center}
	\LetterBeforeFinger
	\begin{tableau}{c:c:c|c:c|s:s}
	\inp{Input: /\textsc{red}, \ipa{ulampoy}/}	\const{Red=$\sigma$}	\const{Dep-IO}	\const{Max-IO}	\const{Onset}	\const{No-Coda}	\const{Align-Red-L}	\const{Max-BR}
	\cand[\HandRight]{u-\underline{la}-lam.poy}			\vio{}			\vio{}		\vio{}		\vio{*}		\vio{**}	\vio{u}			\vio{mpoy}
	\cand{u-\underline{lam}-lam.poy}				\vio{}			\vio{}		\vio{}		\vio{*}		\vio{***!}	\vio{u}			\vio{poy}
	\cand{\underline{u}-u.lam.poy}				\vio{}			\vio{}		\vio{}		\vio{**!}	\vio{**}	\vio{}			\vio{lampoy}
	\cand{\underline{ul}-u.lam.poy}				\vio{}			\vio{}		\vio{}		\vio{**!}	\vio{***}	\vio{}			\vio{ampoy}
	\cand{\underline{la}-lam.poy}				\vio{}			\vio{}		\vio{*!}	\vio{}		\vio{**}	\vio{}			\vio{mpoy}
	\cand{\underline{tu}-tu-lam.poy}				\vio{}			\vio{*!}	\vio{}		\vio{}		\vio{**}	\vio{}			\vio{lampoy}
	\cand{\underline{u.l}-u.lam.poy}				\vio{l!}		\vio{}		\vio{}		\vio{}		\vio{**}	\vio{}			\vio{lampoy}
	\cand{u-\underline{lam.poy}-lam.poy}			\vio{p!oy}		\vio{}		\vio{}		\vio{}		\vio{****}	\vio{u}			\vio{}
	\end{tableau}
\end{center}
\caption{After Kager (1999:229).}\label{large_example}
\end{sidewaystable}

\section{Parameters}

\subsection{Changing formats}
The default \verb+\cand+ and \verb+\const+ commands typset the argument in IPA (TIPA) and small caps, respectively. Though this is generally appropriate, there are also commands \verb+\cand*+ and \verb+\const*+ that apply no formatting. The following code and tableau illustrate this.

\begin{center}
	\begin{tableau}{c|c}
	\inp{Illustration}            \const{C1}  \const*{C2}
	\cand*{Option 1}              \vio{*!}    \vio{}
	\cand*[\HandRight]{Option 2}  \vio{}      \vio{*}
	\end{tableau}
\end{center}

\begin{verbatim}
	\begin{tableau}{c|c}
	\inp{Illustration}            \const{C1}  \const*{C2}
	\cand*{Option 1}              \vio{*!}    \vio{}
	\cand*[\HandRight]{Option 2}  \vio{}      \vio{*}
	\end{tableau}
\end{verbatim}

\noindent Usage of \verb+\const*+ is illustrated in the next section.

\subsection{Controlling cell shading}
There are two systems in use for shading OT tableaux. One system is to shade cells in a row after the crucial violation. The other system is to shade an entire column, if the associate constraint generates no crucial violations.

\tabl\ will do cell-shading automatically, if you invoke the \verb+\ShadingOn+ command before creating the tableau.\footnote{More specifically, \tabl\ will look for the exclamation point. You have to provide the exclamation point.} (There is also \verb+\ShadingOff+ if you wish to turn this feature off for subsequent tableaux in the document; this is the default setting.) This tableau...

\begin{center}
	\ShadingOn
	\begin{tableau}{c|c}
	\inp{\ips{ba}}		\const{*VcdObs}	\const*{\textsc{Ident-IO}-[nas]}
	\cand{ba}		\vio{*!}	\vio{}
	\cand[\HandRight]{pa}	\vio{}		\vio{*}
	\end{tableau}
\end{center}

\noindent ...is produced by the following code...

\begin{verbatim}
	\ShadingOn
	\begin{tableau}{c|c}
	\inp{\ips{ba}}         \const{*VcdObs}  \const*{\textsc{Ident-IO}-[nas]}
	\cand{ba}              \vio{*!}         \vio{}
	\cand[\HandRight]{pa}  \vio{}           \vio{*}
	\end{tableau}
\end{verbatim}

The alternative is to shade an entire column by using `s' instead of `c' in the argument to the \verb+tableau+ environment:

\begin{center}
	\begin{tableau}{c|s}
	\inp{\ips{ba}}         \const{*VcdObs}  \const*{\textsc{Ident-IO}-[nas]}
	\cand{ba}              \vio{*!}         \vio{}
	\cand[\HandRight]{pa}  \vio{}           \vio{*}
	\end{tableau}
\end{center}

\noindent ...is produced by...

\begin{verbatim}
	\begin{tableau}{c|s}
	\inp{\ips{ba}}         \const{*VcdObs}  \const*{\textsc{Ident-IO}-[nas]}
	\cand{ba}              \vio{*!}         \vio{}
	\cand[\HandRight]{pa}  \vio{}           \vio{*}
	\end{tableau}
\end{verbatim}

You can also mix the approaches, which is illustrated in the code for Table~\ref{large_example}. (That code is not printed in this manual, but it's available in the .tex version of this file.)

You can control the darkness of the shading using a command like \verb+\SetCellShading{0.4}+. A value of 1 corresponds to white, 0 to black. The default is 0.9.

\subsection{Symbol position}
Some people prefer the ``finger-of-optimality'' to go after the letter. This can be done by using the \verb+\LetterBeforeFinger+ command:

\begin{center}
	\LetterBeforeFinger
	\begin{tableau}{c|c}
	\inp{\ips{ba}}		\const{*VcdObs}	\const*{\textsc{Ident-IO}-[nas]}
	\cand{ba}		\vio{*!}	\vio{}
	\cand[\HandRight]{pa}	\vio{}		\vio{*}
	\end{tableau}
\end{center}

\noindent ...is produced by the following code...

\begin{verbatim}
	\LetterBeforeFinger
	\begin{tableau}{c|c}
	\inp{\ips{ba}}         \const{*VcdObs}  \const*{\textsc{Ident-IO}-[nas]}
	\cand{ba}              \vio{*!}         \vio{}
	\cand[\HandRight]{pa}  \vio{}           \vio{*}
	\end{tableau}
\end{verbatim}

\noindent \verb+\FingerBeforeLetter+ is also available. This is the default.

\subsection{Different symbols}
Using the optional argument to \verb+\cand+, you can add any annotation to a candidate. The following will be a trip down Memory Lane for some people:

\begin{center}
	\begin{tableau}{c|c}
	\inp{\ips{ba}}         \const{*VcdObs} \const*{\textsc{Ident-IO}-[nas]}
	\cand[(FFC)]{ba}       \vio{*!}        \vio{}
	\cand[\HandRight]{pa}  \vio{}          \vio{*}
	\cand[\ding{96}]{ta}   \vio{}          \vio{*}
	\cand[\HandLeft]{sa}   \vio{}          \vio{*}
	\end{tableau}
\end{center}

\noindent Generated by...

\begin{verbatim}
	\begin{tableau}{c|c}
	\inp{\ips{ba}}         \const{*VcdObs} \const*{\textsc{Ident-IO}-[nas]}
	\cand[(FFC)]{ba}       \vio{*!}        \vio{}
	\cand[\HandRight]{pa}  \vio{}          \vio{*}
	\cand[\ding{96}]{ta}   \vio{}          \vio{*}
	\cand[\HandLeft]{sa}   \vio{}          \vio{*}
	\end{tableau}
\end{verbatim}

\noindent For most needs, \verb+\HandRight+ and \verb+\HandLeft+ will be sufficient.

\end{document}
