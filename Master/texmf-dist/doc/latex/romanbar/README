README for romanbar package, 2012/01/01, v1.0f


TABLE OF CONTENTS
=================

1 Introduction
2 Download
3 Installation
4 Additional Packages
5 Package Compatibility
6 Authors/Maintainer
7 Bug Reports
8 Known Problems


1 INTRODUCTION
==============

This LaTeX package allows to write Roman numbers (or any
other text) with bars. (Additionally, commands for converting
Arabic numbers into Roman ones are provided and
an \ifnumeric test function.)
The original code for the bars was written by
Prof. Enrico Gregorio (egreg) and published at
http://tex.stackexchange.com/questions/24065/
roman-numerals-formatting/24084#24084
This material is subject to the LaTeX Project Public License
(LPPL). See http://www.ctan.org/tex-archive/help/Catalogue/
licenses.lppl.html for the details of that license.


2 DOWNLOAD
==========

`romanbar' should become available on CTAN soon:
  CTAN:macros/latex/contrib/romanbar/

Running
tex romanbar.dtx
generates the files
romanbar.ins, romanbar.drv, romanbar.sty,
and romanbar-example.tex.

Also a ZIP file is provided that contains these files,
the manual (romanbar.pdf), the compiled example
(romanbar-example.pdf), and this README, already
sorted in a TDS tree (to be available soon):
  CTAN:install/macros/latex/contrib/romanbar.tds.zip

`CTAN:' means one of the `Comprehensive TeX Archive Network'
nodes or one of its mirrors. This is explained in
  http://www.tex.ac.uk/cgi-bin/texfaq2html?label=archives

The CTAN stuff will be mirrored automatically from the
ftp server, so
  ftp://ftp.tug.org/pub/tex/romanbar/
corresponds to
  CTAN:macros/latex/contrib/romanbar/


3 INSTALLATION
==============

Installation with ZIP file in TDS format
----------------------------------------
The ZIP file `romanbar.tds.zip' contains the files
sorted in a TDS tree. Thus you can directly unpack the
ZIP file inside a TDS tree.
(See CTAN:tds.zip for an explanation of TDS.)
Example:
  cd /...somewhere.../texmf
  unzip /...downloadpath.../romanbar.tds.zip
Do not forget to refresh the file name database of this
TDS tree.
Example:
  texhash /...somewhere.../texmf

Manual installation
-------------------
a) Download the romanbar files from CTAN.
   If necessary, unpack them.
b) Generate the package and driver files:
     tex romanbar.dtx
c) Install the file `*.sty' in your TDS tree:
     cp *.sty TDS:tex/latex/romanbar/
   Replace `TDS:' by the prefix of your TDS tree
   (texmf directory).
d) Create the documentation (if necessary), e.g.
     pdflatex romanbar.dtx
     makeindex -s gind.ist romanbar.idx
     pdflatex romanbar.dtx
     makeindex -s gind.ist romanbar.idx
     pdflatex romanbar.dtx
e) Update the databases if necessary, e.g. for teTeX:
     mktexlsr .../texmf
f) Create the romanbar-example.pdf (if necessary), e.g.
     pdflatex romanbar-example.tex
g) Copy the documentation files to
   "TDS:doc/latex/romanbar/":
   README, romanbar.pdf, romanbar-example.tex,
   romanbar-example.pdf.


4 ADDITIONAL PACKAGES
=====================

romanbar itself does not load other packages.
The romanbar-example.tex uses
* hyperref: http://ctan.org/pkg/hyperref
which again loads other packages
(see the result of the \listfiles command in the log-file
 of the example).


5 PACKAGE COMPATIBILITY
=======================

There are no known incompatibilities.


6 AUTHOR/MAINTAINER
=====================

* Author of the original code for the bars:
   Prof. Enrico Gregorio (egreg), see
   http://tex.stackexchange.com/questions/24065/
    roman-numerals-formatting/24084#24084
* Author of the recent package and current maintainer:
   H.-Martin M�nch


7 BUG REPORTS
==============

A bug report should contain:
* Comprehensive problem description. This includes error or
  warning messages.
* \errorcontextlines=\maxdimen can be added in the
  TeX code to get more information in TeX error messages.
* Minimal test file that shows the problem, but does not
  contain any unnecessary packages and code.
* Used drivers/programs.
* Version information about used packages and programs.
* If you are using LaTeX, then add "\listfiles". Then
  a list of version information is printed at the end
  of the LaTeX run.
* Please no other files than the minimal test file.
  The other files .log, .dvi, .ps, .pdf are seldom
  necessary, so send them only on request.
* Please .zip or .tar.gz your file(s) before sending them!

Bug address
-----------
Bug reports can be send to the maintainer:
  H.-Martin M�nch
  <Martin [dot] Muench [at] Uni-Bonn [dot] de>


8 KNOWN PROBLEMS
=================

Depending on used pdf-viewer and zoom level
the bars might not look as good as when printed.