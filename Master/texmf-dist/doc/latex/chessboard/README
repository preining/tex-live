chessboard --- A package to print chessboards
Version 1.5 Ulrike Fischer 2007

CHANGES
1.5: Corrected some bugs.
     Added key getpiecelists.
     Adapted the package to xskak.
1.4: changed the code so that it work with xkeyval 2.5
1.3a: corrected a small bug
1.3:A lot of the pgf-code have been rewritten and extended.

ATTENTION
Upgrading to this version
can change the look of existing documents.

Notably two things can give problems: Since version 1.3 the value of the
padding key affects much more objects (marks) in the pgf
pictures. So it could be necessary to reset the padding. And the
applycolor has changed its behaviour. It will now also affect
the foreground picture.


CONTENTS AND INSTALLATION

- Readme (this file)
- chessboard-scr.dtx
- chessboard.ins
- chessboard.pdf

Run TeX/LaTeX on chessboard.ins to unpack the dtx.
Put the four extracted files in tex/latex/chessboard/.

The dtx-file doesn't contain any documentation.
So running LaTeX on it makes no sense!

Put chessboard.pdf in
 doc/latex/chessboard/

DOCUMENTATION

chessboard.pdf. There isn't a source for the documentation as it use some
local fonts and styles.

DESCRIPTION

This package offers commands to print chessboards. It can print partial boards,
hide pieces and fields, color the boards and put various marks on the board.
It has a lot of options to place pieces on the board. Using exotic pieces e.g. for
fairy chess is possible.
It can be used together with the newest version (1.4) of the skak package
which you can find at CTAN and together with xskak. 


REQUIREMENTS
chessboard uses some primitives of e\TeX. It needs a recent version
of chessfss and xkeyval. It also needs the packages xifthen,
pgfcore and pgfbaseshapes (from the pgf bundle),
and pst-node (from pstricks).
