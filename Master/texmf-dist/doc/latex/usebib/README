The usebib package is a poor person's replacement for the
more powerful methods provided by biblatex to access data
from a .bib file. After

  \usepackage{usebib}
  \bibinput{biblio}

in the preamble, one is able to say

  \usebibdata{newton1687}{title}

for printing in the document the title of the work with
the BibTeX key `newton1687'. Access to other fields is
provided.

This is version 1.0 of the package

Copyright (C) 2011 by Enrico Gregorio 
<Enrico dot Gregorio at univr dot it>
-------------------------------------------------------

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either
version 1.3c of this license or (at your option) any
later version. The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions
of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is Enrico Gregorio.

This work consists of the files
  usebib.dtx 
  usebib.ins
and the derived file usebib.sty.

To install the distribution:

o run "latex usebib.ins"
o move "usebib.sty" to locations where LaTeX will find
  it (the FAQ on CTAN in /help/uktug-FAQ gives more
  information about this magic place

To produce the documentation in pdf format:

o run "pdflatex usebib.dtx"
o run "makeindex -s gind.ist usebib"
o run "makeindex -s gglo.ist -o usebib.gls usebib.glo"
o run "pdflatex usebib.dtx"

2012/03/17
Enrico Gregorio
