----------------------------------------------------------------
tqft --- a style file for drawing TQFT diagrams with TikZ/PGF
E-mail: stacey@math.ntnu.no
Released under the LaTeX Project Public License v1.3c or later
See http://www.latex-project.org/lppl.txt
----------------------------------------------------------------

This package defines some node shapes useful for drawing TQFT diagrams with TikZ/PGF.

