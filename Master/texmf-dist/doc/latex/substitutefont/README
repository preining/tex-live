Combine font families
*********************

:Date:      Last revised 2012-07-05
:Copyright: © 2011 Günter Milde <milde@users.sf.net>
:Contributor: Special thanks to Ulrike Fischer who provided
              the indirect definition with active ``\nfss@catcodes``.
:Licence:   This work may be distributed and/or modified under the
            conditions of the `LaTeX Project Public License`_,
            either version 1.3 of this license or (at your option)
            any later version.

.. _LaTeX Project Public License: http://www.latex-project.org/lppl.txt

Many free fonts are extensions of a basic font family with new glyphs
or shapes. Sometimes they are given a new name due to license reasons
or the creators preference.

The `substitutefont` package facilitates the task to set up a font
family as substitute for another one in a specified font encoding.

Example:
  Palatino with the standard package `mathpazo` for Latin and 
  `GFS Didot`_ for Greek::
  
    \usepackage[sc,slantedGreek]{mathpazo}
    \substitutefont{LGR}{pplx}{udidot}

Source
  `<substitutefont.sty>`_, `<substitutefont.sty.html>`__

Tests
  * Palatino wiht Greek from `GFS Didot`_:
    `<greek-palatino-didot.tex>`__, `<greek-palatino-didot.pdf>`__
  * Latin Modern with Cyrillic from `CM LGC`_:
    `<cyrillic-times-paratype.tex>`__, `<cyrillic-times-paratype.pdf>`__
  * `TeX-Gyre` with Cyrillic from `ParaType`_:
    `<cyrillic-times-paratype.tex>`__, `<cyrillic-times-paratype.pdf>`__

References:
  `LaTeX2e font selection
  <http://mirror.ctan.org/macros/latex/doc/fntguide.pdf>`_

  `LaTeX font encodings
  <http://mirror.ctan.org/macros/latex/doc/encguide.pdf>`_

  .. _GFS Didot:  http://mirror.ctan.org/help/Catalogue/entries/gfsdidot.html
  .. _CM LGC: http://mirror.ctan.org/help/Catalogue/entries/cm-lgc.html
  .. _ParaType: http://mirror.ctan.org/help/Catalogue/entries/paratype.html
