The gmverse package
is a work of Grzegorz `Natror' Murzynowski,
<natror at o2 dot pl>

   
1. Copyright 2006, 2008 by Grzegorz `Natror' Murzynowski

This program is subject to the LaTeX Project Public License. 
See http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html
for the details of that license.


2. Introduction

The gmverse.sty package redefines the verse environment to introduce
right alignment of the rest of the broken lines. With the voc package
option the verse environment typesets its contents optically centered
(horizontally) on the page.


3. Installation

Just put the gmverse.sty somewhere in the texmf/tex/latex
branch. Creating a /texmf/tex/latex/gm directory may be advisable if
you consider using other packages written by me.

Then you should refresh your TeX distribution's files'
database most probably.


4. Contents

The distribution of this package consists of the following three files.

gmverse.sty
README
gmverse.pdf


5. Documentation

The last of the above files (the .pdf) is a documentation compiled
from the .sty file by running LaTeX on the gmverse.sty file twice:
xelatex gmverse.sty
 in the directory you wish the documentation to be in, 
you don't have copy the .sty file  there---TeX will find it,
then MakeIndex on the gmverse.idx file, and then LaTeX
on gmverse.sty once more.

MakeIndex shell command:
         makeindex -r gmverse
The -r switch is to forbid MakeIndex make implicit ranges since the
(code line) numbers will be hyperlinks

Compiling of the documentation requires the packages: gmdoc
(gmdoc.sty and gmdocc.cls), gmverb.sty, gmutils.sty and also some
standard packages: hyperref.sty, xcolor.sty, geometry.sty,
multicol.sty, lmodern.sty, fontenc.sty that should have been installed
on your computer by default.

If you have not installed the mwart.cls class (available on CTAN in
mwcls package), the result of your compilation may differ a bit from
the .pdf provided in this .zip archive in formattings: If you have not
installed mwart.cls, the standard article.cls will be used.
