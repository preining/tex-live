\documentclass[UTF8, fancyhdr, hyperref]{ctexart}
\usepackage[margin = 2cm, centering, includefoot]{geometry}
\usepackage[
	backend = biber, defernumbers = true, style = caspervector, utf8
]{biblatex}
\usepackage{CJKspace, enumitem, fancyvrb, hologo}

\pagestyle{fancy}\fancyhf{}\cfoot{\thepage}
\renewcommand{\headrulewidth}{0pt}
\setlist{nolistsep}
\hypersetup{allcolors = blue}

\DeclareBibliographyCategory{cited}
\AtEveryCitekey{\addtocategory{cited}{\thefield{entrykey}}}
\addbibresource{readme.bib}
\nocite{*}

\newcommand{\myemph}[1]{\emph{\textcolor{red}{#1}}}

\begin{document}
\title{\textbf{biblatex 参考文献和引用样式：\texttt{caspervector} v0.1.6}}
\author{%
	Casper Ti.\ Vector\thanks{\ %
		\href{mailto:CasperVector@gmail.com}{\texttt{CasperVector@gmail.com}}.%
	}%
}
\date{2013/01/10}
\maketitle

\section{引言}

传统的 \hologo{BibTeX} 引擎存在一些固有的问题：
首先，其样式文件（\verb|bst| 文件）使用后缀式的栈语言编写，
使开发者难以入门和精通，更不便于一般用户进行自定义；
其次，其排序方式很单一，
无法直接实现中文 \TeX{} 用户常常遇到的按汉语拼音排序等需求。

与此相对应，
biblatex\supercite{biblatex}/biber\supercite{biber}
是一套新兴的 \TeX{} 参考文献排版引擎，
其样式文件（设定参考文献样式的 \verb|bbx| 文件和设定引用样式的 \verb|cbx| 文件）
使用 \LaTeX{} 编写，便于学习；
同时，其支持根据 locale 进行排序。

目前 \TeX{} 社区中尚无人发布为中文用户设计的 biblatex 样式。
为了个人需要，同时也是为了给社区提供一个有益的参考，
本文作者根据实际应用中遇到的常见需求编写了
\verb|caspervector| 这个用于顺序编码制的中英文 biblatex 样式。
其逻辑框架基于
国家标准 GB/T 7714--2005\supercite{gbt7714-2005}，
但在其基础上根据个人审美趣味和 biblatex 所能实现的功能而
对参考文献和引用格式进行了较大幅度的修改。

\section{许可协议}

版权所有 \copyright\ 2012 Casper Ti.\ Vector。%
\verb|caspervector| 参考文献和引用样式以
\LaTeX{} Project Public License\footnote{\ %
	\url{http://www.latex-project.org/lppl/}.%
}发布。%
\verb|caspervector| 样式目前由其作者维护。

\section{系统要求和安装方式}
\subsection{系统要求}

\begin{itemize}
	\item biblatex 宏包（\myemph{必需}）：
		\verb|caspervector| 样式基于 biblatex 宏包。
	\item 中文环境（\myemph{必需}）：
		\verb|caspervector| 样式虽支持中文，但其本身不提供中文环境。
		用户仍然需要中文环境才能排版出文档。
	\item biber 程序（\myemph{可选}）：
		用 biber 可以方便地实现文献按字母和拼音顺序排序。
\end{itemize}

以上要求在\myemph{最新}的\myemph{完全版}
\TeX{}Live 系统和 \CTeX{} 套装中都有完善的支持。

\subsection{安装方式}

使用 \TeX{}Live 的用户可以通过在终端中以管理员权限执行
\begin{Verbatim}[frame = single]
tlmgr install biblatex-caspervector
\end{Verbatim}
来安装 \verb|caspervector| 样式。

使用 \CTeX{} 套装的用户可以通过在命令提示符中执行
\begin{Verbatim}[frame = single]
mpm --install=biblatex-caspervector
\end{Verbatim}
来安装 \verb|caspervector| 样式。

\section{使用简介}
\subsection{样式调用}

用户应当通过以下命令调用 \verb|caspervector| 样式：
\begin{Verbatim}[frame = single]
% 如果系统上无法使用 biber，可将“biber”改为“bibtex”。
% “gbk”可能需要改为“utf8”，根据用户使用的字符编码而定。
% “...” 代表其它选项。
\usepackage[
	backend = biber, style = caspervector, gbk, sorting = centy, ...
]{biblatex}
\end{Verbatim}
其中 \verb|sorting| 选项用于指定按哪些字段排序，
除 biblatex 提供的标准选项\supercite{biblatex}外，
\verb|caspervector| 样式还提供 \verb|centy| 和 \verb|ecnty| 两种排序方案
（后者为 \verb|caspervector| 的默认选择），
表示依次按文献语言
（\textbf{ce}nty 表示中文文献在前，\textbf{ec}nty 表示英文文献在前；
文献语言根据 \verb|userf| 字段进行区分，详见第 \ref{sec:fields} 部分）、
编著者姓名（\textbf{n}ame）、标题（\textbf{t}itle）
和出版年（\textbf{y}ear）排序。

参考文献数据库
（\verb|.bib| 文件，其格式见第 \ref{sec:fields}、\ref{sec:entries} 部分）
通过 \verb|\addbibresource| 命令导入，%
\myemph{注意不要省略扩展名 \texttt{.bib}}。
例如，本文的参考文献数据库就是通过下述命令导入的：
\begin{Verbatim}[frame = single]
\addbibresource{readme.bib}
\end{Verbatim}
用户可以多次使用 \verb|\addbibresource| 命令，
从多个参考文献数据库中导入参考文献。

\subsection{引用命令}

\verb|caspervector| 样式支持 biblatex 所提供的引用命令，
其中最常用的是 \verb|\supercite|、\verb|\parencite| 和 \verb|\cite|。
三个命令的用法类似：
\begin{Verbatim}[frame = single]
% 可选参数 prenote 和 postnote 分别用于设定引用记号前、后的注释。
\citecommand[prenote][postnote]{key}
\end{Verbatim}
其中 \verb|\cite| 产生无格式化的引用标记\footnote{\ %
	biblatex 的默认设置是带方括号，
	而 \texttt{caspervector} 样式中出于功能完备性的考虑去掉了方括号。%
}，\verb|\parencite| 产生带方括号的引用标记，
而 \verb|\supercite| 产生上标且带方括号的引用标记\footnote{\ %
	biblatex 的默认设置是只上标、不带方括号，
	而 \texttt{caspervector} 样式中根据作者见到的常见需求加上了方括号。%
}。

例如，在本文中，%
\verb|\parencite{gbt7714-2005}| 的输出是“\parencite{gbt7714-2005}”，
而
\begin{Verbatim}[frame = single]
\cite[文献][第 4 页]{gbt7714-2005}
\end{Verbatim}
的输出是“\cite[文献][第 4 页]{gbt7714-2005}”。

\subsection{文献列表}

使用 \verb|\printbibliography| 命令可以在相应位置排版文献列表。
其可（在方括号内）带一些可选参数\supercite{biblatex}，
常见的有：
\begin{itemize}
	\item \verb|title = 标题|：
		可以用于指定文献列表的标题（默认为“参考文献”）。
	\item \verb|heading = 样式|：
		最常用的是当 \verb|heading| 的值为 \verb|bibintoc| 时，
		可以将参考文献加入目录中；
		当其值为 \verb|bibnumbered| 时，
		参考文献列表参与章节编号（当然也会被自动加入目录中）。
\end{itemize}

例如，用
\begin{Verbatim}[frame = single]
\printbibliography[title = {文献}, heading = bibnumbered]
\end{Verbatim}
可以将文献列表的标题改为 “文献”，
并使文献列表参与章节编号。

\subsection{编译方法}

一般情况下，
\begin{Verbatim}[frame = single]
# “texfile”是被 TeX 编译的文件名中除去“.tex”的部分。
# “pdflatex”可改为其它 TeX 程序，使用纯 latex 编译时可能还需要运行 dvipdfmx。
# 如果系统上无法使用 biber，可将执行 biber 的一行改为
#   bibtex texfile
# 但将无法按拼音排序。
pdflatex texfile
biber -l zh__pinyin texfile
pdflatex texfile
\end{Verbatim}
即可实现正确的排版。
某些情况下（详见 biblatex 手册\supercite{biblatex}），
用户可能需要在运行 \verb|biber|（或 \verb|bibtex|）
之后运行两次 \verb|pdflatex|（或 \verb|xelatex|、\verb|latex|）
才能获得正确的引用标记。

另外，上述执行 \verb|biber| 的一行命令中，
\verb|-l| 的参数 \verb|zh__pinyin| 可改为其它
被 Perl 的 \verb|Unicode::Collate| 模块支持的 locale\footnote{\ %
	\url{http://search.cpan.org/~sadahiro/Unicode-Collate/Collate/Locale.pm}.%
}，这样在排序时将使用相应的排序规则。
例如，如果要按笔画排序的话，可以将 \verb|zh__pinyin| 改为 \verb|zh__stroke|。

\section{字段介绍}\label{sec:fields}
\subsection{基本字段}

除非特别指出，此部分字段在所有类型条目中均可用。

\begin{itemize}
	\item \verb|author|、\verb|editor|、\verb|translator|：
		作者、编者、译者。\\
		\myemph{%
			注：
			在析出文献条目中，
			\texttt{author}、\texttt{editor}、\texttt{translator}
			专指析出文献的作者、编者、译者。
			在 \texttt{@patent} 类条目中，
			\texttt{author} 也可指专利的持有者。%
		}
	\item \verb|bookauthor|、\verb|booktitle|：析出文献所出自文献的作者和题名。
	\item \verb|title|：文献题名。
	\item \verb|type|：文献类型和电子文献载体标志代码\supercite{gbt7714-2005}。
	\item \verb|location|：出版地，或（在 \verb|@patent| 类条目中）专利申请地。
	\item \verb|publisher|：出版者。
	\item \verb|journal|/\verb|journaltitle|：连续出版物题名，这两个字段是等价的。
	\item \verb|year|/\verb|date|：出版年、日期，这两个字段只需填写一个即可。
	\item \verb|volume|：期刊中文献所处的卷号。
	\item \verb|number|：期刊中文献所处的期号，或专利的申请号。
	\item \verb|pages|：文献页码。
	\item \verb|url|：文献的 URL。
	\item \verb|urldate|：检索日期，或 URL 的访问日期。
	\item \verb|addendum|：补充说明，排版在文献列表中相应条目的最后。
\end{itemize}

\subsection{特殊字段}

\begin{itemize}
	\item \verb|userf|：
		值为 \verb|zh| 或 \verb|cn| 时，相应条目在文献列表中用中文排版；
		否则（为其他值或未定义时）用中文排版。\\
		\myemph{%
			注：
			不可用 \texttt{language} 字段区分中英文文献，
			因为 biblatex 使用 babel\supercite{babel} 宏包处理此字段，
			但后者目前不支持中文，所以可能出错。%
		}
	\item 其它通用特殊字段，见 biblatex 手册\supercite{biblatex}。
\end{itemize}

\section{条目类型}\label{sec:entries}
\subsection{\texttt{@book} 类型}

\verb|@book| 类型对应于 GB/T 7714--2005 中所指的“专著”和“电子文献”类型，
其支持的常见别名包括 \verb|@booklet|、\verb|@proceedings|、
\verb|@report|、\verb|@thesis|、\verb|@unpublished|。

\verb|@book| 类条目必需的基本字段为 \verb|title|。

除必需字段之外，\verb|@book| 类条目也支持以下基本字段：
\verb|author|、\verb|editor|、\verb|translator|、
\verb|type|、\verb|location|、\verb|publisher|、
\verb|year|/\verb|date|、\verb|pages|、
\verb|url|、\verb|urldate|、\verb|addendum|。

\subsection{\texttt{@incollection} 类型}

\verb|@incollection| 类型对应于 GB/T 7714--2005 中所指的“专著中的析出文献”，
其支持的常见别名包括
\verb|@bookinbook|、\verb|@conference|、\verb|@inbook|、\verb|@inproceedings|。

\verb|@incollection| 类条目必需的基本字段为 \verb|title| 以及 \verb|booktitle|。

除必需字段之外，\verb|@incollection| 类条目也支持以下基本字段：
\verb|author|、\verb|editor|、\verb|translator|、\verb|bookauthor|、
\verb|type|、\verb|location|、\verb|publisher|、
\verb|year|/\verb|date|、\verb|pages|、
\verb|url|、\verb|urldate|、\verb|addendum|。

\subsection{\texttt{@periodical} 类型}

\verb|@periodical| 类型对应于 GB/T 7714--2005 中所指的“连续出版物”。

\verb|@periodical| 类条目必需的基本字段为
\verb|title|/\verb|journal|/\verb|journaltitle| 三者中的至少一个。

除必需字段之外，\verb|@periodical| 类条目也支持以下基本字段：
\verb|author|/\verb|editor|/\verb|translator| 
\verb|type|、\verb|location|、\verb|publisher|、
\verb|year|/\verb|date|、\verb|volume|、\verb|number|、\verb|pages|、
\verb|url|、\verb|urldate|、\verb|addendum|。

\subsection{\texttt{@article} 类型}

\verb|@article| 类型对应于 GB/T 7714--2005 中所指的“连续出版物”。

\verb|@article| 类条目必需的基本字段为
\verb|journal|/\verb|journaltitle| 两者中的至少一个，
以及 \verb|year|/\verb|date| 两者中的至少一个。

除必需字段之外，\verb|@article| 类条目也支持以下基本字段：
\verb|author|、\verb|title|、\verb|type|、
\verb|volume|、\verb|number|、\verb|pages|、
\verb|url|、\verb|urldate|、\verb|addendum|。

\subsection{\texttt{@patent} 类型}

\verb|@patent| 类型用于专利文献。

\verb|@patent| 类条目必需的基本字段为 \verb|title|，
以及 \verb|year|/\verb|date| 两者中的至少一个。

除必需字段之外，\verb|@article| 类条目也支持以下基本字段：
\verb|author|、\verb|title|、\verb|type|、
\verb|location|、\verb|number|、
\verb|url|、\verb|urldate|、\verb|addendum|。

\subsection{\texttt{@customf} 类型}

\verb|@customf| 类型为特殊类型，
专用于在文献列表的相应条目中排版自定义的文字。
此类条目必需且唯一支持的基本字段为 \verb|addendum|，
用户可将其设为自己希望排版的内容。

\myemph{%
	注：
	\texttt{@customf} 类型虽不支持 \texttt{author} 等字段，
	但用户仍可以设定它们的值。
	这样虽不能自动根据这些字段排版，
	但在仍可以根据它们
	（主要是 \texttt{author}、\texttt{title} 和 \texttt{year} 三个字段）
	进行排序。 %
}

\section{FAQ 和其它使用提示}

用户可以通过省略可选字段的方式来避免排版相应的内容。
例如，省略 \verb|type| 字段便可使相应条目不排版文献类型和电子文献载体标志代码。

用户可以通过手动调用格式化命令来临时覆盖预设的格式设定，
例如文献 \parencite{1-7} 中的出版年便是通过设定
\begin{Verbatim}[frame = single]
year = {1845\textmd{\emph{（清同治四年）}}}
\end{Verbatim}
得到的。

\verb|caspervector| 样式不支持 \verb|edition| 字段，
用户可以在 \verb|title| 等字段中手动标注。

\section{存在的问题}

如上文所述，因为 babel 宏包目前不支持中文，
故对中英文的分别处理只能通过比较麻烦的方式实现，
指定语言也只能通过一个自定义的字段（\verb|userf|）进行。

biblatex/biber 所支持的排序方式仍有一定限制，例如这样的需求目前就无法实现：
参考文献数据库中被引用过的参考文献按引用顺序排在最前，
未被引用的参考文献按字母和拼音顺序排序。
不过根据 biber 主页\footnote{\ %
	\url{http://biblatex-biber.sourceforge.net/}.%
}上的说明，biblatex 2.x 系列将支持这一功能。

\section{更新记录}
\VerbatimInput[tabsize = 4, baselinestretch = 1]{ChangeLog.txt}

\printbibliography%
	[heading = bibnumbered, title = {本文参考文献}, category = cited]
\printbibliography[
	heading = bibnumbered, notcategory = cited,
	title = {%
		其它参考文献示例
		（引自\texorpdfstring{文献 \parencite{gbt7714-2005}}{ GB/T 7714-2005}）
	}
]
\end{document}

