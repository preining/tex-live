% arara: pdflatex
% arara: pdflatex
% arara: makeindex: { sort: true, style: modiagram_en.ist }
% arara: pdflatex
% --------------------------------------------------------------------------
% the MODIAGRAM package
%
%   easy creation of molecular orbital diagrams
%
% --------------------------------------------------------------------------
% Clemens Niederberger
% Web:    https://www.bitbucket.org/cgnieder/modiagram
% E-Mail: contact@mychemistry.eu
% --------------------------------------------------------------------------
% If you have any ideas, questions, suggestions or bugs to report, please
% feel free to contact me.
% --------------------------------------------------------------------------
% Copyright 2011--2012 Clemens Niederberger
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Clemens Niederberger.
%
% This work consists of the files modiagram.sty, modiagram_en.tex,
% README and the derived file modiagram_en.pdf.
% --------------------------------------------------------------------------
% if you want to compile this documentation you'll need the document class
% `cnpkgdoc' which you can get here:
%    https://bitbucket.org/cgnieder/cnpkgdoc/
%    the class is licensed LPPL 1.3 or later
\documentclass{cnpkgdoc}
\docsetup{
  pkg      = modiagram,
  subtitle = Easy Creation of Molecular Orbital Diagrams ,
  code-box     = {
    skipbelow        = .5\baselineskip plus .5ex minus .5ex ,
    skipabove        = .5\baselineskip plus .5ex minus .5ex ,
    roundcorner      = 3pt ,
    innerleftmargin  = 1.5em ,
    innerrightmargin = 1.5em
  }
}

\addcmds{
  AO,
  atom,
  ch,
  connect,
  draw,
  EnergyAxis,
  lewis,
  Lewis,
  molecule,
  MOsetup,
  node,
  textcolor,
  textsigma,
  textSigma
}

\usepackage[osf]{libertine}
\cnpkgcolors{
  main   => cnpkgred ,
  key    => yellow!40!brown ,
  module => cnpkgblue ,
  link   => black!90
}

\renewcommand*\othersectionlevelsformat[3]{%
  \textcolor{main}{#3\autodot}\enskip}
\renewcommand*\partformat{%
  \textcolor{main}{\partname~\thepart\autodot}}

\usepackage{embrac}
\ChangeEmph{[}[,.02em]{]}[.055em,-.08em]
\ChangeEmph{(}[-.01em,.04em]{)}[.04em,-.05em]
\usepackage{fnpct}

\usepackage{booktabs}

\usepackage{acro}
\DeclareAcronym{AO}{AO}{atomic orbital}
\DeclareAcronym{MO}{MO}{molecular orbital}

\usepackage{chemmacros,chemfig}
\chemsetup[chemformula]{font-family=fxl}

\usepackage{makeidx}
\usepackage{filecontents}
\begin{filecontents*}{\jobname.ist}
 heading_prefix "{\\bfseries "
 heading_suffix "\\hfil}\\nopagebreak\n"
 headings_flag  1
 delim_0 "\\dotfill "
 delim_1 "\\dotfill "
 delim_2 "\\dotfill "
 delim_r "\\nohyperpage{\\textendash}"
 suffix_2p "\\nohyperpage{\\,f.}"
 suffix_3p "\\nohyperpage{\\,ff.}"
\end{filecontents*}

\makeindex

\TitlePicture{%
\begin{MOdiagram}[style=fancy,distance=7cm,AO-width=15pt,labels,names]
  \atom[N]{left}{
    2p = {0;up,up,up}
  }
  \atom[O]{right}{
    2p = {2;pair,up,up}
  }
  \molecule[NO]{
    2pMO = {1.8,.4;pair,pair,pair,up},
    color = { 2piy*=red }
  }
 \end{MOdiagram}}

\NewDocumentCommand \AOinline { o m }
  {%
    \begingroup
      \IfNoValueTF{#1}
        {\MOsetup{ style=square,AO-width=8pt }}%
        {\MOsetup{ style=square,AO-width=8pt , #1 }}%
      \begin{MOdiagram}
        \AO{s}{0;#2}
      \end{MOdiagram}%
    \endgroup
  }

\pdfstringdefDisableCommands{%
  \def\key#1{#1}%
}

\begin{document}

\section{Licence, Requirements}
Permission is granted to copy, distribute and/or modify this software under the
terms of the LaTeX Project Public License, version 1.3 or later
(\url{http://www.latex-project.org/lppl.txt}). This package has the status
``maintained.''

\modiagram needs and loads the packages \paket{expl3}, \paket{xparse},
\paket{l3keys2e}, \paket[pgf]{\protect\TikZ} and \paket{textgreek}. Additionally the
\TikZ libraries \paket*{calc} and \paket*{arrows} are loaded. Knowledge of
\paket*{pgf} or \TikZ are helpful.

\section{Motivation}
This package has been written as a reaction to a question on
\url{http://tex.stackexchange.com/}. To be more precise: as a reaction to the
question ``\href{http://tex.stackexchange.com/questions/13863/molecular-orbital-diagrams-in-latex}%
{Molecular orbital diagrams in LaTeX}.'' There it says
\begin{zitat}
 I'm wondering if anyone has seen a package for drawing (qualitative) molecular
 orbital splitting diagrams in \LaTeX? Or if there exist any packages that can
 be easily re-purposed to this task?

 Otherwise, I think I'll have a go at it in \TikZ.
\end{zitat}
The problem was solved using \TikZ, since no package existed for that purpose.
For one thing \modiagram is intended to fill this gap. I also found it very
tedious, to make all this copying and pasting when I needed a second, third,
\ldots\ diagram. \modiagram took care of that. 

\section{Main Commands}
All molecular orbital (MO) diagrams are created using the environment
\env{MOdiagram}{}.

\subsection{The \cmd{atom} Command}
\begin{beschreibung}
 \Befehl{atom}[<name>]{left|right}\ma{<AO-spec>} \\
   \oa{<name>} caption of the atom;\\
   \ma{left|right} on the left or the right in the diagram;\\
   \ma{<AO-spec>} specifications of the \acp{AO}.
\end{beschreibung}
Let's take a look at an example:
\begin{beispiel}
 \begin{MOdiagram}
  \atom{right}{
    1s = {  0; pair} ,
    2s = {  1; pair} ,
    2p = {1.5; up, down }
  }
 \end{MOdiagram}
\end{beispiel}

As you can see, the argument \ma{<AO-spec>} is essential to create the actual
orbitals and the electrons within. You can use these key/value pairs to specify
what you need:
\begin{beschreibung}
 \Option{1s}{<rel-energy>; <el-spec>}
 \Option{2s}{<rel-energy>; <el-spec>}
 \Option{2p}{<rel-energy>; <x el-spec>, <y el-spec>, <z el-spec>} \\
   \code{<el-spec>} can have the values \code{pair}, \code{up} and \code{down} or
   can be left empty. \code{<rel-energy>} actually is the $y$ coordinate and shifts
   the \ac{AO} vertically by \code{<rel-energy>} \si{\centi\metre}.
\end{beschreibung}

The argument \ma{left|right} is important, when p orbitals are used. For instance
compare the following example to the one before:
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left}{
    1s = {  0; pair} ,
    2s = {  1; pair} ,
    2p = {1.5; up, down }
  }
 \end{MOdiagram}
\end{beispiel}

When both variants are used one can also see, that the right atom is shifted to
the right (hence the naming). The right atom is shifted by \SI{4}{\centi\metre}
per default and can be adjusted individually, see page~\pageref{option:distance}.
\begin{beispiel}[below]
 \begin{MOdiagram}
  \atom{left}{
    1s = {  0; pair} ,
    2s = {  1; pair} ,
    2p = {1.5; up, down }
  }
  \atom{right}{
    1s = {  0; pair} ,
    2s = {  1; pair} ,
    2p = {1.5; up, down }
  }
 \end{MOdiagram}
\end{beispiel}
With the command \cmd{molecule} (section~\ref{ssec:molecule}) the reason for the
shift becomes clear.

Any of the arguments for the \ac{AO} can be left empty or be omitted.
\begin{beispiel}
 Without argument: default height, full:
 \begin{MOdiagram}
  \atom{left}{1s, 2s, 2p}
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 empty argument: default height, empty:
 \begin{MOdiagram}
  \atom{left}{1s=, 2s=, 2p=}
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 using some values:\\
 \begin{MOdiagram}
  \atom{left}{1s, 2s=1, 2p={;,up} }
 \end{MOdiagram}
\end{beispiel}

\subsection{The \cmd{molecule} Command}\label{ssec:molecule}
\begin{beschreibung}
 \Befehl{molecule}[<name>]{<MO-spec>} \\
   \oa{<name>} caption of the molecule; \\
   \ma{<MO-spec>} specifications of the \acp{MO};
\end{beschreibung}
An example first:
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left} { 1s = {  0; up } }
  \atom{right}{ 1s = {  0; up } }
  \molecule { 1sMO = {.75; pair } }
 \end{MOdiagram}
\end{beispiel}
The command \cmd{molecule} connects the \acp{AO} with the bonding and anti-bondung
\acp{MO}. \cmd{molecule} can only be used \emph{after} one has set \emph{both}
atoms since the orbitals that are to be connected must be known.

The argument \ma{<MO-spec>} accepts a comma separated list of key/value pairs:
\begin{beschreibung}
  \option{1sMO}{<energy gain>/<energy loss>; <s el-spec>, <s* el-spec>}
    connects the \acp{AO} specified by \key{1s}.
  \option{2sMO}{<energy gain>/<energy loss>; <s el-spec>, <s* el-spec>}
    connects the \acp{AO} specified by \key{2s}.
  \option{2pMO}{<s energy gain>/<s energy loss>, <p energy gain>/<p energy loss>;
    <s el-spec>, <py el-spec>, <pz el- spec>, <py* el-spec>, <pz* el-spec>,
    <s* el-spec>}
    connects the \acp{AO} specified by \key{2p}.
\end{beschreibung}
Obviously the regarding \acp{AO} must have been set in order to connect them.
This for example won't work:
\begin{beispiel}[code only]
 \begin{MOdiagram} 
  \atom{left} { 1s = 0 }
  \atom{right}{ 1s = 0 }
  \molecule { 2sMO = .75 }
 \end{MOdiagram}
\end{beispiel}
The value used in \code{<energy gain>} determines how many \si{\centi\metre} the
bonding \ac{MO} lies below the lower \ac{AO} or how many \si{\centi\metre} the
anti-bondung \ac{MO} lies above the higher \ac{AO}.

\begin{beispiel}[below]
 same level:
 \begin{MOdiagram}
  \atom{left} { 1s = {  0; up } }
  \atom{right}{ 1s = {  0; up } }
  \molecule { 1sMO = {.75; pair } }
 \end{MOdiagram}

 different levels:
 \begin{MOdiagram}
  \atom{left} { 1s = {  0; up } }
  \atom{right}{ 1s = {  1; up } }
  \molecule { 1sMO = {.25; pair } }
 \end{MOdiagram}
\end{beispiel}

If you specify \code{<energy loss>} you can create non-symmetrical splittings.
Then, the first value (\code{<energy gain>}) is used for the bonding \ac{MO} and
the second value (\code{<energy loss>}) is used for the anti-bonding \ac{MO}.
\begin{beispiel}[below]
 \begin{MOdiagram}
  \atom{left} { 1s = {  0; up } }
  \atom{right}{ 1s = {  0; up } }
  \molecule { 1sMO = {.75/.25; pair } }
 \end{MOdiagram}

 \begin{MOdiagram}
  \atom{left} { 1s = {  0; up } }
  \atom{right}{ 1s = {  1; up } }
  \molecule { 1sMO = {.25/.75; pair } }
 \end{MOdiagram}
\end{beispiel}

Please be aware, that you have to specify \emph{two} such values or pairs with
\key{2pMO}: the splitting of the \textsigma\ orbitals and the splitting of the
\textpi\ orbitals.
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left} { 2p = { 0; up, up } }
  \atom{right}{ 2p = { 1; up, up } }
  \molecule { 2pMO = { 1.5, .75; pair, up, up } } 
 \end{MOdiagram}
\end{beispiel}

The complete \ac{MO} diagram for triplett dioxygen now could look something like
that:
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left}{
    1s, 2s, 2p = {;pair,up,up}
  }
  \atom{right}{
    1s, 2s, 2p = {;pair,up,up}
  }
  \molecule{
    1sMO, 2sMO, 2pMO = {;pair,pair,pair,up,up}
  }
 \end{MOdiagram}
\end{beispiel}

\subsection{The Naming Scheme}\label{Namensgebung}
Since one wants to be able to put labels to the orbitals and since they are nodes
in a \code{tikzpicture}, the internal naming scheme is important. It closely
follows the function:
\begin{center}
\begin{MOdiagram}[distance=6cm,AO-width=20pt,labels-fs=\tt\footnotesize,labels-style={yshift=10pt}]
 \atom{left}{
   1s=0,
   2s=2,
   2p=5,
   label={ 1sleft={1sleft}, 2sleft={2sleft}, 2pxleft={2pxleft}, 2pyleft={2pyleft}, 2pzleft={2pzleft} }
 }
 \atom{right}{
   1s=0,
   2s=2,
   2p=5,
   label={ 1sright={1sright}, 2sright={2sright}, 2pxright={2pxright}, 2pyright={2pyright}, 2pzright={2pzright} }
 }
 \molecule{
   1sMO=.5,
   2sMO=.5,
   2pMO={1.5,.5},
   label={ 1sigma={1sigma}, 1sigma*={1sigma*}, 2sigma={2sigma}, 2sigma*={2sigma*}, 2psigma={2psigma}, 2psigma*={2psigma*}, 2piy={2piy}, 2piy*={2piy*}, 2piz={2piz}, 2piz*={2piz*} }
 }
\end{MOdiagram}
\end{center}
With these names it is possible to reference the orbitals with the known \TikZ
commands:
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left} { 1s = 0 }
  \atom{right}{ 1s = 0 }
  \molecule { 1sMO = .75 }
  \draw[<->,red,semithick] (1sigma.center) -- (1sigma*.center) ;
  \draw[red] (1sigma*) ++ (2cm,.5cm) node {splitting} ;
 \end{MOdiagram}
\end{beispiel}

\begin{beispiel}
 \begin{MOdiagram}
  \atom{left} { 1s = 0 }
  \atom{right}{ 1s = 0 }
  \molecule { 1sMO = .75 }
  \draw[draw=blue,very thick,fill=blue!40,opacity=.5] (1sigma*) circle (8pt);
  \draw[<-,shorten <=8pt,shorten >=15pt,blue] (1sigma*) --++(2,1) node {anti-bonding MO};
 \end{MOdiagram}
\end{beispiel}

\subsection{Placing AOs and MOs Arbitrarily}\label{ssec:AO_MO_irgendwo}
The standard orbitals are not always sufficient in order to draw a correct \ac{MO}
diagram. For example in the \ac{MO} diagram of \ch{XeF2} one would need the part
that illustrates the interaction between the bonding and anti-bonding combination
of two p orbitals of Flourine with one p orbital of Xenon:
\begin{center}
 \begin{MOdiagram}[names]
  \atom[\lewis{0.,F}\hspace*{5mm}\lewis{4.,F}]{left}{1s=.2;up,up-el-pos={1sleft=.5}}
  \atom[Xe]{right}{1s=1.25;pair}
  \molecule[\ch{XeF2}]{1sMO={1/.25;pair}}
  \AO(1cm){s}{0;up}
  \AO(3cm){s}{0;pair}
  \connect{ AO1 & AO2 }
  \node[right,xshift=4mm] at (1sigma) {\footnotesize bonding};
  \node[above] at (AO2.90) {\footnotesize non-bonding};
  \node[above] at (1sigma*.90) {\footnotesize anti-bonding};
 \end{MOdiagram}
\end{center}

To create diagrams like this there is the following command, which draws a single
\ac{AO}:
\begin{beschreibung}
 \Befehl{AO}[<name>]\da{<xshift>}\ma{<type>}\oa{<key = val>}\ma{<energy>;<el-spec>} \\
   \oa{<name>} (optional) name of the node; if not specified, \code{AO\#} is used where
   \code{\#} is a consecutive number. \\
   \oa{<xshift>} vertical position of the orbitals, a \TeX\ dimension. \\
   \ma{<type>} \code{s} or \code{p}. \\
   \oa{<key = val>} key/value pairs with which the \ac{AO} can be customized, see
   section~\ref{ssec:AO_anpassen}. \\
   \ma{<AO-spec>} specification of the \ac{AO}.
\end{beschreibung}

Depending on the \code{<type>} one s or three p orbitals are drawn.
\begin{beispiel}
 \begin{MOdiagram}
  \AO{s}{0;}
  \AO(-20pt){p}{1;pair,up,down}
 \end{MOdiagram}
\end{beispiel}

If one wants to place such an \ac{AO} at the position of an atom, one has to know
their \code{<xshift>}. They have predefined values (also see
section~\ref{orbital-positionen}):\label{xshift}
\begin{itemize}
 \item atom left: \SI{1}{\centi\metre}
 \item molecule: \SI{3}{\centi\metre}
 \item atom right: \SI{5}{\centi\metre}
\end{itemize}
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left} {1s=0}
  \atom{right}{1s=0}
  \molecule {1sMO=1}
  \AO(1cm){s}{2}
  \AO(3cm){s}{2}
  \AO(5cm){s}{2}
 \end{MOdiagram}
\end{beispiel}
Within the p orbitals there is an additional shift by \SI{20}{pt} per orbital.
This is equivalent to a double shift by the length \code{AO-width} (see
section~\ref{option:AO-width}):
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left} {2p=0}
  \atom{right}{2p=0}
  % above the left atom:
  \AO(1cm)     {s}{ .5}
  \AO(1cm-20pt){s}{  1;up}
  \AO(1cm-40pt){s}{1,5;down}
  % above the right atom:
  \AO(5cm)     {s}{ .5}
  \AO(5cm+20pt){s}{  1;up}
  \AO(5cm+40pt){s}{1.5;down}
 \end{MOdiagram}
\end{beispiel}

The \acp{AO} created with \cmd{AO} also can be connected. For this you can use
the \TikZ command \cmd{draw}, of course. You can use the predefined node names\ldots
\begin{beispiel}
 \begin{MOdiagram}
  \AO{s}{0} \AO(2cm){s}{1}
  \AO{s}{2} \AO(2cm){s}{1.5}
  \draw[red] (AO1.0) -- (AO2.180) (AO3.0) -- (AO4.180);
 \end{MOdiagram}
\end{beispiel}
\ldots\ or use own node names
\begin{beispiel}
 \begin{MOdiagram}
  \AO[a]{s}{0} \AO[b](2cm){s}{1}
  \AO[c]{s}{2} \AO[d](2cm){s}{1.5}
  \draw[red] (a.0) -- (b.180) (c.0) -- (d.180);
 \end{MOdiagram}
\end{beispiel}

The predefined names are \code{AO1}, \code{AO2} \etc for the type \code{s} and
\code{AO1x}, \code{AO1y}, \code{AO1z}, \code{AO2x} \etc for the type \code{p}.
Nodes of the type \code{p} get an \code{x}, \code{y} or \code{z} if you specify
your own name, too.
\begin{beispiel}
 \begin{MOdiagram}
  \AO{p}{0}
  \draw[<-,shorten >=5pt] (AO1y.-90) -- ++ (.5,-1) node {y};
 \end{MOdiagram}
 and
 \begin{MOdiagram}
  \AO[A]{p}{0}
  \draw[<-,shorten >=5pt] (Ay.-90) -- ++ (.5,-1) node {y};
 \end{MOdiagram}
\end{beispiel}

However, if you want the lines to be drawn in the same style as the ones created
by \cmd{molecule}\footnote{which can be customized, see page~\pageref{option:lines}},
you should use the command \cmd{connect}.
\begin{beschreibung}
 \Befehl{connect}{<AO-connect>} \\
   \ma{<AO-connect>} comma separated list of node name pairs connected with
   \code{\&}.
\end{beschreibung}
This command expects a comma separated list of node name pairs that are to be
connected. The names have to be connected with a \code{\&}:
\begin{beispiel}
 \begin{MOdiagram}
  \AO{s}{0;} \AO(2cm){s}{1;}
  \AO{s}{2;} \AO(2cm){s}{1.5;}
  \connect{ AO1 & AO2, AO3 & AO4 }
 \end{MOdiagram}
\end{beispiel}

Some things still need to be said: \cmd{connect} adds the anchor \code{east} to
the first name and the anchor \code{west} to the second one. This means a
connection only makes sense from the left to the right. However, you can add own
anchors using the usual \TikZ way:
\begin{beispiel}
 \begin{tikzpicture}
  \draw (0,0) node (a) {a} ++ (1,0) node (b) {b}
        ++ (0,1) node (c) {c} ++ (-1,0) node (d) {d} ;
  \connect{ a.90 & d.-90, c.180 & d.0 }
 \end{tikzpicture}
\end{beispiel}

\subsection{The Positioning Scheme}\label{orbital-positionen}
The figure below shows the values of the $x$ coordinates of the orbitals
depending on the values of \code{<distance>} (\code{<dist>}) and \code{<AO-width>}
(\code{<AO>}). In sections~\ref{option:distance} and \ref{option:AO-width} these
lengths and how they can be changed are discussed.
\begin{center}
\begin{MOdiagram}[
  AO-width=22pt,
  labels-fs=\ttfamily\scriptsize,
  labels-style={text width=40pt,align=center,yshift=11pt}]
 \atom{left}{
   1s=0,
   2s=2,
   2p=5.5,
   label={
     1sleft={1cm},
     2sleft={1cm},
     2pxleft={1cm - 4*<AO>},
     2pyleft={1cm - 2*<AO>},
     2pzleft={1cm}
   }}
 \atom{right}{
   1s=0,
   2s=2,
   2p=5.5,
   label={
     1sright={1cm + <dist>},
     2sright={1cm + <dist>},
     2pxright={1cm+ <dist>},
     2pyright={1cm + <dist> + 2*<AO>},
     2pzright={1cm + <dist> + 4*<AO>}
   }}
 \molecule{
   1sMO=.5,
   2sMO=.5,
   2pMO={2,.75},
   label={
     1sigma={.5*<dist> + 1cm},
     1sigma*={.5*<dist> + 1cm},
     2sigma={.5*<dist> + 1cm},
     2sigma*={.5*<dist> + 1cm},
     2psigma={.5*<dist> + 1cm},
     2psigma*={.5*<dist> + 1cm},
     2piy={.5*<dist> + 1cm - <AO>},
     2piy*={.5*<dist> + 1cm - <AO>},
     2piz={.5*<dist> + 1cm + <AO>},
     2piz*={.5*<dist> + 1cm + <AO>}
   }
 }
\end{MOdiagram}
\end{center}

\subsection{Default Values}
If you leave the arguments (or better: values) for the specification of the
\ac{AO} or \ac{MO} empty or omit them, default values are used. The table below
shows you, which ones.
\begin{center}\small
\begin{tabular}{l>{\ttfamily}l>{\ttfamily}l>{\ttfamily}l}
 \toprule &
  \normalfont\bfseries\ac{AO}/\ac{MO} &
  \normalfont\bfseries omitted &
  \normalfont\bfseries empty \\
 \midrule
  syntax: &      & 1s                                       & 1s= \\
 \midrule
          & 1s   & \{0;pair\}                               & \{0;\} \\
          & 2s   & \{2;pair\}                               & \{2;\} \\
          & 2p   & \{5;pair,pair,pair\}                     & \{5;,{},\} \\
 \midrule
          & 1sMO & \{.5;pair,pair\}                         & \{.5;,\} \\
          & 2sMO & \{.5;pair,pair\}                         & \{.5;,\} \\
          & 2pMO & \{1.5,.5;pair,pair,pair,pair,pair,pair\} & \{1.5,.5;{},{},{},{},{},\} \\
 \bottomrule
\end{tabular}
\end{center}
This is similar for the \cmd{AO} command (page~\pageref{ssec:AO_MO_irgendwo});
it needs a value for \code{<energy>}, though.
\begin{center}\small
\begin{tabular}{>{\ttfamily}l>{\ttfamily}l>{\ttfamily}l}
 \toprule\bfseries
  <type> & \bfseries <el-spec> \\
 \midrule
  s      & pair \\
  p      & pair,pair,pair \\
 \bottomrule
\end{tabular}
\end{center}

Compare these examples:
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left} { 1s={0;pair} }
  \atom{right}{ 1s }
 \end{MOdiagram}

 \hrulefill
 
 \begin{MOdiagram}
  \atom{left}{ 1s=1 }
  \atom{right}{ 1s= }
 \end{MOdiagram}
\end{beispiel}

\section{Customization}
Th options of the section~\ref{ssec:umgebungs_optionen} can be set global as
package option, \ie with \lstinline+\usepackage[<key = val>]{modiagram}+, or via
the setup command \cmd{MOsetup}{<key = val>}.

\subsection{Environment Options}\label{ssec:umgebungs_optionen}
There are some options with which the layout of the \ac{MO} diagrams can be changed:
\begin{beschreibung}
 \option{style}{<type>} change the style of the orbitals and the connecting lines,
   section~\ref{option:style}.
 \option{distance}{<dim>} distance betwen left and right atom,
   section~\ref{option:distance}.
 \option{AO-width}{<dim>} change the width of orbitals,
   section~\ref{option:AO-width}.
 \option{el-sep}{<num>} distance between the electron pair arrows,
   section~\ref{option:electrons}.
 \option{up-el-pos}{<num>} position of the spin-up arrow,
   section~\ref{option:electrons}.
 \option{down-el-pos}{<num>} position of the spin-down arrow,
   section~\ref{option:electrons}.
 \option{lines}{<tikz>} change the \TikZ style of the connecting lines,
   section~\ref{option:lines}.
 \option{names}{<bool>} add captions to the atoms and the molecule,
   section~\ref{option:names}.
 \option{names-style}{<tikz>} change the \TikZ style of the captions,
   section~\ref{option:names_style}.
 \option{names-style-add}{<tikz>} change the \TikZ style of the captions,
   section~\ref{option:names_style}.
 \option{labels}{<bool>} add default labels to the orbitals,
   section~\ref{option:labels}.
 \option{labels-fs}{<cs>} change the font size of the labels,
   section~\ref{option:labels-fs}.
 \option{labels-style}{<tikz>} change the \TikZ style of the labels,
   section~\ref{option:labels-style}.
\end{beschreibung}
They all are discussed in the following sections. If they're used as options for the environment, they'r set locally and only change that environment.
\begin{beispiel}[code only]
 \begin{MOdiagram}[<key = value>]
  ...
 \end{MOdiagram}
\end{beispiel}

\subsubsection{Option \key{style}}\label{option:style}
There are five different styles which can be chosen.
\begin{itemize}
 \item\key{style}{plain} \AOinline[style=plain]{pair} (default)
 \item\key{style}{square} \AOinline[style=square]{pair}
 \item\key{style}{circle} \AOinline[style=circle]{pair}
 \item\key{style}{round} \AOinline[style=round]{pair}
 \item\key{style}{fancy} \AOinline[style=fancy]{pair}
\end{itemize}

Let's take the \ac{MO} diagram of \ch{H2} to illustrate the different styles:
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[style=plain]% default
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[style=square]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[style=circle]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[style=round]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[style=fancy]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}

\subsubsection{Option \key{distance}}\label{option:distance}
Depending on labels and captions the \SI{4}{\centi\metre} by which the right and
left atom are separated can be too small. With \key{distance}{<dim>} the length
can be adjusted. This will change the position of the right atom to \code{1cm + <dim>}
and the position of the molecule is changed to \code{0.5*(1cm + <dim>)}, also see
page~\pageref{xshift} and section~\ref{orbital-positionen}.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[distance=6cm]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}

\subsubsection{Option \key{AO-width}}\label{option:AO-width}
The length \key{AO-width} sets the length of the horizontal line in a orbital
displayed with the \code{plain} style. It's default value is \SI{10}{pt}.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[AO-width=15pt]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[style=fancy,AO-width=15pt]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
By changing the value of \key{AO-width} the positions of the p and the \textpi\
orbitals also change, see section~\ref{orbital-positionen}.

\subsubsection{Optionen \key{el-sep}, \key{up-el-pos} und \key{down-el-pos}}\label{option:electrons}
These three options change the horizontal positions of the arrows representing
the electrons in an \ac{AO}/\ac{MO}. The option \key{el-sep}{<num>} needs a value
between \code{0} and \code{1}. \code{0} means \emph{no} distance between the arrows
and \code{1} \emph{full} distance (with respect to the length \key{AO-width}, see
section~\ref{option:AO-width}).
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[el-sep=.2]% default
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[el-sep=0]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[el-sep=1]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
The options \key{up-el-pos}{<num>} and \key{down-el-pos}{<num>} can be used
alternatively to place the spin-up and spin-down electron, respectively. Again
they need values between \code{0} and \code{1}. This time \code{0} means \emph{on
the left} and \code{1} means \emph{on the right}.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[up-el-pos=.4,down-el-pos=.6]% default
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[up-el-pos=.333,down-el-pos=.667]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[up-el-pos=.7,down-el-pos=.3]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}

\subsubsection{Option \key{lines}}\label{option:lines}
The option \key{lines} can be used to modify the \TikZ style of the connecting
lines:
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[lines={gray,thin}]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}

\subsubsection{Option \key{names}}\label{option:names}
If you use the option \key{names} the atoms and the molecule get captions provided
you have used the optional \code{<name>} argument of \cmd{atom} and/or \cmd{molecule}.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[names]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}

\subsubsection{Optionens \key{names-style} and \key{names-style-add}}\label{option:names_style}
These options enable to customize the style of the captions of the atoms and of
the molecule. By default this setting is used: \key{names-style}{\{anchor=base\}}%
\footnote{Please see ``\TikZ and PGF -- Manual for Version 2.10'' p.\,183 section
16.4.4 (pgfmanual.pdf) for the meaning}.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[names,names-style={draw=blue}]
  \atom[p]{left} { 1s = {;up} }
  \atom[b]{right}{ 1s = {;up} }
  \molecule[\ch{X2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
With this the default setting is overwritten. As you can see it destroys the
vertical alignment of the nodes. In order to avoid that you can for example
specify \code{text height} and \code{text depth} yourself \ldots
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[names,names-style={text height=1.5ex, text depth=.25ex, draw=blue}]
  \atom[p]{left} { 1s = {;up} }
  \atom[b]{right}{ 1s = {;up} }
  \molecule[\ch{X2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\ldots, add the \code{anchor} again \ldots
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[names,names-style={anchor=base, draw=blue}]
  \atom[p]{left} { 1s = {;up} }
  \atom[b]{right}{ 1s = {;up} }
  \molecule[\ch{X2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\ldots\ or use the option \key{names-style-add}. It doesn't overwrite the
current setting but appends the new declaration:
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[names,names-style-add={draw=blue}]
  \atom[p]{left} { 1s = {;up} }
  \atom[b]{right}{ 1s = {;up} }
  \molecule[\ch{X2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 % use package `chemmacros'
 \MOsetup{names,names-style={text height=2.5ex,text depth=.5ex,draw=blue!80,rounded corners}}
 \begin{MOdiagram}
  \atom[p]{left} { 1s = {;up} }
  \atom[b]{right}{ 1s = {;up} }
  \molecule[\ch{X2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
 \begin{MOdiagram}[names-style-add={fill=blue!20}]
  \atom[p]{left} { 1s = {;up} }
  \atom[b]{right}{ 1s = {;up} }
  \molecule[\ch{X2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}

\subsubsection{Option \key{labels}}\label{option:labels}
If you use the option \key{labels} predefined labels are written below the
orbitals. These labels can be changed, see section~\ref{sec:key:label}.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[labels]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}

\subsubsection{Option \key{labels-fs}}\label{option:labels-fs}
Labels are set with the font size \cmd{small}. If you want to change that you
can use the option \key{labels-fs}.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[labels,labels-fs=\footnotesize]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}
This also allows you to change the font style or font shape of the labels.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[labels,labels-fs=\sffamily\footnotesize]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}

\subsubsection{Option \key{labels-style}}\label{option:labels-style}
The option \key{labels-style} changes the \TikZ style of the nodes within which
the labels are written.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[labels,labels-style={blue,yshift=4pt}]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{ 1sMO = {.75;pair} }
 \end{MOdiagram}
\end{beispiel}

\subsection{\cmd{atom} and \cmd{molecule} Specific Customizations}
\subsubsection{The \key{label} Key}\label{sec:key:label}
If you don't want to use the predefined labels, change single labels or use only
one or two labels, you can use the key \key{label}. This key is used in the
\cmd{atom} and  \cmd{molecule} commands in the \code{<AO-spec>} or
\code{<MO-spec>} argument, respectively. The key awaits a comma separated
key/value list. The names mentioned in section~\ref{Namensgebung} are used as
keys to specify the \ac{AO} that you want to label.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[labels-fs=\footnotesize]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{
    1sMO  = {.75;pair},
    label = { 1sigma = {bonding MO} }
  }
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 \begin{MOdiagram}[style=square,distance=6cm]
  \atom{left} { 1s = {;up} }
  \atom{right}{ 1s = {;up} }
  \molecule{
    1sMO  = {.75;pair} ,
    label = {
      1sigma  = \textsigma,
      1sigma* = \textsigma$^*$
    }
  }
  \node[right] at (1sigma.-45) {bonding};
  \node[right] at (1sigma*.45) {anti-bonding};
 \end{MOdiagram}
\end{beispiel}

If the key is used together with the \key{labels} option (page~\pageref{option:labels})
 single labels are overwritten:
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[labels]
  \atom[H]{left} { 1s = {;up} }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{
    1sMO  = {.75;pair},
    label = { 1sigma = \textcolor{red}{??} }
  }
 \end{MOdiagram}
\end{beispiel}

\subsubsection{The \key{color} Key}\label{sec:key:color}
Analogous to the \key{label} key the \key{color} key can be used to display
coloured electrons:
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}[labels-fs=\footnotesize]
  \atom[H]{left}{
    1s, color = { 1sleft = blue }
  }
  \atom[H]{right}{
    1s, color = { 1sright = red }
  }
  \molecule[\ch{H2}]{
    1sMO,
    label = { 1sigma = {bonding MO} },
    color = { 1sigma = green, 1sigma* = cyan }
  }
 \end{MOdiagram}
\end{beispiel}

\subsubsection{The \key{up-el-pos} and \key{down-el-pos} keys}\label{sec:key:electrons}
The keys \key{up-el-pos} and \key{down-el-pos} allow it to shift the arrows
representing the electrons in a single \ac{AO} or \ac{MO} individually. You need
to use values between \code{0} and \code{1}, also see section~\ref{option:electrons}.
\begin{beispiel}
 % use package `chemmacros'
 \begin{MOdiagram}
  \atom[H]{left}{
    1s        = {;up},
    up-el-pos = { 1sleft=.5 }
  }
  \atom[H]{right}{ 1s = {;up} }
  \molecule[\ch{H2}]{
    1sMO        = {.75;pair} ,
    up-el-pos   = { 1sigma=.15 } ,
    down-el-pos = { 1sigma=.85 }
  }
 \end{MOdiagram}
\end{beispiel}

\subsection{\cmd{AO} Specific Customizations}\label{ssec:AO_anpassen}
These keys enable to customize orbitals created with \cmd{AO}.

\subsubsection{The \key{label} Key}\label{key:AO_label}
The key \key{label[x/y/z]} allows you to put a label to the \ac{AO}/\ac{MO}. If
you use the type \code{p} you can specify the orbital you want to label in square
brackets:
\begin{beispiel}
 \begin{MOdiagram}[style=square]
  \AO{s}[label={s orbital}]{0}
  \AO{p}[label[y]=py,label[z]=pz]{1.5}
 \end{MOdiagram}
\end{beispiel}

\subsubsection{The \key{color} Key}\label{key:AO_color}
Analogous to the \key{label} key there is the key \key{color[x/y/z]} which enables
you to choose a color for the electrons. If you use the type \code{p} you can
specify the orbital in square brackets:
\begin{beispiel}
 \begin{MOdiagram}[style=square]
  \AO{s}[color=red]{0}
  \AO{p}[color[y]=green,color[z]=cyan]{1.5}
 \end{MOdiagram}
\end{beispiel}

\subsubsection{The \key{up-el-pos} and \key{down-el-pos} Keys}\label{key:AO_electrons}
Then there are the keys \key{up-el-pos[x/y/z]} and \key{down-el-pos[x/y/z]} with
which the electrons can be shifted horizontally. You can use values between \code{0}
and \code{1}, also see section~\ref{option:electrons}. If you use the type \code{p}
you can specify the orbital in square brackets:
\begin{beispiel}
 \begin{MOdiagram}[style=square]
  \AO{s}[up-el-pos=.15]{0}
  \AO{p}[up-el-pos[y]=.15,down-el-pos[z]=.15]{1.5}
 \end{MOdiagram}
\end{beispiel}

\subsection{Energy Axis}
Last but not least one might want to add an energy axis to the diagram. For this
there is the command \cmd{EnergyAxis}.
\begin{beschreibung}
 \Befehl{EnergyAxis}[<key = val>] \\
 \oa{<key = val>} key/value pairs to modify the axis.
\end{beschreibung}
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left} { 1s = {;up} }
  \atom{right}{ 1s = {;up} }
  \molecule{ 1sMO = {.75;pair} }
  \EnergyAxis
 \end{MOdiagram}
\end{beispiel}
For the time being there are two keys to modify the axis.
\begin{beschreibung}
  \option{title}{<title>} axis label (default: \code{energy}).
  \option{head}{<tikz-arrow-head>} arrow head; you can use the arrow heads
    specified in the \TikZ library \paket{arrows} (pgfmanual v2.10 pages 256ff.)
    (default: \code{>}).
\end{beschreibung}
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left} { 1s = {;up} }
  \atom{right}{ 1s = {;up} }
  \molecule{ 1sMO = {.75;pair} }
  \EnergyAxis[title]
 \end{MOdiagram}
\end{beispiel}
\begin{beispiel}
 \begin{MOdiagram}
  \atom{left} { 1s = {;up} }
  \atom{right}{ 1s = {;up} }
  \molecule{ 1sMO = {.75;pair} }
  \EnergyAxis[title=E,head=stealth]
 \end{MOdiagram}
\end{beispiel}

\section{Examples}
The example from the beginning of section \ref{ssec:AO_MO_irgendwo}.
\begin{beispiel}
 % use packages `chemmacros' and `chemfig' 
 \begin{MOdiagram}[names]
  \atom[\lewis{0.,F}\hspace*{5mm}\lewis{4.,F}]{left}{1s=.2;up,up-el-pos={1sleft=.5}}
  \atom[Xe]{right}{1s=1.25;pair}
  \molecule[\ch{XeF2}]{1sMO={1/.25;pair}}
  \AO(1cm){s}{0;up}
  \AO(3cm){s}{0;pair}
  \connect{ AO1 & AO2 }
  \node[right,xshift=4mm] at (1sigma) {\footnotesize bonding};
  \node[above] at (AO2.90) {\footnotesize non-bonding};
  \node[above] at (1sigma*.90) {\footnotesize anti-bonding};
 \end{MOdiagram}
\end{beispiel}

\begin{beispiel}[code and float]
 % use packages `chemmacros' (and `textgreek' loaded by `modiagram')
 \begin{figure}
  \centering
  \begin{MOdiagram}[style=square,labels,names,AO-width=8pt,labels-fs=\footnotesize]
   \atom[\ch{O_a}]{left}{
     1s, 2s, 2p = {;pair,up,up}
   }
   \atom[\ch{O_b}]{right}{
     1s, 2s, 2p = {;pair,up,up}
   }
   \molecule[\ch{O2}]{
     1sMO, 2sMO, 2pMO = {;pair,pair,pair,up,up},
     color = { 2piy*=red, 2piz*=red }
   }
   \EnergyAxis
  \end{MOdiagram}
  \caption{MO diagram of \ch{^3 "\textSigma-" O2}.}
 \end{figure}
\end{beispiel}

\begin{beispiel}[code and float]
 % use package `chemfig'
 \begin{figure}
  \centering\MOsetup{style = fancy, distance = 7cm, AO-width = 15pt, labels}
  \begin{MOdiagram}
   \atom[N]{left}{
     2p = {0;up,up,up}
   }
   \atom[O]{right}{
     2p = {2;pair,up,up}
   }
   \molecule[NO]{
     2pMO = {1.8,.4;pair,pair,pair,up},
     color = { 2piy*=red }
   }
   \EnergyAxis
  \end{MOdiagram}
  \caption{Part of the MO diagram of \protect\Lewis{4.,NO}.}
 \end{figure}
\end{beispiel}

\appendix
\printindex
\end{document}