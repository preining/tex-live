\documentclass[a4paper,12pt]{article}

\pagestyle{plain}
\usepackage{fullpage}
\usepackage{moreverb}
\usepackage{wrapfig}
\usepackage{multicol}
\usepackage{appendix}
\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{othelloboard}
\usepackage{hyperref}
%\usepackage{natbib}


%%% TITLE %%%
\newcommand{\fileversion}{v1.2}
\title{The \textsf{othelloboard} package%
\thanks{This document corresponds to \textsf{othelloboard}~\fileversion. Thanks to Brian Rose for permission to typeset material from his book for the example file: \texttt{example-rose-chps1-2.pdf}.}
}\author{Steven Hall \\ \texttt{stevenhall.uk@gmail.com}}
\date{18 August 2011}


%% DOCUMENT %%

\begin{document}
\pagestyle{plain}

\maketitle
\tableofcontents

%------
\section{Drawing a simple board diagram}
\begin{center}
\begin{othelloboard}{1}
\gridrefs
\dotmarkings
\othelloarrayfirstrow		{0}{0}{0}{0}{0}{0}{0}{0}
\othelloarraysecondrow	{0}{0}{2}{0}{0}{0}{0}{0}
\othelloarraythirdrow		{0}{1}{2}{2}{2}{0}{0}{0}
\othelloarrayfourthrow	{0}{1}{2}{1}{2}{1}{0}{0}
\othelloarrayfifthrow		{0}{0}{2}{2}{1}{1}{0}{0}
\othelloarraysixthrow	{0}{0}{2}{1}{1}{1}{0}{0}
\othelloarrayseventhrow	{0}{0}{0}{0}{0}{0}{0}{0}
\othelloarrayeighthrow	{0}{0}{0}{0}{0}{0}{0}{0}
\end{othelloboard}
\end{center}

The board above was produced by the following list of commands:
	\begin{verbatimtab}
	\begin{othelloboard}{1}				
	\dotmarkings
	\othelloarrayfirstrow	{0}{0}{0}{0}{0}{0}{0}{0}
	\othelloarraysecondrow	{0}{0}{2}{0}{0}{0}{0}{0}
	\othelloarraythirdrow	{0}{1}{2}{2}{2}{0}{0}{0}
	\othelloarrayfourthrow	{0}{1}{2}{1}{2}{1}{0}{0}
	\othelloarrayfifthrow	{0}{0}{2}{2}{1}{1}{0}{0}
	\othelloarraysixthrow	{0}{0}{2}{1}{1}{1}{0}{0}
	\othelloarrayseventhrow	{0}{0}{0}{0}{0}{0}{0}{0}
	\othelloarrayeighthrow	{0}{0}{0}{0}{0}{0}{0}{0}
	\end{othelloboard}
\end{verbatimtab}

Try copying this block of code into your own document and typesetting it. Make sure you include \verb=\usepackage{othelloboard}= somewhere in your preamble.

Try replacing any of the \verb=0=s, \verb=1=s and \verb=2=s in the array with any number \verb=0=--\verb=4=. You should see that each number has the following effect (\verb=0= has no effect):

\label{disckey}~\begin{tabular}{l c l l c l}
\verb=1= & draws a white disc: & \begin{picture}(20,20) \put(10,5){\circle{20}} \end{picture} & \hspace{24pt} \verb=3= & draws a white diamond: & \begin{picture}(20,20) \put(10,5){\whitediamond} \end{picture} \\
\verb=2= & draws a black disc: & \begin{picture}(20,20) \put(10,5){\circle*{20}} \end{picture} & \hspace{24pt} \verb=4= & draws a black diamond: & \begin{picture}(20,20) \put(10,5){\blackdiamond} \end{picture} \\
\end{tabular}\\

You should also see that where you choose to put a number in the grid determines where on the Othello board it appears. The \textsf{othelloboard} package has been designed to make creating a board diagram as intuitive and simple as possible, so the position of numbers in the array corresponds to exactly the squares you would expect in the diagram. Changing the `\verb=2=' to a `\verb=1=' e.g.\ in the second row of numbers in the code replaces the black disc on c2 with a white one.

\subsection{The \texttt{othelloboard} environment}
\subsubsection{The basic commands}

Look again at the opening and closing lines of code for the diagram in the previous section.

\begin{description}
\item{\verb=\begin{othelloboard}{1}=} \verb=%		= Mandatory argument for board size.
\item \ldots
\item{\verb=\end{othelloboard}=}
\end{description}

The board diagram was created by entering a series of commands inside of an \verb=othelloboard= environment. The mandatory argument at the end of the \verb=\begin= environment declaration specifies the size of board. Any size at all is possible without loss of detail in the diagram. The board diagram above was set with default value of 1. A value between 0 and 1 produces a smaller board, while a value greater than 1 produces a larger board.

\begin{description}
\item{\verb=\dotmarkings=} produces the tiny marker dots on the inside corners of the X-squares. The command can be deleted or commented out if you don't want the dots.
\end{description}

Next, the array of discs on the board is drawn by eight separate commands, one for each row. E.g.\ row three is drawn by this command:
\begin{description}
\item{\verb=\othelloarraythirdrow{0}{1}{2}{2}{2}{0}{0}{0}=} The command takes eight arguments, one for each square on the third row, a3--h3. The possible values of each argument are \verb=0=, \verb=1=, \verb=2=, \verb=3=, \verb=4= (with effects described at the top of this page). 
\end{description}

If you don't wish to draw any discs at all for a particular row, it is not necessary to include the command for that row. We could easily, e.g., have omitted the first and eighth row commands in our diagram in the previous section (rather than give the command with a just series of \verb=0=s as arguments).

Here is a board drawn at size 0.75 without \verb=\dotmarkings=, and with the \verb=\othelloarray=\ldots\verb=row= commands for the first and eighth rows omitted:\\
\begin{tabular}{ll}
\begin{othelloboard}{0.75}
\othelloarrayfirstrow	{0}{0}{0}{0}{0}{0}{0}{0}
\othelloarraysecondrow	{0}{0}{2}{0}{0}{0}{0}{0}
\othelloarraythirdrow	{0}{1}{2}{2}{2}{0}{0}{0}
\othelloarrayfourthrow	{0}{1}{2}{1}{2}{1}{0}{0}
\othelloarrayfifthrow	{0}{0}{2}{2}{4}{1}{0}{0}
\othelloarraysixthrow	{0}{0}{2}{4}{4}{1}{0}{0}
\othelloarrayseventhrow	{0}{0}{0}{0}{2}{0}{0}{0}
\othelloarrayeighthrow	{0}{0}{0}{0}{0}{0}{0}{0}
\end{othelloboard}
 & 
\begin{minipage}[b]{320pt}
\begin{verbatimtab}
\begin{othelloboard}{0.75}
\othelloarraysecondrow	{0}{0}{2}{0}{0}{0}{0}{0}
\othelloarraythirdrow	{0}{1}{2}{2}{2}{0}{0}{0}
\othelloarrayfourthrow	{0}{1}{2}{1}{2}{1}{0}{0}
\othelloarrayfifthrow	{0}{0}{2}{2}{4}{1}{0}{0}
\othelloarraysixthrow	{0}{0}{2}{4}{4}{1}{0}{0}
\othelloarrayseventhrow	{0}{0}{0}{0}{2}{0}{0}{0}
\end{othelloboard}
\end{verbatimtab}
\ \\
\end{minipage}\\
\end{tabular}
\ \\
Note the use of black diamonds to show the discs just flipped by the black disc placed at e7. This is the convention followed, e.g., in Brian Rose's book (Rose 2005).

\paragraph{Useful tip}\label{tabtips}
You may find that it helps see where you are putting discs if you align all of the rows of numbers in the array. You can do this (as with the examples given in this document) by putting tabs between the command name and its first argument. Don't use spaces though, as unlike tabs (which aren't read as gaps when the file compiles), spaces will break the commands and cause errors.

\verb=\othelloarrayfirstrow= \begin{picture}(30,30)\put(20,8){\vector(-1,-2){5}}\put(0,12){\textit{tabs not spaces}}\end{picture}\verb=		{0}{1}{1}{2}{1}{0}{0}{0}=\\

\subsection{The \texttt{othelloboardnorefs} environment}
\label{norefs}
If you don't want grid reference labels along the top and left edges, you can use the \verb=othelloboardnorefs= environment instead of the \verb=othelloboard= environment. The commands that work inside of the environment are all the same as before, but the outside boundaries of the diagram will be the edges of the 8$\times$8 grid. Our opening example, changing only the environment, gives this output:

\begin{minipage}[t]{196pt}
\vspace{0pt}
\begin{othelloboardnorefs}{1}
\dotmarkings
\othelloarrayfirstrow	{0}{0}{0}{0}{0}{0}{0}{0}
\othelloarraysecondrow	{0}{0}{2}{0}{0}{0}{0}{0}
\othelloarraythirdrow	{0}{1}{2}{2}{2}{0}{0}{0}
\othelloarrayfourthrow	{0}{1}{2}{1}{2}{1}{0}{0}
\othelloarrayfifthrow	{0}{0}{2}{2}{1}{1}{0}{0}
\othelloarraysixthrow	{0}{0}{2}{1}{1}{1}{0}{0}
\othelloarrayseventhrow	{0}{0}{0}{0}{0}{0}{0}{0}
\othelloarrayeighthrow	{0}{0}{0}{0}{0}{0}{0}{0}
\end{othelloboardnorefs}
\end{minipage}
\begin{minipage}[t]{360pt}
\vspace{12pt}
\begin{small}
\begin{verbatimtab}
\begin{othelloboardnorefs}{1}
\dotmarkings
\othelloarrayfirstrow	{0}{0}{0}{0}{0}{0}{0}{0}
\othelloarraysecondrow	{0}{0}{2}{0}{0}{0}{0}{0}
\othelloarraythirdrow	{0}{1}{2}{2}{2}{0}{0}{0}
\othelloarrayfourthrow	{0}{1}{2}{1}{2}{1}{0}{0}
\othelloarrayfifthrow	{0}{0}{2}{2}{1}{1}{0}{0}
\othelloarraysixthrow	{0}{0}{2}{1}{1}{1}{0}{0}
\othelloarrayseventhrow	{0}{0}{0}{0}{0}{0}{0}{0}
\othelloarrayeighthrow	{0}{0}{0}{0}{0}{0}{0}{0}
\end{othelloboardnorefs}
\end{verbatimtab}
\end{small}
\end{minipage}

A standard-size \verb=othelloboard= diagram produces a 208pt$\times$208pt box containing a 192pt$\times$192pt board with grid labels in 16pt margins at the top and left side. A standard-size \verb=othelloboardnorefs= diagram produces a 192pt$\times$192pt box containing just the board (see Appendix \ref{appendix:layout}).

\section{Annotations}
You may want to add some annotations to some of the squares, perhaps for move numbers or to indicate possible moves at a particular point in a game.\\
\begin{minipage}[t]{212pt}
\vspace{0pt}
\begin{othelloboard}{1}
\dotmarkings
\othelloarrayfirstrow		{0}{0}{0}{0}{0}{0}{0}{0}
\othelloarraysecondrow	{0}{0}{2}{0}{0}{0}{0}{0}
\othelloarraythirdrow		{0}{1}{2}{2}{2}{0}{0}{0}
\othelloarrayfourthrow	{0}{1}{2}{1}{2}{1}{0}{0}
\othelloarrayfifthrow		{0}{0}{2}{2}{2}{1}{0}{0}
\othelloarraysixthrow	{0}{0}{2}{2}{2}{1}{0}{0}
\othelloarrayseventhrow	{0}{0}{0}{0}{2}{0}{0}{0}
\othelloarrayeighthrow	{0}{0}{0}{0}{0}{0}{0}{0}
%annotations
\annotationsfirstrow		{}	{}	{}	{}	{}	{}	{}	{}
\annotationssecondrow	{}	{}	{9}	{}	{}	{}	{}	{}
\annotationsthirdrow		{}	{8}	{5}	{4}	{11}	{}	{}	{}
\annotationsfourthrow	{}	{10}	{3}	{}	{}	{6}	{}	{}
\annotationsfifthrow		{}	{}	{7}	{}	{}	{1}	{}	{}
\annotationssixthrow		{}	{}	{13}	{2}	{12}	{14}	{}	{}
\annotationsseventhrow	{}	{}	{}	{}	{15}	{}	{}	{}
\annotationseighthrow	{}	{}	{}	{}	{}	{}	{}	{}
\end{othelloboard}
\end{minipage}
\begin{minipage}[t]{248pt}
\vspace{8pt}
\begin{footnotesize}
\begin{verbatimtab}
\begin{othelloboard}{1}
\dotmarkings
\othelloarraysecondrow	{0}{0}{2}{0}{0}{0}{0}{0}
\othelloarraythirdrow	{0}{1}{2}{2}{2}{0}{0}{0}
\othelloarrayfourthrow	{0}{1}{2}{1}{2}{1}{0}{0}
\othelloarrayfifthrow	{0}{0}{2}{2}{2}{1}{0}{0}
\othelloarraysixthrow	{0}{0}{2}{2}{2}{1}{0}{0}
\othelloarrayseventhrow	{0}{0}{0}{0}{2}{0}{0}{0}
%annotations
\annotationssecondrow	{} {} {9}{} {} {} {} {}
\annotationsthirdrow	{}{8}{5}{4}{11}{} {} {}
\annotationsfourthrow	{}{10}{3}{} {} {6}{} {}
\annotationsfifthrow	{} {} {7}{} {} {1}{} {}
\annotationssixthrow	{}{}{13}{2}{12}{14}{}{}
\annotationsseventhrow	{} {} {} {}{15}{} {} {}
\end{othelloboard}
\end{verbatimtab}
\end{footnotesize}
\end{minipage}
\ \\
\noindent \begin{minipage}[t]{212pt}
\vspace{0pt}
\begin{othelloboard}{1}
\dotmarkings
\othelloarrayfirstrow		{0}{0}{0}{0}{0}{0}{0}{0}
\othelloarraysecondrow	{0}{0}{2}{0}{0}{0}{0}{0}
\othelloarraythirdrow		{0}{1}{2}{2}{2}{0}{0}{0}
\othelloarrayfourthrow	{0}{1}{2}{1}{2}{1}{0}{0}
\othelloarrayfifthrow		{0}{0}{2}{2}{1}{1}{0}{0}
\othelloarraysixthrow	{0}{0}{2}{1}{1}{1}{0}{0}
\othelloarrayseventhrow	{0}{0}{0}{0}{0}{0}{0}{0}
\othelloarrayeighthrow	{0}{0}{0}{0}{0}{0}{0}{0}
%annotations
\annotationssecondrow	{}	{}	{}	{}	{}	{}	{}	{}
\annotationsthirdrow		{}	{}	{}	{}	{}	{}	{}	{}
\annotationsfourthrow	{}	{}	{}	{}	{}	{}	{}	{}
\annotationsfifthrow		{c}	{}	{}	{}	{}	{}	{}	{}
\annotationssixthrow		{}	{}	{}	{}	{}	{}	{}	{}
\annotationsseventhrow	{}	{}	{}	{b}	{a?}	{}	{}	{}
\end{othelloboard}
\end{minipage}
\begin{minipage}[t]{248pt}
\vspace{8pt}
\begin{footnotesize}
\begin{verbatimtab}
\begin{othelloboard}{1}
\dotmarkings
\othelloarraysecondrow	{0}{0}{2}{0}{0}{0}{0}{0}
\othelloarraythirdrow	{0}{1}{2}{2}{2}{0}{0}{0}
\othelloarrayfourthrow	{0}{1}{2}{1}{2}{1}{0}{0}
\othelloarrayfifthrow	{0}{0}{2}{2}{1}{1}{0}{0}
\othelloarraysixthrow	{0}{0}{2}{1}{1}{1}{0}{0}
%annotations
\annotationssecondrow	{} {} {} {} {} {} {} {}
\annotationsthirdrow	{} {} {} {} {} {} {} {}
\annotationsfourthrow	{} {} {} {} {} {} {} {}
\annotationsfifthrow	{c}{} {} {} {} {} {} {}
\annotationssixthrow	{} {} {} {} {} {} {} {}
\annotationsseventhrow	{} {} {}{b}{a?}{} {} {}
\end{othelloboard}
\end{verbatimtab}
\end{footnotesize}
\end{minipage}
\ \\

A row of annotations is added with an \verb=\annotations=\ldots\ command. Again, there is one for each row, each taking eight arguments. Here is a possible instance of the command for adding annotations to the fifth row:

\begin{description}
\item{\verb=\annotationsfifthrow{}{43}{17}{}{}{a}{b?}{??}=} The command has an argument place for each of the squares a5--h5. The command simply prints the value of each argument, centred at the corresponding square. Any string is possible as an argument (including use of symbols in \verb=mathmode=), though you will probably only want to use one- or two-digit numerals or single letters (as in both of the diagrams just given). To leave a square unannotated, put nothing -- \verb={}= -- between the braces; putting zero -- \verb={0}= -- prints a \verb=0=.
\end{description}

To annotate a diagram just add the corresponding annotation command for each row you want to annotate. Make sure that the \verb=\annotations=\ldots\ commands appear \emph{below} the \verb=\othelloarray=\ldots\ commands.

\paragraph{Text colour}
You do not have to specify the colour of annotation text, since the \textsf{othelloboard} package is smart enough to work out the colour of the disc (or diamond) underneath the text on the same square. Annotations on a black shape are automatically set in white; other annotations are set in the default text colour (black), including those on empty squares.

\subsection{Placing an individual annotation at a specified square}

It is also possible to place an individual annotation at a specific square by giving the square name. The command for this is:

\verb=\posannotation{<=\emph{squarename}\verb=>}{<=\emph{annotationstring}\verb=>}=

So \verb=\posannotation{F2}{47}= puts the numeral `47' at square f2. 


\subsection{Transcripts}
Using \verb=\annotations=\ldots\ together with \verb=\othelloarray=\ldots\ commands, it is thus possible to set any transcript in the two standard styles: \\

\noindent \begin{picture}(216,220)(12,-16)
\begin{othelloboard}{1}
\othelloarrayfirstrow		{1}{2}{1}{1}{2}{1}{2}{1}
\othelloarraysecondrow	{2}{1}{2}{2}{2}{1}{2}{2}
\othelloarraythirdrow		{2}{2}{2}{1}{2}{1}{1}{1}
\othelloarrayfourthrow	{1}{1}{2}{1}{2}{1}{2}{2}
\othelloarrayfifthrow		{2}{1}{1}{2}{1}{2}{2}{1}
\othelloarraysixthrow	{1}{2}{2}{1}{2}{2}{1}{1}
\othelloarrayseventhrow	{2}{1}{1}{2}{1}{1}{2}{2}
\othelloarrayeighthrow	{2}{1}{2}{2}{1}{1}{2}{1}
\annotationsfirstrow		{54}	{51}	{34}	{30}	{31}	{32}	{41}	{42}
\annotationssecondrow	{55}	{50}	{43}	{33}	{29}	{28}	{39}	{58}
\annotationsthirdrow		{23}	{27}	{3}	{4}	{25}	{8}	{40}	{59}
\annotationsfourthrow	{24}	{22}	{5}	{}	{}	{6}	{37}	{60}
\annotationsfifthrow		{47}	{20}	{14}	{}	{}	{1}	{35}	{38}
\annotationssixthrow		{26}	{21}	{15}	{2}	{9}	{7}	{12}	{36}
\annotationsseventhrow	{49}	{56}	{16}	{11}	{10}	{18}	{45}	{53}
\annotationseighthrow	{57}	{46}	{17}	{13}	{44}	{52}	{19}	{48}
%\put(12,-14){Figure 3.2}
\end{othelloboard}
\end{picture}
\begin{minipage}[b]{320pt}
\begin{footnotesize}
\begin{verbatimtab}
\begin{othelloboard}{1}
\othelloarrayfirstrow	{1}{2}{1}{1}{2}{1}{2}{1}
\othelloarraysecondrow	{2}{1}{2}{2}{2}{1}{2}{2}
\othelloarraythirdrow	{2}{2}{2}{1}{2}{1}{1}{1}
\othelloarrayfourthrow	{1}{1}{2}{1}{2}{1}{2}{2}
\othelloarrayfifthrow	{2}{1}{1}{2}{1}{2}{2}{1}
\othelloarraysixthrow	{1}{2}{2}{1}{2}{2}{1}{1}
\othelloarrayseventhrow	{2}{1}{1}{2}{1}{1}{2}{2}
\othelloarrayeighthrow	{2}{1}{2}{2}{1}{1}{2}{1}
%annotations
\annotationsfirstrow	{54}{51}{34}{30}{31}{32}{41}{42}
\annotationssecondrow	{55}{50}{43}{33}{29}{28}{39}{58}
\annotationsthirdrow	{23}{27}{3} {4} {25} {8}{40}{59}
\annotationsfourthrow	{24}{22}{5}  {}  {}  {6}{37}{60}
\annotationsfifthrow	{47}{20}{14} {}  {}  {1}{35}{38}
\annotationssixthrow	{26}{21}{15} {2} {9} {7}{12}{36}
\annotationsseventhrow	{49}{56}{16}{11}{10}{18}{45}{53}
\annotationseighthrow	{57}{46}{17}{13}{44}{52}{19}{48} 
\end{othelloboard}
\end{verbatimtab}
\end{footnotesize}
\end{minipage}

\noindent \begin{picture}(216,220)(12,-16)
\begin{othelloboard}{1}
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow		{0}{0}{0}{2}{1}{0}{0}{0}
\annotationsfirstrow		{54}	{51}	{34}	{30}	{31}	{32}	{41}	{42}
\annotationssecondrow	{55}	{50}	{43}	{33}	{29}	{28}	{39}	{58}
\annotationsthirdrow		{23}	{27}	{3}	{4}	{25}	{8}	{40}	{59}
\annotationsfourthrow	{24}	{22}	{5}	{}	{}	{6}	{37}	{60}
\annotationsfifthrow		{47}	{20}	{14}	{}	{}	{1}	{35}	{38}
\annotationssixthrow		{26}	{21}	{15}	{2}	{9}	{7}	{12}	{36}
\annotationsseventhrow	{49}	{56}	{16}	{11}	{10}	{18}	{45}	{53}
\annotationseighthrow	{57}	{46}	{17}	{13}	{44}	{52}	{19}	{48}
%\put(12,-14){Figure 3.3 \label{Figure 3.3}}
\end{othelloboard}
\end{picture}
\begin{minipage}[b]{248pt}
\begin{footnotesize}
\begin{verbatimtab}
\begin{othelloboard}{1}
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow	{0}{0}{0}{2}{1}{0}{0}{0}
%annotations
\annotationsfirstrow	{54}{51}{34}{30}{31}{32}{41}{42}
\annotationssecondrow	{55}{50}{43}{33}{29}{28}{39}{58}
\annotationsthirdrow	{23}{27}{3} {4} {25} {8}{40}{59}
\annotationsfourthrow	{24}{22}{5}  {}  {}  {6}{37}{60}
\annotationsfifthrow	{47}{20}{14} {}  {}  {1}{35}{38}
\annotationssixthrow	{26}{21}{15} {2} {9} {7}{12}{36}
\annotationsseventhrow	{49}{56}{16}{11}{10}{18}{45}{53}
\annotationseighthrow	{57}{46}{17}{13}{44}{52}{19}{48} 
\end{othelloboard}
\end{verbatimtab}
\end{footnotesize}
\ \\
\ \\
\end{minipage}

\section{Automated methods for board creation}

In addition to the manual row-by-row methods described in previous sections for creating board diagrams, \textsf{othelloboard} also supports the automated creation of board diagrams and transcripts from long strings of text in a standard format used by Othello software such as WZebra and Cassio.

\subsection{Drawing a transcript from a list of moves}

It is common practice to give a game transcript as a single long alphanumeric string. Here's an example exported from Cassio:

C4C3D3C5B4D2D6C6E6F4B3B5F3F5G3G4A5A6C2A4A3B6E1C1A7F2C7C8D7F6G6F7
E2D1E8F8D8E7G5H3F1H6E3G1B2B1H4A1A2A8H5B7B8G2H7H8G8G7H2H1

To draw the board for this transcript you can use \verb=\drawtranscript{<=\emph{longstring}\verb=>}=.

\begin{center}%
\begin{minipage}[c]{160pt}%
\vspace{0pt}
\begin{othelloboard}{.8}
\othelloarrayfourthrow{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow{0}{0}{0}{2}{1}{0}{0}{0}
\drawtranscript{C4C3D3C5B4D2D6C6E6F4B3B5F3F5G3G4A5A6C2A4A3B6E1C1A7F2C7C8D7F6G6F7E2D1E8F8D8E7G5H3F1H6E3G1B2B1H4A1A2A8H5B7B8G2H7H8G8G7H2H1}
\end{othelloboard}
\end{minipage}
\hfill
\begin{minipage}[c]{264pt}
\begin{footnotesize}
\begin{verbatimtab}
\begin{othelloboard}{.8}
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow	{0}{0}{0}{2}{1}{0}{0}{0}
\drawtranscript{C4C3D3C5B4D2D6C6E6...B8G2H7H8G8G7H2H1}
\end{othelloboard}
\end{verbatimtab}
\end{footnotesize}
\end{minipage}
\end{center}

\subsection{Drawing a board diagram from a single long string}
Several programs use a standard long string of dashes, Xs and Os to import and export board diagrams. \textsf{othelloboard} also supports such strings. Here is a typical string taken from Cassio:

\begin{small}
\verb=XO---XXX-OOO-OOO-OOOOOO---OOXO---OOXOOO-OOXOOOOOXXXXX---XXXXXX--=
\end{small}

An `X' represents a black disc, an `O' a white disc, and a dash (`-') an empty square. The string gives the state of every square reading from the top row down, left to right. 

To create a board diagram from this string, we simply feed it as an argument into the \verb=\drawboardfromstring{<=\emph{longstring}\verb=>}= command:
\begin{center}
\begin{minipage}[c]{144pt}
\vspace{0pt}
\begin{othelloboard}{.8}
\drawboardfromstring{XO---XXX-OOO-OOO-OOOOOO---OOXO---OOXOOO-OOXOOOOOXXXXX---XXXXXX--}
\end{othelloboard}
\end{minipage}
\hfill
\begin{minipage}[c]{272pt}
\begin{footnotesize}
\begin{verbatim}
\begin{othelloboard}{.8}
\drawboardfromstring{XO---XXX-OOO-OOO-...--XXXXXX--}
\end{othelloboard}
\end{verbatim}
\end{footnotesize}
\end{minipage}
\end{center}

The characters \verb=3= and \verb=4= can also be included in the string for white and black diamonds (respectively).

\section{Tweaks}
Feel free to tweak the code in the \verb=othelloboard.sty= file if the diagrams aren't exactly how you like them. I've commented on it quite thoroughly so that it should be easy to see which bit to tweak in order, e.g., to change the font size of the grid references, or the size of the discs relative to the grid.\\

If you know that you will want marker dots on every diagram you make, you can simply add the following to your preamble (though after you call for the package):\\

\noindent \verb=let\Oldothellogrid\othellogrid= \\
\verb=\renewcommand{\othellogrid}{\dotmarkings\Oldothellogrid}= \\
  
\noindent Now the \verb=\dotmarkings= command will be automatically executed each time you enter into an \verb=othelloboard= (or \verb=othelloboardnorefs=) environment.\\
\ \\

\noindent If you have any suggestions for improving or adding to this package, I'd be very keen to hear them.  Or if you need some help tweaking the package for a particular application I'd be happy to hear from you about that too. Email me on \verb=stevenhall.uk@gmail.com=.


\clearpage
\appendix
\appendixpage
\section{Layout of diagrams}
\label{appendix:layout}
\noindent The board diagrams drawn with the \textsf{othelloboard} package are contained inside a \verb=picture= environment box. They behave like any diagrams drawn inside of the \verb=picture= environment and can be embedded within further boxes for accurate positioning around the page. They can also be enclosed within a \verb=figure= environment or any other float and treated like any diagram you might insert into a document, e.g.\ with labels and captions. The function of this package is just to provide diagrams neatly embedded in a box; the positioning and use of this box is up to you. However, here is some more detailed information about how the box is created and a few tips you may find useful.\\

\noindent The grid of a standard-sized diagram is contained in a 192pt$\times$192pt box that itself is positioned in the bottom right corner of a slightly larger 208pt$\times$208pt box (see dotted line), creating 16pt margins on the left and at the top to allow for alphanumeric labels with a little room spare for padding (with scaled diagrams the proportions are all kept the same, including the margins). It is the larger containing box that you control when you create a standard Othello diagram. The diagram immediately below is set with no vertical space separating it from the text above and below.   \\
\noindent\begin{picture}(240,208)
\vspace{0pt}
\dashbox{2}(208,208)[br]{\begin{othelloboard}{1}
\gridrefs
\dotmarkings
\end{othelloboard}}
\put(24,200){\vector(-1,0){24} \scriptsize{16pt margin}}
\put(8,110){\vector(0,1){98}}
\put(4,102){\scriptsize{208pt}}
\put(8,98){\vector(0,-1){98}}
\put(28,102){\vector(0,1){90}}
\put(24,94){\scriptsize{192pt}}
\put(28,90){\vector(0,-1){90}}
\end{picture}

\noindent It is conceivable that you might want to create an Othello board diagram with no labels and no margins at the top and left side. In this case, you can use the \verb=othelloboardnorefs= environment (see \ref{norefs}), which works just like the \verb=othelloboard= environment except that it produces board diagrams without labels. This environment leaves out the 208pt$\times$208pt containing box that allows margins for the labels, and just draws the 192pt$\times$192pt grid inside of a 192pt$\times$192pt box. Here is an example, again with the diagram placed on the very next line of text with no vertical space, showing the absence of padding around the board.\\
\clearpage
\noindent (Paragraph of text immediately above the diagram code.)\\
\noindent
\begin{othelloboardnorefs}{1}
\dotmarkings
\end{othelloboardnorefs}

\noindent The last two diagrams are the result of putting an \verb=othelloboard= or \verb=othelloboardnorefs= environment between two paragraphs with no extra thought about spacing and layout. It may be sufficient for your needs simply to add line breaks or vertical space before and after a diagram. Alternatively, here are a couple of examples using other methods with nicer results.\\

\noindent Here's a captioned diagram enclosed and centred within a \verb=figure= environment. The code used for this and the following numbered figures is given at the end of this section.

\begin{figure}[h]
\begin{center}
\begin{othelloboard}{.8}
\dotmarkings
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow		{0}{0}{0}{2}{1}{0}{0}{0}
\end{othelloboard}
\caption{The opening position}
\end{center}
\end{figure}

\noindent Here's an example of a diagram set in a block of text using the \verb=wrapfig= package. \\

\noindent Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\begin{wrapfigure}{r}{152pt}
\begin{othelloboard}{0.75}
\dotmarkings
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow		{0}{0}{0}{2}{1}{0}{0}{0}
\end{othelloboard}
\caption{An example using the \texttt{wrapfig} package}
\end{wrapfigure}
blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah  Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah  Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah  Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah  Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah  Blah blah blah blah blah blah blah Blah blah blah blah blah blah blah\\

\noindent Here are a couple of diagrams side-by-side in a \verb=figure= environment.\\

\begin{figure}[ht]
\begin{minipage}[b]{0.5\linewidth}
\centering
\begin{othelloboard}{1}
\dotmarkings
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow		{0}{0}{0}{2}{1}{0}{0}{0}
\end{othelloboard}
\caption{left diagram}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[b]{0.5\linewidth}
\centering
\begin{othelloboard}{1}
\dotmarkings
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow		{0}{0}{0}{2}{1}{0}{0}{0}
\end{othelloboard}
\caption{right diagram}
\end{minipage}
\end{figure}

\noindent You might also consider using \verb=minipage=s to align diagrams, or text and diagrams, horizontally:

\begin{center}
\begin{minipage}[c]{192pt}
\textsf{
\begin{othelloboardnorefs}{1}
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow		{0}{0}{0}{2}{1}{0}{0}{0}
\annotationseighthrow{}{C}{A}{B}{B}{A}{C}{}
\end{othelloboardnorefs}}
\end{minipage}
\hfill
\begin{minipage}[c]{192pt}
Figure 5\\

The text over here is in the \verb=minipage= on the right. Both \verb=minipage=s are contained within a \verb=center= environment in this example.\\

The annotations are in a san-serif font this time.
\end{minipage}
\end{center}
\bigskip

For more examples of layouts using diagrams of different sizes, see the file: \texttt{example\--rose\--chps1\--2.pdf}.

\subsection{Code for the examples in this section}

\begin{scriptsize}
\begin{multicols}{2}
\noindent Figure 1:
\begin{verbatim}
\begin{figure}[h]
\begin{center}
\begin{othelloboard}{.8}
\dotmarkings
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow			{0}{0}{0}{2}{1}{0}{0}{0}
\end{othelloboard}
\caption{The opening position}
\end{center}
\end{figure}
\end{verbatim}

\noindent Figure 2:
\begin{verbatim}
id est laborum ...
\begin{wrapfigure}{r}{152pt}
\begin{othelloboard}{0.75}
\dotmarkings
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow			{0}{0}{0}{2}{1}{0}{0}{0}
\end{othelloboard}
\caption{An example using the \texttt{wrapfig} 
package}
\end{wrapfigure}
blah blah blah blah ...
\end{verbatim}

\noindent Figures 3 \& 4:
\begin{verbatim}
begin{figure}[ht]
\begin{minipage}[b]{0.5\linewidth}
\centering
\begin{othelloboard}{1}
\dotmarkings
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow			{0}{0}{0}{2}{1}{0}{0}{0}
\end{othelloboard}
\caption{left diagram}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[b]{0.5\linewidth}
\centering
\begin{othelloboard}{1}
\dotmarkings
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow			{0}{0}{0}{2}{1}{0}{0}{0}
\end{othelloboard}
\caption{right diagram}
\end{minipage}
\end{figure}
\end{verbatim}

\noindent Figure 5:
\begin{verbatim}
\begin{center}
\begin{minipage}[c]{192pt}
\textsf{
\begin{othelloboardnorefs}{1}
\othelloarrayfourthrow	{0}{0}{0}{1}{2}{0}{0}{0}
\othelloarrayfifthrow			{0}{0}{0}{2}{1}{0}{0}{0}
\annotationseighthrow{}{C}{A}{B}{B}{A}{C}{}
\end{othelloboardnorefs}}
\end{minipage}
\hfill
\begin{minipage}[c]{192pt}
Figure 5\\

The text over here is in the \verb=minipage= on the 
right. Both \verb=minipage=s are contained within 
a \verb=center= environment in this example.\\

The annotations are in a san-serif font this time.
\end{minipage}
\end{center}
\end{verbatim}

\end{multicols}
\end{scriptsize}

\section{Other commands}

\noindent You may want to exploit some of the lower-level commands that were defined as part of main commands for this package. Suppose you want to draw just a couple of discs and diamonds, but without putting them in a board diagram. You might want to make sure that these are the same size and style as those used in actual board diagrams (perhaps you're writing a key like the one on page \pageref{disckey}). In that case, you can use these commands:\\

\begin{tabular}{lclc}
\verb=\whitedisc=	\hspace{8pt}& \begin{picture}(20,20)\put(10,5){\whitedisc} \end{picture}&	 \verb=\whitediamond= \hspace{8pt} &\begin{picture}(20,20)\put(10,5){\whitediamond} \end{picture}\\
\verb=\blackdisc= \hspace{8pt}& \begin{picture}(20,20)\put(10,5){\blackdisc} \end{picture} &	\verb=\blackdiamond= \hspace{8pt} &\begin{picture}(20,20)\put(10,5){\blackdiamond} \end{picture} \\
\end{tabular}\\
\ \\

They work best within a \verb=picture= environment using the \verb=\put= command for accurate placing. Note that the shapes are positioned from their centre-points. These are the same sub-commands that the \verb=\othelloarray=\ldots\verb=row= commands call upon when they execute, so using these ensures the size and style of shapes will be the same as those appearing within board diagrams within the same document.

\subsection{Counters}
The \textsf{othelloboard} package uses counters to store the value of discs/diamonds placed on each square (this is the same value you input in order to draw the discs/diamonds). The package currently only uses these counters to determine the text colour of annotations, but these values are available for the current board diagram until another is drawn (the counters are all reset initially as part of the code for a new diagram).

The counter names follow this format:\\

\verb=disccolourxy=\\

where \verb=xy= is the name for a square. Since \LaTeX\ doesn't allow numbers in counter names, the \verb=x= and the \verb=y= are both letters a--h. This needn't be confusing however. The convention is still standard: column name first, row name second. Thus the square g2 is called \verb=gb= for the purpose of counters.

\noindent To write the value (0--4) of the disc at g2, simply use the code: \verb=\arabic{disccolourgb}=. To use the value of the disc at g2 as part of another command, simply use \verb=\value{disccolourgb}=.

\noindent To illustrate the availability of these counters, here is a lazy way of counting discs that you might use. First, define a couple of further counters to store the number of white and black discs:\\

\verb=\newcounter{numberwdiscs}=

\verb=\newcounter{numberbdiscs}=\\

Next, define a couple of commands for counting up white (black) discs and storing the result in the counters just defined:\footnote{I've now included the full code for the example at the end of the package file in case you'd like to play with it (so you needn't define these commands yourself, they'll just work). The counting commands don't run automatically however with each diagram, to save unnecessary processing. So if you want to use the value of one of the counters, you have to run the count command first.}

\begin{verbatimtab}
\newcommand{\countwhitediscs}{%	increments the counter for every white disc found. 
\ifthenelse{\equal{\value{disccolouraa}}{1}}{\addtocounter{numberwdiscs}{1}}{}
\ifthenelse{\equal{\value{disccolourba}}{1}}{\addtocounter{numberwdiscs}{1}}{}
... }
\end{verbatimtab}

\noindent Now you can just run the count commands and write \verb=\arabic{numberwdiscs}= or \\
\verb=\arabic{numberbdiscs}= to print the number of white (black) discs.
\begin{center}
\begin{minipage}[c]{192pt}
\textsf{
\begin{othelloboard}{.6}
\dotmarkings
\othelloarrayfirstrow{2}{2}{2}{2}{2}{2}{2}{2}
\othelloarraysecondrow{2}{2}{2}{2}{2}{1}{1}{2}
\othelloarraythirdrow{2}{1}{1}{2}{1}{1}{1}{2}
\othelloarrayfourthrow	{2}{1}{2}{1}{1}{1}{1}{2}
\othelloarrayfifthrow		{2}{2}{2}{1}{1}{1}{1}{2}
\othelloarraysixthrow	{2}{2}{1}{1}{1}{1}{1}{2}
\othelloarrayseventhrow	{0}{1}{2}{1}{2}{2}{2}{2}
\othelloarrayeighthrow	{1}{0}{2}{1}{1}{1}{1}{1}
\end{othelloboard}}
\end{minipage}
%\hfill
\begin{minipage}[c]{192pt}
\countwhitediscs
\countblackdiscs
Number of white discs: \arabic{numberwdiscs} \\
Number of black discs: \arabic{numberbdiscs} \\
\end{minipage}
\end{center}
\ \\

Admittedly this doesn't seem like a very useful feature, but if you like playing with this sort of thing you might find it useful to know that the \verb=disccolourxy= counters are available for use after each diagram is drawn. 

\section{Most likely errors}

\begin{itemize}
\item Forgetting to specify a size argument along with the \verb=\begin{othelloboard}= declaration.
\item Putting spaces between the `\verb=w=' in a command name and the `\verb={=' of the first argument. Use tabs if you want to align the arguments for an array (see section \ref{tabtips} above). Also make sure there are no spaces between arguments.
\item Forgetting to include an \verb=\end{othelloboard}= declaration at the end of each diagram.
\end{itemize}

\section{Changes}
\subsubsection*{18 August 2011}
\begin{itemize}
\item Extended the \verb=\drawboardfromstring= command to recognize the characters \verb=3= and \verb=4= (for diamonds) in a string.
\item Included example material typeset using this package -- two chapters from Brian Rose's book. 
\end{itemize}

\bibliographystyle{phil_review}
\begin{thebibliography}{1}
\bibitem{Rose2005} Brian Rose (2005). \emph{Othello: A Minute to Learn \ldots\ A Lifetime to Master}.\\[6pt]
The official version can be downloaded from http://othellogateway.com/rose/book.pdf.
\end{thebibliography}

\clearpage

\makebox[0pt][l]{\hspace{-64pt}
\begin{othelloboard}{2.6}
\dotmarkings
\othelloarrayfirstrow		{1}{2}{1}{1}{2}{1}{2}{1}
\othelloarraysecondrow	{2}{1}{2}{2}{2}{1}{2}{2}
\othelloarraythirdrow		{2}{2}{2}{1}{2}{1}{1}{1}
\othelloarrayfourthrow	{1}{1}{2}{1}{2}{1}{2}{2}
\othelloarrayfifthrow		{2}{1}{1}{2}{1}{2}{2}{1}
\othelloarraysixthrow	{1}{2}{2}{1}{2}{2}{1}{1}
\othelloarrayseventhrow	{2}{1}{1}{2}{1}{1}{2}{2}
\othelloarrayeighthrow	{2}{1}{2}{2}{1}{1}{2}{1}
\annotationsfirstrow		{54}	{51}	{34}	{30}	{31}	{32}	{41}	{42}
\annotationssecondrow	{55}	{50}	{43}	{33}	{29}	{28}	{39}	{58}
\annotationsthirdrow		{23}	{27}	{3}	{4}	{25}	{8}	{40}	{59}
\annotationsfourthrow	{24}	{22}	{5}	{}	{}	{6}	{37}	{60}
\annotationsfifthrow		{47}	{20}	{14}	{}	{}	{1}	{35}	{38}
\annotationssixthrow		{26}	{21}	{15}	{2}	{9}	{7}	{12}	{36}
\annotationsseventhrow	{49}	{56}	{16}	{11}	{10}	{18}	{45}	{53}
\annotationseighthrow	{57}	{46}	{17}	{13}	{44}	{52}	{19}	{48} 
\end{othelloboard}
}


%------
\end{document}
