\documentclass{pracjourn}
                  
%\usepackage{amsmath}
\usepackage{turnstile}

\TPJrevision{2007}{08}{18}
\TPJissue{2007}{3}

\title{A Tool for Logicians}

\author{Arthur Buchsbaum and Francisco Reinaldo}
\email{arthur@inf.ufsc.br, reinaldo.opus@gmail.com}

\abstract{\textsf{turnstile} is a \LaTeX{} package that allows typesetting of the mathematical logic symbol, ``turnstile'', in all of the various ways it is used. This package was developed because there was no easy way in \LaTeX{} to typeset this symbol in its various forms, and place expressions above and below the crossbar.}

\bibliographystyle{plain}

\begin{document}

\maketitle

\section{Introduction}

Logic is a science whose initial motivation was the analysis of correct reasoning. In recent years it has advanced beyond of the study of reasoning, and has many intersections with research areas such as Mathematics, Philosophy, Computer Science, Linguistics, Physics and Artificial Intelligence. One of the main signs used in Logic is the~turnstile sign, from which there are versions such as ``$\,\vdash$'' and ``$\,\models$'', issued respectively by the \LaTeX\ commands \begin{verb}=\vdash=\end{verb} and \begin{verb}=\models=\end{verb}

\section{The turnstile Project} % (fold)
\label{sec:turnstile_project}

The turnstile\footnote{\url{http://tug.ctan.org/tex-archive/macros/latex/contrib/turnstile}} is a sign often used by logicians for denoting a consequence relation, related to a given logic, between a collection of formulas and a formula. Many logicians have complained that there is no easy method in \LaTeX\ to typeset turnstile signs.  They occur in many forms, and must be able to have expressions placed correctly above and below them. \LaTeX\ commands such as \begin{verb}=\vdash=\end{verb} and \begin{verb}=\models=\end{verb} typeset the turnstile sign, but they are not capable of placing data below or above them in an acceptable way. For example, sometimes it is necessary to place the name of a considered logical system below the turnstile sign, and sometimes it is necessary to put additional information above it.

\vspace{0.2em}

If we want to say that a formula $P$ is a logical consequence of a collection $\Gamma$ of~formulas in a logic $\mathrm{L}$, we could try to typeset it by \begin{verbatim}\Gamma \vdash_\mathrm{L} P,\end{verbatim} giving $\Gamma \vdash_\mathrm{L} P$.

\medskip

Note that ``$\mathrm{L}$'' was not placed correctly with respect to the turnstile sign; it~should be placed and centred exactly below the sign. With \texttt{turnstile.sty} we can typeset it by \begin{verbatim}\Gamma \sststile{\mathrm{L}}{} P,\end{verbatim} giving $\Gamma \sststile{\mathrm{L}}{} P$.

\medskip

On the other hand, if we want to say that a formula $P$ is a logical semantical consequence of a collection $\Gamma$ of formulas in a logic $\mathrm{L}$, through varying of $x$ and~$y$\footnote{In \cite{BuchsbaumBeziau2004} and \cite{BuchsbaumPequeno1997} varying objects are presented.}, we could try to typeset it by \begin{verbatim}\Gamma \models_\mathrm{L}^{x,y} P,\end{verbatim} giving $\Gamma \models_\mathrm{L}^{x,y} P$.

\medskip

Note that both ``$\mathrm{L}$'' and ``$x,y$'' were not placed correctly with respect to turnstile sign; they should be placed and centered exactly below and above the sign. With \texttt{turnstile.sty} we can typeset it by \begin{verbatim}\Gamma \sdtstile{\mathrm{L}}{x,y} P,\end{verbatim} giving $\Gamma \sdtstile{\mathrm{L}}{x,y} P$.

\medskip

%%% LAC: Rei - it will take a super-logician to figure out the following paragraphs ;-)

The commands provided by this style are all formed by the string ``tstile'' preceded by a string with two or three letters. These letters indicate the kind of~lines to be drawn successively; they can be ``n'', ``s'', ``d'', or ``t''. The~letter~``n'' says that the line is empty, the letter ``s'' that the line is single, the letter ``d'' that the line is double, and finally ``t'' indicates that the line is triple. The corresponding commands for the two-lettered strings provide the most common turnstile signs, in which there is not a second vertical line following the~horizontal line. The first letter of these strings indicates the kind of the vertical line and the second the kind of the horizontal line to be drawn after the vertical line. The three-lettered strings can contain any of the letters ``n'', ``s'', ``d'', and~``t'', with~the restriction that the last letter must not be ``n'', because the~case in which the third line is empty is already dealt with by the commands with two-lettered strings preceding ``tstile''. The first letter specifies the kind of the first vertical line, the~second the kind of the horizontal line, and the third letter the kind of the second vertical line.

All these commands have three arguments, and the first one is optional.

The first argument, which is optional, gives the size by which the internal expressions must be displayed: ``d'' for displayed formulas, ``t'' for text formulas, ``s'' for first subscript or superscript formulas, and ``ss'' for later subscript or superscript formulas. The default value is ``s''. The result of applying ``t'' or ``d'' is the same, except if there is a mathematical sign in the second or third argument issued in~distinct ways, depending on whether it is used in text math mode or displayed math mode.

The second and third arguments provide the expressions to be placed below and above the turnstile sign respectively, where both these expressions are converted to the size specified by the first argument. On the other hand, if the~second or the third argument is empty, then nothing is put below or above the turnstile sign.

\section{Examples} % (fold)
\label{sec:examples}

Some examples are shown below.  For the sake of illustration, $\Gamma$ is a~given collection of formulas and $P$ is a logical formula. Of course, the signs ``$\Gamma$'' and ``$P$'' illustrate only one possible context in which the turnstile sign could appear.

\begin{verbatim}
\Gamma \sststile{}{} P
\end{verbatim}
\begin{equation}\Gamma \sststile{}{} P \end{equation} \vspace{0.4em}

\pagebreak

\begin{verbatim}
\Gamma \sststile{\mathrm{LPD}}{} P
\end{verbatim}
\begin{equation}\Gamma \sststile{\mathrm{LPD}}{} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sststile{}{x,y} P
\end{verbatim}
\begin{equation}\Gamma \sststile{}{x,y} P\end{equation} \vspace{0.4em}

If the optional argument is not used, then the result is the same as if ``s'' was the optional argument:

\begin{verbatim}
\Gamma \sststile{\mathrm{LPD}}{x,y} P
\end{verbatim}
\begin{equation}\Gamma \sststile{\mathrm{LPD}}{x,y} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sststile[d]{\mathrm{LPD}}{x,y} P
\end{verbatim}
\begin{equation}\Gamma \sststile[t]{\mathrm{LPD}}{x,y} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sststile[t]{\mathrm{LPD}}{x,y} P
\end{verbatim}
\begin{equation}\Gamma \sststile[d]{\mathrm{LPD}}{x,y} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sststile[s]{\mathrm{LPD}}{x,y} P
\end{verbatim}
\begin{equation}\Gamma \sststile[s]{\mathrm{LPD}}{x,y} P\end{equation} \vspace{0.4em}
          
\pagebreak

\begin{verbatim}
\Gamma \sststile[ss]{\mathrm{LPD}}{x,y} P
\end{verbatim}
\begin{equation}\Gamma \sststile[ss]{\mathrm{LPD}}{x,y} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sststile{\mathrm{LPDEFGH}}{x,y} P
\end{verbatim}
\begin{equation}\Gamma \sststile{\mathrm{LPDEFGH}}{x,y} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sststile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \sststile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sdtstile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \sdtstile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \dststile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \dststile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \ddtstile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \ddtstile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}
     
\pagebreak

\begin{verbatim}
\Gamma \dttstile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \dttstile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}
         
\begin{verbatim}
\Gamma \nsststile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \nsststile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \ndststile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \ndststile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \nsdtstile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}  \nsdtstile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \nddtstile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \nddtstile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \ndttstile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \ndttstile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}
 
\pagebreak

\begin{verbatim}
\Gamma \ssststile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \ssststile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}


\begin{verbatim}
\Gamma \stststile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \stststile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \stttstile{\mathrm{LC}}{x,y,z,w} P
\end{verbatim}
\begin{equation}\Gamma \stttstile{\mathrm{LC}}{x,y,z,w} P\end{equation} \vspace{0.4em}

Below are some examples of mathematical expressions below and~above the turnstile sign which show how it changes depending on the optional argument. If no optional argument is given, then it is considered to be ``s''.

The reader should also note that the vertical lines don't stretch according to~the~heights of the expressions located below and above the turnstile sign.  Because logicians use this sign mainly in text mode, we feel that it should have a standard height.

\begin{verbatim}
\Gamma \sststile{\sum_0^\infty 1/2^n}{\int_a^b f} P
\end{verbatim}
\begin{equation}\Gamma \sststile{\sum_0^\infty 1/2^n}{\int_a^b f} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sststile[d]{\sum_0^\infty 1/2^n}{\int_a^b f} P
\end{verbatim}
\begin{equation}\Gamma \sststile[d]{\sum_0^\infty 1/2^n}{\int_a^b f} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sststile[t]{\sum_0^\infty 1/2^n}{\int_a^b f} P
\end{verbatim}
\begin{equation}\Gamma \sststile[t]{\sum_0^\infty 1/2^n}{\int_a^b f} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sststile[s]{\sum_0^\infty 1/2^n}{\int_a^b f} P
\end{verbatim}
\begin{equation}\Gamma \sststile[s]{\sum_0^\infty 1/2^n}{\int_a^b f} P\end{equation} \vspace{0.4em}

\begin{verbatim}
\Gamma \sststile[ss]{\sum_0^\infty 1/2^n}{\int_a^b f} P
\end{verbatim}
\begin{equation}\Gamma \sststile[ss]{\sum_0^\infty 1/2^n}{\int_a^b f} P\end{equation} \vspace{0.4em}

\section{Conclusions} % (fold)
\label{sec:conclusions}

The package \texttt{turnstile.sty} seems to be adequate for typesetting the turnstile sign in~its many forms.  It correctly places additional expressions below and above it, if necessary, and stretches the the crossbar width as much as needed to contain the expressions.

For a future version of this package, we want to look at changing the height of the turnstile sign.  This will take into account the heights of the~expressions above and below, similar to the way we currently allow for the widths of the expressions.

\begin{thebibliography}{1}

\bibitem{BuchsbaumBeziau2004}
Arthur Buchsbaum and Jean-Yves B{\'e}ziau.
\newblock Introduction of implication and~generalization in axiomatic calculi.
\newblock In Jean-Yves B{\'e}ziau, Alexandre~Costa Leite, and Alberto Facchini,
  editors, {\em Aspects of Universal Logic}, number~17 in~Travaux de Logique,
  page 231. Centre de Recherches S{\'e}miologiques, \mbox{Universit{\'e}} de
  Neuch{\^a}tel, December 2004.

\bibitem{BuchsbaumPequeno1997}
Arthur Buchsbaum and Tarcisio Pequeno.
\newblock A general treatment for the deduction theorem in open calculi.
\newblock {\em Logique et Analyse}, 157:9--29, January--March 1997.

\bibitem{KopkaDaly1999}
Helmut Kopka and Patrick~W. Daly.
\newblock {\em A Guide to \LaTeX}.
\newblock Addison-Wesley, 1999.

\bibitem{Lamport1994}
Leslie Lamport.
\newblock {\em \LaTeX\ -- A Document Preparation System -- User's Guide and Reference Manual}.
\newblock Addison-Wesley, 1994.

\bibitem{NerodeShore1997}
Anil Nerode and Richard~A. Shore.
\newblock {\em Logic for Applications}.
\newblock Springer, 1997.

\bibitem{Nolt1996}
John Nolt.
\newblock {\em Logics}.
\newblock Wadsworth, 1996.

\end{thebibliography}

\end{document}
