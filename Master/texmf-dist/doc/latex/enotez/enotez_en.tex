% arara: pdflatex
% arara: makeindex: { sort: true, style: enotez_en.ist }
% arara: biber
% arara: pdflatex
% arara: pdflatex
% --------------------------------------------------------------------------
% the ENOTEZ package
% 
%   Endnotes for LaTeX2e
% 
% --------------------------------------------------------------------------
% Clemens Niederberger
% Web:    https://bitbucket.org/cgnieder/enotez/
% E-Mail: contact@mychemistry.eu
% --------------------------------------------------------------------------
% Copyright 2011-2012 Clemens Niederberger
% 
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
% 
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Clemens Niederberger.
% --------------------------------------------------------------------------
% The enotez package consists of the files
%  - enotez.sty, enotez_en.tex, enotez_en.pdf, README
% --------------------------------------------------------------------------
% If you have any ideas, questions, suggestions or bugs to report, please
% feel free to contact me.
% --------------------------------------------------------------------------
\documentclass[toc=bib,toc=index]{cnpkgdoc}
\docsetup{
  pkg      = enotez ,
  code-box = {
    backgroundcolor  = gray!7!white ,
    skipbelow        = .6\baselineskip plus .5ex minus .5ex ,
    skipabove        = .6\baselineskip plus .5ex minus .5ex ,
    roundcorner      = 3pt ,
  } ,
  gobble   = 1 ,
  subtitle = {Endnotes for \LaTeXe}
}

\addcmds{
  appendix,
  chapter,
  cmd,
  DeclareInstance,
  DeclareTemplateInterface,
  endnote,
  endnotemark,
  endnotetext,
  enmark,
  enmarkstyle,
  enotezwritemark,
  kant,
  phantomsection,
  printendnotes,
  setenotez,
  splitendnotes,
  textsuperscript
}
\setenotez{mark-cs=\textsu}
\DeclareInstance{enotez-list}{addsec}{paragraph}{heading=\addsec{#1}}

% Layout:
% \usepackage{libertine}
\cnpkgusecolorscheme{friendly}
\renewcommand*\othersectionlevelsformat[3]{%
  \textcolor{main}{#3\autodot}\enskip}
\renewcommand*\partformat{%
  \textcolor{main}{\partname~\thepart\autodot}}
\usepackage{fnpct}
\AdaptNote\endnote\multendnote
\usepackage{embrac}[2012/06/29]
\ChangeEmph{[}[,.02em]{]}[.055em,-.08em]
\ChangeEmph{(}[-.01em,.04em]{)}[.04em,-.05em]

\ExplSyntaxOn
\NewDocumentCommand \Default {g}
  {
    \hfill\llap
      {
        \IfNoValueTF { #1 }
          {(initially~empty)}
          {Default:~\code{#1}}
      }
    \newline
  }
\ExplSyntaxOff

\usepackage[backend=biber,style=alphabetic]{biblatex}
\addbibresource{\jobname.bib}

% rudimentary solution for a `maintainer' field:
\DeclareFieldFormat{authortype}{\mkbibparens{#1}}
% \DeclareFieldAlias{maintainer}{author}
\DeclareBibliographyAlias{package}{misc}
\renewbibmacro*{author}{%
  \ifboolexpr{
    test \ifuseauthor
    and
    not test {\ifnameundef{author}}
  }
    {\printnames{author}%
     \iffieldundef{authortype}
       {}
       {\setunit{\space}%
	\usebibmacro{authorstrg}}}
    {}}

\usepackage{filecontents}
\begin{filecontents*}{\jobname.bib}
@package{endnotes,
  title      = {endnotes},
  author     = {Robin Fairbairns},
  authortype = {current maintainer},
  date       = {2003-01-15},
  version    = {NA},
  url        = {http://www.ctan.org/pkg/endnotes},
  urldate    = {2012-07-03}
}
@package{sepfootnotes,
  title   = {sepfootnotes},
  author  = {Eduardo C. Louren\c{c}o de Lima},
  date    = {2012-03-06},
  version = {0.1},
  url     = {http://www.ctan.org/pkg/sepfootnotes},
  urldate = {2012-07-03}
}
\end{filecontents*}

\usepackage{makeidx}
\begin{filecontents*}{\jobname.ist}
 heading_prefix "{\\bfseries "
 heading_suffix "\\hfil}\\nopagebreak\n"
 headings_flag  1
 delim_0 "\\dotfill "
 delim_1 "\\dotfill "
 delim_2 "\\dotfill "
 delim_r "\\textendash"
 suffix_2p "\\nohyperpage{\\,f.}"
 suffix_3p "\\nohyperpage{\\,ff.}"
\end{filecontents*}
\makeindex

\usepackage{kantlipsum}
\usepackage{etoolbox}
\AtBeginEnvironment{beispiel}{\setfnpct{dont-mess-around}}
\usepackage{enumitem}
\begin{document}

\section{Licence and Requirements}
Permission is granted to copy, distribute and/or modify this software under the
terms of the \LaTeX\ Project Public License, version 1.3 or later
(\url{http://www.latex-project.org/lppl.txt}). The package has the status
``maintained.''

\enotez needs and loads the following packages: \paket{expl3},
\paket{xparse}, \paket{xtemplate} and \paket{l3keys2e}. If you're using
\paket{memoir}, a \paket[http://www.ctan.org/pkg/koma-script]{KOMA-Script}
class or the package \paket{caption} \enotez also needs and loads \paket{etoolbox}.

\section{Motivation}
\enotez is a new implementation of endnotes for \LaTeXe\ since the \paket{endnotes}
package~\cite{endnotes} has some deficiencies. Nested endnotes, for example, are
not supported, neither is \paket{hyperref}. The \paket{sepfootnotes} package~\cite{sepfootnotes}
also provides means for endnotes but actually has a different purpose: to
separate input and usage both of footnotes and endnotes. So it might not be the
best solution in every case\footnote{You have to write the actual notes in the
preamble or a separate file and reference them in the text.}. It also does not
allow nested endnotes.

\enotez enables nested endnotes properly and has another mechanism of customizing
the list of endnotes which is easily extendable. One of the main features  of
\enotez is a split list of endnotes in which the notes are automatically
separated by the sections or chapters they were set in, see section~\ref{sec:split}
for more information.

As an aside: \enotez is nicely compatible with the \paket{fnpct} package%
\footnote{You'll have to use \cmd*{AdaptNote} for the time being, though.}.

\section{Usage}
\subsection{Placing the Notes}
The usage is simple: use \cmd{endnote} in the text where you want to place the
note mark.
\begin{beschreibung}
 \befehl{endnote}[<mark>]{<text>} Add an endnote in the text.
\end{beschreibung}
\begin{beispiel}
 This is some text.\endnote{With an endnote.}
\end{beispiel}
There's not really much more to it. It is possible to add a custom mark by
using the optional argument but that shouldn't be needed too often. \cmd{endnote}
works fine inside tables, minipages, floats and captions\footnote{This has been
tested with the standard classes, \paket*{memoir}, and the \paket*{KOMA-Script}
classes, with and without the \paket*{caption} package. If you're using another
package that redefines \cmd*{caption} it might not work.}. Endnotes can also be
nested. Since that seemed making a pair \cmd{endnotemark}/\cmd{endnotetext}
superfluous they are \emph{not} defined by \enotez.
\begin{beispiel}
 This is some text.\endnote{With another endnote.\endnote{This is a
 nested\endnote{And another level deeper\ldots} endnote!}}
 % uses package `kantlipsum':
 Of course you can have several paragraphs\endnote{\kant[1-3]} in an endnote.
\end{beispiel}

\subsection{Printing the Notes}
The notes are printed by using the command \cmd{printendnotes}.
\begin{beschreibung}
 \befehl{printendnotes}*[<style>] Print the list of endnotes. \code{<style>} is
   one of the instances explained in section~\ref{ssec:customizing_the_list}.
\end{beschreibung}
If used without argument it prints all notes set so far with \cmd{endnote}. The
current list will then be cleared. All endnotes set after it are stored again
for the next usage of \cmd{printendnotes}. The starred version will print
\emph{all} endnotes but shouldn't be used more than once if you have nested
endnotes.

It may take several compilation runs until all notes are printed correctly. In
a first run they are written to the \code{aux} file. In the second run they are
available to \cmd{printendnotes}. If you have nested endnotes they will be written
to the \code{aux} file the first time they're printed with \cmd{printendnotes}
which means you might have to compile your file once more. If you change any of
the endnotes or add another one you again will need at least two runs, maybe more.
\enotez tries to warn you in these cases by invoking \LaTeX's warning
\achtung{\code{Label(s) may have changed. Rerun to get cross-references right.}}
but may not catch all cases.

\section{Options}
\subsection{Package Options}
\enotez has a few package options which should be pretty self-explanatory. They
can be set either as package options with \verb=\usepackage[<options>]{enotez}=
or with the setup command.
\begin{beschreibung}
 \befehl{setenotez}{<options>} Setup command for setting \enotez' options.
 \Option{list-name}{<list name>}\Default{Notes}
   The name of the notes list. This name is used for the heading of the list.
 \Option{reset}{\default{true}|false}\Default{false}
   If set to \code{true} the notes numbers will start from 1 again after
   \cmd{printendnotes} has been invoked.
 \Option{counter-format}{arabic|alph|Alph|roman|Roman}\Default{arabic}
   Change the format of the endnote counter.
 \Option{mark-format}{<code>}\Default
   Redefine \cmd{enmarkstyle} to execute \code{<code>}. This command is placed
   directly before the endnote mark in the text.
 \Option{mark-cs}{<command>}\Default{\cmd{textsuperscript}}
   Lets \cmd{enotezwritemark} to be equal to \code{<command>}. This command is
   used to typeset the endnote marks in the text and should take one argument.
 \Option{totoc}{section|chapter|false}\Default{false}
   Add an entry to the table of contents.
 \Option{list-style}{<style>}\Default{plain}
   Sets the default list style, see section \ref{ssec:customizing_the_list} for
   details.
\end{beschreibung}

\subsection{Customizing the List}\label{ssec:customizing_the_list}
The list is typeset with \paket{xtemplate}'s possibilities. \enotez declares
the object \code{enotez-list} and two templates for it, the template \code{paragraph}
and the template \code{list}.

\subsubsection{The \code{paragraph} Template}
The \code{paragraph} template's interface is defined as follows:
\begin{beispiel}[code only]
 \DeclareTemplateInterface{enotez-list}{paragraph}{1}
   {
     % parameter   : type       = default
     heading       : function 1 = \section*{#1}   ,
     format        : tokenlist  = \footnotesize   ,
     number        : function 1 = \enmark{#1}     ,
     number-format : tokenlist  = \normalfont     ,
     notes-sep     : length     = .5\baselineskip ,
   }
\end{beispiel}
The parameters functions are these:
\begin{description}[style=nextline]
 \item[\code{heading}] The command with which the heading is typeset.
 \item[\code{format}] The format of the whole list.
 \item[\code{number}] The command that is used to typeset the numbers of the
   notes. The command \cmd{enmark} is explained soon.
 \item[\code{numbers-format}] The format of the numbers.
 \item[\code{notes-sep}] Additional space between the notes.
\end{description}

\enotez uses this template to define the instance \code{plain}:
\begin{beispiel}[code only]
 \DeclareInstance{enotez-list}{plain}{paragraph}{}
\end{beispiel}
This is the default style of the list.

You can easily define your own instances, though:
\begin{beispiel}[code only]
 \DeclareInstance{enotez-list}{custom}{paragraph}
   {
     heading   = \chapter*{#1}        ,
     notes-sep = \baselineskip        ,
     format    = \normalfont          ,
     number    = \textsuperscript{#1}
   }
\end{beispiel}
This would use a chapter heading for the title, separate the notes with
\verb=\baselineskip= and typeset them with \verb=\normalfont=. The numbers would
be typeset with \verb=\textsuperscript=. You could now use it like this:
\begin{beispiel}[code only]
 \printendnotes[custom]
\end{beispiel}

If you wanted superscripted numbers, you could also redefine \cmd{enmark}. 
\begin{beschreibung}
 \befehl{enmark} is defined like this: \verb=\newcommand*\enmark[1]{#1.}=
\end{beschreibung}

\subsubsection{The \code{list} Template}
The \code{list} template's interface is defined as follows:
\begin{beispiel}[code only]
 \DeclareTemplateInterface{enotez-list}{list}{1}
   {
     % parameter   : type       = default
     heading       : function 1 = \section*{#1} ,
     format        : tokenlist  = \footnotesize ,
     number        : function 1 = \enmark{#1}   ,
     number-format : tokenlist  = \normalfont   ,
     list-type     : tokenlist  = description   ,
   }
\end{beispiel}
This template uses a list to typeset the notes. As you can see the default list
is a \code{description} list.

\enotez defines two instances of this template:
\begin{beispiel}[code only]
 \DeclareInstance{enotez-list}{description}{list}{}
 \DeclareInstance{enotez-list}{itemize}{list}{list-type = itemize}
\end{beispiel}
They're available through \cmd{printendnotes}[description] and
\cmd{printendnotes}[itemize], respectively.

Again you can define your own instances using whatever list you want, possibly
one defined with the power of \paket{enumitem}.

\section{Collect Notes Section-wise and Print List Stepwise}\label{sec:split}
\emph{This feature is experimental and has some limitations}.

Not to be misunderstood: you can use \cmd{printendnotes} as often as you like,
possibly after each section. That is \emph{not} what is meant here. Let's
suppose you are writing a book and have many endnotes in many chapters. It
would be nice if the list of endnotes at the end of the book could be split
into parts for each chapter. This section describes how you can achieve that with
\enotez.

First of all \enotez will rely on the fact that you use \cmd{printendnotes}
only \emph{once}! If you call it more times nobody knows what will happen\ldots

You'll need to tell \enotez that you want to split the notes into groups.
\begin{beschreibung}
 \Option{split}{section|chapter|false}\Default{false}
   Enable the automatic splitting.
 \Option{split-sectioning}{<csname>}\Default
   The command that is used to display the titles between the splits. It needs
   to be a command that takes one argument and should be entered without the
   leading backslash. If the option is not used \enotez will choose
   \code{subsection*} for \key*{split}{section} and \code{section*} for
   \key*{split}{chapter}.
 \Option{split-title}{<tokenlist>}\Default{Notes from <name> <ref>}
   The title that will be inserted between the splits. \code{<name>} is replaced
   by \code{Section} for \key*{split}{section} and \code{Chapter} for
   \key*{split}{chapter}. \code{<ref>} is replaced by the corresponding
   \cmd*{thesection} or \cmd*{thechapter}.
\end{beschreibung}
Set the \key{split} option:
\begin{beispiel}[code only]
 \setenotez{split=section}
\end{beispiel}
Well -- that's it, basically. You'll have to be careful, though:
If you're having nested endnotes the nested ones appear first in the ``Notes''
section (or chapter, respectively). In this case you should have a numbered
section title for the notes, presumably in the appendix. You'll need to create
a new list style:
\begin{beispiel}[code only]
 % preamble:
 \usepackage{enotez}
 \DeclareInstance{enotez-list}{section}{paragraph}{heading=\section{#1}}
 \setenotez{list-style=section,split=section}
 % document:
 \appendix
 \printendnotes
\end{beispiel}

Please beware that the option \key{reset} also impacts here: the numbing will
be reset for each section or chapter, depending on the choice you made for
\key{split}.

\enotez comes with an example document for a split list which you should
find in the same folder as this documentation.

\section{hyperref Support}
If \paket{hyperref} is loaded and you are using the option \key{totoc} (see
p~\pageref{key:totoc}) the list title is linked via a \verb=\phantomsection=.

If \paket{hyperref} is used with \code{hyperfootnotes} set to \code{true} the
endnote marks are linked to the respective entries in the list.

\printendnotes[addsec]

{\EmbracOff\printbibliography}

\setindexpreamble{Section titles are indicated \textbf{bold}, packages
\textsf{sans serif}, commands \code{\textbackslash\textcolor{code}{brown}}
 and options \textcolor{key}{\code{yellow}}.\par\bigskip}

\printindex
\end{document}