% arara: pdflatex: { shell: on }
% arara: pdflatex: { shell: on }
% --------------------------------------------------------------------------
% the SUBSTANCES package
% 
%   A Chemical Database
% 
% --------------------------------------------------------------------------
% Clemens Niederberger
% Web:    https://bitbucket.org/cgnieder/substances/
% E-Mail: contact@mychemistry.eu
% --------------------------------------------------------------------------
% Copyright 2012 Clemens Niederberger
% 
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
% 
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Clemens Niederberger.
% --------------------------------------------------------------------------
% The substances package consists of the files
%  - substances.sty, substances-default.def, substances-examples.sub,
%    substances_en.tex, substances_en.pdf, README
% --------------------------------------------------------------------------
% If you have any ideas, questions, suggestions or bugs to report, please
% feel free to contact me.
% --------------------------------------------------------------------------
%
% if you want to compile this documentation you'll need the document class
% `cnpkgdoc' which you can get here:
%    https://bitbucket.org/cgnieder/cnpkgdoc/
% the class is licensed LPPL 1.3 or later
%
\documentclass[DIV10,toc=index,toc=bib]{cnpkgdoc}
\docsetup{
  pkg = {[index]substances} ,
  code-box = {
    backgroundcolor  = gray!7!white ,
    skipbelow        = .6\baselineskip plus .5ex minus .5ex ,
    skipabove        = .6\baselineskip plus .5ex minus .5ex ,
    roundcorner      = 3pt ,
  } ,
  gobble   = 0
}
\LoadSubstances{substances-examples}
\addcmds{
  @CAS,
  @EC,
  ac,
  AllSubstancesClist,
  AllSubstancesSequence,
  arraybackslash,
  bottomrule,
  CAS,
  celsius,
  ch,
  chem,
  chemfig,
  chemsetup,
  cmc,
  DeclareChemIUPAC,
  DeclareSubstance,
  DeclareSubstanceProperty,
  EC,
  endinput,
  ForAllSubstancesDo,
  GetSubstanceProperty,
  gram,
  IfSubstanceExistF,
  IfSubstanceExistT,
  IfSubstanceExistTF,
  IfSubstanceFieldF,
  IfSubstanceFieldT,
  IfSubstanceFieldTF,
  IfSubstancePropertyF,
  IfSubstancePropertyT,
  IfSubstancePropertyTF,
  iupac,
  Lewis,
  LoadSubstances,
  midrule,
  MolMass,
  NewDocumentCommand,
  newindex,
  normal,
  num,
  per,
  printindex,
  RetrieveSubstanceProperty,
  SI,
  sisetup,
  SubstanceIndexAltEntry,
  SubstanceIndexNameAltEntry,
  SubstanceIndexNameEntry,
  toprule
}

% Layout:
\usepackage[osf]{libertine}
\cnpkgcolors{
  main   => cnpkgred ,
  key    => yellow!40!brown ,
  module => cnpkgblue ,
  link   => black!90
}
\usepackage{booktabs}
\renewcommand*\othersectionlevelsformat[3]{%
  \textcolor{main}{#3\autodot}\enskip}
\renewcommand*\partformat{%
  \textcolor{main}{\partname~\thepart\autodot}}
\usepackage{fnpct}
\usepackage{embrac}[2012/06/29]% option biblatex, falls Bibliography dazu kommt
\ChangeEmph{[}[,.02em]{]}[.055em,-.08em]
\ChangeEmph{(}[-.01em,.04em]{)}[.04em,-.05em]

\usepackage{acro}
\DeclareAcronym{CAS}{CAS,}{Chemical Abstract Service,}
\DeclareAcronym{EC}{EC}{European Commission Number}
\DeclareAcronym{LD50}{LD50,}{Median Lethal Dose}
\DeclareAcronym{IUPAC}{IUPAC,}{International Union of Pure and Applied Chemistry,}
\DeclareAcronym{ghs}{GHS,}{Globally Harmonized System of Classification and
  Labelling of Chemicals,}

% bibliography:
% \usepackage[style=alphabetic,backend=biber]{biblatex}

% index:
\usepackage{imakeidx,filecontents}
\begin{filecontents*}{\jobname.ist}
 heading_prefix "{\\bfseries "
 heading_suffix "\\hfil}\\nopagebreak\n"
 headings_flag  1
 delim_0 "\\dotfill "
 delim_1 "\\dotfill "
 delim_2 "\\dotfill "
 delim_r "\\textendash"
 suffix_2p "\\nohyperpage{\\,f.}"
 suffix_3p "\\nohyperpage{\\,ff.}"
\end{filecontents*}

% \indexsetup{noclearpage}
\makeindex[options=-sl \jobname.ist,intoc,title=Package Index]
\makeindex[name=\jobname-chem,title=Index of Chemicals,intoc,columns=3]

% abstract:
\TitlePicture{\parbox{.7\linewidth}{%
  The \substances package allows you to create a database like file that contains
  data of various chemicals. These data can be retrieved in the document. An index
  creation of the chemicals used in the document is directly supported.}}

\ExplSyntaxOn
\NewDocumentCommand \Default {g}
  {
    \hfill\llap
      {
        \IfNoValueTF { #1 }
          {(initially~empty)}
          {Default:~\code{#1}}
      }
    \newline
  }
\NewDocumentCommand \required {}
  { \hfill \llap { required } \newline }
\NewDocumentCommand \optional {}
  { \hfill \llap { optional } \newline }
\ExplSyntaxOff
\begin{document}

\section{Licence and Requirements}
Permission is granted to copy, distribute and/or modify this software under the
terms of the \LaTeX{} Project Public License, version 1.3 or later
(\url{http://www.latex-project.org/lppl.txt}). The package has the status
``maintained.''

\substances loads and needs the following packages: \paket{expl3},
\paket{xparse}, \paket{xtemplate} and \paket{l3keys2e}. It also needs the
chemistry package \paket{chemmacros}.

\section{About}
The \substances package allows you to create a database like file that contains
data of various chemicals. These data can be retrieved in the document. An index
creation of the chemicals used in the document is directly supported.

\section{Options}
\substances has only a few options:
\begin{beschreibung}
 \Option{draft}{\default{true}|false}\Default{false}
   If set to true all warnings will be errors.
 \Option{final}{\default{true}|false}\Default{true}
   The opposite of \key{draft}.
 \Option{index}{\default{true}|false}\Default{false}
   Add index entries when \cmd{chem} is called, see section~\ref{sec:index}.
 \Option{style}{<style>}\Default{default}
   Load specific style, see section~\ref{ssec:styles}.
 \Option{strict}{\default{true}|false}\Default{false}
   If set to true all warnings will be errors. This option overwrites any
   \key{draft} or \key{final} option that is passed on by the document class.
\end{beschreibung}

\section{The Database}
\subsection{Declaring the Chemicals}
The data about substances are stored via the command
\begin{beschreibung}
 \Befehl{DeclareSubstance}{<id>}\ma{<list of properties>}
\end{beschreibung}
An entry could look like this:
\begin{beispiel}[code only]
\DeclareSubstance{NaCl}{
  name    = Sodiumchloride ,
  sum     = NaCl ,
  CAS     = 7647-14-5,
  mass    = 58.44 ,
  mp      = 801 ,
  bp      = 1465 ,
  phase   = solid ,
  density = 2.17
}
\end{beispiel}

Such entries can either be declared in the document preamble or probably
more useful in a file with the ending \code{.sub}. Such a file can be input in
the document via
\begin{beschreibung}
 \befehl{LoadSubstances}{<filename>} input the file \emph{without} specifying
   the file ending.
\end{beschreibung}
Suppose you have the file \code{mysubstances.sub} then you input it in the
document preamble via \cmd{LoadSubstances}{mysubstances}.

\subsection{Available Fields}
\subsubsection{Always Defined Fields}
Below all fields defined by \substances are listed\footnote{Look in the file
\code{substances-examples.sub} which is part of this package and
should be in the same place as this documentation for example uses.}.
\begin{beschreibung}
 \Option{name}\required The \acs{IUPAC} name of the substance. This is the only
   field that \emph{has} to be used. The field's input is parsed with
   \paket{chemmacros}' command \cmd{iupac}.
 \Option{sort}\optional If you plan to use the \key{index} option you
   should specify this field to get the sorting of the index right. This then
   creates index entries \cmd{index}{<sort field>\@<name field>}.
 \Option{alt}\optional An alternative name. The field's input is parsed with
   \paket{chemmacros}' command \cmd{iupac}.
 \Option{altsort}\optional This is the same as the \key{sort} field but for the
   alternative name.
 \Option{CAS}\optional The \ac{CAS} number. The input needs to be input in the
   form \code{<num>-<num>-<num>}.
 \Option{PubChem}\optional The PubChem number.
\end{beschreibung}

The \key{CAS} field processes the number using the macro \cmd{CAS}{<number>}
which is defined like this:
\begin{beispiel}[code only]
 \def\@CAS#1-#2-#3{\iupac{#1\-#2\-#3}}
 \NewDocumentCommand\CAS{m}{\@CAS#1}
\end{beispiel}
You're free to redefine it to your needs.

\subsubsection{Style-dependend Fields}
\substances defines the style `default' which is loaded if no other style has
been specified. It defines the following additional fields and loads the
packages \paket{chemfig} and \paket{siunitx}.
\begin{beschreibung}
 \Option{formula}\optional The molecular formula of the substance. The field's
   input is parsed with \paket{chemmacros}' command
   \cmd{ch}.
 \Option{structure}\optional The structural formula of the substance. The field's
   input is parsed with \paket{chemfig}'s command \cmd{chemfig}.
 \Option{mp}\optional The boiling point. The field's entry is input into
   the \paket{siunitx} command \cmd{SI} in the following way:
   \cmd{SI}{<field>}\ma{\cmd{celsius}}.
 \Option{bp}\optional The melting point. The field's entry is input into
   the \paket{siunitx} command \cmd{SI} in the following way:
   \cmd{SI}{<field>}\ma{\cmd{celsius}}.
 \Option{density}\optional The density. The field's entry is input into
   the \paket{siunitx} command \cmd{SI} in the following way:
   \cmd{SI}{<field>}\ma{\cmd{gram}\cmd{per}\cmd{cmc}}.
 \Option{phase}\optional The state of aggregation.
 \Option{pKa}\optional The \pKa\ value. The field's entry is input into the
   \paket{siunitx} command \cmd{num}.
 \Option{pKa1}\optional The first of several \pKa\ values. The field's entry is
   input into the \paket{siunitx} command \cmd{num}.
 \Option{pKa2}\optional The second of several \pKa\ values. The field's entry is
   input into the \paket{siunitx} command \cmd{num}.
 \Option{pKb}\optional The \pKb\ values. The field's entry is input into the
   \paket{siunitx} command \cmd{num}.
 \Option{pKb1}\optional The first of several \pKb\ values. The field's entry is
   input into the \paket{siunitx} command \cmd{num}.
 \Option{pKb2}\optional The second of several \pKb\ values. The field's entry is
   input into the \paket{siunitx} command \cmd{num}.
 \Option{pictograms}\optional The GHS pictograms. This field takes a list
   of pictogram names as they're input into \paket{chemmacros}' command
   \cmd{ghspic}.
 \Option{H}\optional The H statements. This field takes a list
   of numbers as they're input into \paket{chemmacros}' command
   \cmd{ghs}{h}\ma{<number>}.
 \Option{P}\optional The P statements. This field takes a list
   of pictogram names as they're input into \paket{chemmacros}' command
   \cmd{ghs}{p}\ma{<number>}.
 \Option{EUH}\optional The EUH statements. This field takes a list
   of pictogram names as they're input into \paket{chemmacros}' command
   \cmd{ghs}{h}\ma{<number>}.
 \Option{LD50}\optional The \ac{LD50} in \si{\milli\gram\per\kilo\gram}. The
   field's entry is input into the \paket{siunitx} command \cmd{SI} in the
   following way: \cmd{SI}{<field>}\ma{\cmd{milli}\cmd{gram}\cmd{per}\cmd{kilo}\cmd{gram}}.
\end{beschreibung}

\subsection{Define Custom Styles}\label{ssec:styles}
\subsubsection{Background}
You might have other needs for fields than the ones defined by \substances and
the `default' style. You can easily define your own style which means that you
save a file with the name \code{substances-<style>.def}. In it you save the
commands you need and use the command \cmd{DeclareSubstanceProperty}, which is
explained in the next section, to declare your own fields.

Please beware that this style file is loaded by the package when the \code{expl3}
namespace is active. This means that all spaces are ignored and you need to use
\code{\textasciitilde} if you need a space. It also means that \code{\_} and
\code{:} are letters (as is \code{{\makeatletter @}}). Should you ever decide to
call \cmd{ExplSyntaxOff} to turn off the \code{expl3} namespace \emph{don't} forget
to use \cmd{ExplSyntaxOn} at the end of the file!

\subsubsection{Declare New Fields or Change Existing Fields}
You might want other fields or change the definition of the predefined ones.
For this there's
\begin{beschreibung}
 \Befehl{DeclareSubstanceProperty}*{<field name>}\oa{<pre code>}\oa{<post code>}
\end{beschreibung}
This command declares a new property field for a substance. The star makes the
property a required one which means an error will be issued if a substance is
declared without it. The optional arguments \code{<pre code>} and \code{<post code>}
specify any code that should be input directly before or after the field entry,
respectively. The \code{<pre code>} may end with a command that takes one mandatory
argument. In this case the field entry will be its argument.

The following example would define a field \key{EC} which uses a custom command
to parse the field entry. The \ac{EC} is assigned to chemical substances
for regulatory purposes within the European Union by the regulatory authorities.
\begin{beispiel}[code only]
 \makeatletter
 \def\@EC#1-#2-#3{#1-#2-#3}
 \newcommand*\EC[1]{\@EC#1}
 \makeatother
 \DeclareSubstanceProperty{EC}[\EC]
\end{beispiel}
For further examples of the usage of pre and post code look at the definition of
the \key{name} and the \key{mp} field:
\begin{beispiel}[code only]
 \DeclareSubstanceProperty*{name}[\iupac]
 \DeclareSubstanceProperty{mp}[\SI][{\celsius}]
\end{beispiel}


\section{Retrieving the Data}
There are two commands defined by \substances that allow the retrieving of the
data. The command \cmd{chem} is intended as user command, the command
\cmd{GetSubstanceProperty} can be used to define your own user command (perhaps
in your own style file, see section~\ref{ssec:styles}).
\begin{beschreibung}
 \Befehl{chem}*[<pre>]\oa{<post>}\ma{<id>}\oa{<property>}
 \Befehl{GetSubstanceProperty}{<id>}\ma{<property>}
\end{beschreibung}
If the command \cmd{chem} is called without the \oa{<property>} argument the
\code{name} entry will be called. The starred version calls the \code{alt} entry
if it is defined and the \code{name} entry otherwise. The arguments \oa{<pre>}
and \oa{<post>} add arbitrary input before or after the output, respectively.

All of the next examples use the data defined in the file \code{substances-examples.sub}
that is part of this package, see section~\ref{sec:examples}.
\begin{beispiel}
 \chem{H2SO4}[structure] \newline
 \chem{H2SO4} has the boiling point \(\chem[T_b =]{H2SO4}[bp]\) and a
 density of \(\chem[\rho =]{H2SO4}[density]\).

 Compare the melting points of methane and ethane,
 \(\chem[T_m=]{methane}[mp]\) and \(\chem[T_m=]{ethane}[mp]\),
 with the boiling points \(\chem[T_b=]{methane}[bp]\) and
 \(\chem[T_b=]{ethane}[bp]\).
 
 \chem{NaCl} has the \ac{CAS} number \chem{NaCl}[CAS].
 
 \chem{acetone} (\chem*{acetone}) is the most simple ketone:
 
 \chem{acetone}[structure]
\end{beispiel}

The following code creates table~\ref{tab:methane}.
\begin{beispiel}[code and float]
\begin{table}[htp]
\centering\chemsetup[ghsystem]{hide}
\sisetup{scientific-notation=fixed,fixed-exponent=0,per-mode=symbol}
\begin{tabular}{l>{\raggedright\arraybackslash}p{.6\linewidth}}
 \toprule
  name              & \chem{methane} \\
  formula           & \chem{methane}[formula] \\
                    & \chem{methane}[structure] \\
 \midrule
  CAS               & \chem{methane}[CAS] \\
  PubChem           & \chem{methane}[PubChem] \\
 \midrule
  boiling point     & \chem{methane}[bp] \\
  melting point     & \chem{methane}[mp] \\
  density           & \chem{methane}[density] \\
  molar mass        & \chem{methane}[mass] \\
 \midrule
                    & \chem{methane}[pictograms] \\
  H statements      & \chem{methane}[H] \\
  P statements      & \chem{methane}[P] \\
 \bottomrule
\end{tabular}
\caption{\label{tab:methane}All properties of \chem{methane} that have been
saved in the example database.}
\end{table}
\end{beispiel}

\section{Additional Commands}
\substances provides a few commands that maybe are useful in building custom
macros for styles. A field exists if it has been defined with
\cmd{DeclareSubstanceProperty} regardless if it has been used or not. A substance
exists if it has been defined with \cmd{DeclareSubstance}.
\begin{beschreibung}
 \Befehl{GetSubstanceProperty}{<id>}\ma{<field>} \cnpkgdocarrow\ You know that
   already: retrieve the property specified in \code{<field>} for a given substance.
   This command is \emph{not} expandable.
 \Befehl{RetrieveSubstanceProperty}{<id>}\ma{<field>} \cnpkgdocarrow\ Same as
   \cmd{GetSubstanceProperty} but expandable.
 \befehl{ForAllSubstancesDo}{<code>} Loops through all existing substances.
   Inside \code{<code>} \code{\#1} may be used to refer to the \code{<id>} of the
   current substance. This command is expandable.
 \befehl{AllSubstancesSequence} A sequence of all substances. This is a sequence
   of balanced groups each containing the \code{<id>} of a substance. This command
   is expandable.
 \befehl{AllSubstancesClist} A comma separated list of all substances. Every
   \code{<id>} is separated from the next with a comma. This command is expandable.
 \Befehl{IfSubstancePropertyTF}{<id>}\ma{<field>}\ma{<true code>}\ma{<false code>}
   \cnpkgdocarrow\ Tests if the property \code{<field>} is defined for the
   substance \code{<id>} and returns either \code{<true code>} or \code{<false code>}.
   This command is expandable.
 \Befehl{IfSubstancePropertyT}{<id>}\ma{<field>}\ma{<true code>} \cnpkgdocarrow\
   Tests if the property \code{<field>} is defined for the substance \code{<id>}
   and returns \code{<true code>} if it is. This command is expandable.
 \Befehl{IfSubstancePropertyF}{<id>}\ma{<field>}\ma{<false code>} \cnpkgdocarrow\
   Tests if the property \code{<field>} is defined for the substance \code{<id>}
   and returns \code{<false code>} if it isn't. This command is expandable.
 \Befehl{IfSubstanceFieldTF}{<field>}\ma{<true code>}\ma{<false code>}
   \cnpkgdocarrow\ Tests if the property \code{<field>} exists and returns either
   \code{<true code>} or \code{<false code>}. This command is expandable.
 \Befehl{IfSubstanceFieldT}{<field>}\ma{<true code>} \cnpkgdocarrow\
   Tests if the property \code{<field>} exists and returns \code{<true code>} if
   it does. This command is expandable.
 \Befehl{IfSubstanceFieldF}{<field>}\ma{<false code>} \cnpkgdocarrow\
   Tests if the property \code{<field>} exists and returns \code{<false code>} if
   it doesn't. This command is expandable.
 \Befehl{IfSubstanceExistTF}{<id>}\ma{<true code>}\ma{<false code>}
   \cnpkgdocarrow\ Tests if the property \code{<field>} exists and returns either
   \code{<true code>} or \code{<false code>}. This command is expandable.
 \Befehl{IfSubstanceExistT}{<id>}\ma{<true code>} \cnpkgdocarrow\
   Tests if the substance \code{<id>} exists and returns \code{<true code>} if
   it does. This command is expandable.
 \Befehl{IfSubstanceExistF}{<id>}\ma{<false code>} \cnpkgdocarrow\
   Tests if the substance \code{<id>} exists and returns \code{<false code>} if
   it doesn't. This command is expandable.
\end{beschreibung}

\begin{beispiel}
Just to demonstrate how these commands can be used. And to get
our demonstration index filled.\par
\newcounter{substances}
\ForAllSubstancesDo{%
  \ifnum0=\value{substances}\relax
  \else, \fi
  \stepcounter{substances}%
  \chem{#1}%
  \IfSubstancePropertyT{#1}{alt}{
    (\chem*{#1})}}
\end{beispiel}

\section{Create an Index}\label{sec:index}
When \substances is called with \key{index}{true} the command \cmd{chem} will
add index entries each time it is used. In this case the entries of the fields
\key{name}, \key{sort}, \key{alt} and \key{altsort} will be expanded during the
process. You should keep that in mind if some error arises. It might be due to
a \cmd{textbf} or similar in your database. In this case you either need to
replace it with some robust command or put a \cmd{noexpand} in front of it.

Alternative names as specified in the \key{alt} also get an index entry with a
reference to the one of the corresponding \key{name} field. The entry of the
\key{name} field in this case gets the \key{alt} name appended in braces.

This behaviour is not customizable for the time being. It is planned for future
versions of this package, though.

As a demonstration an index for all chemicals used in this documentation is
created with the help of the package \paket{imakeidx}.

\subsection{Formatting Commands}
The index entries are formatted with the following commands. You can redefine
them to your needs. If you do make sure they have the same number of required
arguments and are expandable!
\begin{beschreibung}\makeatletter
 \Befehl{SubstanceIndexNameEntry}\code{\#1\#2}\ma{\#1@\#2} \cnpkgdocarrow\
   Formats the name if no \key{alt} field is given. \code{\#1} refers to the
   \key{sort} field entry and \code{\#2} refers to the \key{name} field entry.
 \Befehl{SubstanceIndexNameAltEntry}\code{\#1\#2\#3}\ma{\#1@\#2 (\#3)}
   \cnpkgdocarrow\ Formats the name if also the \key{alt} field is given.
   \code{\#1} refers to the \key{sort} field entry, \code{\#2} refers to the
   \key{name} field entry, and \code{\#3} to the \key{alt} field entry.
 \Befehl{SubstanceIndexAltEntry}\code{\#1\#2\#3}\ma{\#1@\#3|see\#2} \cnpkgdocarrow\
   Formats the entry for the \key{alt} field. \code{\#1} refers to the \key{altsort}
   field entry, \code{\#2} refers to the \key{name} field entry, and \code{\#3}
   to the \key{alt} field entry.
\end{beschreibung}

\subsection{Using makeidx}
Using the option \key{index}{true} with the standard way to create an index will
add the entries \cmd{index}{<name>} to the index. This means you would mix them
with other entries if you have any. Below a sample document is shown that needs
to be compiled with \code{pdflatex}, \code{makeindex} and again with \code{pdflatex}.
\begin{beispiel}[code only]
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[index]{substances}
\LoadSubstances{substances-examples}

\usepackage{makeidx}
\makeindex
\begin{document}

\newcounter{substances}
\ForAllSubstancesDo{%
  \ifnum0=\value{substances}\relax
  \else, \fi
  \stepcounter{substances}\chem{#1}}

\printindex
\end{document}
\end{beispiel}

\subsection{Using splitidx}
Maybe a seperate index for the chemicals will make more sense. In this case you
could use the package \paket{splitidx}. \substances will recognize this and
create \cmd{sindex}[\cmd{jobname}-chem]{<name>} entries each time \cmd{chem} is
used. The sample document below needs to be compiled with \code{pdflatex},
\code{splitindex} and again with \code{pdflatex}.
\begin{beispiel}[code only]
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[index]{substances}
\LoadSubstances{substances-examples}

\usepackage{splitidx}
\makeindex
\newindex[Chemicals]{\jobname-chem}
\begin{document}

\newcounter{substances}
\ForAllSubstancesDo{%
  \ifnum0=\value{substances}\relax
  \else, \fi
  \stepcounter{substances}\chem{#1}}

\printindex[\jobname-chem]
\end{document}
\end{beispiel}

\subsection{Using imakeidx}
Another way to create multiple indexes is the package \paket{imakeidx}. \substances
recognizes its usage and creates index entries \cmd{index}[\cmd{jobname}-chem]{<name>}.
The sample document below needs to be compiled with \code{pdflatex -{}-shell-escape}.
\begin{beispiel}[code only]
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[index]{substances}
\LoadSubstances{substances-examples}

\usepackage{imakeidx}
\makeindex[name=\jobname-chem,title=Chemicals]
\begin{document}

\newcounter{substances}
\ForAllSubstancesDo{%
  \ifnum0=\value{substances}\relax
  \else, \fi
  \stepcounter{substances}\chem{#1}}

\printindex[\jobname-chem]
\end{document}
\end{beispiel}

\section{The Example Database}\label{sec:examples}
The following code shows the example database \code{substances-examples.sub}
that is part of this package.

\lstinputlisting[basicstyle=\ttfamily\footnotesize]{substances-examples.sub}

\appendix
\printindex[\jobname-chem]
\printindex
\end{document}