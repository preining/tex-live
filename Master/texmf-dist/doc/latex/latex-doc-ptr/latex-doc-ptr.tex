\documentclass[10pt]{article}
\usepackage{latex-doc-ptr}

% This document is intended as the result of a "texdoc latex" in TeX Live.  
% The license is Public Domain.  See README file for more.

\begin{document}\thispagestyle{empty}
\section{\LaTeX}

\LaTeX{} is the most popular macro package for \TeX{}.
(A macro package is a set of commands that an author typically uses
to write documents.) 
 
This page does not try to answer all questions about \LaTeX; 
rather  
we suggest some documentation, add-on components, and resources
that a \LaTeX{} user can start with.
We limit our recommendations to freely-available materials,
and you can click on the text to see the documentation on the Internet.
(In case the Internet is not convenient, when the documentation is also 
available
in a typical \TeX{} installation we provide its name in a footnote;
to view it locally using 
\href{http://tug.org/texdoc}{\package{texdoc}},\texdoc{texdoc}
run ``\texttt{texdoc \textit{name}}'' at a command prompt.)




\subsection{Starting out}

The article
\href{http://mirror.ctan.org/info/first-latex-doc/first-latex-doc.pdf}{Getting something out of \LaTeX}
walks a beginner through writing a sample document.
In particular, to use \LaTeX{}, users must install a \TeX{} distribution, 
such as 
\href{http://miktex.org/}{MiK\TeX{}} on Windows, or  
\href{http://www.tug.org/texlive/}{\TeX{}~Live} on
a Unix system such as GNU/Linux or on Windows,
or 
\href{http://www.tug.org/mactex/}{Mac\TeX{}} on Macintosh OS~X.





\subsection{Documentation}

\subsubsection{Essentials}
The most widely-recommended introduction is 
\href{http://mirror.ctan.org/info/lshort/}
 {\package{The Not-So Short Guide to \LaTeXe}}.\texdoc{lshort}
Another good one is from the
\href{http://tug.org/tutorials/tugindia/}{\package{TUGIndia user group}}.
For typesetting mathematics, use the
the American Mathematical Society's  
AMS-\LaTeX{} package, 
introduced in the primer,
\href{http://mirror.ctan.org/info/amslatex/primer/amshelp.pdf}
 {\package{Getting up and running with AMS-\LaTeX}}.\texdoc{amshelp}


\subsubsection{References}  
The official \LaTeX{} documentation from the development team 
is 
\href{http://mirror.ctan.org/macros/latex/doc/usrguide.pdf}
 {\package{\LaTeXe{} for Authors}};\texdoc{usrguide} this focuses on
changes made in recent versions of \LaTeX{}.
The nearest thing to a general reference manual for \LaTeX{} is the unofficial
\href{http://mirror.ctan.org/info/latex2e-help-texinfo/latex2e.html}
 {\package{\LaTeX: Structured documents for \TeX}}.\texdoc{latex2e}
Look for symbols in the
\href{http://mirror.ctan.org/info/symbols/comprehensive/symbols-letter.pdf}
 {\package{Comprehensive List of Symbols}}.\texdoc{comprehensive}
A two-page
\href{http://mirror.ctan.org/info/latexcheat/latexsheet.pdf}
 {\package{\LaTeX{} Cheat Sheet}}\texdoc{latexcheat} is available.
The document 
\href{http://mirror.ctan.org/info/l2tabu/}
 {\textit{l2tabu}}\texdoc{l2tabuen}
can help you to acquire sound habits by
suggesting what you should consider taboo.


\subsubsection{FAQ's}
Many web pages offer help with \TeX{} and \LaTeX{}.
Particularly useful is the
\href{http://www.tex.ac.uk/faq}{\package{English FAQ}}\texdoc{faq} 
and the 
\href{http://tug.org/interest.html}{\package{TUG web resources page}}.
The \href{http://tug.org/pracjourn/}{\package{Prac\TeX{} Journal}}
is an online magazine aimed at beginning and intermediate users, and
\href{http://tug.org/TUGboat/}{\package{TUGboat}}
has published many articles at all levels nearly since the inception of 
\TeX{}.

\subsubsection{Books}
There are many books about \LaTeX{}; 
visit the 
\href{http://tug.org/books/}
 {\package{\TeX{} Users Group Bookstore}} for discounts.



\subsection{Selected \LaTeX{} packages}

\subsubsection{Page size and shape}
Adjust the page dimensions 
and orientation with  
\href{http://mirror.ctan.org/macros/latex/contrib/geometry/geometry.pdf}
 {\package{geometry}}.\texdoc{geometry}
Control headers and footers with 
\href{http://mirror.ctan.org/macros/latex/contrib/fancyhdr/fancyhdr.pdf}
 {\package{fancyhdr}}.\texdoc{fancyhdr}

\subsubsection{Graphics}
Import graphics into a \LaTeX{} document with the \LaTeX{} team's  
\href{http://mirror.ctan.org/macros/latex/required/graphics/}
 {\package{graphicx}} package, and the related \textit{graphics}.
The official documentation is  
\href{http://mirror.ctan.org/macros/latex/required/graphics/grfguide.pdf}
 {\package{Packages in the `graphics' bundle}}.\texdoc{grfguide}
Another package in the same bundle is \package{color}.\texdoc{color}
For even more color capability use
\href{http://mirror.ctan.org/macros/latex/contrib/xcolor/xcolor.pdf}
 {\package{xcolor}}.\texdoc{xcolor}
An excellent introduction to using these is the article
\href{http://tug.org/pracjourn/2005-3/hoeppner/hoeppner.pdf}
 {\package{Strategies for including graphics in \LaTeX{} documents}}. 
% not free: 
% \href{http://www.ctan.org/tex-archive/info/epslatex/}{\textit{Using Imported Graphics in \LaTeX{} and pdf\LaTeX{}}}.


\subsubsection{Index and bibliography}
Make an index with \href{http://mirror.ctan.org/macros/latex/base/makeindx.dtx}
 {\package{makeidx}}.\texdoc{makeindex}
For bibliographies, people use 
\href{http://en.wikipedia.org/wiki/BibTeX}{\package{B{\footnotesize IB}\TeX{}}}.\texdoc{bibtex}
Two powerful tools based on it are: produce your bibliography
in a natural science styles with 
\href{http://mirror.ctan.org/macros/latex/contrib/natbib/natbib.pdf}
 {\package{natbib}},\texdoc{natbib}
and generate your own style by answering a sequence of questions with 
\href{http://mirror.ctan.org/macros/latex/contrib/custom-bib/makebst.pdf}
 {\package{custom-bib}}.\texdoc{custom-bib}

\subsubsection{Computer code and commenting out}
For computer code, look at
\href{http://mirror.ctan.org/macros/latex/contrib/listings/listings.pdf}
 {\package{listings}}.\texdoc{listings}
The 
\href{http://mirror.ctan.org/macros/latex/required/tools/verbatim.pdf}
 {\package{verbatim}}\texdoc{verbatim}
package is also useful for computer code, and 
includes a \texttt{comment} environment to suppress parts of the document.

\subsubsection{Hypertext}
The 
\href{http://mirror.ctan.org/macros/latex/contrib/hyperref/hyperref.pdf}
 {\package{hyperref}}\texdoc{hyperref}
package gives you 
hyper-document features, such as making table of contents 
entries link to the corresponding document part.
If you don't need active links, typeset web addresses with 
\href{http://mirror.ctan.org/macros/latex/contrib/misc/url.sty}
  {\package{url}},\texdoc{url}
which also does computer file names.

\subsubsection{Presentations}
You can get presentation slides by
adjusting the page geometry and writing a regular document.
For more sophisticated effects use
\href
 {http://mirror.ctan.org/macros/latex/contrib/beamer/doc/beameruserguide.pdf}
 {\package{beamer}}.\texdoc{beamer}
The article
\href{http://tug.org/pracjourn/2005-4/mertz/mertz.pdf}
  {\package{Beamer by example}} will get you started.



\subsection{Output and fonts}

\subsubsection{Output}
The 
\href{http://tug.org/applications/pdftex/}{\package{pdf\TeX}}\texdoc{pdftex}
program extends \TeX{}:
it can directly produce web-friendly PDF files, as well as the
traditional DVI format.
For instance,  this document was generated under \TeX{}~Live with 
\verb!pdflatex latex_doc_ptr.tex!.
A further extension to that, 
\href{http://en.wikipedia.org/wiki/XeTeX}{\package{\xetex}},\texdoc{xetex}
can use fonts from your underlying computer platform,
in addition to the fonts from your \TeX{} distribution.
(Mathematics requires much special tuning, though, so most
system fonts cannot be used for math.)


\subsubsection{Fonts}
The font system documentation from the \LaTeX{} developers is 
\href{http://mirror.ctan.org/macros/latex/doc/fntguide.pdf}
 {\package{\LaTeXe{} font selection}}.\texdoc{fntguide}
To move beyond \TeX's default fonts, these two
documents describe some reasonable and free alternatives:
\href{http://mirror.ctan.org/info/Free_Math_Font_Survey/en/survey.html}
 {\package{A Survey of Free Math Fonts for \TeX{} and \LaTeX{}}}
and 
\href{http://www.tug.dk/FontCatalogue/}{\package{The \LaTeX{} Font Catalogue}}.
More is on the 
\href{http://tug.org/fonts/}{\TeX{} Users Group's font page}.



\subsection{Tools for composing \LaTeX{}}

There are many environments to make writing \LaTeX{} source easier.
For instance, many people use a text editor of some sort, such as 
Emacs with the add-on mode 
\href{http://www.gnu.org/software/auctex/}{\package{AUC-\TeX{}}}.
A new environment that is free, runs on all major 
computer platforms, and combines the best ideas 
from available environments while retaining simplicity, is 
\href{http://tug.org/texworks/}{\texworks}.


\subsection{Community}

There are many
\href{http://tug.org/usergroups.html}{user groups}
for \TeX{}.
The 
\href{http://www.ctan.org/}{Comprehensive \TeX{} Archive Network}
contains many more packages than any distribution.
In addition, if you are stuck on an issue, the Usenet group
\href{http://groups.google.com/group/comp.text.tex}{\texttt{comp.text.tex}} 
and 
\href{http://lists.tug.org/texhax}{\texttt{texhax@tug.org}} 
are the most popular mailing lists. 
You can search more than a decade of 
\LaTeX{} discussions, or post a question yourself.


\subsection{Miscellaneous}

\subsubsection{History}
\LaTeX{} was first written in 1985 by Leslie Lamport, building on 
Donald Knuth's \TeX{}.
It is now maintained and developed by the 
\href{http://www.latex-project.org}{\LaTeX{}3 Project}\texdoc{latex3} group.

\subsubsection{Pronunciation}
\LaTeX{} can be pronounced as
``la-tech'' or ``lay-tech,'' with emphasis on either syllable.
(We prefer the first, with emphasis on the first syllable.)

\end{document}
