%% 
%%  Ein Beispiel der DANTE-Edition
%% 
%%  Beispiel 03-11-20 auf Seite 142.
%% 
%%  Copyright (C) 2011 Herbert Voss
%% 
%%  It may be distributed and/or modified under the conditions
%%  of the LaTeX Project Public License, either version 1.3
%%  of this license or (at your option) any later version.
%% 
%%  See http://www.latex-project.org/lppl.txt for details.
%% 
%% 
%% ==command bibtex8 ++FILE++== 
% Show page(s) 1,2
%% 
\documentclass[]{article}
\pagestyle{empty}
\setlength\textwidth{172.40707pt}
\usepackage[T1]{fontenc}
\usepackage[paper=a6,pagesize,DIV15]{typearea}
\usepackage[ngerman]{babel}
\AtBeginDocument{\parindent=0pt}
\usepackage[utf8]{inputenc}
\usepackage[autostyle]{csquotes}

\usepackage{ragged2e}  \usepackage[style=authoryear]{biblatex}
\renewcommand*{\finentrypunct}{}%         abschliessenden Punkt entfernen
\renewcommand*{\newunitpunct}{\addspace}% Punkte entfernen
\DeclareNameFormat{sortname}{%            Namensformatierung
  \usebibmacro{name:last-first}{#1}{#4}{#5}{#7}%
  \ifnumequal{\value{listcount}}{1}%      1. Name??
    {\ifblank{#3#5}{}{\usebibmacro{name:revsdelim}}}{}
  \usebibmacro{name:andothers}}
\DeclareFieldFormat[article]{title}{#1.\isdot}% keine Anfuehrungsstriche und .
\DeclareFieldFormat{title}{\mkbibemph{#1}.}%    Punkt hinter Titel
\DeclareFieldFormat[article]{number}{\mkbibparens{#1}}% (No)
\DefineBibliographyStrings{ngerman}{in={In}}%   Danch Gro\T1\ss  weiter
\renewbibmacro*{publisher+location+date}{%      Reihenfolge aendern
  \printlist{publisher}\setunit*{\addcomma\space}\printlist{location}\newunit}
\renewbibmacro*{journal+issuetitle}{% Punkt zwischen Volume und Number entfernen
  \usebibmacro{journal}\setunit*{\addspace}%
  \iffieldundef{series}{}{\newunit\printfield{series}\setunit{\addspace}}%
  \printfield{volume}\printfield{number}\setunit{\addcomma\space}%
  \printfield{eid}\setunit{\addspace}%
  \usebibmacro{issue+date}\setunit{\addcolon\space}\usebibmacro{issue}\newunit}
\renewbibmacro*{author}{% keine Autoren zusammengefasst werden
  \ifthenelse{\ifuseauthor\AND\NOT\ifnameundef{author}}
    {\usebibmacro{bbx:savehash}%
     \printnames{author}%
     \iffieldundef{authortype}{\setunit{\addspace}}{\setunit{\addcomma\space}}%
     \iffieldundef{authortype}{}{\usebibmacro{authorstrg}\setunit{\addspace}}}%
    {\global\undef\bbx@lasthash\usebibmacro{labeltitle}\setunit*{\addspace}}%
  \usebibmacro{date+extrayear}}
\renewbibmacro*{editor}{\usebibmacro{bbx:editor}{editorstrg}}
\renewbibmacro*{editor+others}{\usebibmacro{bbx:editor}{editor+othersstrg}}
\renewbibmacro*{bbx:editor}[1]{%
  \ifthenelse{\ifuseeditor\AND\NOT\ifnameundef{editor}}
    {\printnames{editor}\setunit{\addcomma\space}\usebibmacro{bbx:savehash}%
     \usebibmacro{#1}\clearname{editor}\setunit{\addspace}}%
    {\global\undef\bbx@lasthash\usebibmacro{labeltitle}\setunit*{\addspace}}%
  \usebibmacro{date+extrayear}}
\DefineBibliographyStrings{ngerman}{bibliography = {Literaturverzeichnis}}
\bibliography{buch3}

\begin{document}
\nocite{*}
\begingroup\RaggedRight\printbibliography[maxnames=5]\endgroup
\end{document}
