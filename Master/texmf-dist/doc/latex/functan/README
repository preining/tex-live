   This is the README file for funcan,
	       a LaTeX2e package for functional analysis

	   Copyright (C) 2004 Antoine Lejay 


* Copyright
===========

This file is part of functan (version 1.0).

functan may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.1
of this license or (at your option) any later version.
The latest version of this license is in
http://www.latex-project.org/lppl.txt
and version 1.1 or later is part of all distributions of LaTeX
version 1999/06/01 or later.

functan (version 1.0) consists of the following files:

- README (this file)
- functan.ins
- functan.dtx

* Description
=============

The goal of this package is to provide a convenient 
and coherent way to deal with name of functional spaces
(mainly Sobolev spaces) in functional analysis and PDE theory.
It also provides a set of macros for dealing with
norms, scalar products and convergence with some
object oriented flavor (it gives the possibility 
to override the standard behavior of norms, ...).

* Requirement
=============

functan needs the amsmath package version 2.0 or higher.

* Installation
==============

latex functan.ins
latex functan.dtx
makeindex -s gglo.ist -o functan.gls functan.glo
latex functan.dtx

* History
=========

15 July 2004 - first release

--
