# fichier principal par défaut
@default_files = main;

# compilation via pdflatex uniquement
$pdf_mode = 1;

# génération de l'index avec fichier de style .ist
$makeindex = 'makeindex -s %R.ist %O -o %D %S';

# génération du glossaire avec fichier de style .gst
add_cus_dep('glo', 'gls', 0, 'makeglo2gls');
sub makeglo2gls {
  system("makeindex -s '$_[0]'.gst -o '$_[0]'.gls '$_[0]'.glo");
}

# fichiers temporaires additionnels à effacer
$clean_ext = "bbl gls glg glo bak backup %R-blx.bib run.xml ist gst synctex.gz";
$cleanup_includes_cusdep_generated = 1;


