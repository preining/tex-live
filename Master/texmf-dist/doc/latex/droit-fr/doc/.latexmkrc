# fichier principal par défaut
#@default_files = (’main’);

# compilation via pdflatex uniquement
$pdf_mode = 1;
$dvi_mode = 0;

# génération de la bibliographie
$bibtex = 2;

# génération de l'index avec fichier de style .ist
$makeindex  = 'makeindex -s main.ist %O -o %D %S';

# génération du glossaire avec fichier de style .gst
add_cus_dep('glo', 'gls', 0, 'makeglo2gls');
sub makeglo2gls {
  system("makeindex -s main.gst -o '$_[0]'.gls '$_[0]'.glo");
}

# fichiers temporaires additionnels à effacer
$clean_ext = "bbl gls glg glo bak backup %R-blx.bib run.xml synctex.gz";
$cleanup_includes_cusdep_generated = 1;


