@echo off

REM  See english abstract on the first lines of bibarts.tex.

REM  Zur Bearbeitung von bibarts.tex soll dreimal
REM        gbib209 bibarts
REM  eingegeben werden, falls LaTeX mit glatex gestartet werden soll und
REM        gbib2e bibarts
REM  soll dreimal eingegeben werden, falls LaTeX mit gltx2e gestartet
REM  werden soll.

REM  bibarts 1.3, Timo Baumann, 28.Mar.1998; keinerlei Haftung!


if "%1"=="" goto INFO

if not exist %1.tex goto TEXT_EXISTIERT_NICHT

if exist %1.bar GBIBSORT %1.bar %1.phy -k

if exist C:\EMTEX\IDXSTYLE\GBIBARTS.IST goto GBIBARTS_IST_IST_DA
if exist GBIBARTS.IST goto HIER

echo Die Datei GBIBARTS.IST konnte nicht unter C:\EMTEX\IDXSTYLE\GBIBARTS.IST
echo gefunden werden.
echo .
echo "%1.bar" wird jetzt bearbeitet mit den Kommandos in der Befehls-Zeile:
echo            MAKEINDX -l %1.bar
if exist %1.bar MAKEINDX -l %1.bar
goto WEITER

:GBIBARTS_IST_IST_DA
if exist %1.bar MAKEINDX -s C:\EMTEX\IDXSTYLE\GBIBARTS.IST -g -l %1.bar
goto WEITER

:HIER
if exist %1.bar MAKEINDX -s GBIBARTS.IST -g -l %1.bar
goto WEITER

:WEITER

call GLATEX %1.tex

goto ENDE

:INFO
echo -------------------------------------------------------------------------
echo  BIBARTS 1.3: %0 = Start von LaTeX mit GLATEX.BAT.
echo     Zur Bearbeitung von BIBARTS.TEX soll dreimal eingegeben werden:
echo               gbib209   bibarts
echo     Hiermit werden GLATEX.BAT, GBIBSORT.EXE und MAKEINDX.EXE 
echo     (mit Index-STyle File GBIBARTS.IST) aufgerufen.
echo     GBIBARTS.IST soll nach C:\EMTEX\IDXSTYLE kopiert werden.
echo  Allgemein k"onnen Texte bearbeitet werden durch Eingabe von
echo               gbib209  [ LaTeX-File.TEX [ ohne .TEX angeben ] ]
echo .
echo  gbib2e.bat, gbib209.BAT und GBIBSORT.EXE sollten in ein Verzeich-
echo  nis kopiert werden, das sich im PATH des Betriebsystems befindet.
echo -------------------------------------------------------------------------

goto ENDE

:TEXT_EXISTIERT_NICHT
echo Eine Datei "%1.tex" existiert nicht. Bearbeitet werden k"onnen:
dir *.tex /B


:ENDE
