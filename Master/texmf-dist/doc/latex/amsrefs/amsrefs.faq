amsrefs.faq  [2007/10/16] American Mathematical Society

Frequently asked questions about the amsrefs package and its companion
packages.

Suggestions? Please send mail to tech-support@ams.org.

Copyright 2004, 2007, 2008, 2010 American Mathematical Society.

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3c
of this license or (at your option) any later version.
The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3c or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is the American Mathematical
Society.

American Mathematical Society
Technical Support
Publications Technical Group
201 Charles Street
Providence, RI 02904
USA
tel: (401) 455-4080
     (800) 321-4267 (USA and Canada only)
fax: (401) 331-3842
email: tech-support@ams.org

------------------------------------------------------------------------
1) Where should I look for documentation?

User documentation is found in the file amsrdoc.pdf.

Some example files (cite-xa.tex, cite-xb.tex, cite-xh.tex, cite-xs.tex
and gktest.ltb) are also provided.

2) How do I define my own bibliography style?

For information on customizing the formatting of references, see
amsrefs.pdf, especially sections 4, 6.10 and 6.27.

3) I'm trying to use amsrefs with hyperref, but various bad things
   are happening.

Unlike most packages, which must be loaded before hyperref, amsrefs
must be loaded *after* hyperref:

    \usepackage{hyperref}
    \usepackage{amsrefs}

4) When I put

       review={MR0113214 (22 \#4052)},

   in a \bib, there is no space between the review number and the
   preceding period in the printed output.

You must use the \MR macro to mark MR reviews:

       review={\MR{0113214 (22 \#4052)}},

This will ensure the spacing is correct and also allow you to use the
msc-links option.
