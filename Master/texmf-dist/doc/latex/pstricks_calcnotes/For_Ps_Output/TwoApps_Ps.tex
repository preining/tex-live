\documentclass[11pt,a4paper,oneside]{article}
\usepackage{lmodern}
\usepackage[T1,T5]{fontenc}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage[dvips]{geometry}
\usepackage{pstricks}
\usepackage{graphicx}
\usepackage{graphics}
\usepackage{pst-plot}
\usepackage{pst-node}
\usepackage{multido}
\usepackage{pst-xkey}
\usepackage{pst-func}
\usepackage[dvips,colorlinks,linktocpage]{hyperref}
\usepackage{pstricks-add}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\RiemannSum#1#2#3#4#5#6#7#8#9{%
\psplot[linecolor=blue]{#1}{#2}{#3}
\pscustom[linecolor=red]{%
\psline{-}(#1,0)(#1,0)
\multido{\ni=#5,\ne=#6}{#4}
{\psline(*{\ni} {#8})(*{\ne} {#9})}}
\multido{\ne=#6,\nc=#7}{#4}
{\psdot(*{\nc} {#3})
\psline[linestyle=dotted,dotsep=1.5pt](\nc,0)(*{\nc} {#3})
\psline[linecolor=red](\ne,0)(*{\ne} {#9})}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\vecfld#1#2#3#4#5#6{%
\multido{#2}{#4}
{\multido{#1}{#3}
{\parametricplot[algebraic,arrows=->,linecolor=red]{0}{1}
{\nx+((#5)*t)*(1/sqrt(1+(#6)^2))|\ny+((#5)*t)*(1/sqrt(1+(#6)^2))*(#6)}}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\pagestyle{headings}
\topmargin=-0.6cm
\textwidth=16.7cm
\textheight=23cm
\headheight=2.5ex
\headsep=0.6cm
\oddsidemargin=.cm
\evensidemargin=-.4cm
\parskip=0.7ex plus0.5ex minus 0.5ex
\baselineskip=17pt plus2pt minus2pt
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\catcode`@=11
\renewcommand\section{\@startsection {section}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\normalfont\large\bfseries}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\normalsize\bfseries}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\gdef\acknw{\section*{%
{\acknwname}\markright{\protect\textsl{\acknwname}}}%
\addcontentsline{toc}{section}{\acknwname}}
\gdef\acknwname{Acknowledgment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand\sectionmark[1]{\markright{\thesection. #1}}
\newcounter{lk}
\newenvironment{listof}{\begin{list}{\rm(\roman{lk})}{\usecounter{lk}%
\setlength{\topsep}{0ex plus0.1ex}%
\setlength{\labelwidth}{1cm}%
\setlength{\itemsep}{0ex plus0.1ex}%
\setlength{\itemindent}{0.5cm}%
}}{\end{list}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Two applications of macros in \texttt{PSTricks}\thanks{The author of this package
is Timothy Van Zandt (email address: \texttt{tvz@econ.insead.fr}).}}
\author{Le Phuong Quan\\
\small{(Cantho University, Vietnam)}}
\begin{document}
\maketitle
\tableofcontents
\section{Drawing approximations to the area under a graph by rectangles}
\subsection{Description}

We recall here an application in Calculus. Let $f(x)$ be a function, defined and bounded on
the interval $[a,b]$. If $f$ is integrable (in Riemann sense) on $[a,b]$, then its integration on this interval
is
$$\int_a^bf(x)dx=\lim_{\|P\|\to 0}\sum_{i=1}^nf(\xi_i)\Delta x_i,$$
where $P\colon a=x_0<x_1<\cdots<x_n=b$, $\Delta x_i=x_i-x_{i-1}$, $\xi_i\in[x_{i-1},x_i]$, $i=1,2,\ldots,n$,
and $\|P\|=\max\{\Delta x_i\colon i=1,2,\ldots,n\}$. Hence, when $\|P\|$ is small enough, we may have an
approximation
\begin{equation}\label{eqn1}
I=\int_a^bf(x)dx\approx\sum_{i=1}^nf(\xi_i)\Delta x_i.
\end{equation}
Because $I$ is independent to the choice of the partition $P$ and of the $\xi_i$, we may
divide $[a,b]$ into $n$ subintervals with equal length  and choose $\xi_i=(x_i+x_{i-1})/2$.
Then, $I$ can be approximately seen as the sum of areas of the rectangles with sides
$f(\xi_i)$ and $\Delta x_i$.

We will make a drawing procedure to illustrate the approximation (\ref{eqn1}). Firstly, we establish
commands to draw the \emph{sum\/} of rectangles, like the area under piecewise-constant functions
(called \textsl{step shape\/}, for brevity). The choice here
is a combination of the macros \texttt{\symbol{92}pscustom} (to \emph{join\/} horizontal segments, automatically)
and \texttt{\symbol{92}multido}, of course. In particular, the horizontal segments are depicted within the loop
\texttt{\symbol{92}multido} by
$$\texttt{\symbol{92}psplot[{\it settings}]\{$x_{i-1}$\}\{$x_i$\}\{$f(\xi_i)$\}}$$
The \texttt{\symbol{92}pscustom} will join these segments altogether with the end points
$(a,0)$ and $(b,0)$, to make the boundary of the step shape. Then, we draw the points $(\xi_i,f(\xi_i))$, $i=1,2,\ldots,n$,
and the dotted segments between these points and the points $(\xi_i,0)$, $i=1,2,\ldots,n$, by
\begin{align*}
&\texttt{\symbol{92}psdot[algebraic,\dots](*\{$\xi_i$\} \{$f(x)$\})},\\
&\texttt{\symbol{92}psline[algebraic,linestyle=dotted,\dots]($\xi_i$,$0$)(*\{$\xi_i$\} \{$f(x)$\})},
\end{align*}
where we use the structure \texttt{(*\{{\it value}\} \{$f(x)$\})} to obtain the point $(\xi_i,f(\xi_i))$. Finally, we draw
vertical segments to split the step shape into rectangular cells by
$$\texttt{\symbol{92}psline[algebraic,\dots]($x_i$,$0$)(*\{$x_i$\} \{$f(x-\Delta x_i/2)$\})}$$
\begin{figure}[htbp]
\centering\begin{pspicture}(-2,-2)(3,3.5)
\psset{yunit=0.2}
\psaxes[labelFontSize=$\footnotesize$,Dy=2,ticksize=2.2pt,labelsep=4pt]{->}(0,0)(-2.5,-10)(3.5,15.5)
\psplot[plotpoints=500,algebraic,linecolor=red]{-2}{3}{x^3-2*x^2+6}
\pscustom{
\psline{-}(-2,0)(-2,0)
\multido{\nx=-2.000+0.500,\ny=-1.750+0.500,\ni=-1.500+0.500}{10}
{\psplot[algebraic]{\nx}{\ni}{(\ny)^3-2*(\ny)^2+6}}
\psline{-}(3,0)}
\end{pspicture}
\hskip3em
\begin{pspicture}(-2,-2)(3,3.5)
\psset{yunit=0.2}
\psaxes[labelFontSize=$\footnotesize$,Dy=2,ticksize=2.2pt,labelsep=4pt]{->}(0,0)(-2.5,-10)(3.5,15.5)
\psplot[plotpoints=500,algebraic,linecolor=red]{-2}{3}{x^3-2*x^2+6}
\pscustom{
\psline{-}(-2,0)(-2,0)
\multido{\nx=-2.000+0.500,\ny=-1.750+0.500,\ni=-1.500+0.500}{10}
{\psplot[algebraic]{\nx}{\ni}{(\ny)^3-2*(\ny)^2+6}}
\psline{-}(3,0)}
\multido{\nx=-2.000+0.500,\ny=-1.750+0.500,\ni=-1.500+0.500}{10}
{\psline[linestyle=dotted,dotsep=1.5pt,dotstyle=o,linecolor=gray](\ny,0)(*{\ny} {x^3-2*x^2+6})
\psdot[dotsize=1.2pt 1,dotstyle=Bo](*{\ny} {x^3-2*x^2+6})}
\end{pspicture}\\[3ex]
\centering\begin{pspicture}(-2,-2)(3,3)
\psset{yunit=0.2}
\psaxes[labelFontSize=$\footnotesize$,Dy=2,ticksize=2.2pt,labelsep=4pt]{->}(0,0)(-2.5,-10)(3.5,15.5)
\psplot[plotpoints=500,algebraic,linecolor=red]{-2}{3}{x^3-2*x^2+6}
\pscustom{
\psline{-}(-2,0)(-2,0)
\multido{\nx=-2.000+0.500,\ny=-1.750+0.500,\ni=-1.500+0.500}{10}
{\psplot[algebraic]{\nx}{\ni}{(\ny)^3-2*(\ny)^2+6}}
\psline{-}(3,0)}
\multido{\nx=-2.000+0.500,\ny=-1.750+0.500,\ni=-1.500+0.500}{10}
{\psline[linestyle=dotted,dotsep=1.5pt,dotstyle=o,linecolor=gray](\ny,0)(*{\ny} {x^3-2*x^2+6})
\psdot[dotsize=1.2pt 1,dotstyle=Bo](*{\ny} {x^3-2*x^2+6})}
\multido{\nx=-2.000+0.500,\ny=-1.750+0.500,\ni=-1.500+0.500}{9}
{\psline(\ni,0)(*{\ni} {(x-0.25)^3-2*(x-0.25)^2+6})}
\end{pspicture}
\caption{Steps to make the drawing procedure.}\label{Fig1}
\end{figure}

We can combine the above steps to make a procedure whose calling sequence consists of main parameters
$a$, $b$, $f$ and $n$, and dependent parameters $x_{i-1}$, $x_i$, $\xi_i$, $f(\xi_i)$ and
$f(x\pm\Delta x_i/2)$. For instant, let us consider the approximations to the integration of $f(x)=\sin x-\cos x$
on the interval $[-2,3]$ in the cases of $n=5$ and $n=20$. Those approximations are given in Figure \ref{Fig2}.

\begin{figure}[htbp]
\centering\begin{pspicture}(-2.5,-3)(3.5,3.01)
\psset{plotpoints=500,algebraic,dotsize=1pt 2,yunit=2}
\psaxes[labelFontSize=$\footnotesize$,Dy=1]{->}(0,0)(-2.5,-1.5)(3.5,1.5)
\RiemannSum{-2}{3}{sin(x)-cos(x)}{5}
{-2.0+1.0}{-1.0+1.0}{-1.5+1.0}
{sin(x+0.5)-cos(x+0.5)}{sin(x-0.5)-cos(x-0.5)}
\end{pspicture}
\hskip4em
\begin{pspicture}(-2.5,-3)(3.5,3.01)
\psset{plotpoints=500,algebraic,dotsize=1pt 2,yunit=2}
\psaxes[labelFontSize=$\footnotesize$,Dy=1]{->}(0,0)(-2.5,-1.5)(3.5,1.5)
\RiemannSum{-2}{3}{sin(x)-cos(x)}{20}
{-2.000+0.250}{-1.750+0.250}{-1.875+0.250}
{sin(x+0.125)-cos(x+0.125)}{sin(x-0.125)-cos(x-0.125)}
\end{pspicture}
\caption{Approximations to the integration of $f(x)=\sin x-\cos x$ on $[-2,3]$.}\label{Fig2}
\end{figure}

In fact, we can make a procedure, say \texttt{RiemannSum}, whose calling sequence is of the form:
$$\texttt{\symbol{92}RiemannSum\{$a$\}\{$b$\}\{$f(x)$\}\{$n$\}\{$x_{\rm ini}$\}\{$x_{\rm end}$\}\{$x_{\rm choice}$\}\{$f(x+\Delta x_i/2)$\}\{$f(x-\Delta x_i/2)$\}},$$
where $x_0=a$ and for each $i=1,2\ldots,n$:
\begin{align*}
x_i&=a+\dfrac{b-a}{n}i,\quad\Delta x_i=x_i-x_{i-1}=\dfrac{b-a}{n},\\
x_{\rm ini}&=x_0+\Delta x_i,\quad x_{\rm end}=x_1+\Delta x_i,\quad x_{\rm choice}=\dfrac{x_{\rm ini}+x_{\rm end}}{2}=\dfrac{x_0+x_1}{2}+\Delta x_i.
\end{align*}
Note that $x_{\rm ini}$, $x_{\rm end}$ and $x_{\rm choice}$ are given in such forms to be
suitable to variable declaration in \texttt{\symbol{92}multido}. They are nothing but
$x_{i-1}$, $x_i$ and $\xi_i$, respectively, at the step $i$-th in the loop.

Tentatively, in \texttt{PSTricks} language, the definition of \texttt{RiemannSum} is suggested to be
\bigskip\hrule
\noindent\begin{tabular}{@{}l}
\verb!\def\RiemannSum#1#2#3#4#5#6#7#8#9{%!\\
\verb!\psplot[linecolor=blue]{#1}{#2}{#3}!\\
\verb!\pscustom[linecolor=red]{%!\\
\verb!\psline{-}(#1,0)(#1,0)!\\
\verb!\multido{\ni=#5,\ne=#6}{#4}!\\
\verb!{\psline(*{\ni} {#8})(*{\ne} {#9})}}!\\
\verb!\multido{\ne=#6,\nc=#7}{#4}!\\
\verb!{\psdot(*{\nc} {#3})!\\
\verb!\psline[linestyle=dotted,dotsep=1.5pt](\nc,0)(*{\nc} {#3})!\\
\verb!\psline[linecolor=red](\ne,0)(*{\ne} {#9})}}!
\end{tabular}\smallskip\hrule
\subsection{Examples}
We just give here two more examples for using the drawing procedure with ease. In the first example, we approximate
the area under the graph of the function $f(x)=x-(x/2)\cos x+2$ on the interval $[0,8]$. To draw the approximation, we try
the case $n=16$; thus $x_0=0$ and for each $i=1,\ldots,16$, we have
$x_i=0.5\,i$, $\Delta x_i=0.5$, $x_{\rm ini}=0.00+0.50$, $x_{\rm end}=0.50+0.50$ and $x_{\rm choice}=0.25+0.50$.
\begin{figure}[htbp]
\centering\begin{pspicture}(0,0)(5.1,6.6)
\psset{plotpoints=500,algebraic,dotsize=1pt 2,unit=0.6}
\RiemannSum{0}{8}{x-(x/2)*cos(x)+2}{16}{0.00+0.50}{0.50+0.50}{0.25+0.50}
{x+0.25-((x+0.25)/2)*cos(x+0.25)+2}{x-0.25-((x-0.25)/2)*cos(x-0.25)+2}
\psaxes[labelFontSize=$\footnotesize$,Dy=1,ticksize=2.2pt,labelsep=4pt]{->}(0,0)(8.5,11)
\end{pspicture}
\vskip0.5ex
\caption{An approximation to the area under the graph of $f(x)=x-(x/2)\cos x+2$ on $[0,8]$.}\label{Fig3}
\end{figure}

To get Figure \ref{Fig3}, we have used the following \LaTeX\ code:
\bigskip\hrule
\noindent\begin{tabular}{@{}l}
\verb!\begin{pspicture}(0,0)(4.125,5.5)!\\
\verb!\psset{plotpoints=500,algebraic,dotsize=2.5pt,unit=0.5}!\\
\verb!\RiemannSum{0}{8}{x-(x/2)*cos(x)+2}{16}{0.00+0.50}{0.50+0.50}{0.25+0.50}!\\
\verb!{x+0.25-((x+0.25)/2)*cos(x+0.25)+2}{x-0.25-((x-0.25)/2)*cos(x-0.25)+2}!\\
\verb!\psaxes[ticksize=2.2pt,labelsep=4pt]{->}(0,0)(8.5,11)!\\
\verb!\end{pspicture}!
\end{tabular}
\smallskip\hrule\smallskip

In the second example below, we will draw an approximation to the integration of $f(x)=x\sin x$ on $[1,9]$.
Choosing $n=10$ and computing parameters needed, we get Figure \ref{Fig4}, mainly by
the command
\begin{align*}
&\texttt{\symbol{92}RiemannSum\{$1$\}\{$9$\}\{$x\sin x$\}\{$10$\}\{$1.00+0.80$\}\{$1.80+0.80$\}\{$1.40+0.80$\}}\\
&\texttt{\{$(x+0.4)\sin(x+0.4)$\}\{$(x-0.4)\sin(x-0.4)$\}}
\end{align*}
in the drawing procedure.
\begin{figure}[htbp]
\centering\begin{pspicture}(0,-2.5)(4.75,4.25)
\psset{plotpoints=500,algebraic,dotsize=1pt 2,unit=0.5}
\RiemannSum{1}{9}{x*sin(x)}{10}%
{1.00+0.80}{1.80+0.80}{1.40+0.80}%
{(x+0.4)*sin(x+0.4)}{(x-0.4)*sin(x-0.4)}
\psaxes[labelFontSize=$\footnotesize$,Dy=1,ticksize=2.2pt,labelsep=4pt]{->}(0,0)(0,-5)(9.5,8.5)
\end{pspicture}
\caption{An approximation to the integration of $f(x)=x\sin x$ on $[1,9]$.}\label{Fig4}
\end{figure}
\section{Drawing the vector field of an ordinary differential equation of order one}
\subsection{Description}
Let us consider the differential equation
\begin{equation}\label{eqn2}
\frac{dy}{dx}=f(x,y).
\end{equation}
At each point $(x_0,y_0)$ in the domain $D$ of $f$, we will put a vector $\mathbf{v}$ with slope
$k=f(x_0,y_0)$. If $y(x_0)=y_0$, then $k$ is the slope of the tangent to the solution curve $y=y(x)$
of (\ref{eqn2}) at $(x_0,y_0)$. The $\mathbf{v}$'s make a \textsl{vector field\/} and the picture
of this field would give us information about the shape of solution curves of (\ref{eqn2}), even
we have not found yet any solution of (\ref{eqn2}).

The vector field of (\ref{eqn2}) will be depicted on a finite grid of points in $D$. This grid is made of
lines, paralell to the axes $Ox$ and $Oy$. The intersectional points of those lines are called \textsl{grid points\/}
and often indexed by $(x_i,y_j)$, $i=1,\ldots,m$, $j=1,\ldots,n$. For convenience, we will use
polar coordinate to locate the terminal point $(x,y)$ of a field vector, with the initial point at
the grid point $(x_i,y_j)$. Then, we can write
\begin{align*}
x&=x_i+r\cos\varphi,\\
y&=y_j+r\sin\varphi.
\end{align*}
Because $k=f(x_i,y_j)=\tan\varphi$ is finite, we may take $-\pi/2<\varphi<\pi/2$.
From $\sin^2\varphi+\cos^2\varphi=1$ and $\sin\varphi=k\cos\varphi$, we derive
$$\cos\varphi=\frac{1}{\sqrt{1+k^2}},\quad\sin\varphi=\frac{k}{\sqrt{1+k^2}}.$$
The field vectors should all have the same magnitude and we choose here that length to be
$1/2$, that means $r=1/2$. Thus, vectors on the grid have their initial points and
terminal ones as
$$(x_i,y_j),\quad \Big(x_i+\frac{1}{2}\cos\varphi,y_j+\frac{1}{2}\sin\varphi\Big).$$

Of macros in \texttt{PSTricks} to draw lines, we select \texttt{\symbol{92}parametricplot}\footnote{\footnotesize
This macro is of ones, often added and updated in the package \texttt{pstricks-add}, the authors:
Dominique Rodriguez (\texttt{dominique.rodriguez@waika9.com}), Herbert Vo\ss\ (\texttt{voss@pstricks.de}).}
for its fitness. We immetiately have the simple parameterization of the vector at the grid point
$(x_i,y_j)$ as
\begin{align*}
x&=x_i+\frac{t}{2}\cos\varphi=x_i+\frac{t}{2\sqrt{1+k^2}},\\
y&=y_j+\frac{t}{2}\sin\varphi=y_j+\frac{tk}{2\sqrt{1+k^2}},
\end{align*}
where $t$ goes from $t=0$ to $t=1$, along the direction of the vector. The macro \texttt{\symbol{92}parametricplot}
has the syntax as
$$\texttt{\symbol{92}parametricplot[{\it settings}]\{$t_{\rm min}$\}\{$t_{\rm max}$\}\{$x(t)$|$y(t)$\}},$$
where we should use the option \texttt{algebraic} to make the declaration of $x(t)$ and $y(t)$ simpler
with \texttt{ASCII} code.
\begin{figure}[htbp]
\centering\begin{pspicture}(0,0)(5,5)
\psset{unit=2}
\psaxes[labelFontSize=$\footnotesize$,Dx=0.5,Dy=0.5,labels=none,ticksize=2pt,labelsep=2pt,linewidth=0.5pt]
{->}(0,0)(2.5,2.5)
\psdots[dotstyle=*,dotsize=3pt](1,1)(1.5,1)(1,1.5)
\psline[linewidth=0.3pt](1.5,0.5)(1.5,2)(1,2)(1,0.5)(2,0.5)(2,1.5)
\psline[linewidth=0.3pt](1,1.5)(2,1.5)\psline[linewidth=0.3pt](1,1)(2,1)
\psarc[linewidth=0.5pt,linestyle=dotted,dotsep=1.5pt](1,1){0.5}{-90}{90}
\psarc[linewidth=0.5pt,linestyle=dotted,dotsep=1.5pt](1.5,1){0.5}{-90}{90}
\psarc[linewidth=0.5pt,linestyle=dotted,dotsep=1.5pt](1,1.5){0.5}{-90}{90}
\psline[linewidth=0.8pt]{->}(1,1.5)(1.27,1.92)
\psline[linewidth=0.8pt]{->}(1,1)(1.382,1.322)
\psline[linewidth=0.8pt]{->}(1.5,1)(1.977,1.147)
\rput(1,-0.1){$x_i$}\rput(1.5,-0.1){$x_{i+1}$}\rput(-0.1,1){$y_j$}\rput(-0.2,1.5){$y_{j+1}$}
\end{pspicture}
\caption{Field vectors on a grid.}\label{Fig5}
\end{figure}

From the above description of one field vector, we go to the one of the whole vector field
on the grid in the domain $R=\{(x,y)\colon a\le x\le b,\,c\le y\le d\}$. To determine the grid
belonging to the interior of $R$, we confine grid points to the range
\begin{equation}\label{eqn3}
a+0.25\le x_i\le b-0.25,\quad c+0.25\le y_j\le d-0.25.
\end{equation}
With respect to the indices $i$ and $j$, we choose initial values as $x_1=a+0.25$ and
$y_1=c+0.25$, with increments $\Delta x=\Delta y=0.5$, as corresponding to the length of vectors and the distance
between grid points as indicated in Figure \ref{Fig5}. Thus, to draw vectors at grid points
$(x_i,y_j)$, we need two loops for $i$ and $j$, with $0\le i\le [2m]$, $0\le j\le [2n]$, where
$m=b-a$, $n=d-c$. Apparently, these two loops are nested \texttt{\symbol{92}multido}'s, with variable declaration
for each loop as follows
\begin{align*}
\texttt{\symbol{92}nx}&=\text{initial value}+\text{increment}=x_1+\Delta x,\\
\texttt{\symbol{92}ny}&=\text{initial value}+\text{increment}=y_1+\Delta y.
\end{align*}
Finally, we will replace \texttt{\symbol{92}nx}, \texttt{\symbol{92}ny} by $x_i$, $y_j$ in the
below calling sequence for simplicity.

Thus, the main procedure to draw the vector field of the equation (\ref{eqn2}) on the grid (\ref{eqn3})
is
\begin{align*}
&\texttt{\symbol{92}multido\big\{$y_j=y_1+\Delta y$\big\}\big\{$[2n]$\big\}}\texttt{\bigg\{\symbol{92}multido\big\{$x_i=x_1+\Delta x$\big\}\big\{$[2m]$\big\}}\\
&\quad\texttt{\Big\{\symbol{92}parametricplot[{\it settings}]\{$0$\}\{$1$\}\Big\{$x_i+\frac{t}{2\sqrt{1+\big[f(x_i,y_j)\big]^2}}$\Big|
$y_j+\frac{tf(x_i,y_j)}{2\sqrt{1+\big[f(x_i,y_j)\big]^2}}$\Big\}\bigg\}}
\end{align*}
where we at least use \texttt{arrows=->} and \texttt{algebraic} for \textit{settings}.

We can combine the steps mentioned above to define a drawing procedure, say \texttt{\symbol{92}vecfld},
that consists of main parameters in the order as
\texttt{\symbol{92}nx=}$x_1+\Delta x$, \texttt{\symbol{92}ny=}$y_1+\Delta y$, $[2m]$, $[2n]$, $r$
and $f(\texttt{\symbol{92}nx},\texttt{\symbol{92}ny})$. We may change these values to modify
the vector field or to avoid the vector intersection. However, we often take $\Delta x=\Delta y=r$.
Such a definition is suggested to be
\bigskip\hrule
\noindent\begin{tabular}{@{}l}
\verb!\def\vecfld#1#2#3#4#5#6{%!\\
\verb!\multido{#2}{#4}{\multido{#1}{#3}!\\
\verb!{\parametricplot[algebraic,arrows=->,linecolor=red]{0}{1}!\\
\verb!{\nx+((#5)*t)*(1/sqrt(1+(#6)^2))|\ny+((#5)*t)*(1/sqrt(1+(#6)^2))*(#6)}}}}!
\end{tabular}\smallskip\hrule
\subsection{Examples}
Firstly, we consider the equation that describes an object falling in a resistive medium:
\begin{equation}\label{eqn4}
\frac{dv}{dt}=9.8-\frac{v}{5},
\end{equation}
where $v=v(t)$ is the speed of the object in time $t$. In Figure \ref{Fig6}, the vector field of (\ref{eqn4}) is given
on the grid $R=\{(t,y)\colon 0\le t\le 9,\,46\le v\le 52\}$, together with the graph of the equilibrium solution
$v=49$.
\begin{figure}[htbp]
\centering\begin{pspicture}(0,41.4)(8.55,47.25)
\psset{xunit=0.9,yunit=0.9}
\multido{\ny=46.25+0.50}{13}
{\multido{\nx=0.25+0.50}{18}
{\parametricplot[linewidth=0.5pt,algebraic,arrows=->,arrowinset=0.55,linecolor=red]{0}{1}{\nx+(t/2)*(1/sqrt(1+(9.8-0.2*(\ny))^2))|\ny+(t/2)*(1/sqrt(1+(9.8-(0.2)*(\ny))^2))*(9.8-(0.2)*(\ny))}}}
\psplot[algebraic,linewidth=1.2pt]{0}{9.25}{49}
\psaxes[labelFontSize=$\footnotesize$,ticksize=2.2pt,labelsep=4pt,Dy=1,Dx=1,Oy=46,linewidth=0.7pt]{->}(0,46)(0,46)(9.5,52.5)
\rput(9.5,45.8){$t$}\rput(-0.2,52.5){$y$}
\end{pspicture}
\caption{The vector field of (\ref{eqn4}).}\label{Fig6}
\end{figure}

Figure \ref{Fig6} is made of the following \LaTeX\ code:
\bigskip\hrule
\noindent\begin{tabular}{@{}l}
\verb!\begin{pspicture}(0,46)(9.5,52.5)!\\
\verb!\vecfld{\nx=0.25+0.50}{\ny=46.25+0.50}{18}{12}{0.5}{9.8-0.2*\ny}!\\
\verb!\psplot[algebraic,linewidth=1.2pt]{0}{9}{49}!\\
\verb!\psaxes[Dy=1,Dx=1,Oy=46]{->}(0,46)(0,46)(9.5,52.5)!\\
\verb!\rput(9.5,45.8){$t$}\rput(-0.2,52.5){$y$}!\\
\verb!\end{pspicture}!
\end{tabular}
\smallskip\hrule\smallskip
Let us next consider the problem
\begin{equation}\label{eqn5}
\frac{dy}{dx}=x+y,\quad y(0)=0.
\end{equation}
It is easy to check that $y=e^x-x-1$ is the unique solution to the problem (\ref{eqn5}). We now draw
the vector field of (\ref{eqn5}) and the solution curve\footnote{\footnotesize
We have used ${\rm ch}(1)+{\rm sh}(1)$ for the declaration of $e$, natural base of logarithmic function.} on the grid $R=\{(x,y)\colon 0\le x\le 3,\,0\le y\le 5\}$ in
Figure \ref{Fig7}.
\begin{figure}[htbp]
\centering\begin{pspicture}(0,0)(3.25,5.5)
\psset{unit=1}
\multido{\ny=0.25+0.50}{10}
{\multido{\nx=0.25+0.50}{6}
{\parametricplot[linewidth=0.5pt,algebraic,arrows=->,arrowinset=0.55,linecolor=red]{0}{1}{\nx+(t/2)*(1/sqrt(1+(\nx+\ny)^2))|\ny+(t/2)*(1/sqrt(1+(\nx+\ny)^2))*(\nx+\ny)}}}
\psplot[algebraic,linewidth=1.2pt]{0}{2.15}{(sh(1)+ch(1))^x-x-1}
\psaxes[labelFontSize=$\footnotesize$,Dy=1,Dx=1,ticksize=2.2pt,labelsep=4pt,linewidth=0.7pt]{->}(0,0)(3.5,5.5)
\rput(3.5,-0.2){$x$}\rput(-0.25,5.5){$y$}
\end{pspicture}
\caption{The vector field of (\ref{eqn5}).}\label{Fig7}
\end{figure}

We then go to the logistic equation, which is chosen to be a model for the dependence
of the population size $P$ on time $t$ in Biology:
\begin{equation}\label{eqn6}
\frac{dP}{dt}=kP\Big(1-\frac{P}{M}\Big),
\end{equation}
where $k$ and $M$ are constants, respectively various to selected species and environment.
For specification, we take, for instant, $k=0.5$ and $M=100$. The right hand side of
(\ref{eqn6}) then becomes $f(t,P)=0.5\,P(1-0.01\,P)$. In Figure \ref{Fig8}, we draw the vector field
of (\ref{eqn6}) on the grid $R=\{(t,P)\colon 0\le t\le 10,\,95\le P\le 100\}$ and the equilibrium
solution curve $P=100$. Furthermore, with the initial condition $P(0)=95$, the equation (\ref{eqn6})
has the unique solution $P=1900(e^{-0.5t}+19)^{-1}$. This solution curve is also given in Figure \ref{Fig8}.
\begin{figure}[htbp]
\centering\begin{pspicture}(0,76)(8.4,80.4)
\psset{xunit=0.8,yunit=0.8}
\multido{\ny=95.25+0.50}{10}
{\multido{\nx=0.25+0.50}{20}
{\parametricplot[linewidth=0.5pt,algebraic,arrows=->,arrowinset=0.55,linecolor=red]{0}{1}{\nx+(t/2)*(1/sqrt(1+((0.5)*(\ny)*(1-(0.01)*(\ny)))^2))|\ny+(t/2)*(1/sqrt(1+((0.5)*(\ny)*(1-(0.01)*(\ny)))^2))*((0.5)*(\ny)*(1-(0.01)*(\ny)))}}}
\psplot[algebraic,linewidth=1.2pt]{0}{10.25}{100}
\psplot[algebraic,linewidth=1.2pt]{0}{10.25}{1900/((ch(1)+sh(1))^(-0.5*x)+19)}
\psaxes[labelFontSize=$\footnotesize$,Dy=1,Dx=1,Oy=95,ticksize=2pt,labelsep=4pt,linewidth=0.7pt]{->}(0,95)(0,95)(10.5,100.5)
\rput(10.5,94.8){$t$}\rput(-0.25,100.5){$P$}
\end{pspicture}
\caption{The vector field of (\ref{eqn6}) with $k=0.5$ and $M=100$.}\label{Fig8}
\end{figure}

The previous differential equations are all of seperated variable or linear cases that
can be solved for closed-form solutions by some simple integration formulas. We will consider one more
equation of the non-linear case whose solution can only be approximated by numerical methods.
The vector field of such an equation is so useful and we will use the Runge-Kutta curves (of order $4$)
to add more information about the behaviour of solution curve. Here, those Runge-Kutta curves are depicted by the procedure
\texttt{\symbol{92}psplotDiffEqn}, also updated from the package \texttt{pstricks-add}.

The vector field of the non-linear differential equation
\begin{equation}\label{eqn7}
\frac{dy}{dx}=y^2-xy+1
\end{equation}
will be depicted on the grid $R=\{(x,y)\colon -3\le x\le 3,\,-3\le y\le 3\}$ and the solutions
of Cauchy problems for (\ref{eqn7}), corresponding to initial conditions
\begin{listof}
\item $y(-3)=-1$,
\item $y(-2)=-3$,
\item $y(-3)=-0.4$,
\end{listof}
will be approximated by the method of Runge-Kutta, with the grid size $h=0.2$. It is very easy
to recognize approximation curves, respective to (i), (ii) and (iii) in Figure \ref{Fig9} below.
\begin{figure}[htbp]
\centering\begin{pspicture}(-3.6,-3.6)(4.2,4.2)
\psset{unit=1.2,dotsize=2.6pt}
\vecfld{\nx=-3.00+0.4}{\ny=-3.00+0.4}{16}{16}{0.35}{(\ny)^2-(\nx)*(\ny)+1}
\psplotDiffEqn[linewidth=1.2pt,algebraic,showpoints=true,plotpoints=24,method=rk4]{-3}{1.9}{-1}{(y[0])^2-x*y[0]+1}
\psplotDiffEqn[linewidth=1.2pt,algebraic,showpoints=true,plotpoints=25,method=rk4]{-2}{3}{-3}{(y[0])^2-x*y[0]+1}
\psplotDiffEqn[linewidth=1.2pt,algebraic,showpoints=true,plotpoints=10,method=rk4]{-3}{-0.96}{-0.4}{(y[0])^2-x*y[0]+1}
\psaxes[labelFontSize=$\footnotesize$,Dy=1,Dx=1,ticksize=2.2pt,labelsep=4pt,linewidth=0.7pt]{->}(0,0)(-3,-3)(3.5,3.5)
\rput(3.5,-0.2){$x$}\rput(-0.25,3.5){$y$}
\end{pspicture}
\caption{The vector field of (\ref{eqn7}) and the Runge-Kutta curves.}\label{Fig9}
\end{figure}
\acknw
I am very grateful to
\begin{itemize}
\item Timothy Van Zandt, Herbert Vo\ss\ and Dominique Rodriguez for helping me with
their great works on \texttt{PSTricks}.
\item H\`an Th\'\ecircumflex\ Th\`anh for helping me with his pdf\hskip.03em\LaTeX\ program.
\end{itemize}
\begin{thebibliography}{10}
\bibitem{mot} Dominique Rodriguez \&\ Herbert Vo\ss. \textsl{PSTricks-add, additional macros for PSTricks\/}.
Version 3.05, \url{http://ctan.org/tex-archive/graphics/pstricks/contrib}, 2008
\bibitem{hai} Helmut Kopka \&\ Patrick W. Daly. \textsl{Guide to \LaTeX \/}.
Addison-Wesley, Fourth Edition, 2004, ISBN 0321173856
\bibitem{ba} Timothy Van Zandt. \textsl{User's Guide\/}. Version 1.5,\\
\url{http://ctan.org/tex-archive/graphics/pstricks/base}, 2007
\end{thebibliography}
\end{document}
