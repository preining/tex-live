Dear friends,

To convert a PSTricks-code picture into a graphic file ".pdf", as doing an exercise, run 
the file "convert.tex" by the following steps:

  1) latex convert.tex (then you automatically get the output file "convert-fig1.tex").
  2) latex convert-fig1.tex (then run divps program the dvi output file to get 
             the file "convert-fig1.ps").
  3) call the file "convert-fig1.ps" by the Ghostview progam to convert this file into its 
   eps version (you can rename the result, say "fig1.eps").  
  4) use the eps2pdf program to convert the file "fig1.eps" into the file "fig1.pdf". 


The file "test.pdf" is the graph of the Max probability distribution function. 
Check your file "fig1.pdf" to see this graph!

