\documentclass{barticle}
\begin{document}
\bng
@\section{@ U-po-kRa-mo-^ni-kaa }

@{\rm \textbf{ebong}}@ A-tho-baa @\sh{@ E-bo-^ng } kaa-je laa-g-be _t^aa-_de-r J^aa-raa 
@{\rm bangtex}@ bYa-bo-haa-r ko-re baa-^ng-laa li-kh-^che-n A-tho-baa li-kh-be-n
bo-le sthi-r ko-re-^che-n-.

E-bo-^ng ke ba-laa ^jaa-y @{\rm bangtex}@ E-r @{\rm preprocessor}@ .
E-taa laa-te-k_ koo-d le-khaa-r bY-paa-re koo-no saa-haa-^jYa naa
ko-r-le-O-, baa-^ng-laa le-khaa-r pRo-se-s-taa-ke be-sh khaa-ni-k-taa 
saa-b-lii-l ko-r-_te paa-re . @{\rm ebong}@ E-r muu-l boi-shi-^s^thY-gu-li
E-k-dash-na-ja-re _de-khe ne-O-yaa ^jaa-k :

\begin{enumerate}

@\item@ le-khaa-taa @{\rm source code}@ E-r mo-_too _de-kh-_te naa ho-ye
_di-bYi baa-^ng-laa g-^r-ne-r _de-kh-_te ho-y-, ^je-mo-n fou-j ka-thaa-taa
@{\rm bangtex}@ E taa-I-p ko-r-_te h-be @\verb!\*ph*eouj!@ ki-n_tu @{\rm ebong}@
E shu-_dhu @\verb!fou-j!@ li-kh-le-I ch-a-l-be-.

@\item@ _du-too E-kse-p__-saa-naa-l ke-s ^cha-^raa-, 
( RI kaar _di-_te @{\rm RII}@ AA-r J li-kh-_te @{\rm J}@ ) ^choo-too haa-_te-r 
roo-maa-n h-ro-f bY-b-haa-r ko-re-I sa-b bYa-njo-n le-khaa ^jaa-y : _taa-I mo-ne raa-kha
soo-jaa dash shu-_d_dha ka-raa-O sa-ha-j . E-k-taa na-^r-b-^re goo-^che-r 
si-ntYaa-ks che-ki-^ng E-r bY-b-sthaa-O re-khe-^chi-. 

@\item@ E-k-I chi-nhe-r jo-nYa E-kaa-_dhi-k @{\rm roman symbol}@ AA-^che dash
Je-khaa-ne Je-taa cha-l_-le vaa-loo _de-khaa-y-! @{\rm a}@ A-th-baa @{\rm o}@
_du-too-I g^oo-j hi-se-be kaa-je laa-gaa-noo Jaa-be-, A-rthaa-__t 
@{\rm ka-thaa, ko-tha}@ @$\rightarrow$@ ka-thaa-. 


@\item@ @{\rm \LaTeX}@ le-khaa-r ni-yo-m dash kaa-nu-n-O Ja-thaa-saa-_dhYo sa-ha-j
raa-khaa-r che-^shtaa ko-re-^chi-.   

\end{enumerate}

@\section{@ ni-ya-m dash kaa-nu-n }

pRo-tho-m kaaj ho-loo sh-b_d-taa-ke @{\rm strutural syllable }@ E ve-nge
ne-O-yaa-. ^je-mo-n mu-k_ti = mu + k_ti-, bo-n_du-k = bo + n_du + k-, 
E-I-ra-ka-m-. _du-too @{\rm syllable}@ paa-shaa-paa-shi raa-kh-_te 
ge-le E-k-taa @{\rm hyphen}@ / `` dash '' _di-_te ho-be-, ^je-mo-n-,
raa dash m = @{\rm raa-m}@, ha dash r dash boo dash laa = @{\rm ha-r-boo-laa}@. 
@{\rm syllable}@ le-khaa-r ni-y-m E-I-ra-ko-m-. 

\begin{enumerate}

@\item@ sWa-r-chi-nho E-k-laa bo-s-be-. Je-koo-no sWa-r-chi-nho li-kh-_te ge-le 
laa-g-be kYaa-pi-taa-l roo-maa-n ha-ro-f-, Ja-thaa : E-I ( @{\rm E-I}@ ).   

@\item@ bYa-njo-n + kaa-r : Je-mo-n bi-bi ( @{\rm bi-bi}@ ), bii-r ( @{\rm bii-r}@ )
I-_tYaa-_di-.

@\item@ bYa-njo-n + fa-laa : @{\rm M, Y, R, L, W} E-I p^aa-ch-ti fa-laa-, U-_dhaa-ro-^n
dash pa-_dMo @\verb!pa-_dMo!@, sa-hJYo @\verb!sa-hJYo!@, baa-kYo @\verb!ba-kYo!, 
pRo-mii-laa @\verb!pRo-mii-laa!@, AA-hLaa-_d @\verb!AA-hLaa-_d!@, 
sWa-ro-ba-r^no @\verb!sWa-ro-ba-r^no!@.


@\item@ Ju-k_to-bYa-njo-n : bYa-njo-n _du-too pa-shaa-paa-shi li-kh-_te ha-be-, Ja-thaa
I-n_dRa-lu-p_to ( @\verb!I-n_dRa-lu-p_to!@ ) . Ju-k_to-bYa-njo-n E-r sa-ngge koo-no
kaa-r baa fa-laa baa AA-re-k-ti bYa-njo-n Ju-k_to ka-raa-r ni-y-m huu-bo-huu bYa-njo-n
E-r mo-_to-.

@\item@ @{\rm , ; ! ? .} E-raa E-k-laa-O bo-s-_te paa-re-, 
AA-tho-baa  sha-b_de-r E-k-ti @{\rm syllable }@ hi-se-be Je-_te paa-re-, Je-mo-n 
ka|-baa-bu-, kha|-baa-bu-, ga|-baa-bu-. ( @{\rm ka\verb!|!-baa-bu-, kha\verb!|!-baa-bu-, ga\verb!|!-baa-bu.}@ )

@\item@ cha-n_dRa-bi-n_du AA-tho-baa ha-s_o-n_t bYa-njo-n E-r p-re Joo-g 
ko-r-_te ho-le bYa-njo-n E-r po-re ( Ja-thaa-kRo-me ) @\verb!^!@ baa @\verb!_!@
s^e-te _di-n-, Je-mo-n p^aa-ch ( @\verb!p^aa-ch!@ ) baa kha-t_-kaa ( @\verb!kha-t_-kaa!@ ) .


\end{enumerate} 


@\section{@ @{\rm \LaTeX\ }@ li-kh-_te ho-le } 
\begin{enumerate}

@\item@ laa-I-ne-r shu-ru Ja-_di ho-y @\verb!\\!@ _di-ye _ta-be se laa-I-n te-ks 
faa-I-le-O E-k-I thaa-k-be-.

@\item@ ( laa-I-ne-r pRo-tho-m ) @\verb!#! E-r pa-re Jaa thaa-k-be _taa-I s-raa-so-ri 
te-ks faa-I-le cho-le Jaa-be-. 

@\item@ _du-too @\verb!#AT ... #AT sign!@  E-r mo-_dhYe Jaa le-khaa ho-be _taa-I s-raa-so-ri 
te-ks faa-I-le cho-le Jaa-be-. laa-I-ne-r E-k-_do-m she-^she E-ro-ka-m sha-b_d naa thaa-kaa-I 
ba-n^ch-nii-y-. 

@\item@ A-nYo Je-koo-no si-mbo-l, @{\rm bangtex }@ E-r ni-y-me li-kh-_te ho-be-.

@\item@ baa-^ng-laa-r mo-_dhYe @\verb!-!@ li-kh-_te ge-le li-khu-n @\verb!dash! 
sh-b_d-taa-I-, Je-mo-n : ja-le dash ja-ngg-le ( @\verb!ja-le dash ja-ngg-le!@ )

@\item@ @\verb!#AT! le-khaa-r khu-b _da-r-kaa-r ho-le _taa-r Jaa-y-gaa-y li-khu-n 
@{\rm \texttt{\#}}@@{\rm \texttt{AT}}@ .

\end{enumerate}

@\section{@ ba-r^no-maa-laa }

@\subsection{@ sWa-ro-chi-nho } 

\begin{table}[h]
\bng
\begin{tabular}{clll}
A 	& @\verb!A!@  & ne-I @\\@
AA 	& @\verb!AA!@ & kaa  & @{\rm kaa}@  @\\@
I	& @\verb!I!@  & ki   & @{\rm ki}@   @\\@
II	& @\verb!II!@ & kii  & @{\rm kii}@  @\\@
U	& @\verb!U!@  & ku   & @{\rm ku}@   @\\@
UU	& @\verb!UU!@ & kuu  & @{\rm kuu}@  @\\@
RI	& @\verb!RI!@ & kRII & @{\rm kRII}@ @\\@
E	& @\verb!E!@  & ke   & @{\rm ke}@   @\\@
OI	& @\verb!OI!@ & koi  & @{\rm koi}@  @\\@
O	& @\verb!O!@  & koo  & @{\rm koo}@  @\\@
OU	& @\verb!OU!@ & kou  & @{\rm kou}@  @\\@ 
\end{tabular}
\end{table}

@\subsection{@ bYaa-njo-n } 

#%\begin{table}
\bng
\begin{tabular}{cll}
k 	& 	@\verb!k!@ 	@\\@
kh 	& 	@\verb!kh!@ 	@\\@
g 	& 	@\verb!g!@ 	@\\@
gh 	& 	@\verb!gh!@ 	@\\@
ng 	& 	@\verb!ng!@ 	@\\@
\hline 
ch 	& 	@\verb!ch!@  	@\\@
^ch 	& 	@\verb!^ch!@	@\\@
j 	& 	@\verb!j!@ 	@\\@
jh 	& 	@\verb!jh!@ 	@\\@
_n 	& 	@\verb!_n,^y!@  @\\@
\hline
t 	& 	@\verb!t!@ 	@\\@
^th 	& 	@\verb!^th!@ 	@\\@
d 	& 	@\verb!d!@	@\\@
dh 	& 	@\verb!dh!@	@\\@
^n 	& 	@\verb!^n!@	@\\@
\hline
_t 	& 	@\verb!_t!@	@\\@
th 	& 	@\verb!th!@	@\\@
_d 	& 	@\verb!_d!@	@\\@
_dh 	& 	@\verb!_dh!@	@\\@
n 	& 	@\verb!n!@	@\\@
\hline
p 	& 	@\verb!p!@	@\\@
f 	& 	@\verb!ph,f!@	@\\@
b 	& 	@\verb!b!@	@\\@
bh 	& 	@\verb!v,bh!@	@\\@
m 	& 	@\verb!m!@ & @\verb!(M)!@ 	@\\@
\hline
^j 	& 	@\verb!^j,J!@ & @\verb!(Y)!@ 	@\\@
r 	& 	@\verb!r!@ & @\verb!(R)!@ 	@\\@
l 	& 	@\verb!l!@ & @\verb!(L)!@ 	@\\@
b 	& 	ne-I & @\verb!(W)!@		@\\@
h 	& 	@\verb!h!@ 	@\\@
\hline
sh 	& 	@\verb!sh!@ 	@\\@
^s 	& 	@\verb!^s!@ 	@\\@
s 	& 	@\verb!s!@	@\\@
\hline
^r 	& 	@\verb!^r!@\ 	@\\@
^rh 	& 	@\verb!^rh!@ 	@\\@
y 	& 	@\verb!y!@ & @\verb!(Y)!@ 	@\\@
^ng 	& 	@\verb!^ng!@ 	@\\@
__t 	& 	@\verb!__t!@ 	@\\@
: 	& 	@\verb!:!@ 	@\\@
\hline
\end{tabular}
#%\end{table}

@\section{@ shoo-_dho-n }

\begin{itemize}

@\item@ A-shu-_d_dh : @\verb!kha|-baa-bu,!@  shu-_d_dho : @\verb!kha|-baa-bu-,!@
@\item@ A-shu-_d_dh : @\verb!\item!@  shu-_d_dho : @\verb!#AT\item#AT!@
@\item@ A-shu-_d_dh : @\verb!ja-le - ja-ngg-le!@  shu-_d_dho : @\verb!ja-le dash ja-ngg-le!@
@\item@ A-shu-_d_dh : @\verb!\section{ pu-no-shcho }!@ shu-_d_dho : @\verb!#AT\section{#AT pu-no-shcho }!@
@\item@ A-shu-_d_dh : @\verb!{\rm hello world}!@ shu-_d_dho : @\verb!#AT{\rm hello world}#AT!@
\end{itemize}

@\section{@ no-mu-naa }

\begin{verse}
_too-maa-r koo-thaa-y _de-sh-? ki-baa pa-ro-maa-_tMo po-ri-cha-y-? @\\@
_tu-mi ^choo-to gha-re bo-se AA-jii-bo-n pa-^raa-shu-noo ka-roo @\\@ 
_too-maa-r saa-maa-nYo AA-y-, _tu-mi sphii-_too-_da-r .. @\\@
\end{verse}

( sha-k_ti cha-ttoo-paa-_dhYaa-y-, A-no-n_to na-kko-_tRo-bii-thii ... ) @\\@

ni-che-, Ja-thaa-kRo-me @{\rm ebong}@ E-bo-^ng @{\rm \emph{Translated} bangtex code}@ . 


\begin{verbatim}
#\begin{verse}
#_too-maa-r koo-thaa-y _de-sh-? ki-baa pa-ro-maa-_tMo po-ri-cha-y-? @#AT\\#AT@
#_tu-mi ^choo-to gha-re bo-se AA-jii-bo-n pa-^raa-shu-noo ka-roo @#AT\\#AT@ 
#_too-maa-r saa-maa-nYo AA-y-, _tu-mi sphii-_too-_da-r .. @#AT\\#AT@
#\end{verse}

#( sha-k_ti cha-ttoo-paa-_dhYaa-y-, A-no-n_to na-kko-_tRo-bii-thii ... )
\end{verbatim}


\begin{verbatim}
#\begin{verse}
#\*t*eamar \*k*eathay \*d*esh? \*k*iba poromatMo po\*r*icoy? \\
#tu\*m*i \*ch*eaTo gho\*r*e bo\*s*e Aajiibon porhashu\*n*ea ko\*r*ea \\
#\*t*eamar samanYo Aay, tu\*m*i s/phii\*t*eador .. \\
#\end{verse}

#( sho\*k/t*i co\*T/T*eapadhYay, Anon/to nokKotRobiithii ... )

\end{verbatim}



\end{document}
