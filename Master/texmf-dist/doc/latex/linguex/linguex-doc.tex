%% linguex-doc.tex version 2.0
%% documentation of linguex.sty version 4.0
%% Author: Wolfgang.Sternefeld@uni-tuebingen.de
%% filedate 2008/06/30
\documentclass{article}
\usepackage{linguexVPagin}
%\usepackage{linguho}
%\usepackage[*]{linguho}
%\documentstyle[linguex]{article}
\sloppy\raggedbottom
%\addtolength{\alignSubExnegindent}{.7em}
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}           
\title{linguex.sty Documentation}
\author{Wolfgang Sternefeld\\
Version 4.0 --- September 2009}
\date{}
\maketitle

\subsection*{Abstract}%
{\tt linguex.sty} is a \LaTeX-tool written for the lazy linguist.  Its 
handling of example numbering, indentations, indexed brackets, and 
grammaticality judgments reduces the effort of formatting linguistic 
examples to a minimum.  It also allows for automatic extraction of a 
handout.

The macro runs with \LaTeXe\ only and requires two additional 
style files: {\tt xspace.sty} from 
the base of  and {\tt cgloss4e.sty} (a modification of the 
midnight gloss macro) for glosses.   

\subsection*{The Three Basic Commands} The linguex macro defines three 
basic commands: \verb-\ex.-, \verb-\a.-, and \verb-\b.-.  The first 
two generate list environments.  The third functions basically like an 
\verb-\item-.  Here is an example.  Type setting

%\begin{samepage}
\begin{verbatim} 
\ex. This is the first level of embedding
    \a. This is the second level
    \b. This is still the second level, but:
        \a. This is the third level
        \b. This is not the end.
        \b. This is the end
\end{verbatim}
will print:

\ex.  This is the first level of embedding
       \a.  This is the second level
       \b.  This is still the second level, but:
             \a. This is the third level
             \b. This is not the end.
             \b. This is the end 

%\end{samepage}
The list environment created by \verb-\ex.- must be closed by a single 
blank line (a \verb-\par-).  The text following this list will not be 
indented.  In order to get a \verb|\parindent|, a \textit{second} 
blank line must be added, immediately following the first.

The commands \verb-\c.-, \verb-\d.-, \verb-\e.-, and \verb-\f.- are 
equivalent copies of \verb-\b.-; they were defined to produce a kind 
of WYSIWYG-effect.

The blank line at the end of \Last simultaneously closes the three 
lists of \Last (opened by \verb|\ex.|, \verb|\a.|, and the embedded 
\verb|\a.|).  To close just one of the embedded lists (introduced by 
\verb|\a.|) put \verb-\z.- at the point of transition from 
level $n$ of the embedding to level $n-1$.  This is exemplified by the 
following example:

%\begin{samepage}
\ex.\a. {\it Government:}\\
       A governs B iff
       \a. A is a governor;
       \b. A m-commands B;
       \c. no barrier intervenes between A and B
       \z.
   \c.    Governors are lexical nodes and tensed I.
       \z.
       (from Haegeman 1991)
       
%
\begin{verbatim}
This is exemplified by the following example:
\ex.\a. {\it Government:}\\
        A governs B iff
         \a. A is a governor;
         \b. A m-commands B;
         \c. no barrier intervenes between A and B
         \z.
    \b.  Governors are lexical nodes and tensed I.
    \z.
    (from Haegeman 1991)
\end{verbatim}%\end{samepage}
%

\subsection*{Cross References}%
can be handled by \verb-\label{...}- and \verb-\ref{...}- in the usual 
manner.  For example, a label between \verb-\ex.- and \verb-\a.- 
stores the main example number for further reference; the same command 
following \verb-\a.- or \verb-\b.- stores the label of a 
(sub-)subexample for further reference.

For cross references in the immediate vicinity of an example, you need 
not \verb|\label| the examples.  \verb-\Next- refers to the following 
example number, and \verb-\NNext- to the next but one.  Reference to 
the previous example is provided by \verb-\Last-; the penultimate one 
is referred to with \verb-\LLast-.

These shorthands always refer to the first level of embedding.  
Reference to subexamples can only be obtained by adding an (optional) argument 
to the above Next- and Last-commands.  For example, saying 
\verb.\NNext[g-ii].  right now yields \NNext[g-ii].  The dash between 
\addtocounter{ExNo}{2}\arabic{ExNo}\addtocounter{ExNo}{-2} and g is 
defined as \verb|\firstrefdash|.  It can be suppressed by 
\verb|\renewcommand{\firstrefdash}{}|.


\subsection*{Footnotes}%
Inside a footnote, the \verb-\Next--command presupposes that the next 
example is still inside the footnote rather than within the main body 
of the document.  Say \verb-\TextNext- to refer to the next example within the 
main text.  (When you intend to make endnotes instead of footnotes, 
all cross references from footnote to text must be handled by 
\verb-\label{...}- and \verb-\ref{...}-.)

In case an \verb-\ex.--environment contains a footnote that contains a 
\verb-\par- (= a blank line --- this will necessarily be the case with a 
footnote containing itself an \verb-\ex.-), the \verb-\par- will be 
misinterpreted as the end of the main text example.  It is therefore 
necessary to split the command \verb-\footnote{...}- into 
\verb-\footnotemark- and \verb-\footnotetext{...}- (see 
\LaTeX-manual).  The footnotetext must be placed outside the 
\verb-\ex.--environment of the main text.

Some style files (e.g.  endnote.sty) modify the definition of 
footnotes (as linguex.sty itself does so in order to let \verb|\ex.| know 
whether or not it is inside a footnote).  Such style files must be 
accommodated to linguex.sty by making sure that \verb-\if@noftnote- is 
set false at the beginning of each footnote (by saying 
\verb-\@noftnotefalse- in the modified footnote definition); otherwise 
you will get the arabic style of example numberings (as being used in 
the main text) rather than the roman numbers (being used inside 
footnotes).


\subsection*{Glosses}%
require {\tt cgloss4e.sty}, which is input by {\tt linguex.sty}.  
Instead of writing \verb.\gll.  (as would be required by the gloss 
macro), one should append a `g' to the last letter of an example 
command, as shown below:

\begin{samepage}\begin{verbatim}
\ex.\a. No gloss
     \bg. This is a first gloss\\
         Dies ist eine erste Glosse\\
                
\exg.  Dies ist nicht die erste Glosse\\
         This is not the first gloss\\
\end{verbatim}
%
\ex.\a. No gloss
       \bg. This is a first gloss\\
          Dies ist eine erste Glosse\\
          
\exg.  *Dies ist nicht die erste Glosse\\
          This is   not  the   first  gloss\\

\end{samepage}
Using \verb.\gll.  still works in principle, but should be avoided, 
for reasons explained in the next section.


\subsection*{Grammaticality Judgments}%
composed out of *, ?, \#, and \%, are automatically prefixed at the
very beginning of a new list.   A standard example like \Next 
looks like this (to increase readability I also removed mathmode from 
subscripting):

\catcode`_=\active
\def_#1{\ifmmode\sb{#1}\else$\sb{#1}$\fi}

\begin{verbatim}
\catcode`_=\active
\def_#1{\ifmmode\sb{#1}\else$\sb{#1}$\fi}
\exg. \%*Wen_i liebt_k seine_i Mutter t_i t_k?\\
        Whom  loves  his   mother\\
        `Who does his mother love?'
\end{verbatim}
\exg. \%*Wen_i liebt_k seine_i Mutter t_i t_k?\\
                Whom  loves  his      mother\\
                `Who does his mother love?'

Automatic prefixing implies that \textbf{nothing} intervenes between the list 
opening command and the judgment.  In particular, labels must {\it 
follow\/} the grammaticality judgment! Likewise, writing 
\verb|\ex.\gll| instead of \verb|\exg.| will have the  effect of not 
prefixing the grammaticality judgment.


\subsection*{Labelled Brackets}%
 \verb-\exi.- identifies ``words'' by looking for space characters 
 between them; it then checks whether the word following a space 
 starts with one of the brackets ``['' or ``]''.  The material between 
 such a bracket and the next space character will be subscripted.  A 
 standard example would be the following:
\begin{verbatim}
\exi. *[CP Wen_i [C$'$ liebt_j [IP [NP seine_i Mutter ]%
      [VP t_i t_j ]]]]
\end{verbatim}
\exi.  *[CP Wen_i [C$'$ liebt_j [IP [NP seine_i Mutter ]%
       [VP t_i t_j ]]]]

Note that above a blank space between {\tt [IP} and {\tt [NP} is 
crucial, otherwise the bracket of {\tt [NP} would be subscripted as 
well.  Note also that a space is required before the grammaticality 
judgment, otherwise the beginning of subscripting cannot be properly 
identified.

As with the gloss macro, grouping surpresses subscripting. 
Consider  the effects of spacing and grouping in \Next:
\begin{verbatim}
\exi.\a.  [[NP Fritz ][ snores ]]S
    \b.   [[NP Fritz][snores]]S
    \c.   [ [NP Fritz ][snores]]S
    \d.   [[NP Fritz {][}snores ]]S
    \e.   {[[+N,--V]} Fritz ][VP snores ] \hfill{} [NP Structure]
    \f.  *[{}[+N,--V] Fritz ][VP snores ] \hfill [NP-Structure]
\end{verbatim}       
\exi.\a.   [[NP Fritz ][ snores ]]S
    \b.       [[NP Fritz][snores]]S
    \c.       [ [NP Fritz ][snores]]S
    \d.       [[NP Fritz {][}snores ]]S
    \e.     {[[+N,--V]} Fritz ][VP snores ] \hfill{} [NP Structure]
    \f.   *[{}[+N,--V] Fritz ][VP snores ] \hfill [NP-Structure]
           
Grouping works the same way as in the gloss macro where it protects 
from glossing.  E.g., a group such as \verb.{][}.  in \Last[d] is 
ignored for subscripting so that the following material is protected 
from automatic subscripting (but note that something like the opposite 
happens in \Last[e] after \verb|\hfill|!).

Note that commands that look ahead and extend over more than one word 
might not work properly unless two or more words are put into a group.  
Suppose we are inside an \verb-\exi.--environment and we want to say 
something like \verb-\b.[Principle C]-.  What will happen?  The space 
between ``\texttt{e}'' and ``\texttt{C}'' will make the subscripting 
device read \verb-\b.[Principle- as a ``word.''  This, unfortunately, 
prevents the argument of \verb-\b.- from being interpreted correctly.  
Solutions are to write \verb-\b.[Principle~C]-, \verb-\b.[{Principle C}]-, 
or \verb-{\b.[Principle C]}-.

Subscripting outside the \verb|\exi.|-environment can be done by using 
\verb-\I- as shown below:

\medskip
\verb|\I[NP Text| $\Longrightarrow$ \I[NP Text,

 \verb.\I($\alpha$ Text.  $\Longrightarrow$ \I({$\alpha$} Text, 

 \verb.\I{$\alpha$}NP Text. $\Longrightarrow$ \I{$\alpha$}NP Text, 

 \verb.\I{Whom}ACC does John\ldots.  $\Longrightarrow$ \I{Whom}ACC 
 does John\ldots

\medskip\noindent
For combining glosses with labelled brackets, 
say \verb-\exig.- or \verb-\exgi.-.  Note that although the 
list-generating commands \verb-\ex.- and \verb-\a.- can be combined in 
any order, all commands that introduce automatic subscripting can be used 
only at the top level, i.e.  cannot be embedded!!!.  Note also that 
$n$ nested \verb-\ex.--commands need $n$ consecutive blank lines 
(\verb.\par.s) to properly identify the end of all lists.

\subsection*{Handouts}%
Since the surface implementation of the list environment does not use 
the begin-end-format, style files that suppress printing between the 
end and the beginning of certain environments (e.g.  {\tt 
xcomment.sty}) could unfortunately not be adopted to produce a handout 
(I didn't see a way of doing so, probably because I didn't understand 
what goes on inside xcomment.sty).

This deficiency might be compensated for by using {\tt linguho.sty}, 
which removes all examples and headings (of sections and subsections) 
from the \LaTeX-document and stores them in a file whose extension is 
.han (namely \textit{jobname}.han).  Putting \verb.\makehandout.  
somewhere at the end of the document (e.g. before the bibliography) will 
print out \textit{jobname}.han at the position of \verb-\makehandout-.  If 
\verb-\section*{...}- and \verb-\subsection*{...}- should also go into 
the handout, say \verb-\usepackage[*]{linguho}-.  Use 
\verb-\maketitle- to get the title into the handout.

Note that all of \LaTeX's \verb|\setlength| and \verb|\settowidth| 
instructions automatically go into \textit{jobname}.han and are 
executed only there.  (The main body of text before 
\verb-\makehandout- is considered irrelevant, but I didn't find a way 
to suppress writing it into the dvi-file.  Any help to solve this 
problem will be appreciated; note that I am not a \TeX-wizzard, only a lazy 
linguist).  

In case you have defined \verb.\active.  characters (e.g.  Umlaut in 
German) these will most likely turn out troublesome (i.e.  these 
commands should normaly be protected).  For a general solution, 
inspect the definition of \verb.\MkOthersSpecial.  in linguho.sty.  
Uncommenting any of the lines there will solve the problem for the 
active character contained in the respective line (sometimes it might 
in addition be necessary to use t1enc.sty or germanb.sty instead of 
german.sty).


\subsection*{Customizing Lengths and Margins}%
The user may modify any of the lengths displayed in the following 
scheme, which also shows their predefined default values:

\setlength{\Exlabelwidth}{0pt}
\setlength{\Exlabelsep}{0pt}
\ex. \label{diagramm}\footnotesize\arraycolsep2pt 
$\begin{array}[t]{@{}l|l|l|l|l@{}}
	 \multicolumn{5}{c}{\backslash{\rm Extopsep} \updownarrow
	 0.66\backslash{\rm baselineskip}}  
\\[1ex]
	\underbrace{\backslash\rm Exindent}_{{\rm 0pt}}& 
	      \fbox{$\backslash$\rm  Exlabelwidth} 
	      \underbrace{\backslash\rm Exlabelsep}_{{\rm 1.3em}} 
	      & {\rm Text ...} &&\\
 &&
 \rm a.  &\rm Text \ldots &   \\
	 && \underbrace{\backslash\rm SubExleftmargin}_{{\rm 2.4em}} 
	 &\rm (i) & \rm Text \ldots  \\
	 &&  & \underbrace{\backslash\rm SubSubExleftmargin}_{{\rm 2em}} 
	 &\rm\ldots text.  \\[2ex]
 \multicolumn{5}{c}{\backslash{\rm Extopsep}\updownarrow 
    0.66\backslash{\rm  baselineskip}}  
    \end{array}$
    \global\alignSubExtrue

The value of \verb.\Exlabelwidth.  is determined by the width of the
current label.  Note that for displaying the table as shown in \Last,
the leftmargin of the list must be zero; accordingly, both
\verb|\Exlabelwidth| and \verb|\Exlabelsep| were set to zero.  To
restore all default values of the lenghts shown in \Last
simultaneously, say \verb-\resetExdefaults-.  The exact behavior of
\verb.\Exlabelwidth.  is explained further below.  With some non
standard fonts the default spacing can be defined only
\verb.\AtBeginDocument.  (for reasons I do not understand), therefore
 the default values for the above lengths are declared  
 \verb.\AtBeginDocument..  In consequence, any
deviation from the default must be specified  
after \verb.\begin{document}.


Some journals require a different arrangement of margins, e.g.\  one 
which aligns the left edges of examples with the left edges of 
subexamples, as shown in \Next and \NNext: 

\resetExdefaults 
\addtolength{\Exlabelsep}{.5em}%
      \addtolength{\alignSubExnegindent}{.5em}
\ex. \label{align}First example:
\a. Subexample1 
    \b. Subexample2
    \a. Subsubexample1 \ldots\global\alignSubExfalse
 
\addtolength{\Exlabelsep}{-.5em}%
\addtolength{\alignSubExnegindent}{-.5em}%
By saying \verb.\alignSubExtrue. before \verb.\begin{document}., 
the sublabels (a. and b. in \Last) 
are given a negative indentation. This is controled by the lengths
\verb.\alignSubExnegindent.  and \verb.\Exlabelsep.,
which can be customized by \verb-\setlength-.  after \verb.\begin{document}..

The spacing between two subsequent examples is defined by 
\verb.\Exredux., whose default is \verb.-\baselineskip..  Saying 
\verb.\resetExdefaults. returns to all defaults as defined below:
\begin{verbatim}
\newcommand{\resetExdefaults}{%
  \setlength{\Exlabelsep}{1.3em}%
  \setlength{\Extopsep}{.66\baselineskip}%
  \setlength{\SubSubExleftmargin}{2.4em}%
  \setlength{\SubExleftmargin}{2em}%
  \setlength{\Exindent}{0pt}%
  \setlength{\Exlabelwidth}{4em}%
  \setlength{\alignSubExnegindent}{\Exlabelsep}%
  \ifalignSubEx\addtolength{\Exlabelsep}{.7em}%
     \addtolength{\alignSubExnegindent}{.7em}\fi
  \setlength{\Exredux}{-\baselineskip}%
}
\end{verbatim}


\subsection*{Customizing Labels}%
The labels of the examples are created by the counters \verb-ExNo-, 
\verb-SubExNo-, and \verb-SubSubExNo- for the three levels of 
embedding respectively; \verb-FnExNo- is used for the first level 
inside footnotes.  These counters are the default labels of the list 
items.  They can be changed anywhere in the document, by using 
\verb.\setcounter.  as usual.

There are a number of further possibilities to generate labels that differ 
from the default.  One is to
use \verb-\a.- without embedding it into \verb|\ex.|. This will yield the 
expected result, namely a list that uses the \verb|SubEx| counter at 
the top-level:

\a. First line
\b. Second line
\c. Third line

Moreover, any of the list commands can take an optional 
argument, whose specification replaces the default label: 
 
\begin{samepage}
\a.[{\bf Principle C} (Chomsky{[81]}): ] An R-expression is free only
      \a.[$\alpha$)] with respect to potential binders
             in A-positions,
      \b.[$\beta$)] within the domain of its chain.
      
\begin{verbatim}
\a.[{\bf Principle C} (Chomsky{[81]}): ] 
      An R-expression is free only
      \a.[$\alpha$)] with respect to potential binders
             in A-positions,
      \b.[$\beta$)] within the domain of its chain.
 \end{verbatim}
\end{samepage}
%
The optional argument must immediately follow ``.'', so don't leave 
space between \verb|\ex.| or \verb|\a.| and ``['' !!!.  By contrast, 
it will often be the case that an example starts with a (labelled) 
bracket like \I[NP .  If so, it is obligatory to put a space 
between ``.'' and  ``['' .

As illustrated above, the topmost \verb#\a.#-command picks up the 
\verb|\Exlabelwidth| of the preceding example.  By contrast, using 
\verb-\ex.[{\it Principle C} ...]- instead of %
\verb-\a.[{\it Principle C} ...]- has the effect of adjusting the 
indentation of the following text to the width of the optional 
argument, as shown below:
  
\ex.[{\bf Principle C} (Chomsky{[81]}): ] An R-expression is free only
      \a.[$\alpha$)] with respect to potential binders
             in A-positions,
      \b.[$\beta$)] within the domain of its chain.
      
Assuming that regular example numbers do not exceed (999), this 
departure from the default behavior only occurs for optional arguments 
whose width exceeds that of (999).

Optional arguments can also be used to simulate other environments. 
E.g., \LaTeX-style itemizing at the top level can be simulated as follows:
\begin{verbatim}
\ex.[\hfill$\bullet$\hfill] Line 1
\b. Line 2
\c. Line 3
\end{verbatim}
\ex.[\hfill$\bullet$\hfill] Line 1
\b. Line 2
\c. Line 3\global\let\oldalph=\alph\global\let\alph=\arabic

Temporarily redefining \verb.\alph., as shown in the following 
example, yields an enumeration:
\begin{verbatim}
\let\oldalph=\alph\let\alph=\arabic
\a.  Text 1 
\b.  Text 2
\c.  Text 3 \global\let\alph=\oldalph
\end{verbatim}

\a.  Text 1 
\b.  Text 2
\c.  Text 3 \global\let\alph=\oldalph\global\alignSubExtrue 

The stylesheet of \textit{English Language 
and Linguistics}  requires examples of the following form:

\setlength{\Exlabelsep}{2em}
\exi.  
[V [V broke_i $\emptyset$ ][VP the vase [V$'$ t_i into 
pieces ]]]

\ex.\def\SubExLBr{(}\def\SubExRBr{)}
\a. It kept warm
      \b. She kept it warm\global\alignSubExfalse


Apart form 
specifying \verb.\alignSubExtrue. before \verb.\begin{document}. 
\Last requires to redefine the brackets of sublabels, as shown below:
\resetExdefaults
\begin{verbatim}
\renewcommand{\SubExLBr}{(}
\renewcommand{\SubExRBr}{)}
\setlength{\Exlabelsep}{2em}
\exi.  [V [V broke_i $\emptyset$ ][VP the vase [V$'$ t_i into 
    pieces~]]]

\ex.\a.  It kept warm 
    \b.  She kept it warm 
\end{verbatim}%
In addition,  \verb|\renewcommand{\firstrefdash}{}|  changes cross 
references from eg.  \Last[b] to \renewcommand{\firstrefdash}{}\Last[b].

The following definitions generate the left (=L) and right (=R) 
environments of example numberings in ordinary text, in footnotes, 
and in cross-references:

\begin{verbatim}
    \newcommand{\ExLBr}{(}
    \newcommand{\ExRBr}{)}
    \newcommand{\FnExLBr}{(}
    \newcommand{\FnExRBr}{)}
    \newcommand{\theExLBr}{(}
    \newcommand{\theExRBr}{)}
    \newcommand{\theFnExLBr}{(}
    \newcommand{\theFnExRBr}{)}
    \newcommand{\SubExLBr}{}
    \newcommand{\SubExRBr}{.}
    \newcommand{\SubSubExLBr}{(}
    \newcommand{\SubSubExRBr}{)}
\end{verbatim}  
For example, one might want to replace \LLast--\Last with
{\def\theExRBr{}\LLast}--{\def\theExLBr{}\Last} by saying

\begin{verbatim}
{\renewcommand{\theExRBr}{}\LLast}--{\renewcommand{\theExLBr}{}\Last}
\end{verbatim}  



\subsection*{$\backslash$\tt Exlabelwidth}%
By default, the value of \verb.\Exlabelwidth.  is determined by the 
width of the example numbering.  More precisely, for labels between 
(1) and (9) the labelwidth is by default the width of the narrowest 
number with two digits plus brackets; accordingly, the 
actual labelwidth is a bit larger than the natural size of the 
label.  Digits may differ in size depending on the font in use, so 
the calculation is done on the fly.
Similarly, \verb.\Exlabelwidth.  has the width of three digits for 
\verb-\theExNo-'s between (10) and (99).  Finally, 
\verb,\Exlabelwidth, has the width of four digits for example numbers 
between (100) and (999).  For even wider labels (usually specified by 
an optional argument, cf.  above), the label retains its natural 
size. I hope that fonts are cooperative by keeping the difference 
between the width of digits minimal. Otherwise you might experience 
weird behavior of  \verb.\Exlabelwidth.

Since the space between the \verb.\Exlabelwidth.  and the text 
remains the same, the transition from (9) to (10) and from (99) 
to (100) will cause a change of indentation, which might look 
ugly, particularly  in handouts.  In order to 
supress the default behavior of \verb|\Exlabelwidth|, it must be 
assigned a particular length.  For example, saying 
\verb.\settowidth{\Exlabelwidth}{(110)}.  will cause the labelwidth of 
all examles from (1) to (99) to be identical.

Since wide labels should still retain their natural size, the user's 
specification of \verb|\Exlabelsep| should not be wider than (1100), 
otherwise the default mechanism is still active, and the value of 
\verb|\Exlabelsep| is ignored.  E.g., saying 
\verb|\setlength{\Exlabelwidth}{3em}| will most likely cause the list 
declaration to take up its default behavior, because ``3em'' is wider 
than ``(1100)''.  (The default value of 
\verb|\Exlabelwidth| is 4em).


\subsection*{cgloss4e.sty}
I found two problems with cgloss4e.sty.  The first relates to AMS-\TeX.
Writing a translation immediated below the glossed material normally
behaves as one would expect, but with AMS-Tex the translation becomes
indented, for reasons I do not understand.  To remidy this, one has to
(re-)define the list indentation, e.g. as shown below (from \LaTeX):

\begin{verbatim}
\makeatletter
\def\@listi{\leftmargin\leftmargini
	    \parsep 5\p@  \@plus2.5\p@ \@minus\p@
	    \topsep 10\p@ \@plus4\p@   \@minus6\p@
	    \itemsep5\p@  \@plus2.5\p@ \@minus\p@}
\let\@listI\@listi
\@listi
\makeatother
\end{verbatim}
The second problem concerns the use of \verb.\gll.  at a position
other than the beginning of a list.  Then the gloss starts a new
paragraph and puts unwanted vspace above and below the list.  One way
to suppress this is to change the flushleft commands used within
cgloss4e.sty into raggedright by saying 
\verb.\renewenvironment{flushleft}{\raggedright}{}..


%\makehandout
\end{document}
