        +-------------------------------------------+
        +                 libgreek                  +
        +  `Libertine/Biolinum Greek in math mode'  +
        +                                           +
        +             jfbu (at) free.fr             +
        +-------------------------------------------+

This is the README file for the LaTeX2e package `libgreek',
version 1.0, 2011/03/14

\usepackage{libgreek} for using the Linux-Libertine font for
the Greek letters in math mode.

\usepackage[biolinum]{libgreek} for using the Biolinum font
instead.

The fonts themselves and their LaTeX support files as
provided in the libertine-legacy (or earlier libertine)
package must be present on the system.

Other options are of the key=value type:

style=TeX: lowercase Greek italic and uppercase upright,
style=ISO: lowercase and uppercase italic,
style=French: lowercase and uppercase upright.

See the pdf documentation for the further options.
Compile or look at libgreekcheck.tex to see the available
Greek macros in math mode.

Copyright (C) 2011, 2012 by Jean-Francois Burnol.

	The files of this package may be distributed and/or modified
	under the conditions of the LaTeX Project Public License,
	either version 1.3 of this license or (at your option) any
	later version.

	The latest version of this license is in
	  http://www.latex-project.org/lppl.txt
	and version 1.3 or later is part of all distributions of
	LaTeX version 2003/12/01 or later.

