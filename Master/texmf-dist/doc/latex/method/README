README for the package method


License
=======
 
The files in this directory (method distribution) are Copyright 1998 1999
Thomas Leineweber, Univerity of Dortmund, Germany


The files associated with the method-package (see below for a list) may be
distributed under the terms of the LaTeX Project Public License, as
described in lppl.txt in the base LaTeX distribution. Either version 1.0
or, at your option, any later version.

ERROR REPORTS
=============

Before you report an error, please check that:

  - the error isn't caused by obsolete versions of other software;
    LaTeX from 1986 is a good candidate ...

  - you use an original version of the package.


  If you think you found a genuine bug please report it together
  with the following information:

  - version of the file

  - version (date!) of your LaTeX

  - a short test file showing the behavior with all unnecessary
    code removed.

  - a transcript (log file) of the session that shows the error.

Please note that it is important to make the file as small as possible
to allow me to find and fix the error soon.

If you have an unchanged version of my package method, you can send me
error reports to

   leineweb@ls6.cs.uni-dortmund.de


Files distributed with this package
===================================

method.ins         This is the installation script that will produce
                   the executable file method.sty when run through LaTeX
                   or TeX
method.dtx         `method' style for LaTeX in docstrip format.
readme             This file
methtest.tex       A test file showing the usage of the package.



Acknowledgements
================

This readme is mostly copied from the readme from the dinbrief package by
K.D. Braune and R. Gussmann

Jean-Pierre Drucbert for the french translation
