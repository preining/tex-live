\documentclass{article}
\usepackage{censor}

\parindent 0in
\parskip 1em

\begin{document}

\vspace*{0in}
\begin{center}
\LARGE The \textsf{censor} Package\\
\small \rule{0in}{1em}Tools for Producing Redacted Documents\\
\large \rule{0in}{2em} Steven B. Segletes\\
steven.b.segletes.civ@mail.mil\\
\rule{0em}{2em}2013/02/19\\
v3.0
\end{center}

\section{Introduction}

The \textsf{censor} package allows a convenient redaction/censor
capability to be employed, for those who need to protect restricted
information, as well as for those who are forced to work in a more
inefficient environment when dealing with restricted information.

Let us assume you have a document for internal use, containing some
restricted (\textit{i.e.}, private, sensitive, proprietary or
classified) information.  To give a very short (fictitious) example:

{\addtolength{\leftskip}{2.3em}
RESTRICTED SOURCE: \hrulefill

\verb|The Liberty missile, with charge diameter (CD) of 80~mm, |\\
\verb|revealed a penetration capability of 1.30 1.19, and |\\
\verb|1.37~CD in three recent tests into armor steel.|

RESTRICTED OUTPUT: \hrulefill

The Liberty missile, with charge diameter (CD) of 80~mm, revealed a
penetration capability of 1.30 1.19, and 1.37~CD in three recent tests
into armor steel.

\hrulefill

}

Censor/redaction capabilities are desirable to sanitize the information.
This would accomplish two things:  allow for wider distribution of the
output and/or allow a less sensitive output to be stored locally with
fewer storage controls.

There are two modes in which the censor package can be used: (i) when
the source (.tex file) remains restricted/secure; and (ii) when the source
(.tex file) is public/unsecure.

\clearpage
\section{Mode I: Restricted/Secure Source (.tex file)}

In this mode, you create the source in a restricted/secure environment,
but would like to produce output that can be more widely distributed, or
stored in a less restricted location.  With the addition of
\verb|\usepackage{censor}| to your document preamble, the \verb|\censor|
command becomes accessible to block out key identifiers:

{\addtolength{\leftskip}{2.3em}
CENSORED RESTRICTED SOURCE: \hrulefill

\verb|The \censor{Liberty} missile, with charge diameter (CD) of|\\
\verb|\censor{80}~mm, revealed a penetration capability of 1.30|\\
\verb|1.19, and 1.37~CD in three recent tests into armor steel.|

CENSORED NO-LONGER-RESTRICTED OUTPUT: \hrulefill

The \censor{Liberty} missile, with charge diameter (CD) of
\censor{80}~mm, revealed a penetration capability of 1.30 1.19, and
1.37~CD in three recent tests into armor steel.

\hrulefill

}

The censored version of the output is (presumably) less sensitive in its
content, and my be stored in or distributed to a less sensitive
environment (\textit{e.g.}, as a hardcopy).  The censored words are not part
of the output document (hardcopy or electronic), even though the space
allocated for the redaction is identical to the original word being
redacted.

The document may also be printed out in its restricted (uncensored)
form, merely with the addition of the \verb|\StopCensoring| command at
the beginning of the document:

{\addtolength{\leftskip}{2.3em}
UNCENSORED RESTRICTED SOURCE: \hrulefill

\verb|\StopCensoring|\\
\verb| |\\
\verb|The \censor{Liberty} missile, with charge diameter (CD) of|\\
\verb|\censor{80}~mm, revealed a penetration capability of 1.30,|\\
\verb|1.19, and 1.37~CD in three recent tests into armor steel.|

UNCENSORED RESTRICTED OUTPUT: \hrulefill

\StopCensoring

The \censor{Liberty} missile, with charge diameter (CD) of
\censor{80}~mm, revealed a penetration capability of 1.30, 1.19, and
1.37~CD in three recent tests into armor steel.

\hrulefill

}

As of version 3.0, the ability to censor ``boxed'' objects like images
and/or tabular environments is made possible with the \verb|\censorbox|
command.  As an example, one could use the following source:

{\addtolength{\leftskip}{2.3em}
UNCENSORED RESTRICTED SOURCE: \hrulefill

\verb|The \censor{Liberty} missile, with charge diameter (CD) of|\\
\verb|\censor{80}~mm, revealed a penetration capability into armor|\\
\verb|steel, as detailed in Table 1.|\\
\verb||\\
\verb|\begin{table}[ht]|\\
\verb|\begin{center}|\\
\verb|\textbf{Table 1. \censor{Liberty} Missile Test Data}\\|\\
\verb|\censorbox{%|\\
\verb|  \small\begin{tabular}{cc}|\\
\verb|  Standoff & Penetration \\|\\
\verb|  (CD) & (CD) \\|\\
\verb|  \hline|\\
\verb|  5.0 & 1.30 \\|\\
\verb|  6.0 & 1.19 \\|\\
\verb|  6.8 & 1.37\\|\\
\verb|  \end{tabular}%|\\
\verb|}|\\
\verb|\end{center}|\\
\verb|\end{table}|\\

\hrulefill

}

in order to turn, what would otherwise be the following restricted
output into a censored, unrestricted table.

{\addtolength{\leftskip}{2.3em}
UNCENSORED RESTRICTED OUTPUT: \hrulefill

The Liberty missile, with charge diameter (CD) of
80~mm, revealed a penetration capability into armor steel, as detailed
in Table 1.

\begin{table}[ht]
\begin{center}
\textbf{Table 1. Liberty Missile Test Data}\\
\small\begin{tabular}{cc}
Standoff & Penetration \\
(CD) & (CD) \\
\hline
 5.0 & 1.30 \\
 6.0 & 1.19 \\
 6.8 & 1.37\\
\end{tabular}
\end{center}
\end{table}

\clearpage CENSORED UNRESTRICTED OUTPUT: \hrulefill

The \censor{Liberty} missile, with charge diameter (CD) of
\censor{80}~mm, revealed a penetration capability into armor steel, as
detailed in Table 1.

\begin{table}[ht]
\begin{center}
\textbf{Table 1. \censor{Liberty} Missile Test Data}\\
\censorbox{%
  \small\begin{tabular}{cc}
  Standoff & Penetration \\
  (CD) & (CD) \\
  \hline
  5.0 & 1.30 \\
  6.0 & 1.19 \\
  6.8 & 1.37\\
  \end{tabular}%
}
\end{center}
\end{table}
\par
\hrulefill

}

Additionally, as of version 2.0, there is provided a block-censor
capability, for redacting larger blocks of text.  This new command
is \verb|\blackout|, and is used in the form 
\verb|\blackout{Block of text}|.  This nice thing about this command is
that it can stretch across line boundaries conveniently (and across
paragraph boundaries, a little less conveniently).  For example,

{\addtolength{\leftskip}{2.3em}
UNCENSORED RESTRICTED SOURCE: \hrulefill


\verb|\blackout{|\\
\verb|The Liberty missile, with charge diameter (CD) of 80~mm,|\\
\verb|revealed a penetration capability of 1.30, 1.19, and 1.37~CD|\\
\verb|in three recent tests into armor steel.}|

CENSORED UNRESTRICTED OUTPUT: \hrulefill

\blackout{%
The Liberty missile, with charge diameter (CD) of
80~mm, revealed a penetration capability of 1.30, 1.19, and
1.37~CD in three recent tests into armor steel.}

\hrulefill

}

There are several caveats regarding the use of \verb|\blackout|.  First,
its argument cannot, without help, extend across paragraph boundaries.
The help needed by \textsf{censor} is to insert the token \verb|\bpar|
on your blank lines that would normally constitute paragraph boundaries,
as shown in the example below:

\verb|\blackout{|\\
\verb|First paragraph to censor.|\\
\verb|	\bpar|\\
\verb|Second paragraph to censor.}|\\

\vspace{-0.7em}%
will appear as

\blackout{
First paragraph to censor.
	\bpar
Second paragraph to censor.}

Secondly, the argument cannot end on so-called ``glue,'' such as a space
or a carriage return.  The following examples are, thus, bad forms of
the \verb|\blackout| command:

\verb|\blackout{Ends in space.  Bad form }|

\verb|\blackout{Ends in carriage return.  Bad form|\\
\verb|}|

Thirdly, periods are not censored (as of version 2.1).  This is done
because it is necessary to preserve the period-space combination that
signifies end-of-sentence.  In this way, {\LaTeX} can preserve spacing
in the redacted version that is identical to the unredacted version.

Fourthly, \verb|\blackout| does not work across environment boundaries,
such as math or tabular mode.  You can blackout text in the table, cell
by cell; But you cannot blackout the whole tabular environment with the
\verb|\blackout| command (instead should use the \verb|\censorbox|
command for tabular environments).

Finally, the argument to \verb|\blackout| can employ tokens; However,
the tokens are expanded into one large blackout, regardless of whether
the expanded token contains spaces.  Thus

\verb|\blackout{\today}|

becomes \blackout{\today}, in spite of the fact that \verb|\today|,
given at the moment by \today, constains spaces.

\clearpage
\section{Mode II: Public/Unsecure Source (.tex file)}

This mode is envisioned for users who must bear a large level of
inconvenience to work in a restricted/secure environment (generally
meaning in a location physically removed from one's desk).  Its use is
envisioned to allow a large percentage of a document to be created in an
public/unsecure environment, with only the restricted details being
completed in a restricted/secure environment.

After including the \verb|\usepackage{censor}| command in the document
preamble, the \verb|\censor*| and \verb|\censorbox*| commands becomes
accessible.  The way this author envisions its use is as follows:

{\addtolength{\leftskip}{2.3em}
CENSORED UNRESTRICTED SOURCE: \hrulefill

\verb|% KEYWORD IDENTIFIERS:|\\
\verb|\def\Missile{\censor*{7}}|\\
\verb|\def\Size{\censor*{2}}|\\
\verb|\def\TableOne{\censorbox*[\small]{26}{5}{3.5}}|\\
\verb||\\
\verb|The {\Missile} missile, with charge diameter (CD) of|\\
\verb|{\Size}~mm, revealed a penetration capability ranging from|\\
\verb|1.19--1.37~CD in three recent tests into armor steel, as|\\
\verb|detailed in Table 1.|\\
\verb||\\
\verb|\begin{table}[ht]|\\
\verb|\begin{center}|\\
\verb|\textbf{Table 1. {\Missile} Missile Test Data}\\|\\
\verb|{\TableOne}|\\
\verb|\end{center}|\\
\verb|\end{table}|\\

CENSORED UNRESTRICTED OUTPUT: \hrulefill

\def\Missile{\censor*{7}}
\def\Size{\censor*{2}}
\def\TableOne{\censorbox*[\small]{26}{5}{3.5}}

The {\Missile} missile, with charge diameter (CD) of {\Size}~mm, revealed a
penetration capability ranging from 1.19--1.37~CD in three recent tests
into armor steel, as detailed in Table 1.

\begin{table}[ht]
\begin{center}
\textbf{Table 1. {\Missile} Missile Test Data}\\
{\TableOne}
\end{center}
\end{table}

\hrulefill

}

In this way, the source (.tex file) contains no restricted information,
and may thus be prepared in an unrestricted environment.  The argument
to the \verb|\censor*| command is a number representing the approximate
width of the redaction (in ex's).  In the case of the \verb|\censorbox*|
command, it has three mandatory arguments and one optional argument
(note that the unstarred form of the \verb|\censorbox| command has not
three, but one, mandatory arguments).  The mandatory arguments to
\verb|\censorbox*| are box width (in ex's), box height (in multiples of
\verb|\baselineskip|) and finally the depth below the baseline where the
bottom of the box should be (in multiples of \verb|\baselineskip|).  The
optional argument can include commands that you want in force for the
\verb|\censorbox| command, most typically fontsize commands, which will
affect the size of the an ex and \verb|\baselineskip|.

Because the redaction width is only approximate, it is possible that the
censored and uncensored originals might have differing text
justification.  In the text, the curly braces are placed around the
keyword identifiers so as to produce the proper interaction with the
surrounding whitespace and punctuation.

Because the source (.tex) file contains no restricted information, the
use of \verb|\StopCensoring| cannot (in and of itself) produce the
restricted document.  Rather it will produce the following:

{\addtolength{\leftskip}{2.3em}
UNCENSORED UNRESTRICTED OUTPUT: \hrulefill

\StopCensoring

\def\Missile{\censor*{7}}
\def\Size{\censor*{2}}
\def\TableOne{\censorbox*[\small]{26}{5}{3.5}}

The {\Missile} missile, with charge diameter (CD) of {\Size}~mm, revealed a
penetration capability ranging from 1.19--1.37~CD in three recent tests
into armor steel, as detailed in Table 1.

\begin{table}[ht]
\begin{center}
\textbf{Table 1. {\Missile} Missile Test Data}\\
{\TableOne}
\end{center}
\end{table}

\hrulefill

}

In order to produce the uncensored, restricted output, the unrestricted
source file must be moved into the restricted/secure environment and be
subject to a small amount of additional editing.  Once in the restricted
environment, the source (.tex file) may be edited such that the censored
keyword identifiers are filled in with their restricted values and, in
the process, changing the \verb|\censor*| and \verb|\censorbox*|
commands to \verb|\censor| and \verb|\censorbox|, respectively.  This
and the addition of the \verb|\StopCensoring| command to the file will
produce the uncensored, restricted result:

\clearpage{\addtolength{\leftskip}{2.3em}
UNCENSORED NO-LONGER-UNRESTRICTED SOURCE: \hrulefill

\verb|\StopCensoring|\\
\verb|\def\Missile{\censor{Liberty}}|\\
\verb|\def\Size{\censor{80}}|\\
\verb|\def\TableOne{\censorbox{%|\\
\verb|  \begin{tabular}{cc}|\\
\verb|  Standoff & Penetration \\|\\
\verb|  (CD) & (CD) \\|\\
\verb|  \hline|\\
\verb|  5.0 & 1.30 \\|\\
\verb|  6.0 & 1.19 \\|\\
\verb|  6.8 & 1.37\\|\\
\verb|  \end{tabular}%|\\
\verb|}}|\\
\verb||\\
\verb|The {\Missile} missile, with charge diameter (CD) of|\\
\verb|{\Size}~mm, revealed a penetration capability ranging|\\
\verb|from 1.19--1.37~CD in three recent tests into armor|\\
\verb|steel, as detailed in Table 1.|\\
\verb||\\
\verb|\begin{table}[ht]|\\
\verb|\begin{center}|\\
\verb|\textbf{Table 1. {\Missile} Missile Test Data}\\|\\
\verb|{\TableOne}|\\
\verb|\end{center}|\\
\verb|\end{table}|\\

UNCENSORED RESTRICTED OUTPUT: \hrulefill

\StopCensoring

\def\Missile{\censor{Liberty}}
\def\Size{\censor{80}}
\def\TableOne{\censorbox{%
  \begin{tabular}{cc}
  Standoff & Penetration \\
  (CD) & (CD) \\
  \hline
  5.0 & 1.30 \\
  6.0 & 1.19 \\
  6.8 & 1.37\\
  \end{tabular}%
}}

The {\Missile} missile, with charge diameter (CD) of
{\Size}~mm, revealed a penetration capability ranging
from 1.19--1.37~CD in three recent tests into armor
steel, as detailed in Table 1.

\begin{table}[ht]
\begin{center}
\textbf{Table 1. {\Missile} Missile Test Data}\\
{\TableOne}
\end{center}
\end{table}

\hrulefill

}

The only changes required of the document were at the very beginning,
among the keyword identifiers.  The primary text of the source document
remained unaltered.  Note also that the censored, unrestricted output
may also be obtained from the no-longer-unrestricted source by removing
the \verb|\StopCensoring| command at the beginning of the file.

It is not envisioned that the \verb|\blackout| command, discussed in the
prior section, is used in this mode, where the source file is in an
unsecure environment.

\clearpage
\section{Summary \& Miscellany}

The complete set of commands available to the \textsf{censor} package,
to bring about text redaction, are:\\
{~\\
\verb|   \censor{|\it text\tt\}\\
\verb|   \censor*{|\it width mult.\tt\}\\
\verb|   \censorbox[|\it pre-commands\tt]\{\it object box\tt\}\\
\verb|   \censorbox*[|\it pre-commands\tt]\{\it width mult.\tt\}\{\it 
height mult.\tt\}\{\it depth mult.\tt\}\\
\verb|   \blackout{|\it extended text passage\tt\}\\
\verb|   \bpar|\\
\verb|   \StopCensoring|\\
\verb|   \RestartCensoring|\\
~\\}
The star ({\tt*}) version of the commands is envisioned when the source
document is being created in an unsecure environment, whereas, the
unstarred version is when the document source may be created in a secure
environment.
In the star ({\tt*}) commands, width multipliers are given in ex's,
whereas height and depth multipliers are given in multiples of
\verb|\baselineskip|.  The depth indicates the distance below the
baseline where the bottom of a boxed object is to be placed (this
command has a more direct effect for inline use, whereas use within
environments is often overridden by the environment).  The use of
pre-commands will typically be fontsize commands, so that measurements
of ex's and \verb|\baselineskip| are done in a relevant fontsize.

The \verb|\bpar| command is provided as a
paragraph separator for use with \verb|\blackout|, for censoring across
paragraph boundaries. 

Censoring may be dynamically turned off and on in a document with the
use of the \verb|\StopCensoring| and \verb|\RestartCensoring| commands,
respectively.  The default is censoring `on.'  

I have found that, in certain cases (for example, with captions created
using the \textsf{boxhandler} package), the censorbox command needs to
be protected by way of \verb|\protect\censorbox{...}|.

It is preferable not to apply \verb|\censor| to whitespace, or text
justification could be adversely affected.  If one wishes to censor a
multi-word phrase, such as ``Little Bo Peep,'' it is recommended to do
it as follows:\\
\verb|   \blackout{Little Bo Peep}|\\
If such a phrase is to be used repeatedly through a document, 
it is most convenient to place it as a keyword identifier:\\
\verb|   \def\Name{\blackout{Little Bo Peep}}|\\
such that subsequent reference is done indirectly:\\
\verb|   We examine the life of {\Name} in this report|

The source code for \textsf{censor} is so short as to be included below:

\verb|\ProvidesPackage{censor}|\\
\verb|[2013/02/19 v3.00|\\
\verb| Provides capability for redaction of sensitive information]|\\
\verb||\\
\verb|\usepackage{pbox}|\\
\verb|\usepackage{ifnextok}|\\
\verb||\\
\verb|\newcommand\censor{\@ifstar{\@cenlen}{\@cenword}}|\\
\verb|  \newcommand\@cenlen[1]{\protect\rule[-.3ex]{#1 ex}{2.1ex}}|\\
\verb|  \newcommand\@cenword[1]{%|\\
\verb|              \protect\rule[-.3ex]{\widthofpbox{#1}}{2.1ex}}|\\
\verb||\\
\verb|\newcommand\un@censor{\@ifstar{\un@cenlen}{\un@cenword}}|\\
\verb|  \newcommand\un@cenlen[1]{\protect\underline{\hspace{#1 ex}}}|\\
\verb|  \newcommand\un@cenword[1]{#1}|\\
\verb||\\
\verb|\newcommand\StopCensoring{%|\\
\verb|           \let\censor\un@censor%|\\
\verb|           \let\censorbox\un@censorbox%|\\
\verb|}|\\
\verb|\newcommand\RestartCensoring{%|\\
\verb|           \renewcommand\censor{\@ifstar{\@cenlen}{\@cenword}}%|\\
\verb|           \renewcommand\censorbox{\@ifstar{\censor@dim}{\censor@box}}%|\\
\verb|}|\\
\verb||\\
\verb|\def\stringend{$}|\\
\verb|\def\blackout#1{\censor@Block#1\stringend}|\\
\verb|\def\censor@Block{\IfNextToken\stringend{\@gobble}%|\\
\verb|  {\IfNextToken\@sptoken{ \bl@t{\censor@Block}}%|\\
\verb|  {\bl@t{\censor@Block}}}}|\\
\verb||\\
\verb|% V2.00 DEFINITION:|\\
\verb|% \def\bl@t#1#2{\censor{#2}#1}|\\
\verb||\\
\verb|% V2.10 DEFINITION:|\\
\verb|\def\bl@t#1#2{\if\bpar#2\par\else\if.#2#2\else\censor{#2}\fi\fi#1}|\\
\verb|\def\bpar{_}|\\
\verb||\\
\verb|% ALTERNATE DEFINITION IF ABOVE PROVES PROBLEMATIC|\\
\verb|%\def\bl@t#1#2{\if.#2#2\else\censor{#2}\fi#1} % JUST PERIODS, NO \bpar|\\
\verb||\\
\verb|\newcommand\censorbox{\@ifstar{\censor@dim}{\censor@box}}|\\
\verb|  \newcommand\censor@dim[4][]{{#1%|\\
\verb|                      \rule[-#4\baselineskip]{#2ex}{#3\baselineskip}}}|\\
\verb|  \newcommand\censor@box[2][]{#1\setbox0\hbox{#2}%|\\
\verb|                      \rule[-\the\dp0]{\the\wd0}{\the\ht0+\the\dp0}}|\\
\verb||\\
\verb|\newcommand\un@censorbox{\@ifstar{\un@censor@dim}{\un@censor@box}}|\\
\verb|  \newcommand\un@censor@dim[4][]{{#1%|\\
\verb|                      \fbox{\rule[-#4\baselineskip]{0ex}{#3\baselineskip}|\\
\verb|                      \rule{#2ex}{0ex}}}}|\\
\verb|  \newcommand\un@censor@box[2][]{#1#2}|\\
\verb||\\
\verb|% NOTE: A \protect\censorbox{} MAY BE REQUIRED INSIDE SOME ENVIRONMENTS|\\
\verb||\\
\verb|\endinput|\\

\end{document}
