\relax
\documentclass[twocolumn]{article}
\usepackage{fontdoc}
\begin{document}
\ProvidesFile{cbgreek.etx}[2004/07/31 v0.2 cbgreek.etx (Greek Encoding File)]

\title{Greek (GRM) Font Encoding}
\author{Luis Rivera\thanks{\textt{jlrn77 at gmail com}.}
\date{December 2004}
\maketitle

\tableofcontents

\section{What is this for}

This file documents (to some extent) the encoding embedded in Claudio Beccari's 
Greek fonts, which are part of the standard distribution of the \textsf{Greek} option of the 
\texttt{Babel} package, maintained by Apostolos Syropoulos.
The purpose of this file is to provide an interface to produce virtual fonts that map to 
or from this set of fonts for their use with \LaTeX.  
This encoding file was originally used to produce virtual fonts to and from the pseudo-beta 
encoding provided by the Silver Mountain Greek fonts developed and commercialized by 
Silver Mountain Software.  
There are plans to restore Beccari's original ligature table in this file.

The production of this software has been supported by a grant from the Consejo 
Nacional de Ciencia y Tecnologia, Mexico.

\encoding

\comment{\section{The Lower Table}}

\comment{The following part of the font occupies the lower part of the table (positions 
00 to 31).  These are usually unaccesible from standard Latin keyboards, so they have to be 
reached to from ligatures and \TeX\ commands.}

\nextslot{'0}
\setslot{endash}
    \ligature{LIG}{hyphen}{emdash}
\endsetslot

\nextslot{'2}
\setslot{BoxedLambda}
\endsetslot

\nextslot{'3}
\setslot{BoxedEta}
\endsetslot

\nextslot{'4}
\setslot{BoxedXi}
\endsetslot

\nextslot{'5}
\setslot{BoxedMu}
\endsetslot

\nextslot{'6}
\setslot{stigma}
\endsetslot

\nextslot{'7}
\setslot{vardigamma}
\endsetslot

%vardigamma

\nextslot{'10}
\setslot{Ios}
\endsetslot

\nextslot{'11}
\setslot{AlphaIos}
\endsetslot

\nextslot{'12}
\setslot{EtaIos}
\endsetslot

\nextslot{'13}
\setslot{OmegaIos}
\endsetslot

\nextslot{'14}
\setslot{AltAlpha}
\endsetslot

\nextslot{'15}
\setslot{AltUpsilondieresis}
\endsetslot

\nextslot{'17}
\setslot{Altupsilondieresis}
\endsetslot

\nextslot{'22}
\setslot{varqoppa}
\endsetslot

\nextslot{'23}
\setslot{qoppa}
\endsetslot

\nextslot{'25}
\setslot{Qoppa}
\endsetslot

\nextslot{'27}
\setslot{Sampi}
\endsetslot

\nextslot{'30}
\setslot{euro}
\endsetslot

\nextslot{'31}
\setslot{permill}
\endsetslot

\nextslot{'33}
\setslot{sampi}
\endsetslot

\nextslot{'34}
\setslot{openquote}
\endsetslot

\nextslot{'35}
\setslot{closequote}
\endsetslot

\nextslot{'36}
\setslot{breve}
\endsetslot

\nextslot{'37}
\setslot{macron}
\endsetslot

\nextslot{'40}
\setslot{circdieresis}
\endsetslot

\nextslot{'41}
\setslot{exclamationmark}
\endsetslot

\nextslot{'42}
\setslot{dieresis}
\endsetslot

\nextslot{'43}
\setslot{acutedieresis}
\endsetslot

\nextslot{'44}
\setslot{gravedieresis}
\endsetslot

\nextslot{'45}
\setslot{percent}
\endsetslot

\nextslot{'46}
\setslot{dot}
\endsetslot

\nextslot{'47}
\setslot{acute}
\endsetslot

\comment{\section{The Middle Table}}
\comment{Most of the following characters are accesible from the standard American keyboard.
Keep in mind though that some of them (like \#, \$, and \%) may still have to be defined as
\LaTeX\ symbols (by means of a \verb|\DeclareTextSymbol|) command in the Greek encoding used 
by your package.}

\nextslot{'50}
\setslot{parenleft}
\endsetslot

\nextslot{'51}
\setslot{parenright}
\endsetslot

\nextslot{'52}
\setslot{asterisk}
\endsetslot

\nextslot{'53}
\setslot{plus}
\endsetslot

\nextslot{'54}
\setslot{comma}
\endsetslot

\nextslot{'55}
\setslot{hyphen}
    \ligature{LIG}{hyphen}{endash}
\endsetslot

\nextslot{'56}
\setslot{period}
\endsetslot

\nextslot{'57}
\setslot{slash}
\endsetslot

\setslot{zero}
\endsetslot

\setslot{one}
\endsetslot

\setslot{two}
\endsetslot

\setslot{three}
\endsetslot

\setslot{four}
\endsetslot

\setslot{five}
\endsetslot

\setslot{six}
\endsetslot

\setslot{seven}
\endsetslot

\setslot{eight}
\endsetslot

\setslot{nine}
\endsetslot

\nextslot{'72}
\setslot{colon}
\endsetslot

\nextslot{'73}
\setslot{semicolon}
\endsetslot

\nextslot{'74}
\setslot{rough}
\endsetslot

\nextslot{'75}
\setslot{equal}
\endsetslot

\nextslot{'76}
\setslot{smooth}
\endsetslot

\nextslot{'77}
\setslot{questionmark}
\endsetslot

\nextslot{'100}
\setslot{roughcirc}
\endsetslot

\setslot{Alpha}
\endsetslot

\setslot{Beta}
\endsetslot

\setslot{roughgrave}
\endsetslot

\setslot{Delta}
\endsetslot

\setslot{Epsilon}
\endsetslot

\setslot{Phi}
\endsetslot

\setslot{Gamma}
\endsetslot

\setslot{Eta}
\endsetslot

\setslot{Iota}
\endsetslot

\setslot{Theta}
\endsetslot

\setslot{Kappa}
\endsetslot

\setslot{Lambda}
\endsetslot

\setslot{Mu}
\endsetslot

\setslot{Nu}
\endsetslot

\setslot{Omicron}
\endsetslot

\setslot{Pi}
\endsetslot

\setslot{Chi}
\endsetslot

\setslot{Rho}
\endsetslot

\setslot{Sigma}
\endsetslot

\setslot{Tau}
\endsetslot

\setslot{Upsilon}
\endsetslot

\setslot{roughacute}
\endsetslot

\setslot{Omega}
\endsetslot

\setslot{Xi}
\endsetslot

\setslot{Psi}
\endsetslot

\setslot{Zeta}
\endsetslot

\nextslot{'133}
\setslot{leftsqbracket}
\endsetslot

\nextslot{'134}
\setslot{smoothcirc}
\endsetslot

\nextslot{'135}
\setslot{rightsqbracket}
\endsetslot

\nextslot{'136}
\setslot{smoothacute}
\endsetslot

\nextslot{'137}
\setslot{smoothgrave}
\endsetslot

\nextslot{'140}
\setslot{grave}
\endsetslot

\setslot{alpha}
\endsetslot

\setslot{beta}
\endsetslot

\setslot{varsigma}
\endsetslot

\setslot{delta}
\endsetslot

\setslot{epsilon}
\endsetslot

\setslot{phi}
\endsetslot

\setslot{gamma}
\endsetslot

\setslot{eta}
\endsetslot

\setslot{iota}
\endsetslot

\setslot{theta}
\endsetslot

\setslot{kappa}
\endsetslot

\setslot{lambda}
\endsetslot

\setslot{mu}
\endsetslot

\setslot{nu}
\endsetslot

\setslot{omicron}
\endsetslot

\setslot{pi}
\endsetslot

\setslot{chi}
\endsetslot

\setslot{rho}
\endsetslot

\setslot{sigma}
\endsetslot

\setslot{tau}
\endsetslot

\setslot{upsilon}
\endsetslot

\setslot{v}
\endsetslot

\setslot{omega}
\endsetslot

\setslot{xi}
\endsetslot

\setslot{psi}
\endsetslot

\setslot{zeta}
\endsetslot

\nextslot{'173}
\setslot{leftguillemot}
\endsetslot

\nextslot{'174}
\setslot{ios}
\endsetslot

\nextslot{'175}
\setslot{rightguillemot}
\endsetslot

\nextslot{'176}
\setslot{circ}
\endsetslot

\nextslot{'177}
\setslot{emdash}
\endsetslot

\comment{\section{The Upper Table}}
\comment{Most of these characters are actually accessed to by ligatures as explained in the
documentation to the \textsf{Greek} option of the \texttt{Babel} package.}

\nextslot{'200}
\setslot{alphagrave}
\endsetslot

\nextslot{'201}
\setslot{alpharough}
\endsetslot

\nextslot{'202}
\setslot{alphasmooth}
\endsetslot

\nextslot{'203}
\setslot{alpharoughgrave}
\endsetslot

\nextslot{'204}
\setslot{alphagraveios}
\endsetslot

\nextslot{'205}
\setslot{alpharoughios}
\endsetslot

\nextslot{'206}
\setslot{alphasmoothios}
\endsetslot

\nextslot{'207}
\setslot{alpharoughgraveios}
\endsetslot

\nextslot{'210}
\setslot{alphaacute}
\endsetslot

\nextslot{'211}
\setslot{alpharoughacute}
\endsetslot

\nextslot{'212}
\setslot{alphasmoothacute}
\endsetslot

\nextslot{'213}
\setslot{alphasmoothgrave}
\endsetslot

\nextslot{'214}
\setslot{alphaacuteios}
\endsetslot

\nextslot{'215}
\setslot{alpharoughacuteios}
\endsetslot

\nextslot{'216}
\setslot{alphasmoothacuteios}
\endsetslot

\nextslot{'217}
\setslot{alphasmoothgraveios}
\endsetslot

\nextslot{'220}
\setslot{alphacirc}
\endsetslot

\nextslot{'221}
\setslot{alpharoughcirc}
\endsetslot

\nextslot{'222}
\setslot{alphasmoothcirc}
\endsetslot

\nextslot{'223}
\setslot{digamma}
\endsetslot

\nextslot{'224}
\setslot{alphacircios}
\endsetslot

\nextslot{'225}
\setslot{alpharoughcircios}
\endsetslot

\nextslot{'226}
\setslot{alphasmoothcircios}
\endsetslot

\nextslot{'230}
\setslot{etagrave}
\endsetslot

\nextslot{'231}
\setslot{etarough}
\endsetslot

\nextslot{'232}
\setslot{etasmooth}
\endsetslot

\nextslot{'234}
\setslot{etagraveios}
\endsetslot

\nextslot{'235}
\setslot{etaroughios}
\endsetslot

\nextslot{'236}
\setslot{etasmoothios}
\endsetslot

\nextslot{'237}
\setslot{dummy}
\endsetslot

\nextslot{'240}
\setslot{etaacute}
\endsetslot

\nextslot{'241}
\setslot{etaroughacute}
\endsetslot

\nextslot{'242}
\setslot{etasmoothacute}
\endsetslot

\nextslot{'243}
\setslot{etaroughgrave}
\endsetslot

\nextslot{'244}
\setslot{etaacuteios}
\endsetslot

\nextslot{'245}
\setslot{etaroughacuteios}
\endsetslot

\nextslot{'246}
\setslot{etasmoothacuteios}
\endsetslot

\nextslot{'247}
\setslot{etaroughgraveios}
\endsetslot

\nextslot{'250}
\setslot{etacirc}
\endsetslot

\nextslot{'251}
\setslot{etaroughcirc}
\endsetslot

\nextslot{'252}
\setslot{etasmoothcirc}
\endsetslot

\nextslot{'253}
\setslot{etasmoothgrave}
\endsetslot

\nextslot{'254}
\setslot{etacircios}
\endsetslot

\nextslot{'255}
\setslot{etaroughcircios}
\endsetslot

\nextslot{'256}
\setslot{etasmoothcircios}
\endsetslot

\nextslot{'257}
\setslot{etasmoothgraveios}
\endsetslot

\nextslot{'260}
\setslot{omegagrave}
\endsetslot

\nextslot{'261}
\setslot{omegarough}
\endsetslot

\nextslot{'262}
\setslot{omegasmooth}
\endsetslot

\nextslot{'263}
\setslot{omegaroughgrave}
\endsetslot

\nextslot{'264}
\setslot{omegagraveios}
\endsetslot

\nextslot{'265}
\setslot{omegaroughios}
\endsetslot

\nextslot{'266}
\setslot{omegasmoothios}
\endsetslot

\nextslot{'267}
\setslot{omegaroughgraveios}
\endsetslot

\nextslot{'270}
\setslot{omegaacute}
\endsetslot

\nextslot{'271}
\setslot{omegaroughacute}
\endsetslot

\nextslot{'272}
\setslot{omegasmoothacute}
\endsetslot

\nextslot{'273}
\setslot{omegasmoothgrave}
\endsetslot

\nextslot{'274}
\setslot{omegaacuteios}
\endsetslot

\nextslot{'275}
\setslot{omegaroughacuteios}
\endsetslot

\nextslot{'276}
\setslot{omegasmoothacuteios}
\endsetslot

\nextslot{'277}
\setslot{omegasmoothgraveios}
\endsetslot

\nextslot{'300}
\setslot{omegacirc}
\endsetslot

\nextslot{'301}
\setslot{omegaroughcirc}
\endsetslot

\nextslot{'302}
\setslot{omegasmoothcirc}
\endsetslot

\nextslot{'303}
\setslot{Digamma}
\endsetslot

\nextslot{'304}
\setslot{omegacircios}
\endsetslot

\nextslot{'305}
\setslot{omegaroughcircios}
\endsetslot

\nextslot{'306}
\setslot{omegasmoothcircios}
\endsetslot

\nextslot{'310}
\setslot{iotagrave}
\endsetslot

\nextslot{'311}
\setslot{iotarough}
\endsetslot

\nextslot{'312}
\setslot{iotasmooth}
\endsetslot

\nextslot{'313}
\setslot{iotaroughgrave}
\endsetslot

\nextslot{'314}
\setslot{upsilongrave}
\endsetslot

\nextslot{'315}
\setslot{upsilonrough}
\endsetslot

\nextslot{'316}
\setslot{upsilonsmooth}
\endsetslot

\nextslot{'317}
\setslot{upsilonroughgrave}
\endsetslot

\nextslot{'320}
\setslot{iotaacute}
\endsetslot

\nextslot{'321}
\setslot{iotaroughacute}
\endsetslot

\nextslot{'322}
\setslot{iotasmoothacute}
\endsetslot

\nextslot{'323}
\setslot{iotasmoothgrave}
\endsetslot

\nextslot{'324}
\setslot{upsilonacute}
\endsetslot

\nextslot{'325}
\setslot{upsilonroughacute}
\endsetslot

\nextslot{'326}
\setslot{upsilonsmoothacute}
\endsetslot

\nextslot{'327}
\setslot{upsilonsmoothgrave}
\endsetslot

\nextslot{'330}
\setslot{iotacirc}
\endsetslot

\nextslot{'331}
\setslot{iotaroughcirc}
\endsetslot

\nextslot{'332}
\setslot{iotasmoothcirc}
\endsetslot

\nextslot{'333}
\setslot{Iotadieresis}
\endsetslot

\nextslot{'334}
\setslot{upsiloncirc}
\endsetslot

\nextslot{'335}
\setslot{upsilonroughcirc}
\endsetslot

\nextslot{'336}
\setslot{upsilonsmoothcirc}
\endsetslot

\nextslot{'337}
\setslot{Upsilondieresis}
\endsetslot

\nextslot{'340}
\setslot{epsilongrave}
\endsetslot

\nextslot{'341}
\setslot{epsilonrough}
\endsetslot

\nextslot{'342}
\setslot{epsilonsmooth}
\endsetslot

\nextslot{'343}
\setslot{epsilonroughgrave}
\endsetslot

\nextslot{'344}
\setslot{omicrongrave}
\endsetslot

\nextslot{'345}
\setslot{omicronrough}
\endsetslot

\nextslot{'346}
\setslot{omicronsmooth}
\endsetslot

\nextslot{'347}
\setslot{omicronroughgrave}
\endsetslot

\nextslot{'350}
\setslot{epsilonacute}
\endsetslot

\nextslot{'351}
\setslot{epsilonroughacute}
\endsetslot

\nextslot{'352}
\setslot{epsilonsmoothacute}
\endsetslot

\nextslot{'353}
\setslot{epsilonsmoothgrave}
\endsetslot

\nextslot{'354}
\setslot{omicronacute}
\endsetslot

\nextslot{'355}
\setslot{omicronroughacute}
\endsetslot

\nextslot{'356}
\setslot{omicronsmoothacute}
\endsetslot

\nextslot{'357}
\setslot{omicronsmoothgrave}
\endsetslot

\nextslot{'360}
\setslot{iotadieresis}
\endsetslot

\nextslot{'361}
\setslot{iotagravedieresis}
\endsetslot

\nextslot{'362}
\setslot{iotaacutedieresis}
\endsetslot

\nextslot{'363}
\setslot{iotacircdieresis}
\endsetslot

\nextslot{'364}
\setslot{upsilondieresis}
\endsetslot

\nextslot{'365}
\setslot{upsilongravedieresis}
\endsetslot

\nextslot{'366}
\setslot{upsilonacutedieresis}
\endsetslot

\nextslot{'367}
\setslot{upsiloncircdieresis}
\endsetslot

\nextslot{'370}
\setslot{alphaios}
\endsetslot

\nextslot{'371}
\setslot{etaios}
\endsetslot

\nextslot{'372}
\setslot{omegaios}
\endsetslot

\nextslot{'373}
\setslot{rhorough}
\endsetslot

\nextslot{'374}
\setslot{rhosmooth}
\endsetslot

\nextslot{'375}
\setslot{boundarychar}
\endsetslot

\nextslot{'376}
\setslot{anwtonos}
\endsetslot

\nextslot{'377}
\setslot{katwtonos}
\endsetslot

\endencoding
\end{document}

