% arara: pdflatex
% arara: makeindex: { sort: true , style: endiagram_en.ist }
% arara: biber
% arara: pdflatex
% arara: pdflatex
% --------------------------------------------------------------------------
% the ENdiagram package
%
%   easy creation of potential energy curve diagrams
%
% --------------------------------------------------------------------------
% Clemens Niederberger
% Web:    https://www.bitbucket.org/cgnieder/endiagram
% E-Mail: contact@mychemistry.eu
% --------------------------------------------------------------------------
% If you have any ideas, questions, suggestions or bugs to report, please
% feel free to contact me.
% --------------------------------------------------------------------------
% Copyright 2012 Clemens Niederberger
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Clemens Niederberger.
%
% This work consists of the files endiagram.sty, endiagram_en.tex,
% README and the derived file endiagram_en.pdf.
% --------------------------------------------------------------------------
% if you want to compile this documentation you'll need the document class
% `cnpkgdoc' which you can get here:
%    https://bitbucket.org/cgnieder/cnpkgdoc/
%    the class is licensed LPPL 1.3 or later
\documentclass[DIV10,toc=index,toc=bib]{cnpkgdoc}
\docsetup{
  language     = en ,
  title        = ENdiagram ,
  subtitle     = {Easy creation of potential energy curve diagrams.} ,
  name         = ENdiagram ,
  info         = {Easy creation of potential energy curve diagrams} ,
  date         = 2012/08/20 ,
  version      = 0.1a ,
  keywords     = {energy diagrams,tikz,LaTeX},
  modules      = true ,
  code-box     = {
    backgroundcolor = gray!2!white ,
    skipbelow       = .6\baselineskip plus .5ex minus .5ex ,
    skipabove       = .6\baselineskip plus .5ex minus .5ex ,
    roundcorner     = 3pt
  }
}
\usepackage[osf]{libertine}
\cnpkgcolors{
  main   => cnpkgred ,
  key    => yellow!40!brown ,
  module => cnpkgblue ,
  link   => black!90
}

\renewcommand*\othersectionlevelsformat[3]{%
  \textcolor{main}{#3\autodot}\enskip}
\renewcommand*\partformat{%
  \textcolor{main}{\partname~\thepart\autodot}}

\addcmds{
  AddAxisLabel,
  arrow,
  calory,
  ch,
  chemfig,
  chemsetup,
  color,
  command,
  DeclareChemIUPAC,
  draw,
  ENcurve,
  ENsetup,
  fontfamily,
  iso,
  joule,
  kilo,
  lewis,
  MakeOrigin,
  mole,
  node,
  per,
  schemestart,
  schemestop,
  selectfont,
  setatomsep,
  ShowEa,
  ShowGain,
  ShowNiveaus,
  sisetup,
  tert,
  tikz,
  transitionstatesymbol
}
\usepackage{endiagram,chemmacros,chemfig,ragged2e,fnpct}

\usepackage{acro}
\acsetup{short-format=\scshape}
\DeclareAcronym{su}{su}{standard unit}
\DeclareAcronym{CTAN}{ctan}{Comprehensive \TeX\ Archive Network}
\DeclareAcronym{MWE}{mwe}{minimal working example}

\begin{filecontents}{\jobname.ist}
 heading_prefix "{\\bfseries "
 heading_suffix "\\hfil}\\nopagebreak\n"
 headings_flag  1
 delim_0 "\\dotfill "
 delim_1 "\\dotfill "
 delim_2 "\\dotfill "
 delim_r "\\nohyperpage{\\textendash}"
 suffix_2p "\\nohyperpage{\\,f.}"
 suffix_3p "\\nohyperpage{\\,ff.}"
\end{filecontents}

\begin{filecontents}{\jobname.bib}
@book{brueckner,
  author    = {Reinhard Br\"uckner},
  title     = {Reaktionsmechanismen},
  publisher = {Springer-Verlag Berlin Heidelberg},
  edition   = {3.\,Auflage, 2.\,korrigierter Nachdruck},
  year      = {2009},
  isbn      = {978-3-8274-1579-0}
}
@online{tikz,
  author  = {Till Tantau},
  title   = {Ti\textbf{\textit{k}}Z/pgf},
  url     = {http://sourceforge.net/projects/pgf/},
  urldate = {2012-01-31}
}
\end{filecontents}

\usepackage[backend=biber,style=alphabetic]{biblatex}
\addbibresource{\jobname.bib}

\setlength{\marginparwidth}{1.5\marginparwidth}

\ExplSyntaxOn
\NewDocumentCommand \Default {g}
  {
    \hfill\llap
      {
        \IfNoValueTF { #1 }
          {(initially~empty)}
          {Default:~\code{#1}}
      }
    \newline
  }
\NewDocumentCommand \NoDefault {}
  { \hfill \newline }
\ExplSyntaxOff

\makeindex
\begin{document}

\section{Licence and Requirements}
Permission is granted to copy, distribute and/or modify this software under the
terms of the LaTeX Project Public License, version 1.3 or later
(\url{http://www.latex-project.org/lppl.txt}). This package has the status
``maintained.''

\ENdiagram needs the \paket{l3kernel} and the package \paket{xparse}. \paket{xparse}
is part of the \paket{l3packages} bundle. \ENdiagram also needs \paket[pgf]{\TIKZ}
and \paket{siunitx}.

Basic knowledge of \TikZ/pgf \cite{tikz} is recommended.

\section{Caveat}
This package is in an experimental state. There is lots of code to clean up and
there are many loose ends to be tied together until I'll be satisfied to publish
this package as stable. However, as the unofficial release on my blog has gotten
quite some interest\footnote{$\ge 400$ downloads} I decided to publish this
experimental version on the \ac{CTAN} nonetheless.

If you detect any bugs -- and I guess you will -- please write me a short email
with a \ac{MWE} showing the undesired behaviour or report on issue on
\url{https://www.bitbucket.org/cgnieder/endiagram}.

\section{Before We Start}
This document presents commands and options in a consistent way:
\begin{beschreibung}
 \Umg{environment}[<options>]{...}
 \Befehl{command}[<options>]{<arg>}
 \option{option}{\default{default}|value} choice option with a \default{default value} 
 that will be used, if the option is set without value.
 \option{option}{<type>} option that needs an input of a certain type.
 \option[command]{option}{<type>} option that belongs to the module
 \textcolor{module}{\code{command}}.
\end{beschreibung}
Options can be used in the \oa{<options>} arguments. There are two kinds of
options: choice options where you can choose one of the values separated with
\code{|}; an underlined value is a default value that is used, if no value is
given. The others need a value of a certain type like a number (\code{<num>}),
arbitrary input (\code{<text>}), \TikZ\ options (\code{<tikz>}) \etc.

As a rule commands are only defined inside the \env{endiagram}{} environment.

Options can also be set up with this command:
\begin{beschreibung}
 \Befehl{ENsetup}[<module>]{<options>}
\end{beschreibung}

\begin{beispiel}[code only]
 \ENsetup{option1 = value1, option2 = value2}
 \ENsetup{module/option = value}
 \ENsetup[module]{option = value}
\end{beispiel}

Options that belong to a module are specific to a command. The command
\cmd{command} they belong to can \emph{only} have the options marked with
\textcolor{module}{\code{command}} in his argument \oa{<options>}.

All other options can also be set globally as package options. These are options
which do \emph{not} belong to a module like for example the \key{draft} option
(see page~\pageref{key:debug}):
\begin{beispiel}[code only]
 \usepackage[draft]{endiagram}
\end{beispiel}

\section{The Curve --  \cmd{ENcurve}}\secidx{curve}
The potential energy curves are drawn inside the \env{endiagram}{\ldots}
environment using the command \cmd{ENcurve}.
\begin{beschreibung}
 \Umg{endiagram}[<options>]{}
 \Befehl{ENcurve}[<options>]{<level1>,<level2>,<level3>}
\end{beschreibung}
The command needs a comma separated list of relative energy levels.
\cmd{ENcurve}{1,4,0} means the maximum is four times higher above the end level
than the starting level.
\begin{beispiel}
\begin{endiagram}
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

\cmd{ENcurve} can read any number of values but needs \emph{at least three}.
Less values will cause an error.
\begin{beispiel}[below]
\begin{endiagram}
 \ENcurve{1,5,2.5,4,0}
\end{endiagram}
\quad
\begin{endiagram}[scale=.7]
 \ENcurve{1,7,2.5,6,3,4,0}
\end{endiagram}
\end{beispiel}

\subsection{Properties}
\subsubsection{Scaling}\secidx[scaling]{curve}
Values given to \cmd{ENcurve} are multiples of \ENdiagram's \ac{su}. As a default
it is set to \SI{.5}{cm} but can be changed using an option. There are other ways
to influence the size of the diagram, too.
\begin{beschreibung}
 \Option{unit}{<length>}\Default{.5cm} The standard unit for \cmd{ENcurve} and
 some other commands. This document refers to it with \ac{su}.
 \Option{scale}{<factor>}\Default{1}
\end{beschreibung}

A changed \ac{su}:
\begin{beispiel}
\begin{endiagram}[unit=2em]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

Scaled by the factor \code{1.5}:
\begin{beispiel}
\begin{endiagram}[scale=1.5]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

\subsubsection{Influencing the position relative to the axes}\secidx[position]{curve}
The \code{offset} options control the length and position of the horizontal axis
relative to the curve.
\begin{beschreibung}
 \Option{offset}{<num>}\Default{0} \code{<num>} is a multiple of the \ac{su} (see
 page~\pageref{key:unit}).
 \Option{r-offset}{<num>}\Default{0} \code{<num>} is a multiple of the \ac{su}.
 \Option{l-offset}{<num>}\Default{0} \code{<num>} is a multiple of the \ac{su}.
\end{beschreibung}

The default behaviour for comparison:
\begin{beispiel}
\begin{endiagram}
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

\key{l-offset} controls the distance of the start of the $x$ axis to the start
of the curve:
\begin{beispiel}
closer to the $y$ axis:\\
\begin{endiagram}[l-offset=-1]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

\key{r-offset} controls the ``protruding'' of the $x$ axis after the curve:
\begin{beispiel}
$x$ axis extended to the right:\\
\begin{endiagram}[r-offset=1]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

\key{offset} changes both values equally:
\begin{beispiel}
$x$ extended to the left and the right:\\
\begin{endiagram}[offset=1]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

\subsubsection{Increment between the levels}\secidx[increment]{curve}
With the option
\begin{beschreibung}
 \Option[ENcurve]{step}{<num>}\Default{2} \code{<num>} is a multiple of the \ac{su}
 (see page~\pageref{key:unit}).
\end{beschreibung}
the default increment between the levels can be changed.
\begin{beispiel}[below]
\begin{endiagram}
 \ENcurve{1,4,0}
\end{endiagram}
\quad
\begin{endiagram}
 \ENcurve[step=3]{1,4,0}
\end{endiagram}
\end{beispiel}

Sometimes a certain level should be shifted against the others. This is possible
using an optional argument to the value of that level:
\begin{beschreibung}
 \Befehl{ENcurve}{<level>[<offset>],...}\NoDefault
   \oa{<offset>} is a multiple of the \ac{su} (see page~\pageref{key:unit}) and is set
   to \code{0} as default. The level will be shifted to the right (positive values)
   or left (negative values).
\end{beschreibung}
\begin{beispiel}[below]
\begin{endiagram}
 \ENcurve{1,4,0}
\end{endiagram}
\quad
\begin{endiagram}
 \ENcurve{1,4[.5],0}
\end{endiagram}
\quad
\begin{endiagram}
 \ENcurve{1,4[-.5],0}
\end{endiagram}
\end{beispiel}

\subsubsection{The shape}\secidx[shape]{curve}
The option
\begin{beschreibung}
 \Option[ENcurve]{looseness}{<value>}\Default{.5} should be a number between
 \code{0} and \code{1}.
\end{beschreibung}
changes the shape of the curve.
\begin{beispiel}[below]
\begin{endiagram}
 \ENcurve[looseness=0]{0,3,1}
\end{endiagram}
\quad
\begin{endiagram}
 \ENcurve{0,3,1}% corresponds looseness=.5
\end{endiagram}
\quad
\begin{endiagram}
 \ENcurve[looseness=1]{0,3,1}
\end{endiagram}
\end{beispiel}

\subsubsection{Ending minima}\secidx[minima]{curve}
Sometimes potential energy curves are drawn with local minima at the start and
the end of the cuve. The option
\begin{beschreibung}
 \Option[ENcurve]{minima}{\default{true}|false}\Default{false}
\end{beschreibung}
en- or disables them:
\begin{beispiel}
\begin{endiagram}
 \ENcurve[minima]{1,4,0}
\end{endiagram}
\end{beispiel}

\subsubsection{\TIKZ\ style}\secidx[style]{curve}
The style of the curve can be changed using \TikZ\ options:
\begin{beschreibung}
 \Option[ENcurve]{tikz}{<tikz>}\Default Valid are options that can be used with
 \verb=\draw=.
\end{beschreibung}
\begin{beispiel}
\begin{endiagram}
 \ENcurve[tikz={red,dotted}]{0,3,1}
\end{endiagram}
\end{beispiel}

\subsection{The Axes}\secidx[axes]{curve}\secidx{axes}
There are also possibilities to customize the axes.
\begin{beschreibung}
 \Option{axes}{xy|y|y-l|y-r|x|all|false}\Default{xy} Number and type of axes.
 \Option{x-axis}{<tikz>}\Default \TikZ options to the $x$ axis.
 \Option{y-axis}{<tikz>}\Default \TikZ options to the $y$ axis.
 \Option{x-label}{below|right}\Default{below} Position of the $x$ axis label.
 \Option{y-label}{above|left}\Default{left} Position of the $y$ axis label.
 \Option{x-label-pos}{<value>}\Default{.5} Position of the $x$ axis label when
 \key{x-label}{below} is set.
 \Option{y-label-pos}{<value>}\Default{.5} Position of the $y$ axis label when
 \key{y-label}{left} is set.
 \Option{x-label-offset}{<length>}\Default{0pt} Distance between label and $x$
 axis.
 \Option{y-label-offset}{<length>}\Default{0pt} Distance between label and $y$
 axis.
 \Option{x-label-angle}{<angle>}\Default{0} Angle which rotates the $x$ axis
 label counter clockwise.
 \Option{y-label-angle}{<angle>}\Default{0} Angle which rotates the $y$ axis
 label counter clockwise.
 \Option{x-label-text}{<text>}\Default{\$\cmd{xi}\$} $x$ axis label.
 \Option{y-label-text}{<text>}\Default{\$E\$} $y$ axis label.
\end{beschreibung}

No axes:
\begin{beispiel}
\begin{endiagram}[axes=false]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

All axes:
\begin{beispiel}
\begin{endiagram}[axes=all]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

Only the $x$ axis:
\begin{beispiel}
\begin{endiagram}[axes=x]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

Changed labels:
\begin{beispiel}
\begin{endiagram}[x-label-text=\footnotesize reaction coordinate]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

Different positions of the labels:
\begin{beispiel}[below]
\begin{endiagram}[
  x-label=right,
  y-label=above]
 \ENcurve{1,4,0}
\end{endiagram}
\quad
\begin{endiagram}[
  x-label-pos=.7,
  y-label-pos=.7,
  y-label-angle=-90,
  y-label-offset=5pt]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}

Crazy setup:
\begin{beispiel}
\begin{endiagram}[x-axis={draw=blue,dashed,font=\color{green}}]
 \ENcurve{1,4,0}
\end{endiagram}
\end{beispiel}
\secidx*{axes}

\subsection{Debugging Information}\secidx[debugging]{curve}\index{debugging}
For precise adjustments of details -- particularly with the options and commands
described in the next sections -- some information is useful that is hidden
normally. These options enable access:
\begin{beschreibung}
 \Option{debug}{\default{true}|false}\Default{false}
 \Option{draft}{\default{true}|false}\Default{false} An Alias to \key{debug}.
 \Option{final}{\default{true}|false}\Default{true} The opposite of \key{draft}.
\end{beschreibung}
\begin{beispiel}[below]
\begin{endiagram}[debug]
 \ENcurve{1,4,0}
\end{endiagram}
\quad
\begin{endiagram}[debug,unit=2em]
 \ENcurve[step=3,minima]{1,4,0}
\end{endiagram}
\end{beispiel}
Shown are a grid, the origin and the coordinates and names of the levels.
Depending on the commands you're using you get more information.It is described
with the commands they belong to.
\secidx*{curve}

\section{The Levels}\secidx{levels}
\subsection{The \cmd{ShowNiveaus} Command}
The command
\begin{beschreibung}
 \Befehl{ShowNiveaus}[<options>]
\end{beschreibung}
draws horizontal lines to the levels:
\begin{beispiel}
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowNiveaus
\end{endiagram}
\end{beispiel}

\subsection{Customization}
A number of options allow fine-tuning:
\begin{beschreibung}
 \Option[ShowNiveaus]{length}{<num>}\Default{1} The length of the lines.
 \code{<num>} is a multiple of the \ac{su} (see page~\pageref{key:unit}).
 \Option[ShowNiveaus]{shift}{<num>}\Default{0} Shift to the right (positive
 values) or the left (negative values). \code{<num>} is a multiple of the \ac{su}.
 \Option[ShowNiveaus]{tikz}{<tikz>}\Default \TikZ options to modify the style
 of the lines.
\end{beschreibung}

Longer lines:
\begin{beispiel}
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowNiveaus[length=2]
\end{endiagram}
\end{beispiel}

Without \key{shift} are centered to the extrema , \ie they protrude by half of
the value specified with \key{length}.
\begin{beispiel}[below]
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowNiveaus[shift=.5]
\end{endiagram}
\quad
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowNiveaus[length=2,shift=1]
\end{endiagram}
\end{beispiel}
Maybe the examples in the next section will make it more clear why \key{shift}
can be useful.

\subsection{Choose Levels Explicitly}
If you don't want to draw a line to every level you can use this option:
\begin{beschreibung}
 \Option[ShowNiveaus]{niveau}{<id1>,<id2>}\NoDefault
   The \code{<id>} is the name of the level as shown by the \key{debug} option,
   see page~\pageref{key:debug}.
\end{beschreibung}

The debug information helps in choosing the right level. The names of the levels
follow the scheme \code{N-<number of curve>-<number of level>}.
\begin{beispiel}[below]
\begin{endiagram}[debug]
 \ENcurve{3,4,0}
\end{endiagram}
\quad
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowNiveaus[niveau=N1-2]
\end{endiagram}
\end{beispiel}

Every level can have a different color, length and shift:
\begin{beispiel}
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowNiveaus[length=2,tikz=red,niveau=N1-2]
 \ShowNiveaus[niveau=N1-1,shift=-.5]
 \ShowNiveaus[niveau=N1-3,shift=.5]
\end{endiagram}
\end{beispiel}
\secidx*{Niveaus}

\section{The Energy Gain}\secidx{energy gain}
\subsection{The \cmd{ShowGain} Command}
The command
\begin{beschreibung}
 \Befehl{ShowGain}[<options>]
\end{beschreibung}
enables you the show the energy gain or loss of the reaction. It is always the
difference between the first and the last level.
\begin{beispiel}
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowGain
\end{endiagram}
\end{beispiel}

\subsection{Customization}
The command has options to modify the appearance.
\begin{beschreibung}
 %%
 \Option[ShowGain]{tikz}{<tikz>}\Default{<->} \TikZ options for the vertical line.
 %%
 \Option[ShowGain]{connect}{<tikz>}\Default{dashed,help lines} \TikZ options for
 the connecting line.
 %%
 \Option[ShowGain]{connect-from-line}{\default{true}|false}\Default{false} The
 connecting line starts either at the maximum/minimum or at the line drawn by
 \cmd{ShowNiveaus}. This option works with the default values but otherwise can
 lead to unwanted results. To avoid that you can either set \cmd{ShowGain}
 \emph{before} \cmd{ShowNiveaus} or you need to choose another way.
 %%
 \Option[ShowGain]{offset}{<num>}\Default{0} Shifts the vertical line to the
 right (positive value) or the left (negative value). \code{<num>} is a multiple
 of \ac{su} (see page~\pageref{key:unit}).
 %%
 \Option[ShowGain]{label}{\default{true}|false|<text>}\Default{false} Use the
 default label (\code{true}) or an own label (\code{<text>}).\label{key:showgain_label}
 %%
 \Option[ShowGain]{label-side}{right|left}\Default{right} The side of the vertical
 line on which the label should be placed.
 %%
 \Option[ShowGain]{label-pos}{<value>}\Default{.5} Position at the line. \code{0}
 means at the height of $H_1$, \ie the starting level, \code{1} means at the
 height of $H_2$, \ie the ending level.
 %%
 \Option[ShowGain]{label-tikz}{<tikz>}\Default \TikZ options for the label.
\end{beschreibung}

\begin{beispiel}
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowGain[connect={dotted,red},offset=2]
\end{endiagram}
\end{beispiel}

Using the \key{label} option:
\begin{beispiel}[below]
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowGain[label]
\end{endiagram}
\begin{endiagram}
 \ENcurve{0,4,3}
 \ShowGain[label]
\end{endiagram}
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowGain[label=exothermic]
\end{endiagram}
\end{beispiel}

Connecting lines and levels are overlapping:
\begin{beispiel}
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowNiveaus
 \ShowGain[connect=red]
\end{endiagram}
\end{beispiel}

A possible solution:
\begin{beispiel}
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowNiveaus
 \ShowGain[connect-from-line,connect=red]
\end{endiagram}
\end{beispiel}

Better would be to set \cmd{ShowNiveaus} \emph{after} \cmd{ShowGain}, particularly
if you're not using the default settings.
\begin{beispiel}
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowNiveaus[niveau=N1-1]
 \ShowGain[connect={red,dotted}]
\end{endiagram}
\begin{endiagram}
 \ENcurve{3,4,0}
 \ShowGain[connect={red,dotted}]
 \ShowNiveaus[niveau=N1-1]
\end{endiagram}
\end{beispiel}

\subsection{Debugging Information}\index{debugging}\secidx[debugging]{energy gain}
Using the \key{debug} option (see page~\pageref{key:debug}) gives you further
information:
\begin{beispiel}
\begin{endiagram}[debug]
 \ENcurve{3,4,0}
 \ShowGain
\end{endiagram}
\end{beispiel}
\secidx*{energy gain}

\section{The Activation Energy}\secidx{activation energy}
\subsection{The \cmd{ShowEa} Command}
This command is similar to the commands \cmd{ShowNiveaus} and \cmd{ShowGain}.
\begin{beschreibung}
 \Befehl{ShowEa}[<options>]
\end{beschreibung}
It enables to show the activation energy:
\begin{beispiel}
\begin{endiagram}
 \ENcurve{2,4,0}
 \ShowEa
\end{endiagram}
\end{beispiel}

The default behaviour shows the difference between the \emph{first} maximum
after a \emph{previous} minimum to that minimum:
\begin{beispiel}
\begin{endiagram}
 \ENcurve{2,0,4}
 \ShowEa
\end{endiagram}
\end{beispiel}

This also holds if there is more than one maximum. How you choose a different
one is described in the next section.
\begin{beispiel}
\begin{endiagram}
 \ENcurve{1,5,2.5,4,0}
 \ShowEa
\end{endiagram}
\end{beispiel}

\subsection{Choose Level Explicitly}
The default behaviour is all right if there is only one maximum. If there are
more one might want to choose a different one. The following options allow that.
\begin{beschreibung}
 \Option[ShowEa]{max}{first|all}\Default{first}
   Show the difference to the first maximum or to all maxima.
 \Option[ShowEa]{from}{\{(<coordinate1>)to(<coordinate2>)\}}\NoDefault
   Specify the coordinates that should be connected. You can either use the
   coordinates \code{(<x>,<y>)} or the name \code{(<name>)} of the node.
\end{beschreibung}

Using \key[ShowEa]{max}{all}:
\begin{beispiel}
\begin{endiagram}
 \ENcurve{1,5,2.5,4,0}
 \ShowEa[max=all]
\end{endiagram}
\end{beispiel}


Since in most cases this won't be what you want you can specify the coordinates
yourself. The option \key{debug} (see page~\pageref{key:debug}) may help.
\begin{beispiel}[below]
\begin{endiagram}[debug]
 \ENcurve{1,5,2.5,4,0}
 \ShowEa
\end{endiagram}
\quad
\begin{endiagram}
 \ENcurve{1,5,2.5,4,0}
 \ShowEa[from={(0,1) to (6,4)}]
\end{endiagram}

\begin{endiagram}
 \ENcurve{1,5,2.5,4,0}
 \ShowEa[from={(N1-1) to (N1-3)}]
\end{endiagram}
\quad
\begin{endiagram}
 \ENcurve{1,5,2.5,4,0}
 \ShowEa[from={(N1-5) to (N1-4)}]
\end{endiagram}
\end{beispiel}
In every case the position of the vertical line is determined by the \emph{first}
coordinate.

\subsection{Customization}
Again there are a number of options to customize the appearance.
\begin{beschreibung}
 \Option[ShowEa]{tikz}{<tikz>}\Default{<->} \TikZ options for the vertical line.
 \Option[ShowEa]{connect}{<tikz>}\Default{dashed,help lines} \TikZ options for
 the horizontal line.
 \Option[ShowEa]{label}{\default{true}|false|<text>}\Default{false} Use the
 default label ($E_{\mathrm{a}}$) or an own label.\label{key:showea_label}
 \Option[ShowEa]{label-side}{right|left}\Default{right} The side of the vertical
 line where the label should appear.
 \Option[ShowEa]{label-pos}{<value>}\Default{.5} Determines the vertical position
 of the label relative to the vertical line. \code{0} means at the lower end,
 \code{1} means at the upper end.
 \Option[ShowEa]{label-tikz}{<tikz>}\Default \TikZ options for the label.
\end{beschreibung}

\begin{beispiel}
\begin{endiagram}
 \ENcurve[step=4]{1,6,0}
 \ShowEa[label,label-pos=.3]
\end{endiagram}
\end{beispiel}

\begin{beispiel}
\begin{endiagram}
 \ENcurve{1,5,2.5,4,0}
 \ShowEa[
   from    = {(N1-1) to (N1-3)},
   connect = {draw=none},
   label   = endoth.]
 \ShowEa[
   from  = (N1-3) to (N1-5),
   label = exoth.,
   label-pos = .7]
 \ShowGain[label=exoth.] 
\end{endiagram}
\end{beispiel}

\subsection{Debugging Information}\index{debugging}\secidx[debugging]{activation energy}
The \key{debug} option gives you further information.
\begin{beispiel}[below]
\begin{endiagram}[debug]
 \ENcurve{1,5,2,3,0}
 \ShowEa[max=all]
\end{endiagram}
\quad
\begin{endiagram}[debug]
 \ENcurve{1,5,2,3,0}
 \ShowEa[from={(0,1) to (6,3)}]
\end{endiagram}
\end{beispiel}
\secidx*{activation energy}

\section{Several Curves in one Diagram}\secidx{several curves}
It's easy to draw several curves. You only need to use \cmd{ENcurve} more than
once.
\begin{beispiel}
\begin{endiagram}
 \ENcurve[tikz=blue]{1,4,0}
 \ENcurve[tikz=red]{1,2.5,0}
 \draw[blue] (5,5) -- ++(1,0) node[black,right] {with enzyme};
 \draw[red] (5,4) -- ++(1,0) node[black,right] {without enzyme};
\end{endiagram}
\end{beispiel}

The commands \cmd{ShowNiveaus}, \cmd{ShowGain} and \cmd{ShowEa} always relate to
the curve set at last. This means you can use them selectively.
\begin{beispiel}
\begin{endiagram}
 \ENcurve[tikz=blue]{1,4,0}
 \ShowEa[tikz={blue,<->}]
 \ENcurve[tikz=red]{1,2.5,0}
\end{endiagram}

\begin{endiagram}
 \ENcurve[tikz=blue]{1,4,0}
 \ENcurve[tikz=red]{1,2.5,0}
 \ShowEa[tikz={red,<->}]
\end{endiagram}
\end{beispiel}

Using more than one curves explains the multiple numbering of the level names:
\begin{beispiel}
% the names of the levels (N1-1)
% and (N1-3) are hidden behind
% (N2-1) and (N2-3), resp.
\begin{endiagram}[debug]
 \ENcurve{1,4,0}
 \ENcurve{1,2.5,0}
\end{endiagram}

\begin{endiagram}
 \ENcurve[tikz=blue]{1,4,0}
 \ENcurve[tikz=red]{1,2.5,0}
 \draw[<->] (N1-2) -- (N2-2) ;
\end{endiagram}
\end{beispiel}

Of course it's possible to choose different options for different curves. This
means you can use curves with a different number of maxima.
\begin{beispiel}
\begin{endiagram}
 \ENcurve[step=4]{1,6,0}
 \ENcurve[
   tikz={densely dotted}]
  {1,4[1],2.5,3[-1],0}
\end{endiagram}
\end{beispiel}
\secidx*{several curves}

\section{Usage of Ti\textit{k}Z}\index{TikZ@\TIKZ}
Since the \env{endiagram}{\ldots} environment only is a \code{tikzpicture}
environment (well, more or less) you can use \TikZ commands inside it. This
means you can easily add additional information to the diagram.
\begin{beispiel}
% needs the package `chemmacros'
\begin{endiagram}
 \ENcurve{1,5,2,3,0}
 \ShowNiveaus[length=2,niveau={N1-2,N1-3,N1-4}]
 \node[above,xshift=4pt] at (N1-2) {[\"UZ1]$^{\transitionstatesymbol}$} ;
 \node[below] at (N1-3) {ZZ} ;
 \node[above,xshift=4pt] at (N1-4) {[\"UZ2]$^{\transitionstatesymbol}$} ;
\end{endiagram}
\quad
\begin{endiagram}
 \ENcurve{2,3,0}
 \draw[<-,red] (N1-2) -- ++(2,1) node[right] {transition state} ;
\end{endiagram}
\end{beispiel}

\subsection{The Origin}\index{origin}
The nodes \code{(origin-l)} and \code{(origin-r)} are set at the end of the
environment. This means they are \emph{not} available inside the
\env{endiagram}{\ldots} environment. If you want to use them you either need to
look up their coordinates using the \key{debug} option (see
page~\pageref{key:debug}) \ldots
\begin{beispiel}
\begin{endiagram}[y-label=above]
 \ENcurve{1,3[.5],0}
 \draw[dashed,help lines]
   (N1-2) -- (N1-2 -| -1.5,-1)
   node[left,black] {max} ;
\end{endiagram}
\end{beispiel}
\ldots\ or use this option:
\begin{beschreibung}
 \Option{tikz}{<tikz>}\Default \TikZ options for the \env{endiagram}{\ldots}
 environment.
\end{beschreibung}
With it you can pass arbitrary \TikZ options to the internal \code{tikzpicture}
environment.
\begin{beispiel}
\begin{endiagram}[
   y-label = above,
   tikz    = {remember picture}]
 \ENcurve{1,3[.5],0}
\end{endiagram}
\tikz[remember picture,overlay]{
 \draw[dashed,help lines]
   (N1-2) -- (N1-2 -| origin-l)
   node[left,black] {max} ;
}
\end{beispiel}

There is an easier way, though: you can use the following command \emph{after}
drawing all curves:
\begin{beschreibung}
 \Befehl{MakeOrigin}\label{cmd:makeorigin}
\end{beschreibung}

\begin{beispiel}
\begin{endiagram}[y-label = above]
 \ENcurve{1,3[.5],0}
 \MakeOrigin
 \draw[dashed,help lines]
   (N1-2) -- (N1-2 -| origin-l)
   node[left,black] {max} ;
\end{endiagram}
\end{beispiel}

\section{Axes Ticks and Labels}\secidx{axes ticks and labels}
\subsection{Automatic Ticks}\ENsetup{AddAxisLabel/font=\fontfamily{fxlf}\selectfont}
The $y$ axes can get ticks automatically.
\begin{beschreibung}
 \Option{ticks}{y|\default{y-l}|y-r|none}\Default{none} Adds ticks to the
 specified axes.
 \Option{ticks-step}{<num>}\Default{1} \code{<num>} is a multiple of the \ac{su}.
 \key{ticks-step}{2} means that only every second tick is added.
\end{beschreibung}
\begin{beispiel}
\ENsetup{ticks,y-label=above}
\begin{endiagram}
 \ENcurve{1,5,2.5,4,0}
\end{endiagram}
\begin{endiagram}[ticks-step=2]
 \ENcurve{1,5,2.5,4,0}
\end{endiagram}
\end{beispiel}
These ticks obey the \key{energy-unit} option, see section~\ref{sec:reale_werte}.

\subsection{The \cmd{AddAxisLabel} Command}
% TODO auf Unterschied zwischen den Ticks produziert durch AddAxisLabel und Option ticks hinweisen
To be able to add labels to the ticks there is the command
\begin{beschreibung}
 \Befehl{AddAxisLabel}[<options>]{(<point1>)[<opt. label>];(<point2>);...}
 \Befehl{AddAxisLabel}*[<options>]{<level1>[<opt. label>];<level2>;...}
\end{beschreibung}
As you can see there are two variants. The first one awaits a list of coordinates
in the \TikZ sense. The second awaits $y$ values. Every of these values has an
optional argument with which you can specify the label.

The first variant also draws lines between the points specifiad and the $y$ axis.
Internally this command calls \cmd{MakeOrigin}, see p.\,\pageref{cmd:makeorigin},
which means it should be used \emph{after} drawing all curves.

Example for the second variant:
\begin{beispiel}
\begin{endiagram}[y-label=above]
 \ENcurve{1,3,2}
 \AddAxisLabel*{1;2;3}
\end{endiagram}
\begin{endiagram}[y-label=above]
 \ENcurve{1,3,2}
 \AddAxisLabel*{1[10];2[20];3[30]}
\end{endiagram}
\end{beispiel}

Example for the first variant:
\begin{beispiel}
\begin{endiagram}[y-label=above]
 \ENcurve{1,3,2}
 \AddAxisLabel{(N1-1)[$H_1$];(N1-3)[$H_2$]}
\end{endiagram}
\end{beispiel}

The optional arguments can also get \TikZ options. The description should read:
\begin{beschreibung}
 \Befehl{AddAxisLabel}{(<point>)[<opt. label>,<tikz>]}
 \Befehl{AddAxisLabel}*{<level>[<opt. label>,<tikz>]}
\end{beschreibung}

\begin{beispiel}
\begin{endiagram}[y-label=above]
 \ENcurve{1,3,2}
 \AddAxisLabel*{1[10];2[20,{draw,fill=green!15}];3[30]}
\end{endiagram}
\begin{endiagram}[y-label=above]
 \ENcurve{1,3,2}
 \AddAxisLabel{(N1-1)[$H_1$];(N1-3)[$H_2$,{draw,font=\color{red},fill=green!15}]}
\end{endiagram}
\end{beispiel}

\subsection{Customization}
You have several options to customize the labels:
\begin{beschreibung}
 \Option[AddAxisLabel]{axis}{y-l|y-r|x}\Default{y-l} Choose which axis gets the
 labels.
 \Option[AddAxisLabel]{connect}{<tikz>}\Default{dashed,help lines} Change the
 style of the lines.
 \Option[AddAxisLabel]{font}{<commands>}\Default You can add commands like
 \verb=\footnotesize= and/or \verb=\color{red}= to format the label text.
\end{beschreibung}

\begin{beispiel}[below]
\begin{endiagram}[y-label=above]
 \ENcurve{1,3,2}
 \AddAxisLabel{(2,3)[\"UZ]}
\end{endiagram}
\begin{endiagram}[y-label=above,x-label=right]
 \ENcurve{1,3,2}
 \AddAxisLabel[axis=x,connect=dotted]{(2,3)[\"UZ]}
\end{endiagram}
\begin{endiagram}[axes=all,y-label=above]
 \ENcurve{1,3,2}
 \AddAxisLabel[axis=y-r,connect=red]{(2,3)[\"UZ]}
\end{endiagram}
\end{beispiel}
\secidx*{axes ticks and labels}

\section{Actual Values}\secidx{actual values}\label{sec:reale_werte}
\subsection{The Basics}
If you want to have a more quantitative diagram or use actual values for the
energies you can use these options:
\begin{beschreibung}
 \Option{energy-unit}{<unit>}\Default The unit of the energy scale. A unit in
 the \paket{siunitx} sense.
 \Option{energy-step}{<num>}\Default{1} Determines which increment on the energy
 scale corresponds to the \ac{su}.
 \Option{energy-zero}{<num>}\Default{0} Shifts the origin of the energy scale by
 \code{<num>} in multiples of the energy scale.
 \Option{energy-unit-separator}{<anything>}\Default{/} Separates the $y$ axes
 label from the unit.
 \Option{energy-round}{<num>}\Default{3} Rounds the value to this number of figures.
 \Option{energy-round-places}{\default{true}|false}\Default{false} Switch rounding
 mode to places.
\end{beschreibung}
Choosing a unit will add ticks and labels to the $y$ axis automatically and has
an impact on the commands \cmd{ShowGain} and \cmd{ShowEa}, see section~\ref{ssec:real_einfluss}.

\begin{beispiel}
\begin{endiagram}[ticks,y-label=above,energy-step=10]
 \ENcurve{1,3,2}
\end{endiagram}
\begin{endiagram}[y-label=above,energy-step=10,energy-zero=30]
 \ENcurve{1,3,2}
 \AddAxisLabel*{1;2;3}
\end{endiagram}
\end{beispiel}

\begin{beispiel}
\begin{endiagram}[y-label=above,energy-step=15,energy-zero=30,energy-unit=\kilo\joule]
 \ENcurve{1,3,2}
 \AddAxisLabel*{1;2;3}
\end{endiagram}
\begin{endiagram}[y-label=above,energy-step=10,energy-unit=\kilo\joule,energy-unit-separator={ in }]
 \ENcurve{1,3,2}
 \AddAxisLabel*{1;2;3}
\end{endiagram}
\end{beispiel}

\begin{beispiel}
\ENsetup{energy-unit=kJ,energy-step=10,energy-zero=.613,y-label-offset=20pt}
\begin{endiagram}
 \ENcurve{1.0613,4.3465,2.9876}
 \ShowGain[label]
\end{endiagram}

\begin{endiagram}[energy-round-places]
 \ENcurve{1.0613,4.3465,2.9876}
 \ShowGain[label]
\end{endiagram}
\end{beispiel}

\subsection{Impact on Other Commands}\label{ssec:real_einfluss}
Using the option \key{energy-unit} changes the default labels of \cmd{ShowGain}
and \cmd{ShowEa}. Now an actual value is shown:
\begin{beispiel}[below]
 % uses \DeclareSIUnit{\calory}{cal}
\sisetup{per-mode = fraction}
\ENsetup{
  energy-step           = 100,
  energy-unit           = \kilo\calory\per\mole,
  energy-unit-separator = { in },
  y-label               = above,
  AddAxisLabel/font     = \fontfamily{fxlf}\selectfont\footnotesize
}
\begin{endiagram}[scale=1.5]
 \ENcurve{2.232,4.174,.308}
 \AddAxisLabel*{0;1;2;3;4}
 \ShowEa[label,connect={draw=none}]
 \ShowGain[label]
\end{endiagram}
\end{beispiel}

This behaviour can be switched off, though:
\begin{beschreibung}
 \Option{calculate}{\default{true}|false}\Default{true}
\end{beschreibung}
\begin{beispiel}
 % uses \DeclareSIUnit{\calory}{cal}
\sisetup{per-mode = fraction}
\ENsetup{
  energy-step           = 100,
  energy-unit           = \kilo\calory\per\mole,
  energy-unit-separator = { in },
  y-label               = above,
  AddAxisLabel/font     = \footnotesize,
}
\begin{endiagram}[scale=1.5,calculate=false]
 \ENcurve{2.232,4.174,.308}
 \AddAxisLabel*{0;1;2;3;4}
 \ShowEa[label,connect={draw=none}]
 \ShowGain[label]
\end{endiagram}
\end{beispiel}
\secidx*{Actual values}

\section{Example}
The illustration of the Bell-Evans-Polanyi principle (figure~\ref{fig:bell-evans-polanyi})
serves as an example for a more complex usage. One reaction is coloured as it
an exception to the principle. The figure is a reproduction of a similar figure
in~\cite{brueckner}.
\begin{beispiel}[code and float]
% uses the packages `chemmacros', `chemfig' and `libertine'
\begin{figure}[h]
\centering
\setatomsep{1.5em}
\DeclareChemIUPAC\iso{\textit{i}}
\chemsetup[chemformula]{font-family=fxl}
\ENsetup{
  ENcurve/minima,
  AddAxisLabel/font=\fontfamily{fxlf}\selectfont\footnotesize
}
\begin{endiagram}[
   tikz         = {yscale=1.5}, scale        = 1.7,
   y-label      = above,        y-label-text = $\Delta H$,
   x-label      = right,        x-label-text = RK,
   energy-step  = 10]
 \ENcurve{0,3.5,1}
 \ENcurve[tikz=red]{0,3.7,.4}
 \ENcurve{0,4.3[.2],2.4}
 \ENcurve{0,4.7[.3],2.7}
 \ENcurve{0,4.9[.35],2.9}
 \ENcurve{0,5.2[.4],3.3}
 \AddAxisLabel*{1;2;3;4;6}
 \AddAxisLabel{(N1-1)[0];(N1-2)[35];(N2-2)[37];(N3-2)[43];(N4-2)[47];(N5-2)[49];(N6-2)[52]}
 \draw[right] (N1-3) ++ (1,0) node {\small \ch{2 "\chemfig{=_[:30]-[::-60]\lewis{0.,}}~" + N2} } ;
 \draw[right,red] (N2-3) ++ (1,-.3) node {\small \ch{2 "\chemfig{[:-60]*6(=-=-(-\lewis{0.,})=-)}~" + N2} } ;
 \draw[right] (N3-3) ++ (1,-.2) node {\small \ch{2 "\tert-\lewis{0.,Bu}~" + N2} } ;
 \draw[right] (N4-3) ++ (1,-.1) node {\small \ch{2 "\iso-\lewis{0.,Pr}~" + N2} } ;
 \draw[right] (N5-3) ++ (1,0) node {\small \ch{2 "\lewis{0.,Et}~" + N2} } ;
 \draw[right] (N6-3) ++ (1,0) node {\small \ch{2 "\lewis{0.,Me}~" + N2} } ;
 \draw[above,font=\fontfamily{fxlf}\selectfont\footnotesize]
   (N1-3) node {10} (N2-3) node[red] {4}
   (N3-3) node {24} (N4-3) node {27}
   (N5-3) node {29} (N6-3) node {33} ;
\end{endiagram}

\setatomsep{2em}
\schemestart
 \chemfig{R-[:30]N=N-[:30]R}
 \arrow{->[$\Delta$]}[,2.1]
 \ch{2 "\lewis{0.,R}~" + N2}
\schemestop
\caption{\label{fig:bell-evans-polanyi}Enthalpie-Entwicklung entlang
der Reaktionskoordinate bei einer Serie von Thermolysen aliphatischer
Azoverbindungen. Alle Thermolysen dieser Serie -- mit Ausnahme der
farbig hervorgehobenen -- folgen dem Bell-Evans-Polanyi-Prinzip~\cite{brueckner}.}
\end{figure}
\end{beispiel}

\clearpage
{\RaggedRight
\printbibliography}

\setindexpreamble{Section titles are indicated \textbf{bold}, packages
\textsf{sans serif}, commands \code{\textbackslash\textcolor{code}{brown}},
 options \textcolor{key}{\code{yellow}} and modules \textcolor{module}{\code{blue}}.
 \par\bigskip}
\printindex

\end{document}