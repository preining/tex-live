The mandi package  provides commands  for typesetting symbols, expressions, and
quantities used in introductory physics and astronomy. Many of the commands are
inspired by  Matter & Interactions  by Ruth Chabay  and Bruce Sherwood. Many of
the  astronomical commands  were inspired by my own classroom needs. It must be
understood that mandi does  not do any  computations! It only provides commands
for typesetting.

Run  the  file  mandi.dtx  through (pdf)LaTeX  to  generate README (this file),
mandi.ins, mandi.sty,  vdemo.py,  and  mandi.pdf (user documentation). I assume
a TeXLive2011 or later distribution is installed.
