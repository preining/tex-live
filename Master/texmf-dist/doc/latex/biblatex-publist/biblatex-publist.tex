\documentclass{article}
\usepackage{charter}
\usepackage[scaled=0.87]{beramono}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{listings}
\lstset{basicstyle={\ttfamily}}
\usepackage{url}
\usepackage[unicode=true,
 bookmarks=true,bookmarksnumbered=false,bookmarksopen=false,
 breaklinks=false,pdfborder={0 0 0},backref=false,colorlinks=false]
 {hyperref}
\hypersetup{pdftitle={The biblatex-publist manual},
 pdfauthor={J�rgen Spitzm�ller},
 pdfkeywords={biblatex,publication list}}

\newcommand*\jmacro[1]{\textbf{\texttt{#1}}}
\newcommand*\joption[1]{\textbf{\texttt{#1}}}
\newcommand*\jfmacro[1]{\texttt{#1}}
\newcommand*\bpl{\texttt{biblatex-publist}}
\newcommand*\bibltx{\texttt{biblatex}}

\def\condbreak#1{%
\vskip 0pt plus #1\pagebreak[3]\vskip 0pt plus -#1\relax}


\begin{document}

\title{biblatex-publist}


\author{J�rgen Spitzm�ller%
\thanks{\protect\href{mailto:juergen@spitzmueller.org}{juergen (at) spitzmueller (dot) org}.%
}}


\date{Version 0.4, 30/10/2012}
\maketitle
\begin{abstract}
The \bpl\ package provides a \emph{biblatex bibliography style file}
({*}.bbx) for publication lists, i.\,e.\ a bibliography containing
one's own publications. The style file draws on \bibltx 's \emph{authoryear}
style, but provides some extra features needed for publication lists,
such as the omission of the own name from author or editor data. The
package requires at least version 2.0 of the \bibltx\ package.%
\footnote{For \bibltx , see \url{http://www.ctan.org/tex-archive/macros/latex/contrib/biblatex}.%
}
\end{abstract}

\section{Aim of the package}

The \bpl\ package ships a \emph{biblatex bibliography style file
}({*}.bbx) for a specific task: academic publication lists. Such lists,
usually part of the academic CV, contain all or selected publications
of a specific author, usually sorted by genre and year. Even though
publication lists are actually nothing else than (specific) bibliographies,
they diverge from those in some respects. Most notably, it is widespread
practice to omit your own name in your publication list and only list
your co-authors, if there are any. If you want to follow this practice,
a normal bibliography style does not produce the desired result.

Given the fact that maintaining a publication list is a routine task
in an academian's life, it is surprising how few specified solutions
exist to generate such lists (particularly from Bib\TeX{} data). For
traditional Bib\TeX{}, Nicolas Markey has written a specific Bib\TeX{}
style file, \emph{publist.bst}%
\footnote{\url{http://www.lsv.ens-cachan.fr/~markey/BibTeX/publist/?lang=en};
see also \cite{ttb}.%
}, which helps a lot if you want to produce a publication list with
Bib\TeX{}. For \bibltx , however, no equivalent solution exists yet.
The \bpl\ package is the result of the aim of emulating the features
of \emph{publist.bst} with \bibltx's means. It draws on Nicolas Markey's
conceptual ideas, and it is in an early stage of development. Bug
reports, comments and ideas are welcome.


\section{Loading the package}


\subsection{Standard usage\label{sec:standard-usage}}

The standard way of using the package is to load the style file via
\begin{quote}
\begin{lstlisting}[language={[LaTeX]TeX},moretexcs={[1]{omitname}}]
\usepackage[bibstyle=publist]{biblatex}
\omitname[first name]{surname}
\end{lstlisting}

\end{quote}
The \jmacro{\textbackslash{}omitname} macro (at least with the mandatory
\emph{surname} argument) needs to be given once. It tells the style
file which name it should suppress in the author\slash{}editor list
(usually yours). That is to say: For all of your publications where
you are the sole author or editor, the author\slash{}editor name
will be omitted completely, as in:
\begin{quote}
\textbf{2012.} Some recent trends in gardening. In: \emph{Gardening
Practice} 56, pp. 34--86.
\end{quote}
If there are co-authors\slash{}co-editors, your name will be filtered
out and the collaborators added in parentheses, as in:
\begin{quote}
\textbf{1987} (with John Doe and Mary Hall). Are there new trends
in gardening? In: \emph{Gardening Practice} 24, pp. 10--15.
\end{quote}

\subsection{Additional options}

Currently, there are the following additional options (next to the
options provided by the \bibltx\ package itself%
\footnote{Please refer to the \bibltx\ manual \cite{bibltx} for those.%
}):
\begin{description}
\item [{\joption{omitname}}] \joption{=<surname>}
\item [{\joption{omitfirstname}}] \joption{=<first name>}


This is an alternative to the \jmacro{\textbackslash{}omitname} macro
described in sec.~\ref{sec:standard-usage}. However, due to the
way bibliography options are implemented in \bibltx, this only works
if your name does not consists of non-ASCII characters. Hence, the
\jmacro{\textbackslash{}omitname} macro is the recommended way.

\item [{\joption{boldyear}}] \joption{{[}=true|false{]}} default: \emph{true}.


By default, the year is printed in bold. To prevent this, pass the
option \joption{boldyear=false} to \bibltx .

\item [{\joption{marginyear}}] \joption{{[}=true|false{]}} default: \emph{false}.


With this option set to \joption{true}, the publication year will
be printed in the margin once a new year starts. The option also has
the effect that all marginpars are printed ``reversed'', i.\,e.
on the left side in one-sided documents (via \jfmacro{\textbackslash{}reversemarginpar}).

\end{description}
The appearance of the \emph{marginyear} is controlled by the \jmacro{\textbackslash{}plmarginyear}
macro, which has the following default definition:
\begin{quote}
\begin{lstlisting}[language={[LaTeX]TeX},moretexcs={[2]{providecommand,plmarginyear}}]
\providecommand*\plmarginyear[1]{%
  \raggedleft\small\textbf{#1}%
}
\end{lstlisting}

\end{quote}
If you want to change the appearance, just redefine this macro via
\jfmacro{\textbackslash{}renewcommand{*}}.


\section{Localization}

Since the package draws on \bibltx , it supports localization. Currently,
the following languages are supported: English, French and German.%
\footnote{Please send suggestions for other languages to the package author.%
} The following additional localization keys (\jfmacro{\textbackslash{}bibstrings})
are added by the package:
\begin{itemize}
\item \emph{with}: the preposition ``with'' that precedes the list of
co-authors.
\item \emph{parttranslationof}: the expression ``partial translation of''
for entries referring to partially translated work via \bibltx 's
``related entries'' feature (see sec.~\ref{sec:partial-translations}).
\end{itemize}

\section{Further Extensions}

The following extensions of standard \bibltx\ features are provided.


\subsection{Review bibliography type\label{sub:review-bibliography-type}}

Although a \emph{review} entry type is provided by \bibltx , this
type is treated as an alias for \emph{article}. The \bpl\ package
uses this entry type for a specific purpose: Foreign reviews of your
own work. It therefore defines a new bibliography environment \emph{reviews}
with a specific look (particularly as far as the author names are
concerned) and its own numbering; furthermore, it redefines the \emph{review}
bibliography driver. The purpose of this is that you can add other
people's reviews of your work to your publication list, while these
titles are clearly marked and do not interfere with the overall numbering
(see sec.~\ref{sec:example} for an example).


\subsection{Partial translations\label{sec:partial-translations}}

A new ``related entry'' type \emph{parttranslationof} is provided.
This is an addition to the \emph{translationof} related entry type
\bibltx\ itself provides. Please refer to the \bibltx\ manual \cite{bibltx}
on what ``related entries'' are and how to use them.


\section{An example\label{sec:example}}

Publication lists are usually categorized by genre (monographs, articles,
book chapters, etc.). For this task, we use \jfmacro{\textbackslash{}refsections}.
Other possibilities were not tested and might fail.

The suggested procedure is to maintain separate bib files for each
category, say \emph{mymonographs.bib}, \emph{myarticles.bib}, \emph{myproceedings.bib}.%
\footnote{But see sec.~\ref{sec:filtering} for an alternative.%
} Then a typical file would look like in Listing~\ref{example}.

\begin{lstlisting}[caption={Example document},float,frame=single,label={example},language={[LaTeX]TeX},moretexcs={[4]{omitname,addbibresource,printbibliography,maketitle}}]
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}

\usepackage{csquotes}% not required, but recommended
\usepackage[bibstyle=publist]{biblatex}
\omitname[John]{Doe}

\addbibresource{%
    mymonographs.bib,
    myarticles.bib,
    myproceedings.bib
}

\begin{document}

\title{John Doe's publications}
\date{\today}
\maketitle

\section{Monographs}
\begin{refsection}[mymonographs]
\nocite{*}
\printbibliography[heading=none]
\end{refsection}

\section{Proceedings}
\begin{refsection}[myproceedings]
\nocite{*}
\printbibliography[heading=none]
\end{refsection}

\section{Articles}
\begin{refsection}[myarticles]
\nocite{*}
\printbibliography[heading=none]
\end{refsection}

\end{document}
\end{lstlisting}


If you want to add other people's reviews of your work, add a section
such as:
\begin{quote}
\begin{lstlisting}[frame=single,language={[LaTeX]TeX},moretexcs={[3]{bibfont,subsubsection,printbibliography}}]
\subsubsection*{Reviews of my thesis}
\begin{refsection}[mythesis-reviews]
\renewcommand\bibfont{\small}
\nocite{*}
\printbibliography[heading=none,env=reviews]
\end{refsection}
\end{lstlisting}

\end{quote}
Note that the \jfmacro{\textbackslash{}printbibliography} option
\joption{env=reviews}  is crucial if you want to use the specifics
\bpl\ defines for reviews (see sec.~\ref{sub:review-bibliography-type}).


\section{Filtering\label{sec:filtering}}

If you have a bibliographic database consisting not only of your own
publications, you can extract yours with the bibliography filter \joption{mine},
which has to be passed to \jfmacro{\textbackslash{}printbibliography},
as in:
\begin{quote}
\begin{lstlisting}[frame=single,language={[LaTeX]TeX},moretexcs={[1]{printbibliography}}]
\begin{refsection}[mybibliography]
\nocite{*}
\printbibliography[heading=none,filter=mine]
\end{refsection}
\end{lstlisting}

\end{quote}
Of course, you can also use other filter possibilities provided by
\bibltx , such as filtering by type or by keyword. So if you want
to extract all of your articles from a larger database with entries
of diverse type and authors, specify:
\begin{quote}
\begin{lstlisting}[language={[LaTeX]TeX},moretexcs={[1]{printbibliography}}]
\printbibliography[heading=none,filter=mine,type=article]
\end{lstlisting}
\condbreak{2\baselineskip}
\end{quote}

\section{Change Log}
\begin{description}
\item [{V.~0.4~(2012-10-30):}]~

\begin{itemize}
\item More robust name parsing (especially for names with non-ASCII characters
encoded with \LaTeX{} macros). The code was kindly suggested by Enrico
Gregorio.%
\footnote{Cf. \url{http://tex.stackexchange.com/questions/79555/biblatex-bibliographyoption-with-braces}.%
}
\item Add \jmacro{\textbackslash{}omitname} command.
\item Support \joption{firstinits} option.
\end{itemize}
\item [{V.~0.3~(2012-10-23):}]~

\begin{itemize}
\item Bug fix: Add missing ``and'' if omitted name was last minus one.
\item Bug fix: Fix output with ``et al.'' if omitted name is first and
\emph{liststop} is 1.
\item Set \joption{maxnames} default to 4.
\item Add filter possibility (see sec.~\ref{sec:filtering}).
\item Add French localization.
\item Some corrections to the manual.
\end{itemize}
\item [{V.~0.2~(2012-10-21):}] Initial release to CTAN.
\end{description}

\section{Credits}

Thanks go to Enrico Gregorio (egreg on \emph{tex.stackexchange.com})
for helping me with correct name parsing (actually, the code the package
uses is completely his), Marko Budi�i\'{c} for testing and bug reports,
Nicolas Markey for \emph{publist.bst} and of course Philipp Lehman
(not only) for \bibltx.
\begin{thebibliography}{1}
\bibitem{bibltx}Lehman, Philipp (with Audrey Boruvka, Philip Kime
and Joseph Wright): \emph{The biblatex Package. Programmable Bibliographies
and Citations}. August 2012. \url{http://www.ctan.org/tex-archive/macros/latex/contrib/biblatex}.

\bibitem{ttb}Markey, Nicolas: \emph{Tame the BeaST. The B to X of
BibTEX}. October 11, 2009. \url{http://www.ctan.org/tex-archive/info/bibtex/tamethebeast}.\end{thebibliography}

\end{document}
