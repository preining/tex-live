%% psbaomanual.tex
% This program consists of the files:
%
%   psbao.sty
%   psbaomanual.tex
%
%
\documentclass[a4paper]{book}
\usepackage{psbao,array,url,lettrine,float,multicol}
\newcommand{\pstricks}{\textsf{PSTricks}}
\newcommand{\commandname}[1]{{\ttfamily\symbol{'134}#1}}
% The following code is taken from the doc package. It defines a global 
% macro \bslash that produces a bslash (if present in the current font). 
\makeatletter
{\catcode`\|=\z@ \catcode`\\=12 |gdef|bslash{\}}
\makeatother
\newcommand{\cmd}[1]{\texttt{\bslash #1}}

\title{\psbao\\{\normalfont\normalsize Typesetting Bao Diagrams with \pstricks}}
\author{Nino Vessella\\web site: www.vessella.it}
\date{October, 2008}

\setbaounit{0.5cm}

\begin{document}
\maketitle

\section*{Introduction}

\lettrine[lines=2]{\textbf{T}}{his package} provides functionality to typeset Bao diagrams in \LaTeXe. It has been created using the Go package written by Victor Bos (September 4, 2008) as basis and it is built on top of the \pstricks{} package. 

To all the people who have never played bao I suggest to visit \textit{\url{http://www.swahili.it/bao}}.

\section*{Download and installation}

\lettrine[lines=2]{\textbf{T}}{he} \psbao{} package can be found at CTAN (mirrors) in the directory\linebreak 
\url{graphics/pstricks/contrib/psbao/}.

\noindent It is also available from
\url{http://www.vessella.it/ghala/}.
\medskip

To install the package, download the files
\texttt{psbao.sty} and \texttt{psbaomanual.tex} and put them in a
directory where \LaTeX{} can find them. After that, test the
installation by running \LaTeX{} on \texttt{psbaomanual.tex}. 

Note that \textsf{psbao} uses \textsf{pstricks} to draw
graphics. Therefore, the same things that apply to viewing and
printing documents with \textsf{pstricks} graphics also apply to
documents with \textsf{psbao} graphics. In particular, it is usually
better to use a PostScript viewer (e.g., \texttt{gv} or
\texttt{gsview}) instead of a DVI viewer (like \texttt{xdvi} or
\texttt{yap}).

\section*{Bao boards}

\lettrine[lines=2]{\textbf{T}}{he} interface of \psbao{} is based on the notation used in\linebreak\url{http://www.swahili.it/bao/}. That is, the rows of the $4\times8$ board are indexed by $1,2,\ldots,8$ and the columns are indexed by $b, a, A, B$.

First of all it must be set the initial position of a Bao game. For the standard initial setting is used the command \verb|\initbao|, but the board is showed by the environmnet \verb|\begin{showbaoboard}| $\dots$ \verb|\end{showbaoboard}| (Figure~\ref{fig:ko:standard}):

\begin{figure}[!ht]
\setlength{\columnsep}{20pt}\small
\begin{multicols}{2}
\noindent Code:\\
\mbox{}\\
\begin{verbatim}
\initbao
\begin{figure}
 \begin{center}
  \begin{showbaoboard}
  \end{showbaoboard}
  \caption{...}
  \label{fig:...}
 \end{center}
\end{figure}
\end{verbatim}
\mbox{}\\
Prints:\\
\mbox{}\\
    \begin{showbaoboard}
    \end{showbaoboard}
\mbox{}\\
\caption{Setting up the standard initial situation: default size 0.6cm}
\label{fig:ko:standard}
\end{multicols}
\end{figure}


For the initial setting of the beginner's variant \textit{Bao la kujifunza} is used the command \verb|\initjbao| (Figure~\ref{fig:empty:jifunza}):

\begin{figure}[!ht]
\setlength{\columnsep}{20pt}\small
\begin{multicols}{2}
\noindent Code:\\
\mbox{}\\
\begin{verbatim}
\initjbao
\begin{figure}
 \begin{center}
  \begin{showbaoboard}
  \end{showbaoboard}
  \caption{...}
  \label{fig:...}
 \end{center}
\end{figure}
\end{verbatim}
\mbox{}\\
Prints:\\
\mbox{}\\
\initjbao
  \begin{showbaoboard}
  \end{showbaoboard}
\mbox{}\\
  \caption{Bao la kujifunza}
  \label{fig:empty:jifunza}
\end{multicols}
\end{figure}

The default horizontal unit distance is~$0.6$cm. The vertical unit distance is computed by the \psbao{} package.
To re-size a Bao board, the command
\verb|\setbaounit| can be used. This command takes the desired
horizontal unit distance as an argument. It is not advisable, though possible, to set the unit distance to a value less than 0.3cm, see Figure~\ref{fig:empty:resize}(b).

\initbao
\weka{0}{n}{0}
\weka{0}{a}{5} % 3,0
\weka{0}{a}{6} % 2,1
\weka{0}{a}{7} % 1,2
\weka{0}{A}{5} % 0,3
\weka{0}{A}{6}
\weka{0}{A}{7}
\weka{0}{s}{0}
\weka{10}{b}{2} % 3,0
\weka{27}{a}{2} % 2,1
\weka{33}{A}{2} % 1,2
\weka{4}{B}{2} % 0,3
\weka{10}{A}{1}
\begin{figure}[H]
 \begin{center}
  \begin{tabular}{cc}
   \setbaounit{.5cm}
   \begin{showbaoboard}
   \end{showbaoboard}
&
   \setbaounit{0.3cm}
    \weka{10}{b}{2} % 3,0
    \weka{27}{a}{2} % 2,1
    \weka{33}{A}{2} % 1,2
    \weka{4}{B}{2} % 0,3
    \weka{10}{A}{1}
   \begin{showbaoboard}
   \end{showbaoboard}
\\
\\
(a) Horizontal step 0.5cm.
&
(b) Horizontal step 0.3cm.
\end{tabular}
  \caption{Resized Bao boards}
  \label{fig:empty:resize}
 \end{center}
\end{figure}

For the diagrams in this document we have set the horizontal
unit distance to~$0.5$cm (\verb|\setbaounit{0.5cm}|).

\section*{Seeds and situations}

\lettrine[lines=2]{\textbf{T}}{he command} to put seeds into the holes is \verb|\weka| which takes three parameters: the number of seeds, the row and the column of the hole. For example, \verb|\weka{2}{a}{4}| puts two seeds into the hole at position~$(a,4)$. The \verb|\weka| command can be used to modify a previous situation. For instance, the situation of Figure~\ref{fig:ko:situation} is defined as follows.

\begin{figure}[!ht]
\setlength{\columnsep}{20pt}\small
\begin{multicols}{2}
\noindent Code:\\
\mbox{}\\
\begin{verbatim}
\weka{1}{b}{2} % 3,0
\weka{2}{a}{2} % 2,1
\weka{3}{A}{2} % 1,2
\weka{4}{B}{2} % 0,3
\weka{5}{A}{1}
\weka{9}{A}{3}
\begin{showbaoboard}
\end{showbaoboard}
\end{verbatim}
Prints:\\
\mbox{}\\
\weka{1}{b}{2} % 3,0
\weka{2}{a}{2} % 2,1
\weka{3}{A}{2} % 1,2
\weka{4}{B}{2} % 0,3
\weka{5}{A}{1}
\weka{9}{A}{3}
\begin{showbaoboard}
\end{showbaoboard}
\mbox{}\\
  \caption{Example of weka}
  \label{fig:ko:situation}
\end{multicols}
\end{figure}

The same situation can be set by \verb|\initbao| which takes one optional parameter to indicate the number of seeds contained in all holes (Figure~\ref{fig:ko:sit}).

\begin{figure}[!ht]
\setlength{\columnsep}{20pt}\small
\begin{multicols}{2}
\noindent Code:\\
\mbox{}\\
\begin{verbatim}
\initbao[0,%
        0,0,0,0,0,0,1,0,%
        0,0,0,0,0,0,1,0,%
        5,3,0,0,0,0,0,0,%
        0,3,9,0,0,0,0,0,%
        0]
\begin{figure}
\begin{center}
 \begin{showbaoboard}
 \end{showbaoboard}
\end{center}
\end{figure}
\end{verbatim}
Prints:\\
\mbox{}\\
\initbao[0,%
            0,0,0,0,0,0,1,0,%
            0,0,0,0,0,0,1,0,%
            5,3,0,0,0,0,0,0,%
            0,3,9,0,0,0,0,0,%
            0]
  \begin{showbaoboard}
  \end{showbaoboard}
\mbox{}\\
  \caption{Writing the number of seeds of all holes}
  \label{fig:ko:sit}
\end{multicols}
\end{figure}

To put seeds into the stores is used the same command: \verb|\weka|. In this case instead of index of the row can be used the letters $s$ or $S$ for the south store, and the letters $n$ or $N$. For example, \verb|\weka{2}{s}{4}| puts two seeds into the store of South. The third parameter must be any number.
\begin{figure}[!ht]
\setlength{\columnsep}{20pt}\small
\begin{multicols}{2}
\noindent Code:\\
\begin{verbatim}
\begin{showbaoboard}
\weka{10}{n}{2}
\weka{1}{b}{2}
\weka{2}{a}{2}
\weka{3}{A}{2}
\weka{4}{B}{2}
\weka{5}{A}{1}
\weka{9}{A}{3}
\weka{9}{s}{3}
\end{showbaoboard}
\end{verbatim}
\mbox{}\\
\mbox{}\\
Prints:\\
\mbox{}\\
\weka{10}{n}{2}
\weka{1}{b}{2} % 3,0
\weka{2}{a}{2} % 2,1
\weka{3}{A}{2} % 1,2
\weka{4}{B}{2} % 0,3
\weka{5}{A}{1}
\weka{9}{A}{3}
\weka{9}{s}{3}
\begin{showbaoboard}
\end{showbaoboard}
\mbox{}\\
\caption{Setting up a situation with stores}
\label{fig:ko:ghala}
\end{multicols}
\end{figure}

The command: \verb|\weka| keeps the previous situation, so if it is necessary to set a completely new situation it must be used the command \verb|\initbao|.

\section*{Move directions}

\lettrine[lines=2]{\textbf{T}}{o} indicate the direction of move it is possible to use the command \verb|\markpos|, which takes three parameters: the
direction marker, the column, and the row. For example, the move a3\textless\; is shown on the diagram by \verb|\markpos{marksx}{a}{3}| and A3\textgreater\; is shown on the diagram by \verb|\markpos{markdx}{A}{3}|:

\begin{figure}[!ht]
\setlength{\columnsep}{20pt}\small
\begin{multicols}{2}
\noindent Code:\\
\begin{verbatim}
\weka{10}{n}{2}
\weka{3}{a}{3}
\weka{3}{A}{3}
\weka{9}{s}{3}
\begin{showbaoboard}
 \markpos{\marksx}{a}{3}
\end{showbaoboard}
\end{verbatim}
\mbox{}\\
\mbox{}\\
Prints:\\
\weka{10}{n}{2}
\weka{3}{a}{3}
\weka{3}{A}{3}
\weka{9}{s}{3}
\begin{showbaoboard}
 \markpos{\marksx}{a}{3}
\end{showbaoboard}
\mbox{}\\
\caption{The move a3\textless}
\label{fig:ko:left}
\end{multicols}
\end{figure}

\begin{figure}[!ht]
\setlength{\columnsep}{20pt}\small
\begin{multicols}{2}
\noindent Code:\\
\begin{verbatim}
\weka{10}{n}{2}
\weka{3}{a}{3}
\weka{3}{A}{3}
\weka{9}{s}{3}
\begin{showbaoboard}
 \markpos{\markdx}{A}{3}
\end{showbaoboard}
\end{verbatim}
\mbox{}\\
\mbox{}\\
Prints:\\
\weka{10}{n}{2}
\weka{3}{a}{3}
\weka{3}{A}{3}
\weka{9}{s}{3}
\begin{showbaoboard}
 \markpos{\markdx}{A}{3}
\end{showbaoboard}
\mbox{}\\
\caption{The move A3\textgreater}
\label{fig:ko:right}
\end{multicols}
\end{figure}

\section*{Markers}

\lettrine[lines=2]{\textbf{A}}{ hole} on the board can be marked with the command \verb|\markpos|. This command takes three parameters: the
marker, the column, and the row. Available markers and the commands to
generate them are listed in Table~\ref{tbl:markers}.

Note that to mark a non-empty hole the command \verb|\weka| has to be used together with marker command, \verb|\markpos|.

Note, also, that the special holes (as \textit{kichwa} or \textit{kimbi}) can be used the marks \verb|\markdd| or \verb|\markss|, indifferently (but consistently!). Those commands must be inside the environment \verb|\begin{showboard}| $dots$ \verb|\end{showboard}|.

\setbaounit{0.4cm}
\weka{1}{b}{2}
\weka{2}{a}{2}
\weka{3}{A}{2}
\weka{4}{B}{2}
\weka{5}{A}{1}
\weka{9}{A}{3}
\begin{table}
\begin{center}
\begin{tabular}{|c|l|l|l|}
\hline
Diagram & Mark & Description & Command\\
\hline\hline 
\raisebox{0.2\height}
{%
\begin{showbaoboard}
\markpos{\marksx}{a}{2}
\end{showbaoboard}%
} &
\commandname{marksx} & a2\textless  &
{\commandname{markpos}\{\commandname{marksx}\}\{a\}\{2\}}\\\hline
%
\raisebox{0.2\height}
{
\begin{showbaoboard}
\markpos{\markdx}{A}{3}
\end{showbaoboard}
} &
\commandname{markdx}& A3\textgreater &
{\commandname{markpos}\{\commandname{markdx}\}\{A\}\{3\}}\\\hline
%
\raisebox{0.2\height}
{
\begin{showbaoboard}
\markpos{\markss}{a}{2}
\end{showbaoboard}} &
\commandname{markss}& Special hole &
{\commandname{markpos}\{\commandname{markss}\}\{a\}\{2\}}\\\hline
%
\raisebox{0.2\height}
{
\begin{showbaoboard}
\markpos{\markdd}{A}{1}
\end{showbaoboard}} &
\commandname{markdd}& Special hole &
{\commandname{markpos}\{\commandname{markdd}\}\{A\}\{1\}}\\\hline
%
\raisebox{0.2\height}
{
\begin{showbaoboard}
\markpos{\markma}{A}{5}
\end{showbaoboard}} &
\commandname{markma}& Any nr of seeds &
{\commandname{markpos}\{\commandname{markma}\}\{A\}\{5\}}\\\hline
\end{tabular}
\caption{Markers on holes}
\label{tbl:markers}
\end{center}
\end{table}


\end{document}
