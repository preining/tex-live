\documentclass[11pt]{article}
\usepackage{psu-thesis}
\usepackage{texnames} % only needed for \BibTeX macro
\setlength{\paperwidth}{8.5in} \setlength{\paperheight}{11in}
\setlength{\textwidth}{6.5in} \setlength{\textheight}{8.5in}
\setlength{\topmargin}{0in} \setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\begin{document}
\begin{singlespace}
\title{Manual for psu-thesis \LaTeX\ Style File}
\author{Matthew W. Floros}
\date{September 17, 2008}
\maketitle
\section*{Included Files}
\begin{verbatim}
manual.tex
manual.pdf
psu-thesis.sty
psu-thesis.bst
mssample.tex
mssample.pdf
phdsample.tex
phdsample.pdf
\end{verbatim}

\section*{Introduction}

The origin of this style file came directly from my horrible experience trying
to write a technical Master's thesis using ``a popular word processor.''  It
was at that time that I decided that it would be best to bite the bullet
and simply learn \TeX\ and not have to deal with that nightmare again for my
PhD thesis. 

Unfortunately, at the time I was writing my Ph.D. thesis, Penn State did not
provide any support for \TeX\ users. Although there were several style files
floating around different departments, they seemed to me as a novice user to
require quite a bit of effort to use them.  The style file itself had to be
changed, i.e. lines commented out or uncommented, and there was no
documentation.  My goal was to merge together parts of various style files and
make a single package whose features could all be accessed through issuing
simple \LaTeX\ commands in the document rather than editing the style file
itself.

I received my PhD in December of 2000 and did the development work on this
package then.  I wrote most of this documentation in 2002 and then came back to
it in 2008 when I realized I could upload it to CTAN and make it available to
other people.  A fair amount of work went into it and I thought others could
benefit.  As of 2000, the Penn State Thesis Office had no interest in
having anything to do with a \LaTeX\ style file and only maintained templates
for ``popular word processors.''  I'm sure they still feel the same way.

{\bfseries Note that I have not reviewed the thesis guidelines since 2000 and
they may have changed.  When I submitted my thesis in 2000, it passed Thesis
Office scrutiny using this package.  The committee page was contributed from a
2004 thesis, and incorporated ``as-is,'' with appropriate logic to include it or
not.}

There are few guidelines for the main body of the thesis, so nearly all of the
provided commands can be classified as either front matter or back matter. 
These commands are used to set up the title page, abstract, appendices, and so
forth.  There are some commands which do not fall into these two categories
which are provided at the end.  A summary of the commands is provided below.

Note that the setspace package is a separate \LaTeX package, not something
developed at PSU.  It is a required package because the line spacing and some of
the commands in the psu-thesis style file rely on commands in the setspace style
file.  

To include the style file (and bibliographic style file psuthesis.bst) in your
thesis document, simply issue the command \verb+\usepackage{psu-thesis}+ in the
preamble \emph{after} the \verb+documentclass+ command  and
\verb+\usepackage{doublespace}+ command since many of the psu-thesis commands
rely on the doublespace package.  It is also intended to be used with the
\verb+report+ document class:

\begin{verbatim}
\documentclass[dvips,11pt,twoside]{report}
\usepackage{doublespace}
\usepackage{psu-thesis}
\bibliographystyle{psuthesis}
\end{verbatim}

The package was set up with both one-sided and two-sided printing in mind, so
the optional arguments \verb+oneside+ and \verb+twoside+ to the report document
class can be used and the macros provided in this package will adapt
accordingly.

\section*{Package Options}

The following options are available:
\begin{verbatim}
master
doctor
signature
draft
\end{verbatim}

The first two package options, \verb+master+ and \verb+doctor+ are available
which set defaults for those thesis types:

\begin{tabular}{ll}
\verb+master+ & \verb+doctor+ \\ \hline
Set degree to ``Master of Science'' & Set degree to ``Doctor of Philosophy'' \\
Permission to Copy Page             & Committee Page  \\
List of Tables                      & List of Tables    \\
List of Figures                     & List of Figures 
\end{tabular}

The \verb+signature+ option will include the signature page, which has since
been supplanted by the committee page.  The \verb+\includesignature+ command in
the preamble produces the same effect.

The \verb+draft+ option is useful for version control of drafts of your
thesis while it is being written.  It produces a box with the date the document
was produced at the top of every page,

\noindent\usebox{\draftbox}

You can also place the \verb+\draft+ command in the preamble to activate this
feature.

\section*{Front Matter}

Most of the commands provided are related to the front matter.  Commands can be
divided into three groups,  commands which set information for the title page,
commands to set up the signature page, and commands to include and format the
other front matter items.  

The front matter definition commands can be issued either in the preamble or
within the document itself.  The only command that is required to appear within
the document is the \verb+\makefrontmatter+ command which generates and outputs
all of the frontmatter material based on the other commands issued.  Of course
if the commands to create the front matter parts are not in the preamble, they
must appear \emph{before} the \verb+\makefrontmatter+ command.

\subsection*{Title Page Information}

A variety of general information is required for the style file to format the
title page, signature page, and so forth.  These commands to input the
information are straightforward.  The appropriate information is simply the
argument of the command:

\begin{tabular}{ll}
\verb+\title{}+ & thesis title (it should be in all caps; this is not automatic)\\
\verb+\author{}+ & thesis author \\
\verb+\dept{}+ & academic department in which thesis is earned \\
\verb+\college{}+ & college the above department belongs to \\
\verb+\submitdate{}+ & month and year of submission/publication of thesis \\
\verb+\copyrightyear{}+ & year of publication of thesis (for title and copyright
pages) \\
\verb+\degree{}+ & degree being obtained, e.g. ``Doctor of Philosophy'' 
\end{tabular}

The thesis type is set to produce ``A Thesis in...'' by default but can be
changed to ``A Thesis Proposal in...'' with the \verb+\proposal+ command or ``A
Dissertation in'' with the \verb+\dissertation+ command.

Additionally, the \verb+\includecopyrightline+ command will include a copyright
line on the title page using the copyright year and author information supplied.
Alternatively, the command \verb+\includecopyrightpage+ will cause a separate
copyright page to be output immediately following the title page.  Both the
title page line and separate page can be included, but only one is recommended.

\subsection*{Signature/Committee Page Information}

The signature and committee pages can be set up with up to six thesis readers
using the \verb+\includesignature+ and \verb+\includecommittee+ commands,
respectively.  A suite of commands are included to aid in the formatting of the
signature and committee pages.  Headings and spacing are set automatically.  The
author information is taken from the \verb+\author{}+ command above.  Each
reader is added with a consecutive command, i.e.

\begin{verbatim}
\firstreader{}{}
\secondreader{}{}
\thirdreader{}{}
\fourthreader{}{}
\fifthreader{}{}
\sixthreader{}{}
\end{verbatim}

Each reader command requires two arguments.  The first is the reader's name, and
the second is the title and affiliation of that reader, for example

\noindent\verb+\firstreader{Isaac Newton}{Professor of Mathematics}+ 

\noindent would produce \vspace{0.25in}

\signature{Isaac Newton \\ Professor of Mathematics \\}

\vspace{0.25in}

\noindent for the signature page and similar without the lines for the committee
page. The signature page is optional, and can be included with 
\verb+\includesignature+ in the preamble or the \verb+signature+ package option.
It is not numbered, so it can be bound with the author's personal copy and the
page numbers will not differ from the reference copy.

Several macros have been defined to format the reader affiliations.  First,
macros are defined to identify the professor as assistant, associate, or full
professor:
\begin{verbatim}
\assistprof{}
\assocprof{}
\prof{}
\end{verbatim}
\noindent Additionally, the macro \verb+head{}+ is defined to identify that the
reader is the head of the department.  The Thesis Office likes to see both
\verb+\prof{}+ and \verb+\head{}+ for department heads.

Macros are provided for additional information as follows:

\begin{tabular}{ll}
Macro Command & Text Produced \\ \hline
\verb+\adviser+ & Thesis Adviser \\
\verb+\coadviser+ & Thesis Co-Adviser \\
\verb+\chair+ & Chair of Committee \\
\verb+\cochair+ & Co-Chair of Committee \\
\verb+\adviserchair+ & Thesis Adviser, Chair of Committee \\
\verb+\coadviserchair+ & Thesis Co-Adviser, Co-Chair of Committee
\end{tabular}

These macros are not necessarily of much value since it is nearly as easy to
simply type the appropriate information into the document rather than using the
macros, but several style files I came across contained such shortcuts, so they
were retained in this package.

For example, either

\begin{verbatim}
\secondreader{Isaac Newton}{\prof{Mathematics}\head{Mathematics}
\adviserchair} \end{verbatim}

\noindent or

\noindent\begin{verbatim}
\secondreader{Isaac Newton}{Professor of Physics \\ 
Head of Department of Physics \\ Thesis Adviser, Chair of Committee}
\end{verbatim}
 
\noindent would produce \vspace{0.25in}

\signature{Isaac Newton \\ Professor of Physics \\
  Head of Department of Physics \\ Thesis Adviser, Chair of Committee}

\vspace{0.25in}


\subsection*{Permission to Copy Page}

The permission to copy page is produced with the \verb+\includepermission+
command.  The name is taken from the \verb+\author+ entry.  No other input is
required since the required text is given in the thesis guide.

\subsection*{List of Symbols}

Three macros are included for the List of Symbols.  The first,
\verb+\listofsymbols[width]{}+, causes the List of Symbols to be included in the
frontmatter.  The List of Symbols itself is the required argument, which can of
course be entered directly or in a separate file and accessed with an
\verb+\input+ or similar command.  

The second macro is to aid in generating the symbol list and takes the form

\noindent \verb+\symbolentry{symbol}{definition}+  

The command outputs the symbol and definition in two columns.  The definition
column is in paragraph form, so manual line breaking is not necessary.  The
width of the symbol column is set by the optional argument \verb+[width]+ in
the \verb+\listofsymbols[width]{}+ command.  The default is 1 inch.  The
definition column will be the remainder of the page, i.e.
\verb+\textwidth-width+.  To change the width in the middle of the list of
symbols, use the command \verb+\setsymwidth{width}+.  It redefines the symbol
width and automatically adjusts the description width to the remainder of the
page.

By default, the first argument is output in math mode, but plain
text symbols can be created with \verb+\mbox{}+.  For example,
\verb+\symbolentry{g}{Acceleration due to Gravity}+ produces

\symbolentry{g}{Acceleration due to gravity}

\subsection*{Unformatted Front Matter Pages}

The remaining frontmatter pages do not include any formatting, hence commands
are provided for the sole purpose of determining whether to include each page or
not, and placing them in the correct order.   They can be defined in any order
but will appear in a specific order when the document is processed.  These front
matter items can be included are in two groups, commands which merely include
the associated item, and commands which include the item and define its
contents.  The items will be output in the following order if the corresponding
command is present:

\begin{tabular}{ll}
Macro Command & Front Matter Item \\ \hline
\verb+\includecopyrightpage+      & Copyright Page \\
\verb+\includepermission+         & Permission to Copy Page (For MS Theses) \\
\verb+\includesignature+          & Signature Page (For PhD Theses) \\
\verb+\includecommittee+          & Committee Page (For PhD Theses) \\
\verb+\dedicationtext{content}+   & Dedication \\
\verb+\abstracttext{content}+     & Abstract \\
\multicolumn{2}{c}{(Table of Contents)} \\
\verb+\includelistoffigures+      & List of Figures \\
\verb+\includelistoftables+       & List of Tables \\
\verb+\listofsymbols[wid]{content}+ & List of Symbols \\
\verb+\dedicationtext{content}+   & Dedication \\
\verb+\prefacetext{content}+      & Preface \\
\verb+\acknowltext{content}+      & Acknowledgements \\
\verb+\epigraphtext{content}+     & Epigraph \\
\verb+\frontispiece{content}+     & Frontispiece
\end{tabular}
\vspace{12pt}

For my thesis the content for each of these was an \verb+\include+ command to
include a separate file, but you can put your actual content in the braces if
you like.

\section*{Back Matter}

There is considerably less back matter than front matter, but unfortunately, the
thesis office wants some of the items to appear differently than the \LaTeX\
defaults.  The following macros are provided to format the back matter items
to the thesis office standards.

\subsection*{Appendices}

Two commands are provided for inclusion of appendices, \verb+\singleappendix+
and \verb+\appendices+.  The reason for these is that the thesis office wants a
single appendix to be called ``Appendix'' and \LaTeX\ treats it as a chapter and
by default if the \verb+\appendix+ command was issued, would call it ``Appendix
A'' and would put ``A'' in the table of contents.  The provided macros
\verb+\singleappendix+ and \verb+\appendices+ format the appendix/appendices to
be called ``Appendix'' in both the text and the table of contents in the case of
a single appendix and have the letters prefaced by the word ``Appendix'' in the
table of contents in the case of multiple appendices.  

The appropriate command
should be issued, followed by a \verb+\chapter+ command to provide a title for
the appendix, 

\begin{verbatim}
\singleappendix \chapter{Appendix name}
<text of appendix>
\end{verbatim}

\noindent or 

\begin{verbatim}
\appendices \chapter{First appendix name}
<text of appendix>

\chapter{Second appendix name}
<text of appendix>
\end{verbatim}

\noindent etc.

\subsection*{Bibliography Entry} Unfortunately, the bibliography is also not
included in the table of contents by default.  A command is provided to include
the bibliography in the table of contents and single space the list of
references.  The command is \verb+\includebibliography{}+.  Unlike the other
``include'' commands, the \verb+\includebibliography{}+ command takes an
argument which is the bibliography itself.  This is for single space
formatting.  Simply include the bibliographic entries as its argument to print
out the bibliography.  Using \LaTeX features is fine, so the command would
likely be

\noindent \verb+\includebibliography{\bibliography{refs}}+

\noindent if \BibTeX\ is used. The psuthesis.bst style file is included for
producing a bibliography with \BibTeX.

\subsection*{Vita}

A command \verb+\vita{}+ is provided to include a vita/\emph{curriculum vitae}
at the end of the thesis.  It is not required by the thesis office, but many
like to include it.  The argument is the vita itself, most likely an
\verb+\input{}+ or \verb+\include+ command linking to a file containing the
vita.  The macro provides a clean page and the author name from the front
matter.

\subsection*{UMI Abstract}  

The UMI Abstract is required by the thesis office for PhD theses.  UMI keeps
your thesis on file and if you read the fine print, keeps your royalties unless
a lot of people request your thesis in a given year.  The UMI abstract is not
bound with the thesis, but it can be placed at the end so a whole separate
document is not required.  The command \verb+\UMIabstract{}+ formats the
specific header required by UMI using information from the front matter and
includes the abstract supplied as the argument.  The UMI abstract has a word
limit (consult the thesis guide for details), but if the thesis abstract falls
within the word limit it can be the same for both.  The abstract is printed
without a page number since it is not bound with the thesis.

\section*{Numbering by Chapter}

The \verb+\numberbychapter+ command changes the numbering scheme for figures,
tables, and equations to follow the chapters so that they are numbered 1.1,
1.2, 1.3, ... in the first chapter, and 2.1, 2.2, 2.3, ... in the second
chapter and so forth.  This command should always be included to follow thesis
office guidelines.  Include it in the preamble to activate.


\section*{Other notes}

There may be other commands hiding in the style file that are not documented
here.  These instructions and the sample files were thrown together to try to
make the package usable without people tracking down my email address and asking
me about them.  The \verb+mssample+ and \verb+phdsample+ files are provided to
show how many of the features work in for MS and PhD theses.  Feel free to look
through the .sty file itself and discover commands I forgot to document in these
instructions.  If you know enough about \TeX\ and \LaTeX\ to be using them to
write a thesis, you can figure it out.

Finally, I put together much of the style file by piecing together parts of
other style files that I picked up from various people.  Some of the macros
identify who wrote them, and I preserved the original author information if
present, even if I modified the macros.  So if there is some piece of code in
the style file that you wrote but isn't credited to you, thank you for your
contribution and it wasn't me who took your name out.

I hope you find the package helpful, if so, please tell the Thesis Office they
should support \LaTeX\ and/or tell your friends about this macro package.

\end{singlespace}
\end{document}
