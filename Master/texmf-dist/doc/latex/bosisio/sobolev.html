<html>
<!--
+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
|            Copyright(C) 1997-2010 by F. Bosisio             |
|                                                             |
| This program can be redistributed and/or modified under     |
| the terms of the LaTeX Project Public License, either       |
| version 1.3 of this license or (at your option) any later   |
| version. The latest version of this license is in           |
|   http://www.latex-project.org/lppl.txt                     |
| and version 1.3 or later is part of all LaTeX distributions |
| version 2005/12/01 or later.                                |
|                                                             |
| This work has the LPPL maintenance status `maintained'.     |
| The Current Maintainer of this work is F. Bosisio.          |
|                                                             |
| This work consists of files sobolev.dtx and sobolev.html    |
| and of the derived files sobolev.sty and sobolev.pdf.       |
|                                                             |
| E-mail:   fbosisio@bigfoot.com                              |
| CTAN location: macros/latex/contrib/bosisio/                |
+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
-->
<head>
  <title>Package sobolev</title>
</head>
<body>

<h1 align="CENTER">
Package <tt>sobolev</tt><br />
<small>
This is version 2.2, last revised 1997/11/14; documentation date 2005/04/09.
</small>
</h1>

<p align="CENTER">
<strong>
Author: <i>F. Bosisio</i><br />
<small>
E-mail: <a href="mailto:fbosisio@bigfoot.com">
	<tt>fbosisio@bigfoot.com</tt>
	</a><br />
CTAN location: <a href="http://mirror.ctan.org/macros/latex/contrib/bosisio">
	       <tt>macros/latex/contrib/bosisio</tt>
	       </a>
</small>
</strong>
</p>

<p align="LEFT"></p>

<!------------------------------------------------------------------------>

<h3>Abstract:</h3>
<div>
Documentation for the package <tt>sobolev</tt>.
</div>

<hr /><br />
<!------------------------------------------------------------------------>

<h2>Table of contents</h2>

<ul>
  <li><a href="#SECTION01">Introduction</a></li>
  <li><a href="#SECTION02">The options</a></li>
  <li><a href="#SECTION03">The commands</a>
    <ul>
      <li><a href="#SECTION03a">The <tt>\H</tt> command</a></li>
      <li><a href="#SECTION03b">The <tt>\Hdiv</tt> command</a></li>
      <li><a href="#SECTION03c">The <tt>\L</tt> command</a></li>
      <li><a href="#SECTION03d">The <tt>\W</tt> command</a></li>
      <li><a href="#SECTION03e">The <tt>\D</tt> command</a></li>
      <li><a href="#SECTION03f">The <tt>\Norm</tt> command</a></li>
      <li><a href="#SECTION03g">The <tt>\SemiNorm</tt> command</a></li>
      <li><a href="#SECTION03h">The <tt>\Scalar</tt> command</a></li>
      <li><a href="#SECTION03i">The <tt>\Crochet</tt> command</a></li>
    </ul></li>
</ul>

<hr /><br />
<!------------------------------------------------------------------------>

<h2>
<a name="SECTION01">Introduction</a>
</h2>

<p>
This package provides some commands which are useful when dealing with
Sobolev spaces and their relatives.

<p>
In particular some commands are redefined, so care should be taken,
expecially when including this package in an already existent LaTeX file.

<p>
The redefined commands are <tt>\H</tt> and <tt>\L</tt>.
The effect of ``<tt>\H</tt>'' (which is a type of accent) can now be achieved
by the command ``<tt>\HAccent</tt>'', whilst the job of ``<tt>\L</tt>''
(i.e. print an ``L'' with a superimposed bar) is now done by the command
``<tt>\Lbar</tt>''.

<h2>
<a name="SECTION02">The options</a>
</h2>

<p>
Two options are available at the moment: <tt>DivInBrackets</tt> and
<tt>DivAsExponent</tt>.
They only affect the output of the ``<tt>\Hdiv</tt>'' command.

<p>
The firt options (<tt>DivInBrackets</tt>, which is the default) makes
<tt>\Hdiv</tt> behave like ``<tt>H(div;</tt>...<tt>)</tt>'', while the
second one (<tt>DivAsExponent</tt>) makes <tt>\Hdiv</tt> expand to
``<tt>H^{div}(</tt>...<tt>)</tt>''.

<h1>
<a name="SECTION03">The commands</a>
</h1>

<p>
Most of the subsequent space-generating commands have mandatory arguments to
indicate the type of the space.
Often this argument consists of a single digit: in this case it is <em>not</em>
necessary to enclose it in brackets, since in LaTeX the names of commands
consists of letters only, and so a digit following it is certainly an argument.
This saves a lot of typing and is the only reason that makes these commans
useful (if you always had to type the brackets, then it would have been simpler
to type the expansion of the command than the command itself !).
In other words, you can think as if several commands exist (like <tt>\H</tt>,
<tt>\H1</tt>, <tt>\H10</tt>, etc.), the ones with the digit beeing a sort of
abbreviation for the general one.

<h3>
<a name="SECTION03a">The <tt>\H</tt> command</a>
</h3>

<p>
The <tt>\H</tt> command is used to generate the symbol of sobolev spaces.
It takes a mandatory argument, which is used as a superscript, and an optional
argument, which is used as a subscript.

<p>
As explained above, if the mandatory argument is a digit, it need not be
enclosed in brackets.
Moreover, if the optional argument is the digit ``0'', it can be typed without
the square brackets.

<p>
Here are some examples (whith the <tt>\DefaultSet</tt> set to its default
value <tt>\Omega</tt>):
<pre>
	\H2		==>	H<sup>2</sup>(\Omega)
	\H10		==>	H<sup>1</sup><sub>0</sub>(\Omega)
	\H1[\Gamma_D]	==>	H<sup>1</sup><sub>\Gamma<sub>D</sub></sub>(\Omega)

	\H^{-1/2}	==>	H<sup>-1/2</sup>(\Omega)
</pre>

<h3>
<a name="SECTION03b">The <tt>\Hdiv</tt> command</a>
</h3>

<p>
The <tt>\Hdiv</tt> command is used to generate the sobolev space
called ``H div''.
It takes only an optional argument, which is used as a subscript and which
need not to be surrounded by the square brackets if it is the digit ``0''.

<p>
If the (default) option <tt>DivInBrackets</tt> is in effect, it differs from
the command <tt>\H</tt> in that the word ``div'' is printed (in roman type)
inside brackets, before the set.
If, instead, the option <tt>DivAsExponent</tt> is active, then it is simply
an abbreviation for <tt>\Hdiv</tt>.

<p>
Here are some examples:
<pre>
				   DivInBrackets		     DivAsExponent
     \Hdiv	       ==>   H(div;\Omega)		H<sup>div</sup>(\Omega)
     \Hdiv0	       ==>   H<sub>0</sub>(div;\Omega)	      H<sup>div</sup><sub>0</sub>(\Omega)
     \Hdiv[\Gamma_D]   ==>   H<sub>\Gamma<sub>D</sub></sub>(div;\Omega)	H<sup>div</sup><sub>\Gamma<sub>D</sub></sub>(\Omega)
</pre> 

<h3>
<a name="SECTION03c">The <tt>\L</tt> command</a>
</h3>

<p>
The <tt>\L</tt> command is used to generate the symbol of Lebesgue-measurable
functions.
It has one argument which is the exponent of the L-space.
Again, if this argument is a digit (or a single symbol,
like ``<tt>\infty</tt>'') the surrounding braces are optional.
Like for the <tt>\H</tt> command, the output of <tt>\DefaultSet</tt>
is appended.

<p>
Here are some examples:  
<pre>
	\L2		==>	L<sup>2</sup>(\Omega)
	\L{10}		==>	L<sup>10</sup>(\Omega)
	\L\infty	==>	L<sup>\infty</sup>(\Omega)
</pre>

<h3>
<a name="SECTION03d">The <tt>\W</tt> command</a>
</h3>

<p>
The <tt>\W</tt> command is completly analogous, except that it prints a ``W''
insted af an ``L'' and that it has two argument, both printed as a supercript,
separated by a comma.
It is used for the generalized Sobolev spaces.

<p>
Here is an example of how it is used:  
<pre>
	\W{k}{p}	==>	W<sup>k,p</sup>(\Omega)
	\W1\infty	==>	W<sup>1,\infty</sup>(\Omega)
</pre>

<h3>
<a name="SECTION03e">The <tt>\D</tt> command</a>
</h3>

<p>
The <tt>\D</tt> command is used in the theory of distributions:
it prints the space of distributions over the <tt>\DefaultSet</tt> if
followed by a prime symbol, or its dual space, otherwise.
<pre>
	\D	==>	<i>D</i>(\Omega)
	\D'	==>	<i>D</i>'(\Omega)
</pre>

<h3>
<a name="SECTION03f">The <tt>\Norm</tt> command</a>
</h3>

<p>
The <tt>\Norm</tt> command has a mandatory and an optional argument;
it generates the norm of the mandatory argument, with the optional argument,
if present, as a whole subscript, to denote the space within which the norm
is taken.

<p>
Some examples:  
<pre>
	\Norm{f(x)}	==>	|| f(x) ||
	\Norm{g}[L^2]	==>	|| g ||<sub>L<sup>2</sup></sub>
</pre>

<h3>
<a name="SECTION03g">The <tt>\SemiNorm</tt> command</a>
</h3>

<p>
The <tt>\SemiNorm</tt> command is completly analoguos, but generates the
semi-norm instead of the norm.

<p>
Some examples:  
<pre>
	\SemiNorm{f(x)} 	==>	|f(x)|
	\SemiNorm{g[H^1]} 	==>	|g|<sub>H<sup>1</sup></sub>
</pre>

<h3>
<a name="SECTION03h">The <tt>\Scalar</tt> command</a>
</h3>

<p>
The <tt>\Scalar</tt> command has two arguments; a third optional argument
(which is used as a whole subscript) may follow inside square brackets.
The output consists of the two arguments separated by a comma and enclosed
in a pair of adjustable-size brackets, with the optional argument placed as
a subscript (to denote the space inside which the scalr product is taken).

<p>
Some examples:
<pre>
	\Scalar{f}{g}		==>	(f,g)
	\Scalar{u}{v}[L^2]	==>	(u,v)<sub>L<sup>2</sup></sub>
</pre>

<h3>
<a name="SECTION03i">The <tt>\Crochet</tt> command</a>
</h3>

<p>
The <tt>\Crochet</tt> command has two arguments; a third optional argument
(which is used as a whole subscript) may follow inside square brackets.
The output consists of the two arguments separated by a comma and enclosed
in a pair of adjustable-size angular-parenthesys, with the optional argument
placed as a subscript (to denote the space inside which the duality is taken).

<p>
Some examples:
<pre>
	\Crochet{f}{g}		==>	&lt;f,G&gt;
	\Crochet{u}{v}[D]	==>	&lt;u,v&gt;<sub>D</sub>
</pre>

<hr /><br />
<!------------------------------------------------------------------------>

<address>
  <i>F. Bosisio</i><br />
  E-mail: <a href="mailto:fbosisio@bigfoot.com"><tt>fbosisio@bigfoot.com</tt></a>
</address>

<!------------------------------------------------------------------------>

</body>
</html>
