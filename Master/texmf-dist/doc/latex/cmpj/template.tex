%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                                                                      %%%
%%%    INSTITUTE FOR CONDENSED MATTER PHYSICS                            %%%
%%%                                                                      %%%
%%%    Template for preparing an article for publication                 %%%
%%%    in the Condensed Matter Physics journal <cmp@icmp.lviv.ua>        %%%
%%%    using LaTeX2e                                                     %%%
%%%                                                                      %%%
%%%    Last time modified: January 15, 2013                              %%%
%%%                                                                      %%%
%%%    Copyright (C) 2013 by the Institute for Condensed Matter Physics  %%%
%%%                      of the National Academy of Sciences of Ukraine  %%%
%%%                                                                      %%%
%%%    This file can be redistributed and/or modified under the terms    %%%
%%%    of the LaTeX Project Public License (lppl).                       %%%
%%%                                                                      %%%
%%%    Please report errors to: Andrij Shvaika                           %%%
%%%                             ashv (a) icmp lviv ua                    %%%
%%%                                                                      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[intlimits,twoside,a4paper]{article}

\usepackage{amsmath,amssymb}
\usepackage{graphicx}

\usepackage[T2A]{fontenc}
\usepackage[cp1251]{inputenc}
%
%% Support for Ukrainian language. Comment two lines above if you do 
%% not use Ukrainian.


\usepackage[eqsecnum]{cmpj2}
%% Fields in square brackets are optional. 
%% Use 'eqsecnum' for equation numbering by sections;
%% and 'preprint' for the preprint layout with the increased
%% font size and interline spacing.
%%
%% Other options control font schema (not required for regular usage):
%% 'droid' for droid fonts (default);
%% 'helvet' for Helvetica fonts as in old 'cmpj.sty';
%% 'cm' for standard TeX Computer Modern fonts.

\articletype{Regular article}

%% If the title of your article is longer than 45 characters please 
%% indicate its short form (optional argument) which goes into the 
%% running head.


%
\title[Short title: Template and instructions]%
{Long title: Template and instructions to the authors%
\thanks{``\ldots{}I wouldn't stand
by and see the rules broken -- because right is right, and wrong
is wrong, and a  body ain't got no business doing wrong when he
ain't ignorant and knows better.'' \protect\\ [0.9ex] \strut\qquad
Twain~M., The Adventures of Huckleberry Finn. 1884.}}
%
\author[Short authors list]{A.U. Thor\refaddr{label1,label2},
        B.U. Thor\refaddr{label2}, \ldots}
\addresses{
\addr{label1} Ornstein University, 1 Rain St., 10041 Celsius, Softland
\addr{label2} Zernike Institute, 2 River Ave., 20451 Fahrenheit, Solidshire
}
%
%% or for single author or if all authors are from the same institute:
%
%  \author[Short authors list]{1st Author, 2st Author, \ldots}
%  \address{Institute}
%
%% Fields in square brakets (short title and short authors list) are
%% optional. Use them if your entries exceeds 45 characters.
%

\sloppy

\begin{document}

\maketitle

\begin{abstract}
These instructions describe how to prepare and submit an article
using \LaTeX{} and \texttt{cmpj2.sty} for publishing in
``Condensed Matter Physics'' journal. Each submitted manuscript
should contain an abstract reflecting the essence of the study.
The abstract will be published separately from the article in a
variety of bibliographical services so it should be completely
self-contained (\emph{no footnotes or numbered references}).
Extremely important references may be incorporated into the
abstract itself: [Thor~A.U. et al., Condens. Matter Phys., 2004,
\textbf{7}, 111]. The abstract should be written as a single
paragraph not exceeding 150 words. Mathematical expressions are
discouraged. Please define all nonstandard symbols and
abbreviations.
%
\keywords Up to six keywords
%
\pacs Up to six PACS numbers
\end{abstract}



\section{Editorial process: from submission to offprints}

This guide is intended to help in preparing \LaTeX{} compuscripts
for publication in ``Condensed Matter Physics'' (hereinafter CMP)
journal. We appreciate your effort to follow our style in
preparing your article for mutual benefit: speedier publication,
greater accuracy and higher quality. Below is a description of the
features of CMP style. In addition, you will find some general 
advice and specific remarks on preparing your compuscripts.

Authors are encouraged to submit their manuscripts electronically,
through the Editorial Office at \href{mailto:cmp@icmp.lviv.ua}{\texttt{cmp@icmp.lviv.ua}}.
Third-party submissions cause excessive problems. Author
self-submissions are exceedingly preferred. Please send articles as
attachments to e-mail messages. All appropriate files (\LaTeX{}
sources and figure files) should be combined into a single archive. 
However, if the size of files exceeds 5~MB a multi-part archive 
should be created. Then every part of the archive should be sent 
separately with a clear description 
(e.g. ``Manuscript of A.U.~Thor et al., part~1 of~5'' etc.). 
All information about the submission should be included in
the main body of the message. This must include: full name, postal
and e-mail addresses, telephone and fax numbers of the
corresponding author, the full title of the article, the full list
of authors and the full list of files in the attached archive(s).

Please check your article carefully for accuracy, consistency and
clarity before submission. Ask someone else (preferably native
speaker or person with perfect knowledge of English) to read the
manuscript, however satisfied you may be with its quality. Your
colleague can often find errors or obscure passages missed by the
author.

Compuscripts should be coded in \LaTeXe{} with the \verb|article| class,
preferably using the CMP style available at the journal Web page
\\
\centerline{\href{http://www.icmp.lviv.ua/journal/src/cmpj.zip}{\texttt{http://www.icmp.lviv.ua/journal/src/cmpj.zip}}}
\\
Manuscript should be arranged in the following order: title of
article, short title of not more than 45~characters, author's name
and affiliation, abstract (up to 150~words), up to six key words
and PACS numbers, text of the article, acknowledgements,
appendices and references. At the end of the document or in
separate file the contributors are asked to provide title,
author's name and institute, abstract and key words in Ukrainian
(see subsection \ref{ua-part}). Figures and tables should be
included in the text where they have appeared or at the end of the
article.

The Editorial Office acknowledges receipt of a compuscript by
e-mail to the corresponding author. All contributions will be
evaluated according to the standard procedures of peer review,
usually by two referees. Only contributions which meet the
scientific and formal standards of the journal can be accepted for
publication. The Editorial Office will send an acceptance letter
to the corresponding author. Proofs are provided in the form of
PDF files after preparing an article for publishing by the production
department. Authors should prepare a list of necessary corrections
as soon as possible and e-mail it to the Editorial Office. All
articles are freely available in electronic forms (PS, PDF)
at the journal Web page:
\\
\centerline{\href{http://www.icmp.lviv.ua/journal/}{\texttt{http://www.icmp.lviv.ua/journal/}}}

We look forward to receiving your articles!




\section{Style guide}


\subsection{Title information}

The manuscript should begin with the title of the paper in lower
case letters except for proper nouns, certain abbreviations,
physical quantities, chemical symbols etc. The use of nonstandard
abbreviations and acronyms is not allowed. Unnecessary words in
the title should be dropped. The title is followed by the names of
all authors (with first name initials) and the corresponding
institutions (from the smallest to the largest unit, e.g. group,
department, university) with addresses, as given in the above
example (street or P.O. box, city with zip codes, country).
Countries should be written in English. There are two slightly
different forms of affiliations in the CMP style depending on
whether all authors work in the same institution. The
corresponding author electronic mail address and optionally phone
and fax numbers can be placed in a footnote. Dates of receipt of
original and revised versions will be added by the Editorial
Office.

An abstract should accompany each manuscript; it should
be completely self-contained, not exceeding 150 words and
written as a single paragraph. Mathematical expressions
should be avoided.

Up to six keywords should at a glance give a general idea of the
article to a reader. PACS numbers (up to six classification codes) 
\emph{must} be given.
Please use the full, permanently updated classification system which
is available online at
\\
\centerline{\href{http://publish.aps.org/PACS/}{\texttt{http://publish.aps.org/PACS/}}}
\\
There should be no more than six numbers, the principal number
should be placed first, and the check characters should always be
included.


\subsection{Main body of the paper}

The CMP style makes use of the conventional \LaTeX{} document
layout so below we will only point out peculiarities of the style
and some important general features (see also
section~\ref{tex-issues} for \TeX{}-related issues).

The body of the paper should be divided into sections, subsections
etc. (however, in short papers section may not be necessary)
using standard \LaTeX{} commands. Text should be typed as usual.
Compounded words like ``semi-infinite, $a$-axis'' are separated by
a short hyphen ``\verb|-|'', the en-dash ``\verb|--|'' is used in
number ranges (12--21) and em-dash ``\verb|---|''~--- in sentences. Please, make
a difference between left and right quotes (\verb|`| vs. \verb|'|
or \verb|``| vs. \verb|''|, respectively) instead of using the
symbol \verb|"| everywhere. \emph{Emphasized text} is obtained with
the command \verb|\emph{...}|. Extra or exceptional hyphenations
are added by means of the
command \verb|\hyphenation|, which should be placed in the
preamble of the document.

Use \verb|\label| and \verb|\ref| for cross-references to
equations, figures, tables, sections, subsections, etc., instead of
plain numbers. The \verb|\label| instruction should be typed
immediately after (or one line below), but not inside the argument
of a number-generating instruction such as \verb|\section| or
\verb|\caption|, roughly in the position where the number appears,
in environments such as \texttt{equation}, \texttt{figure},
\texttt{table}, etc.

A limited number of private definitions should be placed in the
preamble of the article, and not at any other place in the
document. Such private definitions, i.e. definitions made using
the commands \verb|\newcommand| and \verb|\newenvironment|, should
be used with great care. Large macro packages should be avoided.
Definitions that are not used in the article should be omitted. Do
not change existing environments, commands and other standard
parts of \LaTeX{}. A short description of the various definitions,
in the form of \TeX{} comment lines, is appreciated. Deviation
from these rules may cause inaccuracies in the article or a delay
in publication.

Footnotes should be avoided whenever possible. If required they
should be used only for brief notes that do not fit conveniently
into the text. The standard \LaTeX{} macro \verb|\footnote| should
be used and will normally give an appropriate symbol.

Your article will be read by many people whose native language is
not English so keep sentences as short and simple as possible.
UK~English spellings are preferred (colour, flavour, behaviour,
tunnelling, artefact, focused, focusing, fibre, etc.). Using of
``-ize'' spellings is encouraged (diagonalize, renormalization,
minimization, etc.). The words ``table'', ``figure'', ``equation''
and ``reference'' should be written in full and \emph{not}
contracted to ``Tab.'', ``Fig.'', ``eq.'' and ``Ref.''




\subsection{Mathematical material}

\subsubsection{General advice}

For in-line formulas use \verb|\(...\)| or \verb|$...$|:
$E_{\textrm{F}}$, \(T_{\textrm{C}}=123\)~K. Avoid built-up
constructions, for example fractions, matrices, integrals, sums,
etc., in in-line formulas. For unnumbered displayed one-line
formulas use the \verb|displaymath| environment or the shorthand
notation \verb|\[...\]|. For numbered displayed one-line formulas
use the equation environment. Do \emph{not} use \verb|$$...$$|,
but only the \LaTeX{} environments, so that the document style
determines the formula layout. For example, the definition of
Dirac $\delta$-function in the Fourier representation looks like
\begin{equation}
\label{delta-def}
\delta(x) = \frac{1}{2\pi}
           \int_{-\infty}^{\infty} \re^{\ri sx}
           \rd s,
\end{equation}
where the expression $\re^{\ri sx}$ is tolerable but the exponents
with more complex arguments should be replaced by $\exp({\ri sx})$.
For displayed multi-line formulas use the standard \verb|eqnarray| environment or \verb|align| one of the \AmS-\LaTeX{} bundle.
For example,
\begin{align}
\Delta^{(+)} &\equiv 1-\Theta (\varphi^{(+)}_1 Q_{11}+\varphi^{(+)}_4 Q_{22})
-\Theta (\varphi^{(+)}_2 +\varphi^{(+)}_3) Q_{12}
\nonumber\\
&+\Theta^2 (Q_{11}Q_{22}-Q_{12}^2)(\varphi^{(+)}_1 \varphi^{(+)}_4 -
\varphi^{(+)}_2 \varphi^{(+)}_3)=0.
\label{multi-line}
\end{align}
Please note that the equation number stands in the last line of the
multi-line mathematical expression numbered as a whole.

Keep in mind that an \emph{empty} line in \TeX{} sources starts
a \emph{new} paragraph! So do \emph{not} separate mathematical
environments (and other too!) by empty lines unless you do
\emph{want} start a paragraph at this place!





\subsubsection{Roman vs. italic}


In mathematics mode \LaTeX{} automatically sets variables in a
math italic font. Such an italicization should be accepted in
general. However, there are some cases where it is better to use a
Roman font to mark a special meaning of certain symbols: the Euler
number ``$\re$'', ``$\ri$'' when used as imaginary
unit ($\ri=\sqrt{-1}$), differential ``$\rd$'', and
the operators ``$\Im$'' and ``$\Re$'' for the
imaginary and real parts of complex numbers, respectively [see
equation~(\ref{delta-def}) for example] and \verb|cmpj.sty| provides commands \verb|\re|, \verb|\ri|, \verb|\rd|, \verb|\Im|, and \verb|\Re| for typesetting such symbols, respectively.

Mathematical functions, such as \verb|cos|, \verb|sin|, etc.,
should appear in Roman type. \LaTeX{} provides built-in commands for
most of these functions (e.g.\ \verb|\cos| and \verb|\sin|, respectively).

Subscripts and superscripts should be in Roman type if they are
labels rather than variables or characters that take values:
$k_{\textrm{B}}$ (Boltzmann constant), $E_{\textrm{F}}$ (Fermi energy),
$T_{\textrm{C}}$ (Curie temperature) and $c_{\textrm{ion}}$ (ion concentration).

Units (e.g. V/cm or V~cm$^{-1}$, K, Pa, etc.) should be written in text mode
(not in math environment) with a nonbreakable space to the number before:
$T_{\textrm{C}}=123$~K. Symbols of chemical elements are also typed in text mode:
H$_2$O, Ca$^{2+}$, $^{14}$N$_2$, etc.


\subsection{A few miscellaneous remarks}

Mathematical expressions are rather sophisticated object to type out.
Below there is a list of the most simple and most common errors which one
should avoid.
\begin{itemize}
%
%
\item
%
For simple fractions in the
text the solidus \verb|/|, as in $\hbar=h/2\pi$, should be used
instead of \verb|\frac| or \verb|\over|, care being taken to use
parentheses where necessary to avoid ambiguity, for example to
distinguish between $1/(n-1)$ and $1/n-1$. Exceptions to this are
the proper fractions $\frac12$, $\frac13$, $\frac34$, etc., which
are better left in this form. In displayed equations horizontal
lines are preferable to solidi provided the equation is kept
within a height of two lines. A two-line solidus should be avoided
where possible; the construction $(\ldots)^{-1}$ should be used
instead.
%
%
\item
%
Angle brackets (e.g. the ``bra-ket'' notation, average values and
Green functions) are obtained with \verb|\langle| and \verb|\rangle| commands:
$\langle p|q \rangle$, $\langle S^z \rangle$ and
$\langle\langle a^{+}_i | a_j \rangle\rangle_{\omega}$.
%
%
\item
%
The symbol \verb|\mid| should be used as binary operator only.
The character ``~$|$~'' is a proper element of such mathematical constructions
as modulus, the ``bra-ket'' notation and Green functions:
$X^{pq}= |p\rangle \langle q|$ and $A=|b/c|$.
%
%
\item
%
The root sign $\sqrt[n]{x}$ looks fine with simple expressions only,
the power $1/n$ should be used for complex ones.
%
%
\item
%
Braces, brackets and parentheses should be used in
the following order: $\{[(\;)]\}$. The same ordering of brackets
should be used within each size. However, this ordering can be
ignored if the brackets have a special meaning.
%
%
\item
%
Decimal point (not comma!) should be used in decimal fractions.
%
%
\item
%
Please treat mathematics as a part of text, writing down punctuation
marks where necessary.
\end{itemize}


\subsection{Floats: figures and tables}


Put the tables and figures in the text with the \verb|table| and
\verb|figure| environments, and position them near the first
reference of the table or the figure. Each table or figure should
have an explanatory caption which should be as concise as possible.
If a table or figure is divided into parts these should be labelled
(a), (b), (c), etc., but there should be only one caption for the
whole float, not separate ones for each part. The caption comes
\emph{before} the table and \emph{after} the figure. It should have
a full stop at the end. Simple samples are presented here (see
figure~\ref{fig-smp1} and table~\ref{tbl-smp1}).

\begin{figure}[htb]
\centerline{\includegraphics[width=0.65\textwidth]{eps_demo}}
\caption{This is a sample of true EPS figure which allows arbitrary
scaling without the loss of quality. Figure should be centred in the
line.} \label{fig-smp1}
\end{figure}


\begin{table}[htb]
\caption{Coefficients of symmetrized occupancies of orientational states
which correspond to irreducible representations of the point symmetry
group 2/m. Just a sample. Table also should be centred in the line.}
\label{tbl-smp1}
\vspace{2ex}
\begin{center}
\renewcommand{\arraystretch}{0}
\begin{tabular}{|c|c||c|c|c|c|c|c|c|c|}
\hline
&&(1,1)&(1,2)&(1,3)&(1,4)&(2,1)&(2,2)&(2,3)&(2,4)\strut\\
\hline
\rule{0pt}{2pt}&&&&&&&&&\\
\hline
\raisebox{-1.7ex}[0pt][0pt]{A$_g$}
      & $x_+$& 1/2&  1/2&  0&  0&  1/2&  1/2&  0&  0\strut\\
\cline{2-10}
      & $z_+$& 0&  0&  1/2&  1/2&  0&  0&  1/2&  1/2\strut\\
\hline
\raisebox{-1.7ex}[0pt][0pt]{B$_g$}
      & $x_-$& 1/2&  1/2&  0&  0& $-1/2$& $-1/2$&  0&  0\strut\\
\cline{2-10}
      & $z_-$& 0&  0&  1/2&  1/2&  0&  0& $-1/2$& $-1/2$\strut\\
\hline
\raisebox{-1.7ex}[0pt][0pt]{B$_u$}
      & $y_+$& 1/2& $-1/2$&  0&  0&  1/2& $-1/2$&  0&  0\strut\\
\cline{2-10}
      & $u_-$& 0&  0&  1/2& $-1/2$&  0&  0& $-1/2$&  1/2\strut\\
\hline
\raisebox{-1.7ex}[0pt][0pt]{A$_u$}
      & $y_-$& 1/2& $-1/2$&  0&  0& $-1/2$&  1/2&  0&  0\strut\\
\cline{2-10}
      & $u_+$& 0&  0&  1/2& $-1/2$&  0&  0&  1/2& $-1/2$\strut\\
\hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
\end{table}


Where possible tables should not be broken over pages. If a table
has related notes these should appear directly below the table
rather than at the bottom of the page. Notes can be designated
with footnote symbols. The \verb|minipage| environment is useful
in this case.

Figures may be included in an article as Encapsulated PostScript
(EPS) files by means of the \verb|\includegraphics| command or using the
\LaTeX{} picture environment. Each figure should have a brief
caption describing it and, if necessary, interpreting the various
lines and symbols on the figure. As much lettering as possible
should be removed from the figure itself and included in the
caption.

Keep in mind that your tables and figures may move from their
original places during preparation of the article for publishing. Collecting
all tables and figures at the end of the article is acceptable but
undesirable.


\subsection{How to prepare a good figure}

\emph{Authors should remember that the final printed quality of
illustrations can never be better than the quality of the original
artwork.}

Figures must be carefully prepared and submitted ready for
reproduction.
Figures are often reduced to half of their original size so
lettering and symbols should be clear and large
enough (smallest letters should not be smaller
than 2~mm, line strength should be greater than 1~pt).
All figure elements should be of proportional sizes and have uniform strength and contrast.
Avoid small open symbols, small dots, small decimal
points, hairlines, close-dotted or short-dashed lines. Draw a
closed axes frame in diagrams with the axis titles (quantity
followed by the unit in brackets) parallel to the corresponding
axis, outside the frame. For numbers, use a decimal point instead
of a comma.
Authors should avoid including any unnecessary text
around a figure, such as captions, figure numbers, author or
file names. Grey scales in plots and diagrams, which might get
difficult to distinguish after reducing, or which often disappear
during the printing process, should be avoided.
Labelling should be uniform in size throughout all figures.
Lettering should not be pasted on to the figures as it may easily
become detached as a result of handling.

Colour figures submitted in electronic format will generally
remain in colour in the electronic versions of an article at no
cost. However, in the paper version of the CMP journal figures are printed in black
and white or grayscale.




\begin{figure}[htb]
%\vspace{-2ex}%
\centerline{\includegraphics{icmphome}}
\caption{This is a sample of a bitmap image. This
cosy building homes the Institute for Condensed Matter Physics and
the CMP Editorial Office too. Photo by Oleh Vorobyov.}
\label{fig-smp2}
\end{figure}

Roughly speaking, there are two types of electronic figures:
vector graphics and bitmap graphics. A \emph{vector} image is a
set of arranged objects: lines, polygons, ellipses, shades,
characters, etc. Such an image allows almost arbitrary scaling
without loss of quality. Vector images are typically charts,
plots, diagrams, etc., produced by various computer software. A
\emph{bitmap} image is a two-dimensional array of pixels
(PICture'S ELements) or ``dots''. Continuous tone photographs are
their the most widespread samples (like figure~\ref{fig-smp2}).
Image quality is determined by DPI (Dot Per Inch --- a pretty
self-explained definition). Below, there are necessary
requirements for each type of images.
\begin{description}
%
%
\item[Vector images]
%
are to be prepared in Encapsulated PostScript (EPS) format. The
Bounding Box should indicate the area of the figure with a minimum
of white space around it and not the dimensions of the page.
Please make sure to use only standard fonts
(Helvetica or Arial, Times or Times New Roman, Symbol)
or include the fonts in the figure file.
Authors should avoid using special effects generated by including verbatim
PostScript code within the \LaTeX{} file with specials other than the
standard figure inclusion ones.
Files should be prepared
in ASCII format (not binary) with no TIFF preview. If Windows
Postscript drivers are used, check that the Postscript option is
set to Encapsulated Postscript (EPS).
%
%
\item[Bitmap images]
%
should be sent in TIFF (or TIF) format. Continuous-tone figures
should have at least 300~DPI, line drawings -- 600~DPI minimum
resolution in final size. Screen or web resolution is
\emph{absolutely insufficient} for publication. If authors convert
the bitmap image to EPS format and include it in such a form in
the compuscript, they should also send an original TIFF version of
the image to the Editorial Office.

Use of lossy JPEG (JPG) format (or JPEG compression) is \emph{strictly prohibited}
for images with sharp margins (especially line-art) because of large visible artefacts.
Continuous-tone images (especially large figures with good resolution) \emph{can} be
sent in JPEG format with compression set to ``High quality''.
\end{description}

Every time if you are in doubt, please feel free consult the
Editorial Office and you will receive an advice of our \TeX{}nical
Editor.




\subsection{Acknowledgements and appendices}

The acknowledgement section follows the main body of the paper and
precedes any appendixes. This unnumbered section starts with
command
\\
\strut\hfill%
\verb|\section*{Acknowledgments}|%
\hfill\strut%
\\
Technical detail that it is necessary to include, but that
interrupts the flow of the article, may be consigned to an
appendix. Any appendixes should be included at the end of the main
text of the paper, after the acknowledgements section (if any) but
before the reference list. The command \verb|\appendix| is used to
signify the start of the appendixes.


\subsection{References}

References should be numbered sequentially through the text and prepared
by means of \verb|\cite| and \verb|\bibitem| commands, linking citations in the text
with their entry in the reference list, which is composed within
the standard \verb|thebibliography| environment.

When several references occur together in the text \verb|\cite|
may be used with multiple labels with commas but no spaces
separating them. Thus
\\
\strut\hfill%
\verb|\cite{Zub74,Bus89,Mel00,Mry94,Sta11,Sta04}|%
\hfill\strut%
\\
would give
\cite{Zub74,Bus89,Mel00,Mry94,Sta11,Sta04}.
Note that labels are sorted in an ascending order and the groups of consecutive numbers are condensed.

The citation scheme for journals is: Surnames\verb|~|Initials.,
Journal Title (according to the standard abbreviations), Year of publication,
\textbf{Vol.}, starting page (comments: e.g. in Ukrainian, unpublished, private communication, in press) \cite{Bus89,Mel00,Mry94,Sta11}.
For journals without volume numbers place the issue number instead as in reference~\cite{Mry94}.
The names of all authors of cited papers should be given.
Title of journal should be abbreviated according to the ISI standards (see \href{http://library.caltech.edu/reference/abbreviations/}{http://library.caltech.edu/reference/abbreviations/}). 
Please supply \href{http://www.doi.org/}{DOI} for e-version of the papers \cite{Bus89,Mel00,Sta11,Scient2012} if available.

For books, the following order is required (skip irrelevant information):
Book Authors, Book Title. Book Series Vol. No., Editor(s) (Ed[s].),
Publisher, Place, Year \cite{Zub74,Abr64,Olver64}. When citing conference proceedings, please
add all available data such as title, date, and place of the
conference as well as publisher, place, and year of publication \cite{Yuk87}.

Please add language of publication for materials which are not written in English \cite{Mel00,Sta04,Yuk87}. Indicate materials accepted for publications by adding ``(in press)'' \cite{Scient2012}. If a preprint exists of the paper not yet accepted for publication, give the e-print \href{http://arxiv.org}{arXiv} number \cite{Shv04}. 
Any web resources should be cited as references \cite{url1,url2,url3}. Please avoid references to unpublished materials, private communication and web pages \cite{han12,url1,url2,url3}.

It is important to confirm the accuracy of bibliographic information in references. This has become more important now that the journal is online. To ensure that the \href{http://www.crossref.org/}{CrossRef} facility can hyper-link to the articles quoted in the bibliography, \textbf{multiple references must not be used}. I.e., a list of references under the same number is not allowed: each reference has to be singly numbered.
Before submitting your article, please ensure you have checked your paper for any relevant references you may have missed.

A typical numerical reference list might be
\cite{Zub74,Bus89,Mel00,Mry94,Sta11,Abr64,Olver64,Sta04,Shv04,Yuk87,han12,Scient2012,url1,url2}.


{\small \topsep 0.6ex
\begin{verbatim}
\begin{thebibliography}{99}
\bibitem{Zub74} Zubarev~D.N., Nonequilibrium Statistical Thermodynamics, Consultants 
    Bureau, New-York, 1974.
\bibitem{Bus89} Bussmann-Holder~A., Simon~A., B\"uttner~H., Phys. Rev. B, 1989, 
    \textbf{39}, 207; \bibdoi{10.1103/PhysRevB.39.207}.
\bibitem{Mel00} Melnyk~R., Patsahan~O., Theor. Math. Phys., 2000, \textbf{124}, 1145; 
    \bibdoi{10.1007/BF02551084} [Teor. Mat. Fiz., 2000, \textbf{124}, 339 (in Russian)].
\bibitem{Mry94} Mryglod~I.M., Tokarchuk~M.V., Condens. Matter Phys., 1993, \textbf{3}, 116.
\bibitem{Sta11} Stasyuk~I.V., Velychko~O.V., Condens. Matter Phys., 2011, \textbf{14}, 
    13004; \bibdoi{10.5488/CMP.14.13004}.
\bibitem{Abr64} Handbook of Mathematical Functions with Formulas, Graphs, and Mathematical 
    Tables. National Bureau of Standards Applied Mathematics Series Vol.~55, Abramovitz~M., 
    Stegun~I.A. (Eds.), U.S. Government Printing Office, Washington, D.C., 1964.
\bibitem{Olver64} Olver~F.W.J., In: Handbook of Mathematical Functions with Formulas, 
    Graphs, and Mathematical Tables. National Bureau of Standards Applied Mathematics 
    Series Vol.~55, Abramovitz~M., Stegun~I.A. (Eds.), U.S. Government Printing Office, 
    Washington, D.C., 1964, 355--434.
\bibitem{Scient2012} Mryglod~O., Kenna~R., Holovatch~Yu., Berche~B.,
    Scientometrics (in press); \bibdoi{10.1007/s11192-012-0874-7}.
\bibitem{Sta04} Stasyuk~I.V., Mysakovych~T.S., Preprint of the Institute for Condensed 
    Matter Physics, ICMP--04--12U, Lviv, 2004 (in Ukrainian).
\bibitem{Shv04} Shvaika~A.M., Vorobyov~O., Freericks~J.K., Devereaux~T.P., 
    Preprint \arxiv{cond-mat/0408400}, 2004.
\bibitem{Yuk87} Yukhnovkii~I.R., Idzyk~I.M., Kolomiets~V.O., 
    In: Proceedings of the Conference ``Modern Problems of Statistical Physics'' 
    (Lviv, 1987), Vol.~2, Naukova Dumka, Kiev, 1987, 97--102 (in Russian).
\bibitem{han12} Han~Y., Kida~T., Ikeda~M., Hagiwara~M., Stre\v{c}ka~J., Honda~Z. 
    (unpublished).
\bibitem{url1} \url{http://en.wikipedia.org/wiki/Condensed_matter_physics}.
\bibitem{url2} \url{http://earthdata.nasa.gov/labs/worldview/?map=-24.484375,14.05078125,50.75,62.14453125&products=baselayers.VIIRS_CityLights_2012~overlays.sedac_bound&switch=geographic}.
\bibitem{url3} \href{http://earthdata.nasa.gov/labs/worldview/?map=-24.484375,14.05078125,50.75,62.14453125&products=baselayers.VIIRS_CityLights_2012~overlays.sedac_bound&switch=geographic}
    {EOSDIS Worldview. City Lights}.
\end{thebibliography}
\end{verbatim}
}



\subsection{Ukrainian part}
\label{ua-part}

If you are not familiar with Ukrainian language, just uncomment
two lines before \verb|\ukrainianpart| in the template. The
necessary translation will be made by the Editorial Office.



\section{\protect\LaTeX{} and related issues}
\label{tex-issues}

The great advantage of \LaTeX{} over other text processing systems
is its ability to handle mathematics to almost any degree of
complexity. For this reason it prevails in major publishing houses working in the
fields of physics and mathematics. However, even so brilliant tool becomes
useless without necessary level of knowledge. A lot of useful (and free) information
one can find at ``\TeX{} Users Group'' web site

\centerline{\href{http://www.tug.org/}{\texttt{http://www.tug.org/}}}

``The Not So Short Introduction to \LaTeXe{}'' by Tobias Oetiker is an
absolutely necessary book for beginners:
\\
\centerline{\href{http://ctan.org/tex-archive/info/lshort/}{\texttt{http://ctan.org/tex-archive/info/lshort/}}}
\\
(available in a variety of languages including Ukrainian). Ukrainian \TeX{}nicians
will find a plenty of language-specific information at Andrij Shvaika's
``\TeX{} \& Ukrainian'' Web-page
\\
\strut\hfill%
\href{http://ph.icmp.lviv.ua/~ashv/tex/TeXandUkrainian.win.html}{\texttt{http://ph.icmp.lviv.ua/\~{}ashv/tex/TeXandUkrainian.win.html}}%
\hfill\strut%


Remember, you write \LaTeX{} sources not for computers only,
but for human beings also! Please make clear and fine codes!
Your document should compile without errors. Do the best you can!
However, some underfulls (and few small overfulls) warnings are acceptable,
since the compuscript will be slightly (?) modified anyway.

Frequently good physicists (especially young scientists) have a
little knowledge how to write a good looking article. As
comprehensive (and free) tutorials on the subject one can mention
``AIP Style Manual''
\\
\centerline{\href{http://www.aip.org/pubservs/style.html}{\texttt{http://www.aip.org/pubservs/style.html}}}
\\
and ``Reviews of Modern Physics Style Guide''
\\
\strut\hfill%
\href{http://rmp.aps.org/files/rmpguide.pdf}{\texttt{http://rmp.aps.org/files/rmpguide.pdf}}%
\hfill\strut%
\\
Remember: ``\textit{Ignorantia non est argumentum}'' and
``\textit{Scientia potentia est}''!


%% Type in your references using {thebibliography} environment 
%% or create them from your bibtex database using cmpj.bst style (experimental).

%\bibliographystyle{cmpj}
%\bibliography{mybibdb}


\begin{thebibliography}{99}
\bibitem{Zub74} Zubarev~D.N., Nonequilibrium Statistical Thermodynamics, Consultants 
    Bureau, New-York, 1974.
\bibitem{Bus89} Bussmann-Holder~A., Simon~A., B\"uttner~H., Phys. Rev. B, 1989, 
    \textbf{39}, 207; \bibdoi{10.1103/PhysRevB.39.207}.
\bibitem{Mel00} Melnyk~R., Patsahan~O., Theor. Math. Phys., 2000, \textbf{124}, 1145; 
    \bibdoi{10.1007/BF02551084} [Teor. Mat. Fiz., 2000, \textbf{124}, 339 (in Russian)].
\bibitem{Mry94} Mryglod~I.M., Tokarchuk~M.V., Condens. Matter Phys., 1993, \textbf{3}, 116.
\bibitem{Sta11} Stasyuk~I.V., Velychko~O.V., Condens. Matter Phys., 2011, \textbf{14}, 
    13004; \bibdoi{10.5488/CMP.14.13004}.
\bibitem{Abr64} Handbook of Mathematical Functions with Formulas, Graphs, and Mathematical 
    Tables. National Bureau of Standards Applied Mathematics Series Vol.~55, Abramovitz~M., 
    Stegun~I.A. (Eds.), U.S. Government Printing Office, Washington, D.C., 1964.
\bibitem{Olver64} Olver~F.W.J., In: Handbook of Mathematical Functions with Formulas, 
    Graphs, and Mathematical Tables. National Bureau of Standards Applied Mathematics 
    Series Vol.~55, Abramovitz~M., Stegun~I.A. (Eds.), U.S. Government Printing Office, 
    Washington, D.C., 1964, 355--434.
\bibitem{Scient2012} Mryglod~O., Kenna~R., Holovatch~Yu., Berche~B.,
    Scientometrics (in press); \bibdoi{10.1007/s11192-012-0874-7}.
\bibitem{Sta04} Stasyuk~I.V., Mysakovych~T.S., Preprint of the Institute for Condensed 
    Matter Physics, ICMP--04--12U, Lviv, 2004 (in Ukrainian).
\bibitem{Shv04} Shvaika~A.M., Vorobyov~O., Freericks~J.K., Devereaux~T.P., 
    Preprint \arxiv{cond-mat/0408400}, 2004.
\bibitem{Yuk87} Yukhnovkii~I.R., Idzyk~I.M., Kolomiets~V.O., 
    In: Proceedings of the Conference ``Modern Problems of Statistical Physics'' 
    (Lviv, 1987), Vol.~2, Naukova Dumka, Kiev, 1987, 97--102 (in Russian).
\bibitem{han12} Han~Y., Kida~T., Ikeda~M., Hagiwara~M., Stre\v{c}ka~J., Honda~Z. 
    (unpublished).
\bibitem{url1} \url{http://en.wikipedia.org/wiki/Condensed_matter_physics}.
\bibitem{url2} \url{http://earthdata.nasa.gov/labs/worldview/?map=-24.484375,14.05078125,50.75,62.14453125&products=baselayers.VIIRS_CityLights_2012~overlays.sedac_bound&switch=geographic}.
\bibitem{url3} \href{http://earthdata.nasa.gov/labs/worldview/?map=-24.484375,14.05078125,50.75,62.14453125&products=baselayers.VIIRS_CityLights_2012~overlays.sedac_bound&switch=geographic}
{EOSDIS Worldview. City Lights}.
\end{thebibliography}

%
%% If you have problems with typesetting in ukrainian uncomment lines below.
%
%  \lastpage
%  \end{document}

\ukrainianpart

\title{����� �����: ������ ����� �� ������ �������}
\author{�.�. ���\refaddr{label1,label2}, �.�. ���\refaddr{label2}}
\addresses{
\addr{label1} ����������� ��. ���������, ��������, 10041 ������, ���. ���, 1
\addr{label2} �������� ��. �������, �������, 20451 ���������, ��. г���, 2
}
%
%% ���� ����� � ���� ��� ������ � � ������ ��������:
%
%  \author{1� �����, 2� �����, \ldots}
%  \address{��������\ldots}
%
%%

\makeukrtitle

\begin{abstract}
\tolerance=3000%
� ��� ������� ������� �� ���������� �� ��������
������ ��� ��������� � ������ ``Condensed Matter Physics'' ��
��������� \LaTeX{} � ������������� ����� \texttt{cmpj2.sty}.
����� ��������� ������� ������� ������ ��������, �� ��������
���� ����������. �������� ���������������� ������ �� ����� �
������������ ������������� ��������, ���� ���� ������ ����
�������� �������������� (\emph{��� ������� �� �����������
�������}). ������ ������ ������� ����� �������� � ��������
[Thor~A.U. et al., Condens. Matter Phys., 2004, \textbf{7}, 111].
�������� ������� ���������� ���� � ������ ������ ������� ��
150~���. ����������� ������������ ������ �� ������. ��� ������
��������� ��� ��� ������������� ������� �� ���������.
%
\keywords �� ����� �������� ���

\end{abstract}

\end{document}
