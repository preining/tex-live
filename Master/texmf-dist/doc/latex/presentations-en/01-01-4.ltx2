%% 
%%  An UIT Edition example
%%  
%%  1st edition
%% 
%%  Example 01-01-4 on page 7.
%% 
%%  Copyright (C) 2012 Herbert Voss
%% 
%%  It may be distributed and/or modified under the conditions
%%  of the LaTeX Project Public License, either version 1.3
%%  of this license or (at your option) any later version.
%% 
%%  See http://www.latex-project.org/lppl.txt for details.
%% 
%% 
%% ==== 
% Show page(s) 1,5,12
%% 
%% 
\documentclass[landscape]{exafoils}
\pagestyle{empty}
\usepackage[utf8]{inputenc}
\usepackage[scaled]{helvet}
%\StartShownPreambleCommands
\documentclass[landscape]{foils}
\usepackage{fixseminar,amsmath,url} \usepackage[display]{texpower}
%\StopShownPreambleCommands
\begin{document}
\title{The \texttt{texpower} / {\normalfont \texttt{foils} demonstration}}
\author{Herbert Voß\\\url{mailto:herbert@dante.de}}
\maketitle
\foilhead{A list environment} \pause
\stepwise{
  \begin{description}
  \item[foo.] \step{bar.}
  \step{\item[baz.]} \step{foobar.}
  \end{description}}

\foilhead{An aligned equation}\pause
\parstepwise{
\begin{align}
\sum_{i=1}^{n} i & \step{=} \restep{1 + 2 + \cdots + (n-1) + n}\\
     & \step{=} \restep{1 + n + 2 + (n-1) + \cdots}\\
     & \step{=} \restep {
      \switch {\vphantom{\underbrace{(1+n) + \cdots + (1+n)}_{\times\frac{n}{2}}}%
        (1 + n) + \cdots + (1 + n)%
      }{\underbrace{(1 + n) + \cdots + (1 + n)}_{\times\frac{n}{2}}}%
     }\\
      & \step{=} \restep{\frac{(1 + n)\step{{}\cdot n}}{\restep{2}}}
\end{align}
}
\end{document}
