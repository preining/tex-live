\documentclass{nlctdoc}

\usepackage{bibleref}
\usepackage[colorlinks,bookmarks,pdfauthor={Nicola L.C. Talbot},
            hyperindex=false,
            pdftitle={bibleref.sty: a LaTeX package for 
            typesetting bible references}]{hyperref}

\usepackage{creatdtx}

\CheckSum{2503}
\OnlyDescription

\begin{document}
\title{bibleref.sty v1.14:
a \LaTeXe\ package for typesetting bible references}
\author{Nicola Talbot\\[10pt]
School of Computing Sciences\\
University of East Anglia\\
Norwich. Norfolk. NR4 7TJ.\\
United Kingdom\\
\url{http://theoval.cmp.uea.ac.uk/~nlct/}}
\date{2011-01-19}

\maketitle
\tableofcontents

\section{Introduction}

The \sty{bibleref} package was designed to provide consistent formatting for referencing
parts of the bible.

\begin{definition}[\DescribeMacro{\bibleverse}]
\cs{bibleverse}\marg{book title}\texttt(\meta{chapter}\texttt{:}\meta{verse(s)}\texttt)
\end{definition}
This command can be used to cite a bible book, chapter or verse 
or range of chapters or verses.

\begin{important}
Be careful if you use \sty{bibleref} with \sty{babel} as some of 
\styfmt{babel}'s language options make the colon character active,
which will cause a problem for \cs{bibleverse} (and \ics{ibibleverse},
described later). Check the \styfmt{babel} documentation to find out
how to make the colon non-active.
\end{important}

The book title, \meta{book title}, may be given either as the
full title (e.g.\ \texttt{Matthew}) or as an abbreviation (e.g.\
\texttt{Mt} or \texttt{Matt}), most standard abbreviations are
recognised.  Books with multiple parts should be preceded by the
book number in uppercase Roman numerals. For example, the second
book of Kings should be entered as \verb|\bibleverse{IIKings}|

You may have any number, or zero, sets of parenthesis
\verb|(|\meta{chapter}\texttt{:}\meta{verse(s)}\verb|)|,
indicating the chapter and verse or verses. Verses can be
specified as a comma separated list of individual verses or range
of verses. A range of verses should be written with a single
hyphen, e.g.\ \verb|2-4|. In the typeset output the verses will
be separated with
\begin{definition}[\DescribeMacro{\BRvsep}]
\cs{BRvsep}
\end{definition}
(an en-dash by default.) A chapter may be referenced without a
verse, but the colon must remain, e.g.\ \verb|(12:)| simply
indicates chapter 12.

A range of verses spanning more than one chapter can
be entered as \texttt(\meta{ch}\texttt:\meta{v}\texttt{)-(}\meta
{ch}\texttt:\meta{v}\texttt)

If there is no verse, but there are multiple chapters or chapter
ranges, these may be specified in the \meta{chapter} part, using a
comma to separate individual chapters and a hyphen to separate
ranges. Note that you still need a colon at the end of the list.

Examples:
\begin{center}
\begin{tabular}{ll}
\verb|\bibleverse{Ex}| & \bibleverse{Ex}\\
\verb|\bibleverse{Exodus}(20:)| & \bibleverse{Exodus}(20:)\\
\verb|\bibleverse{Exod}(20:17)| & \bibleverse{Exod}(20:17)\\
\verb|\bibleverse{IICo}(12:21)| & \bibleverse{IICo}(12:21)\\
\verb|\bibleverse{IICor}(12:21-32)| & \bibleverse{IICor}(12:21-32)\\
\verb|\bibleverse{Ex}(20:17)(21:3)| & \bibleverse{Ex}(20:17)(21:3)\\
\verb|\bibleverse{Ex}(15:)(17:)(20:)| & \bibleverse{Ex}(15:)(17:)(20:)\\
\verb|\bibleverse{Rev}(1:2,5,7-9,11)| & \bibleverse{Rev}(1:2,5,7-9,11)\\
\verb|\bibleverse{IChronicles}(1:3)-(2:7)| &
\bibleverse{IChronicles}(1:3)-(2:7)\\
\verb|\bibleverse{Luke}(1,4-5,8:)| &\bibleverse{Luke}(1,4-5,8:)
\end{tabular}
\end{center}

The style of the reference can be specified either by 
package option or as the argument to the command
\begin{definition}[\DescribeMacro{\biblerefstyle}]
\cs{biblerefstyle}\marg{style}
\end{definition}
Styles are listed in Table~\ref{tab:styles}.

\begin{table}[tbh]
\caption{Bible Citation Styles (can be used as package option or in the argument to 
\cs{biblerefstyle})}
\label{tab:styles}
\vspace{10pt}
\begin{center}
\begin{tabular}{lp{0.5\textwidth}}
\bfseries Style & \bfseries Example\\
default & \biblerefstyle{default}\bibleverse{IICor}(12:1-5)\\
jerusalem & \biblerefstyle{jerusalem}\bibleverse{IICor}(12:1-5)\\
anglosaxon & \biblerefstyle{anglosaxon}\bibleverse{IICor}(12:1-5)\\
JEH & \biblerefstyle{JEH}\bibleverse{IICor}(12:1-5)\\
NTG & \biblerefstyle{NTG}\bibleverse{IICor}(12:1-5)\\
MLA & \biblerefstyle{MLA}\bibleverse{IICor}(12:1-5)\\
chicago & \biblerefstyle{chicago}\bibleverse{IICor}(12:1-5)\\
text & \biblerefstyle{text}\bibleverse{IICor}(12:1-5)
\end{tabular}
\end{center}
\end{table}

You can change the name of a book using 
\begin{definition}[\DescribeMacro{\setbooktitle}]
\cs{setbooktitle}\marg{name}\marg{new title}
\end{definition}
Note that \meta{name} must be the full name, not the
abbreviated name of the book. For example, to change
Revelation to Apoc, do 
\verb|\setbookname{Revelation}{Apoc}|
(\textbf{Note} that you shouldn't do 
\verb|\setbookname{Rev}{Apoc}|)

If you want a different title for a book depending on whether it's
in the main body of the document or in the index, you can set the
index version using:
\begin{definition}[\DescribeMacro{\setindexbooktitle}]
\cs{setindexbooktitle}\marg{name}\marg{title}
\end{definition}
In this case, \meta{name} should be the name you'll use in 
\cs{ibibleverse}. For example, if you do:
\begin{verbatim}
\setbooktitle{Psalms}{Psalm}
\setindexbooktitle{Psalms}{Psalms}
\end{verbatim}
Then \verb|\ibibleverse{Psalms}(2:)| will print Psalm the
document and Psalms in the index, but \verb|\ibibleverse{Ps}(2:)|
will print Psalms in both the document and the index.

You can add a book using
\begin{definition}[\DescribeMacro{\addbiblebook}]
\cs{addbiblebook}\marg{name}\marg{title}
\end{definition}
For example:
\begin{verbatim}
\addbiblebook{Susanna}{Story of Susanna}
\end{verbatim}

\section{Defining New Styles}

You can define a new style using the command
\begin{definition}[\DescribeMacro{\newbiblerefstyle}]
\cs{newbiblerefstyle}\marg{style-name}\marg{commands}
\end{definition}\noindent
where \meta{commands} are
the commands needed to modify the citation style.

Example: 
This new style is based on the \qt{default} style, but
has verses in lower case Roman numerals, and redefines
\qt{Revelation} as \qt{Apocalypse}.
\begin{verbatim}
\newbiblerefstyle{sample}{%
\biblerefstyle{default}%
\renewcommand{\BRversestyle}[1]{\romannumeral##1}%
\setbooktitle{Revelation}{Apocalypse}%
}
\end{verbatim}
Note the use of \verb|##1| instead of \verb|#1|.

If you want to distribute a new style, see
section~\ref{sec:distnewstyles}.

\section{Indexing Bible References}

\begin{definition}[\DescribeMacro{\ibibleverse}]
\cs{ibibleverse}\oarg{format}\marg{book title}\texttt(\marg{chapter}\texttt{:}\meta{verse(s)}\texttt)
\end{definition}
This does the same as \cs{bibleverse}
but also adds an index entry (provided you have used 
\cs{makeindex} in the preamble.) The default page number 
format is given by the command 
\begin{definition}[\DescribeMacro{\bvidxpgformat}]
\cs{bvidxpgformat}
\end{definition}
This is \texttt{textrm} by default, but can be redefined. To 
override the page number format for a particular entry you can
use the optional argument to \cs{ibibleverse}. For example:
\begin{verbatim}
\ibibleverse[textit]{Exodus}
\end{verbatim}
(Note there is no backslash.)

\begin{definition}[\DescribeMacro{\ibiblechvs}]
\cs{ibiblechvs}\oarg{format}\marg{book title}\texttt(\marg{chapter}\texttt{:}\meta{verse(s)}\texttt)
\end{definition}
This is similar to \cs{ibibleverse} except that it doesn't display
the book name in the text. The reference is indexed under the book
title in the same way as \cs{ibibleverse}.

\begin{definition}[\DescribeMacro{\ibiblevs}]
\cs{ibiblevs}\oarg{format}\marg{book title}\texttt(\marg{chapter}\texttt{:}\meta{verse(s)}\texttt)
\end{definition}
This is similar to \cs{ibiblechvs} except that it only displays the 
verse(s) in the text.

You may need to create your own custom \app{makeindex} style file
as the default uses a comma and space to separate the item from
the page number, which may cause confusion. For example, you
could create a file called \texttt{sample.ist} and write in the
lines:
\begin{verbatim}
delim_0 "\\dotfill "
delim_1 "\\dotfill "
delim_2 "\\dotfill "
\end{verbatim}
See the \app{makeindex} documentation for further details.

\subsection{Index Categories}

You may prefer to index the bible reference under a category with
the book title as a sub item of that category. This can be achieved
using:
\begin{definition}[\DescribeMacro{\biblerefcategory}]
\cs{biblerefcategory}\oarg{sort key}\marg{category title}
\end{definition}
The optional argument is the sort key to be used by
\app{makeindex} or \app{xindy}. If omitted \meta{category title} is
used.

Note that \ics{ibibleverse}, \ics{ibiblechvs} and \ics{ibiblevs} 
use the category that is currently set. If you need to keep
switching categories, you may find it easier to define a shortcut
command. For example, suppose you're discussing the differences
between the King James and the New Jerusalem versions, you could
define the following:
\begin{verbatim}
\newcommand{\kjref}{\biblerefcategory{King James}\ibibleverse}
\newcommand{\njref}{\biblerefcategory{New Jerusalem}\ibibleverse}
\end{verbatim}
You can now use \cs{kjref} and \cs{njref} in the same way that you
would use \cs{ibibleverse}

You can disable the category by setting it to nothing:
\begin{verbatim}
\biblerefcategory{}
\end{verbatim}

\subsection{The \sty{bibleref-xidx} Package}

The \sty{bibleref} package comes with a supplementary package
\sty{bibleref-xidx} that modifies the way bible references are added to 
the index. With no package options, i.e., just
\begin{verbatim}
\usepackage{bibleref-xidx}
\end{verbatim}
the verses will appear as sub-items of the chapters. Note that a
verse range will be considered a single item so, for example,
\begin{verbatim}
\ibibleverse{Ex}(9:1)
\ibibleverse{Ex}(9:4)
\ibibleverse{Ex}(9:1-4)
\end{verbatim}
will occur as three separate entries under Exodus chapter~9.
Also note that \verb|(9-14:)| and \verb|(9:)-(14:)| will be treated
differently.

The \sty{bibleref-xidx} package can be loaded with the following options:
\begin{description}
\item[\pkgopt{noverses}] Don't show the verses in the index.
\item[\pkgopt{verses}] Show the verses (and chapters) in the index
(default).
\item[\pkgopt{nochapters}] Don't show the chapters (and verses) in the 
index.
\item[\pkgopt{chapters}] Show the chapters in the index (default).
\end{description}
All other package options will be passed to the \sty{bibleref}
package, which \sty{bibleref-xidx} automatically loads.

\subsection{Separate Scripture Index}

If you want a separate index for bible verses, in addition to
a general index, you can redefine
\begin{definition}[\DescribeMacro{\biblerefindex}]
\cs{biblerefindex}
\end{definition}
This command defaults to \cs{index}, but can be changed to
the appropriate indexing command. For example, suppose you
are using the \sty{multind} package and you want a general
index and a scripture index, you can do something like:
\begin{verbatim}
\documentclass{article}
\usepackage{bibleref}
\usepackage{multind}

\makeindex{scripture}
\makeindex{general}

\renewcommand{\biblerefindex}{\index{scripture}}
\end{verbatim}
In the document, you can use \cs{ibibleverse} as before, and
the scripture index is displayed using
\begin{verbatim}
\printindex{scripture}{Scripture Index}
\end{verbatim}
You will then need to run \app{makeindex} on the file
\texttt{scripture.idx}. See the \sty{multind} documentation
for further details.

\subsection{Changing the Sort Order}

The bible reference entries will be sorted alphabetically by
\app{makeindex}. However you may prefer the entries to be sorted
according to their order in the bible. This can either be done
using \app{xindy} instead of \app{makeindex} and creating your
own custom alphabet (see \app{xindy} manual for details) or you
can use \sty{bibleref}'s mapping command.

\begin{definition}[\DescribeMacro{\biblerefmap}]
\cs{biblerefmap}\marg{label}\marg{new sort key}
\end{definition}
For example, in the preamble:
\begin{verbatim}
\biblerefmap{Genesis}{01}
\biblerefmap{Exodus}{02}
\biblerefmap{Leviticus}{03}
\biblerefmap{Numbers}{04}
\biblerefmap{Deuteronomy}{05}
...
\end{verbatim}
When you run \app{makeindex}, the references will now be sorted
in numerical order.

If you want to subdivide the index into, say, old and new 
testament, you can add this to the mapping. For example:
\begin{verbatim}
\biblerefmap{Genesis}{1@Old Testament!01}
\biblerefmap{Exodus}{1@Old Testament!02}
\biblerefmap{Leviticus}{1@Old Testament!03}
\biblerefmap{Numbers}{1@Old Testament!04}
\biblerefmap{Deuteronomy}{1@Old Testament!05}
...
\biblerefmap{Matthew}{2@New Testament!01}
\biblerefmap{Mark}{2@New Testament!02}
...
\end{verbatim}

\section{Distributing New Styles}
\label{sec:distnewstyles}

If you want a new bible reference style (or translation) publicly
available, then it's a good idea to upload it to CTAN
(\url{www.ctan.org}). If you've never done this before, you might
find it a bit daunting, so here are a few pointers.

I suggest that you give the name of your new package the prefix \texttt{bibleref-} as
then it will appear close to \sty{bibleref} in the alphabetical list
in the catalogue. Suppose your new style is called
``mystyle''\footnote{Choose a more descriptive name than this.},
then create a file called \texttt{bibleref-mystyle.sty}. At the
start of the file, do:
\begin{verbatim}
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{bibleref-mystyle}[2011/01/19 v1.0 Bible reference
style conforming to something or other]
\end{verbatim}
(Change the date and the brief description in the line above to 
something more appropriate.) Next you need to load \sty{bibleref}:
\begin{verbatim}
\RequirePackage{bibleref}
\end{verbatim}
Now do the new style stuff (or redefine styles to provide
translations):
\begin{verbatim}
\newbiblerefstyle{mystyle}{%
\biblerefstyle{default}%
\renewcommand{\BRversestyle}[1]{\romannumeral##1}%
\setbooktitle{Revelation}{Apocalypse}%
}
\end{verbatim}
(Again change the above to whatever is appropriate.) Put any new
book definitions that your style may require, for example:
\begin{verbatim}
\addbiblebook{Susanna}{Story of Susanna}
\end{verbatim}
and add any \cs{biblerefmap} commands that are required. Then set
your new style as the current style, if appropriate:
\begin{verbatim}
\biblerefstyle{mystyle}
\end{verbatim}
On the last line of the file add:
\begin{verbatim}
\endinput
\end{verbatim}

Next you need to create a file called ``README'' that contains some
information about your new package, including what license you want
to distribute it under. (If you don't know anything about licenses, 
I recommend you choose the \LaTeX\ Project Public License, LPPL.)

It's worth adding a sample file or a little bit of documentation
that illustrates how the references appear.

Put your .sty file, README file and samples or documentation into a
zip archive. You're now ready to upload it. There are several upload
pages. The main one is at \url{http://www.ctan.org/upload.html}.
The name of your contribution is the name of your package
(``bibleref-mystyle'' in this example). Remember to put your name
and email in the relevant input boxes. Sometime after you've
uploaded your zip file, you'll get a copy of the announcement from
one of the CTAN maintainers. It usually takes a day to establish
itself. After that people can download and use it.

\section{Acknowledgements}
Many thanks to all the useful comments from comp.text.tex, 
especially from Jesse~Billett, Brooks~Moses and Ulrich~M.~Schwarz.

\StopEventually{\clearpage\phantomsection
\addcontentsline{toc}{section}{Index}
\PrintIndex
}

\end{document}
