% Test definitions for accents and composite accents in Greek
\documentclass[british,greek,a4paper]{article}
\pagestyle{empty}
\enlargethispage{4ex}
\usepackage{cmap} % fix search and cut-and-paste in Acrobat Reader

\usepackage%
{lmodern}
% {gfsartemisia}
% {gfsbaskerville}
% [default]{gfsbodoni}
% \usepackage[default]{gfscomplutum}
% {gfsdidot}
% [default]{gfsneohellenic}
% {lmodern} \usepackage{gfsporson} \renewcommand*\rmdefault{porson}
% [default]{gfssolomos}
% {kerkis}
% {teubner}
\renewcommand{\ttdefault}{txtt}

% Load the extended font encoding definitions (after font package)
\usepackage[LGRx,T1]{fontenc}

\usepackage{listings}
\lstset{basicstyle=\ttfamily}

% Babel package::

\usepackage{babel}
% revert the polutonikogreek definition of \~:
\addto\extraspolutonikogreek{\renewcommand*{\~}{\a~}}
\addto\extrasgreek{\renewcommand*{\~}{\a~}}

% "Lipsiakos" italic font `cbleipzig`:
\newcommand*{\lishape}{\fontencoding{LGR}\fontfamily{cmr}%
		       \fontshape{li}\selectfont}
\DeclareTextFontCommand{\textli}{\lishape}


\begin{document}

\selectlanguage{british}

\section*{Greek diacritics with standard accent macros}

% The greek diacritics are
%
% dase\~ia
% yil\'h
% t\'onos
% bare\~ia
% perispwm\'enh
% dialutika

\texttt{lgrxenc.def} is a comprehensive font encoding definition file.
Diacritics can be input using an extension of the standard macro
commands. Composite diacritics are input as
backslash followed by the LGR transliteration.%
\footnote{This makes it easy to follow the advise in
	  \emph{teubner-doc}: ``typeset your paper with the regular
	  accent vowel ligatures and [{\ldots}] substitute them in the
	  final revision with the accented vowel macros only in those
	  instances where the lack of kerning is disturbing''.}

The example in greek-usage.pdf:
%
\begin{quote} \selectlanguage{greek}
    T\'i f\'hic? \<Id\`wn \>enj\'ede pa\~id''
    \>eleuj\'eran t\`ac plhs\'ion N\'umfac stefano\~usan,
    S\'wstrate, \>er\~wn \'ap\~hljec e\>uj\'uc?
\end{quote}
is input as
\begin{lstlisting}
    T\'i f\'hic? \<Id\`wn \>enj\'ede pa\~id''
    \>eleuj\'eran t\`ac plhs\'ion N\'umfac stefano\~usan,
    S\'wstrate, \>er\~wn \'ap\~hljec e\>uj\'uc?
\end{lstlisting}
%
Improvements:
%
\begin{itemize}

\item Accents can be placed on any character:%
  \footnote{Babel's \emph{polutonikogreek} option re-defines
    \textbackslash\textasciitilde (even for
    Latin with \textbackslash textlatin).
    This document reverts the babel definition in the preamble
    to make the tilde/perispomeni-accent work as usual in both,
    English (\textgreek{ni\~n\~o} ni\~n\~o) and Greek
    (\foreignlanguage{greek}{ni\~n\~o \textlatin{ni\~n\~o}}). }
    \textgreek{\"k \`l \'m \~<n \<o \>'p \>9 \`\>-}

\item Kerning is preserved
  \selectlanguage{greek}
  \begin{tabular}[t]{llll}
     & \textlatin{roman} & \textlatin{italic} & \textlatin{cbleipzig} \\
    \foreignlanguage{british}{accent macro:}  &
      a\>ut'os & \emph{a\>ut\'os} & \textli{a\>ut\'os} \\
    \foreignlanguage{british}{transliteration:} &
      a>ut'os & \emph{a>ut'os} & \textli{a>ut'os}\\
  \end{tabular}
  \selectlanguage{british}

  Kerning only works with pre-composed glyphs:
  \textgreek{A\"UA $\rightarrow$ A\~UA}
  (like in any font encoding: AVA $\rightarrow$ A\~VA).


\item Following Greek typesetting convention, diacritics (except the
  dialytika) are placed to the left of capital letters and and dropped
  by \verb|\MakeUppercase|:

  \begin{quote} \selectlanguage{greek}
    \'antropos $\mapsto$ \MakeUppercase{\'antropos},
    \>'antropos $\mapsto$ \MakeUppercase{\'>antropos},\\
    Aqill\'eas $\mapsto$ \MakeUppercase{Aqill\'eas},
    \>Aqille\'us $\mapsto$ \MakeUppercase{\>Aqille\'us}.
  \end{quote}

  % Greek differs from Latin in that it capitalises letters with
  % diacritics differently, depending on whether the entire word is in
  % capitals (whereupon diacritics are eliminated), or the initial is
  % capitalised only, as in the first word in a sentence or in a title
  % (whereupon the diacritics are retained, although they appear to the
  % left of the letter rather than above it.)

  The dialytika is printed even in cases where it's not needed
  in lowercase:
  \begin{quote} \selectlanguage{greek}
    \'aulos $\mapsto$ \MakeUppercase{\'aulos},
    \'>aulos $\mapsto$ \MakeUppercase{\'\>aulos}%
    \footnote{\selectlanguage{british} Fails if written as
      \texttt{\textbackslash >'} in \texttt{polutonikogreek}
      (which changes the \texttt{\textbackslash uccode} of
      \texttt{\textbackslash >}):
      \selectlanguage{greek} \'>aulos $\mapsto$ \MakeUppercase{\>'aulos}}
    % from http://diacritics.typo.cz/index.php?id=69  μάινα -> ΜΑΪΝΑ
    m\'aina $\mapsto$ \MakeUppercase{m\'aina},\\
    % from  http://de.wikipedia.org/wiki/Neugriechische_Orthographie#Das_Trema
    % κέικ, ἀυπνία/αϋπνία
    k\'eik, $\mapsto$ \MakeUppercase{k\'eik},
    \>aupn\'ia $\mapsto$ \MakeUppercase{\>aupn\'ia}.
  \end{quote}
\end{itemize}

\selectlanguage{british} Composite diacritics can be specified as
backslash + LGR transliteration%
\footnote{However, \textbackslash{}MakeUppercase fails, if a
	  non-escaped tilde character (like in
	  \texttt{\textbackslash{}>\textasciitilde{}a}) is used in a
	  document which does not define the \emph{greek} or
	  \emph{polutonikogreek} language.
	 }
or combined accent macros, e.\,g. \textgreek{\~>a} can be written as
\begin{quote}
  \verb+\~>a+, \verb+\>~a+,
  \verb+\~\>{a}+, or \verb+\~\>a+.
\end{quote}
However, braces in composite accents
(\verb+\~{\>a}+, \verb+\~{>a}+, or \verb+\~{\>{a}}+)
lead to errors.

Accent macros can start with \verb|\a| instead of \verb|\| when the
short form is redefined, e.\,g. inside a \emph{tabbing} environment.
This works also for the new-defined Dasia and Psili shortcuts
(becoming \verb|\a<| and \verb|\a>|):
%
\begin{quote}
\selectlanguage{greek}
\begin{tabbing}
T'i f'hic? \= T\a'i f\a'hic? \\
<Id`wn \> \a>enj\a'ede pa\a~id
\end{tabbing}
\end{quote}

\end{document}
