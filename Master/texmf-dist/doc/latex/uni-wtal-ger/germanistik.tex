%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
\documentclass[12pt,notitlepage,parskip]{scrartcl}
\usepackage{xltxtra}
\usepackage{fontspec}
\usepackage{xunicode}
\setmainfont{Linux Libertine}
\usepackage{polyglossia}
\setmainlanguage[spelling=new, latesthyphen=true]{german}
\setotherlanguage[variant=ancient]{greek}
\usepackage{babel}
\usepackage[babel]{csquotes}
\usepackage[
  style=uni-wtal-ger,
  firstfull=true,
  journalnumber=afteryear,
  ibidpage=true,
  idembibformat=dash,
  maxnames=2,
  xref=true,
  bibwarn=false,
  ]{biblatex}

% Verweist auf die bib-Datei. Achtung: Dateinamen OHNE Endung eintragen:
\bibliography{germanistik}

\begin{document}
Dies ist eine Beispieldatei für den bib\LaTeX-Zitierstil \texttt{uni-wtal-ger}.

Auch wenn ich diesem Stil den sehr spezifischen Namen \texttt{uni-wtal-ger}
gegeben habe – eben weil ich ihn speziell für die Anwendung in der
literaturwissenschaftlichen Germanistik der Uni Wuppertal geschrieben habe –, so
ist er durchaus für viele andere – wahrscheinlich überwiegend
geisteswissenschaftliche – Zitierbedürfnisse geeignet.

Er basiert auf Dominik Waßenhovens \texttt{authortitle-dw} und modifiziert
diesen entsprechend. \texttt{authortitle-dw} muss daher ebenfalls installiert
sein.\footnote{http://biblatex.dominik-wassenhoven.de/authortitle-dw.shtml}

Die \texttt{Präambel} dieses Beispiels ist für \XeTeX\ optimiert, das ich
aufgrund seiner Vorteile – hier sei allem voran die UTF-8-Basis zu nennen – nur
jedem empfehlen kann. Sollte normales \LaTeX\ und kein UTF-8 verwendet werden,
so müssen die \texttt{Präambel} sowie auch die Einträge der \texttt{bib}-Datei
entsprechend angepasst werden.

Die Beispiele sowie die Zitierregeln orientieren sich an der Wuppertaler
Germanistik-Broschüre,\Footcite[Vgl.][36-40]{germanistik} zeigen und erklären
jedoch auch allgemein die Wiedergabe des Quellcodes.

\section{Bibliographische Angaben in literaturwissenschaftlichen Hausarbeiten
(der Germanistik der Bergischen Universität Wuppertal)}

\subsection{uni-wtal-ger – Umsetzung}

Wie schon auf der Homepage erwähnt, bildet dieser Zitierstil die Vorgaben der
Broschüre nur nahezu ab. Nahezu deshalb, da die dort beschriebenen Zitierregeln
sich leider etwas widersprechen und somit eine perfekte Nachahmung mit
bib\LaTeX\ nahezu unmöglich wird. So ist es z.B. inkonsequent, bei
unselbstständig publizierten Texten \textit{In: Vorname Nachname / Vorname
Nachname (Hg.)} – und somit weiterhin den schon weiter oben geforderten
\texttt{Delimiter} \textit{/} zu fordern, bei Lexika sowie bei Texten „in ein-
oder mehrbändigen Werken desselben Autors“ jedoch auf einmal Komma und
abgekürztes \textit{u.} zwischen den Namen zu verlangen. Auch die Position, an
der ein Band angegeben werden soll, ist zu unterschiedlich, um es ohne sehr
komplizierte Umwege über die logische Programmierung umzusetzen. Des Weiteren
halte ich die Tatsache, dass möglichst alles abgekürzt, \textit{Ebenda} auf der
Beispielseite jedoch ausgeschrieben wird,\Footcite[Vgl.][34]{germanistik} für
äußerst inkonsequent. Somit ahmt der hier vorliegende Zitierstil die Wuppertaler
Vorgaben zwar nicht zu 100\% nach – und zwar an manchen Stellen aus technischen
Gründen, an anderen mit Absicht –, jedoch immerhin fast und schließlich in einer
konsequenten Art und Weise; und das ist beim Zitieren schließlich ebenfalls
wichtig.

Im Folgenden nun ein anschaulicher Aufbau eines Beispiels, orientiert an den
Beispielen aus der Germanistik-Broschüre:

\subsubsection{Selbstständig publizierte Texte (Bücher)}

Die erste Monographie wird zitiert.\Footcite[1]{selbst-mono}
\texttt{firstfull=true} erzeugt eine detaillierte Fußnote. Entry Type ist:
\texttt{@Book}. Nun eine zweite Monographie desselben Autors, jedoch ein anderer
Titel. Das Verhalten von \textit{Ders.} (im Literaturverzeichnis) ist über
\texttt{idembibformat} anzupassen. Entry
Type: \texttt{@Book}.\Footcite[1]{selbst-mono-2test}\\
Nun zitiere ich einen Sammelband. Entry Type:
\texttt{@Collection}.\Footcite[1]{selbst-sammel} Damit wie durch die
Bestimmungen vorgegeben die 1. Auflage nicht genannt wird, muss das Feld
\texttt{edition} leer sein und darf nicht die \texttt{1} enthalten.\\ Ich
zitiere nun noch einmal die erste Monographie,\Footcite[1]{selbst-mono} verweise
noch einmal darauf\Footcite[Vgl.][9]{selbst-mono} und nun wieder auf den
Sammelband.\Footcite[Vgl.][5]{selbst-sammel} Die Zitierweise von Lempicki wird,
bedingt durch die Unterbrechung der Reihe durch den Sammelband, nun erst einmal
verkürzt (Zugriff auf \texttt{shorttitle}) und anschließend mit \textit{ebd.}
wiedergegeben. Nun zitiere ich eine Monographie eines weiteren Autors. Die volle
Fußnote wird wieder erzeugt.\Footcite[1]{selbst-mono2} Beim zweiten Mal
unterbricht nichts die Reihe, ein erneutes Zitieren erzeugt nun direkt
\textit{Ebd.}\Footcite[1]{selbst-mono2}

\subsubsection{Unselbstständig publizierte Texte (Aufsätze, Essays usw.)}
Nun wird ein Lexikon-Eintrag zitiert. Entry Type:
\texttt{@InCollection}.\Footcite[444]{unselbst-lexikon} In der Fußnote wird auf
den Eintrag selbst, im Literaturverzeichnis auf das komplette Lexikon verwiesen
(s.u.). Dies funktioniert folgendermaßen: Das Lexikon selbst hat einen eigenen
Eintrag in der \texttt{bib}-Datei (Entry Type: \texttt{@Collection}, hier
bezeichnet mit \texttt{selbst-lexikon-parent}). Dort sind alle wichtigen
Eckdaten zum Lexikon hinterlegt. Alle einzelnen, zitierten Lexikon-Artikel
bekommen anschließend einen eigenen Eintrag in der \texttt{bib}-Datei. Dort wird
dann zunächst auf den \texttt{parent} referiert (\texttt{xref}), danach werden
die artikelspezifischen Felder definiert und schließlich angegeben, dass kein
eigener Eintrag im Literaturverzeichnis erzeugt werden soll
(\texttt{skipbib=true}). Ein \texttt{options = \{useeditor=false\}} im
\texttt{parent}-Eintrag sorgt dafür, dass im Literaturverzeichnis letztlich der
Name des Lexikons vorne steht und dieses nicht nach dem \texttt{editor}
alphabetisch einsortiert wird.

Für Texte in Sammelbänden\Footcite[71\psq]{unselbst-sammel} ist der Entry Type
ebenfalls\Footcite[368]{unselbst-sammel2}
\texttt{@InCollection}.\Footcite[368]{unselbst-sammel2}

Hier noch der unter „Texte in Sammelbänden“ eingeordnete Metzler-Artikel – als
\texttt{@InCollection} natürlich.\Footcite{unselbst-lexikon2}

Texte in ein- oder mehrbändigen Werken desselben Autors kann man über
\texttt{@InBook} zitieren.\Footcite[269\psqq]{unselbst-sammel-ders}

Zeitschriftenartikel\Footcite[336]{unselbst-zeitsch} zitiert man über
\texttt{@Article}.\Footcite[40]{unselbst-zeitsch2} \texttt{volume} definiert
hierbei den Jahrgang, wohingegen \texttt{number} die Heftnummer definiert.\\
Zeitungsartikel (Tages- und Wochenzeitungen) funktionieren
genauso.\Footcite{unselbst-zeitung}

Ausstellungskataloge habe ich nicht berücksichtigt, ebenso wenig Internetzitate.

\section{Anmerkungen}
Ich würde mich insbesondere über Feedback aus Wuppertal freuen, damit ich einen
Eindruck davon bekomme, inwiefern \LaTeX\ sowie dieser Stil an der Bergischen
Universität eingesetzt werden. Für Anregungen bin ich dankbar.

– Carsten A. Dahlmann (\texttt{Ace@Dahlmann.net})

\newpage
\printbibliography

\end{document}