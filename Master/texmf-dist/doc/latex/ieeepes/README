Package ieeepes for LaTeX2e
===========================

This package allows typesetting of transactions, as well as discussions
and closures, for the IEEE Power Engineering Society:
	IEEE Transactions on Energy Conversion
	IEEE Transactions on Power Delivery
	IEEE Transactions on Power Systems


List of Files
-------------

README			this file
ieeepes.sty		macro definiton file
ieeepes.bst		style file for bibtex
ieeepes_doc.tex         documentation
ieeepes_check.tex	test for the ieeepes package (expect 0 errors!)
ieeepes_check.bib	needed for ieeepes_check.tex, ieeepes_doc.tex
ieeepes_skel.tex	skeleton for a paper
vk.eps			sample author image


Installation
------------

Copy *.sty to a directory which is searched by TeX.

Copy *.bst to a directory where bibtex looks for bibtex style files.

If you don't know how to do this consult your local TeX installation
documentation.

Note: The ieeepes package requires vmargin.sty to run.
      A copy of vmargin is included for your convenience.
Note: Printing author images with ieeepes requires the standard LaTeX2e
      graphics bundle.
Note: Both the vmargin and graphics bundle can be found on any CTAN archive
      or their mirrors.


Documentation
-------------

To obtain a printed version, process the manual ieeepes_doc.tex with LaTeX:

	latex ieeepes_doc
	latex ieeepes_doc
	latex ieeepes_doc

Generating this manual does not require vmargin or graphics. Do not run bibtex.

A printed version of the test file can be generated with:

	latex ieeepes_check
	bibtex ieeepes_check
	latex ieeepes_check
	latex ieeepes_check

Processing the test file requires ieeepes to be correctly installed.

The resulting dvi files can then be converted to the format your
printer understands.

I know this does not use the docstrip system - I might use it later,
but at the moment I find it more important to get finished at all.


Copyright
---------

% Copyright (C) Volker Kuhlmann 1993, 1995, 1996, 1999
%
% Volker Kuhlmann
% c/o University of Canterbury
% ELEC Dept
% Creyke Road
% Christchurch, New Zealand
% E-Mail: v.kuhlmann@elec.canterbury.ac.nz
%
% This program can be redistributed and/or modified under the terms
% of the LaTeX Project Public License, distributed from CTAN
% archives as macros/latex/base/lppl.txt; either
% version 1 of the License, or (at your option) any later version.

Please drop me a note if you use this package, including any comments
you wish to make. The more people I know of using this package, the more
I feel encouraged to make further improvements.

If you know of any mistakes in the layout, please contact me, saying
why you think the layout generated by ieeepes is incorrect, and I will
fix it.
