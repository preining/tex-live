% arara: xelatex
% arara: xelatex
% --------------------------------------------------------------------------
% the BOHR package
% 
%   simple atom representation according to the Bohr model
% 
% --------------------------------------------------------------------------
% Clemens Niederberger
% Web:    https://bitbucket.org/cgnieder/bohr/
% E-Mail: contact@mychemistry.eu
% --------------------------------------------------------------------------
% Copyright 2012 Clemens Niederberger
% 
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
% 
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Clemens Niederberger.
% --------------------------------------------------------------------------
% The bohr package consists of the files
%  - bohr.sty
%  - bohr_en.tex, bohr_en.pdf
%  - bohr_elements_english.def, bohr_elements_german.def
%  - README
% --------------------------------------------------------------------------
% If you have any ideas, questions, suggestions or bugs to report, please
% feel free to contact me.
% --------------------------------------------------------------------------
%
% if you want to compile this documentation you'll need the document class
% `cnpkgdoc' which you can get here:
%    https://bitbucket.org/cgnieder/cnpkgdoc/
% the class is licensed LPPL 1.3 or later
%
% use `xelatex --shell-scape' for compilation
%
\documentclass[toc=index]{cnpkgdoc}
\docsetup{
  pkg      = bohr ,
  cmd      = \BOHR ,
  code-box = {
    skipbelow        = .5\baselineskip plus .5ex minus .5ex ,
    skipabove        = .5\baselineskip plus .5ex minus .5ex ,
    roundcorner      = 3pt ,
    innerleftmargin  = 1.5em ,
    innerrightmargin = 1.5em
  } ,
  gobble   = 1
}
\addcmds{bohr,ch,elementname,elementsymbol,setbohr}
\usepackage{chemmacros}

\usepackage{fontspec}
\usepackage{superiors}
\usepackage{fnpct}

\usepackage{embrac}[2012/06/29]
\ChangeEmph{[}[,.02em]{]}[.055em,-.08em]
\ChangeEmph{(}[-.01em,.04em]{)}[.04em,-.05em]

\renewcommand*\othersectionlevelsformat[3]{\textcolor{main}{#3\autodot}\enskip}
\renewcommand*\partformat{\textcolor{main}{\partname~\thepart\autodot}}

\usepackage{filecontents}
\begin{filecontents}{\jobname.ist}
 heading_prefix "{\\bfseries "
 heading_suffix "\\hfil}\\nopagebreak\n"
 headings_flag  1
 delim_0 "\\dotfill "
 delim_1 "\\dotfill "
 delim_2 "\\dotfill "
 delim_r "\\textendash"
 suffix_2p "\\nohyperpage{\\,f.}"
 suffix_3p "\\nohyperpage{\\,ff.}"
\end{filecontents}

\usepackage{imakeidx}
% \indexsetup{noclearpage}
\makeindex[columns=2,intoc,options={-sl \jobname.ist}]

\TitlePicture{%
  \parbox{.7\linewidth}{This package provides means for the creation of simple
  Bohr models of atoms up to the atomic number 112. Additionally commands are
  provided to convert atomic numbers to element symbols or element names and vice
  versa.\par
  The package is inspired by a question on \url{http://tex.stackexchange.com/}:
  \href{http://tex.stackexchange.com/questions/73410/draw-bohr-atomic-model-with-electron-shells-in-tex}%
  {Draw Bohr atomic model with electron shells in TeX?}}%
}

\newcommand*\Default[1]{%
  \hfill\llap
    {%
      \ifblank{#1}%
        {(initially~empty)}%
        {Default:~\code{#1}}%
    }%
  \newline
}

\begin{document}

\section{Licence and Requirements}\secidx{License}
\BOHR is placed under the terms of the \LaTeX{} Project Public License,
version 1.3 or later (\url{http://www.latex-project.org/lppl.txt}).
It has the status ``maintained.''

\BOHR loads and needs the packages \paket[pgf]{tikz}, \paket{pgfopts} and
\paket{etoolbox}.
\secidx*{License}

\section{Options}\secidx{Options}
Every option described in the manual can also be used as package option. Options
are indicated as \key{option} and are all key/value like options. Some options can
be set without value, too. Then the \default{underlined} value is used.
\secidx*{Options}

\section{Usage}\secidx{Usage}
\BOHR is used like any other \LaTeXe\ package:
\begin{beispiel}[code only]
 \usepackage{bohr}
\end{beispiel}

The main command, \cmd{bohr}, creates the models:
\begin{beschreibung}
 \Befehl{bohr}\oa{<num of shells>}\ma{<number of electrons>}\ma{<atom name>}
\end{beschreibung}
This is described best by an example:
\begin{beispiel}
 \bohr{3}{Li}
\end{beispiel}
There is not much more to it. Another example using the optional argument:
\begin{beispiel}
 \bohr[2]{2}{$\mathrm{Li^+}$}
\end{beispiel}
\secidx*{Usage}

\section{Customization}\secidx{Customization}
\BOHR provides a handful of options to customize the appearance:
\begin{beschreibung}
 \Befehl{setbohr}{<options>}\newline
   Options are set in a key/value syntax using this command.
 \Option{insert-symbol}{\default{true}|false}\Default{false}
   If set to \code{true} \BOHR will insert the atomic symbol suiting to the given
   electron number if \emph{no} third argument is given.
 \Option{insert-number}{\default{true}|false}\Default{false}
   If set to \code{true} \BOHR will use the appropriate number of electrons
   for the given element symbol in the third argument if \emph{no} second argument
   is given. This of course only works if the third argument is one of the 112
   element symbols.
 \Option{insert-missing}{\default{true}|false}\Default{false}
   Sets both \key{insert-symbol} and \key{insert-number}.
 \Option{atom-style}{<code>}\Default{}
   This code will be placed immediatly before the third argument of \cmd{bohr}.
   The last macro in it may need one argument.
 \Option{name-options-set}{<tikz>}\Default{}
   This value is passed to the options of the \cmd*{node} the third argument of
   \cmd{bohr} is placed in.
 \Option{name-options-add}{<tikz>}\Default{}
   This value will be added to options set with \key{name-options-set}.
 \Option{nucleus-options-set}{<tikz>}\Default{draw=black!80,fill=black!10,opacity=.25}
   This value is passed to the options of the \cmd*{draw} command that draws the
   circle around the name-node.
 \Option{nucleus-options-add}{<tikz>}\Default{}
   This value will be added to options set with \key{nucleus-options-set}.
 \Option{nucleus-radius}{<dim>}\Default{1em}
   The radius of the circle around the name-node.
 \Option{electron-options-set}{<tikz>}\Default{blue!50!black!50}
   This value is passed to the options of the \cmd*{fill} command that draws the
   electrons.
 \Option{electron-options-add}{<tikz>}\Default{}
   This value will be added to options set with \key{electron-options-set}.
 \Option{electron-radius}{<dim>}\Default{1.5pt}
   The radius of the circles that represent the electrons.
 \Option{shell-options-set}{<tikz>}\Default{draw=blue!75,thin}
   This value is passed to the options of the \cmd*{draw} command that draws the
   circles that represent the shells.
 \Option{shell-options-add}{<tikz>}\Default{}
   This value will be added to options set with \key{shell-options-set}.
 \Option{shell-dist}{<dim>}\Default{1em}
   The distance between the nucleus and the first shell and between subsequent
   shells.
  \Option{german}{\default{true}|false}\Default{false}
   If set to \code{true} the German names are defined (see section~\ref{sec:additional}
   to understand what I mean). They are also defined if you use \paket{babel} and
   select language \code{german} or \code{ngerman} in the preamble. The same holds
   for \paket{polyglossia}.
\end{beschreibung}

\begin{beispiel}
 \setbohr{name-options-set={font=\footnotesize\sffamily}}
 \bohr{2}{He} \bohr{7}{N}
\end{beispiel}

\begin{beispiel}
 % uses package `chemmacros'
 \setbohr{atom-style={\footnotesize\sffamily\ch}}
 \bohr{0}{H+} \bohr{10}{F-}
\end{beispiel}

\begin{beispiel}
 \setbohr{
   shell-options-add = dashed,
   shell-dist        = .5em,
   insert-missing
 }
 \bohr{6}{} \bohr{}{K}
\end{beispiel}
\secidx*{Customization}

\section{Additional Commands}\label{sec:additional}
\BOHR provides some additional commands that return the element symbol or the
element name to a given atomic number and vice versa.
\begin{beschreibung}
 \Befehl{elementsymbol}{<atomic number>|<element name>}\newline
   Returns the element symbol for a given atomic number or element name.
   \cmd{elementsymbol}{80}: \elementsymbol{80}; \cmd{elementsymbol}{rhenium}:
   \elementsymbol{rhenium}.
 \Befehl{elementname}{<atomic number>|<element symbol>}\newline
   Returns the element name for a given atomic number or element symbol.
   \cmd{elementname}{80}: \elementname{80}; \cmd{elementname}{Rh}: \elementname{Rh}.
 \Befehl{atomicnumber}{<element name>|<element symbol>}\newline
   Returns the atomic number for a given element name or element symbol.
   \cmd{atomicnumber}{Hg}: \atomicnumber{Hg}; \cmd{atomicnumber}{rhenium}
   \atomicnumber{rhenium}.
 \Befehl{Z}\newline
   If this command isn't defined by some other package it is available as an
   alias of \cmd{atomicnumber}.
\end{beschreibung}

\begin{beispiel}
 The elements \elementname{F}, \elementname{Cl}, \elementname{Br},
 \elementname{I} and \elementname{At} are called \emph{halogens}.
\end{beispiel}

\section{Implementation}
\implementation

\printindex
\end{document}