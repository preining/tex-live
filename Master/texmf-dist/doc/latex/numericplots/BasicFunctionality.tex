% Copyright 2010 Thomas Koenig, Alexander Michel
%
% This file is part of NumericPlots.
%
% NumericPlots is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% NumericPlots is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with NumericPlots.  If not, see <http://www.gnu.org/licenses/>.

\part{Using the package}

\section{Basic Functionality}

The package NumericPlots\\
\verb+\usepackage{NumericPlots}+
\\
is intended to be used to plot numeric data which
may, e.g., be exported from Matlab by export2latex.m. The data must be defined
in the form
\begin{verbatim}
	\def\IdentI{
	1.0 1.0e2
	1.1 11e1
	1.2 1.25e2
	1.3 110
	1.4 100
	1.5 90
	1.6 80
	}
\end{verbatim}
\def\IdentI{
	1.0 1.0e2
	1.1 11e1
	1.2 1.25e2
	1.3 110
	1.4 100
	1.5 90
	1.6 80
	}
\def\IdentII{
	1.0 125
	1.05 100
	1.1 75
	1.15 85
	1.2 90
	1.3 115
	1.4 130
	1.5 125
	1.6 120
	}
\def\LogData{
	6 6
	10 10
	20 20
	30 30
	40 40
	50 50
	60 60
	70 70
	80 80
	90 90
	100 100
	200 200
	300 300
	400 400
	500 500
	600 600
	700 700
	800 800
	900 900
	1000 1000
	1100 1100
	1200 1200
	1300 1300
	1400 1400
	1500 1500
}
where the first column contains the x, the second column the y-data.

\subsection{plots}

% \begin{LTXExample}
% \begin{NumericDataPlot}{\textwidth}{5cm}
% 	\setxAxis{xMin=1, xMax=2, Dx=0.1}
% 	\setyAxis{yMin=50, yMax=150, Dy=25}
% 	
% 	\plotxAxis{x-axis label}
% 	\plotyAxis{y-axis label}
% 	
% 	\listplot[style=StdLineStyA]{\IdentI}
% \end{NumericDataPlot}
% \end{LTXExample}

The easiest plot may be done by
\begin{verbatim}
	\begin{NumericDataPlot}{\textwidth}{5cm}
		\setxAxis{xMin=-1, xMax=2, Dx=0.5}
		\setyAxis{yMin=-50, yMax=150, Dy=25}
		
		\plotxAxis{x-axis label}
		\plotyAxis{y-axis label}
		
		\listplot[style=StdLineStyA]{\IdentI}
	\end{NumericDataPlot}
\end{verbatim}
\begin{NumericDataPlot}{\textwidth}{5cm}
	\setxAxis{xMin=-1, xMax=2, Dx=0.5, xO=0}
	\setyAxis{yMin=-50, yMax=150, Dy=25, yO=0}
	
	\plotxAxis{x-axis label}
	\plotyAxis{y-axis label}
	
	\listplot[style=StdLineStyA]{\IdentI}
\end{NumericDataPlot}

if you want to add a legend, you simply call

\begin{minipage}{0.5\linewidth}
\begin{verbatim}
\LegendDefinition{
	\LegLine{style=StdLineStyA} & IdentI
}
\end{verbatim}
\end{minipage}\begin{minipage}{0.5\linewidth}
\centering
\LegendDefinition{
	\LegLine{style=StdLineStyA} & IdentI
}
\end{minipage}

To plot multiple data in one plot call \newline
\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
	\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=2, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxAxis{x-axis label}
		\plotyAxis{y-axis label}
		
		\listplot[style=StdLineStyA]
		   {\IdentI}
		\listplot[style=StdLineStyB]
		   {\IdentII}
		
		\putSE{\LegendDefinition{
			\LegLine{style=StdLineStyA} & IdentI \\
			\LegLine{style=StdLineStyB} & IdentII
		}}
	\end{NumericDataPlot}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\vspace{10pt}
	\begin{NumericDataPlot}{\textwidth}{5cm}
		\setxAxis{xMin=1, xMax=2, Dx=0.2}
		\setyAxis{yMin=50, yMax=150, Dy=25}
		
		\plotxAxis{x-axis label}
		\plotyAxis{y-axis label}
		
		\listplot[style=StdLineStyA]{\IdentI}
		\listplot[style=StdLineStyB]{\IdentII}
		
		\putSE{\LegendDefinition{
			\LegLine{style=StdLineStyA} & IdentI \\
			\LegLine{style=StdLineStyB} & IdentII
		}}
	\end{NumericDataPlot}
\end{minipage}


\subsection{Label and TickLabels}

The commands \texttt{plotxAxis} and \texttt{plotyAxis} take the options
\texttt{NoLabel}, \texttt{NoTicks}, \texttt{NoTickLabel} as well as
\texttt{LabelOption} and \texttt{TickLabelOption} which may be used to eliminate
or change the look of the labels.

Standard values for \texttt{LabelOption} and \texttt{TickLabelOption} may be set\\
by \verb|\newcommand{\StdLabelOption}{\color{blue}|\\
and \verb|\newcommand{\StdTickLabelOption}{\small}|.

The option \texttt{xLabelSep} for \verb|\plotxAxis| (and \texttt{yLabelSep} for
\verb|\plotyAxis|) may be used to set the seperation between the axis and the label. Standard value
is 10pt for the x-label and 20pt for the y-label.

\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
	\begin{NumericDataPlot}{\textwidth}{5cm}
		\setxAxis{xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis{yMin=75, yMax=130, Dy=25}
		
		\plotxAxis
		[LabelOption=\LARGE,%
		TickLabelOption=\color{red},%
		xLabelSep=40pt]
		{x-axis label}
		\plotyAxis
		[NoLabel, NoTicks, NoTickLabel]
		{y-axis label}
		
		\listplot[style=StdLineStyA]{\IdentI}
		\listplot[style=StdLineStyB]{\IdentII}
		
	\end{NumericDataPlot}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\vspace{10pt}
	\begin{NumericDataPlot}{\textwidth}{5cm}
		\setxAxis{xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis{yMin=75, yMax=130, Dy=25}
		
		\plotxAxis[LabelOption=\LARGE, TickLabelOption=\color{red},
		xLabelSep=40pt]{x-axis label}
		\plotyAxis[NoLabel, NoTicks, NoTickLabel]{y-axis label}
		
		\listplot[style=StdLineStyA]{\IdentI}
		\listplot[style=StdLineStyB]{\IdentII}
		
	\end{NumericDataPlot}
\end{minipage}


\subsection{Place ``Objects'' in the plot.}

There are basically two different options to place objects in the plot. To
understand the difference one has to keep in mind that the axis have two
different coordinate systems. One is the system defined by xMin, xMax, yMin and
yMax (refered to as ``DataCoordinateSystem''), the other ist the system defined
by xCoordMin, xCoordMax, yCoordMin and yCoordMax (refered to as
``PictureCoordinateSystem''), see section \ref{sec:MultiplePlots}.

It is now possible to place stuff in the graph with the DataCoordinates with the
command NDPput, see the following example.

\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
	\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=2, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxAxis{x-axis label}
		\plotyAxis[NoLabel]{}
		
		\listplot[style=StdLineStyA]
		   {\IdentI}
		
		% put some stuff somewhere
		\NDPput[x=1.2, y=75, RefPoint=br]{text}
		\NDPput[x=1.2, y=100]{$a^2$}
		
		% or put nodes...
		\NDPput[x=1.6, y=100]{\pnode{A}}
		\NDPput[x=1.8, y=150]{\pnode{B}}
		% ...and draw a line between them
		\ncline{A}{B}
		
		% or put the legend at a specific position
		\NDPput[x=1.8, y=75]{\LegendDefinition{
			\LegLine{style=StdLineStyA} & IdentI
		}}
	\end{NumericDataPlot}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\vspace{10pt}
	\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=2, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxAxis{x-axis label}
		\plotyAxis[NoLabel]{}
		
		\listplot[style=StdLineStyA]
		   {\IdentI}
		
		% put some stuff somewhere
		\NDPput[x=1.2, y=75, RefPoint=br]{text}
		\NDPput[x=1.2, y=100]{$a^2$}
		
		% or put nodes...
		\NDPput[x=1.6, y=100]{\pnode{A}}
		\NDPput[x=1.8, y=150]{\pnode{B}}
		% ...and draw a line between them
		\ncline{A}{B}
		
		% or put the legend at a specific position
		\NDPput[x=1.8, y=75]{\LegendDefinition{
			\LegLine{style=StdLineStyA} & IdentI
		}}
	\end{NumericDataPlot}
\end{minipage}

For convenience the commands \verb|\putXX{object}| where
$XX\in\left(N,S,E,W,NW,NE,SW,SE\right)$ are defined to place something in the
North, South,\ldots, SouthEast corner of the plot. Also, the command
\verb|\putExpY{xx}| and \verb|\putExpX{xx}| may be used to place exponents at
the axes.

\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
	\begin{NumericDataPlot}
	   ...
	   \putExpY{$\times 10^{-3}$}
	   \putExpX{$\times 10^{-6}$}
		
	   \putN{N}
	   \putS{S}
	   \putW{W}
	   \putE{E}
	   \putNW{NW}
	   \putNE{NE}
	   \putSW{SW}
	   \putSE{SE}
	\end{NumericDataPlot}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\vspace{10pt}
	\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxAxis{x-axis label}
		\plotyAxis[NoLabel]{}
		
		\listplot[style=StdLineStyA]
		   {\IdentI}
		
		\putExpY{$\times 10^{-3}$}
		\putExpX{$\times 10^{-6}$}
		
		\putN{N}
		\putS{S}
		\putW{W}
		\putE{E}
		\putNW{NW}
		\putNE{NE}
		\putSW{SW}
		\putSE{SE}
	\end{NumericDataPlot}
\end{minipage}


Alternatively, stuff can be placed
within the plot with \verb|\rput|.

\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
	\begin{NumericDataPlot}
		...
		
		% put text in the middle of the plot
		\rput{45}(500,500){text}
		% put a formula in the lower left corner
		\rput[bl](0,0){$a^2+b^2=c^2$}
		
		% or put nodes...
		\NDPput[x=1.2, y=125]{\pnode{A}}
		\rput(750,900){\Rnode{B}{peak}}
		% ...and draw a line between them
		\ncline{<-}{A}{B}
		
		% or put the legend at a specific position
		\rput{-45}(750,250){\LegendDefinition{
			\LegLine{style=StdLineStyA} & IdentI
		}}
	\end{NumericDataPlot}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\vspace{10pt}
	\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxAxis{x-axis label}
		\plotyAxis[NoLabel]{}
		
		\listplot[style=StdLineStyA]
		   {\IdentI}
		
		% put text in the middle of the plot
		\rput{45}(500,500){text}
		% put a formula in the lower left corner
		\rput[bl](0,0){$a^2+b^2=c^2$}
		
		% or put nodes...
		\NDPput[x=1.2, y=125]{\pnode{A}}
		\rput(750,900){\Rnode{B}{peak}}
		% ...and draw a line between them
		\ncline{<-}{A}{B}
		
		% or put the legend at a specific position
		\rput{-45}(750,250){\LegendDefinition{
			\LegLine{style=StdLineStyA} & IdentI
		}}
	\end{NumericDataPlot}
\end{minipage}



% =================================
% |								  |
% |     Linestyles and colors     |
% |								  |
% =================================

\subsection{Linestyles and colors}

While using the package, there are predefined linestyles which may be used:

\begin{minipage}{0.6\linewidth}
	\centering
	\small
	
	\input{DataTestRealData}
	
	\begin{NumericDataPlot}[]{\linewidth}{0.7\linewidth}
		\setxAxis{xMin=2, xMax=17, xO=5, Dx=4}
		\setyAxis{yMin=20, yMax=70, yO=20, Dy=20}
			
        \plotxAxis[NoLabel, AxisStyle=Boxed]{}
        \plotyAxis[NoLabel, AxisStyle=Boxed]{}

		\listplot[style=StdLineStyA]{\DataA}
		\listplot[style=StdLineStyB]{\DataB}
		\listplot[style=StdLineStyC]{\DataC}
		\listplot[style=StdLineStyD]{\DataD}
		\listplot[style=StdLineStyE]{\DataE}
		\listplot[style=StdLineStyF]{\DataF}
		\listplot[style=StdLineStyG]{\DataG}
	\end{NumericDataPlot}
\end{minipage}
\begin{minipage}{0.4\linewidth}
	\centering
	\LegendDefinition{
		\LegLine{style=StdLineStyA} & StdLineStyA \\
		\LegLine{style=StdLineStyB} & StdLineStyB \\
		\LegLine{style=StdLineStyC} & StdLineStyC \\
		\LegLine{style=StdLineStyD} & StdLineStyD \\
		\LegLine{style=StdLineStyE} & StdLineStyE \\
		\LegLine{style=StdLineStyF} & StdLineStyF \\
		\LegLine{style=StdLineStyG} & StdLineStyG \\
	}
\end{minipage}

When using the package option \texttt{BW} the standard line styles will be
replaced by their black and white counterparts:

\begin{minipage}{0.6\linewidth}
	\centering
	\small
	
	\input{DataTestRealData}
	
	\begin{NumericDataPlot}[]{\linewidth}{0.7\linewidth}
		\setxAxis{xMin=2, xMax=17, xO=5, Dx=4}
		\setyAxis{yMin=20, yMax=70, yO=20, Dy=20}
			
        \plotxAxis[NoLabel, AxisStyle=Boxed]{}
        \plotyAxis[NoLabel, AxisStyle=Boxed]{}

		\listplot[style=BWStdLineStyA]{\DataA}
		\listplot[style=BWStdLineStyB]{\DataB}
		\listplot[style=BWStdLineStyC]{\DataC}
		\listplot[style=BWStdLineStyD]{\DataD}
		\listplot[style=BWStdLineStyE]{\DataE}
		\listplot[style=BWStdLineStyF]{\DataF}
		\listplot[style=BWStdLineStyG]{\DataG}
	\end{NumericDataPlot}
\end{minipage}
\begin{minipage}{0.4\linewidth}
	\centering
	\LegendDefinition{
		\LegLine{style=BWStdLineStyA} & BWStdLineStyA \\
		\LegLine{style=BWStdLineStyB} & BWStdLineStyB \\
		\LegLine{style=BWStdLineStyC} & BWStdLineStyC \\
		\LegLine{style=BWStdLineStyD} & BWStdLineStyD \\
		\LegLine{style=BWStdLineStyE} & BWStdLineStyE \\
		\LegLine{style=BWStdLineStyF} & BWStdLineStyF \\
		\LegLine{style=BWStdLineStyG} & BWStdLineStyG \\
	}
\end{minipage}

For values which are nearly the same (reference and measurement, e.g.) the
following line styles may be used:

\begin{minipage}{0.6\linewidth}
	\centering
	\small
	
	\input{DataTestRealData}
	
	\begin{NumericDataPlot}[]{\linewidth}{0.7\linewidth}
		\setxAxis{xMin=2, xMax=17, xO=5, Dx=4}
		\setyAxis{yMin=20, yMax=70, yO=20, Dy=20}
			
        \plotxAxis[NoLabel, AxisStyle=Boxed]{}
        \plotyAxis[NoLabel, AxisStyle=Boxed]{}

		\listplot[style=StdLineStyX]{\DataC}
		\listplot[style=StdLineStyY]{\DataC}
		
		\listplot[style=BWStdLineStyX]{\DataD}
		\listplot[style=BWStdLineStyY]{\DataD}
	\end{NumericDataPlot}
\end{minipage}
\begin{minipage}{0.4\linewidth}
	\centering
	\LegendDefinition{
		\LegLine{style=StdLineStyX} & StdLineStyX \\
		\LegLine{style=StdLineStyY} & StdLineStyY \\
		\LegLine{style=BWStdLineStyX} & BWStdLineStyX \\
		\LegLine{style=BWStdLineStyY} & BWStdLineStyY \\
	}
\end{minipage}

It is, of course, possible to redefine the available linestyles or to define new
linestyles.

\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
	\definecolor{MyColor}{cmyk}{0.6 0.21 1.0 0.2}
	\newpsstyle{MyLine}
	  {linecolor=MyColor, linewidth=2pt,
	   linestyle=dashed,
	   dash=1pt 1pt 4pt 1pt 1pt 3pt,
	   dotstyle=*, showpoints=true,
	   dotsize=5pt}
	\newpsstyle{MyLineA}
	  {linecolor=blue, linestyle=dotted,
	   dotstyle=asterisk, showpoints=true}
	
	\listplot[style=MyLine]
	  {\IdentI}
	\listplot[style=MyLineA]
	  {\IdentII}
	
	\putSE{\LegendDefinition{
	  \LegLine{style=MyLine} & IdentI
	  \LegLine{style=MyLineA} & IdentII
	}}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\begin{NumericDataPlot}
	   {\textwidth}{5cm}
	
	   \definecolor{MyColor}{cmyk}{0.6 0.21 1.0 0.2}
	   \newpsstyle{MyLine}{linecolor=MyColor, linewidth=2pt, linestyle=dashed,
	   dash=1pt 1pt 4pt 1pt 1pt 3pt, dotstyle=*, showpoints=true, dotsize=5pt}
	   \newpsstyle{MyLineA}{linecolor=blue, linestyle=dotted,
	   dotstyle=asterisk, showpoints=true}
	
		\setxAxis
		   {xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxAxis[NoLabel]{}
		\plotyAxis[NoLabel]{}
		
		\listplot[style=MyLine]
		   {\IdentI}
		\listplot[style=MyLineA]
		   {\IdentII}
		
		\putSE{\LegendDefinition{
			\LegLine{style=MyLine} & IdentI
		}}
	\end{NumericDataPlot}
\end{minipage}


% =================================
% |								  |
% |            Legend             |
% |								  |
% =================================

\subsection{Legend}

The legend may be created with \verb|\LegendDefinition|. The command takes the
two optional arguments \texttt{nrCols} and \texttt{LabelOrientation=[l|c|r]}.
The mandatory argument is the definition of a table as demonstrated in the
follwing examples.

	\begin{verbatim}
	\LegendDefinition{
		\LegLine{style=StdLineStyA} & IdentI\\
		\LegLine{style=StdLineStyB, linewidth=3pt} & second legend
	}
	\end{verbatim}
	\LegendDefinition{
		\LegLine{style=StdLineStyA} & IdentI\\
		\LegLine{style=StdLineStyB, linewidth=3pt} & second legend
	}
	\begin{verbatim}
	\newpsstyle{LegendBoxStyle}%
		{framearc=0.2, fillstyle=solid, fillcolor=yellow, opacity=0.2}
	\LegendDefinition[nrCols=2]{
		\LegLine{style=StdLineStyA} & IdentI &
		\LegLine{style=StdLineStyB, linewidth=3pt} & legend 2
	\newpsstyle{LegendBoxStyle}%
		{fillstyle=solid, fillcolor=white}
	}
	\end{verbatim}
	\newpsstyle{LegendBoxStyle}%
		{framearc=0.2, fillstyle=solid, fillcolor=yellow, opacity=0.2}
	\LegendDefinition[nrCols=2]{
		\LegLine{style=StdLineStyA} & IdentI &
		\LegLine{style=StdLineStyB, linewidth=3pt} & legend 2
	}
	\newpsstyle{LegendBoxStyle}%
		{fillstyle=solid, fillcolor=white}
	\begin{verbatim}
	\LegendDefinition[LabelOrientation=c]{
		\LegLine{style=StdLineStyA} & IdentI\\
		\LegLine{style=StdLineStyB, linewidth=3pt} & legend 2 \\
		\LegLine{style=StdLineStyC} & whatever this data is\ldots\\
		\LegLine{style=StdLineStyD} & and more data
	}
	\end{verbatim}
	\LegendDefinition[LabelOrientation=c]{
		\LegLine{style=StdLineStyA} & IdentI\\
		\LegLine{style=StdLineStyB, linewidth=3pt} & second legend\\
		\LegLine{style=StdLineStyC} & whatever this data is\ldots\\
		\LegLine{style=StdLineStyD} & and more data
	}

\subsection{Add Lines to the Plot}

Horizontal and vertical lines may be added to the plot with the commands
\verb|\NDPhline{coord}|, \verb|\NDPvline{coord}| and \verb|\NDPline{coord}|. It is also possible to put
nodes and draw lines between them, see placing stuff\ldots

\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
	\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxAxis[NoLabel]{}
		\plotyAxis[NoLabel]{}
		
		\listplot[style=StdLineStyA]
		   {\IdentI}
		
		\NDPhline[linecolor=LineColorD]{73}
		\NDPvline[linecolor=LineColorE, linestyle=dashed]{1.5}
		\NDPline[linecolor=red]{1.1}{75}{1.3}{125}
	\end{NumericDataPlot}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\vspace{10pt}
	\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxAxis[NoLabel]{}
		\plotyAxis[NoLabel]{}
		
		\listplot[style=StdLineStyA]
		   {\IdentI}
		
		\NDPhline[linecolor=LineColorD]{73}
		\NDPvline[linecolor=LineColorE, linestyle=dashed]{1.5}
		\NDPline[linecolor=red]{1.1}{75}{1.3}{125}
	\end{NumericDataPlot}
\end{minipage}

\subsection{Add Boxes to the Plot}

Horizontal and vertical boxes may be added to the plot with the commands
\verb|\NDPhbox{coord}|, \verb|\NDPvbox{coord}| and \verb|\NDPbox{coord}|. It is also possible to put
nodes and draw lines between them, see placing stuff\ldots

\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
		\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxAxis[NoLabel]{}
		\plotyAxis[NoLabel]{}
				
		\NDPhbox[fillstyle=solid,fillcolor=green]%
                {75.0}{100.0}%

		\listplot[style=StdLineStyA] {\IdentI}

		\NDPvbox[fillstyle=solid,fillcolor=red]
                {1.4}{1.5}

        \NDPbox[fillstyle=solid,%
            fillcolor=orange, opacity=0.2, linestyle=none]%
			{1.1}{60}{1.15}{130}
	\end{NumericDataPlot}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\vspace{10pt}
		\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxAxis[NoLabel]{}
		\plotyAxis[NoLabel]{}
				
		\NDPhbox[fillstyle=solid,fillcolor = green]%
                {75.0}{100.0}%

		\listplot[style=StdLineStyA] {\IdentI}

		\NDPvbox[fillstyle=solid,fillcolor=red]
                {1.4}{1.5}

        \NDPbox[fillstyle=solid,%
            fillcolor=orange, opacity=0.2, linestyle=none]%
			{1.1}{60}{1.15}{130}
	\end{NumericDataPlot}
\end{minipage}


\subsection{Grid}

One may choose not to plot the grid with the option \texttt{NoGrid} for the
commands \verb|\plotxAxis| and \verb|\plotyAxis|.

If the grid is plottet with the axis it may happen that the grid is plottet over
the axis. To avoid this, plot the grid first and then plot the axis as shown.

\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
	\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxGrid
		\plotyGrid
		\plotxAxis
		[NoLabel, NoGrid, AxisStyle=Boxed]{}
		\plotyAxis
		[NoLabel, NoGrid, AxisStyle=Boxed]{}
		
		\listplot[style=StdLineStyA]
		   {\IdentI}
		
	\end{NumericDataPlot}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\vspace{10pt}
	\begin{NumericDataPlot}
	   {\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=1.6, Dx=0.2}
		\setyAxis
		   {yMin=50, yMax=150, Dy=25}
		
		\plotxGrid
		\plotyGrid
		\plotxAxis[NoLabel, NoGrid, AxisStyle=Boxed]{}
		\plotyAxis[NoLabel, NoGrid, AxisStyle=Boxed]{}
		
		\listplot[style=StdLineStyA]
		   {\IdentI}
		
	\end{NumericDataPlot}
\end{minipage}


\subsection{Logarithmic axes}


\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
	\begin{NumericDataPlot}{\textwidth}{5cm}
		\setxAxis
		   {xMin=6, xMax=2500, Dx=10, xLog}
		\setyAxis
		   {yMin=0, yMax=2500, Dy=500}
		\plotxAxis{}
		\plotyAxis{}
		
		\listplot{\LogData}
	\end{NumericDataPlot}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\vspace{10pt}
	\begin{NumericDataPlot}{\textwidth}{5cm}
		\setxAxis{xMin=6, xMax=2500, Dx=10, xLog}
		\setyAxis{yMin=0, yMax=2500, Dy=500}
		\plotxAxis{}
		\plotyAxis{}
		
		\listplot{\LogData}
	\end{NumericDataPlot}
\end{minipage}


\begin{minipage}[T]{0.5\linewidth}
	\begin{verbatim}
	\begin{NumericDataPlot}{\textwidth}{5cm}
		\setxAxis
		   {xMin=1, xMax=1500, Dx=10, xLog}
		\setyAxis
		   {yMin=1, yMax=1500, Dy=10, yLog}
		\plotxAxis{}
		\plotyAxis{}
		
		\listplot{\LogData}
	\end{NumericDataPlot}
	\end{verbatim}
\end{minipage}
\begin{minipage}[T]{0.5\linewidth}
	\vspace{10pt}
	\begin{NumericDataPlot}{\textwidth}{5cm}
		\setxAxis{xMin=1, xMax=1500, Dx=10, xLog}
		\setyAxis{yMin=1, yMax=1500, Dy=10, yLog}
		\plotxAxis{}
		\plotyAxis{}
		
		\listplot{\LogData}
	\end{NumericDataPlot}
\end{minipage}





