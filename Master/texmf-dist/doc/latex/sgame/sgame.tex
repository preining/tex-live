%
%
%
\documentclass[12pt]{article}
\usepackage{mjoarti}
\usepackage{mathpazo}
\usepackage{pstricks}
\usepackage{pst-mjo}
\usepackage{sgame}
\usepackage{url}
\begin{document}

\renewcommand{\descriptionlabel}[1]{\hspace\labelsep
                                \normalfont #1\phantom{.}}

\begin{center}
{\bfseries Manual for {\tt sgame.sty} and {\tt sgamevar.sty}}\\
by\\
Martin J. Osborne\\
{\tt Martin.Osborne@utoronto.ca}\\
For Version 2.14, 2008-12-2
\end{center}

{\sloppy
\section{Introduction}
{\tt sgame.sty} and {\tt sgamevar.sty} are \LaTeXe\ style files for drawing
strategic games.  The latest versions are available at
\url{http://www.economics.utoronto.ca/osborne/latex}.  The style
\texttt{sgamevar.sty} (but not \texttt{sgame.sty}) is compatible with
\texttt{beamer}.  Both styles are incompatible with the \texttt{memoir} class
and with \texttt{array.sty} (and hence any package, like \texttt{jurabib.sty},
that loads \texttt{array.sty}).  Please notify me of bugs.  
\section{sgame.sty versus sgamevar.sty}
\verb+sgame.sty+ and \verb+sgamevar.sty+ differ in two respects.
\begin{itemize}
   \item The character that ``tabs'' to a new column is \verb+&+ in
\verb+sgame.sty+ and \verb+\>+ in \verb+sgamevar.sty+.  In every other respect
the syntax is identical.  This manual applies to \verb+sgame.sty+.  If you are
using \verb+sgamevar.sty+, replace every occurrence of \verb+&+ with
\verb+\>+.
   \item \verb+sgame.sty+ produces a \verb+game+ environment that cannot be
used as an argument of a macro, whereas \verb+sgamevar.sty+ does not have this
limitation.  You need to use \verb+sgamevar.sty+, for example, to be able to
make a \verb+game+ environment an argument of TeXPower's \verb+\stepwise+
macro.
\end{itemize}

(I prefer the \verb+&+ syntax, but have been unable to find a way of
implementing it without changing the \verb+\catcode+ of \verb+&+, which
prevents a \verb+game+ environment from being the argument of a macro.)
\section{Installation}
\begin{itemize}
   \item Put \texttt{sgame.sty} (and/or \texttt{sgamevar.sty}) in a directory
from which \TeX\ reads input files.  (In MiKTeX the directory might be
something like \verb+\miktex\localtexmf\tex\latex\+ or a subdirectory
thereof.)
   \item Let \TeX\ know that \texttt{sgame} and/or \texttt{sgamevar} have
arrived.  (In MiKTeX, ``refresh the filename database''.)
   \item Check that you have the \verb+color+ package.  One way to check is to
put \verb+\usepackage{color}+ in the preamble of a document and see if you get
an error.  If you don't have the package, get it from CTAN.
   \item To use the package in a document, put the lines
\verb+\usepackage{sgame}+ (or \verb+\usepackage{sgamevar}+) and
\verb+\usepackage{color}+ (or \verb+\usepackage{pstcol}+, part of PSTricks,
which loads \verb+color+) in the preamble.
\end{itemize}
\section{Compatibility with other packages}
\begin{itemize}
   \item The style is incompatible with \verb+colortbl.sty+.
   \item To use in \verb+beamer+, put a game in a\newline
\verb+\begin{frame}[fragile]...\end{frame}+ environment.
   \item In the case of incompatibilities with other styles, try using
\verb+sgamevar+ rather than \verb+sgame+.
\end{itemize}
\section{Description}
The style defines two environments, \verb+game+ and \verb+game*+.  Each
environment can be used in four ways, depending on the values of its optional
arguments.  In each case, the starred version differs from the unstarred 
versionin that action labels are not accommodated.
\subsection{No optional argument}
Without any optional arguments, its syntax is\vspace*{4mm}\newline
\verb+\begin{game}{+\emph{number-of-rows}%
\verb+}{+\emph{number-of-columns}\verb+}+\emph{game-body}%
\verb+\end{game}+\vspace*{4mm}\newline
where \emph{number-of-rows} is the number of rows in the game and
\emph{number-of-columns} is the number of columns.  \emph{game-body} consists
of rows in the style of the rows in a \verb+tabular+ environment, \verb+&+
``tabbing'' to a new cell, and \verb+\\+ ending a row.

The following example is illustrated in Figure~\ref{f:ex1}.

\begin{verbatim}
\def\sgtextcolor{blue}%
\def\sglinecolor{red}%
\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{2}
      & $L$    & $R$\\
$T$   &$2,2$   &$100,35$\\
$B$   &$3,0$   &$350,16$
\end{game}\hspace*{\fill}%
\begin{game*}{2}{2}
$2,2$   &$100,35$\\
$3,0$   &$350,16$
\end{game*}\hspace*{\fill}%
\caption[]{A two row, two column strategic game, with and without
action labels.}
\end{figure}
\end{verbatim}

\def\sgtextcolor{blue}%
\def\sglinecolor{red}%
\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{2}
&$L$&$R$\\
$T$&$2,2$&$100,35$\\
$B$&$3,0$&$350,16$
\end{game}\hspace*{\fill}%
\begin{game*}{2}{2}
$2,2$&$100,35$\\
$3,0$&$350,16$
\end{game*}\hspace*{\fill}%
\caption[]{A two row, two column strategic game, with and without action
labels.}\label{f:ex1}
\end{figure}
\begin{itemize}
   \item Note that the width of every column is the same.  Further, the
content of each cell of the table is centered vertically.
   \item As in a \LaTeXe\ \verb+tabular+ environment, spaces on either side of
the text in each cell is ignored.  (Thus in the \verb+tex+ file you can, if
you wish, align the columns using spaces or tabs.)
   \item The first \verb+&+ {\bfseries must} be preceded by a space token
(space or carriage return).  (It may not immediately follow the closing brace
of the second argument.)
   \item If all the text in every cell of the payoff matrix is in math mode,
you can avoid typing all the \verb+$+'s by setting \verb+\gamemathtrue+,
either by putting this command in your file, or by changing
\verb+\gamemathfalse+ to \verb+\gamemathtrue+ in the style file.  Under this
setting, all text in every cell of the payoff matrix is automatically in math
mode.  (Note that even if the text in the cells consists only of
comma-separated pairs of numbers, it should still be in math mode, else the
spacing is not correct.)
\end{itemize}
\subsection{One optional argument: game label}
With one optional argument, the syntax of the \verb+game+ environment
is\vspace*{4mm}\newline
\verb+\begin{game}{+\emph{number-of-rows}%
\verb+}{+\emph{number-of-columns}\verb+}[+\emph{game-label}%
\verb+]+\emph{game-body}\newline\verb+\end{game}+\vspace*{4mm}\newline
where \emph{number-of-rows} is the number of rows in the game,
\emph{number-of-columns} is the number of columns, and \emph{game-label} is a
label for the game that is centered beneath the game.  (The sense in which it
is centered may be changed by setting \verb+\irpawcgltrue+ or
\verb+\irplwcgltrue+---see Section~\ref{s:param}.)  This case is useful if two
or more games are positioned side-by-side, and each requires a label.

The following example is illustrative.

\begin{verbatim}
\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{2}[$A$]
      & $L$    & $R$\\
$a$   &$2,2$   &$0,3$\\
$b$   &$3,0$   &$1,1$
\end{game}\hspace*{20mm}%
\begin{game}{2}{2}[$B$]
      & $L$    & $R$\\
$a$   &$2,3$   &$0,4$\\
$b$   &$3,1$   &$1,2$
\end{game}\hspace*{\fill}%
\caption[]{A three player strategic game, in which
player~3 chooses $A$ or $B$.}
\end{figure}
\end{verbatim}

\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{2}[$A$]
      & $L$    & $R$\\
$a$   &$2,2$   &$0,3$\\
$b$   &$3,0$   &$1,1$
\end{game}\hspace*{20mm}%
\begin{game}{2}{2}[$B$]
      & $L$    & $R$\\
$a$   &$2,3$   &$0,4$\\
$b$   &$3,1$   &$1,2$
\end{game}\hspace*{\fill}%
\caption[]{A three player strategic game, in which player~3 chooses $A$ or
$B$.}\label{f:ex2}
\end{figure}
\subsection{Two optional arguments: player labels}
In the case of two optional arguments, the syntax of the \verb+game+
environment is\vspace*{4mm}\newline
\verb+\begin{game}{+\emph{number-of-rows}%
\verb+}{+\emph{number-of-columns}\verb+}[+\emph{row-player-label}%
\verb+]+\newline
\verb+[+\emph{column-player-label}%
\verb+]+\emph{game-body}\verb+\end{game}+\vspace*{4mm}\newline
where \emph{number-of-rows} is the number of rows in the game,
\emph{number-of-columns} is the number of columns, and \emph{row-player-label}
and \emph{column-player-label} are the names of the players.

The following example illustrates this case.

\begin{verbatim}
\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{3}[Player~1][Player~2]
      & $L$    & $M$    & $R$\\
$T$   &$2,2$   &$2,0$   &$0,3$\\
$B$   &$3,0$   &$0,9$   &$1,1$
\end{game}\hspace*{\fill}%
\caption[]{A two row, three column strategic game with
player labels.}
\end{figure}
\end{verbatim}

\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{3}[Player~1][Player~2]
&$L$&$M$&$R$\\
$T$&$2,2$&$2,0$&$0,3$\\
$B$&$3,0$&$0,9$&$1,1$
\end{game}\hspace*{\fill}%
\caption[]{A two row, three column strategic game with player
labels.}\label{f:ex3}
\end{figure}
\subsection{Three optional arguments: player labels and game label}
The case of three optional arguments combines the two previous cases: the
syntax of the \verb+game+ environment is\vspace*{4mm}\newline
\verb+\begin{game}{+\emph{number-of-rows}%
\verb+}{+\emph{number-of-columns}\verb+}[+\emph{row-player-label}%
\verb+]+\newline
\verb+[+\emph{column-player-label}%
\verb+][+\emph{game-label}\verb+]+\emph{game-body}\verb+\end{game}+\vspace*{4m%
m}\newline
where \emph{number-of-rows} is the number of rows in the game,
\emph{number-of-columns} is the number of columns, \emph{row-player-label} and
\emph{column-player-label} are the names of the players, and \emph{game-label}
is a label for the game.

The following example illustrates this case.

\begin{verbatim}
\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{3}[Player~1][Player~2][$A$]
      & $L$    & $M$    & $R$\\
$T$   &$2,2$   &$2,0$   &$0,3$\\
$B$   &$3,0$   &$0,9$   &$1,1$
\end{game}\hspace*{\fill}%
\caption[]{A two row, three column strategic game with
player labels and a label.}
\end{figure}
\end{verbatim}

\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{3}[Player~1][Player~2][$A$]
&$L$&$M$&$R$\\
$T$&$2,2$&$2,0$&$0,3$\\
$B$&$3,0$&$0,9$&$1,1$
\end{game}\hspace*{\fill}%
\caption[]{A two row, three column strategic game with player labels and a
label.}\label{f:ex4}
\end{figure}
\section{Cell formatting}
You may regard the format of the payoffs in the first column of the game in
Figure~\ref{f:ex5} as unsatisfactory, and the formatting in the left panel of
Figure~\ref{f:ex6} as superior.  This latter formatting is implemented by
adding ``phantom'' minus signs before the ``2'' in the top left cell and
before the ``3'' in the bottom left cell.  The top left cell, for example, is
entered as \verb+$-2,\phantom{-}2$+.  (\verb+\phantom{<x>}+ leaves a space
whose size is equal to the size of the object \verb+<x>+.)
\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{2}
&$L$&$R$\\
$T$&$-2,2$&$0,3$\\
$B$&$3,-4$&$1,1$
\end{game}\hspace*{\fill}%
\caption[]{A strategic game.}\label{f:ex5}
\end{figure}

You may want to consider other formats.  The right panel of Figure~\ref{f:ex6}
shows an alternative.  This format is achieved be entering the element of each
cell as
\begin{quote}
\verb+\begin{array}{c}#1\\[1.6mm]#2\end{array}+,
\end{quote}
where \verb+#1+ is player~1's payoff and \verb+#2+ is player~2's payoff. 
(Actually, I defined the macro
\begin{quotation}
\verb+\def\stackedpayoffs#1#2{%+

\verb+  \begin{array}{c}#1\\[1.6mm]#2\end{array}+

\verb+}+
\end{quotation}
and entered each element as \verb+\stackedpayoffs{#1}{#2}+.  I set
\verb+\gamestretch+ to be 2.1 to improve the appearance of the game.)%
\def\stackedpayoffs#1#2{\begin{array}{c}#1\\[1.6mm]#2\end{array}}
\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{2}
&$L$&$R$\\
$T$&$-2,\phantom{-}2$&$0,3$\\
$B$&$\phantom{-}3,-4$&$1,1$
\end{game}\hspace*{\fill}%
\renewcommand{\gamestretch}{2.1}%
\begin{game}{2}{2}
&$L$&$R$\\
$T$&$\stackedpayoffs{-2}{2}$&$\stackedpayoffs{0}{3}$\\
$B$&$\stackedpayoffs{3}{-4}$&$\stackedpayoffs{1}{1}$
\end{game}\hspace*{\fill}%
\caption[]{Two alternative presentations of the game in
Figure~\protect\ref{f:ex5}.}\label{f:ex6}
\end{figure}

Figure~\ref{f:ex7} shows two more alternatives.  The one on the left is
achieved by entering the element of each cell as
\begin{quote}
\verb+\begin{array}{rl}#1&\\[1.6mm]&#2\end{array}+,
\end{quote}
and the one on the left differs only in that there is a \verb+\phantom{-}+ in
front of player~2's payoff in the top left cell, player~1's payoff in the
bottom left cell, and player~2's payoffs in both of the right cells.%
\def\binmorepayoffs#1#2{\begin{array}{rl}#1&\\[1.6mm]&#2\end{array}}
\begin{figure}[htb]\hspace*{\fill}%
\renewcommand{\gamestretch}{2.1}%
\begin{game}{2}{2}
&$L$&$R$\\
$T$&$\binmorepayoffs{-2}{2}$&$\binmorepayoffs{0}{3}$\\
$B$&$\binmorepayoffs{3}{-4}$&$\binmorepayoffs{1}{1}$
\end{game}\hspace*{\fill}%

\vspace*{4mm}

\hspace*{\fill}%
\begin{game}{2}{2}
&$L$&$R$\\
$T$&$\binmorepayoffs{-2}{\phantom{-}2}$& $\binmorepayoffs{0}{\phantom{-}3}$\\
$B$&$\binmorepayoffs{\phantom{-}3}{-4}$& $\binmorepayoffs{1}{\phantom{-}1}$
\end{game}\hspace*{\fill}%
\caption[]{Two additional presentations of the game in
Figure~\protect\ref{f:ex5}.}\label{f:ex7}
\end{figure}
\section{Vertical position of text within cells}\label{s:VertPos}
The vertical distance between the lines at the top and bottom of a row in
\LaTeX's \verb+tabular+ environment is equal to \verb+\arraystretch+ times
\verb+\baselineskip+.  The baseline of the text in each cell is placed 30\% of
the distance from the bottom line.  Thus if \verb+\arraystretch+ is large
relative to the height of the text, the text is not near the middle of the
row.  (See the left panel of Figure~\ref{f:VertPos}; or try setting
\verb+\arraystretch+ equal to 5 and process a document with a \verb+tabular+
environment.)  (See the definitions of \verb+\strutbox+ and of \verb+\@array+
in \verb+latex.ltx+.)

\verb+sgame.sty+ makes the vertical distance between the lines at the top and
bottom of a row of a \verb+game+ the same (\verb+\arraystretch+ times
\verb+\baselineskip+), but places the baseline of the text in each cell so
that the distances above and below an upper case I placed on the baseline are
equal.  (See the right panel of Figure~\ref{f:VertPos}.)  (If you'd like to
use another character, define this character to be \verb+\sg@alignchar+.)
\begin{figure}[htb]
\hspace*{\fill}
\begin{pspicture}(80,55)
\psline(0,5)(30,5)
\psline(0,55)(30,55)
\psline[linecolor=lightgray](0,20)(30,20)
\rput[B](15,20){\fontsize{14mm}{10mm}\selectfont AygIx}
\psline[linecolor=lightgray]{<->}(15,5)(15,20)
\rput*(15,12.5){$0.3d$}
\psline[linecolor=lightgray]{<->}(15,20)(15,55)
\rput*(15,37.5){$0.7d$}
\rput[B](15,0){\texttt{tabular} environment}
%
\psline[linecolor=lightgray]{<->}(40,5)(40,55)
\rput*(40,30){$d$}
%
\psline(50,5)(80,5)
\psline(50,55)(80,55)
\psline[linecolor=lightgray](50,25)(80,25)
\psline[linecolor=lightgray](50,35)(80,35)
\rput[B](65,25){\fontsize{14mm}{10mm}\selectfont AygIx}
\psline[linecolor=lightgray]{<->}(65,5)(65,25)
\rput*(65,15){$x$}
\psline[linecolor=lightgray]{<->}(65,35)(65,55)
\rput*(65,45){$x$}
\rput[B](65,0){\texttt{game} environment}
\end{pspicture}
\hspace*{\fill}
\caption[]{Vertical positioning of text within cells in the \texttt{tabular}
and \texttt{game} environments.}\label{f:VertPos}
\end{figure}

If you want to use \verb+sgame+'s vertical positioning in your \verb+tabular+
environments, as well as your \verb+game+ environments, set
\verb+\gamevaligntrue+ before the environment.  (If you want to switch back at
some point, you can do so by setting \verb+\gamevalignfalse+.)

If the height of the text in a cell exceeds \verb+\arraystretch+ times
\verb+\baselineskip+, then the height of the row is increased to accommodate 
the text (just as the distance between lines of text is increased to 
accommodate large text).  The result is that the lines at the top and bottom
of the row touch the top and bottom of the text, and this row is taller than
the others.  To improve the appearance of the table you may want to increase
\verb+\gamestretch+ (analogous to \verb+\arraystretch+), to put some white 
space above and below your oversized text.

When \verb+\gamestretch+ is increased from 1, the height of \emph{every} row, 
including the first row (the action labels), is increased.  If you don't want
to increase the space under the action labels, declare \verb+\ssualfalse+. 
(``ssual'' stands for ``stretch space under action labels''.)  See
Figures~\ref{f:ssual1} and \ref{f:ssual2} for examples (for games with and
without player labels).%
\begin{figure}[htb]\hspace*{\fill}\renewcommand{\gamestretch}{3}%
\begin{game}{2}{2}
&$L$&$R$\\
$T$&$2,1$&$2,2$\\
$B$&$3,3$&$4,4$
\end{game}%
\hspace*{\fill}%
\ssualfalse%
\begin{game}{2}{2}
&$L$&$R$\\
$T$&$2,1$&$2,2$\\
$B$&$3,3$&$4,4$
\end{game}%
\hspace*{\fill}%
\caption[]{Positioning the action labels of the column player when
\texttt{gamestretch} is larger than 1, for a game without player labels.  In
the left game we have \texttt{ssualtrue} (the default); in the right game
\texttt{ssualfalse} is declared.}\label{f:ssual2}
\end{figure}
\begin{figure}[htb]\hspace*{\fill}\renewcommand{\gamestretch}{3}%
\begin{game}{2}{2}[Player 1][Player 2]
&$L$&$R$\\
$T$&$2,1$&$2,2$\\
$B$&$3,3$&$4,4$
\end{game}%
\hspace*{\fill}%
\ssualfalse%
\begin{game}{2}{2}[Player 1][Player 2]
&$L$&$R$\\
$T$&$2,1$&$2,2$\\
$B$&$3,3$&$4,4$
\end{game}%
\hspace*{\fill}%
\caption[]{Positioning the action labels of the column player when
\texttt{gamestretch} is larger than 1, for a game with player labels.  In the
left game we have \texttt{ssualtrue} (the default); in the right game
\texttt{ssualfalse} is declared.}\label{f:ssual1}
\end{figure}
\section{Thick lines}
To change the width of the cell boundaries, set \verb+\arrayrulewidth+ (by
writing something like \verb+\arrayrulewidth2mm+ or, if you prefer,
\verb+\setlength{\arrayrulewidth}{2mm}+).  In addition, you may need to
stretch the game vertically to accommodate the thicker lines.  To do so, set
\verb+\gamestretch+ (by writing something like
\verb+\renewcommand{\gamestretch}{2}+).  Here's an example.

\begin{verbatim}
\def\sgtextcolor{blue}%
\def\sglinecolor{red}%
\renewcommand{\gamestretch}{2}
\arrayrulewidth2mm
\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{2}
      & $L$    & $R$\\
$T$   &$2,2$   &$100,35$\\
$B$   &$3,0$   &$350,16$
\end{game}\hspace*{\fill}%
\caption[]{Thick lines.}
\end{figure}
\end{verbatim}

\def\sgtextcolor{blue}%
\def\sglinecolor{red}%
\renewcommand{\gamestretch}{2}
\arrayrulewidth2mm
\begin{figure}[htb]\hspace*{\fill}%
\begin{game}{2}{2}
      & $L$    & $R$\\
$T$   &$2,2$   &$100,35$\\
$B$   &$3,0$   &$350,16$
\end{game}\hspace*{\fill}%
\caption[]{Thick lines.}
\end{figure}



\section{Parameters}\label{s:param}
\verb+\gamestretch+: a number that controls the spacing between rows,
analogous to \verb+\arraystretch+.  Possible values: positive real numbers. 
Default: value of \verb+\arraystretch+ when \verb+sgame.sty+ is loaded (1 in
standard styles).  Example: \verb+\renewcommand{\gamestretch}{2}+.

\vspace{4mm}

\noindent\verb+\sgcolsep+: horizontal padding within cells.  Possible values:
any dimension.  Default: value of \verb+\tabcolsep+ when \verb+sgame.sty+ is
loaded.  Example: \verb+\sgcolsep=10pt+.

\vspace{4mm}

\noindent\verb+\sglabelsep+: vertical space between game and label.  Possible
values:  any dimension.  Default: 5pt.  Example: \verb+\sglabelsep=10pt+.

\vspace{4mm}

\noindent\verb+\irpawcgltrue+ (``include row player actions when centering
game label''): causes game label to be centered under box consisting of row
player actions and payoff matrix.  Default: \verb+\irpawcglfalse+ (i.e.\ game
label is centered under box consisting only of payoff matrix).   Example:
\verb+\irpawcgltrue+.

\vspace{4mm}

\noindent\verb+\irplwcgltrue+ (``include row player label when centering game
label''): causes game label to be centered under box consisting of row player
label, row player actions, and payoff matrix.  (Is overridden by
\verb+\irpawcgltrue+.)  Default: \verb+\irplwcglfalse+ (i.e.\ game label is
centered under payoff matrix).   Example: \verb+\irplwcgltrue+.

\vspace{4mm}

\noindent\verb+\gamemathtrue+: put all text in cells in payoff matrix in math
mode.  Default: \verb+\gamemathfalse+.  Example: \verb+\gamemathtrue+.

\vspace{4mm}

\noindent\verb+\gamevaligntrue+: vertically align the text within cells of a
\verb+tabular+ environment as they are aligned in a \verb+game+ environment. 
Default: \verb+\gamevalignfalse+.  Example: \verb+\gamevaligntrue+.  (See
Section~\ref{s:VertPos}.)

\vspace{4mm}

\noindent\verb+\ssualtrue+: stretch space under action labels when
\verb+\gamestretch+ is greater than 1.  Default: \verb+\ssualtrue+.  Example:
\verb+\ssualfalse+.  (See Section~\ref{s:VertPos}.)

\vspace{4mm}

\noindent\verb+\sglinecolor+: color of lines around payoffs.  Default:
\verb+black+.  Example: \verb+\def\sglinecolor{lightgray}+.

\vspace{4mm}

\noindent\verb+\sgtextcolor+: color of text in game (action labels, payoffs,
player labels).  Default: \verb+black+.  Example:
\verb+\def\sgtextcolor{blue}+.
\section{History}
\subsection*{Version 1}
\begin{description}
\item[1993/12] First version.
\end{description}
\subsection*{Version 2}
\begin{description}
\item[1997-10] Labels allowed (via additional optional argument).  Parameters
added: \verb+\sglabelsep+, \verb+\sgcolsep+, \verb+\irplwcgltrue+,
\verb+\irpawcgltrue+, and \verb+\gamemathtrue+.
\item[2002-6 (version 2.1)] Modification of \LaTeX's \verb+\@array+ changed so
that it does not change spacing in \verb+tabular+ environment.  Color
functionality added.  Parameters added: \verb+\gamevaligntrue+,
\verb+\ssualtrue+ (allowing more control over vertical spacing when
\verb+\gamestretch+ is greater than 1), \verb+\sglinecolor+,
\verb+\sgtextcolor+.
\item[2004-5-13 (version 2.11)] Minor bug fix.
\item[2004-5-21 (version 2.12)] Minor bug fix.
\item[2005-7-8 (version 2.13)] Minor bug fix.
\item[2008-12-2 (version 2.14)] Minor bug fix: length of bottom line (bug
inherited from \verb+tabular+ environment; noticeable only when lines are
thick).  Bug pointed out by Marcel Priebsch.
\end{description}

}

\end{document}

