
     *File List*
-----RELEASE.---   --  -- --   --     -- 
nicefilelist.RLS  2012/12/12  v0.7a  [wrap], `mono' typo, vs.
-----PACKAGE.---   --  -- --   --     -- 
nicefilelist.sty  2012/12/12  v0.7a  more file list alignment (UL)
------DOCSRC.---   --  -- --   --     -- 
nicefilelist.tex  2012/12/12   --    documenting nicefilelist.sty
    srcfiles.tex  2012/10/30   --    file infos -> SrcFILEs.txt
--------DEMO.---   --  -- --   --     -- 
    provonly.fd    --  -- --   --    no date, no version, but a lot of info,
                                     look how that is wrapped!
       wrong.prv   * NOT FOUND *
       empty.f     * NOT FOUND *
--------USED.---   --  -- --   --     -- 
    hardwrap.sty  2011/02/12  v0.2   Hard wrap messages
    myfilist.sty  2012/11/22  v0.71  \listfiles -- mine only (UL)
    readprov.sty  2012/11/22  v0.5   file infos without loading (UL)
    fifinddo.sty  2012/11/17  v0.61  filtering TeX(t) files by TeX (UL)
     makedoc.sty  2012/08/28  v0.52  TeX input from *.sty (UL)
    niceverb.sty  2012/11/27  v0.51  minimize doc markup (UL)
    texlinks.sty  2012/12/08  v0.71  TeX-related links (UL)
     makedoc.cfg  2012/11/30   --    documentation settings
    mdoccorr.cfg  2012/11/13   --    `makedoc' local typographical corrections
-not-so-much.---   --  -- --   --     -- 
   kvsetkeys.sty  2009/07/30  v1.5   Key value parser with default handler
                                     support (HO)
     ***********

 List made at 2012/12/12, 23:52
 from script file srcfiles.tex

