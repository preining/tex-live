\documentclass[DIV10]{cnpkgdoc}
\docsetup{
  pkg = mychemistry,
  title = mychemistry -- examples,
  subtitle = Create Reaction Schemes with \LaTeXe\ and Chemfig ,
  code-box     = {
    skipbelow        = .5\baselineskip plus .5ex minus .5ex ,
    skipabove        = .5\baselineskip plus .5ex minus .5ex ,
    roundcorner      = 3pt ,
    innerleftmargin  = 1.5em ,
    innerrightmargin = 1.5em
  }
}

\addcmds{
  @addtoreset,
  anywhere,
  arrow,
  branch,
  celsius,
  ch,
  chemabove,
  chemand,
  chembelow,
  chemfig,
  chemname,
  cmpd,
  colorlet,
  command,
  draw,
  elmove,
  floatstyle,
  fpch,
  fplus,
  fscrm,
  fscrp,
  Hpl,
  iupac,
  lewis,
  Lewis,
  listof,
  makeinvisible,
  makevisible,
  marrow,
  mch,
  mCsetup,
  mech,
  mesomeric,
  N,
  node,
  para,
  pch,
  phantom,
  reactant,
  restylefloat,
  setarrowlabel,
  setarrowlength,
  setarrowline,
  setatomsep,
  setatomsize,
  setbondlength,
  setbondshape,
  setcrambond,
  setmergelength,
  setrcndist,
  setrxnalign,
  setschemealign,
  setschemename,
  SI,
  text,
  therxnscheme,
  tikzset,
  trans,
  transition
}

\chemsetup[option]{circled=all}

\usepackage{chemnum}
\usepackage{fnpct}
\usepackage{booktabs}
\usepackage{siunitx}
\usepackage{scrhack}

\TitlePicture{%
  \parbox{.7\linewidth}{%
     Since the documentation is already long enough I decided to provide an
     extra file containing only examples and a few words where to find possibly
     interesting code.\par
     For all the undocumented little macros like \cmd{fscrp} or \cmd{delm} have
     a look in the \paket*{chemmacros} documentation.
  }
}

\begin{document}

\section{Addition Reaction}\label{sec:addition}
A simple reaction scheme with two different products.

\begin{rxnscheme}{Addition Reaction}
 \reactant{ \chemfig{=_[::-30]-[::60](=[::60]O)-[::-60]} }
 \arrow{ $+ \Hpl$ }{}
 \mesomeric[,rf]{
   \reactant{ \chemfig{=_[:-30]-[::60](-[::60]OH)(-[::-120,.3,,,white]\fplus)-[::-60]} }
   \marrow[below]
   \reactant[below]{ \chemfig{\fplus-[6,.3,,,white]-[:-30]=_[::60](-[::60]OH)-[::-60]} }
 }
 \branch[right=of rf,,yshift=3em]{
   \arrow{}{}
   \reactant{ \chemname{\chemfig{=_[:-30]-[::60](-[::60]OH)(-[::-120]R)-[::-60]}}{1,2-adduct} }
 }
 \branch[right=of rf,,yshift=-5em]{
   \arrow{}{}
   \reactant{ \chemname{\chemfig{R-[6]-[:-30]=_[::60](-[::60]OH)-[::-60]}}{1,4-adduct} }
 }
\end{rxnscheme}


\begin{beispiel}[code only]
 \begin{rxnscheme}[,]{Addition Reaction}
  \reactant{ \chemfig{=_[::-30]-[::60](=[::60]O)-[::-60]} }
  \arrow{ $+ \Hpl$ }{}
  \mesomeric[,rf]{
    \reactant{ \chemfig{=_[:-30]-[::60](-[::60]OH)(-[::-120,.3,,,white]\fplus)-[::-60]} }
    \marrow[below]
    \reactant[below]{ \chemfig{\fplus-[6,.3,,,white]-[:-30]=_[::60](-[::60]OH)-[::-60]} }
  }
  \branch[right=of rf,,yshift=3em]{
    \arrow{}{}
    \reactant{ \chemname{\chemfig{=_[:-30]-[::60](-[::60]OH)(-[::-120]R)-[::-60]}}{1,2-adduct} }
  }
  \branch[right=of rf,,yshift=-5em]{
    \arrow{}{}
    \reactant{ \chemname{\chemfig{R-[6]-[:-30]=_[::60](-[::60]OH)-[::-60]}}{1,4-adduct} }
  }
 \end{rxnscheme}
\end{beispiel}

\section{Mesomerism}
\begin{rxnscheme}[,,,.8]{Mesomerism}
 \setatomsep{1.6em}
 \reactant[,start]{ \chemname{\chemfig{*6(-=-=(-[,,,,white]\phantom{Br})-=)}}{benzene \cmpd{benzene}} }
 \arrow[,,2.8]{}{}
 \reactant{ \chemname{\chemfig{*6(-=-=(-Br)-=)}}{bromobenzene \cmpd{bromobenzene}} }
 \arrow[start.below,,,pfeil_a]{\ch{Br2} / \ch{AlBr3}}{$-\ch{AlBr4-}$}
 \mesomeric[pfeil_a.below,mesomerism,xshift=8.5em,yshift=-2.5em]{
   \reactant{
     \chemfig{*6(=[@{e1}]-=-(-[:120]Br)(-[:60]H)-(-[:-30,.4,,,white]\fplus)-[@{e2}])}
     \elmove{e1}{60:4mm}{e2}{0:4mm}
   }
   \marrow
   \reactant{
     \chemfig{*6(-(-[:90,.4,,,white]\fplus)-[@{e4}]=[@{e3}]-(-[:120]Br)(-[:60]H)-=)}
     \elmove{e3}{180:4mm}{e4}{150:4mm}
   }
   \marrow
   \reactant{
     \chemfig{*6(-=-(-[:-150,.4,,,white]\fplus)-(-[:120]Br)(-[:60]H)-=)}
   }
 }
 \branch[above=of mesomerism,,xshift=7.5em]{
   \arrow[above]{$-\Hpl$}{}
 }
\end{rxnscheme}

If you put something relative to an arrow you might have to consider that the
arrow's anchor point is in the middle of the arrow. That's why \cmd{mesomeric}
is shifted with \code{yshift=-2.5em} in line 9.
\begin{beispiel}[code only]
 \begin{rxnscheme}[,,,.8]{Mesomerism}
  \setatomsep{1.6em}
  % main reaction:
  \reactant[,start]{ \chemname{\chemfig{*6(-=-=(-[,,,,white]\phantom{Br})-=)}}{benzene \cmpd{benzene}} }
  \arrow[,,2.8]{}{}
  \reactant{ \chemname{\chemfig{*6(-=-=(-Br)-=)}}{bromobenzene \cmpd{bromobenzene}} }
  % branch:
  \arrow[start.below,,,pfeil_a]{\ch{Br2} / \ch{AlBr3}}{$-\ch{AlBr4-}$}
  \mesomeric[!!pfeil_a.below!!,mesomerism,xshift=8.5em,!!yshift=-2.5em!!]{
    \reactant{
      \chemfig{*6(=[@{e1}]-=-(-[:120]Br)(-[:60]H)-(-[:-30,.4,,,white]\fplus)-[@{e2}])}
      \elmove{e1}{60:4mm}{e2}{0:4mm}
    }
    \marrow
    \reactant{
      \chemfig{*6(-(-[:90,.4,,,white]\fplus)-[@{e4}]=[@{e3}]-(-[:120]Br)(-[:60]H)-=)}
      \elmove{e3}{180:4mm}{e4}{150:4mm}
    }
    \marrow
    \reactant{
      \chemfig{*6(-=-(-[:-150,.4,,,white]\fplus)-(-[:120]Br)(-[:60]H)-=)}
    }
  }
  % last arrow inside a branch, since it cannot be shifted by itself:
  \branch[above=of mesomerism,,xshift=7.5em]{
    \arrow[above]{$-\Hpl$}{}
  }
 \end{rxnscheme}
\end{beispiel}

\section{The Former Titlepage}
This scheme used to be on the title page of the examples file. It isn't any more
but here's the scheme, anyway.
\begin{rxnscheme}[,,,.7]{The Titlepage}
  \setatomsep{1.5em}\footnotesize
  \reactant[,a]{ \chemfig{C(-[4]*6(=-=-=-))(-[2]*6(=-=-=-))(-[6,,,2]HO)-C(-[2]CH_3)(-[6]OH)-CH_3} }
    \arrow[a.20]{}{}
    \reactant[20]{ \chemfig{C(-[4]*6(=-=-=-))(-[2]*6(=-=-=-))(-[@{e1}6,,,2]H_2@{e2}\chembelow{O}{\fplus})-C(-[2]CH_3)(-[6]OH)-CH_3}\elmove{e1}{10:4mm}{e2}{-10:4mm} }
    \arrow[,,1.42]{$-\ch{H2O}$}{}
    \reactant{ \chemfig{\chembelow{C}{\fplus}(-[4]*6(=-=-=-))(-[2]*6(=-=-=-))-C(-[2]CH_3)(-[6]OH)-CH_3} }{}
    \arrow[a.-20,-|>]{}{}
    \reactant[-20]{ \chemfig{C(-[4]*6(=-=-=-))(-[2]*6(=-=-=-))(-[6,,,2]HO)-C(-[2]CH_3)(-[@{e3}6]@{e4}\chembelow{O}{\fplus}H_2)-CH_3}\elmove{e3}{170:4mm}{e4}{-170:4mm} }
    \arrow[,,1.42]{$-\ch{H2O}$}{}
    \reactant[,end]{ \chemfig{C(-[4]*6(=-=-=-))(-[2]*6(=-=-=-))(-[6,,,2]HO)-\chembelow{C}{\fplus}(-[2]CH_3)-CH_3} }
    \anywhere{below=of end}{}
\end{rxnscheme}

\begin{beispiel}[code only]
 \begin{rxn}[,.7]
  \setatomsep{1.5em}\footnotesize
  % reaction above:
  \reactant[,a]{ \chemfig{C(-[4]*6(=-=-=-))(-[2]*6(=-=-=-))(-[6,,,2]HO)-C(-[2]CH_3)(-[6]OH)-CH_3} }
  \arrow[a.45]{}{}
  \reactant[45]{ \chemfig{C(-[4]*6(=-=-=-))(-[2]*6(=-=-=-))(-[@{e1}6,,,2]H_2@{e2}\chembelow{O}{\fplus})-C(-[2]CH_3)(-[6]OH)-CH_3}\elmove{e1}{10:4mm}{e2}{-10:4mm} }
  \arrow[,,1.42]{$-\ch{H2O}$}{}
  \reactant{ \chemfig{\chembelow{C}{\fplus}(-[4]*6(=-=-=-))(-[2]*6(=-=-=-))-C(-[2]CH_3)(-[6]OH)-CH_3} }{}
  % going down:
  \arrow[a.-45,-|>]{}{}
  \reactant[-45]{ \chemfig{C(-[4]*6(=-=-=-))(-[2]*6(=-=-=-))(-[6,,,2]HO)-C(-[2]CH_3)(-[@{e3}6]@{e4}\chembelow{O}{\fplus}H_2)-CH_3}\elmove{e3}{170:4mm}{e4}{-170:4mm} }
  \arrow[,,1.42]{$-\ch{H2O}$}{}
  \reactant{ \chemfig{C(-[4]*6(=-=-=-))(-[2]*6(=-=-=-))(-[6,,,2]HO)-\chembelow{C}{\fplus}(-[2]CH_3)-CH_3} }
 \end{rxn}
\end{beispiel}

\section{Condensation Reaction}
\begin{rxnscheme}{Condensation Reaction}
 \reactant{\chemfig{**6(---(-CH_2OH)-(-OH)--)}}
 \chemand
 \reactant{\chemfig{**6(----(-OH)-(-HOCH_2)-)}}
 \arrow[,-+>]{}{\ch{H2O}}
 \reactant{\chemfig{**6(---(-CH_2-[:-30]O-[:30]CH_2-[:-30]**6(-----(-OH)-))-(-OH)--)}}
 \arrow[-90,-+>,,dec]{}{\ch{CH2O}}
 \anywhere{dec.180,,xshift=-.2em}{\SI{200}{\celsius}}
 \reactant[-90,target]{\chemfig{**6(---(-CH_2-[:-30]**6(-----(-OH)-))-(-OH)--)}}
 \branch[left=of target]{
   \reactant{\chemfig{**6(---(-CH_2OH)-(-OH)--)}}
   \chemand
   \reactant{\chemfig{**6(----(-OH)-(-H)-)}}
   \arrow[,-+>]{}{\ch{H2O}}
 }
\end{rxnscheme}

\begin{beispiel}[code only]
 \begin{rxnscheme}{Condensation Reaction}
  \reactant{\chemfig{**6(---(-CH_2OH)-(-OH)--)}}
  \chemand
  \reactant{\chemfig{**6(----(-OH)-(-HOCH_2)-)}}
  \arrow[,-+>]{}{\ch{H2O}}
  \reactant{\chemfig{**6(---(-CH_2-[:-30]O-[:30]CH_2-[:-30]**6(-----(-OH)-))-(-OH)--)}}
  \arrow[-90,-+>,,dec]{}{\ch{CH2O}}
  \anywhere{dec.180,,xshift=-.2em}{\SI{200}{\celsius}}
  \reactant[-90,target]{\chemfig{**6(---(-CH_2-[:-30]**6(-----(-OH)-))-(-OH)--)}}
  \branch[left=of target]{
    \reactant{\chemfig{**6(---(-CH_2OH)-(-OH)--)}}
    \chemand
    \reactant{\chemfig{**6(----(-OH)-(-H)-)}}
    \arrow[,-+>]{}{\ch{H2O}}
  }
 \end{rxnscheme}
\end{beispiel}

\section{Substitution vs. Elimination}
\begin{rxnscheme}{Substitution vs. Elimination}
 % first reaction:
 \reactant[,start_a]{\chemfig{@{H}H-[@{b1}:-60]\chemabove{C}{\scriptstyle\beta}(<[:-100]H)(<:[:-150]H)-[@{b2}]\chemabove{C}{\scriptstyle\alpha}(<[:20]H)(<:[:60]H)-[@{b3}:-60]@{X1}X}}
 \arrow{E2}{}
 \reactant{\chemfig{H-[:60]C(-[:120]H)=C(-[:60]H)-[:-60]H}}
 \chemand
 \reactant{\ch{X-}}
 \chemand
 \reactant{\chemfig{O(-[:60]H)-[:-60]H}}
 % second reaction:
 \reactant[start_a.-90,start_b,yshift=-4em]{\chemfig{H-[:-60]C(<[:-100]H)(<:[:-150]H)-@{C}C(<[:20]H)(<:[:60]H)-[@{b4}:-60]@{X2}X}}
 \arrow{S$_\text{N}$2}{}
 \reactant{\chemfig{H-[:-60]C(<[:-100]H)(<:[:-150]H)-C(<[:-80]H)(<:[:-30]H)-[:60]OH}}
 \chemand
 \reactant{\ch{X-}}
 % nucleophile/base:
 \anywhere{start_b.135,nuc,xshift=-3em,yshift=2em}{\chemfig{H-@{O}\chemabove{\lewis{026,O}}{\hspace{5mm}\fscrm}}}
 % electron movements:
 \anywhere{nuc.0}{
   \elmove{O}{90:1.5cm}{H}{180:1cm}
   \elmove{b1}{60:1cm}{b2}{90:5mm}
   \elmove{b3}{-170:5mm}{X1}{180:5mm}
   \elmove{O}{-90:1cm}{C}{100:1.5cm}
   \elmove{b4}{-170:5mm}{X2}{180:5mm}
 }
\end{rxnscheme}

You may see in line 20 that the \cmd{elmove} commands are put inside of
\cmd{anywhere}. This is necessary in order to produce the right scheme. But this
time you can position \cmd{anywhere} literally anywhere.
\begin{beispiel}[code only]
 \begin{rxnscheme}{Substitution vs. Elimination}
  % first reaction:
  \reactant[,start_a]{\chemfig{@{H}H-[@{b1}:-60]\chemabove{C}{\scriptstyle\beta}(<[:-100]H)(<:[:-150]H)-[@{b2}]\chemabove{C}{\scriptstyle\alpha}(<[:20]H)(<:[:60]H)-[@{b3}:-60]@{X1}X}}
  \arrow{\mech[e2}{}
  \reactant{\chemfig{H-[:60]C(-[:120]H)=C(-[:60]H)-[:-60]H}}
  \chemand
  \reactant{\ch{X-}}
  \chemand
  \reactant{\chemfig{O(-[:60]H)-[:-60]H}}
  % second reaction:
  \reactant[start_a.-90,start_b,yshift=-4em]{\chemfig{H-[:-60]C(<[:-100]H)(<:[:-150]H)-@{C}C(<[:20]H)(<:[:60]H)-[@{b4}:-60]@{X2}X}}
  \arrow{\mech[2]}{}
  \reactant{\chemfig{H-[:-60]C(<[:-100]H)(<:[:-150]H)-C(<[:-80]H)(<:[:-30]H)-[:60]OH}}
  \chemand
  \reactant{\ch{X-}}
  % nucleophile/base:
  \anywhere{start_b.135,nuc,xshift=-3em,yshift=2em}{\chemfig{H-@{O}\chemabove{\lewis{026,O}}{\hspace{5mm}\fscrm}}}
  % electron movements:
  \anywhere{nuc.0}{
    \elmove{O}{90:1.5cm}{H}{180:1cm}
    \elmove{b1}{60:1cm}{b2}{90:5mm}
    \elmove{b3}{-170:5mm}{X1}{180:5mm}
    \elmove{O}{-90:1cm}{C}{100:1.5cm}
    \elmove{b4}{-170:5mm}{X2}{180:5mm}
  }
 \end{rxnscheme}
\end{beispiel}

\section{Scheme with three Lines}\label{sec:three_lines}
\begin{rxnscheme}{Scheme with three Lines}
 \setatomsep{1.5em}
 \footnotesize
 \reactant[,start]{\chemfig{EtO-(=[2]O)-[:-60](-Br)-[:-120](=[6]O)-[4]EtO}}
 \chemand
 \reactant{\chemfig{*6(-=-=-*5(-(=O)-\chemabove{\lewis{4:,N}}{\fscrm}(-[4,.7,,,draw=none]\chemabove{K}{\fscrp})-(=O)--)=)}}
 \arrow{}{}
 \reactant{\chemfig{*6(-=-=-*5(-(=O)-N(-(-[::-60](=[::-60]O)-[::60]EtO)-[::60](=[::60]O)-[::-60]EtO)-(=O)--)=)}}
 % newline, started with \anywhere:
 \anywhere{start.-90,a,xshift=-4em,yshift=-5em}{}
 \arrow[a.0,,.6]{\chemabove{\lewis{0:,B}}{\fscrm}}{}
 \arrow{\ch{R-X}}{}
 \reactant{\chemfig{*6(-=-=-*5(-(=O)-N(-(-[4]R)(-[::-60](=[::-60]O)-[::60]EtO)-[::60](=[::60]O)-[::-60]EtO)-(=O)--)=)}}
 \arrow[,,1.25]{\Hpl/\ch{H2O}}{}
 \reactant{\chemfig{*6(-=-=-*5(-(=O)-N(-(-[4]R)(-[::-60](=[::-60]O)-[::60]HO_2C)-[::60](=[::60]O)-[::-60]HO_2C)-(=O)--)=)}}
 % newline, started with \anywhere:
 \anywhere{a.-90,b,yshift=-7em}{}
 \arrow[b.0]{$- {\ch{CO2}}$}{}
 \arrow{\Hpl}{\ch{H2O}}
 \reactant{\chemfig{R-(-[6]H)(-[2]C|O_2\mch)-NH_3\pch}}
\end{rxnscheme}
\begin{beispiel}[code only]
 \begin{rxnscheme}{Scheme with three Lines}
  \setatomsep{1.5em}
  \footnotesize
  \reactant[,start]{\chemfig{EtO-(=[2]O)-[:-60](-Br)-[:-120](=[6]O)-[4]EtO}}
  \chemand
  \reactant{\chemfig{*6(-=-=-*5(-(=O)-\chemabove{\lewis{4:,N}}{\fscrm}(-[4,.7,,,draw=none]\chemabove{K}{\fscrp})-(=O)--)=)}}
  \arrow{}{}
  \reactant{\chemfig{*6(-=-=-*5(-(=O)-N(-(-[::-60](=[::-60]O)-[::60]EtO)-[::60](=[::60]O)-[::-60]EtO)-(=O)--)=)}}
  % newline, started with \anywhere:
  \anywhere{start.-90,a,xshift=-4em,yshift=-5em}{}
  \arrow[a.0,,.6]{\chemabove{\lewis{0:,B}}{\fscrm}}{}
  \arrow{\ch{R-X}}{}
  \reactant{\chemfig{*6(-=-=-*5(-(=O)-N(-(-[4]R)(-[::-60](=[::-60]O)-[::60]EtO)-[::60](=[::60]O)-[::-60]EtO)-(=O)--)=)}}
  \arrow[,,1.25]{\Hpl/\ch{H2O}}{}
  \reactant{\chemfig{*6(-=-=-*5(-(=O)-N(-(-[4]R)(-[::-60](=[::-60]O)-[::60]HO_2C)-[::60](=[::60]O)-[::-60]HO_2C)-(=O)--)=)}}
  % newline, started with \anywhere:
  \anywhere{a.-90,b,yshift=-7em}{}
  \arrow[b.0]{$- {\ch{CO2}}$}{}
  \arrow{\Hpl}{\ch{H2O}}
  \reactant{\chemfig{R-(-[6]H)(-[2]C|O_2\mch)-NH_3\pch}}
 \end{rxnscheme}
\end{beispiel}


\section{Hydratisation}\label{sec:hydratisation}
\pgfdeclaredecoration{ddbond}{initial}{%
  \state{initial}[width=2pt]{%
    \pgfpathlineto{\pgfpoint{2pt}{0pt}}%
    \pgfpathmoveto{\pgfpoint{1.5pt}{2pt}}%
    \pgfpathlineto{\pgfpoint{2pt}{2pt}}%
    \pgfpathmoveto{\pgfpoint{2pt}{0pt}}%
  }%
  \state{final}{%
    \pgfpathlineto{\pgfpointdecoratedpathlast}%
  }%
}%
\tikzset{lddbond/.style={decorate,decoration=ddbond}}%
\tikzset{rddbond/.style={decorate,decoration={ddbond,mirror}}}%
A scheme with transition states.

\begin{rxnscheme}{Hydratisation}
 \reactant[,carbonyl_A]{\chemfig{R_2C=O}}
 \anywhere{above=of carbonyl_A}{\chemfig{H-[:-30]O-[:30]H}}
 \arrow[,<=>]{\tiny slow}{}
 \transition[,transition_A]{\chemfig{R_2C(-[2,,2,,densely dotted]\chemabove{O}{\delp}(-[:150]H)-[:30]H)-[:-30,1.15,,,lddbond]O-[6,,,,densely dotted]H-[,,,,densely dotted]\chemabove{A}{\delm}}}
 \anywhere{below=of transition_A,,text width=3cm}{(general transition state, acid cat.)}
 \arrow[,<=>,.7]{}{}
 \reactant{\chemfig{R_2C(-[:60]\chemabove{O}{\fscrp}H_2)-[:-60]OH}}
 \arrow[below right,<=>,.7]{$-\Hpl$}{}
 \reactant[below right]{\chemfig{R_2C(-[:60]OH)-[:-60]OH}}
 \arrow[below left,<=>,.7]{}{\ch{H2O}}
 \reactant[below left,zw]{\chemfig{R_2C(-[:60]OH)-[:-60]O|\mch}}
 \arrow[left,<=>,.7]{}{}
 \transition[left,transition_B]{\chemfig{R_2C(-[2,,2,,densely dotted]O(-[:150]H-[4,,,,densely dotted]\chemabove{B}{\delp})-[:30]H)-[:-30,1.15,,,lddbond]\chemabove{O}{\delm}-[6,,,,draw=none]\phantom{H}}}
 \anywhere{below=of transition_B,,text width=3cm}{(general transition state, base cat.)}
 \arrow[left,<=>]{}{\tiny slow}
 \reactant[left,carbonyl_B]{\chemfig{R_2C=O}}
 \anywhere{above=of carbonyl_B}{\chemfig{H-[:-30]O-[:30]H}}
\end{rxnscheme}

For this example we first declare a style for the delocalized double bonds:
\begin{beispiel}[code only]
 \pgfdeclaredecoration{ddbond}{initial}{%
   \state{initial}[width=2pt]{%
     \pgfpathlineto{\pgfpoint{2pt}{0pt}}%
     \pgfpathmoveto{\pgfpoint{1.5pt}{2pt}}%
     \pgfpathlineto{\pgfpoint{2pt}{2pt}}%
     \pgfpathmoveto{\pgfpoint{2pt}{0pt}}%
   }%
   \state{final}{%
     \pgfpathlineto{\pgfpointdecoratedpathlast}%
   }%
 }%
 \tikzset{lddbond/.style={decorate,decoration=ddbond}}%
 \tikzset{rddbond/.style={decorate,decoration={ddbond,mirror}}}%
\end{beispiel}
Now the delocalized double bond can be used via \paket*{chemfig}'s fifth option
(see the \paket{chemfig} manual):
\begin{beispiel}[code only]
 \chemfig{-[,,,,lddbond]-[,,,,rddbond]}
\end{beispiel}
\chemfig{-[,,,,lddbond]-[,,,,rddbond]}\par\medskip

Then the whole code looks like follows:
\begin{beispiel}[code only]
 \begin{rxnscheme}{Hydratisation}
  \reactant[,carbonyl_A]{\chemfig{R_2C=O}}
  \anywhere{above=of carbonyl_A}{\chemfig{H-[:-30]O-[:30]H}}
  \arrow[,<=>]{\tiny slow}{}
  \transition[,transition_A]{\chemfig{R_2C(-[2,,2,,densely dotted]\chemabove{O}{\delp}(-[:150]H)-[:30]H)-[:-30,1.15,,,lddbond]O-[6,,,,densely dotted]H-[,,,,densely dotted]\chemabove{A}{\delm}}}
  \anywhere{below=of transition_A,,text width=3cm}{(general transition state, acid cat.)}
  \arrow[,<=>,.7]{}{}
  \reactant{\chemfig{R_2C(-[:60]\chemabove{O}{\fscrp}H_2)-[:-60]OH}}
  \arrow[below right,<=>,.7]{$-\Hpl$}{}
  \reactant[below right]{\chemfig{R_2C(-[:60]OH)-[:-60]OH}}
  \arrow[below left,<=>,.7]{}{\ch{H2O}}
  \reactant[below left,zw]{\chemfig{R_2C(-[:60]OH)-[:-60]O|\mch}}
  \arrow[left,<=>,.7]{}{}
  \transition[left,transition_B]{\chemfig{R_2C(-[2,,2,,densely dotted]O(-[:150]H-[4,,,,densely dotted]\chemabove{B}{\delp})-[:30]H)-[:-30,1.15,,,lddbond]\chemabove{O}{\delm}-[6,,,,draw=none]\phantom{H}}}
  \anywhere{below=of transition_B,,text width=3cm}{(general transition state, base cat.)}
  \arrow[left,<=>]{\tiny langsam}{}
  \reactant[left,carbonyl_B]{\chemfig{R_2C=O}}
  \anywhere{above=of carbonyl_B}{\chemfig{H-[:-30]O-[:30]H}}
 \end{rxnscheme}
\end{beispiel}
You can see that \cmd{anywhere} was used several times, either to place molecules
or to label molecules.

\section{Esterification}\label{sec:veresterung}
\begin{rxnscheme}{Esterification}
 \reactant{\chemfig{H-C(=[:60]O)-[:-60]O-H}}
 \arrow[,-+>,1.5,protolysis]{\ch{H2SO4}}{\ch{HSO4-}}
 \anywhere{below=of protolysis,,yshift=1em}{\tiny protolysis}
 \mesomeric{
   \reactant{\chemfig{H-@{a2}C(-[:60]O-H)(-[:30,.5,,,draw=none]{\fscrp})-[:-60]O-H}}
   \marrow
   \reactant{\chemfig{H-C(=[:60]\chemabove{O}{\fscrp}-H)-[:-60]O-H}}
 }
 \branch[below,,xshift=-5em]{
   \arrow[below,<=>]{\chemfig{H-[:120]@{a1}O-[:60]CH_3}}{\tiny addition}
   \reactant[below]{\chemfig{H-C(-[2]O-[:30]H)(-\chemabove{O}{\fscrp}(-[:60]CH_3)-[:-60]H)-[6]O-[:-30]H}\elmove{a1}{90:1.5cm}{a2}{0:3cm}}
 }
 \branch[left,,yshift=-3.5em]{
   \arrow[left,<=>]{}{\tiny protolysis}
 }
 \reactant[left]{\chemfig{H-C(-[2]O-[:30]H)(-O-CH_3)-[@{b1}6]@{a3}\chemabove{O}{\hspace*{-4mm}\fscrp}(-[:-150]H)-[:-30]H}\elmove{b1}{0:5mm}{a3}{20:5mm}}
 \arrow[below,<=>]{$- {\ch{H2O}}$}{\tiny elimination}
 \mesomeric[below,,xshift=6em]{
   \reactant{\chemfig{H-C(-[:60]O-H)(-[,.5,,,draw=none]{\fscrp})-[:-60]O-CH_3}}
   \marrow
   \reactant{\chemfig{H-C(=[:60]\chemabove{O}{\fscrp}-H)-[:-60]O-CH_3}}
 }
 \arrow[,-+>,1.5]{\ch{HSO4-}}{\ch{H2SO4}}
 \reactant{\chemfig{H-C(=[:60]O)-[:-60]O-CH_3}}
\end{rxnscheme}

\begin{beispiel}[code only]
 \begin{rxn}{Esterification}
  \reactant{\chemfig{H-C(=[:60]O)-[:-60]O-H}}
  \arrow[,-+>,1.2,protolysis]{\ch{H2SO4}}{\ch{HSO4-}}
  \anywhere{below=of protolysis,,yshift=1em}{\tiny protolysis}
  \mesomeric{
    \reactant{\chemfig{H-@{a2}C(-[:60]O-H)(-[:30,.5,,,draw=none]{\fscrp})-[:-60]O-H}}
    \marrow
    \reactant{\chemfig{H-C(=[:60]\chemabove{O}{\fscrp}-H)-[:-60]O-H}}
  }
  \branch[below,,xshift=-5em]{
    \arrow[below,<=>]{\tiny addition}{\chemfig{H-[:120]@{a1}O-[:60]CH_3}}
    \reactant[below]{
      \chemfig{H-C(-[2]O-[:30]H)(-\chemabove{O}{\fscrp}(-[:60]CH_3)-[:-60]H)-[6]O-[:-30]H}
      \elmove{a1}{90:1.5cm}{a2}{0:3cm}
    }
  }
  \branch[left,,yshift=-3.5em]{
    \arrow[left,<=>]{}{\tiny protolysis}
  }
  \reactant[left]{
    \chemfig{H-C(-[2]O-[:30]H)(-O-CH_3)-[@{b1}6]@{a3}\chemabove{O}{\hspace*{-4mm}\fscrp}(-[:-150]H)-[:-30]H}
    \elmove{b1}{0:5mm}{a3}{20:5mm}
  }
  \arrow[below,<=>]{$- {\ch{H2O}}$}{\tiny elimination}
  \mesomeric[below,,xshift=6em]{
    \reactant{\chemfig{H-C(-[:60]O-H)(-[,.5,,,draw=none]{\fscrp})-[:-60]O-CH_3}}
    \marrow
    \reactant{\chemfig{H-C(=[:60]\chemabove{O}{\fscrp}-H)-[:-60]O-CH_3}}
  }
  \arrow[,-+>,1.2]{\ch{HSO4-}}{\ch{H2SO4}}
  \reactant{\chemfig{H-C(=[:60]O)-[:-60]O-CH_3}}
 \end{rxnscheme}
\end{beispiel}

\section{Electrophilic Addition}\label{sec:elektrophile_addition}
This scheme forms a circle.
\begin{rxnscheme}{Electrophilic Addition}
 \setarrowlength{3em}
 \reactant{\chemfig{>[:-20]C(<[:40])=[6]C(<[:-130])<[:-20]}}
 \chemand[,plus]
 \reactant{\chemfig{\lewis{246,Br}-\lewis{026,Br}}}
 \arrow[plus.-90,<=>]{\footnotesize fast}{}
 \reactant[-90,attack]{\chemfig{>[:-20]C(<[:40])=[@{db}6]C(<[:-130])<[:-20]}}
 \anywhere{right=of attack}{
   \chemfig{@{Br1}\lewis{246,Br}-[@{b2}]@{Br2}\lewis{026,Br}}
   \elmove{db}{20:5mm}{Br1}{135:5mm}
   \elmove{b2}{-120:5mm}{Br2}{-120:5mm}
 }
 % to the left:
 \arrow[attack.-135,<=>,2]{$- {\ch{Br-}}$}{\footnotesize slow}
 \reactant[-135,carbenium_a]{\vflipnext\chemfig{-[:-30]\chembelow{C}{\fscrp}(-[:30])-[6]C(<[:-150])(<:[:-100])-[:-30]\lewis{137,Br}}}
 \anywhere{below=of carbenium_a}{\footnotesize carbenium ion}
 \arrow[,<<=>]{}{}
 \reactant[,bromonium]{\chemfig{>:[:-60]C?(<[:160])-[6]C(<[:-110])(<:[:-150])-[:30]\lewis{17,Br}?-[4,.5,,,draw=none]{\fscrp}}}
 \anywhere{below=of bromonium,,yshift=.35em}{\footnotesize bromonium ion}
 % to the right:
 \arrow[attack.-45,<=>,2]{\footnotesize slow}{$- {\ch{Br-}}$}
 \reactant[-45,carbenium_b]{\chemfig{-[:-30]\chemabove{C}{\fscrp}(-[:30])-[6]C(<:[:-150])(<[:-100])-[:-30]\lewis{157,Br}}}
 \anywhere{below=of carbenium_b}{\footnotesize carbenium ion}
 \arrow[left,<<=>]{}{}
 \mCsetup{reset}
\end{rxnscheme}

\begin{beispiel}[code only]
 \begin{rxnscheme}{Electrophilic Addition}
  \setarrowlength{3em}
  \reactant{\chemfig{>[:-20]C(<[:40])=[6]C(<[:-130])<[:-20]}}
  \chemand[,plus]
  \reactant{\chemfig{\lewis{246,Br}-\lewis{026,Br}}}
  \arrow[plus.-90,<=>]{\footnotesize fast}{}
  \reactant[-90,attack]{\chemfig{>[:-20]C(<[:40])=[@{db}6]C(<[:-130])<[:-20]}}
  \anywhere{right=of attack}{
    \chemfig{@{Br1}\lewis{246,Br}-[@{b2}]@{Br2}\lewis{026,Br}}
    \elmove{db}{20:5mm}{Br1}{135:5mm}
    \elmove{b2}{-120:5mm}{Br2}{-120:5mm}
  }
  % to the left:
  \arrow[attack.-135,<=>,2]{$- {\ch{Br-}}$}{\footnotesize slow}
  \reactant[-135,carbenium_a]{\vflipnext\chemfig{-[:-30]\chembelow{C}{\fscrp}(-[:30])-[6]C(<[:-150])(<:[:-100])-[:-30]\lewis{137,Br}}}
  \anywhere{below=of carbenium_a}{\footnotesize carbenium ion}
  \arrow[,<<=>]{}{}
  \reactant[,bromonium]{\chemfig{>:[:-60]C?(<[:160])-[6]C(<[:-110])(<:[:-150])-[:30]\lewis{17,Br}?-[4,.5,,,draw=none]{\fscrp}}}
  \anywhere{below=of bromonium,,yshift=.35em}{\footnotesize bromonium ion}
  % to the right:
  \arrow[attack.-45,<=>,2]{\footnotesize slow}{$- {\ch{Br-}}$}
  \reactant[-45,carbenium_b]{\chemfig{-[:-30]\chemabove{C}{\fscrp}(-[:30])-[6]C(<:[:-150])(<[:-100])-[:-30]\lewis{157,Br}}}
  \anywhere{below=of carbenium_b}{\footnotesize carbenium ion}
  \arrow[left,<<=>]{}{}
  \mCsetup{reset}
 \end{rxnscheme}
\end{beispiel}

\section{Activation of Fatty Acids}\label{sec:activation_fatty_acid}
\begin{rxnscheme}{Activation of Fatty Acids}
 \reactant[,ATP]{\chemfig{\chemabove{O}{\hspace*{-5mm}\fscrm}-P(=[2]O)(-[6]\chembelow{O}{\fscrm})-O-P(=[2]O)(-[6]\chembelow{O}{\fscrm})-@{O1}O-[@{b1}]@{P}P(=[2]O)(-[6]\chembelow{O}{\fscrm})-O-CH_2-[6,1.5,1](-[6,.5])(-[:20,1.3]O?[a])<[7](-[2,.5])(-[6]OH)-[,,,,line width=3pt](-[2,.5])(-[6]OH)>[1]?[a](-[6,.5])-[2,1.5]N?[b]-[:18]([:30]*6(-N=-N=(-NH_2)-=))-[:90]-[:162]N=^[:-126]?[b]}}
 \anywhere{below right=of ATP,,xshift=-4em,yshift=3em}{\bfseries ATP}
 \arrow[below,,1.5]{}{\chemname{\chemfig{R-C(=[:-60]O)-[:60]@{O2}O-[@{b2}]H}}{fatty acid}}
 \branch[on chain=going below]{
   \reactant[,pyrophosphat]{
     \chemfig{\chemabove{O}{\hspace*{-5mm}\fscrm}-P(=[2]O)(-[6]\chembelow{O}{\fscrm})-O-P(=[2]O)(-[6]\chembelow{O}{\fscrm})-\chemabove{O}{\hspace*{5mm}\fscrm}}
     \elmove{b1}{100:1cm}{O1}{90:5mm}
     \elmove{O2}{135:1cm}{P}{-135:1cm}
     \elmove{b2}{-90:5mm}{O2}{-60:5mm}
     }
   \anywhere{below=of pyrophosphat}{pyrophosphate PP$_\text{i}$}
   \chemand
   \reactant[,acyl-amp]{\chemfig{R-@{C}C(=[:-60]O)-[@{b3}:60]@{O3}O-P(-[6]\chembelow{O}{\fscrm})(=[2]O)-O-CH_2-[6,,1,1]r|ibos|e-[2,1.05,3,1]a|denine}}
   \anywhere{below=of acyl-amp}{\bfseries acyl-AMP}
 }
 \branch[on chain=going below,,xshift=-8em]{
   \arrow[below]{\ch{H2O}}{}
   \reactant[below,Pi]{2~\chemfig{HO-P(=[2]O)(-[6]\chembelow{O}{\fscrm})-O}}
   \anywhere{below right=of Pi}{P$_\text{i}$}
 }
 \branch[,,xshift=4em]{
   \arrow[below,-+>]{\chemfig{CoA-@{S}S-[@{b4}:-60]H}}{AMP}
   \reactant[below,acyl-SCoA]{
     \chemfig{R-C(=[:-60]O)-[:60]S-CoA}
     \elmove{S}{45:3cm}{C}{-120:2cm}
     \elmove{b3}{135:5mm}{O3}{110:7mm}
     \elmove{b4}{-120:7mm}{S}{-100:5mm}
   }
   \anywhere{below=of acyl-SCoA}{acyl-SCoA}
 }
\end{rxnscheme}

\begin{beispiel}[code only]
 \begin{rxnscheme}{Activation of Fatty Acids}
  \reactant[,ATP]{\chemfig{\chemabove{O}{\hspace*{-5mm}\fscrm}-P(=[2]O)(-[6]\chembelow{O}{\fscrm})-O-P(=[2]O)(-[6]\chembelow{O}{\fscrm})-@{O1}O-[@{b1}]@{P}P(=[2]O)(-[6]\chembelow{O}{\fscrm})-O-CH_2-[6,1.5,1](-[6,.5])(-[:20,1.3]O?[a])<[7](-[2,.5])(-[6]OH)-[,,,,line width=3pt](-[2,.5])(-[6]OH)>[1]?[a](-[6,.5])-[2,1.5]N?[b]-[:18]([:30]*6(-N=-N=(-NH_2)-=))-[:90]-[:162]N=^[:-126]?[b]}}
  \anywhere{below right=of ATP,,xshift=-4em,yshift=3em}{\bfseries ATP}
  \arrow[below,,1.5]{\chemname{\chemfig{R-C(=[:-60]O)-[:60]@{O2}O-[@{b2}]H}}{fatty acid}}{}
  \branch[on chain=going below]{
    \reactant[,pyrophosphat]{
      \chemfig{\chemabove{O}{\hspace*{-5mm}\fscrm}-P(=[2]O)(-[6]\chembelow{O}{\fscrm})-O-P(=[2]O)(-[6]\chembelow{O}{\fscrm})-\chemabove{O}{\hspace*{5mm}\fscrm}}
      \elmove{b1}{100:1cm}{O1}{90:5mm}
      \elmove{O2}{135:1cm}{P}{-135:1cm}
      \elmove{b2}{-90:5mm}{O2}{-60:5mm}
      }
    \anywhere{below=of pyrophosphat}{pyrophosphate PP$_\text{i}$}
    \chemand
    \reactant[,acyl-amp]{\chemfig{R-@{C}C(=[:-60]O)-[@{b3}:60]@{O3}O-P(-[6]\chembelow{O}{\fscrm})(=[2]O)-O-CH_2-[6,,1,1]r|ibos|e-[2,1.05,3,1]A|denine}}
    \anywhere{below=of acyl-amp}{\bfseries acyl-AMP}
  }
  \branch[on chain=going below,,xshift=-8em]{
    \arrow[below]{\ch{H2O}}{}
    \reactant[below,Pi]{2~\chemfig{HO-P(=[2]O)(-[6]\chembelow{O}{\fscrm})-O}}
    \anywhere{below right=of Pi}{P$_\text{i}$}
  }
  \branch[,,xshift=4em]{
    \arrow[below,-+>]{\chemfig{CoA-@{S}S-[@{b4}:-60]H}}{AMP}
    \reactant[below,acyl-SCoA]{
      \chemfig{R-C(=[:-60]O)-[:60]S-CoA}
      \elmove{S}{135:2cm}{C}{-135:1cm}
      \elmove{b3}{-45:5mm}{O3}{-70:7mm}
      \elmove{b4}{-120:7mm}{S}{-100:5mm}
    }
    \anywhere{below=of acyl-SCoA}{acyl-SCoA}
  }
 \end{rxnscheme}
\end{beispiel}

\section{Change the layout with \TikZ}\label{sec:tikz_layout}
\begin{rxnscheme}{Change the layout with \TikZ}
  \colorlet{mCgreen}{green!50!gray}
  \colorlet{mCblue}{cyan!50!gray}
  \colorlet{mCred}{magenta!50!gray}
  \tikzset{reactant/.style={draw=#1,fill=#1!10,minimum width=.8\textwidth,inner sep=1em,rounded corners}}
  \mCsetup{arrowlength=3em,arrowline=very thick}
  \reactant[,,reactant=mCgreen]{
    \chemname{\chemfig{Alky|l--[6](-[4,,,2]Acy|l)-[6]-O-P(=[2]O)(-[6]O|\mch)-O-[:-30]-[:30]-[:-30]NH_2}}{\bfseries Phosphatidylethanolamine}
  }
  \arrow[below]{\iupac{\N\-acyl\|transferase}}{}
  \reactant[below,,reactant=mCblue]{
    \chemname{\chemfig{Alky|l--[6](-[4,,,2]Acy|l)-[6]-O-P(=[2]O)(-[6]O|\mch)-O-[:-30]-[:30]-[:-30]\chembelow{N}{H}-[:30](=[2]O)-[:-30]-[:30]-[:-30]-[:30]=_-[:-30]-[:30]=_-[:-60]-[::-60]=_[:180]-[::-30]-[::60]=_[:180]-[::-30]-[::60]-[::-60]-[::60]-[6]}}{\bfseries\iupac{\N\-arachidonoyl\-PE}}
  }
  \arrow[below]{Phospholipase D}{}
  \reactant[below,,reactant=mCred]{
    \chemname{\chemfig{HO-[:-30]-[:30]-[:-30]\chembelow{N}{H}-[:30](=[2]O)-[:-30]-[:30]-[:-30]-[:30]=_-[:-30]-[:30]=_-[:-60]-[::-60]=_[:180]-[::-30]-[::60]=_[:180]-[::-30]-[::60]-[::-60]-[::60]-[6]}}{\bfseries Anandamide}
  }
  \mCsetup{reset}
\end{rxnscheme}

This is an example for the usage of the \code{<tikz>} option. Please take a closer
look at lines \numlist{5;7;11;15}.
\begin{beispiel}[code only]
 \begin{rxnscheme}{Change the layout with \TikZ}
  \colorlet{mCgreen}{green!50!gray}
  \colorlet{mCblue}{cyan!50!gray}
  \colorlet{mCred}{magenta!50!gray}
  !!\tikzset{reactant/.style={draw=#1,fill=#1!10,minimum width=.8\textwidth,inner sep=1em,rounded corners}}!!
  \mCsetup{arrowlength=3em,arrowline=very thick}
  \reactant[,,!!reactant=mCgreen!!]{
    \chemname{\chemfig{Alky|l--[6](-[4,,,2]Acy|l)-[6]-O-P(=[2]O)(-[6]O|\mch)-O-[:-30]-[:30]-[:-30]NH_2}}{\bfseries Phosphatidylethanolamine}
  }
  \arrow[below]{}{\iupac{\N-acyl\|transferase}}
  \reactant[below,,!!reactant=mCblue!!]{
    \chemname{\chemfig{Alky|l--[6](-[4,,,2]Acy|l)-[6]-O-P(=[2]O)(-[6]O|\mch)-O-[:-30]-[:30]-[:-30]\chembelow{N}{H}-[:30](=[2]O)-[:-30]-[:30]-[:-30]-[:30]=_-[:-30]-[:30]=_-[:-60] -[::-60]=_[:180]-[::-30]-[::60]=_[:180]-[::-30]- [::60]-[::-60]-[::60]-[6]}}{\bfseries\iupac{\N\-arachidonoyl\-PE}}
  }
  \arrow[below]{}{Phospholipase D}
  \reactant[below,,!!reactant=mCred!!]{
    \chemname{\chemfig{HO-[:-30]-[:30]-[:-30]\chembelow{N}{H}-[:30](=[2]O)-[:-30]-[:30]-[:-30]-[:30]=_-[:-30]-[:30]=_-[:-60] -[::-60]=_[:180]-[::-30]-[::60]=_[:180]-[::-30]- [::60]-[::-60]-[::60]-[6]}}{\bfseries Anandamide}
  }
  \mCsetup{reset}
 \end{rxnscheme}
\end{beispiel}

\section{Claisen-Kondensation}
\begin{rxnscheme}{Claisen-Kondensation}
 \colorlet{mCred}{red!50!gray}
 \setatomsep{1.5em}
 % Ergebnis:
 \branch[,one,draw=mCred,fill=mCred!10,rounded corners,inner sep=.5em]{
   \reactant{\chemfig{[:30]-(=[2]O)-[:-30]O--[:-30]}}
   \chemand
   \reactant{\chemfig{[:30]-(=[2]O)-[:-30]O--[:-30]}}
   \arrow[,,2]{\ch{NaOEt}, \ch{EtOH}}{}
   \reactant{\chemfig{[:30]-(=[2]O)-[:-30]-(=[2]O)-[:-30]O--[:-30]}}
 }
 % Mechanismus:
 \branch[-90,,xshift=-13.5em]{
   \arrow[-90,<<=>]{\ch{^-OEt}}{}
 }
 \mesomeric[-90,two,xshift=4.5em]{
   \reactant{\chemfig{[:30](-[:150,.3,,,draw=none]@{C1}\fscrm)-(=[2]O)-[:-30]O--[:-30]}}
   \marrow
   \reactant{\chemfig{[:30]=(-[2]O|\mch)-[:-30]O--[:-30]}}
 }
 \chemand
 \reactant{\ch{EtOH}}
 \branch[two.-90,three,xshift=-5.5em]{
   \arrow[-90,<=>,,,both]{\chemfig[][scale=.7]{[:30]-@{C2}(=[@{b1}2]O@{O1})-[:-30]O--[:-30]}}{}
 }
 \reactant[three.-90]{\chemfig{-(-[@{b2}2]@{O2}O|\mch)(-[6]-[:-30](=[6]O)-[:30]O-[:-30]-[:30])-[@{b3}]@{O3}O-[:30]-[:-30]}}
 \arrow[,<=>]{}{}
 \reactant[,four]{\chemfig{[:30]-(=[2]O)-[:-30]@{C3}(-[:-120]H)(-[@{b4}:-60]H@{H})-(=[2]O)-[:-30]O--[:-30]}}
 \chemand
 \reactant{\chemfig{\mch @{O4}OEt}}
 \arrow[four.-90]{}{}
 \mesomeric[-90]{
   \reactant{\chemfig{[:30]-(=[2]O)-[:-30]=(-[2]O|\mch)-[:-30]O--[:-30]}}
   \marrow
   \reactant{\chemfig{[:30]-(=[2]O)-[:-30](-[6,.3,,,draw=none]\fscrm)-(=[2]O)-[:-30]O--[:-30]}}
   \marrow
   \reactant{\chemfig{[:30]-(-[2]O|\mch)=[:-30]-(=[2]O)-[:-30]O--[:-30]}}
 }
 \arrow[-90]{\Hpl, \ch{H2O}}{}
 \reactant[-90]{\chemfig{[:30]-(=[2]O)-[:-30]-(=[2]O)-[:-30]O--[:-30]}}
 \anywhere{one.0}{
   \elmove{C1}{-100:2cm}{C2}{-90:2cm}
   \elmove{b1}{10:5mm}{O1}{0:5mm}
   \elmove{O2}{180:5mm}{b2}{180:5mm}
   \elmove{b3}{80:5mm}{O3}{90:5mm}
   \elmove{b4}{0:5mm}{C3}{0:7mm}
   \elmove{O4}{-90:1cm}{H}{-45:1cm}
 }
\end{rxnscheme}

\begin{beispiel}[code only]
 \begin{rxnscheme}{Claisen-Kondensation}
  \colorlet{mCred}{red!50!gray}
  \setatomsep{1.5em}
  % Ergebnis:
  \branch[,one,draw=mCred,fill=mCred!10,rounded corners,inner sep=.5em]{
    \reactant{\chemfig{[:30]-(=[2]O)-[:-30]O--[:-30]}}
    \chemand
    \reactant{\chemfig{[:30]-(=[2]O)-[:-30]O--[:-30]}}
    \arrow[,,2]{\ch{NaOEt}, \ch{EtOH}}{}
    \reactant{\chemfig{[:30]-(=[2]O)-[:-30]-(=[2]O)-[:-30]O--[:-30]}}
  }
  % Mechanismus:
  \branch[-90,,xshift=-13.5em]{
    \arrow[-90,<=>]{\ch{^-OEt}}{}
  }
  \mesomeric[-90,two,xshift=4.5em]{
    \reactant{\chemfig{[:30](-[:150,.3,,,draw=none]@{C1}\fscrm)-(=[2]O)-[:-30]O--[:-30]}}
    \marrow
    \reactant{\chemfig{[:30]=(-[2]O|\mch)-[:-30]O--[:-30]}}
  }
  \chemand
  \reactant{\ch{EtOH}}
  \branch[two.-90,three,xshift=-5.5em]{
    \arrow[-90,<=>,,,both]{\chemfig[][scale=.7]{[:30]-@{C2}(=[@{b1}2]O@{O1})-[:-30]O--[:-30]}}{}
  }
  \reactant[three.-90]{\chemfig{-(-[@{b2}2]@{O2}O|\mch)(-[6]-[:-30](=[6]O)-[:30]O-[:-30]-[:30])-[@{b3}]@{O3}O-[:30]-[:-30]}}
  \arrow[,<=>]{}{}
  \reactant[,four]{\chemfig{[:30]-(=[2]O)-[:-30]@{C3}(-[:-120]H)(-[@{b4}:-60]H@{H})-(=[2]O)-[:-30]O--[:-30]}}
  \chemand
  \reactant{\chemfig{\mch @{O4}OEt}}
  \arrow[four.-90]{}{}
  \mesomeric[-90]{
    \reactant{\chemfig{[:30]-(=[2]O)-[:-30]=(-[2]O|\mch)-[:-30]O--[:-30]}}
    \marrow
    \reactant{\chemfig{[:30]-(=[2]O)-[:-30](-[6,.3,,,draw=none]\fscrm)-(=[2]O)-[:-30]O--[:-30]}}
    \marrow
    \reactant{\chemfig{[:30]-(-[2]O|\mch)=[:-30]-(=[2]O)-[:-30]O--[:-30]}}
  }
  \arrow[-90]{\Hpl, \ch{H2O}}{}
  \reactant[-90]{\chemfig{[:30]-(=[2]O)-[:-30]-(=[2]O)-[:-30]O--[:-30]}}
  \anywhere{one.0}{
    \elmove{C1}{-100:2cm}{C2}{-90:2cm}
    \elmove{b1}{10:5mm}{O1}{0:5mm}
    \elmove{O2}{180:5mm}{b2}{180:5mm}
    \elmove{b3}{80:5mm}{O3}{90:5mm}
    \elmove{b4}{0:5mm}{C3}{0:7mm}
    \elmove{O4}{-90:1cm}{H}{-45:1cm}
  }
 \end{rxnscheme}
\end{beispiel}


\section{Extensive Synthesis}\label{sec:tikzsynthese}
As last example we can create extensive syntheses, using the \cmd{merge} command.
\begin{rxnscheme}[,,,.8]{Extensive Synthesis}
 \setatomsep{1.5em}
 \branch[,start_left]{
   \reactant{\chemfig{=_[::30]-[::-60]-[::60](-[::-60])(-[::120])-[::0]OH}}
   \arrow[below]{\ch{HBr}}{}
   \reactant[below]{\chemfig{Br-[::30]-[::-60]=_[::60](-[::-60])-[::60]}}
 }
 \branch[right=of start_left,start_center,yshift=1em]{
   \reactant{\chemname{\chemfig[][scale=.8]{**6(--(-SO_2Cl)---(-)-)}}{tosyle chloride}}
   \arrow[below]{\ch{NaOH}}{\ch{Zn}}
   \reactant[below]{\chemfig[][scale=.8]{**6(--(-SO_2Na)---(-)-)}}
 }
 \branch[right=of start_center,start_right,xshift=3em,yshift=-10em]{
   \reactant{\chemname{\chemfig{-[::30](-[::60])=_[::-60]-[::60]COOH}}{\iupac{3\-methyl\-2\-butenoic acid}}}
   \arrow[below]{\ch{CH3OH}}{}
   \reactant[below]{\chemfig{-[::30](-[::60])=_[::-60]-[::60]CO_2CH_3}}
 }
 \branch[below=of start_left,target_one,xshift=5em,yshift=-5em]{
   \reactant{\chemfig[][scale=.8]{**6(--(-SO_2-[:30]-[::-60]=_[::60](-[::60])-[::-60])---(-)-)}}
 }
 \branch[below=of target_one,target_two,xshift=6em,yshift=-6em]{
   \mesomeric{\chemfig[][scale=.8]{-[::30](-[::60])=^[::-60]-[::60](-[::60]S(=[::90]O)(=[::-90]O)-[::0]**6(---(-)---))-[::-60](-[::0])(-[::-120])-[::60](-[::60,.5,,,white]\fminus)-[::-60]CO_2CH_3}}
   \arrow[below,,.5]{}{}
   \arrow[below,,.5]{\ch{KOH}}{}
   \reactant[below]{\chemname{\chemfig{-[::-30](-[::-60])=^[::60]>[::-60](-[::90,1.2])-[::30,1.2](-[::120,1.2](-[::-60])-[::0])<:[::-30]COOH}}{\iupac{\trans\-chrysanthemum acid}}}
 }
 \merge{target_one}{start_left}{start_center}
 \merge[\ch{NaOCH3}]{target_two}{target_one}{start_right}
\end{rxnscheme}
\begin{beispiel}[code only]
 \begin{rxnscheme}[,,,.8]{Extensive Synthesis}
  \setatomsep{1.5em}
  \branch[,start_left]{
    \reactant{\chemfig{=_[::30]-[::-60]-[::60](-[::-60])(-[::120])-[::0]OH}}
    \arrow[below]{\ch{HBr}}{}
    \reactant[below]{\chemfig{Br-[::30]-[::-60]=_[::60](-[::-60])-[::60]}}
  }
  \branch[right=of start_left,start_center,yshift=1em]{
    \reactant{\chemname{\chemfig[][scale=.8]{**6(--(-SO_2Cl)---(-)-)}}{tosyle chloride}}
    \arrow[below]{\ch{NaOH}}{\ch{Zn}}
    \reactant[below]{\chemfig[][scale=.8]{**6(--(-SO_2Na)---(-)-)}}
  }
  \branch[right=of start_center,start_right,xshift=3em,yshift=-10em]{
    \reactant{\chemname{\chemfig{-[::30](-[::60])=_[::-60]-[::60]COOH}}{\iupac{3\-methyl\-2\-butenoic acid}}}
    \arrow[below]{\ch{CH3OH}}{}
    \reactant[below]{\chemfig{-[::30](-[::60])=_[::-60]-[::60]CO_2CH_3}}
  }
  \branch[below=of start_left,target_one,xshift=5em,yshift=-5em]{
    \reactant{\chemfig[][scale=.8]{**6(--(-SO_2-[:30]-[::-60]=_[::60](-[::60])-[::-60])---(-)-)}}
  }
  \branch[below=of target_one,target_two,xshift=6em,yshift=-6em]{
    \mesomeric{\chemfig[][scale=.8]{-[::30](-[::60])=^[::-60]-[::60](-[::60]S(=[::90]O)(=[::-90]O)-[::0]**6(---(-)---))-[::-60](-[::0])(-[::-120])-[::60](-[::60,.5,,,white]\fminus)-[::-60]CO_2CH_3}}
    \arrow[below,,.5]{}{}
    \arrow[below,,.5]{\ch{KOH}}{}
    \reactant[below]{\chemname{\chemfig{-[::-30](-[::-60])=^[::60]>[::-60](-[::90,1.2])-[::30,1.2](-[::120,1.2](-[::-60])-[::0])<:[::-30]COOH}}{\iupac{\trans\-chrysanthemum acid}}
  }
  \merge{target_one}{start_left}{start_center}
  \merge[\ch{NaOCH3}]{target_two}{target_one}{start_right}
 \end{rxnscheme}
\end{beispiel}

\end{document}