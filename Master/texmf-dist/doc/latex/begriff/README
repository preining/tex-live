begriff.sty - a LaTeX2e package for typesetting begriffsschrift

This style style can be used with LaTeX to typeset material written in
Frege's Begriffsschrift.  It defines eight new LaTeX math mode commands:

\BGassert            - generates an assertion sign
\BGcontent           - generates an assertion sign
\BGnot               - generates a negation sign
\BGquant{v}          - generates a universal quantifier with variable v
\BGconditional{a}{c} - generates a conditional with antecendent
                       a and consequent c. Note that in the Begriffsschrift, 
                       the antecendent is placed below the consequent.

(the following three commands were introduced in version 1.5)

\BGterm{x}           - creates a right-justified terminal node x 
\BGstem{x}           - inserts arbitrary LaTeX maths x into a non-terminal node
\BGbracket{x}        - places the expression x inside brackets

You also have the normal resources of LaTeX math mode, and (if you
have the amssymb package, AMS symbols) at your disposal; so, you can
use \mathfrak{v} to get a fraktur character, \acute{v} to get a
character with a slanting hat, and \alpha (etc.) to get greek letters.

You can also tweak the appearance of the resulting material by
modifying the values of the following lengths:

\BGthickness - the thickness of lines (by default 0.4pt, 
               bigger numbers = bolder)
\BGbeforelen - the length of line inserted before a begriffsschrift symbol
\BGafterlen  - the length of line inserted after a begriffsschrift symbol
\BGspace     - the length of space inserted between begriffsschrift and 
               normal maths

(and, since version 1.5)

\BGlinewidth - the total width of the diagram

This is version 1.6 (20th May 2005)

which includes: 

Changes made in October 2004 by Richard Heck <heck@fas.harvard.edu>
Purpose of changes: To make the format of the formulae closer to how
they are set in Frege's published works.

And some additional changes made by Josh Parsons (May 2005) to fix a problem with linespacing.

The style file is hereby placed under the GNU General Public License.

For more information, see:

http://weka.ucdavis.edu/~ahwiki/bin/view/Main/BegriffsschriftLaTeX
