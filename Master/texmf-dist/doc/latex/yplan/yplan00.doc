yplan00.doc
------------
Date: August 4th, 1999
--------------------
This is the documentation for the yplan00a.tex and
yplan00b.tex files. Yplan is a vertical-type daily
planner.
%% --------------------------------------------
%% Copyright  1999 RWD Nickalls & Harald Harders
%% This program can be redistributed and/or modified under the terms
%% of the LaTeX Project Public License Distributed from CTAN
%% archives in directory macros/latex/base/lppl.txt; either
%% version 1 of the License, or any later version.
%% ---------------------------------------------

Authors:
      Dr RWD Nickalls,
      Department of Anaesthesia,
      City Hospital,
      Nottingham, UK.
      dicknickalls@compuserve.com
      TEL: +44-(0)-115-9691169 Ext:45637
      FAX: +44-(0)-115-9627713

      Harald Harders
      harald.harders@dlr.de

The Yplan00 YearPlanner consists of three files
yplan00a.tex, yplan00b.tex, and yplan.sty
which cover 6 months each as follows:-

yplan00a.tex  (January to June 2000)
yplan00b.tex  (July to December 2000)
yplan.sty     (Package file for international language support)

in CTAN/macros/latex/contrib/other/yplan/

These files will print a vertical-type daily planner
(ie months along the top, days downwards), with each
6-month period fitting onto a single A4 sheet.
They will also fit onto USA standard size paper.
The vertical size can be adjusted by changing \datestrut.
These files are really just large tables, and so you
may wish to extract just the tables and modify them
for your own use.

I was motivated to design this year-planner as my A4
diary did not have good year-planner at the end.

The files run nicely on Latex 2e, and may have to
be adjusted very slightly to run on LaTeX 2.09.

Dick Nickalls & Harald Harders

International support (by Harald Harders)
----------------------------------------
You can switch the language to either
english, german, french, spanish, italian, portuguese
by inserting the line
\usepackage[<language>]{yplan}
after the row containing the \documentclass command in the files 
yplan00a.tex and yplan00b.tex.
You have to replace <language> by your language name.
In addition you may use month names in lowercase letters (With an
uppercase first letter) by adding the keyword lowercase to the
optional parameters of the \usepackage command, e.g.
\usepackage[german,lowercase]{yplan}

However when using yplan.sty the adaption to LaTeX 2.09 isn't
possible anymore.

If you find an error in the names or abbreviations please
send me a note.

Harald Harders
