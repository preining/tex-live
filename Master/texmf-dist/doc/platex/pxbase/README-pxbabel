==================================================
== PXbase バンドル v0.5  <2010/06/15>           ==
==           by「ZR」（八登 崇之/Takayuki YATO）==
==                      <zrbabbler@yahoo.co.jp> ==
==================================================
(This file is encoded in UTF-8,)

----------------------------------------------------
 pxbabel パッケージ (v0.4) -- CJK 間のフォント切替 
----------------------------------------------------

■ 対応環境

    pLaTeX (ptexenc 拡張版) / upLaTeX2e

    pLaTeX の場合は OTF パッケージが必要。

■ 読込

    \usepackage[<オプション>]{pxbabel}

    オプションは以下の通り。

      japanese=<言語名> ; korean=<言語名> ;
      schinese=<言語名> ; tchinese=<言語名>
        各々の言語に対して実際に使用する Babel の言語オプションの名前を
        指定する。詳しくは「機能」の節を参照。

      main=<言語名>
        基底言語、すなわち文書開始時に有効である言語を、言語オプションの
        名前（言語識別子でない）で指定する。bxbase/pxbase パッケージの
        \bxmainlanguage を呼び出している。

      noswitchcaption  (既定で有効)
        Babel におけるキャプションや日付の文字列の切替機能を抑止する。
        このオプションがあると、bxbase/pxbase パッケージの
            \bxcaptionlanguage{default}
        が実行され、キャプションは文書クラスで設定されたもののままに
        なり、言語切替に追随して変更されなくなる。ただし、文書作成者が
        \bxcaptionlanguage を実行していた場合はそちらが優先される。
        この機能の詳細については pxbase パッケージの説明書を参照。

      switchcaption
        Babel におけるキャプションや日付の文字列の切替機能を保持する。
        noswitchcaption の否定。

      switchfont  (既定で有効)
        このパッケージが提供する、CJK 言語でフォントを切り替える機能を
        有効にする。

      noswitchfont
        このパッケージが提供する、CJK 言語でフォントを切り替える機能を
        無効にする。switchfont の否定。

■ 機能 (upLaTeX において)

    upTeX は標準で、日本語・中国語(繁体・簡体)・韓国語(以下これらを CJK
    言語と総称する)の TeX フォント(和文 TFM)を用意している。本パッケージ
    は Babel において CJK 言語の切替処理(\extras...)の中に、フォントの
    切替を追加し、言語の選択に応じて適切なフォントが選ばれるようにする。
    言語に対する Babel の言語オプションの名前は変わりうるので、その名前
    をパッケージ読込時に指定できるようにした。また、ある CJK 言語の為の
    オプションが読み込まれていない時は、フォントの切替のみをする簡易の
    言語オプションをその場で生成する。

    言語オプションの名前は \usepackage のオプションの中で
      <言語識別子>=<言語オプション名>
    の形式で指定する。ここで <言語識別子> は japanese(日本語), korean
    (韓国語), schinese(簡体字中国語), tchinese(繁体字中国語) のいずれか
    で、<言語オプション名> に Babel の言語オプションの名前を指定する。
    ある CJK 言語に対して指定を省略した場合は、言語識別子と同じ名前が
    言語オプション名となる。指定された言語オプションが存在する場合には
    その切替処理の中にフォント切替が追加される。存在しない場合は、指定
    された名前の言語オプションがその場で生成され、それは Babel で読み込
    まれた他の言語と同じように使用できる。

■ 機能 (pLaTeX において)

    OTF パッケージの multi オプションは pLaTeX において韓国語・中国語を
    扱うためのもので、これを使うと \UTFK／\UTFC／\UTFT で韓国語／中国語
    (簡体)／中国語(繁体)のフォントを用いて文字が出力される(これらの命令
    の引数は \UTF と同じで Unicode 値 16 進)。

    pLaTeX における本パッケージの動作は次のようである。まず、upLaTeX の
    場合と同じように CJK 言語のための Babel 言語オプションを生成する。
    その上で、CJK 言語に切り替わった場合には、\UTF の動作を「本来の \UTF」
    ／\UTFK／\UTFC／\UTFT に切り替える。この機能は単独ではあまり有用とは
    いえないが、bxutf8 入力エンコーディング(BXbase バンドル)と併用して CJK
    言語の UTF-8 直接入力を行う時に有用となる。すなわち直接入力された CJK
    文字は \UTF に変換されるので、結果的に upLaTeX の時と同じように、Babel
    で切り替えた言語用のフォントで出力することが可能となる。

    ※ ただし、この機構には、「JIS X 0208 にある文字はそもそも \UTF に変換
    されずに直接日本語フォントで出力される」という欠点をもつ。これを解決
    するには PXfltsrc バンドル等の機能を用いて文書にテキスト変換フィルタ
    を適用されるという処理が必要になる。

■ 開発者向け機能

    \pxLanguageName{<言語識別子>}
      その言語に対する言語オプションの生成。

    \pxDeclareBasicCJKEncoding
    \pxDeclareBasicCJKShape
    \pxDeclareBasicCJKFamily
      CJK 言語用の NFSS のフォント定義の命令。

■ 更新履歴

    README ファイルを参照。

# EOF
