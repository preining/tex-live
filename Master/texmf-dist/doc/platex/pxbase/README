==================================================
== PXbase バンドル v0.5  <2010/06/15>           ==
==           by「ZR」（八登 崇之/Takayuki YATO）==
==                      <zrbabbler@yahoo.co.jp> ==
==================================================
(This file is encoded in UTF-8,)

pLaTeX2e / upLaTeX2e における文書およびマクロパッケージの作成を支援する
基本的な機能を提供する。文書作成の為の機能には、具体的には Unicode の
コード値による文字入力、 PDF の文書情報に日本語を使用する設定の自動化、
upLaTeX の和文文字カテゴリ（kcatcode）の変更および CJK 間のフォント切替
の支援等が含まれる。

  - ifuptex パッケージ: upTeX 判定
  - pxbase パッケージ: 基礎ライブラリ
  - pxcjkcat パッケージ: 和文文字カテゴリ操作
  - pxbabel パッケージ: CJK 間のフォント切替 
  - upkcat パッケージ: 文字指定の kcatcode 操作
  - pxbasenc パッケージ: 文字コード関係ライブラリ(試作)

※ 対応環境は各々のパッケージの解説を参照。

※ pxbabel の解説は README-pxbabel を参照。

※ pxcjkcat の解説は README-pxcjkcat を参照。

■ 本ソフトウェアの一次配布サイト(作者のサイト)

    En toi Pythmeni tes TeXnopoleos ～電脳世界の奥底にて～
    http://zrbabbler.hp.infoseek.co.jp/

    ※ 以下のページに一部機能の使用例を紹介した。
    「Unicode による文字入力」
    http://zrbabbler.hp.infoseek.co.jp/unichar.html
    「upLaTeX を使おう」
    http://zrbabbler.hp.infoseek.co.jp/uplatex.html
    「PXbase パッケージ」
    http://zrbabbler.hp.infoseek.co.jp/pxbase.html

■ インストール

    TDS 1.1 に従ったシステムでは、各ファイルを次の場所に移動する。
    ・*.sty, *.def → $TEXMF/tex/platex/PXbase/
    (残りのファイルは不要)

    W32TeX を C:\usr\local にインストールした場合の例。
    ・*.sty, *.def → C:\usr\local\share\texmf-local\tex\platex\PXbase


-------------------------------------------
 pxbase パッケージ (v0.5) -- 基礎ライブラリ 
-------------------------------------------

pLaTeX/upLaTeX で和文を扱う上で必要になる基本的な機能を提供する。また
他の PX シリーズのパッケージの下請けの役割も果たす。

■ 対応環境

    pLaTeX2e / upLaTeX2e

■ 読込

    \usepackage で読み込む。オプションはない。

■ 機能

    使用している (u)pTeX の漢字コード設定の情報表示。

    \infojenc : 漢字コードの情報を次の形で端末とログに出力する。
      Kanji encoding: source=UTF8 internal=SJIS;

    これ以降に述べる機能は bxbase パッケージと共通である。

    DVI special 出力の命令。

    \usejapanesepdfstring
      pLaTeX と dvipdfmx の組み合わせで日本語の文書情報をもつ PDF を作
      る為に必要な pdf:tounicode special を出力する。
        pdf:tounicode 90ms-RKSJ-UCS2     (内部漢字コードが Shift_JIS)
        pdf:tounicode EUC-UCS2           (内部漢字コードが EUC-JP)
      この special は DVI のなるべく先頭に近い位置に出力する。
      (新しい版の hyperref への対策。)

    \recordpapersize
      次の形の papersize special 命令を DVI の先頭に出力する。
        papersize=<幅>,<高さ>
      (幅と高さは現在の \paperwidth と \paperheight の値。)

    \dvipdfmxmapline{<テキスト>}
      dvipdfmx のマップ指定を文書中で行う。
      ※次の形の papersize special 命令を DVI の先頭に出力する。
        pdf:mapline <テキスト>

    \dvipdfmxmapfile{<ファイル名>}
      dvipdfmx のマップファイル指定を文書中で行う。
      ※次の形の papersize special 命令を DVI の先頭に出力する。
        pdf:mapfile <ファイル名>

    符号値による文字入力のための命令。

    \Ux{<コード値>,...}
    \UI{<コード値>,...}
      Unicode コード値による入力を行う。\Ux は欧文用、\UI は和文用(I は
      Ideographic の意味)。コード値は以下の形式で表す。コンマで区切って
      複数文字入力できる。
        - <16進数>:  A72C, 02000B, 1bd 等。
        - +<10進数>: +254, +0937 等。
        - '<8進数>:  '376, '1651 等。
      Unicode 文字の出力には次の順番で利用可能な最初の機能を用いる。(\UI
      の場合 3) を飛ばす。)
        1) zxjatype パッケージ。この場合、その機構に従って出力される。\UI
           は必ず和文フォントで出力し、\Ux は和文/欧文切替の対象となる。
        2) XeTeX の Unicode 出力。
        3) [\Ux のみ] bxums パッケージ。
        4) upTeX の和文 Unicode 出力。
        5) UTF/OTF/pxotf パッケージ。
        6) ums/bxsuika パッケージ。
      どれも使えない場合は「16進表現による代替表現」になる。

    \AJ{<コード値>,...}
      Adobe-Japan1 のコード値による入力を行う。コード値は 10 進数で指定
      する。出力には次の順番で利用可能な最初の機能を用いる。
        1) zxotf パッケージ。
        2) UTF/OTF/pxotf パッケージ。
      どれも使えない場合は代替表現になる。

    \JI{<コード値>,...}
      いわゆる「JIS コード」（JIS X 0208 の GL 表現）のコード値による入力
      を行う。コード値指定の方法は \Ux と同じ。出力には (u)pTeX の JIS
      コード和文出力の機能を使う。使えない場合は代替表現になる。

    \KI{<コード値>,...}
      いわゆる「区点コード」のコード値による入力を行う。コード値指定は
      以下のいずれかの方法で指定する。
        - RRCC : RR は区番号、CC は点番号を 10 進 2 桁で表したもの。
        - PRRCC : P は面番号。JIS X 0213 のための指定。
      出力には (u)pTeX の JIS コード和文出力の機能を使う。使えない場合は
      代替表現になる。JIS X 0213 の文字を出力したい場合は、現在の和文
      フォントのエンコーディングが JIS X 0213 に対応するものである必要が
      ある。さらに、第 2 面の文字を出力する場合には pTeX の内部漢字コード
      が sjis でなければならない。

    \bxUx / \bxUI / \bxAJ / \bxJI / \bxKI
       \Ux 等は非常に短い名前なので他のパッケージと衝突する恐れがあり、
       そこでこのパッケージでは既に同名の命令がある場合は上書きしない
       ようにしている。\bxUx 等はそれぞれ \Ux 等と同じで、先のような場合
       にも常に使える。

    Bwbel 関係の命令。

    \bxcaptionlanguage{<言語名>}
      Babel では、通常言語が切り替わるとキャプションや日付の文字列が切り
      替わる。この命令を実行すると、文字列は指定された言語のもので固定
      され、Babel の言語切替の命令に追随しなくなる。この命令自体はプレ
      アンブル中で何回でも使用できて最後のものが有効になる。引数には有効
      な言語オプションの名前の他に以下のものが指定できる。
        - main : 基底言語、すなわち Babel で最後に読み込まれた言語だが
          次に述べる \bxmainlanguage で変更可能。
        - default : 文書クラスで指定されたものをそのまま用いる。日本語
          の文書クラスで Babel を用いる時に便利である。
      この命令の意義については後の「キャプション文字列の切替機能について」
      の節を参照。

    \bxmainlanguage{<言語名>}
      基底言語、すなわち \begin{document} の直後において有効となる言語を
      設定する。通常は Babel において最後に読み込まれた言語オプションが
      基底言語になる。

    その他の命令。

    \safecaret
      一部の箇所で TeX エスケープ形式(^^ab)の解釈が失敗するのを回避する。
      詳細は「TeX エスケープ形式(^^ab)の処理」の節を参照。

■ utf8x 入力エンコーディングの fasterror 設定

    unicode パッケージ(バンドル)が提供する utf8x 入力エンコーディングで
    は、パッケージで未定義の Unicode 文字が入力された場合エラーになる。
    その時のエラーメッセージ中に該当の文字の Unicode 名を出力するが、この
    際に高位バイトを含むファイル(テキスト情報をハフマン符号で圧縮したもの
    と思われる)を用いるので、pTeX では処理に失敗する。そこで本パッケージ
    では、ucs パッケージが読み込まれた場合には上記の機能を抑止するオプ
    ションである fasterror を常に有効にする。(utf8x が指定された時に ucs
    が未読込の場合は自動的に読み込まれる。)

■ TeX エスケープ形式(^^ab)の処理

    ptexenc 対応の pTeX では入力漢字コードが UTF-8 の時に JIS X 0208 に
    含まれない文字をエスケープ形式(^^ab)の UTF-8 バイト列に変換する。通常
    はこの形式は該当のバイト列と等価の解釈をされる。ところがここで <^> の
    catcode が本来の値 6 から変更されているとこの処理が失敗してしまう。
    具体的には次のような場合が該当する。
      - Babel の一部の言語(esperanto 等)を使用した場合
      - verbatim や類似の環境の中
    \safecaret 命令をプレアンブルで実行した場合、これらの場合でエスケープ
    形式の連続する出現をバイト列と解釈するようにする。

■ キャプション文字列の切替機能について

    Babel の機能の 1 つとして、言語の切替に伴って、キャプションの文字列
    (「参考文献」や「図」等)および日付の書式をその言語用のものに切り替え
    るというものがある。これにより、ある言語(例えばスロベニア語)の為の
    文書クラスがなくても英語用のクラスと Babel の言語切替でスロベニア語
    のキャプションの文書が作成できる(レイアウトがその言語の使用圏で許容
    されるかの問題は残るが)。だがこの目的には、プレアンブルでキャプション
    言語を 1 回指定できればよく、文書途中での言語切替にまでキャプション
    が追随する利点はあまりなく、場合によっては弊害があると思われる。この
    ような場合は
      \bxcaptionlanguage{main}
    を実行すれば、キャプションは基底言語(\bxmainlanguage の説明参照)で
    固定され以後は変更されない。また
      \bxcaptionlanguage{spanish}
    のように直接に言語名を指定することもできる。

    日本語の場合はもっと特殊な事情がある。日本語のレイアウトは欧米の言語
    のものとは大きく異なるので、日本語の文書を作成する場合には日本語用の
    文書クラスが用いられるのが普通であり、そこでは当然キャプションは既に
    日本語になっている。このような場合は
      \bxcaptionlanguage{default}
    を実行すれば、キャプションは文書クラスで設定されたものに固定され、
    以後は変更されない。

■ 開発者向け機能

    使用中の漢字コード系の情報表示。

    \pxInternalJEnc : [文字トークンへの \let] 内部漢字コードを表す。
      - s : シフトJIS
      - e : EUC
      - u : Unicode (upTeX)

    \pxSourceJEnc : [文字トークンへの \let] 入力 TeX ソースの漢字コード
      を表す。
      - s : シフトJIS
      - e : EUC
      - u : UTF-8

    \pxDocClassType : [整数定数] 使用中の文書クラスの種別。
      - 1 : pLaTeX2e/upLaTeX2e 標準文書クラス
      - 2 : 奥村氏製作の新標準文書クラス(jsclasses)
      - 0 : 上記のもの以外

    \pxUpScale : [マクロ] 和文の標準のフォントのスケール。\pxDocClassType
      により決まる: \pxDocClassType が 1 なら 0.962216、2 なら 0.92469、
      0 なら 1。

    残りの機能は bxbase パッケージと共通である。ここでは省略する。


-----------------------------------------
 ifuptex パッケージ (v0.2) -- upTeX 判定 
-----------------------------------------

※ 本パッケージはマクロパッケージ開発者向けのものである。

現在実行中の TeX が upTeX であるかを判定する。

■ 対応環境

    plain TeX / LaTeX2e (和文・欧文共)

■ 読込

    plain TeX の場合:
      \input ifuptex.sty
    LaTeX2e の場合:
      \usepackage{ifuptex}

■ 機能

    \ifupTeX [if-トークン]
      upTeX を使っているか。
    \ifnativeupTeX [if-トークン]
      upTeX を内部文字コードが Unicode の状態で使っているか。
    \RequireupTeX
      upTeX を使っている場合は何もしない。そうでない場合はエラーを出す。
    \RequirenativeupTeX
      upTeX を内部文字コードが Unicode の状態で使っている場合は何も
     しない。そうでない場合はエラーを出す。


-------------------------------------------------------
  upkcat パッケージ (v0.2) -- 文字指定の kcatcode 操作
-------------------------------------------------------

※ 本パッケージはマクロパッケージ開発者向けのものである。

文字を指定して(属するブロックの) kcatcode を操作する場合
    \kcatcode`<文字>
の形式を使うことになるが、その際にもしその文字の現在の kcatcode が 15
である場合は文字がバイト列とみなされるので、この形式が使えない。この
パッケージはそのような場合でも使用可能な、kcatcode の参照・変更の命令
を提供する。なお、文書作成者はこちらではなく pxcjkcat パッケージの機能を
使うべきである。

■ 対応環境

    plain upTeX / upLaTeX2e

■ 読込

    plain upTeX の場合:
      \input upkcat.sty
    LaTeX2e の場合:
      \usepackage{upkcat}

■ 機能

    \getkcatcode{<文字>}
      <文字> の現在の kcatcode の値(15～19)をマクロ \thekcatcode に(文字
      列として)返す。
    \setkcatcode{<文字>}{<値>}
      <文字> の kcatcode の値を <値> にする。


----------------------------------------

■ 更新履歴

    version 0,5 <2010/06/15>
      - pxbase: \JI/\KI を追加。
      - pxbase: \dvipdfmxmapline/\dvipdfmxmapfont を追加。

    version 0,4a <2010/02/07>
      - pxcjkcat: upTeX v0.29 における kcatcode のブロック分割の変更に
        対応。それに伴い ccv1, ccv2 オプションを新設。
      - pxcjkcat: \cjkcategory の第 1 引数に文字そのものを指定できる
        ようにした。
      - pxcjkcat: なぜかモード設定時の「Enclosed CJK Letters and Months」
        (cjk07)の kcatcode の設定値が 16 になっていた。upTeX 既定値に
        合わせて 18 に修正した。

    version 0,4 <2009/07/05>
      - safe caret 機構のコード(\safecaret の実装の確信)を pxbase.def に
        移動（BXbase と共通に）。
      - Babel に加えて verbatim でも safe caret 機構が働くようにする。
        ただし「pxbase は単に読み込むだけでは他人のコードを書き換えない」
        という指針があるので、\safecaret を実行しないと有効にならない。
      - pxbabel は中で \safecaret を呼ぶので、pxbabel を読み込むと safe
        caret は自動的に有効になる。
      - pxbabel でエンコーディングを J20 等に変えた場合に OTF パッケージ
        の \CID が動かなくなるのを修正。

    version 0,3 <2008/04/06>
      - pxbabel に safe caret 機構を追加。

    version 0,2b <2008/03/28>
      pxbabel に \UTF の切替を追加。

    version 0,2a <2008/03/18>
      pxbabel, pxbase のバグ取り。
      pxbabel の説明書をまだ書いていないことに気づいた ;-) 慌てて作成。

    version 0.2 <2008/03/14>
      最初の公開版。

# EOF
