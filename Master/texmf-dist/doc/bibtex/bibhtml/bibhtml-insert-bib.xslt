<?xml version="1.0" encoding="UTF-8"?>
<!--
 ! bibhtml-extract-aux.xslt
 !
 ! Part of bibhtml, version 2.0.1, released 2009 November 2
 ! Hg node 6ca3807543d6.
 ! See <http://purl.org/nxg/dist/bibhtml>
 !
 ! This sample script processes an XML file which contains elements like
 ! <span class='cite'>citation</span>, extracting each of the `citation'
 ! strings and emitting a .aux file which, once a \bibdata line has been
 ! appended, is suitable for processing with BibTeX.  Adapt or extend as
 ! appropriate.
 !-->
<stylesheet xmlns="http://www.w3.org/1999/XSL/Transform"
                version="1.0"
                exclude-result-prefixes="h"
                xmlns:h="http://www.w3.org/1999/xhtml">

  <output method="xml"
            version="1.0"
            doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
            omit-xml-declaration="yes"/>

  <param name='bibfile-name'>bibliography</param>
    

  <!-- identity transform -->
  <template match="*">
    <copy>
      <copy-of select="@*"/>
      <apply-templates/>
    </copy>
  </template>

  <template match="h:span[@class='cite']">
    <h:a>
      <attribute name='href'>
        <text>#</text>
        <copy-of select='.'/>
      </attribute>
      <copy-of select='.'/>
    </h:a>
  </template>

  <template match='processing-instruction("bibliography")'>
    <copy-of select='document(concat($bibfile-name,".bbl"))'/>
  </template>
</stylesheet>
