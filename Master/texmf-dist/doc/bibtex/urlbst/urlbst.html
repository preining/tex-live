<?xml version="1.0"?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Urlbst</title>
<link href="http://nxg.me.uk/" rev="author"/>
<link type="text/css" rel="stylesheet"
      href="http://nxg.me.uk/style/base.css"/>
</head>

<body>
<h1>Urlbst</h1>
<div class="abstract">
<p>A script to add a <code>webpage</code> BibTeX entry type, and add
support for general <code>url</code> and <code>lastchecked</code>
fields, to (most) BibTeX <code>.bst</code> files.  Optionally adds
basic support for <code>eprint</code>, <code>doi</code> and <code>pubmed</code> fields, and
hypertex/hyperref support.</p>

<p>The Persistent URL for this page is
<code>http://purl.org/nxg/dist/urlbst</code>.</p>

<p>Version 0.7, 2011 July 20.</p>
</div>

<p>Contents</p>
<ul>
<li><a href='#usage'>Usage</a></li>
<li><a href='#example'>For example...</a></li>
<li><a href='#download'>Download and installation</a></li>
<li><a href='#information'>Further Information</a></li>
<li><a href='#history'>Version history</a></li>
</ul>

<p>The standard BibTeX styles have no explicit support for the web, in
that they include no `webpage' type, and have no support for adding
URLs to references.  There are rumours that such support might appear
in BibTeX 1.0, but there is no estimate of when that is likely to arrive.</p>

<p>The Perl script <code>urlbst</code> can be used to add this support to an
arbitrary <code>.bst</code> file which has a reasonably `conventional'
structure.  The result is meant to be robust rather than pretty.</p>

<p>It has a different goal from Patrick Daly's `custom-bib' package --
that is intended to create a BibTeX style .bst file from scratch, and
supports `url' and `eprint' fields.  This package, on the other hand,
is intended for the case where you already have a style file that
works (or at least, which you cannot or will not change), and edits it
to add the new webpage entry type, plus the url, lastchecked and
eprint fields.  Fuller details are in the <a href="urlbst.pdf"
>manual</a>.</p>

<h2><a name='usage'>Usage</a></h2>
<pre>
urlbst [--[no]eprint] [--[no]doi] [--[no]pubmed]
           [--inlinelinks] [--nohyperlinks] [--hypertex] [--hyperref]
           [--literal key=value]
           [input-file [output-file]]
</pre>
<p>If either the input-file or the output-file is omitted, they
default to stdin and stdout respectively.  If the input file already
has a URL entry type, then the script objects.
By default (in the current version), the output
includes support for eprint, doi and pubmed, but these can be disabled
by <code>--noeprint</code>, <code>--nodoi</code> and
<code>--nopubmed</code>, respectively.</p>

<p>The added fields are:</p>
<ul>
<li><code>url</code> and <code>lastchecked</code>, to associate a URL with
  a reference, along with the date at which the URL was last checked
  to exist;</li>
<li><code>doi</code>, for a reference's <a href='http://doi.org'>DOI</a>;</li>
<li><code>eprint</code>, for an <a href='http://arxiv.org'>arXiv</a> eprint reference; and</li>
<li><code>pubmed</code> for a reference's 
  <a href='http://pubmed.gov'>PubMed</a> identifier (PMID).</li>
</ul>

<p>If either of the <code>--hypertex</code> or <code>--hyperref</code>
options is present, then the generated <code>.bst</code> file includes
support for hyperlinks in the generated eprint entries in the
bibliography, with the format being either HyperTeX (see <a
href="http://arxiv.org/hypertex/#implementation" >arXiv, again</a>),
supported by xdvi, dvips and others, or using the support available
from the hyperref package.  These options have no effect unless one of
the <code>--eprint</code> or <code>--doi</code> options is given: when
URLs are included in the bibliography, they are written out using the
<code>\url{...}</code> command, and hyperref automatically processes
that in the correct way to include a hyperlink.</p>

<p>The <code>--inlinelinks</code> option tells urlbst not to write out
the URL as text within the bibliography entry.  Instead, urlbst will
produce a more conventional-looking and compact bibliography entry,
but the entry's title will now be a hyperlink to that URL.  This
hyperlink may be useful to anyone who reads your document online
(this option can only be used with <code>--hyperref</code> or
<code>--hypertex</code>).  Any DOI or eprint text which would be
produced as a result of the appropriate option will still appear in
this mode.</p>

<p>You may adjust various literal strings, either for the purposes of
internationalisation, or simply because you prefer different text.  To
do this, give the <code>--literal</code> option, followed by a
<code>key=value</code> pair, for example <code>--literal online="on
th'intert00bs"</code>, to replace the default <code>URL:</code> text.
The possible values, and their defaults, are below (say
<code>--literal help</code> to get this list printed out):</p>
<table>
<tr><th>keyword</th><th>default</th><th>meaning</th></tr>
<tr><td>cited</td><td>cited</td><td>indicator of citation, or "last checked", date</td></tr>
<tr><td>doiprefix</td><td>doi:</td><td>text printed before DOI</td></tr>
<tr><td>eprintprefix</td><td>arXiv:</td><td>text printed before eprint
ref</td></tr>
<tr><td>linktext</td><td>[link]</td><td>fallback link text</td></tr>
<tr><td>pubmedprefix</td><td>PMID:</td><td>text printed before PUBMED reference</td></tr>
<tr><td>online</td><td>online</td><td>indication that resource is online</td></tr>
<tr><td>urlintro</td><td>URL:</td><td>location of online resource (an
alternative is "Available from:")</td></tr>
</table>

<p>The distribution includes preconverted versions of the four
standard BibTeX .bst style files.</p>

<p>Only the style files which result from conversion of the standard
styles are checked in the regression tests.  Other style files which
are known to work include</p>
<blockquote>
acm.bst, amsalpha.bst, amsplain.bst, apalike.bst, gerabbrv.bst,
geralpha.bst, gerapali.bst, gerplain.bst, gerunsrt.bst, ieeetr.bst,
siam.bst, mla.bst
</blockquote>

<p>Style files which are known to fail:</p>
<ul>
<li>Currently (as of at least v0.5), there exists a style <code>achicago.bst</code>
which seems to fox <code>urlbst</code>; it turns out that there's a
simpler but almost equivalent style <code>chicago.bst</code> which
works.</li>

<li>Some APA-like styles, including at least <code>apager.bst</code>,
seem to be sufficiently different from the most common styles, that
they confuse <code>urlbst</code> completely.</li>

<li>The <em>koma-script</em> styles and the <em>refer</em> styles are
not designed to produce conventional <code>.bbl</code> files, and
<code>urlbst</code> does not aim to produce anything useful from these.</li>
</ul>
<p>This might be a good time for me to revisit the rather
convoluted logic within the script, to make it a little more robust in
the face of variants like these, but don't hold your breath.  The
logic within the script is pretty hideous (it essentially works by
spotting patterns in the input style file, and replacing or extending
bits of BibTeX style-file code.  This is a fragile technique, and is
probably at the limits of its usefulness, therefore I'd take some
persuading to add significant extra functionality to the script.</p>

<p>The natbib and revtex style files already have URL fields.  If you have a
BibTeX style file which you think ought to work, but with which the
script fails, send it to me, and I'll try to work out what I've
missed (no promises, though).</p>

<p>Note that the command to invoke the script changed from `urlbst.pl'
to plain `urlbst' between versions 0.3 and 0.4.</p>

<h2><a name='example'>For example...</a></h2>
<p>To add URL support to the standard <code>siam.bst</code> file, you
can give the command</p>
<pre>
% urlbst /path/to/original/siam.bst siamurl.bst
</pre>
<p>Your TeX installation will likely have some way of helping you find where
the original <code>.bst</code> files are.  On teTeX-based systems, for
example, the command <code>kpsewhich siam.bst</code> returns the full
path of the <code>siam.bst</code> file which BibTeX would find.</p>

<p>The distributed files <code>abbrvurl.bst</code>,
<code>alphaurl.bst</code>, <code>plainurl.bst</code> and
<code>unsrturl.bst</code> are versions of the standard style files
which have been pre-converted.</p>

<p>There is more general discussion of including URLs in
bibliographies in the <a
href="http://www.tex.ac.uk/cgi-bin/texfaq2html?label=citeURL" >UK TeX
FAQ</a>.</p>

<h2><a name='download'>Download and installation</a></h2>

<p>Download the file as a
<a href="urlbst-0.7.tar.gz" >tarball</a>
or
<a href="urlbst-0.7.zip" >zip file</a>
and unpack it.  Or you can clone the source repository at 
<a href='http://bitbucket.org/nxg/urlbst/'>bitbucket.org</a>.</p>

<p>To install, you should simply copy the distributed
<code>urlbst</code> script to someplace on the path (such as
<code>/usr/local/bin</code>, or <code>~/local/bin</code>, depending on
your privileges and tastes).</p>

<p>If you (might) want to use the pre-converted standard
<code>.bst</code> files, then you'll need to copy these to somewhere
in the BibTeX search path.  Type <kbd>kpsepath bst</kbd> on Unix to
find out the list of places BibTeX searches, and pick either one of
the user-local locations, or a system-wide one.  If you're installing 
in a system-wide location, you'll need admin privileges, obviously,
and you will probably need to use <code>texhash</code>,
<code>mktexlsr</code> or its equivalent, to update LaTeX's filename
cache.  For further hints here, see 
the TeX FAQ entries on <a
href='http://www.tex.ac.uk/cgi-bin/texfaq2html?label=instpackages'
>installing a package</a> and <a
href='http://www.tex.ac.uk/cgi-bin/texfaq2html?label=wherefiles'
>where LaTeX puts files</a>.</p>

<p>That should be all you have to do.</p>

<p>The urlbst script is distributed with a path which should just work,
as long as the Perl binary is in your path, but if you have problems
here, then you might want to change the first line to something like</p>
<pre>
#! /usr/local/bin/perl
</pre>
<p>if that's where your Perl binary is.</p>

<p>You can also use the conventional <code>./configure; make; make
install</code> to configure and install the package (as root, or using
<code>sudo</code> if you're installing it in a system location),
though you still have to install the <code>.bst</code> files by hand.
This is rather heavyweight for the trivial amount of configuration
required, so it's almost always simpler just to do things by hand.</p>

<p>If you need to change some of the <code>urlbst</code> defaults,
however, or if your fingers type <code>./configure</code>
spontaneously, then you can be reassured that the configure script
supports the standard <code>--prefix</code> option, plus the following
options setting defaults:</p>
<dl>

<dt><code>--with-eprints=url</code>, <code>--without-eprints</code></dt>

<dd>This makes the <code>--eprints</code> option to urlbst available
by default, and allows you to optionally specify a prefix for creating
URLs from eprint numbers.  The default for this URL is
<code>http://arxiv.org/abs/</code> -- this is appropriate for arXiv,
obviously, but there are now a variety of other preprint archives
appearing, for which this might be inappropriate.  If you have
comments on this default, or alternatives, please let me know.  This
option is enabled by default; if you have some reason for disabling
the <code>--eprints</code> option for urlbst, the give the configure
option <code>--without-eprints</code></dd>

<dt><code>--with-doi=url</code>, <code>--without-doi</code></dt>

<dd>This makes available the <code>--doi</code> option to urlbst and,
as with the <code>--with-eprints</code> option, allows you to
customise the DOI resolver URL.  The <code>--doi</code> option to
urlbst is on by default.</dd>

<dt><code>--with-href=0/1/2</code></dt>

<dd>This allows you to specify the type of hyperlinks which are
inserted in the bibliography.  Values 0, 1 and 2, indicating no
hyperlinks, hypertex-style links, and hyperref-style links,
respectively.  The default is <code>--with-href=0</code>.  The
<code>hyperref</code>-style option is intended to complement the
increasingly standard <code>hyperref</code> package.</dd>

</dl>

<p>The first two options simply change defaults, and if you never use
the new <code>eprints</code> or <code>doi</code> fields, then the
defaults don't matter to you.  </p>

<p>Since the modified style generates URLs wrapped in the
<code>\url{}</code> macro, it is useful to use the resulting
bibliography style alongside the <code>url</code> package.  Since this
tends to work with <code>hyperref</code> anyway, where possible, you
don't get any extra goodness by changing the <code>--with-href</code>
default at configuration time.</p>

<h3>Documentation</h3>

<p>Basic documentation is in the file <code>urlbst.tex</code>.  This
is distributed as a PDF file, but if you want to regenerate it, use</p>
<pre>
% latex urlbst
% bibtex urlbst
% latex urlbst
% latex urlbst
</pre>

<h2><a name='information'>Further Information and acknowledgements</a></h2>

<p><code>urlbst</code> is archived on CTAN at <a
href="http://www.tex.ac.uk/tex-archive/biblio/bibtex/contrib/urlbst/"
><code>biblio/bibtex/contrib/urlbst/</code></a>, and discussed in the
<a href="http://www.tex.ac.uk/cgi-bin/texfaq2html?label=citeURL" >TeX
FAQ</a>.</p>

<p>The source code for the package is maintained at 
<a href='http://bitbucket.org/nxg/urlbst/'>bitbucket.org</a>, 
which also includes an issue tracker, where you can report bugs (or
just mail me).</p>

<p>The <a href="http://purl.org/nxg/dist/urlbst"
>home page of urlbst</a> might possibly have more recent versions than the
one you're looking at now.</p>

<p>Thanks are due to many people for suggestions and requests,
and to 'ijvm' for code contributions.</p>

<p>Copyright 2002-11, Norman Gray.  Released under the terms of the GNU
General Public Licence.</p>

<h2><a name='history'>Version history</a></h2>
<dl>
<dt><strong>0.7, </strong></dt>
<dd>Add --nodoi, --noeprints and --nopubmed options (which defaulted
on, and couldn't otherwise be turned off)</dd>

<dt>0.7b1, 2011 March 17</dt>
<dd>Allow parameterisation of literal strings, with option <code>--literal</code>.</dd>

<dt>0.6-5, 2011 March 8</dt>
<dd>Adjust support for inline links (should now work for arXiv, DOI and Pubmed)</dd>

<dt>0.6-4, 2009 April 28</dt>
<dd>Work around BibTeX linebreaking bug (thanks to Andras Salamon for the bug report).</dd>

<dt>0.6-3, 2009 April 19</dt>
<dd>Fix inline link generation (thanks to Eric Chamberland for the bug report).</dd>

<dt>0.6-2, 2008 November 17</dt>
<dd>We now turn on inlinelinks when we spot format.vol.num.pages,
which means we include links for those styles which don't include a
title in the citation (common for articles in physical science styles,
such as aip.sty).</dd>

<dt>0.6-1, 2008 June 16</dt>
<dd>Fixed some broken links to the various citation standards
(I think in this context this should probably <em>not</em> be happening, yes?).  
The distributed <code>*url.bst</code> no longer have the
<code>--inlinelinks</code> option turned on by default.</dd>

<dt><strong>0.6, 2007 March 26</strong></dt>
<dd><p>Added the option <code>--inlinelinks</code>, which adds inline hyperlinks
to any bibliography entries which have URLs, but does so inline, rather
than printing the URL explicitly in the bibliography.  This is (primarily)
useful if you're preparing a version of a document which will be read
on-screen.  Thanks to Jason Eisner for the suggestion, and much testing.</p>
<p>Incorporate hyperref bugfixes from Pawe&#x0142; Widera.</p>
<dl>
<dt>0.6b3</dt>
<dd>Further reworkings of the inlinelinks support, so that it's now
fired by a format.title (or format.btitle) line, with a fallback in
fin.entry.  This should be more robust, and allows me to delete some
of the previous version's gymnastics.</dd>

<dt>0.6b2</dt>
<dd>Reworked <code>inlinelinks</code> support; should now be more
robust.  Incorporate hyperref bugfixes from Pawe&#x0142; Widera.</dd>

<dt>0.6b1</dt>
<dd>Added the option <code>inlinelinks</code>, which adds inline hyperlinks
to any bibliography entries which have URLs, but does so inline, rather
than printing the URL explicitly in the bibliography.  This is (only)
useful if you're preparing a version of a document which will be read
on-screen.</dd>
</dl>
</dd>

<dt>0.5.2, 2006 September 6</dt>
<dd>Another set of documentation-only changes, hopefully clarifying
installation.</dd>

<dt>0.5.1, 2006 January 10</dt>
<dd>No functionality changes.  Documentation and webpage changes only,
hopefully clarifying usage and configuration</dd>

<dt><strong>0.5, 2005 June 3</strong></dt>
<dd>Added support for Digital Object Identifiers (DOI) fields in
bibliographies.</dd>

<dt>0.4-1, 2005 April 12</dt>
<dd>Documentation improvements -- there are now examples in the help text!</dd>

<dt><strong>0.4, 2004 December 1</strong></dt>
<dd>Bug fixes: now compatible with mla.bst and friends.
Now uses <code>./configure</code> (optionally).  Assorted reorganisation.</dd>

<dt><strong>0.3, 2003 June 4</strong></dt>
<dd>Added --eprint, --hypertex and --hyperref options.</dd>

<dt><strong>0.2, 2002 October 23</strong></dt>
<dd>The `editor' field is now supported in the webpage entry type.  Basic
documentation added.</dd>

<dt><strong>0.1, 2002 April</strong></dt>
<dd>Initial version</dd>

</dl>


<div class="signature">
<a href="http://nxg.me.uk"
	>Norman Gray</a><br/>
2011 July 20
</div>

</body>
</html>
