upTeX, upLaTeX  --- unicode version of ASCII pTeX, pLaTeX
2012.09.21 Ver1.11
TANAKA, Takuji   KXD02663(at)nifty(dot)ne(dot)jp

[ Contents ]
00readme_uptex.txt :: This file
01uptex_doc_utf8.txt :: A document (in Japanese)
02uptex_changelog_utf8.txt :: Change Log
under Build/ ::  sources of binaries
under Master/ :: macros, classes, fonts. etc.
under bin/ :: utility
under samples/ :: samples for test

[ Building upTeX ]
The sources work with TeXLive svn r27750 .
Ref. TeXLive and Subversion  http://www.tug.org/texlive/svn/

[ Status ]
Stable (hopefully) version.
No warranty.

[ References ]
[1] ASCII Nihongo TeX (Publishing TeX)
    ASCII MEDIA WORKS
    http://ascii.asciimw.jp/pb/ptex/
[2] upTeX, upLaTeX ― unicode version of pTeX, pLaTeX
    http://homepage3.nifty.com/ttk/comp/tex/uptex_en.html

