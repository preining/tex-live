#!/bin/bash

# commands to compile the `plipsum' package.
#
# 1 - the tex format
notangle -Rplipsum.tex plipsum.nw > plipsum.tex
#
# 2 - the example file.
notangle -Rpliptest.tex plipsum.nw > pliptest.tex
#
# 3 - the printable documentation.
noweave -tex -filter "elide paragraphs plipsum.tex pliptest.tex" \
plipsum.nw > plipsumdoc.tex
pdftex plipsumdoc.tex
