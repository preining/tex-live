#! /bin/bash
cd example
mpost --tex=latex graphs.mp
mv graphs.? graphs.??? ../
cd ..
echo \\documentclass[twocolumn]{article}\
     \\usepackage[dvips]{epsfig}\
     \\begin{document}\
     \\title{Exemplifier}\
     \\author{L. Nobre G.}\
     \\date{\\today}\
     \\maketitle\
     \\begin{center}\
     > Exemplifier.tex
for a in example/*.mp
do
  b=`basename $a .mp`
  mpost --tex=latex example/$b
  for c in $b.?
  do
    echo \\vfill\
      \\begin\{tabular\}\{c\}\
      \\epsfig\{file=$c\,width=0.9\\columnwidth\} \\\\\
      \\texttt\{$c\}\
      \\end\{tabular\} \\\\ >> Exemplifier.tex
  done
done
echo \\end{center}\\end{document} >> Exemplifier.tex
latex Exemplifier.tex
dvips -o Exemplifier.ps Exemplifier.dvi
bzip2 Exemplifier.ps
rm Exemplifier.aux
rm Exemplifier.dvi
rm Exemplifier.tex
mv Exemplifier.ps.bz2 doc/
rm *.?.PS *.?.aux *.?.latex *.?.dvi *.log *.?.mgk *.?.orig
rm example/*.mpx *.mpx
mkdir allps
mv *.? allps/
cd doc
pdflatex featpostmanual.tex
pdflatex featpostmanual.tex
pdflatex featpostbeamer.tex
pdflatex featpostbeamer.tex
pdflatex metapostpropaganda.tex
pdflatex metapostpropaganda.tex
