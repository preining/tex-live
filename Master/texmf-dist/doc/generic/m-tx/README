This is M-Tx (Music-from-TeXt), version 0.60d (2013-01-19).

M-Tx is a preprocessor for PMX that facilitates inputting lyrics. It
builds the PMX input file based on a language very similar to PMX. M-Tx
includes most of PMX's functionality, but it also permits in-line PMX
commands to give access to virtually all of PMX.

The author of M-Tx is Dirk Laurie (dpl@sun.ac.za).

To install (on TDS-compliant TeX systems):

 +  unzip tex-archive/install/m-tx.tds.zip at the root of a texmf tree and, 
    if necessary, update the filename database; e.g., texhash texmf. 
    Documentation for M-Tx is installed under doc/generic/tmx. You must use 
    an unzipping program that converts text files to the text-file format of 
    your platform, such as "unzip -a ... " on Linux.

 +  *either* copy the executables appropriate for your
    platform (Windows, OSX) to a folder on the executable PATH 
    and ensure it has execute permission 

 +  *or* on any Unix-like system with gcc and standard development tools
    installed: unpack mtx-0.60d.tar.gz (anywhere), move to the resulting
    mtx-0.60d directory, and do

       ./configure [--prefix=$HOME]
       make       (or make -f Makefile.orig if you have fpc installed)
       make install  (as root, if necessary)

    Then install a symbolic link m-tx in any directory on the executable
    PATH to scripts/m-tx/m-tx.lua

Documentation for M-Tx and the wrapper script is installed under 

doc/generic/m-tx

Many examples of M-Tx and MusiXTeX typesetting may be found at the Werner
Icking Music Archive at

http://icking-music-archive.org/

Support for users of MusiXTeX and related software may be obtained via
the MusiXTeX mail list at

http://tug.org/mailman/listinfo/tex-music

M-Tx may be freely copied, duplicated and used in conformance to the
GNU General Public License (Version 2, 1991, see included file gpl.txt).

This CTAN distribution is maintained by Bob Tennent (rdt@cs.queensu.ca).
Jan. 19th, 2013
