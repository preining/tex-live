\documentclass[11pt]{article}
\usepackage[textwidth=6.5in,textheight=8.5in]{geometry}
\usepackage[osf]{mathpazo}
\PassOptionsToPackage{urlcolor=black,colorlinks}{hyperref}
\RequirePackage{hyperref}
\usepackage{xcolor}
\newcommand{\myurl}[1]{\textcolor{blue}{\underline{\textcolor{black}{\url{#1}}}}}
\newcommand{\mtxVersion}{0.60d}
\begin{document}
\title{Installation of the CTAN M-Tx Distribution}
\author{Bob Tennent\\
\small\url{rdt@cs.queensu.ca}}
\date{\today}
\maketitle 
\thispagestyle{empty}

\section{Introduction}
We assume that you have already installed MusiXTeX and PMX.
Before trying to install M-Tx from CTAN, check whether your TeX distribution
provides packages for M-Tx; this will be easier than doing it yourself.
But if your TeX distribution
doesn't have M-Tx (or doesn't have the most recent version), this distribution
of M-Tx is very easy to install, though
you may need to read the material on 
installation of (La)TeX files in the 
TeX FAQ\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html}},
particularly
the pages on 
which tree to use\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html?label=what-TDS}}
and installing files\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html?label=inst-wlcf}}.

\section{Installing \texttt{m-tx.tds.zip}}

In this distribution of M-Tx, many of the files to be installed 
(including macros, documentation, and a wrapper script \verb\m-tx.lua\) are in 
\texttt{tex-archive/install/m-tx.tds.zip} at CTAN. The file
\verb|mt-x.tds.zip| is a zipped TEXMF
hierarchy; simply download it and unzip in the root folder/directory of whichever TEXMF tree
you decide is most appropriate, likely a ``local'' or ``personal'' one.
This should work with any TDS\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html?label=tds}}
compliant TeX distribution, including MikTeX, TeXlive and teTeX,
but you must use an unzipping program that 
converts text files to the text-file format of your
platform, such as \verb\unzip -a ...\ on Linux.


After unzipping the archive, update the filename database as necessary,
for example, by executing \verb\texhash ~/texmf\ or 
clicking the button labelled ``Refresh FNDB" in the MikTeX settings program.

Documentation for M-Tx is installed under \verb\doc/generic/m-tx\
in the TEXMF tree.  

\section{Installing \texttt{prepmx}}

The next step in the installation is to install
the one essential file that can't be installed in a TEXMF tree: the \texttt{prepmx} preprocessor. 


\subsection{Pre-Compiled Executables}

On Windows
systems, one can install \texttt{prepmx.exe} 
in the \texttt{Windows} sub-directory; this is a pre-compiled
executable and should be copied to any
folder on the PATH of executables. 
This might entail creating a suitable folder and adding that folder
to the PATH as follows: 
right click on the ``My Computer'' desktop icon,
left click on 
\begin{center}
Properties$\rightarrow$Advanced$\rightarrow$Environment Variables
\end{center}
in the ``System Variables'' section, scroll
down to ``path'', select it, click edit, and append the full path name you have selected for the new 
folder.  Also install the batch script \verb|m-tx.bat| in a folder on the PATH.

On the MAC OS-X/Intel platform, one can install the \texttt{prepmx} binary
that is in the 
\texttt{OSX} sub-directory.  Ensure that the file has the execute permission set:
\begin{list}{}{}\item
\verb\chmod +x prepmx\
\end{list}

\subsection{Compilation from Source}

On any platform with basic GNU tools (\texttt{gcc}, \texttt{tar}, \texttt{gunzip}, \texttt{make}),
you should be able to build the \texttt{prepmx} executable as follows:

\begin{enumerate}
\item Unpack the \texttt{mtx-\mtxVersion.tar.gz} archive:
\begin{list}{}{}
\item \texttt{tar zxvf mtx-\mtxVersion.tar.gz}
\end{list}
and move to the resulting \texttt{mtx-\mtxVersion} directory.
\item Configure:
\begin{list}{}{}
\item \verb\./configure --prefix=$HOME\
\end{list}
or just
\begin{list}{}{}
\item\verb\./configure \
\end{list}
if you have super-user privileges.
\item Build:
\begin{list}{}{}
\item \verb\make\
\end{list}
If this fails, you might want to install \texttt{fpc} (the Free Pascal Compiler\footnote{%
\myurl{http://www.freepascal.org/}}) and
try
\begin{list}{}{}
\item \verb\make -f Makefile.orig\
\end{list}

\item Install:
\begin{list}{}{}
\item \verb\make install\
\end{list}
This step should be executed as root if you need super-user privileges.
\end{enumerate}
You should now have an executable \verb\prepmx\ 
in your PATH.

\section{The Processing Script}

The Lua script \verb\.../scripts/m-tx/m-tx.lua\ 
is simply a convenient wrapper that
by default runs the following processes in order (and then deletes intermediate files):
\begin{itemize}\topsep=0pt\itemsep=0pt
\item \verb\prepmx\ (pre-pre-processing pass)
\item \verb\pmxab\ (pre-processing pass)
\item \verb\etex\  (1st pass)
\item \verb\musixflx\ (2nd pass)
\item \verb\etex\ (3rd pass)
\item \verb\dvips\ (to convert \verb\dvi\ output to Postscript)
\item \verb\ps2pdf\ (to convert \verb\ps\ output to Portable Document Format)
\end{itemize}%
There are many options to vary the default behaviour.

On a Unix-like system (with \texttt{luatex} installed), you can put a
symbolic link to \texttt{m-tx.lua} in any directory on the executable PATH as follows:
\begin{list}{}{}
\item \verb\ln -s <path to m-tx.lua> m-tx\
\end{list}
On Windows, install
the batch script
\begin{list}{}{}
\item \verb|Windows\m-tx.bat| 
\end{list}
to a folder
on the executable PATH.

Documentation for the \verb\m-tx\ script is in the
\verb\doc/generic/m-tx/scripts\ directory.


\section{Discussion}

Other pre-processor packages, additional documentation, additional
add-on packages, and many examples of M-Tx and MusiXTeX typesetting may be found
at the Werner Icking Music Archive\footnote{%
\myurl{http://icking-music-archive.org}}.
Support for users of MusiXTeX and related software may be obtained via
the MusiXTeX mail list\footnote{%
\myurl{http://tug.org/mailman/listinfo/tex-music}}.
M-Tx may be freely copied, duplicated and used in conformance to the
GNU General Public License (Version 2, 1991, see included file \verb\gpl.txt\).

\end{document}
