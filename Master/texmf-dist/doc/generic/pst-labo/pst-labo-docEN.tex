%	$Id: pst-labo-docEN.tex,v 1.4 2005/10/13 19:07:24 patrick Exp patrick $	
\title{\texttt{PST-Labo} - chemical objects\thanks{The english translation was done by Patrick Drechsler}\\
\footnotesize{version \PSTfileversion}}
\author{%
Denis Girou\thanks{\url{Denis.Girou@idris.fr}}, 
Christophe Jorssen\thanks{\url{Christophe.Jorssen@wanadoo.fr}},
Manuel Luque\thanks{\url{Mluque5130@aol.com}} and
Herbert Vo\ss\thanks{\url{voss@pstricks.de}}}
\date{\today}
\maketitle

\begin{abstract}
  \LPack{pst-labo} provides macros for a variety of devices used
  mainly for chemical applications.\cite{pst-labo} Since most of these
  devices have a standardised design \LPack{pst-labo} spares you the
  trouble of having to create them manually. Besides the usage of the
  package \LPack{pst-osci} this document also describes how to create
  ``high-level'' objects using the command \PST .\cite{PSTricks2} All
  basic objects are included in the file \LFile{pst-laboObj.tex} and
  are loaded during the start of \LPack{pst-labo}. These objects can
  be used for personal extensions. Section~\ref{sec:pstlabo:Objects}
  gives an overview of all objects.
\end{abstract}

\tableofcontents

\newpage
% ---------------------------------------------------------------------------------------
\section{Parameter}\label{sec:pstlabo8:Parameter}
% ---------------------------------------------------------------------------------------
Table~\ref{tab:pst-labo:Parameter} describes all parameters unique to
\LPack{pst-labo}.

\noindent
\LTXtable{\linewidth}{pstlabo8-tab1-EN.tex}%



% ---------------------------------------------------------------------------------------
\subsection{\texttt{glassType}}\label{subsec-pstlabo-glassType}
% ---------------------------------------------------------------------------------------
\Loption{glassType} describes the type of glass container. A normal
test tube is used by default.

\bgroup
\begin{LTXexample}[preset=\raggedright]
\psset{unit=0.5cm}
\pstTubeEssais
\pstTubeEssais[glassType=ballon]
\pstTubeEssais[glassType=erlen]
\pstTubeEssais[glassType=becher]
\pstTubeEssais[glassType=flacon]
\pstTubeEssais[glassType=fioleJauge]
\end{LTXexample}
\egroup


% ---------------------------------------------------------------------------------------
\subsection{\texttt{bouchon}}\label{subsec-pstlabo-bouchon}
% ---------------------------------------------------------------------------------------
The option \Loption{bouchon} seals the respective glass container with
a plug.

\bgroup
\begin{LTXexample}[width=0.575\linewidth,preset=\raggedright]
\psset{unit=0.45cm}
\psset{bouchon=true}
\pstTubeEssais[glassType=tube]
\pstTubeEssais[glassType=ballon]
\pstTubeEssais[glassType=erlen]
\pstTubeEssais[glassType=flacon]
\end{LTXexample}
\egroup


% ---------------------------------------------------------------------------------------
\subsection{\texttt{pince}}\label{subsec-pstlabo-pince}
% ---------------------------------------------------------------------------------------
The option \Loption{pince} attaches a wooden test tub clamp to the
glass container.

\bgroup
\begin{LTXexample}[width=0.55\linewidth,preset=\raggedright]
\psset{unit=0.5cm}
\psset{bouchon=true,pince=true}
\pstTubeEssais[glassType=tube]\hspace{1cm}
\pstTubeEssais[glassType=erlen]
\end{LTXexample}
\egroup




% ---------------------------------------------------------------------------------------
\subsection{\texttt{tubeDroit}}\label{subsec-pstlabo-tubeDroit}
% ---------------------------------------------------------------------------------------
The option \Loption{tubeDroit} inserts a narrow glass tube into the
glass container. Since this combination is only useful in combination
with the option \verb+bouchon=true+ it is set to this value by default
internally. It is to be noted that there is no vertical spacing of the
narrow glass tube inserted by default, so the user has to take care of
this manually, f.\,ex.~using \verb+\rule{0pt}{4cm}+.

\bgroup
\begin{LTXexample}[width=0.55\linewidth,preset=\raggedright]
\psset{unit=0.5cm}
\psset{tubeDroit=true}
\rule{0pt}{4cm}%
\pstTubeEssais
\pstTubeEssais[glassType=ballon]
\pstTubeEssais[glassType=erlen]
\end{LTXexample}
\egroup

% ---------------------------------------------------------------------------------------
\subsection{\texttt{tubeCoude}}\label{subsec-pstlabo-tubeCoude}
% ---------------------------------------------------------------------------------------
The option \Loption{tubeCoude} is basically identical to the previous
one, except for the fact that a right-angled glass tube is
drawn. Therefor the extra space needed in the vertical direction is
less.

\bgroup
\begin{LTXexample}[width=0.55\linewidth,preset=\raggedright]
\psset{unit=0.5cm}
\psset{tubeCoude=true}
\rule{0pt}{2.5cm}%
\pstTubeEssais[glassType=erlen]
\end{LTXexample}
\egroup

    
% ---------------------------------------------------------------------------------------
\subsection{\texttt{tubeCoudeU}}\label{subsec-pstlabo-tubeCoudeU}
% ---------------------------------------------------------------------------------------
The option \Loption{tubeCoude} is basically identical to the previous
one, except for the fact that a U-shaped glass tube is drawn. Therefor
there is less space needed in the vertical direction.

\bgroup
\begin{LTXexample}[width=0.5\linewidth,preset=\raggedright]
\psset{unit=0.5cm}
\psset{tubeCoudeU=true}
\rule{0pt}{2.5cm}%
\pstTubeEssais[glassType=ballon]
\end{LTXexample}
\egroup



% ---------------------------------------------------------------------------------------
\subsection{\texttt{tubeCoudeUB}}\label{subsec-pstlabo-tubeCoudeUB}
% ---------------------------------------------------------------------------------------
The option \Loption{tubeCoudeUB} is an extension of the U-shaped glass
tube which is only useful if extending the tube to the bottom makes
sense, as shown for instance in the macro \Lmcs{pstChauffageBallon}.

\bgroup
\begin{LTXexample}[pos=t,preset=\raggedright]
\psset{unit=0.5cm,glassType=ballon}
\pstChauffageBallon[tubeCoudeU] \pstChauffageBallon[tubeCoudeUB]
\end{LTXexample}
\egroup


% ---------------------------------------------------------------------------------------
\subsection{\texttt{etiquette} and \texttt{Numero}}\label{subsec-pstlabo-etiquette}
% ---------------------------------------------------------------------------------------
The option \Loption{etiquette} is a switch displaying labels defined
using the option \Loption{Numero}.


\bgroup
\begin{LTXexample}[width=0.42\linewidth,preset=\raggedright]
\psset{unit=0.5cm}
\pstTubeEssais[etiquette]
\pstTubeEssais[etiquette,Numero=1]
\pstTubeEssais[glassType=flacon,bouchon,%
  etiquette,Numero={\small Cu$^{2+}$}]
\end{LTXexample}
\egroup

% ---------------------------------------------------------------------------------------
\subsection{\texttt{tubePenche}}\label{subsec-pstlabo-tubePenche}
% ---------------------------------------------------------------------------------------
The option \Loption{tubePenche} allows tilting the chemical devices to
almost any desired angle while keeping the the air-water level
horizontal. The angles must be within the interval of $-65\ldots
+65$.

\bgroup
\begin{LTXexample}[width=0.4\linewidth,preset=\raggedright]
\psset{unit=0.5cm}
\pstTubeEssais[tubeDroit=true,tubePenche=40]
\pstTubeEssais[tubePenche=-20,bouchon]
\end{LTXexample}
\egroup  


% ---------------------------------------------------------------------------------------
\subsection{\texttt{tubeSeul}}\label{subsec-pstlabo-tubeSeul}
% ---------------------------------------------------------------------------------------
\Loption{tubeSeul} influences the size of the chosen box. This can be
advantageous as the following example using \Lmcs{psframebox} shows:
If you do not wish to add a further container to the box on the right
hand side the box would still have the same size as the one on the
left. The option \verb+tubeSeul=true+ prevents this from
happening. This option only has effects in combination with the macro
\Lmcs{pstChauffageTube} and glass containers of the type \verb+ballon+
and \verb+tube+.

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5cm,glassType=ballon,becBunsen}
\psframebox{\pstChauffageTube[becBunsen,barbotage]}
\psframebox{\pstChauffageTube[tubeSeul=true]}
\end{LTXexample}
\egroup  

% ---------------------------------------------------------------------------------------
\subsection{\texttt{becBunsen}}\label{subsec-pstlabo-becBunsen}
% ---------------------------------------------------------------------------------------
The option \Loption{becbunsen} toggles the drawing of a Bunsen burner.
\Loption{becbunsen} is set to \verb+true+ by default for the macro
\Lmcs{pstChauffeTube} and to \verb+false+ for the macro
\Lmcs{pstChauffageBallon}.

\bgroup
\begin{LTXexample}[width=0.5\linewidth]
\psset{unit=0.5cm,tubeSeul=true}
\pstChauffageTube 
\pstChauffageTube[becBunsen=false]
\end{LTXexample}
\egroup  


% ---------------------------------------------------------------------------------------
\subsection{\texttt{barbotage}}\label{subsec-pstlabo-barbotage}
% ---------------------------------------------------------------------------------------
The option \Loption{barbotage} creates an additional test tube which is
connected via a narrow glass tube to the original glass container. To
supply the necessary space the option \Loption{tubeSeul} should not be
activated (see section~\vref{subsec-pstlabo-tubeSeul})

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5cm}
\pstChauffageTube[tubeSeul=true] 
\pstChauffageTube[barbotage]
\end{LTXexample}
\egroup  



% ---------------------------------------------------------------------------------------
\subsection{\texttt{substance}}\label{subsec-pstlabo-substance}
% ---------------------------------------------------------------------------------------
The type of substance within the glass container can be selected by
the option \Loption{substance}. The default value is a blue fluid
(\Lmcs{pstBullesChampagne}). The available macros are summarised in
table~\ref{tab-pstlabo-substance}. It should be pointed out that
\Lmcs{pstFilaments} and \Lmcs{pstBULLES} are required parameters.

\begin{table}[!htb]
\caption{Summary of macros for the option \Loption{substance}}\label{tab-pstlabo-substance}
\centering
\begin{tabular}{@{}lcl@{}}
\emph{macro} & \emph{default} & \emph{comment}\\\hline
\Lmcs{pstBullesChampagne}\Largs{value} & 25 & standard\\
\Lmcs{pstFilaments}\Largs{value}\Largb{color} & 5\\ 
\Lmcs{pstBilles}\Largs{value} & 50 & two dimensional\\
\Lmcs{pstBULLES}\Largs{value}\Largb{color} & 20 & three dimensional
\end{tabular}
\end{table}

The optional value describes the number of passes for the internal
\Lmcs{multido}-loop. It is basically unlimited although values larger
than $80$ can lead to problems with \TeX 's defined memory. The later
can be modified in the \TeX\ configuration file. The location of this
file can be acquired using \verb+kpsewhich texmf.cnf+.

\begin{lstlisting}[language=sh]
voss@shania:~> kpsewhich texmf.cnf
/usr/local/texlive/2005/texmf/web2c/texmf.cnf
\end{lstlisting}


The first example shows the default values using two random
colours. The second example demonstrates the use of optional
arguments. Basically the macros \Loption{substance} and
\Loption{solide} can be mixed randomly.

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5cm,glassType=becher}
\pstTubeEssais 
\pstTubeEssais[substance=\pstBullesChampagne] 
\pstTubeEssais[substance=\pstFilaments{red}]
\pstTubeEssais[substance=\pstBilles]
\pstTubeEssais[substance=\pstBULLES{white}]
\end{LTXexample}
\egroup  


\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5cm,glassType=becher}
\pstTubeEssais[substance={\pstBullesChampagne[80]}] 
\pstTubeEssais[substance={\pstFilaments[20]{black}}]
\pstTubeEssais[substance={\pstBilles[80]}]
\pstTubeEssais[substance={\pstBULLES[20]{white}}]
\end{LTXexample}
\egroup  

When using the optional parameters for internal looping it has to be
noted that this parameter itself is used as part of another optional
parameter and therefor has to be set in curly braces as the above
example illustrates.


% ---------------------------------------------------------------------------------------
\subsection{\texttt{solide}}\label{subsec-pstlabo-solide}
% ---------------------------------------------------------------------------------------
The option \Loption{solide} describes the type of substance within the
glass containers. Table~\ref{tab-pstlabo-solide} summarises all
available values. The same rules apply as described in
section~\ref{subsec-pstlabo-substance}.

\begin{table}[!htb]
\caption{Summary of macros for the option \Loption{substance}}\label{tab-pstlabo-solide}
\centering
\begin{tabular}{@{}lc@{}}
\emph{macro} & \emph{default} \\\hline
\Lmcs{pstTournureCuivre}\Largs{value} & 30 \\
\Lmcs{pstClouFer}\Largs{value} & 60\\ 
\Lmcs{pstGrenailleZinc}\Largs{value} & 25 
\end{tabular}
\end{table}

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5cm,glassType=becher}
\pstTubeEssais 
\pstTubeEssais[solide=\pstTournureCuivre] 
\pstTubeEssais[solide=\pstClouFer]
\pstTubeEssais[solide=\pstGrenailleZinc]
\end{LTXexample}
\egroup  

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5cm,glassType=becher}
\pstTubeEssais[solide={\pstTournureCuivre[50]}] 
\pstTubeEssais[solide={\pstGrenailleZinc[80]}]
\pstTubeEssais[glassType=ballon,solide={\pstClouFer[50]}]
\end{LTXexample}
\egroup  

% ---------------------------------------------------------------------------------------
\subsection{\texttt{tubeRecourbe}}\label{subsec-pstlabo-tubeRecourbe}
% ---------------------------------------------------------------------------------------
The option \Loption{tubeRecourbe} creates a device to collect exhausting
gas from the glass container, including a Bunsen burner.

\bgroup
\begin{LTXexample}[pos=t,preset=\raggedright]
\psset{unit=0.5cm,glassType=erlen,recuperationGaz,substance=\pstTournureCuivre}
\pstChauffageBallon
\pstChauffageBallon[tubeRecourbe]
\end{LTXexample}
\egroup  

% ---------------------------------------------------------------------------------------
\subsection{\texttt{tubeRecourbeCourt}}\label{subsec-pstlabo-tubeRecourbeCourt}
% ---------------------------------------------------------------------------------------
The option \Loption{tubeRecourbe} creates a device to collect exhausting
gas from the glass container, excluding a Bunsen burner.

\bgroup
\begin{LTXexample}[pos=t,preset=\raggedright]
\psset{unit=0.5cm,glassType=flacon,recuperationGaz,substance=\pstFilaments{red}}
\pstChauffageBallon[tubeRecourbe]
\pstChauffageBallon[tubeRecourbeCourt]
\end{LTXexample}
\egroup  


% ---------------------------------------------------------------------------------------
\subsection{\texttt{doubletube}}\label{subsec-pstlabo-doubletube}
% ---------------------------------------------------------------------------------------
\Loption{doubletube} enables arranging two narrow glass tubes, one of
which has a stopcock.

\bgroup
\begin{LTXexample}[width=0.6\linewidth]
\rule{0pt}{4cm}
\psset{unit=0.5cm,glassType=ballon,%
  substance=\pstClouFer}
\pstBallon
\pstBallon[doubletube]
\end{LTXexample}
\egroup  


% ---------------------------------------------------------------------------------------
\subsection{\texttt{refrigerantBoulle}}\label{subsec-pstlabo-refrigerantBoulle}
% ---------------------------------------------------------------------------------------
\Loption{refrigerantBoulle} is one of the options for outputting a
more complex setup. When adding further devices it should be noted
that the geometrical origin is located in the centre of the setup.

\bgroup
\begin{LTXexample}[width=0.3\linewidth]
\psset{unit=0.5cm}
\pstBallon[refrigerantBoulles,glassType=ballon,%
  substance=\pstClouFer]
\end{LTXexample}
\egroup  


% ---------------------------------------------------------------------------------------
\subsection{\texttt{recuperationGaz}}\label{subsec-pstlabo-recuperationGaz}
% ---------------------------------------------------------------------------------------
\Loption{recuperationGaz} describes the device collecting expanded gases.

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5cm,glassType=flacon,tubeRecourbe,substance={\pstFilaments[10]{red}}}
\pstChauffageBallon
\pstChauffageBallon[recuperationGaz]
\end{LTXexample}
\egroup  


% ---------------------------------------------------------------------------------------
\subsection{\texttt{burette}}\label{subsec-pstlabo-burette}
% ---------------------------------------------------------------------------------------
The macro \Lmcs{pstDosage} displays a buret by default. This option
suppresses its display.

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.4cm}
\pstDosage[glassType=erlen]
\pstDosage[glassType=erlen,burette=false]
\end{LTXexample}
\egroup  

% ---------------------------------------------------------------------------------------
\subsection{\texttt{niveauReactifBurette} and \texttt{couleurReactifBurette}}\label{subsec-pstlabo-niveauReactifBurette}
% ---------------------------------------------------------------------------------------
\Loption{niveauReactifBurette} and \Loption{couleurReactifBurette}
control amount and color of the fluid in the buret.

\begin{center}
\bgroup
\psset{unit=0.4cm,glassType=erlen,niveauLiquide1=60}
\pstDosage[niveauReactifBurette=25,couleurReactifBurette=cyan]
\pstDosage[niveauReactifBurette=10]
\egroup  
\end{center}
\begin{lstlisting}
\psset{unit=0.4cm,glassType=erlen,niveauLiquide1=60}
\pstDosage[niveauReactifBurette=25,couleurReactifBurette=cyan]
\pstDosage[niveauReactifBurette=10]
\end{lstlisting}


% ---------------------------------------------------------------------------------------
\subsection{\texttt{AspectMelange} and \texttt{CouleurDistillat}}\label{subsec-pstlabo-AspectMelange}
% ---------------------------------------------------------------------------------------
\Loption{AspectMelange} defines the color of a chemical substance and
must comply to a predefined style to ensure the creation of a color
gradient. \Loption{CouleurDistillat} defines the color of the
distillate without this restriction.

\begin{center}
\bgroup
\psset{unit=0.4cm}
\pstDistillation(-3,-10)(7,6)\quad
\pstDistillation[AspectMelange=Diffusion,CouleurDistillat=red](-3,-10)(7,6)
\egroup  
\end{center}

\begin{lstlisting}
\psset{unit=0.4cm}
\pstDistillation(-3,-10)(7,6)\quad
\pstDistillation[AspectMelange=Diffusion,CouleurDistillat=red](-3,-10)(7,6)
\end{lstlisting}


% ---------------------------------------------------------------------------------------
\subsection{\texttt{phmetre}}\label{subsec-pstlabo-phmetre}
% ---------------------------------------------------------------------------------------
\Loption{phmetre} displays a pH-meter. Note that this option is only
available with the macro \Lmcs{pstDosage}.

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5cm,glassType=becher,burette=false}
\pstDosage
\pstDosage[phmetre]
\end{LTXexample}
\egroup  

% ---------------------------------------------------------------------------------------
\subsection{\texttt{agitateurMagnetique}}\label{subsec-pstlabo-agitateurMagnetique}
% ---------------------------------------------------------------------------------------
\Loption{agitateurMagnetique} is activated by default and displays a
heat block. When deactivated this option only the symbols are
suppressed, the rectangle is still displayed.

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5cm,burette=false,glassType=becher}
\pstDosage
\pstDosage[agitateurMagnetique=false]
\end{LTXexample}
\egroup


% ---------------------------------------------------------------------------------------
\subsection{\texttt{niveauLiquide1}, \texttt{niveauLiquide2}, \texttt{niveauLiquide3}
    and  \texttt{aspectLiquide1}, \texttt{aspectLiquide2}, \texttt{aspectLiquide3}}\label{subsec-pstlabo-niveauLiquide1}
% ---------------------------------------------------------------------------------------
These options define fluid level and style of the liquids 1, 2 and 3
respectively. The style can either be one of the default values or a
newly defined one as described in
section~\ref{subsec-pstlabo-substance}. Depending on the macro used
not all possible options can be used.

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.4cm,glassType=becher}
\rule{0pt}{6cm}
\pstDosage[niveauReactifBurette=18,niveauLiquide1=30,aspectLiquide1=Champagne,%
  glassType=becher,phmetre=true]
\pstDosage[niveauReactifBurette=20,niveauLiquide1=40,aspectLiquide1=Champagne,%
  glassType=becher,phmetre=false,agitateurMagnetique=false]
\end{LTXexample}
\egroup  

\bgroup
\makebox[\textwidth]{%
\begin{pspicture}(0,0)(5,6)
  \rput(4,3){\pstChauffageBallon[becBunsen=true,unit=0.5]}
  \rput(2.5,4){\pstBallon[glassType=becher,xunit=1,yunit=0.5,aspectLiquide1=Champagne,runit=0.7]}
  \psset{glassType=tube}
  \rput(2.5,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=70,aspectLiquide1=Diffusion]}
  \rput(3,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=75,aspectLiquide1=Sang]}
  \rput(2,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=80]}
\end{pspicture}
\begin{pspicture}(0,0)(5,6)
  \rput(4,3){\pstChauffageBallon[becBunsen=true,unit=0.5]}
  \rput(2.5,4){\pstBallon[glassType=becher,xunit=1,yunit=0.5,aspectLiquide1=Champagne,runit=0.7]}
  \rput(2.4,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=70,aspectLiquide1=Diffusion]}
  \rput(2.8,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=65,aspectLiquide1=Sang]}
  \rput(1.7,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=80,tubePenche=10]}
  \rput(3.5,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=80,tubePenche=-10]}
\end{pspicture}
\begin{pspicture}(1,3)(5,6)
  \rput(2.5,4){\pstBallon[glassType=ballon,unit=0.5,niveauLiquide1=15]}
  \rput(1.3,5.4){\pstTubeEssais[unit=0.5,niveauLiquide1=95,
  niveauLiquide2=60,niveauLiquide3=30,tubePenche=-60]}
\end{pspicture}
}
\egroup  

\begin{lstlisting}
\begin{pspicture}(0,0)(5,6)
  \rput(4,3){\pstChauffageBallon[becBunsen=true,unit=0.5]}
  \rput(2.5,4){\pstBallon[glassType=becher,xunit=1,yunit=0.5,aspectLiquide1=Champagne,runit=0.7]}
  \psset{glassType=tube}
  \rput(2.5,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=70,aspectLiquide1=Diffusion]}
  \rput(3,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=75,aspectLiquide1=Sang]}
  \rput(2,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=80]}
\end{pspicture}
\begin{pspicture}(0,0)(5,6)
  \rput(4,3){\pstChauffageBallon[becBunsen=true,unit=0.5]}
  \rput(2.5,4){\pstBallon[glassType=becher,xunit=1,yunit=0.5,aspectLiquide1=Champagne,runit=0.7]}
  \rput(2.4,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=70,aspectLiquide1=Diffusion]}
  \rput(2.8,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=65,aspectLiquide1=Sang]}
  \rput(1.7,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=80,tubePenche=10]}
  \rput(3.5,3.7){\pstTubeEssais[tubeDroit=true,unit=0.35,niveauLiquide1=80,tubePenche=-10]}
\end{pspicture}
\begin{pspicture}(1,3)(5,6)
  \rput(2.5,4){\pstBallon[glassType=ballon,unit=0.5,niveauLiquide1=15]}
  \rput(1.3,5.4){\pstTubeEssais[unit=0.5,niveauLiquide1=95,
  niveauLiquide2=60,niveauLiquide3=30,tubePenche=-60]}
\end{pspicture}
\end{lstlisting}



% ---------------------------------------------------------------------------------------
\section{Predefined colours and styles}\label{sec:pstlabo8:Stile}
% ---------------------------------------------------------------------------------------
The following summary shows all predefined colours and styles provided
by the package \LPack{pst-labo}, all of which all can be adapted by
the user.

\begin{lstlisting}[xrightmargin=-\marginparwidth]
\definecolor{Beige}         {rgb}{0.96,0.96,0.86}
\definecolor{GrisClair}     {rgb}{0.8,0.8,0.8}
\definecolor{GrisTresClair} {rgb}{0.9,0.9,0.9}
\definecolor{OrangeTresPale}{cmyk}{0,0.1,0.3,0}
\definecolor{OrangePale}    {cmyk}{0,0.2,0.4,0}
\definecolor{BleuClair}     {cmyk}{0.2,0,0,0}
\definecolor{LightBlue}     {rgb}{.68,.85,.9}
\definecolor{Copper}        {cmyk}{0,0.9,0.9,0.2}
\definecolor{Marron}        {cmyk}{0,0.3,0.5,.3}
%
\newpsstyle{aspectLiquide1}   {linestyle=none,fillstyle=solid,fillcolor=cyan}
\newpsstyle{aspectLiquide2}   {linestyle=none,fillstyle=solid,fillcolor=yellow}
\newpsstyle{aspectLiquide3}   {linestyle=none,fillstyle=solid,fillcolor=magenta}
\newpsstyle{Champagne}        {linestyle=none,fillstyle=solid,fillcolor=Beige}
\newpsstyle{BilleThreeD}      {linestyle=none,fillstyle=gradient,gradmidpoint=0,gradend=white,GradientCircle=true}
\newpsstyle{Sang}             {linestyle=none,fillstyle=solid,fillcolor=red}
\newpsstyle{Cobalt}           {linewidth=0.2,fillstyle=solid,fillcolor=blue}
\newpsstyle{Huile}            {linestyle=none,fillstyle=solid,fillcolor=yellow}
\newpsstyle{Vinaigre}         {linestyle=none,fillstyle=solid,fillcolor=magenta}
\newpsstyle{Diffusion}        {linestyle=none,fillstyle=gradient,gradmidpoint=0}
\newpsstyle{DiffusionMelange2}{fillstyle=gradient,gradbegin=white,gradend=red,gradmidpoint=0,linecolor=red}
\newpsstyle{flammeEtGrille}   {linestyle=none,fillstyle=gradient,gradmidpoint=0,gradbegin=OrangePale,gradend=yellow}
\newpsstyle{rayuresJaunes}    {fillstyle=hlines,linecolor=yellow,hatchcolor=yellow}
\newpsstyle{DiffusionBleue}   {fillstyle=gradient,gradmidpoint=0,linestyle=none,gradbegin=green,gradend=cyan}
\end{lstlisting}


% ---------------------------------------------------------------------------------------
\section{Macros}\label{sec:pstlabo8:Makros}
% ---------------------------------------------------------------------------------------

% ---------------------------------------------------------------------------------------
\subsection{\CMD{pstTubeEssais}}\label{sec:pstlabo8:pstTubeEssais}
% ---------------------------------------------------------------------------------------
This macro displays the simplest type of glass container and has
already been used numerous times in previous examples. The default
value when used without further parameters is a normal test tube
(\verb+glassType=tube+).

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5}
\pstTubeEssais
\pstTubeEssais[glassType=becher]
\pstTubeEssais[glassType=erlen,niveauLiquide1=80]
\pstTubeEssais[glassType=flacon]
\pstTubeEssais[glassType=ballon,niveauLiquide1=20,aspectLiquide1=DiffusionBleue]
\pstTubeEssais[glassType=fioleJauge]
\end{LTXexample}
\egroup

% ---------------------------------------------------------------------------------------
\subsection{\CMD{pstChauffageTube}}\label{sec:pstlabo8:pstChauffageTube}
% ---------------------------------------------------------------------------------------
\Lmcs{pstChauffageTube} enhances the previous macro to include either
a heat block, a Bunsen burner or a second narrow glass tube
respectively.

\begin{center}
\bgroup
\psset{unit=0.5}
\pstChauffageTube[tubeSeul]
\pstChauffageTube[glassType=ballon,becBunsen=false,tubeSeul]
\pstChauffageTube[glassType=erlen,becBunsen,pince,tubeSeul]
\pstChauffageTube[becBunsen,barbotage,glassType=flacon]
\pstChauffageTube[becBunsen,tubeCoude,glassType=ballon,niveauLiquide1=20,%
   aspectLiquide1=DiffusionBleue,tubeSeul,pince]
\egroup
\end{center}

\begin{lstlisting}[xrightmargin=-\marginparwidth]
\psset{unit=0.5}
\pstChauffageTube[tubeSeul]
\pstChauffageTube[glassType=ballon,becBunsen=false,tubeSeul]
\pstChauffageTube[glassType=erlen,becBunsen,pince,tubeSeul]
\pstChauffageTube[becBunsen,barbotage,glassType=flacon]
\pstChauffageTube[becBunsen,tubeCoude,glassType=ballon,niveauLiquide1=20,%
   aspectLiquide1=DiffusionBleue,tubeSeul,pince]
\end{lstlisting}


% ---------------------------------------------------------------------------------------
\subsection{\CMD{pstBallon}}\label{sec:pstlabo8:pstBallon}
% ---------------------------------------------------------------------------------------
\Lmcs{pstBallon} is basically identical to \Lmcs{pstTubeEssais} with
more possible options.

\begin{center}
\bgroup
\psset{unit=0.5cm}
\pstBallon\hspace{-0.5cm}
\pstBallon[glassType=erlen]\hspace{-0.5cm}
\pstBallon[glassType=becher,xunit=0.75cm,yunit=0.3cm,aspectLiquide1=Champagne,runit=0.4cm]\hspace{-0.5cm}
\raisebox{0.5cm}{\pstBallon[refrigerantBoulles=true]}
\egroup  
\end{center}

\begin{lstlisting}[xrightmargin=-\marginparwidth]
\psset{unit=0.5cm}
\pstBallon\hspace{-0.5cm}
\pstBallon[glassType=erlen]\hspace{-0.5cm}
\pstBallon[glassType=becher,xunit=0.75cm,yunit=0.25cm,aspectLiquide1=Champagne,runit=0.4cm]\hspace{-0.5cm}
\raisebox{1cm}{\pstBallon[refrigerantBoulles=true]}
\end{lstlisting}

% ---------------------------------------------------------------------------------------
\subsection{\CMD{pstChauffageBallon}}\label{sec:pstlabo8:pstChauffageBallon}
% ---------------------------------------------------------------------------------------
\Lmcs{pstChauffageBallon} enhances the previous macro by displaying a
Bunsen burner by default.

\begin{center}
\bgroup
\psset{unit=0.5cm}
\pstChauffageBallon
\pstChauffageBallon[barbotage,tubeCoudeUB,becBunsen,substance=\pstBilles]\\
\pstChauffageBallon[glassType=flacon,recuperationGaz,tubeRecourbeCourt,substance={\pstFilaments[10]{red}}]
\pstChauffageBallon[doubletube,recuperationGaz,substance=\pstClouFer]
\egroup
\end{center}

\begin{lstlisting}[xrightmargin=-\marginparwidth]
\psset{unit=0.5cm}
\pstChauffageBallon
\pstChauffageBallon[barbotage,tubeCoudeUB,becBunsen,substance=\pstBilles]\\
\pstChauffageBallon[glassType=flacon,recuperationGaz,tubeRecourbeCourt,substance={\pstFilaments[10]{red}}]
\pstChauffageBallon[doubletube,recuperationGaz,substance=\pstClouFer]
\end{lstlisting}



\begin{center}
\bgroup
\psset{unit=0.5cm}
\pstChauffageBallon[glassType=erlen,tubeRecourbe,recuperationGaz,substance=\pstTournureCuivre]\hspace{3cm}
\pstChauffageBallon[glassType=becher,aspectLiquide1=Champagne,substance=\pstBullesChampagne]\hspace{.25cm}
\pstChauffageBallon[glassType=erlen,substance=\pstBullesChampagne,tubeDroit]
\egroup  
\end{center}

\begin{lstlisting}[xrightmargin=-\marginparwidth]
\psset{unit=0.5cm}
\pstChauffageBallon[glassType=erlen,tubeRecourbe,recuperationGaz,substance=\pstTournureCuivre]
\pstChauffageBallon[glassType=becher,aspectLiquide1=Champagne,substance=\pstBullesChampagne]
\pstChauffageBallon[glassType=erlen,substance=\pstBullesChampagne,tubeDroit]
\end{lstlisting}




% ---------------------------------------------------------------------------------------
\subsection{\CMD{pstEntonnoir}}\label{sec:pstlabo8:pstTubeEssais}
% ---------------------------------------------------------------------------------------
\Lmcs{pstEntonnoir} displays a funnel. Called without any parameters
it is combined with a test tube. It can be combined with any kind of setup.

\bgroup
\begin{LTXexample}[pos=t]
\psset{unit=0.5cm}
\pstEntonnoir
\pstEntonnoir[glassType=becher,tubePenche=-20]
\pstEntonnoir[glassType=flacon,etiquette=true,Numero={\green 37},%
  aspectLiquide1=DiffusionBleue,niveauLiquide1=80]
\end{LTXexample}
\egroup  

% ---------------------------------------------------------------------------------------
\subsection{\CMD{pstEprouvette}}\label{sec:pstlabo8:pstEprouvette}
% ---------------------------------------------------------------------------------------
\Lmcs{pstEprouvette} displays a measuring cylinder. Its size can by
changed using the \PST scaling factor.

\bgroup
\begin{LTXexample}
\pstEprouvette[yunit=0.5cm]
\pstEprouvette[unit=0.6cm,niveauLiquide1=100,niveauLiquide2=60,niveauLiquide3=30]
\end{LTXexample}
\egroup  



% ---------------------------------------------------------------------------------------
\subsection{\CMD{pstpipette}}\label{sec:pstlabo8:pstpipette}
% ---------------------------------------------------------------------------------------
\Lmcs{pstpipette} displays a pipette. Its scaling range can be altered
by changing \PST scaling factors.

\bgroup
\begin{LTXexample}
\pstpipette[unit=0.5cm,tubePenche=40]
\pstpipette[yunit=0.5cm]
\end{LTXexample}
\egroup  


% ---------------------------------------------------------------------------------------
\subsection{\CMD{pstDosage}}\label{sec:pstlabo8:pstDosage}
% ---------------------------------------------------------------------------------------
\Lmcs{pstDosage} is normally used in combination with other
devices. The buret has a maximal capacity of 25 mL. The current height
and substrate can be changed by their options. An optional pH-meter
(only with glass type \verb+becher+) or heat block can be selected.

\bgroup\noindent
\makebox[\linewidth]{%
\psset{unit=0.5cm}
\pstDosage
\pstDosage[glassType=becher,phmetre=true]
\pstDosage[niveauReactifBurette=10,niveauLiquide1=60,aspectLiquide1=Champagne,%
  glassType=flacon,agitateurMagnetique=false]
\pstDosage[glassType=erlen,burette=false]}
\egroup  

\begin{lstlisting}[xrightmargin=-\marginparwidth]
\psset{unit=0.5cm}
\pstDosage
\pstDosage[glassType=becher,phmetre=true]
\pstDosage[niveauReactifBurette=10,niveauLiquide1=60,aspectLiquide1=Champagne,%
  glassType=flacon,agitateurMagnetique=false]
\pstDosage[glassType=erlen,burette=false]}
\end{lstlisting}


% ---------------------------------------------------------------------------------------
\subsection{\CMD{pstDistillation}}\label{sec:pstlabo8:pstDistillation}
% ---------------------------------------------------------------------------------------
The only macro which has be passed on to \Lmcs{pstDistillation} are
the dimensions of the \Lmenv{pspicture} environment.

\medskip
\noindent
\Lmcs{pstDistillation}\\
\Lmcs{pstDistillation}\Largr{$x_{ll},y_{ll}$}\Largr{$x_{ur},y_{ur}$} % $

\medskip
In case these coordinates are not supplied a rectangle of
$(-4,-10)(8,7)$ is used, assuming that further objects will be
included using \Lmcs{rput},

\bgroup
\begin{LTXexample}
\psset{unit=0.5cm}
\pstDistillation(-3,-10)(7,6)
\end{LTXexample}
\egroup  





% ---------------------------------------------------------------------------------------
\section{Basic objects}\label{sec:pstlabo:Objects}
% ---------------------------------------------------------------------------------------
The file \LFile{pst-laboObj.tex} includes all possible basic
objects. For lack of space they are not displayed here explicitly.


% ---------------------------------------------------------------------------------------
\section{Examples}\label{sec:pstlabo:Beispiele}
% ---------------------------------------------------------------------------------------
Creating complex examples is eased significantly by using a coordinate
grid underlying the setup (\Lmcs{psgrid}) as has been shown previously
in section~\vref{sec:overlay:Ueberschreiben}.

\begin{figure}
\centering
\bgroup
\sffamily
\begin{pspicture}(0,-4)(7,4)
  \rput(3,0){\pstDosage[niveauReactifBurette=25,niveauLiquide1=30,%
    aspectLiquide1=Champagne,glassType=becher,phmetre,unit=0.5]}
  \rput(2,3){B\"urette}   
  \rput(4.7,3.6){25 mL}
  \rput(5.2,-2.2){H$_3$O$^+$+Cl$^-$}
  \rput(.8,-3){PH-Messer} 
  \rput(5,-2.8){20 mL}
  \rput(5,1){Na$^+$+OH$^-$} \rput(6.4,-3.6){Heizplatte}
  \psline{->}(2.7,2.9)(3.4,2.9)
\end{pspicture}
\begin{pspicture}(-3,-2)(2,3)
  \psset{unit=0.5cm}
  \rput(-4.5,4.0){\pstEprouvette[tubePenche=-60,niveauLiquide1=90,niveauLiquide2=50]}
  \rput(.5,0){\pstEntonnoir[glassType=flacon,niveauLiquide1=30]}
  \rput(.5,7.5){
    \framebox{\begin{minipage}{3.2cm}Nach der Dekan\-ta\-tion
    sind die einzelnen Phasen getrennt, das Leichteste sammelt man durch Filtrieren.
    \end{minipage}}}
\end{pspicture}
\egroup
\caption{Example usage of \Lmcs{pstDosage}}
\end{figure}

\begin{figure}
\centering
\bgroup
\psset{unit=0.5cm,etiquette}
\newpsstyle{Nickel}{fillstyle=solid,fillcolor=green}
\pstTubeEssais[niveauLiquide1=20,aspectLiquide1=Champagne,Numero=1]\kern-20pt
\pstTubeEssais[niveauLiquide1=30,aspectLiquide1=Cobalt,Numero=2]\kern-20pt
\pstTubeEssais[niveauLiquide1=40,aspectLiquide1=Sang,Numero=3]\kern-20pt
\pstTubeEssais[Numero=4]\kern-20pt
\psset{bouchon,glassType=flacon}
\pstTubeEssais[aspectLiquide1=Vinaigre,Numero={\footnotesize MnO$_4^-$}]
\pstTubeEssais[aspectLiquide1=Huile,unit=0.75,Numero={\footnotesize Ce$^{4+}$}]
\pstTubeEssais[aspectLiquide1=Nickel,Numero={\footnotesize Ni$^{2+}$}]
\pstTubeEssais[Numero={\footnotesize Cu$^{2+}$}]
\pstTubeEssais[niveauLiquide1=30,aspectLiquide1=Champagne,Numero={\footnotesize NaOH},unit=0.75]
\egroup  
\caption{Example usage of \Lmcs{pstTubeEssais}}
\end{figure}


\nocite{*}
\bibliographystyle{plain}
\bibliography{pst-labo-doc}


\endinput