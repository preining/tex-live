\documentclass[11pt]{article}
\usepackage[textwidth=6.5in,textheight=8.5in]{geometry}
\usepackage[osf]{mathpazo}
\PassOptionsToPackage{urlcolor=black,colorlinks}{hyperref}
\RequirePackage{hyperref}
\usepackage{xcolor}
\newcommand{\myurl}[1]{\textcolor{blue}{\underline{\textcolor{black}{\url{#1}}}}}
\newcommand{\pmxVersion}{2.6.19}
\begin{document}
\title{Installation of the CTAN PMX Distribution}
\author{Bob Tennent\\
\small\url{rdt@cs.queensu.ca}}
\date{\today}
\maketitle 
\thispagestyle{empty}

\section{Introduction}
We assume that you have already installed a MusiXTeX distribution.
Before trying to install PMX from CTAN, check whether your TeX distribution
provides a package for PMX; this will be easier than doing it yourself.
But if your TeX distribution
doesn't have PMX (or doesn't have the most recent version), this distribution
of PMX is very easy to install, though
you may need to read the material on 
installation of (La)TeX files in the 
TeX FAQ\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html}},
particularly
the pages on 
which tree to use\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html?label=what-TDS}}
and installing files\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html?label=inst-wlcf}}.

\section{Installing \texttt{pmx.tds.zip}}

In this distribution of PMX, most of the files to be installed 
(including macros, documentation, and a processing script) are in 
\texttt{tex-archive/install/support/pmx.tds.zip} at CTAN.
The file \verb|pmx.tds.zip| is a zipped TEXMF
hierarchy; simply download it and unzip in the root folder/directory of whichever TEXMF tree
you decide is most appropriate, likely a ``local'' or ``personal'' one.
This should work with any TDS\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html?label=tds}}
compliant TeX distribution, including MikTeX, TeXlive and teTeX.

After unzipping the archive, update the filename database as necessary,
for example, by executing \verb\texhash ~/texmf\ or 
clicking the button labelled ``Refresh FNDB" in the MikTeX settings program.

Documentation for PMX is installed under \verb\doc/generic/pmx\
in the TEXMF tree.  

\section{Installing \texttt{pmxab} and \texttt{scor2prt}}

The next step in the installation is to install
the two essential files that can't be installed in a TEXMF tree: the \texttt{pmxab} preprocessor 
and \texttt{scor2prt}, an executable used to produce single-player parts from multi-player scores.

\subsection{Pre-Compiled Executables}

On Windows
systems, one can install \texttt{pmxab.exe} and
\texttt{scor2prt.exe} in the \texttt{Windows} sub-directory; these are pre-compiled
executables and should be copied to any
folder on the PATH of executables. 
This might entail creating a suitable folder and adding that folder
to the PATH as follows: 
in ``My Computer''
click on 
\begin{center}
View System Information\quad$\rightarrow$\quad Advanced\quad$\rightarrow$\quad Environment Variables
\end{center}
scroll
down to ``path'', select it, click edit, and add the path to \verb|...\scripts\musixtex\Windows| after a semi-colon.
The batch script \verb|pmx2pdf.bat| should also be installed to a folder on the PATH.


On the MAC OS-X platform (version 10.2 or better), one can install \texttt{pmxab} and \texttt{scor2prt} that are in the 
\texttt{OSX} sub-directory.

\subsection{Compilation from Source}

If you have conventional GNU development tools (\texttt{tar}, \texttt{gunzip}, \texttt{make})
and \texttt{gcc}\footnote{%
\myurl{http://gcc.gnu.org/gcc/}}
on your platform, 
you should be able to build \texttt{pmxab} and \texttt{scor2prt} executables.

To build \texttt{pmxab} and \texttt{scor2prt} for your platform: 
\begin{enumerate}
\item Unpack the \texttt{pmx-\pmxVersion.tar.gz} archive:
\begin{list}{}{}
\item \texttt{tar zxvf pmx-\pmxVersion.tar.gz}
\end{list}
and move to the resulting \texttt{pmx-\pmxVersion} directory.
\item Configure:
\begin{list}{}{}
\item \verb\./configure\
\end{list}
or, if you want the executables to be installed in your own path,
\begin{list}{}{}
\item \verb\./configure --prefix=$HOME\
\end{list}
\item Install:
\begin{list}{}{}
\item \verb\make install\
\end{list}
as root (admin) to install to the system path, or just
\begin{list}{}{}
\item \verb\make install\
\end{list}
to install in your own path.
\end{enumerate}
You should now have executables \verb\pmxab\ and
\verb\scor2prt\ available for use.

\section{The Processing Script}

The Lua script \verb\.../scripts/pmx/pmx2pdf.lua\ 
is simply a convenient wrapper that
by default runs the following processes in order (and then deletes intermediate files):
\begin{itemize}\topsep=0pt\itemsep=0pt
\item \verb\pmxab\ (pre-processing pass)
\item \verb\etex\  (1st pass)
\item \verb\musixflx\ (2nd pass)
\item \verb\etex\ (3rd pass)
\item \verb\dvips\ (to convert \verb\dvi\ output to Postscript)
\item \verb\ps2pdf\ (to convert \verb\ps\ output to Portable Document Format)
\end{itemize}%
There are many options to vary the default behaviour.

On a Unix-like system (with \texttt{texlua} installed), put a
symbolic link \texttt{pmx2pdf} in any directory on the executable PATH as follows:
\begin{list}{}{}
\item \verb\ln -s <path to pmx2pdf.lua> pmx2pdf \
\end{list}
On Windows, you can \emph{either}
copy the batch file
\begin{list}{}{}
\item \verb|Windows\pmx2pdf.bat| 
\end{list}
to a folder
on the executable PATH \emph{or} add the folder
\verb|Windows| to the executable PATH, as explained above.

Documentation for \verb\pmx2pdf\ is in the
\verb\doc/generic/pmx/scripts\ directory.


\section{Discussion}



Other pre-processor packages, additional documentation, additional
add-on packages, and many examples of PMX and MusiXTeX typesetting may be found
at the Werner Icking Music Archive\footnote{%
\myurl{http://icking-music-archive.org}}.
Support for users of MusiXTeX and related software may be obtained via
the MusiXTeX mail list\footnote{%
\myurl{http://tug.org/mailman/listinfo/tex-music}}.
PMX may be freely copied, duplicated and used in conformance to the
GNU General Public License (Version 2, 1991, see included file \verb\gpl.txt\).

\end{document}
