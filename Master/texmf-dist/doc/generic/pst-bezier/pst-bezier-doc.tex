%% $Id: pst-bezier-doc.tex 86 2009-01-29 10:34:00Z herbert $
\documentclass[11pt,english,BCOR10mm,DIV12,bibliography=totoc,parskip=false,smallheadings
    headexclude,footexclude,oneside]{pst-doc}
\usepackage[utf8]{inputenc}
\usepackage{pst-bezier}
\usepackage{esvect}
\let\vec\vv

\let\pstBezierFV\fileversion
\lstset{pos=l,wide=false,language=PSTricks,
    morekeywords={multidipole,parallel},basicstyle=\footnotesize\ttfamily}
%
\begin{document}

\title{\texttt{pst-bezier}}
\subtitle{A PSTricks package for drawing Bezier curves; v.\pstBezierFV}
\author{Tobias Nähring \\Herbert Vo\ss}
\docauthor{}
\date{\today}
\maketitle

\tableofcontents

\clearpage

\begin{abstract}
\noindent
The \LPack{pstricks} package provides (essentially) two main macros for
drawing curves: \Lcs{pscurve} and \Lcs{psbezier}. Both macros
employ Bezier \Index{spline}s.

The \Lcs{pscurve} macro takes multiple interpolated points as
arguments. Thus, it is easy to draw long multiply bent curves. The
problem with \Lcs{pscurve} is that there is no easy
way to change the automatically computed
control points without simultaneously changing the interpolated
points. Note that some control is possible via the
\Lkeyword{curvature} option.

The \Lcs{psbezier} macro gives full control over the
interpolation points and the control points of one Bezier polynominal
of degree three (two interpolated points and two control
points).

\vfill\noindent
Thanks to: \\
    Jean-C\^ome Charpentier.
\end{abstract}

%% Author: Tobias N"ahring

\section{Introduction}

If one demands for the access to certain control points of one
multiply bent curve one has to use multiple instances of the
\Lcs{psbezier} macro.  With this approache each inner interpolation
point of the curve has to be input twice. Furthermore, if one needs
smooth joints one has to compute control points symmetrically to the
corresponding interpolation points for every joint even if one does
not care so much about the exact tangential direction at some of those
joints.  That can be rather tedious.

The \Lcs{psbcurve} macro of the package \LPack{pst-bezier} is intented to
demonstrate a way to combine the nice properties of the macros
\Lcs{pscurve} and \Lcs{psbezier}.  It provides an easy input
format to describe `arbitrarily' many interpolation points of a curve
and to fix the control points at some freely selected interpolation
points.

Note, that \LPack{pst-bezier} is \emph{no final package} (e.g.
the automatical computation of the control points is not as refined as
that one for the macro \Lcs{pscurve}).

\section{Installation and usage of \texttt{pst-bezier.tex}}
\paragraph{Installation:}
As prerequisites for \LPack{pst-bezier} you need resent working
versions of \LaTeX{} and \LPack{pstricks}. The files
\LFile{pst-bezier.tex} and \LFile{pst-bezier.sty} must be somewhere
in your \TeX-input path. Further more, the file
\LFile{pst-bezier.pro} must be in some path, where \Lprog{dvips} can
find it.

\paragraph{Usage:}
As usual, load the packages \LPack{pstricks} and \LPack{pst-bezier}
in that order via the \Lcs{usepackage} macro.

Now you are ready to use the \Lcs{psbcurve} macro within your document
body. This macro is described in the next section with all its options.

Whith the following simple \LaTeX-source code you can test whether you have
correctly installed the package:

\begin{LTXexample}
\documentclass{minimal}
\usepackage{pstricks}
\usepackage{pst-bezier}
\begin{document}
 \begin{pspicture}(6,4)
   \psbcurve(1,2)(5,2) % Draw just one straight line.
 \end{pspicture}
\end{document}
\end{LTXexample}


\section{The \nxLcs{psbcurve} macro}
In the most simple form you can specify any number of interpolation
points as the argument of \Lcs{psbcurve}.

\begin{LTXexample}
\begin{pspicture}[showgrid=true](5,3)
  \psbcurve(1,1)(2,2)(3,1)(4,2)
\end{pspicture}
\end{LTXexample}

As usual, options can be specified within brackets.


\begin{LTXexample}
\begin{pspicture}[showgrid=true](5,3)
  \psbcurve[showpoints=true](1,1)(2,2)(3,1)(4,2)
\end{pspicture}
\end{LTXexample}

As you can see in the above example, the \Lkeyword{showpoints} feature works
(partially) with \Lcs{psbcurve}.

The next figure shows again the curve from the first example. This
time labels are added to the points (this is just for the following
description, it is not a feature of \Lcs{psbcurve}).

\begin{LTXexample}
\begin{pspicture}[showgrid=true](5,3)
  \psbcurve[showpoints=true](1,1)(2,2)(3,1)(4,2)
  \uput[-90](1,1){$\vec{p}_{0}=\vec{l}_{1}$}
  \uput[90](1.5,2){$\vec{r}_{1}$}
  \uput[90](2,2){$\vec{p}_{1}$}
  \uput[90](2.5,2){$\vec{l}_{2}$}
  \uput[-90](2.5,1){$\vec{r}_{2}$}
  \uput[-90](3,1){$\vec{p}_{2}$}
  \uput[-90](3.5,1){$\vec{l}_{3}$}
  \uput[90](4,2){$\vec{r}_{3}=\vec{p}_{3}$}
\end{pspicture}
\end{LTXexample}

The points labeled with $\vec{p}_{k}$ $(k=0,\dots,3)$ are the
interpolation points, these ones labelled with $\vec{l}_{1},\hdots,\vec{l}_{3}$,
and these ones labelled with $\vec{r}_{1},\hdots,\vec{r}_{3}$ are the left and
right control points, respectively.

Between each consecutive pair $\vec{p}_{k-1},\vec{p}_{k}$ of interpolation
points the \Lcs{psbcurve} macro draws a cubic Bezier spline.
The control points  $\vec{l}_{k}$ and $\vec{r}_{k}$ determine the tangential
direction of the bezier spline at the interpolation points. More
exactly, the bezier spline from $\vec{p}_{k-1}$ to $\vec{p}_{k}$ is tangent to
the vector $\vec{l}_{k}-\vec{p}_{k-1}$ at the point $\vec{p}_{k-1}$ and tantengial
to the vektor $\vec{r}_{k}-\vec{p}_{k}$ at the point $\vec{p}_{k}$.

Without any optional modifier arguments (described later in this text)
the control points are computed automatically
from the interpolation points by the formulas\footnote{Note that this
  method is very crude. To compute the curve such that the curvature
  is continuous would require solving a nonlinear system of
  equations. That is not implemented yet.}
%
\begin{align*}
  \vec{l}_{1}&= \vec{p}_{0}\\
  \vec{l}_{k}&= t_{k}(\vec{p}_{k}-\vec{p}_{k-2})&&\text{for }k=2,\hdots,n\\
  \vec{r}_{k}&= t_{k}(\vec{p}_{k-1}-\vec{p}_{k+1})&&\text{for }k=1,\hdots,n-1\\
  \vec{r}_{n}&= \vec{p}_{n}
\end{align*}
%
where $t_{k}$ $(k=1,\hdots,n)$ are real coefficients which are called
tension and which default to the value \Lkeyword{bcurveTension}=0.25.

You can change the appearance of the curve by several modifiers.
First of all you can directly set the left and right control points
via the modifiers \Lnotation{l}\Largr{\CAny} and \Lnotation{r}\Largr{\CAny}, resp., as
shown in the next two examples. The unmodified curve is drawn in the
background in {\color{blue}blue} color.



\begin{LTXexample}
\pspicture[showgrid=true](5,3)
\psset{showpoints=true}
\psbcurve[linecolor=blue,linewidth=0.01](1,1)%
  (2,2)(3,1)(4,2)
\psbcurve(1,1)l(2,1)(2,2)(3,1)r(4,1)(4,2)
\uput[-90](2,1){$\vec{l}_{1}$}
\uput[-90](4,1){$\vec{r}_{3}$}
\endpspicture
\end{LTXexample}

\begin{LTXexample}
\pspicture[showgrid=true](5,3)
\psset{showpoints=true}
\psbcurve[linecolor=blue,linewidth=0.01](1,1)%
  (2,2)(3,1)(4,2)
\psbcurve(1,1)(2,2)l(2,1)(3,1)(4,2)
\uput[-90](2,1){$\vec{l}_{2}$}
\endpspicture
\end{LTXexample}


On the right hand side the last example is shown once more without grid and
with \Lkeyset{showpoints=false}. There, you see that there is a corner at the second
interpolation point.


\begin{LTXexample}
\pspicture(5,3)
\psbcurve(1,1)(2,2)l(2,1)(3,1)(4,2)
\endpspicture
\end{LTXexample}

If you change some left control point $\vec{l}_{k}$ with the help of the
\Lnotation{L}\Largr{\CAny} modifier then the control point
$\vec{r}_{k-1}$ is set symmetrically to $\vec{l}_{k}$ with respect to the
interpolation point $\vec{p}_{k-1}$. In that way you get a smooth joint as
demonstrated in the next example.

\begin{LTXexample}
\pspicture[showgrid=true](5,3)
\psbcurve[linecolor=blue,linewidth=0.01](1,1)%
  (2,2)(3,1)(4,2)
\psset{showpoints=true}
\psbcurve(1,1)(2,2)L(2,1)(3,1)(4,2)
\uput[-90](2,1){$\vec{l}_{2}$}
\uput[0](2,2){$\vec{p}_{1}$}
\uput[0](2,3){$\vec{r}_{1}$}
\endpspicture
\end{LTXexample}

With the \Lnotation{t}\Largb{t} modifier you can change the tension of the
automatically computed control points of the current Bezier spline.


\begin{LTXexample}
\pspicture[showgrid=true](5,3)
\psset{showpoints=true}
\psbcurve[linecolor=blue,linewidth=0.01](1,1)%
  (2,2)(3,1)(4,2)
\psbcurve(1,1)(2,2)t{0.5}(3,1)(4,2)
\endpspicture
\end{LTXexample}


As you can see from the example both control points of the current
spline are affected by the \Lnotation{t}\Largb{t} modifier.
If you want to change the tension of just the left or right control
point you can use the \Lnotation{tl}\Largb{t} or \Lnotation{tr}\Largb{t} modifier,
respectively, as demonstrated in the following two examples.

\begin{LTXexample}
\pspicture[showgrid=true](5,3)
\psset{showpoints=true}
\psbcurve[linecolor=blue,linewidth=0.01](1,1)%
  (2,2)(3,1)(4,2)
\psbcurve(1,1)%
  (2,2)tl{0.5}(3,1)(4,2)
\endpspicture
\end{LTXexample}


\begin{LTXexample}
\pspicture[showgrid=true](5,3)
\psset{showpoints=true}
\psbcurve[linecolor=blue,linewidth=0.01](1,1)%
  (2,2)(3,1)(4,2)
\psbcurve(1,1)(2,2)tr{0.5}(3,1)(4,2)
\endpspicture
\end{LTXexample}


The \Lnotation{ts}\Largb{t} modifier changes the tension of the left and right
control points next to the interpolation point which stands in front
of the modifier. In the next example a negative tension value leads to
a rather surprising effect.

\begin{LTXexample}
\pspicture[showgrid=true](5,3)
\psset{showpoints=true}
\psbcurve[linecolor=blue,linewidth=0.01](1,1)%
  (2,2)(3,1)(4,2)
\psbcurve(1,1)(2,2)ts{-0.5}(3,1)(4,2)
\endpspicture
\end{LTXexample}

The default value of the tension can be set with the option
\Lkeyword{bcurveTension} as in the following example.


\begin{LTXexample}
\pspicture[showgrid=true](5,3)
\psset{showpoints=true}
\psbcurve[linecolor=blue,linewidth=0.01](1,1)%
  (2,2)(3,1)(4,2)
\psbcurve[bcurveTension=0.5](1,1)%
  (2,2)(3,1)(4,2)
\endpspicture
\end{LTXexample}

You can set this option also with the help of the \Lcs{psset} macro.
%
It is even possible to change the value of \Lkeyword{bcurveTension} in the
middle of a \Lcs{psbcurve}. Just use the modifier \Lnotation{T}\Largb{t} for
that purpose as shown in the following example.

\begin{LTXexample}
\pspicture[showgrid=true](5,6)
\psset{showpoints=true}
\psbcurve[linecolor=blue,linewidth=0.01](1,1)%
  (2,2)(3,1)(4,2)(4,4)(3,5)%
  (2,4)(1,5)
\psbcurve(1,1)(2,2)(3,1)(4,2)%
  T{0.5}(4,4)(3,5)(2,4)(1,5)
\endpspicture
\end{LTXexample}

Certainly, you can use the \Lnotation{T}\Largb{t} modifier several times in one
curve. (Try it for yourself.)
%
The \texttt{linestyle} and \texttt{fillstyle} options (and several
more) are respected by \Lcs{psbcurve} as the following example shows.

\begin{LTXexample}
\pspicture[showgrid=true](5,3)
\psbcurve[linestyle=dashed,
  linewidth=3pt,
  dash=0.5 0.2,
  fillstyle=solid,
  fillcolor=blue](1,1)(2,2)(3,1)(4,2)
\endpspicture
\end{LTXexample}

\section{Things that do not work (`known bugs')}
As already mentioned this project is something like an experiment. So,
there are many things that do not work.

\begin{itemize}
\item new lines inside the argument list are not ignored.
\item The control points are computed in a rather crude way (see
  above). The \Lkeyword{curvature} option is not recognised.
\item If \Lkeyword{fillstyle} is set to \Lkeyword{solid} and
  \Lkeyset{showpoints=true} then the fill color covers the interpolation and control points.
\item arrow heads do not work.
\end{itemize}


\section{List of all optional arguments for \texttt{pst-bezier}}

\xkvview{family=pst-bezier,columns={key,type,default}}


\bgroup
\raggedright
\nocite{*}
\bibliographystyle{plain}
\bibliography{pst-bezier-doc}
\egroup

\printindex
\end{document}


