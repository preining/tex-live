%% $Id: pst-func-doc.tex 72 2009-01-08 22:00:47Z herbert $
\documentclass[11pt,english,BCOR10mm,DIV12,bibliography=totoc,parskip=false,smallheadings
    headexclude,footexclude,oneside]{pst-doc}
\usepackage[utf8]{inputenc}
\usepackage{pst-math}
\let\pstMathFV\fileversion
\usepackage{pstricks-add}
\lstset{pos=t,wide=true,language=PSTricks,
    morekeywords={psGammaDist,psChiIIDist,psTDist,psFDist,psBetaDist,psPlotImpl},basicstyle=\footnotesize\ttfamily}
%
\def\pshlabel#1{\footnotesize#1}
\def\psvlabel#1{\footnotesize#1}
%
\makeatletter
\def\DefOfOperator{\@ifstar{\DefOfOperator@}{\DefOfOperator@@}}
\def\DefOfOperator@#1#2#3#4{{\operator@font#1}:\left\{\begin{array}{ccc} #2&\to&#3\\
x&\mapsto&#4\end{array}\right.}
\def\DefOfOperator@@#1#2#3{{\operator@font#1}:\left\{\begin{array}{ccc} #2&\to&#3\\
x&\mapsto&{\operator@font#1}(x)\end{array}\right.}
\makeatother

\begin{document}

\title{\texttt{pst-math}}
\subtitle{Special mathematical PostScript functions; v.\pstMathFV}
\author{Christoph Jorssen\\Herbert Vo\ss}
\docauthor{Herbert Vo\ss}
\date{\today}
\maketitle

\tableofcontents

\clearpage

\begin{abstract}
\noindent
\LPack{pst-math} is an extension to the \Index{PostScript} language. The files \LFile{pst-math.sty}
and \LFile{pst-math.tex} are only wrapper files for the \LFile{pst-math.pro} file, which
defines all the new mathematical functions for use with PostScript.

\vfill\noindent
Thanks to: \\
Denis Bitouzé;
Jacques L'helgoualc'h; 
Patrice M\'egret; 
Dominik Rodriguez
\end{abstract}

\section{Introduction}
\nxLPack{pst-math} defines \Lcs{pstPi} on \TeX\ level which expects 1,2,3 or 4 as
parameter. It is not available on PostScript level.

\begin{BDef}
\Lcs{pstPI}\Larg\#
\end{BDef}

\Lcs{pstPI}1 $\Rightarrow$ $\pi$\\
\Lcs{pstPI}2 $\Rightarrow$ $\dfrac{\pi}{2}$\\[2pt]
\Lcs{pstPI}3 $\Rightarrow$ $\dfrac{\pi}{3}$\\[2pt]
\Lcs{pstPI}4 $\Rightarrow$ $\dfrac{\pi}{4}$


\section{Trigonometry}

\LPack{pst-math} introduces natural trigonometric PostScript operators \Lps{COS}, \Lps{SIN} and \Lps{TAN} defined by
\[\DefOfOperator{cos}{\mathbb R}{[-1,1]}\]
\[\DefOfOperator{sin}{\mathbb R}{[-1,1]}\]
\[\DefOfOperator{tan}{\mathbb R\backslash\{k\dfrac{\pi}2,k\in\mathbb Z\}}{\mathbb R}\]
where $x$ is in \emph{radians}. \Lps{TAN} does \emph{not} produce a PS error\footnote{\nxLps{TAN} is defined with
Div, a special PSTricks operator rather than with div, the default  PS operator.} when $x=k\dfrac{\pi}{2}$.

\begin{center}
\begin{tabular}{@{} >{\sffamily}l l >{\sffamily}l l @{} }
\emph{Stack} & \emph{Operator} & \emph{Result} & \emph{Description} \\\hline
num & \Lps{COS} & real & Return \Index{cosine} of \textsf{num} radians \\
num & \Lps{SIN} & real & Return \Index{sine} of \textsf{num} radians \\
num & \Lps{TAN} & real & Return \Index{tangent} of \textsf{num} radians\\\hline
\end{tabular}
\end{center}

%\begin{LTXexample}[pos=t,wide=false]
\begin{pspicture}*(-5,-2)(5,2)
\SpecialCoor % For label positionning
\psaxes[labels=y,Dx=\pstPI2]{->}(0,0)(-5,-2)(5,2)
\uput[-90](!PI 0){$\pi$}  \uput[-90](!PI neg 0){$-\pi$}
\uput[-90](!PI 2 div 0){$\frac{\pi}2$} 
\uput[-90](!PI 2 div neg 0){$-\frac{\pi}2$}
\psplot[linewidth=1.5pt,linecolor=blue]{-5}{5}{x COS}
\psplot[linewidth=1.5pt,linecolor=red]{-5}{5}{x SIN}
\psplot[linewidth=1.5pt,linecolor=green]{-5}{5}{x TAN}
\end{pspicture}
%\end{LTXexample}


\begin{lstlisting}
\begin{pspicture}*(-5,-2)(5,2)
\SpecialCoor % For label positionning
\psaxes[labels=y,Dx=\pstPI2]{->}(0,0)(-5,-2)(5,2)
\uput[-90](!PI 0){$\pi$}  \uput[-90](!PI neg 0){$-\pi$}
\uput[-90](!PI 2 div 0){$\frac{\pi}2$} 
\uput[-90](!PI 2 div neg 0){$-\frac{\pi}2$}
\psplot[linewidth=1.5pt,linecolor=blue]{-5}{5}{x COS}
\psplot[linewidth=1.5pt,linecolor=red]{-5}{5}{x SIN}
\psplot[linewidth=1.5pt,linecolor=green]{-5}{5}{x TAN}
\end{pspicture}
\end{lstlisting}


\LPack{pst-math} introduces natural trigonometric postscript 
operators \Lps{ACOS}, \Lps{ASIN} and \Lps{ATAN} defined by

\[\DefOfOperator{acos}{[-1,1]}{[0,\pi]}\]
\[\DefOfOperator{asin}{[-1,1]}{[-\dfrac{\pi}2,\dfrac{\pi}2]}\]
\[\DefOfOperator{atan}{\mathbb R}{]-\dfrac{\pi}2,\dfrac{\pi}2[}\]

\begin{center}
\begin{tabular}{@{} >{\sffamily}l l >{\sffamily}l l @{} }
\emph{Stack} & \emph{Operator} & \emph{Result} & \emph{Description} \\\hline
num & \Lps{ACOS} & angle & Return \Index{arccosine} of \textsf{num} in radians \\
num & \Lps{ASIN} & angle & Return \Index{arcsine} of  \textsf{num} in radians \\
num & \Lps{ATAN} & angle & Return \Index{arctangent} of \textsf{num} in radians \\\hline
\end{tabular}
\end{center}

\medskip\noindent
\begin{tabularx}{\linewidth}{!{\vrule width3pt}X}
\Lps{ATAN} is \emph{not} defined as the already existing PS operator \Lps{atan}. \Lps{ATAN} needs only \emph{one}
argument on the stack.
\end{tabularx}

\begin{LTXexample}[pos=t,wide=false]
\begin{pspicture}(-5,-2)(5,4)
\SpecialCoor % For label positionning
\psaxes[labels=x,Dy=\pstPI2]{->}(0,0)(-5,-2)(5,4)
\uput[0](!0 PI){$\pi$}   \uput[0](!0 PI 2 div){$\frac{\pi}2$}
\uput[0](!0 PI 2 div neg){$-\frac{\pi}2$}
\psplot[linewidth=1.5pt,linecolor=blue]{-1}{1}{x ACOS} \psplot[linecolor=red]{-1}{1}{x ASIN}
\psplot[linewidth=1.5pt,linecolor=green]{-5}{5}{x ATAN}
\end{pspicture}
\end{LTXexample}

\section{Hyperbolic trigonometry}

\LPack{pst-math} introduces hyperbolic trigonometric postscript operators 
\Lps{COSH}, \Lps{SINH} and \Lps{TANH} defined by

\[\DefOfOperator{cosh}{\mathbb R}{[1,+\infty[}\]
\[\DefOfOperator{sinh}{\mathbb R}{\mathbb R}\]
\[\DefOfOperator{tanh}{\mathbb R}{]-1,1[}\]

\begin{center}
\begin{tabular}{@{} >{\sffamily}l l >{\sffamily}l l @{} }
\emph{Stack} & \emph{Operator} & \emph{Result} & \emph{Description} \\\hline
num & \Lps{COSH} & real & Return \Index{hyperbolic cosine} of \textsf{num} \\
num & \Lps{SINH} & real & Return \Index{hyperbolic sine} of \textsf{num} \\
num & \Lps{TANH} & real  & Return \Index{hyperbolic tangent} of \textsf{num}\\\hline
\end{tabular}
\end{center}

\begin{LTXexample}[pos=t,wide=false]
\begin{pspicture}*(-5,-5)(5,5)
\psaxes{->}(0,0)(-5,-5)(5,5)
\psplot[linewidth=1.5pt,linecolor=blue]{-5}{5}{x COSH}
\psplot[linewidth=1.5pt,linecolor=red]{-5}{5}{x SINH}
\psplot[linewidth=1.5pt,linecolor=green]{-5}{5}{x TANH}
\end{pspicture}
\end{LTXexample}

\LPack{pst-math} introduces reciprocal \Index{hyperbolic} trigonometric postscript operators \Lps{ACOSH}, \Lps{ASINH} and
\Lps{ATANH} defined by
\[\DefOfOperator{acosh}{[1,+\infty[}{\mathbb R}\]
\[\DefOfOperator{asinh}{\mathbb R}{\mathbb R}\]
\[\DefOfOperator{atanh}{]-1,1[}{\mathbb R}\]

\begin{center}
\begin{tabular}{@{} >{\sffamily}l l >{\sffamily}l l @{} }
\emph{Stack} & \emph{Operator} & \emph{Result} & \emph{Description} \\\hline
num & \Lps{ACOSH} & real & Return \Index{reciprocal hyperbolic cosine} of  \textsf{num} \\
num & \Lps{ASINH} & real & Return \Index{reciprocal hyperbolic sine} of \textsf{num} \\
num & \Lps{ATANH} & real & Return \Index{reciprocal hyperbolic tangent} of \textsf{num}\\\hline
\end{tabular}
\end{center}

\begin{LTXexample}[pos=t,wide=false]
\begin{pspicture}(-5,-4)(5,4)
\psaxes{->}(0,0)(-5,-4)(5,4)
\psplot[linewidth=1.5pt,linecolor=blue]{1}{5}{x ACOSH}
\psplot[linewidth=1.5pt,linecolor=red]{-5}{5}{x ASINH}
\psplot[linewidth=1.5pt,linecolor=green]{-.999}{.999}{x ATANH}
\end{pspicture}
\end{LTXexample}

\section{Other operators}

\LPack{pst-math} introduces postscript operator \Lps{EXP} defined by
\[\DefOfOperator{exp}{\mathbb R}{\mathbb R}\]

\begin{center}
\begin{tabular}{@{} >{\sffamily}l l >{\sffamily}l l @{} }
\emph{Stack} & \emph{Operator} & \emph{Result} & \emph{Description} \\\hline
num & \Lps{EXP} & real & Return \Index{exponential} of \textsf{num}\\\hline
\end{tabular}
\end{center}

\begin{LTXexample}[pos=t,wide=false]
\begin{pspicture}*(-5,-1)(5,5)
\psaxes{->}(0,0)(-5,-0.5)(5,5)
\psplot[linecolor=blue,linewidth=1.5pt,plotpoints=1000]{-5}{5}{x EXP}
\end{pspicture}
\end{LTXexample}

\LPack{pst-math} introduces postscript operator \Lps{GAUSS} defined by

\[\DefOfOperator*{gauss}{\mathbb R}{\mathbb R}{\displaystyle\frac{1}{\sqrt{2\pi\sigma^2}}\exp-\frac{(x-\overline x)^2}{2\sigma^2}}\]

\begin{center}
\begin{tabularx}{\linewidth}{@{} >{\sffamily}l l >{\sffamily}l X @{} }
\emph{Stack} & \emph{Operator} & \emph{Result} & \emph{Description} \\\hline
num1 num2 num3 & \Lps{GAUSS} & real & Return gaussian of \textsf{num1} with 
    mean \textsf{num2} and \Index{standard deviation} \textsf{num3}\\\hline
\end{tabularx}
\end{center}

\begin{LTXexample}[pos=t,wide=false]
\psset{yunit=5}
\begin{pspicture}(-5,-.1)(5,1.1)
\psaxes{->}(0,0)(-5,-.1)(5,1.1)
\psplot[linecolor=blue,linewidth=1.5pt,plotpoints=1000]{-5}{5}{x 2 2 GAUSS}
\psplot[linecolor=red,linewidth=1.5pt,plotpoints=1000]{-5}{5}{x 0 .5 GAUSS}
\end{pspicture}
\end{LTXexample}

\LPack{pst-math} introduces postscript operator \Index{SINC} defined by
\[\DefOfOperator*{sinc}{\mathbb R}{\mathbb R}{\displaystyle\frac{\sin x}x}\]

\begin{center}
\begin{tabular}{@{} >{\sffamily}l l >{\sffamily}l l @{} }
\emph{Stack} & \emph{Operator} & \emph{Result} & \emph{Description} \\\hline
num & \Lps{SINC} & real & Return \Index{cardinal sine} of \textsf{num} radians\\\hline
\end{tabular}
\end{center}

\begin{LTXexample}[pos=t,wide=false]
\psset{xunit=.25,yunit=3}
\begin{pspicture}(-20,-.5)(20,1.5)
\SpecialCoor % For label positionning
\psaxes[labels=y,Dx=\pstPI1]{->}(0,0)(-20,-.5)(20,1.5)
\uput[-90](!PI 0){$\pi$}   \uput[-90](!PI neg 0){$-\pi$}
\psplot[linecolor=blue,linewidth=1.5pt,plotpoints=1000]{-20}{20}{x SINC}
\end{pspicture}
\end{LTXexample}

\LPack{pst-math} introduces postscript operator \Lps{GAMMA} and \Lps{GAMMALN} defined by

\[\DefOfOperator*{\Gamma}{\mathbb{R} \backslash\mathbb{Z}}{\mathbb R}{\displaystyle\int_0^\infty t^{x-1}\mathrm e^{-t}\,\mathrm d t}\]
\[\DefOfOperator*{\ln\Gamma}{]0,+\infty[}{\mathbb R}{\ln\displaystyle\int_0^t t^{x-1}\mathrm e^{-t}\,\mathrm d t}\]

\begin{center}
\begin{tabular}{@{} >{\sffamily}l l >{\sffamily}l l @{} }
\emph{Stack} & \emph{Operator} & \emph{Result} & \emph{Description} \\\hline
num & \Lps{GAMMA}   & real & Return $\Gamma$\index{Gamma func@$\Gamma$ function} function of \textsf{num}\\
num & \Lps{GAMMALN} & real & Return \Index{logarithm} of $\Gamma$ function of \textsf{num}\\\hline
\end{tabular}
\end{center}

\begin{LTXexample}[pos=t,wide=false]
\begin{pspicture*}(-.5,-.5)(6.2,6.2)
\psaxes{->}(0,0)(-.5,-.5)(6,6)
\psplot[linecolor=blue,linewidth=1.5pt,plotpoints=200]{.1}{6}{x GAMMA}
\psplot[linecolor=red,linewidth=1.5pt,plotpoints=200]{.1}{6}{x GAMMALN}
\end{pspicture*}
\end{LTXexample}

\begin{LTXexample}[pos=t,wide=false]
\psset{xunit=.25,yunit=3}
\begin{pspicture}(-20,-.5)(20,1.5)
\psaxes[Dx=5,Dy=.5]{->}(0,0)(-20,-.5)(20,1.5)
\psplot[linecolor=blue,linewidth=1.5pt,plotpoints=1000]{-20}{20}{x BESSEL_J0}
\psplot[linecolor=red,linewidth=1.5pt,plotpoints=1000]{-20}{20}{x BESSEL_J1}
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[pos=t,wide=false]
\psset{xunit=.5,yunit=3}
\begin{pspicture}*(-1.5,-.75)(19,1.5)
\psaxes[Dx=5,Dy=.5]{->}(0,0)(-1,-.75)(19,1.5)
\psplot[linecolor=blue,linewidth=1.5pt,plotpoints=1000]{0.0001}{20}{x BESSEL_Y0}
\psplot[linecolor=red,linewidth=1.5pt,plotpoints=1000]{0.0001}{20}{x BESSEL_Y1}
%\psplot[linecolor=green,plotpoints=1000]{0.0001}{20}{x 2 BESSEL_Yn}
\end{pspicture}
\end{LTXexample}

\section{Numerical integration}

\begin{center}
\begin{tabular}{@{} >{\sffamily}l l >{\sffamily}l l @{} }
\emph{Stack} & \emph{Operator} & \emph{Result} & \emph{Description} \\\hline
num num /var $\lbrace$ function $\rbrace$ num & \Lps{SIMPSON} & real & Return $\displaystyle\int\limits_a^b f(t)\mathrm{d}t$\\
num 
\end{tabular}
\end{center}

%a b var f Ierr

the first two variables are the low and high boundary \Index{integral}, both can be values or
\PS expressions. \verb+/var+ is the definition of the integrated variable (not x!), which is
used in the following function description, which must be inside of braces. The last
number is the tolerance for the step adjustment. The function \Lps{SIMPSON} can be
nested.

\begin{LTXexample}[pos=t,wide=false]
\psset{xunit=.75}
\begin{pspicture*}[showgrid=true](-0.4,-3.4)(10,3)
  \psplot[linestyle=dashed,linewidth=1.5pt]{.1}{10}{1 x div}
  \psplot[linecolor=red,linewidth=1.5pt]{.1}{10}{
    1           % start 
    x           % end
    /t          % variable
    { 1 t div } % function
    .001        % tolerance 
    SIMPSON }   %
  \psplot[linecolor=blue,linewidth=1.5pt]{.1}{10}{1 x /t { 1 t div } 1 SIMPSON }
\end{pspicture*}
\end{LTXexample}

\begin{LTXexample}[pos=t,wide=false]
%%% Gaussian and relative integral from -x to x to its value sqrt{pi}
\psset{unit=2}
\begin{pspicture}[showgrid=true](-3,-1)(3,1)
  \psplot[linecolor=red,linewidth=1.5pt]{-3}{3}{Euler x dup mul neg exp }
  \psplot[linecolor=green,linewidth=1.5pt]{-3}{3}
     { x neg x /t { Euler t dup mul neg exp } .001 SIMPSON Pi sqrt div}
\end{pspicture}
\end{LTXexample}


\psset{unit=1.75cm}
%%% successive polynomial developments of sine-cosine
\begin{pspicture}[showgrid=true](-3,-2)(3,2)
\psaxes{->}(0,0)(-3,-2)(3,2)
\psset{linewidth=1.5pt}
\psplot[linecolor=green, algebraic=false, plotpoints=61, showpoints=true]
         {-3}{3}{0 x /tutu
                 {1 0 tutu /toto { toto } .1 SIMPSON sub} 
                    .01 SIMPSON }
\psplot[linecolor=blue, algebraic=false, plotpoints=61, showpoints=true]
         {-3}{3}{1 0 x /tata
                 {0 tata /tutu
                  {1 0 tutu /toto { toto } .1 SIMPSON sub}
                   .01 SIMPSON }
                    .01 SIMPSON  sub}
\psplot[linecolor=yellow, algebraic=false, plotpoints=61, showpoints=true]
         {-3}{3}{0 x /titi
                 {1 0 titi /tata
                  {0 tata /tutu
                   {1 0 tutu /toto { toto } .1 SIMPSON  sub}
                    .01 SIMPSON }
                     .01 SIMPSON  sub}
                      .01 SIMPSON }
\psplot[linecolor=red, algebraic=false, plotpoints=61, showpoints=true]
         {-3}{3}{1 0 x /tyty
                 {0 tyty /titi
                  {1 0 titi /tata
                   {0 tata /tutu
                    {1 0 tutu /toto { toto } .1 SIMPSON  sub}
                     .01 SIMPSON }
                      .01 SIMPSON  sub}
                       .01 SIMPSON }
                        .01 SIMPSON  sub}
\psplot[linecolor=magenta, algebraic=false, plotpoints=61, showpoints=true]
         {-3}{3}{0 x /tete
                 {1 0 tete /tyty
                  {0 tyty /titi
                   {1 0 titi /tata
                    {0 tata /tutu
                     {1 0 tutu /toto { toto } .1 SIMPSON  sub}
                      .01 SIMPSON }
                       .01 SIMPSON  sub}
                        .01 SIMPSON }
                         .01 SIMPSON  sub}
                          .01 SIMPSON }%%% FIVE nested calls
\end{pspicture}
\psset{unit=1cm}

\begin{lstlisting}
\psset{unit=1.75cm}
%%% successive polynomial developments of sine-cosine
\begin{pspicture}[showgrid=true](-3,-2)(3,2)
\psaxes{->}(0,0)(-3,-2)(3,2)
  \psplot[linecolor=green, algebraic=false, plotpoints=61, showpoints=true]
         {-3}{3}{0 x /tutu
                 {1 0 tutu /toto { toto } .1 SIMPSON sub} 
                    .01 SIMPSON }
  \psplot[linecolor=blue, algebraic=false, plotpoints=61, showpoints=true]
         {-3}{3}{1 0 x /tata
                 {0 tata /tutu
                  {1 0 tutu /toto { toto } .1 SIMPSON sub}
                   .01 SIMPSON }
                    .01 SIMPSON  sub}
  \psplot[linecolor=yellow, algebraic=false, plotpoints=61, showpoints=true]
         {-3}{3}{0 x /titi
                 {1 0 titi /tata
                  {0 tata /tutu
                   {1 0 tutu /toto { toto } .1 SIMPSON  sub}
                    .01 SIMPSON }
                     .01 SIMPSON  sub}
                      .01 SIMPSON }
  \psplot[linecolor=red, algebraic=false, plotpoints=61, showpoints=true]
         {-3}{3}{1 0 x /tyty
                 {0 tyty /titi
                  {1 0 titi /tata
                   {0 tata /tutu
                    {1 0 tutu /toto { toto } .1 SIMPSON  sub}
                     .01 SIMPSON }
                      .01 SIMPSON  sub}
                       .01 SIMPSON }
                        .01 SIMPSON  sub}
  \psplot[linecolor=magenta, algebraic=false, plotpoints=61, showpoints=true]
         {-3}{3}{0 x /tete
                 {1 0 tete /tyty
                  {0 tyty /titi
                   {1 0 titi /tata
                    {0 tata /tutu
                     {1 0 tutu /toto { toto } .1 SIMPSON  sub}
                      .01 SIMPSON }
                       .01 SIMPSON  sub}
                        .01 SIMPSON }
                         .01 SIMPSON  sub}
                          .01 SIMPSON }%%% FIVE nested calls
\end{pspicture}
\end{lstlisting}

\begin{LTXexample}[pos=t,wide=false]
% ce code definit la fonction [cos(2pix cos(t))-cos(2pix)]^2 / sin(t) avec x=h/lambda
\def\F{
  0.01 3.1 
  /t 
  { TwoPi x mul t COS mul COS TwoPi x mul COS sub 2 exp t SIN div }
   .01 SIMPSON  60 mul }
% D = 2*(cos^2(2pix))/F
\def\fD{TwoPi x mul COS dup mul 2 mul \F\space div}
\psset{llx=-1.5cm,lly=-0.5cm,urx=0.2cm,ury=0.2cm,
  xAxisLabel={$h/\lambda$},xAxisLabelPos={0.5,-45},yAxisLabel={$R_r$ en ohms},
  yAxisLabelPos={-0.1,150}}
\begin{psgraph}[Dy=50,Dx=0.1,xticksize=300 0,yticksize=1 0,
  comma=true,axesstyle=frame](0,0)(1,300){10cm}{5cm}
  \psplot{0}{1}{\F}
  \psplot[linecolor=red]{0.01}{.1}{\fD}%
\end{psgraph}
\end{LTXexample}


\clearpage

\section{Numerical functions}

\begin{center}
\begin{tabular}{@{} >{\sffamily}l l >{\sffamily}l l @{} }
\emph{Stack} & \emph{Operator} & \emph{Result} & \emph{Description} \\\hline
num  & \Lps{norminv} & real & Return $\mathop{norminv}(num)$\\
\end{tabular}
\end{center}


\begin{LTXexample}[width=5cm,wide=false]
\psset{xunit=5}
\begin{pspicture}(-0.1,-3)(1.1,4)
\psaxes{->}(0,0)(0,-3)(1.1,4)
\psplot[plotpoints=200,algebraic,linecolor=red]{0}{0.9999}{norminv(x)}
\end{pspicture}
\end{LTXexample}


These function returns the inverse normal.

\bgroup
\raggedright
\nocite{*}
\bibliographystyle{plain}
\bibliography{pst-math-doc}
\egroup

\printindex




\end{document}
