#!/bin/sh
#
# Ein Beispiel der DANTE-Edition
#
## Beispiel 06-11-7 auf Seite 125.
#
# Copyright (C) 2013 Herbert Voss
#
# It may be distributed and/or modified under the conditions
# of the LaTeX Project Public License, either version 1.3
# of this license or (at your option) any later version.
#
# See http://www.latex-project.org/lppl.txt for details.
#
# Running pdffonts on Linux
#
#CODEbegin
pdffonts 02-03-3.pdf
#CODEend
