#!/bin/sh
#
# Ein Beispiel der DANTE-Edition
#
## Beispiel 02-01-4 auf Seite 28.
#
# Copyright (C) 2013 Herbert Voss
#
# It may be distributed and/or modified under the conditions
# of the LaTeX Project Public License, either version 1.3
# of this license or (at your option) any later version.
#
# See http://www.latex-project.org/lppl.txt for details.
#
#
#CODEbegin
pdffonts 02-01-3.pdf
#CODEend
