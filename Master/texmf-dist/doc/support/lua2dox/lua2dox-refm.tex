\documentclass[a4paper]{article}
%\nofiles

\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{listings}
\input{lua.def} % not installed properly so just include here

%%
% h
\parindent=0bp
\textwidth=450bp
\oddsidemargin=0bp

% v
\parskip=.5em
\voffset=-70bp
\textheight=750bp


%%%%%%%%%%%%%%%%%
\def\sectionSprout(#1,#2){\vspace{#1}\penalty#2\vspace{-#1}}
\def\Sect{\sectionSprout(125bp,-150)\section}
\def\SSect{\sectionSprout(100bp,-100)\subsection}
\def\SSSect{\sectionSprout(80bp,-50)\subsubsection}

\def\BT{\begin{tabular}}
\def\ET{\end{tabular}}

\def\BE{\begin{enumerate}}
\def\EE{\end{enumerate}}
\def\IE{\item }

\def\BI{\begin{itemize}}
\def\EI{\end{itemize}}
\def\II{\item }

\long\def\Nerd#1{{\tt #1}}
\def\URL#1{{\tt #1}}

\def\LuaToDoX{Lua2D\raise-.3em\hbox{O}X$_{lua}$}
\def\luatodox{\Nerd{lua2dox\_lua}}
\def\Doxygen{\Nerd{Doxygen}}
\def\doxygen{\Nerd{doxygen}}
\def\To{$\rightarrow$}
\def\BS{$\backslash$}

\def\Foot#1{\footnote{#1}}

\lstset{language=C++
	,frame=single
	,numbers=left
	,numberstyle=\tiny
	,tabsize=2
	}

\hypersetup{
	pdfinfo={
		 Title={Lua2DoX lua ref manual}
		,Subject={Autodoc tool for Lua}
		}
	,colorlinks=true
	,linkcolor=blue
	}
	
\def\SetRefmanVerDate#1#2{%
	\def\RefmanVersion{#1}%
	\def\RefmanDate{#2}%
	}
\def\SetRefmanCopyright#1{%
	\def\RefmanCopyright{#1}%
	}
	
\SetRefmanCopyright{Copyright (c) 2012-13 Simon Dales}
\SetRefmanVerDate{0.2}{7th February 2013}


\begin{document}
\pagestyle{empty}
\begin{center}
	\huge \LuaToDoX
	
	\large
	\RefmanVersion
	
	\RefmanCopyright
	
	\RefmanDate

{\fboxsep=.5em\fboxrule=.1bp\fbox{%
\begin{minipage}{300bp}
\hyphenpenalty=10000%
\small
%\begin{abstract}
This is a hack to enable \doxygen{} to document lua.

It uses the well-known \doxygen{} filter mechanism to allow \doxygen{} to read extra computer languages.

If this works for you then enjoy.

%\end{abstract}
\end{minipage}
}}
\end{center}

\tableofcontents

\newpage
\pagestyle{headings}

\Sect{Introduction}

Documenting sourcecode is a hassle.
There are two basic methods of providing documented sourcecode: ``literate programming'' and ``autodoc''.
Without these tools a human has to keep the documentation and code aligned, which rarely happens.

\SSect{Literate Programming}
	
If one is sufficiently organised, and it fits in with your existing work practices, then there is
a lot going for literate programming.
Here you write a combined document that contains both code and documentation fragments.
When you want code or documentation you run it through a program which makes the appropriate files.

This is theoretically the neatest way of making code because no code is generated without documentation.
The best known example of literate programming is the source of \TeX.

\SSect{Autodoc}

In the real world we get source from many sources, usually where literate programming wasn't used or isn't available.

One solution is to use an automatic documentation tool:
feed it sourcecode and it will extract what documentation it can.

If you feed it raw source then all it can do is tell you where it found features such as function and class declarations.

If your source has been suitably prepared, then more detailed documentation can be produced.
This preparation takes the form of ``magic comments''.
These are source comments that contain ``magic'' features that let the autodoc tool know to make use of them.

\SSSect{Doxygen}

\Doxygen{} is one of the many autodoc tools use to document C-like languages.
It is widely used and supported in the linux world and has Mac OS X and Windows ports.

\begin{lstlisting}[firstnumber=83]
void helloWorldOnce(){
	printf("hello world\n");
	}
/*!
	\brief writes hello world *n

	This is a demo of how to loop round
	and print something

	\param N number of times round 
*/
void helloWorldN(int N){
	for (int i=0;i<N;i++)
		printf("hello world %d\n",i);
	}
\end{lstlisting}

\hspace{-.35em}{\fboxsep=.5em\fboxrule=.1bp\fbox{%
\begin{minipage}{\textwidth}
{\sf void {\bfseries helloWorldN}(int N)}

{\itshape writes hello world *n}

This is a demo of how to loop round
and print something

{\bfseries Parameters:}

\hspace*{1em}{\itshape N} number of times round

Definition at line {\bfseries  94} of file {\bfseries hello.cpp}.

\vskip1em
{\sf void {\bfseries helloWorldOnce}()}

Definition at line {\bfseries  83} of file {\bfseries hello.cpp}.

\end{minipage}\hspace{-.35em}%
}%
}\hspace*{-10em}\rule{0bp}{0bp}%

\SSSect{\LuaToDoX}

In the case of \doxygen{}, which is intended for C-like languages, it expects `\Nerd{///}' or `\Nerd{//!}' for C++ comments and `\Nerd{/**}\ldots\Nerd{*/}'/`\Nerd{/*!}\ldots\Nerd{*/}' for C-style comments.
For \LuaToDoX{} we recycle this convention and use `\Nerd{-{}-!}' and `\Nerd{-{}-[[!}\ldots\Nerd{]]}'.

The internal format of the magic comments is identical to \doxygen.
This is not suprising in that \Nerd{lua2dox\_filter} is a \doxygen{} filter.
What it does is to read lua code and output something not entirely unlike C++.

This lets us make Doxygen output some documentation to let
us develop this code.

\begin{lstlisting}[firstnumber=83,language=lua]
function helloWorldOnce()
	io.write("hello world\n")
end
--[[!
	\brief writes hello world *n

	This is a demo of how to loop round
	and print something

	\param N number of times round 
	]]
function helloWorldN(N)
	for i=0,N do
		io.write("hello world" .. i .. "\n")
	end
end
\end{lstlisting}

\hspace{-.35em}{\fboxsep=.5em\fboxrule=.1bp\fbox{%
\begin{minipage}{\textwidth}
{\sf function {\bfseries helloWorldN}(N)}

{\itshape writes hello world *n}

This is a demo of how to loop round
and print something

{\bfseries Parameters:}

\hspace*{1em}{\itshape N} number of times round

Definition at line {\bfseries  94} of file {\bfseries hello.lua}.

\vskip1em
{\sf function {\bfseries helloWorldOnce}()}

Definition at line {\bfseries  83} of file {\bfseries hello.lua}.

\end{minipage}\hspace{-.35em}%
}%
}\hspace*{-10em}\rule{0bp}{0bp}%

\SSect{Origins}

\LuaToDoX{} inspired by the functionality of a Perl module called ``\Nerd{lua2dox}''%
\Foot{\URL{http://search.cpan.org/\~{}alec/Doxygen-Lua-0.02/lib/Doxygen/Lua.pm}}.
Found on CPAN when looking for something else; kinda handy.

Improved from \Nerd{lua2dox} to make the \doxygen{} output more friendly.
Runs faster in lua than in Perl.

Because this Perl based system is called ``\Nerd{lua2dox}'', I have decided to add ``\Nerd{\_lua}'' to the name to keep the two separate, so it is called ``\luatodox''.

\newpage
\Sect{Installation}
\SSect{System requirements}

Should run on any reasonably modern computer (PC or Mac OS X).

Requires a reasonably recent: \doxygen{} and \Nerd{luatex} or \Nerd{lua}.
\BI
\II \doxygen{} $\ge$ 1.8.1
\II \Nerd{luatex} $\ge$ 0.6 / \Nerd{lua} 5.1
\EI

\SSSect{Luatex/lua}


Whilst \Nerd{luatex} isn't strictly required to make it run; \TeX{} users
are more likely to have a recent version (via \Nerd{texlive}) of \Nerd{texlua}
rather than relying on your, possibly aging, distro's lua.

It should work with either \Nerd{texlua} or \Nerd{lua}.
The calling bash script determines which one is available (\Nerd{texlua} then \Nerd{lua}) and selects that one.

\SSSect{\doxygen}

Install this using whatever mechanism you usually use.

It is also helpful if you know how to use \doxygen.

\SSect{Linux/other Unices}

Only tested for linux, but should work for any Unix-like OS. 

For other OS you need to figure it out yourself.
If you do, then please email me with the code/suggestions.

\BE
\IE make an appropriatly named temporary directory (``\Nerd{tmp}'').
\IE \Nerd{cd tmp}
\IE untar into this directory.

\IE manually check if lua2dox.lua runs on your system.

From the commandline run ``\Nerd{./lua2dox\_filter -{}-help}''.
If it runs and produces a sensible result then it probably works.

\IE Manually pick an appropriate directory for this system.

Make this new directory and copy/mv all these files into there.

If you are installing it as a personal app then put it somewhere in your
userspace. For system-wide: ask your sysadmin.

In this example called ``\Nerd{$\sim$/programs/lua2dox}''.
\IE ``\Nerd{cd $\sim$/programs/lua2dox}''.

\IE Run ``\Nerd{./install.sh}''.
This makes the links to the active code.

Default is to put in `\Nerd{$\sim$/bin}''. For system-wide: ask your sysadmin.
\EE

\SSect{Mac OS X}

Being a unix-like OS then install as for Linux.

\SSect{Windows}

As for unix but use the .BAT files.

\newpage
\Sect{Useage}

Because this is a \doxygen{} filter then it is assumed that the endusers are familiar with
the use of this app.
When familiar then one can move on to documenting lua code.

\SSect{Learn to use \Doxygen}

Ensure \doxygen{} is installed on your system and that you are familiar with its use.
Best is to try to make and document some simple C/C++/PHP to see what it produces.

Included with this distribution are some examples of php and lua code.
They are two simple programs that create some classes and then show their use.
The documentation should be nearly the same as each other.

\SSect{running \LuaToDoX}
\SSSect{make Doxyfile}
Run ``\Nerd{doxygen -g}'' to create a default \Nerd{Doxyfile}.
This is the configuration file that \doxygen{} needs for making the documentation.

	Then alter it to let it recognise lua. Add the following lines:

\begin{lstlisting}[language=bash,numbers=none]
FILE_PATTERNS     =  *.lua
FILTER_PATTERNS   =  *.lua=lua2dox_filter
\end{lstlisting}


You might also like to edit some other entries:

\begin{lstlisting}[language=bash,numbers=none]
PROJECT_NAME      =  <name of project>
PROJECT_NUMBER    =  <version number>
OUTPUT_DIRECTORY  =  docs
SOURCE_BROWSER    =  yes
GENERATE_LATEX    =  no
\end{lstlisting}

For example:

\begin{lstlisting}[language=bash, numbers=none]
PROJECT_NAME      =  "A Test Project"
PROJECT_NUMBER    =  "0.1 20120926a"
\end{lstlisting}

Either add them to the end or find/edit the appropriate entry in \Nerd{Doxyfile}.

\SSSect{run \doxygen}

Once \Nerd{Doxyfile} has been edited run \doxygen{} and \LuaToDoX{} will be called as a filter.

% When reading source with classes multiple passes are needed.
% Each pass generates a list of member functions (as a file) that were found on this pass.
% This list is read in on the next pass.
% If the class+methods haven't changed this time then you only need to run it once, else run twice,
% much like running \LaTeX{} \Nerd{$\backslash$tableofcontents}.

\SSect{In use}

As with \doxygen{} the HTML output is the most useful for development.
When the project is finished then PDF output (via \LaTeX) may be useful.

Typically you will have some source files open in your editor/IDE.
Also keep a web-browser window open pointing at the documentation.

After a bit of editing the documentation will become stale.
At this point run \doxygen{} and refresh the HTML pages.

\newpage
\Sect{Design}

\LuaToDoX{} is a \doxygen{} filter.
It has a basic parser that will read each line of lua and output an equivalent in pseudo-C++.
It loops round until it has found the end of the file.
It only has to be good enough for \doxygen{} to see it as legal.
Therefore our lua interpreter is fairly limited, but ``good enough''.

The parser is relatively crude and assumes that the programmer writes lua in a fairly
clean way.
Some bits of lua will confuse it.

In order for the source browser to work in the \doxygen{} output it must preserve the number of lines.
The output of each line is either written as-is or a converted line.
When \doxygen{} re-reads the source to make the source-HTML/\LaTeX{}/\ldots it uses the line number
to set the position.


It only supports comments that preceded the function/class declaration.
Any in the middle will act as cruft in the resulting documentation.
This will be slightly out of place but at least should refer to somewhere near to
where it was intended.
In particular ``\Nerd{\BS todo}''. This will appear in the ToDo list, but not associated with the right function.

\SSect{Classes}
Class declarations are assumed to be using a ``well-known'' userdefined function ``\Nerd{class()}''.
It processes\goodbreak``\Nerd{TAA = class(TA)}''$_{lua}$ \To{} ``\Nerd{class TAA: public TA\{\}};''$_{C++}$.

% However it will probably have some member functions.
% These get converted from \goodbreak''\Nerd{A.foo(a,b)}''$_{lua}$ \To{} ``\Nerd{A::foo(a,b)}''$_{C++}$.
% This is stored in a temporary file.
% When \LuaToDoX{} is run a subsequent time this is used to generate a list of methods.
% So \goodbreak``\Nerd{AA = class(A)}''$_{lua}$ \To{} ``\Nerd{class AA: public A\{foo(a,b);bar(x);\}};''$_{C++}$

\SSSect{Methods}

Methods are mangled as: ``\Nerd{TA.fred(this,A)}''$_{lua}$ to ``\Nerd{/*! \BS memberof TA */ fred(this,A)}''$_{C++}$.
This is a \doxygen{} feature introduced in 1.8.1.
It allows the filter to work in a single pass.

\SSect{Multiline function declarations}

Because \LuaToDoX{} can only process one line at a time it cannot be guaranteed to correctly process function declarations
with multiline parameter list.
This is because the parser isn't sophisticated enough to guarantee finding the close paren on some random later line.

So I have put in a hack that will insert the ``missing'' close paren.
It looks at the function declaration that you have got and checks for a close paren.
It if is not there then it will add a spoof parameter and the close.

The effect is that you will get the function documented, but not with the parameter list you might expect.

\begin{lstlisting}[firstnumber=94]
function foo(A
	,B
	,C)
\end{lstlisting}

Becomes
\begin{lstlisting}[firstnumber=94]
function foo(A,___MissingCloseParenHere___)
\end{lstlisting}

\SSect{Multiple magic comments}

The hacking of lua to C++ by \LuaToDoX{} has the limitation that some magic comments will not get associated with the correct function/class. This is a ``feature'' that might get correct in a later version.

\newpage
\Sect{ChangeLog}
\input{ChangeLog}

\end{document}

%%eof