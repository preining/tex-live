---- Copyright 2012 Simon Dales
--
-- This work may be distributed and/or modified under the
-- conditions of the LaTeX Project Public License, either version 1.3
-- of this license or (at your option) any later version.
-- The latest version of this license is in
--   http://www.latex-project.org/lppl.txt
--
-- This work has the LPPL maintenance status `maintained'.
-- 
-- The Current Maintainer of this work is Simon Dales.
--

--[[!
	\file
	\brief test some classes
	
	]]
		
require 'class'

--! \brief write to stdout
function TIO_write(Str)
	if Str then
		io.write(Str)
	end
end

--! \brief writeln to stdout
function TIO_writeln(Str)
	if Str then
		io.write(Str)
	end
	io.write('\n')
end

--! \class Animal
--! \brief a base class
Animal = class()

--! \brief constructor
function Animal.init(this)
	this:setKind('animal')
end

--! \brief set kind of object
function Animal.setKind(this,Kind)
	this.kind = Kind
end

--! \brief say the call of this animal
function Animal.call(this)
	local speigel = this.speigel
	if speigel then
		speigel = ' says "' .. speigel .. '"'
	else
		speigel = ' <undefined call>'
	end
	
	TIO_writeln(this.kind .. speigel)
end

--! \brief an abstract bird
Bird = class(Animal)

--! \brief constructor
function Bird.init(this)
	Animal.init(this)
	this:setKind('bird')
end

--! \brief a subclassed bird
Pigeon = class(Bird)

--! \brief constructor
function Pigeon.init(this)
	Bird.init(this)
	this:setKind('pigeon')
	this.speigel = 'oh my poor toe Betty'
end

--! \brief another subclassed bird
RedKite = class(Bird)

--! \brief constructor
function RedKite.init(this)
	Bird.init(this)
	this:setKind('red kite')
	this.speigel = 'weee-ooo ee oo ee oo ee oo'
end

--! \brief a base mammal
Mammal = class(Animal)

--! \brief constructor
function Mammal.init(this)
	Animal.init(this)
end

--! \brief a subclassed mammal
Cat = class(Mammal)

--! \brief constructor
function Cat.init(this)
	Mammal.init(this)
	this:setKind('cat')
	this.speigel = 'meow'
end

--! \brief another subclassed mammal
Dog = class(Mammal)

--! \brief constructor
function Dog.init(this)
	Mammal.init(this)
	this:setKind('dog')
	this.speigel = 'woof'
end

--eof