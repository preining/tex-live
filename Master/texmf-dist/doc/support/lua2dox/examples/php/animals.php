<?php
/***** Copyright 2012 Simon Dales
**
** This work may be distributed and/or modified under the
** conditions of the LaTeX Project Public License, either version 1.3
** of this license or (at your option) any later version.
** The latest version of this license is in
**   http://www.latex-project.org/lppl.txt
**
** This work has the LPPL maintenance status `maintained'.
** 
** The Current Maintainer of this work is Simon Dales.
*/

/*!
	\file
	\brief test some classes
	
	Here we make some animals
	
	*/
	

//! \brief write to stdout
function TIO_write($Str)
{
	printf('%s',$Str);
}

//! \brief writeln to stdout
function TIO_writeln($Str)
{
	printf("%s\n",$Str);
}

//! \brief a base class
class Animal{
	function Animal(){ //! \brief constructor
		$this->setKind('animal');
		}
	function setKind($Kind){ //! \brief set kind of object
		$this->kind = $Kind;
		}
	function call(){ //! \brief say the call of this animal
		$speigel = $this->speigel;
		if (isset($speigel))
			$speigel = ' says "' . $speigel . '"';
		else
			$speigel = ' <undefined call>';
	
		TIO_writeln($this->kind . $speigel);
		}

};

//! \brief an abstract bird
class Bird extends Animal{
	function Bird(){ //! \brief constructor
		$this->setKind('bird');
		}
};

//! \brief a subclassed bird
class Pigeon extends Bird{
	function Pigeon(){ //! \brief constructor
		$this->setKind('pigeon');
		$this->speigel = 'oh my poor toe Betty';
		}
};

//! \brief another subclassed bird
class RedKite extends Bird{
	function RedKite(){ //! \brief constructor
		$this->setKind('red kite');
		$this->speigel = 'weee-ooo ee oo ee oo ee oo';
		}
};

//! \brief a base mammal
class Mammal extends Animal{
};

//! \brief a subclassed mammal
class Cat extends Mammal{
	function Cat(){ //! \brief constructor
		$this->setKind('cat');
		$this->speigel = 'meow';
		}
};

//! \brief another subclassed mammal
class Dog extends Mammal{
	function Dog(){ //! \brief constructor
		$this->setKind('dog');
		$this->speigel = 'woof';
		}
};

//eof
?>