@echo off

  if [%1]==[-h]     goto :help
  if [%1]==[--help] goto :help
  
  goto :init

:help

  echo Syntax:
  echo install [^<dir^>]
  echo.

:init

  rem Avoid clobbering anyone else's variables

  setlocal

  rem Safety precaution against awkward paths

  cd /d "%~dp0"
  
  rem Set the install location, using the same logic as the .sh file
  
  set INSTALLDIR=%1
  set DEFAULT_INSTALLDIR=%USERPROFILE%\bin
  
  if [%INSTALLDIR%] == {] (
    set INSTALLDIR=%DEFAULT_INSTALLDIR%
  )
  
  set APP_NAME=lua2dox_filter.bat
  
  goto :install
  
:install

  echo Install to %INSTALLDIR%
  copy /y %APP_NAME% %INSTALLDIR% > nul
  echo Install done