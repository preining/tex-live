#!/bin/sh

###########################################################################
#   Copyright (C) 2012 by Simon Dales   #
#   simon@purrsoft.co.uk   #
#                                                                         #
#   This program is free software; you can redistribute it and/or modify  #
#   it under the terms of the GNU General Public License as published by  #
#   the Free Software Foundation; either version 2 of the License, or     #
#   (at your option) any later version.                                   #
#                                                                         #
#   This program is distributed in the hope that it will be useful,       #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#   GNU General Public License for more details.                          #
#                                                                         #
#   You should have received a copy of the GNU General Public License     #
#   along with this program; if not, write to the                         #
#   Free Software Foundation, Inc.,                                       #
#   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
###########################################################################

INSTALLDIR="$1"
####################
HOME="/home/${USER}"
DEFAULT_INSTALLDIR="${HOME}/bin"

if test -z "${INSTALLDIR}"
then
	INSTALLDIR="${DEFAULT_INSTALLDIR}"
fi

APP_NAME="lua2dox_filter"
############################
go_splat(){
	echo "ERROR!: $1"
	exit 1
	}
	
mk_link(){
	echo "mk \"$1\" -> \"$2\""
	ln -s "$1" "$2" || go_splat "creating link: \"$1\" -> \"$2\""
	}
	
do_install(){
	echo "install to \"${INSTALLDIR}\""
	
	CURRENT_DIR=`pwd`
 	APP_PATH="${CURRENT_DIR}/${APP_NAME}"
	echo "app path=\"${APP_PATH}\""
	cd "${INSTALLDIR}"
	
	mk_link "${APP_PATH}" "lua2dox_filter"
	
	echo "install done."
	}

## main
case "${INSTALLDIR}" in
--help)
	echo "Syntax:"
	echo "$0 <dir>|<help>"
	echo ""
	echo "--help show this help"
	echo "<dir> install directory for links"
	echo "default <dir>=\"${DEFAULT_INSTALLDIR}\""
	;;
*)
do_install
	;;
esac
##eof