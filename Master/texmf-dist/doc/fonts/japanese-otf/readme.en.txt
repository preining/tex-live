package name: otf or japanese-otf

This is the file readme.en.txt written by Norbert Preining for the
upload to CTAN.

This package is originally named "otf", but for the sake of not
choosing to general names on CTAN (and in TeX Live) it will be
called "japanese-otf". This change has been discussed with
the author of the package, Saito Shuzaburo. The version of
the package used is 
	otfbeta.zip 2012/1/22 v1.7b4

The original source has been downloaded from
	http://psitau.kitunebi.com/otf.html
as
	http://psitau.kitunebi.com/otfbeta.zip

This package provides advanced typesetting options for platex and 
friends.

After downloading the script makeotf has been run and the generated
vf/tfm/ofm files have been included in the upload to CTAN.

Furthermore, the map file as used in TeX Live was added in the directory
TeXLive-maps.

The contents of this package is under the license given in the 
COPYRIGHT file, which is more or less BSD license.

The map files in this package are originally written by Hironori Kitagawa
and in public domain.

Norbert Preining
2012-01-06
