\documentclass[12pt]{article}
\usepackage{a4,mxedruli,xucuri,tipa}
\usepackage{times}

% Designed by Johannes Heinecke                                             %
%             <johannes.heinecke@wanadoo.fr>                                %
% This software is under the LaTeX Project Public License                   %

\newif\ifpdf\ifx\pdfoutput\undefined\pdffalse\else\pdftrue\fi

\ifpdf
\usepackage[pdftex,colorlinks=true,
  urlcolor=urlcol, % URL: http://
  citecolor=bibcol, % bibTeX
  linkcolor=linkcol, % sections, footnotes, ...
  pagecolor=linkcol, % links to other pages
  filecolor=filecol, % URL: file:/
  pdftitle={The Georgian Alphabets},
  pdfauthor={Johannes Heinecke},
  pdfsubject={},pdfkeywords={},pagebackref,
  pdfpagemode=None,bookmarksopen=true]{hyperref}
 \usepackage{color}
 \definecolor{linkcol}{rgb}{0.75,0,0}
 \definecolor{bibcol}{rgb}{0,0.5,0}
 \definecolor{urlcol}{rgb}{0,0,0.75}
 \definecolor{filecol}{rgb}{0,0,0.75}
 \urlstyle{same}

\else
\fi


\parskip7mm
\parindent 0pt
\addtolength{\topmargin}{-1ex}
\addtolength{\textheight}{2ex}

\font\logo=logo10 scaled \magstep1
\font\logogr=logobf10 scaled \magstep3
%\font\phon=wsuipa12

\title{{\mxedc mxedruli} --- \it Mxedruli,\\ 
       {\xucr XUCURI, xucuri} --- Xucuri\\
The Georgian Alphabets\thanks{With improvements by 
Jan De Lameillieure, Berlin and Mark Leisher, Las Cruces, New Mexico}}

\author{Johannes Heinecke (\href{mailto:johannes.heinecke@wanadoo.fr}{johannes.heinecke@wanadoo.fr})\\
Lannion, France}


\date{Version 3.3c, 18th January 2009}

\begin{document}
\maketitle

\thispagestyle{empty}

\section{Introduction}
This is a short documentation of the two alphabets
used by Georgian and some of its neighbouring languages from
the Kartvelian language family. The first alphabet is called 
{\it Mxedruli}.  Some letters used by Old Georgian or 
other languages such as Ossetian are also included.
The second alphabet is called {\it Xucuri\/}. Whereas {\it Mxedruli\/} 
does differentiate majuscules and minuscules, {\it Xucuri\/} distinguishes
between majuscules (also called {\it Mrg(v)lovani\/}) and minuscules
({\it \d Kutxovani\/}). However, in opposition to the Roman, Greek and
Cyrillic alphabets in a text either majuscules or minuscules are
used. They cannot be combined. {\it Xucuri\/} is now restricted to
religious use.

It is implemented using {\logo METAFONT} and can be used in
\LaTeX\ or \TeX-documents.
The font are of a rather simple design (cf. section \ref{Examples}) and
surely may be improved.  I would be very happy if any suggestions,
improvements, corrections, extensions, enhancements etc.  could be
forwarded to me\footnote{By e-mail: \href{mailto:johannes.heinecke@wanadoo.fr}{johannes.heinecke@wanadoo.fr}.  I try to realize suggestions etc. as
quick as possible. So please check my WWW-Homepage for the newest
$\beta$-release: \url{http://pagesperso-orange.fr/heinecke/mxedruli/}}.  Thank
you very much!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% USAGE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The fonts and their usage}
\subsection{\emph{Mxedruli}}
The tables below show the names of the {\it Mxedruli\/} letters,
followed by the letter in normal and bold shape and in a
``capital''\footnote{As there are no capital letters in {\it mxedruli\/}
the letters are scaled to the same height for headlines etc.} mode.
Within a \LaTeX-document they can be activated by \verb+\mxedr+
(normal), \verb+\mxedb+ (bold)
\verb+\mxedi+ (italics) or \verb+\mxedc+
analogously to standard \LaTeX\ font commands such as \verb+\rm+,
\verb+\bf+, \verb+\it+ or \verb+\sc+ respectively. Therefore the
\verb|mxedruli.sty| stylefile must be loaded for standard font
selection and NFSS2.\footnote{NFSS1 is supported too. 
Due to the lack of NFSS1 however, I
could not test, whether the NFSS1 routines work satisfactorily.}.
Please refer to section
\ref{Install} in order to install the fonts. They are provided for
300dpi printers, if you need different solutions section \ref{MF}
describes how you can produce the {\tt .pk}-files required.

Within \LaTeX\ the font size commands have also effect on the {\it
Mxedruli\/} letters.  Finally, in the last column, the required
\LaTeX\ or \TeX-input to produce the {\it Mxedruli\/} letter is shown.


(As I designed some new letters I had to reposition the
`{\mxedr .+c}' (\verb-.+c-) from its original position\footnote{As
coded in versions anterior to 2.1} {\it '171\/}
to {\it '014\/}. On {\it '171\/} ({\it y\/}) the new
letter `{\mxedr y}' ({\it schwa\/}) is installed now. This is
mainly important for those users, who address the letters
directly by using the \TeX\ \verb+\char+{\it nnn\/} or the
\LaTeX\ \verb+\symbol{+{\it nnn\/}\verb+}+ commands respectively.
If you address the letter as described here, nothing has changed
compared to previous versions.)

For those (Kartvelian) languages, which use {\it Mxedruli\/} together
with diacritics\footnote{Sometimes also used in Kartvelian
dialectology or descriptive linguistics of Kaukasian languages in
Georgian, in order to provide a phonetic transcription without recurring to the
Internal Phonetic Alphabet.} an umlaut (`\char127'), a circumflex (`\char94')
and a macron (`\char22') have
been defined. They can be used as in normal \LaTeX-documents by the
\verb+\"+{\it char\/} (umlaut on {\it char\/}), \verb+\^+{\it char\/} (circonflex)
 or \verb+\=+{\it char\/} (macron)
command respectively, e.g. \verb+{\mxedr \"o}+ yields `{\mxedr \"o}',
\verb+{\mxedr+ \verb+\^a}+ yields `{\mxedr \^a}', 
and \verb+{\mxedr \=u}+ yields `{\mxedr \=u}'.

The table in sections \ref{mxedletters} and \ref{xucletters}
contains in the leftmost column the corresponding 
Unicode-code\footnote{Cf. \url{http://www.unicode.org/charts/}.} Please note that the
letter-names do slightly differ to the names used in the Unicode name table. This is due
to different conventions.


\vfill
\subsubsection{Standard letters}\label{mxedletters}

\begin{center}
\begin{tabular}{|l |c |c |c |c |c |c|c|}
\hline
           & \multicolumn{4}{c|}{\it Mxedruli} &  &  &\\
\cline{2-5}
\raisebox{1.5ex}[1.5ex]{Name}  & normal  & bold  & italics & capital &
\raisebox{1.5ex}[1.5ex]{Transcription} & \raisebox{1.5ex}[1.5ex]{input} & \raisebox{1.5ex}[1.5ex]{Unicode} \\
\hline\hline
an         & \mxedr a   & \mxedb a   & \mxedi a   & \mxedc a   & a       & \verb|a| & 10D0  \\ \hline
ban        & \mxedr b   & \mxedb b   & \mxedi b   & \mxedc b   & b       & \verb|b| & 10D1  \\ 
\hfill \it or & \mxedr b1   & \mxedb b1  &  \mxedi b1   &   &     & \verb|b1| &  \\ \hline
gan        & \mxedr g   & \mxedb g   & \mxedi g   & \mxedc g   & g       & \verb|g| & 10D2 \\ \hline
don        & \mxedr d   & \mxedb d   & \mxedi d   & \mxedc d   & d       & \verb|d| & 10D3 \\ \hline
en         & \mxedr e   & \mxedb e   & \mxedi e   & \mxedc e   & e       & \verb|e| & 10D4 \\ \hline
vin        & \mxedr v   & \mxedb v   & \mxedi v   & \mxedc v   & v       & \verb|v| & 10D5 \\ \hline
zen        & \mxedr z   & \mxedb z   & \mxedi z   & \mxedc z   & z       & \verb|z| & 10D6  \\ \hline
tan        & \mxedr t   & \mxedb t   & \mxedi t   & \mxedc t   & t       & \verb|t| & 10D7  \\ \hline
in         & \mxedr i   & \mxedb i   & \mxedi i   & \mxedc i   & i       & \verb|i| & 10D8  \\ \hline
\d kan     & \mxedr .k  & \mxedb .k  & \mxedi .k  & \mxedc .k  & \d k    & \verb|.k| & 10D9 \\ \hline
las        & \mxedr l   & \mxedb l   & \mxedi l   & \mxedc l   & l       & \verb|l|  & 10DA \\ \hline
man        & \mxedr m   & \mxedb m   & \mxedi m   & \mxedc m   & m       & \verb|m|  & 10DB \\ \hline
nar        & \mxedr n   & \mxedb n   & \mxedi n   & \mxedc n   & n       & \verb|n|  & 10DC \\ \hline
on         & \mxedr o   & \mxedb o   & \mxedi o   & \mxedc o   & o       & \verb|o|  & 10DD \\ \hline
\.par      & \mxedr .p  & \mxedb .p  & \mxedi .p  & \mxedc .p  & \.p     & \verb|.p| & 10DE \\ \hline
\v zan     & \mxedr +z  & \mxedb +z  & \mxedi +z  & \mxedc +z  & \v z    & \verb|+z| & 10DF \\ \hline
ran        & \mxedr r   & \mxedb r   & \mxedi r   & \mxedc r   & r       & \verb|r| & 10E0  \\ \hline
san        & \mxedr s   & \mxedb s   & \mxedi s   & \mxedc s   & s       & \verb|s|  & 10E1 \\ \hline
\d tar     & \mxedr .t  & \mxedb .t  & \mxedi .t  & \mxedc .t  & \d t    & \verb|.t| & 10E2 \\ \hline
un         & \mxedr u   & \mxedb u   & \mxedi u   & \mxedc u   & u       & \verb|u|  & 10E3 \\ \hline
par        & \mxedr p   & \mxedb p   & \mxedi p   & \mxedc p   & p       & \verb|p|  & 10E4 \\ \hline
kan        & \mxedr k   & \mxedb k   & \mxedi k   & \mxedc k   & k       & \verb|k|  & 10E5 \\ \hline
\.gan      & \mxedr .g  & \mxedb .g  & \mxedi .g  & \mxedc .g  & \.g     & \verb|.g| & 10E6 \\ \hline
\.qar      & \mxedr q   & \mxedb q   & \mxedi q   & \mxedc q   & \.q     & \verb|q|\footnotemark & 10E7  \\ \hline
\v san     & \mxedr +s  & \mxedb +s  & \mxedi +s  & \mxedc +s  & \v s    & \verb|+s| & 10E8 \\ \hline
\v cin     & \mxedr +c  & \mxedb +c  & \mxedi +c  & \mxedc +c  & \v c    & \verb|+c| & 10E9 \\ \hline
can        & \mxedr c   & \mxedb c   & \mxedi c   & \mxedc c   & c       & \verb|c|  & 10EA \\ \hline
dzil       & \mxedr j   & \mxedb j   & \mxedi j   & \mxedc j   & j/dz    & \verb|j|  & 10EB \\ \hline
\d cil     & \mxedr .c  & \mxedb .c  & \mxedi .c  & \mxedc .c  & \d c    & \verb|.c| & 10EC \\ \hline
\d{\v c}ar & \mxedr .+c & \mxedb .+c & \mxedi .+c & \mxedc .+c & \d{\v c}& \verb|.+c| & 10ED \\ \hline
xan        & \mxedr x   & \mxedb x   & \mxedi x   & \mxedc x   & x       & \verb|x| & 10EE  \\ \hline
d\v zan    & \mxedr +j  & \mxedb +j  & \mxedi +j  & \mxedc +j  & d\v z   & \verb|+j| & 10EF \\ \hline
ha         & \mxedr h   & \mxedb h   & \mxedi h   & \mxedc h   & h       & \verb|h|  & 10F0 \\ \hline
\end{tabular}
\end{center}

\footnotetext{Although the transcription of `{\mxedr q}' is {\it \.q\/} the
\TeX-input will remain `{\tt q}' for the letter {\it \.qar\/} as
it appears with a far more frequent than {\it qar\/} (`{\mxedr q1}')
which is coded as `{\tt q1}'.}

\subsubsection{Other letters}

\begin{center}
\begin{tabular}{|l |c |c |c |c |c |c| c|}
\hline
           & \multicolumn{4}{c|}{\it Mxedruli} &  &  & \\
\cline{2-5}
\raisebox{1.5ex}[1.5ex]{Name}  & normal  & bold  & italics & capital &
\raisebox{1.5ex}[1.5ex]{Transcription} & \raisebox{1.5ex}[1.5ex]{input} & \raisebox{1.5ex}[1.5ex]{Unicode} \\ \hline\hline
qar        & \mxedr q1  & \mxedb q1  & \mxedi q1  & \mxedc q1  & q       & \verb|q1| & 10F4 \\ \hline
h\=e       & \mxedr e0  & \mxedb e0  & \mxedi e0  & \mxedc e0  & \=e, e\u\i     & \verb|e0| & 10F1 \\ \hline
ho         & \mxedr o1  & \mxedb o1  & \mxedi o1  & \mxedc o1  & \=o, oy & \verb|o1| & 10F5 \\ \hline
jo         & \mxedr i1  & \mxedb i1  & \mxedi i1  & \mxedc i1  & \u\i    & \verb|i1| & 10F2 \\ \hline
wi         & \mxedr w   & \mxedb w   & \mxedi w   & \mxedc w   & w       & \verb|w| & 10F3  \\ \hline
fi         & \mxedr f   & \mxedb f   & \mxedi f   & \mxedc f   & f       & \verb|f|  & 10F6  \\ \hline
schwa      & \mxedr y   & \mxedb y   & \mxedi y   & \mxedc y   & \textschwa, y       & \verb|y| & 10F7  \\ \hline
elifi      & \mxedr a1  & \mxedb a1  & \mxedi a1  & \mxedc a1  & '       & \verb|a1| & 10F8 \\ \hline
\ae        & \mxedr e1  & \mxedb e1  & \mxedi e1  & \mxedc e1  & \ae     & \verb|e1| & \\ \hline
\end{tabular}
\end{center}

The letters \emph{qar}, \emph{h\=e}, \emph{ho}, \emph{jo}, \emph{wi} and \emph{fi}
are seldomly used or archaic letters (\emph{fi} may be used to transcribe
latin \emph{f}). \emph{Schwa} and \emph{elifi} are mainly used in Mingrelian or Svan, whereas \emph{\ae} is used to write the Ossetian
language using Mxedruli.

\subsubsection{Digits}


\begin{center}
\begin{tabular}{|c |c |c | c|}
\hline
\multicolumn{3}{|c|}{\it Mxedruli}  & \\
\cline{1-3}
normal & bold & capital & \raisebox{1.5ex}[1.5ex]{input} \\
\hline\hline
 \mxedr 1   & \mxedb 1 & \mxedc 1   &  \verb|1|  \\ \hline
 \mxedr 2   & \mxedb 2 & \mxedc 2   &  \verb|2|  \\ \hline
 \mxedr 3   & \mxedb 3 & \mxedc 3   &  \verb|3|  \\ \hline
 \mxedr 4   & \mxedb 4 & \mxedc 4   &  \verb|4|  \\ \hline
 \mxedr 5   & \mxedb 5 & \mxedc 5   &  \verb|5|  \\ \hline
\end{tabular} \hspace{10mm}
%
\begin{tabular}{|c |c |c | c|}
\hline
\multicolumn{3}{|c|}{\it Mxedruli} & \\
\cline{1-3}
normal & bold & capital & \raisebox{1.5ex}[1.5ex]{input} \\
\hline\hline
\mxedr 6  & \mxedb 6 &  \mxedc6  & \verb|6|  \\ \hline
\mxedr 7  & \mxedb 7 &  \mxedc7  & \verb|7|  \\ \hline
\mxedr 8  & \mxedb 8 &  \mxedc8  & \verb|8|  \\ \hline
\mxedr 9  & \mxedb 9 &  \mxedc9  & \verb|9|  \\ \hline
\mxedr 0  & \mxedb 0 &  \mxedc0  & \verb|0|  \\ \hline
\end{tabular}
\end{center}


\subsubsection{Punctuation}\label{punkte}
\begin{center}


\begin{tabular}[t]{|c |c |c | c|}
\hline
\multicolumn{3}{|c|}{\it Mxedruli} & \\
\cline{1-3}
normal & bold & capital & \raisebox{1.5ex}[1.5ex]{input} \\
\hline\hline
 \mxedb .   & \mxedr .  & \mxedc .   &   \verb|.|   \\ \hline
 \mxedr ,   & \mxedb ,  & \mxedc ,   &   \verb|,|   \\ \hline
 \mxedr !   & \mxedb !  & \mxedc !   &   \verb|!|   \\ \hline
 \mxedr ?   & \mxedb ?  & \mxedc ?   &   \verb|?|   \\ \hline
 \mxedr =   & \mxedb =  & \mxedc =   &   \verb|=|   \\ \hline
 \mxedr -   & \mxedb -  & \mxedc -   &   \verb|-|   \\ \hline
 \mxedr --  & \mxedb -- & \mxedc --    &  \verb|--|  \\ \hline
 \mxedr --- & \mxedb --- & \mxedc ---  &  \verb|---| \\ \hline
 \mxedr /   & \mxedb /   & \mxedc /    &  \verb|/|   \\ \hline
 \mxedr \char92 & \mxedb \char92 & \mxedc \char92   &  \verb|\char92| \\ \hline
\end{tabular} \hspace{10mm}
%
\begin{tabular}[t]{|c |c |c | c|}
\hline
\multicolumn{3}{|c|}{\it Mxedruli} & \\
\cline{1-3}
normal & bold & capital & \raisebox{1.5ex}[1.5ex]{input} \\
\hline\hline
 \mxedr :  & \mxedb :  & \mxedc :  &   \verb|:|   \\ \hline
 \mxedr ;  & \mxedb ;  & \mxedc ;  &   \verb|;|   \\ \hline
 \mxedr +  & \mxedb +  & \mxedc +  &   \verb|+|   \\ \hline
 \mxedr (  & \mxedb (  & \mxedc (  &   \verb|(|   \\ \hline
 \mxedr )  & \mxedb )  & \mxedc )  &   \verb|)|   \\ \hline
 \mxedr [  & \mxedb [  & \mxedc [  &   \verb|[|   \\ \hline
 \mxedr ]  & \mxedb ]  & \mxedc ]  &   \verb|]|   \\ \hline
 \mxedr '' & \mxedb '' & \mxedc '' &   \verb|''|  \\ \hline
 \mxedr ,, & \mxedb ,, & \mxedc ,, &   \verb|,,|  \\ \hline
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\emph{Xucuri}}
The following tables show the names and the coding of the {\it
Xucuri\/} alphabets ({\it Mrgvlovani\/} and {\it \d
Kutxovani\/}). These fonts do not have any 
diacritics. Further, for the time being, it only consists of the normal
series.\footnote{If there is a demand for italics and bold versions, I
will gladly provide the driver files required. Please contact me as
indicated on page 1.}

Within a \LaTeX-document they can be activated by \verb+\xucr+.
If used within a \LaTeX-document together with the style file provided
({\tt xucuri.sty}) the \LaTeX\ size commands will also work. 
(\LaTeXe\ is supported together with NFSS2, as is \LaTeX 2.09)

\subsubsection{Letters}\label{xucletters}

\begin{center}
\begin{tabular}{|l |c |c| c| c|c|c|}
\hline
%           & \it Xucuri &   \\
%\cline{2-2}
%\raisebox{1.5ex}[1.5ex]{Name}  & normal  &
%\raisebox{1.5ex}[1.5ex]{Transcription} & 
%raisebox{1.5ex}[1.5ex]{\TeX-input} \\
Name & \multicolumn{2}{c|}{\it Xucuri} & Transcription & input & \multicolumn{2}{c|}{Unicode}\\
     & majuscules & minuscules & & & & \\
     & \it Mrg(v)lovani & \it \d Kutxovani & & & \it Mrg(v)lovani & \it \d Kutxovani \\
\hline\hline
an         & \xucr A   & \xucr a   & a       & \verb|A|, \verb|a| & 10A0 & 2D00 \\ \hline
ban        & \xucr B   & \xucr b   & b       & \verb|B|, \verb|b| & 10A1 & 2D01 \\ \hline
gan        & \xucr G   & \xucr g   & g       & \verb|G|, \verb|g| & 10A2 & 2D02 \\ \hline
don        & \xucr D   & \xucr d   & d       & \verb|D|, \verb|d| & 10A3 & 2D03 \\ \hline
en         & \xucr E   & \xucr e   & e       & \verb|E|, \verb|e| & 10A4 & 2D04 \\ \hline
vin        & \xucr V   & \xucr v   & v       & \verb|V|, \verb|v| & 10A5 & 2D05 \\ \hline
zen        & \xucr Z   & \xucr z   & z       & \verb|Z|, \verb|z| & 10A6 & 2D06 \\ \hline
tan        & \xucr T   & \xucr t   & t       & \verb|T|, \verb|t| & 10A7 & 2D07 \\ \hline
in         & \xucr I   & \xucr i   & i       & \verb|I|, \verb|i| & 10A8 & 2D08 \\ \hline
\d kan     & \xucr .K  & \xucr .k  & \d k    & \verb|.K|, \verb|.k| & 10A9 & 2D09\\ \hline
las        & \xucr L   & \xucr l   & l       & \verb|L|, \verb|l| & 10AA & 2D0A \\ \hline
man        & \xucr M   & \xucr m   & m       & \verb|M|, \verb|m| & 10AB & 2D)B \\ \hline
nar        & \xucr N   & \xucr n   & n       & \verb|N|, \verb|n| & 10AC & 2D0C \\ \hline
on         & \xucr O   & \xucr o   & o       & \verb|O|, \verb|o| & 10AD & 2D0D \\ \hline
\.par      & \xucr .P  & \xucr .p  & \.p     & \verb|.P|, \verb|.p| & 10AE & 2D0E\\ \hline
\v zan     & \xucr +Z  & \xucr +z  & \v z    & \verb|+Z|, \verb|+z| & 10AF & 2D0F\\ \hline
ran        & \xucr R   & \xucr r   & r       & \verb|R|, \verb|r| & 10B0 & 2D10 \\ \hline
san        & \xucr S   & \xucr s   & s       & \verb|S|, \verb|s| & 10B1 & 2D11 \\ \hline
\d tar     & \xucr .T  & \xucr .t  & \d t    & \verb|.T|, \verb|.t| & 10B2 & 2D12\\ \hline
un         & \xucr U   & \xucr u   & u       & \verb|U|, \verb|u| & 10B3 & 2D13  \\ \hline
par        & \xucr P   & \xucr p   & p       & \verb|P|, \verb|p| & 10B4 & 2D14 \\ \hline
kan        & \xucr K   & \xucr k   & k       & \verb|K|, \verb|k| & 10B5 & 2D15 \\ \hline
\.gan      & \xucr .G  & \xucr .g  & \.g     & \verb|.G|, \verb|.g| & 10B6 & 2D16 \\ \hline
\.qar      & \xucr Q   & \xucr q   & \.q     & \verb|Q|, \verb|q|\footnotemark & 10B7 & 2D17 \\ \hline
\v san     & \xucr +S  & \xucr +s  & \v s    & \verb|+S|, \verb|+s| & 10B8 & 2D18 \\ \hline
\v cin     & \xucr +C  & \xucr +c  & \v c    & \verb|+C|, \verb|+c| & 10B9 & 2D19 \\ \hline
can        & \xucr C   & \xucr c   & c       & \verb|C|, \verb|c|  & 10BA & 2D1A \\ \hline
dzil       & \xucr J   & \xucr j   & j/dz    & \verb|J|, \verb|j|  & 10BB & 2D1B \\ \hline
\d cil     & \xucr .C  & \xucr .c  & \d c    & \verb|.C|, \verb|.c| & 10BC & 2D1C \\ \hline
\d{\v c}ar & \xucr .+C & \xucr .+c & \d{\v c}& \verb|.+C|, \verb|.+c| & 10BD & 2D1D \\ \hline
xan        & \xucr X   & \xucr x   & x       & \verb|X|, \verb|x| & 10BE & 2D1E  \\ \hline
d\v zan    & \xucr +J  & \xucr +j  & d\v z   & \verb|+J|, \verb|+j| & 10BF & 2D1F \\ \hline
ha         & \xucr H   & \xucr h   & h       & \verb|H|, \verb|h|  & 10C0 & 2D20 \\ \hline
\end{tabular}
\end{center}

\footnotetext{Although the transcription of `{\xucr Q}' and
`{\xucr q}' is {\it \.Q\/} and {\it \.q\/} respectively, the
\TeX-input will remain `{\tt Q}' and `{\tt q}' for the letter {\it \.qar\/} 
as it appears with a far more frequent than {\it qar\/}
(`{\xucr Q1}', `{\xucr q1}') which is coded as `{\tt Q1}' and `{\tt q1}'.}


\begin{center}
\begin{tabular}{|l |c| c |c| c|}
\hline
%           & \it Xucuri &  & \\
%\cline{2-2}
%\raisebox{1.5ex}[1.5ex]{Name}  & normal  &
%\raisebox{1.5ex}[1.5ex]{Transcription} & 
%\raisebox{1.5ex}[1.5ex]{\TeX-input} \\
Name & \multicolumn{2}{c|}{\it Xucuri} & Transcription & \TeX-input \\
     & majuscules & minuscules & & \\
     & \it Mrg(v)lovani & \it \d Kutxovani & & \\
\hline\hline
qar    & \xucr Q1  & \xucr q1  & q       & \verb|Q1|, \verb|q1|  \\ \hline
h\=e   & \xucr E0  & \xucr e0  & \=e, e\u\i & \verb|E0|, \verb|e0|  \\ \hline
ho     & \xucr O1  & ---\footnotemark       & \=o, oy & \verb|O1|       \\ \hline
jo     & \xucr I1  & \xucr i1  & \u\i    & \verb|I1|, \verb|i1|  \\ \hline
wi     & \xucr W   & \xucr w   & w       & \verb|W|, \verb|w|    \\ \hline
\end{tabular}
\end{center}

\footnotetext{The letter {\it ho\/} does only have a majuscule form.}

\subsubsection{Punctuation}
Please refer to section \ref{punkte} on page
\pageref{punkte}. Punctuation for {\it Xucuri\/} is encoded exactly as
for {\it Mxedruli\/}

\subsection{Correspondant letters}

This table shows, which character of {\it Mxedruli\/} corresponds to
which {\it Xucuri\/} character. Characters not found here do only occur in
one of the alphabets.

\begin{center}
\begin{tabular}{|l |c |c | c|}\hline
           & \it Mxedruli & \multicolumn{2}{c|}{\it Xucuri}  \\ 
           &              & majuscules & minuscules \\
           &              & \it Mrg(v)lovani & \it \d Kutxovani \\
\hline
an         & \mxedr a   & \xucr A   & \xucr a   \\ \hline
ban        & \mxedr b   & \xucr B   & \xucr b   \\ \hline
gan        & \mxedr g   & \xucr G   & \xucr g   \\ \hline
don        & \mxedr d   & \xucr D   & \xucr d   \\ \hline
en         & \mxedr e   & \xucr E   & \xucr e   \\ \hline
vin        & \mxedr v   & \xucr V   & \xucr v   \\ \hline
zen        & \mxedr z   & \xucr Z   & \xucr z   \\ \hline
tan        & \mxedr t   & \xucr T   & \xucr t   \\ \hline
in         & \mxedr i   & \xucr I   & \xucr i   \\ \hline
\d kan     & \mxedr .k  & \xucr .K  & \xucr .k  \\ \hline
las        & \mxedr l   & \xucr L   & \xucr l   \\ \hline
man        & \mxedr m   & \xucr M   & \xucr m   \\ \hline
nar        & \mxedr n   & \xucr N   & \xucr n   \\ \hline
\end{tabular}

\begin{tabular}{|l |c |c | c|}\hline
           & \it Mxedruli & \multicolumn{2}{c|}{\it Xucuri}  \\ 
           &              & majuscules & minuscules \\
           &              & \it Mrg(v)lovani & \it \d Kutxovani \\
\hline
on         & \mxedr o   & \xucr O   & \xucr o   \\ \hline
\.par      & \mxedr .p  & \xucr .P  & \xucr .p  \\ \hline
\v zan     & \mxedr +z  & \xucr +Z  & \xucr +z  \\ \hline
ran        & \mxedr r   & \xucr R   & \xucr r   \\ \hline
san        & \mxedr s   & \xucr S   & \xucr s   \\ \hline
\d tar     & \mxedr .t  & \xucr .T  & \xucr .t  \\ \hline
un         & \mxedr u   & \xucr U   & \xucr u   \\ \hline
par        & \mxedr p   & \xucr P   & \xucr p   \\ \hline
kan        & \mxedr k   & \xucr K   & \xucr k   \\ \hline
\.gan      & \mxedr .g  & \xucr .G  & \xucr .g  \\ \hline
\.qar      & \mxedr q   & \xucr Q   & \xucr q   \\ \hline
\v san     & \mxedr +s  & \xucr +S  & \xucr +s  \\ \hline
\v cin     & \mxedr +c  & \xucr +C  & \xucr +c  \\ \hline
can        & \mxedr c   & \xucr C   & \xucr c   \\ \hline
dzil       & \mxedr j   & \xucr J   & \xucr j   \\ \hline
\d cil     & \mxedr .c  & \xucr .C  & \xucr .c  \\ \hline
\d{\v c}ar & \mxedr .+c & \xucr .+C & \xucr .+c \\ \hline
xan        & \mxedr x   & \xucr X   & \xucr x   \\ \hline
d\v zan    & \mxedr +j  & \xucr +J  & \xucr +j  \\ \hline
ha         & \mxedr h   & \xucr H   & \xucr h   \\ \hline
%\hline
%% \end{tabular}
%% \end{center}
%% 
%% \begin{center}
%% \begin{tabular}{|l |c |c|}
%% \hline
%%            & \it Mxedruli & \it Xucuri  \\ \hline
qar        & \mxedr  q1 & \xucr Q1  & \xucr q1  \\ \hline
h\=e       & \mxedr  e0 & \xucr E0  & \xucr e0  \\ \hline
ho         & \mxedr  o1 & \xucr O1  & \xucr ---  \\ \hline
jo         & \mxedr  i1 & \xucr I1  & \xucr i1  \\ \hline
wi         & \mxedr  w  & \xucr W   & \xucr w   \\ \hline
\end{tabular}
\end{center}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIRST EXAMPLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\section{Examples}\label{Examples}
\subsection{{\mxedb  ve.pxis .tqaosani} -- {The Knight in the Tiger's Skin}}
The following example are two stanzas from {\it \v Sota Rustaveli\/}'s
{\it Ve\.pxis \d T\.qaosani\/} ``The Knight in the Tiger's Skin''.%
\footnote{This example you can find slightly changed in the example
file {\tt vepxis.tex}.}

\begin{center}
{\Large\mxedc ve.pxis .tqaosani}

\medskip
{\large\mxedc +sota rustaveli}
\end{center}

\bigskip

\begin{verse}
\begin{mxedr}
.gmertsa +semvedre, nutu .kvla damxsnas soplisa +sromasa,\\
cecxls, .cqalsa da mi.casa, haerta tana +sromasa;\\
momcnes prteni da a.gvprinde, mivhxvde mas +cemsa ndomasa,\\
d.gisit da .gamit vhxedvide mzisa elvata .krtomasa.

\medskip
mze u+senod ver ikmdebis, ratgan +sen xar masa .cili,\\
gana.gamca mas eaxel misi e.tli, ar tu .cbili!\\
muna gnaxo, madve gsaxo, ganminatlo guli +crdili,\\
tu sicocxle m.care mkonda, si.kvdilimca mkonda .t.kbili!
\end{mxedr}
\end{verse}

\bigskip
This was set with the following:


\begin{small}
\begin{verbatim}
   \documentclass[12pt]{article}
   \usepackage{mxedruli}
   \begin{document}
   \begin{center}
   {\Large\mxedc ve.pxis .tqaosani}

   \medskip
   {\large\mxedc +sota rustaveli}
   \end{center}

   \bigskip

   \begin{verse}
   \begin{mxedr}
   .gmertsa +semvedre, nutu .kvla damxsnas soplisa 
         +sromasa,\\
   cecxls, .cqalsa da mi.casa, haerta tana +sromasa;\\
   momcnes prteni da a.gvprinde, mivhxvde mas +cemsa 
         ndomasa,\\
   d.gisit da .gamit vhxedvide mzisa elvata .krtomasa.

   \medskip
   mze u+senod ver ikmdebis, ratgan +sen xar masa .cili,\\
   gana.gamca mas eaxel misi e.tli, ar tu .cbili!\\
   muna gnaxo, madve gsaxo, ganminatlo guli +crdili,\\
   tu sicocxle m.care mkonda, si.kvdilimca mkonda .t.kbili!
   \end{mxedr}
   \end{verse}
   \end{document}
\end{verbatim}
\end{small}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% SECOND EXAMPLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Another example with different font sizes}
The second sample text is due to the lack of some proper text%
\footnote{If you would send me a sample text, you prepared with
these fonts to be included in this documentation, I would be most grateful.}
when I prepared this file just taken from the preface of a small
Georgian-German dictionary, but it should give another impression how
this font -- using different sizes and the ``capitals''-- looks like as well:

\begin{center}
{\LARGE\mxedc kartul--germanuli 
leksi.koni}

\medskip
\mxedi +sedgenili: \\
{\large\mxedb o. xuci+svilis da \\
t. xa.tia+svilis mier}

\bigskip
\bigskip
{\mxedc gamomcemloba ,,ganatleba''\\
tbilisi ---} 1977
\end{center}

\bigskip
\mxedr
.cinamdebare kartul--germanul leksi.kon+si +ser+ceulia kartuli
enis leksi\-.ki\-dan si.tqvebi, romlebic qvelaze me.tad
ixmareba sala.para.ko ena+si, agretve mxa.tvrul, .poli.ti.kur,
mecnierul da sas.cavlo li.tera.tura+si. ganmar.tebulia si.tqvis
qvela jiritadi mni+svneloba da ilus.trirebulia nimu\-+se\-bit.
vcdilobdit, satanado prazeologizmebis mo+sveleibit gang\-ve\-mar\-.ta
si.tqvis niuansobrivi gagebanic.

\rm
The input was:


\begin{small}
\begin{verbatim}
  \documentclass[12pt]{article}
  \usepackage{mxedruli}
  \begin{document}
  \begin{center}
  {\LARGE\mxedc kartul--germanuli 
  leksi.koni}

  \medskip
  \mxedi +sedgenili: \\
  {\large\mxedb o. xuci+svilis da \\
  t. xa.tia+svilis mier}

  \bigskip
  \bigskip
  {\mxedc gamomcemloba ,,ganatleba''\\
  tbilisi ---} 1977
  \end{center}

  \bigskip \mxedr 
  .cinamdebare kartul--germanul leksi.kon+si
  +ser+ceulia kartuli enis lek\-si.ki\-dan si.tqvebi,
  romlebic qvelize me.tad ixmareba sala.para.ko eni+si,
  abretve mxa.tvrul, .poli.ti.kur, mecnierul da sas.cavlo
  li.tera.turi+si. ganmar.tbulia si.tqvis qvela jiritadi
  mni+svneloba da ilus.trarebulia nimu\-+se\-bit.
  vcdilobdit, satanado prazeologizsegis mo+svelibit
  gang\-ve\-mar\-.ta si.tqvis niuansobrivi gagebanic.
  \end{document}
\end{verbatim}
\end{small}


\subsection{An example using \emph{Xucuri}}
The following example is taken from {\it N. Marr and M. Bri\`ere, 
La Langue G\'eor\-gienne, Paris 1931\/}, p. 595:

\begin{xucr}
\begin{center}
SAXAREBAI1 MATEES TAVISAI1.

B
\end{center}

{\rm 1.} --- XOLO IESOW KRIS.TEES +SUBASA BETLEMS
HOWRIAS.TANISASA. D.GETA HERODE MEPISATA. AHA
MOGOWNI A.GMOSAVALIT MOVIDES IEROWSALEEMD DA
I.TQODES:

{\rm 2.} --- SADA ARS ROMELI IGI I+SVA. MEOWPEE
HOWRIATAI1? RAI1\-RETOW VIXILET VARS.KOWLAVI MISI
A.GMOSAVALIT DA MOVEDIT TAV\-QOWA\-NIS-CEMAD MISA.

{\rm 3.} --- VITARCA ESMA ESE HERODES
MEPESA. +SEJR.COWNDA DA +SOVELI IEROWSALEEMI
MISTANA.

\end{xucr}

This was generated using the following input:


\begin{small}
\begin{verbatim}
   \documentclass[12pt]{article}
   \usepackage{xucuri}

   \begin{xucr}
   \begin{center}
   SAXAREBAI1 MATEES TAVISAI1.
   
   B
   \end{center}
   
   {\rm 1.} --- XOLO IESOW KRIS.TEES +SUBASA BETLEMS
   HOWRIAS.TANISASA. D.GETA HERODE MEPISATA. AHA
   MOGOWNI A.GMOSAVALIT MOVIDES IEROWSALEEMD DA
   I.TQODES:
   
   {\rm 2.} --- SADA ARS ROMELI IGI I+SVA. MEOWPEE
   HOWRIATAI1? RAI1RETOW VIXILET VARS.KOWLAVI MISI
   A.GMOSAVALIT DA MOVEDIT TAVQOWANIS-CEMAD MISA.
   
   {\rm 3.} --- VITARCA ESMA ESE HERODES
   MEPESA. +SEJR.COWNDA DA +SOVELI IEROWSALEEMI
   MISTANA.
   \end{xucr}
\end{verbatim}
\end{small}


The example which illustrates the {\it Xucuri\/} minuscules is again
taken from {\it N. Marr and M. Bri\`ere, La Langue G\'eor\-gienne,
Paris 1931\/}, p. 599: 

\begin{xucr}
\begin{center}\Large
saxarebai1 lu.kai1s tavisai1.

ie
\end{center}

{\rm 11.} --- merme i.tqoda da tkua: .kacsa visme escnes
or je.

{\rm 12.} --- da hrkua umr.cemesman man mamasa twssa:
mamao. momec me romeli mxudebis na.cilidam.kw drebelisa. da ganuqo
mat sacxovrebeli igi.
\end{xucr}


The input was


\begin{small}
\begin{verbatim}
  \begin{xucr}
  \begin{center}\Large
  saxarebai1 lu.kai1s tavisai1.

  ie
  \end{center}

  {\rm 11.} --- merme i.tqoda da tkua: .kacsa visme escnes
  or je.

  {\rm 12.} --- da hrkua umr.cemesman man mamasa twssa:
  mamao. momec me romeli mxudebis na.cilidam.kw drebelisa. 
  da ganuqo mat sacxovrebeli igi.
\end{xucr}
\end{verbatim}
\end{small}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% FONT CODING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Font coding}
\def\cell#1{\char'#1}

\def\cellrow#1{
      &  \cell{#10} & \cell{#11} & \cell{#12} & \cell{#13} &
         \cell{#14} & \cell{#15} & \cell{#16} & \cell{#17} \\ \hline}

\def\reihe#1{\it '#10 \cellrow{#1}}

The following table shows the internal encoding of the 
defined letters of {\it Mxedruli\/} and {\it Xucuri\/}:


\begin{center}
{\mxedr
\begin{tabular}{ r || c | c | c | c | c | c | c | c | }
  & \it 0 & \it 1 & \it 2 & \it 3 & \it 4 & \it 5 & \it 6 & \it 7  \\
\hline\hline
\reihe{00}\reihe{01}\reihe{02}\reihe{03}\reihe{04}
\reihe{05}\reihe{06}\reihe{07}\reihe{10}\reihe{11}
\reihe{12}\reihe{13}\reihe{14}\reihe{15}\reihe{16}
\reihe{17}
\end{tabular}}
%\end{center}

%\begin{center}
{\xucr
\begin{tabular}{ r || c | c | c | c | c | c | c | c | }
  & \it 0 & \it 1 & \it 2 & \it 3 & \it 4 & \it 5 & \it 6 & \it 7  \\
\hline\hline
\reihe{00}\reihe{01}\reihe{02}\reihe{03}\reihe{04}
\reihe{05}\reihe{06}\reihe{07}\reihe{10}\reihe{11}
\reihe{12}\reihe{13}\reihe{14}\reihe{15}\reihe{16}
\reihe{17}
\end{tabular}}
\end{center}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% FILES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Files}\label{Install}
The package consists of the following files:

\begin{list}{}{%
  \labelwidth55mm
  \labelsep2mm
  \itemindent0mm
  \parsep0mm
  \leftmargin57mm
  \topsep0mm
  \itemsep0pt
 }
\renewcommand{\makelabel}[1]{#1\hfill}


\item[\tt readme] A very short information file.
%\item[\tt fonts/pk300/mxed10.*pk] The {\tt .pk}-Files for standard 
%    {\it Mxedruli\/} generated for use with 300 dpi printers.
%\item[\tt fonts/pk300/mxedbf10.*pk] The {\tt .pk}-Files for bold 
%    {\it Mxedruli\/} generated for use with 300 dpi printers.
%\item[\tt fonts/pk300/mxedi10.*pk] The {\tt .pk}-Files for italics
%    {\it Mxedruli\/} generated for use with 300 dpi printers.
%\item[\tt fonts/pk300/mxedc10.*pk] The {\tt .pk}-Files for capital
%    {\it Mxedruli\/} generated for use with 300 dpi printers.
%\item[\tt fonts/pk300/xuc10.*pk] The {\tt .pk}-Files for 
%    {\it Xucuri\/} generated for use with 300 dpi printers.
%\item [\tt fonts/tfm/*.tfm] \TeX\ Font Metrics Files.
 
\item[\tt inputs/umxed.fd] Font definition files for use with NFSS2.
\item[\tt inputs/uxuc.fd]  %Font definition file for use with NFSS2.
\item[\tt inputs/mxedruli.sty] \LaTeX\ Style File.
\item[\tt inputs/xucuri.sty] % \LaTeX\ Style File.

\item[\tt mf/mxed.mf] The standard generation file for {\logo METAFONT}.
\item[\tt mf/mxed10.mf] Driver file for normal {\it Mxedruli\/}.
     Run {\logo METAFONT} on this file to generate {\it Mxedruli\/} 
     for any other resolution than 300 dpi.
\item[\tt mf/mxedbf10.mf] As before, but bold {\it Mxedruli\/}.
\item[\tt mf/mxedi10.mf] As before, but italics {\it Mxedruli\/}.
\item[\tt mf/mxedc10.mf] As before, but capital {\it Mxedruli\/}.
\item[\tt mf/mxedacc.mf] Accents for other Kartvelian languages (Svan).
\item[\tt mf/mxedbase.mf] {\logo METAFONT} macros etc.
\item[\tt mf/mxedcaps.mf] ``Capital'' letters.
\item[\tt mf/mxedd.mf] Digits (can be replaced by Computer Modern digits,
     cf. below).
\item[\tt mf/mxedfont.mf] Letters.
\item[\tt mf/mxedp.mf] Punctuation (can be replaced by Computer Modern
     punctuation, cf. below).

\item[\tt mf/xuc.mf] The standard generation file for {\logo METAFONT}.
\item[\tt mf/xuc10.mf] Driver file for {\it Xucuri\/}.
     Run {\logo METAFONT} on this file to generate {\it Xucuri\/} 
     for any other resolution than 300 dpi.
\item[\tt mf/xucbase.mf] {\logo METAFONT} macros etc.
\item[\tt mf/xucfont.mf] {\it Xucuri\/} majuscules.
\item[\tt mf/xucd.mf] Digits (can be replaced by Computer Modern digits,
     cf. below).
\item[\tt mf/xucl.mf] {\it Xucuri\/} minuscules.
\item[\tt mf/xucp.mf] Punctuation (can be replaced by Computer Modern
     punctuation, cf. below).

\item[\tt alphabets.tex] An example showing both Xucuri and Mxedruli
\item[\tt mxeddoc.ps] This documentation.
\item[\tt ossetic.tex] A short example of Ossetic text, written in
     {\it Mxedruli\/}.
\item[\tt vepxis.tex] The first {\it Mxedruli\/}-example of this 
     documentation.
\end{list}

To install the fonts etc., please copy all {\tt .mf}-files from the 
{\tt mf}-directory to the appropriate directory of
your \TeX-system. 
%The {\tt .tfm}-files must be copied into
%the {\tt tfm}-directory of your \TeX-system. 
Further, the files
from the {\tt inputs}-directory must be copied into the directory
where \LaTeX\ can find them.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% METAFONT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Using {\logogr METAFONT}} % on {\tt mxed[bf|c|i]10.mf}/{\tt xuc10.mf}}
\label{MF}
If you are going to regenerate the fonts, there are two possibilities,
using the Computer Modern digits and punctuation (which are far nicer)
or the digits and punctuation provided in \verb|mxedd.mf| and
\verb|mxedp.mf| or \verb|xucp.mf| respectively.

In the first case you should use the following command in a UN*X 
environment:\footnote{Do so analogously for {\tt mxedbf10}, 
{\tt mxedi10}, {\tt mxedc10} and {\tt xuc10} respectively.}\\
\verb|  mf '&cmmf \mode=|{\it your\_mode\/}\verb|;' input mxed10|


Else you use plain {\logo METAFONT} by:\\
\verb|  mf '\mode=|{\it your\_mode\/}\verb|;' input mxed10|

(Other sizes are generated by:\\
\verb|  mf '&cmmf \mode=|{\it your\_mode\/}\verb|; mag=magstep|{\it
n\/}\verb|' input mxed10|

or\\
\verb|  mf '\mode=|{\it your\_mode\/}\verb|; mag=magstep|{\it
n\/}\verb|' input mxed10|

respectively.) {\it your\_mode\/} has to be replaced by the mode your
printer requires, e.g. \verb|localmode| or \verb|laserjet|, {\it n\/} by
a valid magstep ({\tt 1}, {\tt 2}, {\tt 3}, {\tt 4}, {\tt 5}
or {\tt half}).

In either case \verb|mf| must be followed by \verb|gftopk| to 
generate the \verb|.pk|-files. Please refer to the documentation
of \verb|gftopk| for further information on postprocessing
{\logo METAFONT} output.

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%% EOF %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
