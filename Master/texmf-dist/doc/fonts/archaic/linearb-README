    The linearb bundle provides a font and package for the Linear B script,
which was a syllabary used in the Bronze Age for writing Mycenaean Greek.
It is one of a series for archaic scripts.

Changes in version 1.2 (2005/06/22)
o Font supplied as Postscript Type1 as well as MetaFont
o 47 new pictograms
o Added map file
o Reconfigured the encoding of the numerals and unknown glyphs

Changes in version 1.1 (2001/08/01)
o Changed \Bpiii to \Bpaiii

Changes in version 1.0 (1999/06/20)
o First public release

------------------------------------------------------------------
  Author: Peter Wilson (Herries Press) herries dot press at earthlink dot net
  Copyright 1999--2005 Peter R. Wilson

  This work may be distributed and/or modified under the
  conditions of the Latex Project Public License, either
  version 1.3 of this license or (at your option) any
  later version.
  The latest version of the license is in
    http://www.latex-project.org/lppl.txt
  and version 1.3 or later is part of all distributions of
  LaTeX version 2003/06/01 or later.

  This work has the LPPL maintenance status "author-maintained".

  This work consists of the files:
    README (this file)
    linearb.dtx
    linearb.ins
    linearb.pdf
  and the derived files
    linearb.sty
    t1linb10.fd
    linearb.map
    linb10.mf

------------------------------------------------------------------
     The distribution consists of the following files:
README (this file)
linearb.dtx
linearb.ins
linearb.pdf     (user manual)
trylinearb.tex  (example usage)
trylinearb.pdf
linb10.afm
linb10.pfb
linb10.tfm

    To install the bundle:
o If you want MetaFont sources uncomment the appropriate lines in linearb.ins.
o run: latex linearb.ins, which will generate:
       linearb.sty
       *.fd files
       linearb.map
       and possibly linb10.mf
o Move *.sty and *.fd files to a location where LaTeX will find them
    e.g., .../texmf-local/tex/latex/linearb
o Move *.afm, *.pfb and *.tfm files to where LaTeX looks for font information
    e.g., .../texmf-var/fonts/afm/public/archaic/*.afm
          .../texmf-var/fonts/type1/public/archaic/*.pfb
          .../texmf-var/fonts/tfm/public/archaic/*.tfm
o Add the *.map information to the dvips/pdftex font maps
   If you want the MetaFont version as well:
   o Move the *.mf files to, e.g., .../texmf-var/fonts/source/public/linearb
   o Add a line like the following to the (texmf/fontname/)special.map file:
     linb10.mf    public      linearb 
o Refresh the database
 (for more information on the above see the FAQ).

o run: (pdf)latex trylinearb for a test of the font

    If you want a full manual with all the MetaFont and LaTeX code and commentry, 
comment out the \OnlyDescription line in linearb.dtx.
o run: (pdf)latex linearb.dtx
o (for an index run: makeindex -s gind.ist *.idx)
o run: (pdf)latex *.dtx
o Print *.(pdf|dvi) for a hardcopy of the package manual

2005/06/22
Peter Wilson
herries dot press at earthlink dot net

