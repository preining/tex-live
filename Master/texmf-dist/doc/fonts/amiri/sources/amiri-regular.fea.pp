languagesystem DFLT dflt;
languagesystem arab dflt;
languagesystem arab ARA;
languagesystem arab URD;
languagesystem arab SND;
languagesystem latn dflt;
languagesystem latn TRK;

@aAlf.fina = [ uni0625.fina uni0627.fina uni0774.fina uni0773.fina uni0623.fina uni0622.fina uni0675.fina uni0672.fina uni0673.fina uni0671.fina ];
@aAlf.isol = [ uni0625 uni0627 uni0774 uni0773 uni0623 uni0622 uni0675 uni0672 uni0673 uni0671 ];
@aAyn.fina = [ uni06FC.fina uni063A.fina uni075E.fina uni075D.fina uni075F.fina uni06A0.fina uni0639.fina ];
@aAyn.init = [ uni06FC.init uni063A.init uni075E.init uni075D.init uni075F.init uni06A0.init uni0639.init ];
@aAyn.isol = [ uni06FC uni063A uni075E uni075D uni075F uni06A0 uni0639 ];
@aAyn.medi = [ uni06FC.medi uni063A.medi uni075E.medi uni075D.medi uni075F.medi uni06A0.medi uni0639.medi ];
@aBaa.fina = [ uni0751.fina uni0750.fina uni0753.fina uni0680.fina uni062A.fina uni0754.fina uni062B.fina uni0679.fina uni067C.fina uni0756.fina uni0752.fina uni066E.fina uni067F.fina uni0755.fina uni08A0.fina uni067D.fina uni067E.fina uni067B.fina uni0628.fina uni067A.fina ];
@aBaa.init = [ uni0777.init uni0680.init uni0776.init uni06BC.init uni0750.init uni0756.init uni0768.init uni06CE.init uni0775.init uni06BD.init uni0626.init uni066E.init uni0620.init uni064A.init uni06BB.init uni067F.init uni0755.init uni08A0.init uni067D.init uni067E.init uni067B.init uni0628.init uni067A.init uni0751.init uni0646.init uni0753.init uni0752.init uni062A.init uni0678.init uni063D.init uni062B.init uni0679.init uni06B9.init uni0769.init uni0649.init uni067C.init uni0754.init uni06D1.init uni06D0.init uni06BA.init uni06CC.init uni0767.init ];
@aBaa.isol = [ uni0751 uni0750 uni0753 uni0680 uni062A uni0754 uni062B uni0679 uni067C uni0756 uni0752 uni066E uni067F uni0755 uni08A0 uni067D uni067E uni067B uni0628 uni067A ];
@aBaa.medi = [ uni0777.medi uni0680.medi uni0776.medi uni06BC.medi uni0750.medi uni0756.medi uni0768.medi uni06CE.medi uni0775.medi uni06BD.medi uni0626.medi uni066E.medi uni0620.medi uni064A.medi uni06BB.medi uni067F.medi uni0755.medi uni08A0.medi uni067D.medi uni067E.medi uni067B.medi uni0628.medi uni067A.medi uni0751.medi uni0646.medi uni0753.medi uni0752.medi uni062A.medi uni0678.medi uni063D.medi uni062B.medi uni0679.medi uni06B9.medi uni0769.medi uni0649.medi uni067C.medi uni0754.medi uni06D1.medi uni06D0.medi uni06BA.medi uni06CC.medi uni0767.medi ];
@aDal.fina = [ uni0690.fina uni06EE.fina uni0689.fina uni0688.fina uni075A.fina uni0630.fina uni062F.fina uni0759.fina uni068C.fina uni068B.fina uni068A.fina uni068F.fina uni068E.fina uni068D.fina ];
@aDal.isol = [ uni0690 uni06EE uni0689 uni0688 uni075A uni0630 uni062F uni0759 uni068C uni068B uni068A uni068F uni068E uni068D ];
@aFaa.fina = [ uni0760.fina uni0761.fina uni0641.fina uni06A1.fina uni06A2.fina uni06A3.fina uni06A4.fina uni06A5.fina uni06A6.fina ];
@aFaa.init = [ uni066F.init uni0761.init uni0760.init uni0642.init uni0641.init uni06A8.init uni06A1.init uni06A2.init uni06A3.init uni06A4.init uni06A5.init uni06A6.init uni06A7.init ];
@aFaa.isol = [ uni0760 uni0761 uni0641 uni06A1 uni06A2 uni06A3 uni06A4 uni06A5 uni06A6 ];
@aFaa.medi = [ uni066F.medi uni0761.medi uni0760.medi uni0642.medi uni0641.medi uni06A8.medi uni06A1.medi uni06A2.medi uni06A3.medi uni06A4.medi uni06A5.medi uni06A6.medi uni06A7.medi ];
@aHaa.fina = [ uni062E.fina uni062D.fina uni0681.fina uni0687.fina uni0685.fina uni062C.fina uni0682.fina uni0757.fina uni0684.fina uni076F.fina uni076E.fina uni0683.fina uni06BF.fina uni077C.fina uni0758.fina uni0772.fina uni0686.fina ];
@aHaa.init = [ uni062E.init uni062D.init uni0681.init uni0687.init uni0685.init uni062C.init uni0682.init uni0757.init uni0684.init uni076F.init uni076E.init uni0683.init uni06BF.init uni077C.init uni0758.init uni0772.init uni0686.init ];
@aHaa.isol = [ uni062E uni062D uni0681 uni0687 uni0685 uni062C uni0682 uni0757 uni0684 uni076F uni076E uni0683 uni06BF uni077C uni0758 uni0772 uni0686 ];
@aHaa.medi = [ uni062E.medi uni062D.medi uni0681.medi uni0687.medi uni0685.medi uni062C.medi uni0682.medi uni0757.medi uni0684.medi uni076F.medi uni076E.medi uni0683.medi uni06BF.medi uni077C.medi uni0758.medi uni0772.medi uni0686.medi ];
@aHeh.fina = [ uni0647.fina uni06C1.fina uni06C3.fina uni06D5.fina uni0629.fina ];
@aHeh.init = [ uni0647.init uni06C1.init ];
@aHeh.isol = [ uni0647 uni06C1 uni06C3 uni06D5 uni0629 ];
@aHeh.medi = [ uni0647.medi uni06C1.medi ];
@aKaf.fina = [ uni063B.fina uni063C.fina uni077F.fina uni0764.fina uni0643.fina uni06B0.fina uni06B3.fina uni06B2.fina uni06AB.fina uni06AC.fina uni06AD.fina uni06AE.fina uni06AF.fina uni06A9.fina uni06B4.fina uni0763.fina uni0762.fina uni06B1.fina ];
@aKaf.init = [ uni063B.init uni063C.init uni077F.init uni0764.init uni0643.init uni06B0.init uni06B3.init uni06B2.init uni06AB.init uni06AC.init uni06AD.init uni06AE.init uni06AF.init uni06A9.init uni06B4.init uni0763.init uni0762.init uni06B1.init ];
@aKaf.isol = [ uni063B uni063C uni077F uni0764 uni0643 uni06B0 uni06B3 uni06B2 uni06AB uni06AC uni06AD uni06AE uni06AF uni06A9 uni06B4 uni0763 uni0762 uni06B1 ];
@aKaf.medi = [ uni063B.medi uni063C.medi uni077F.medi uni0764.medi uni0643.medi uni06B0.medi uni06B3.medi uni06B2.medi uni06AB.medi uni06AC.medi uni06AD.medi uni06AE.medi uni06AF.medi uni06A9.medi uni06B4.medi uni0763.medi uni0762.medi uni06B1.medi ];
@aLam.fina = [ uni06B5.fina uni06B7.fina uni0644.fina uni06B8.fina uni06B6.fina uni076A.fina ];
@aLam.init = [ uni06B5.init uni06B7.init uni0644.init uni06B8.init uni06B6.init uni076A.init ];
@aLam.isol = [ uni06B5 uni06B7 uni0644 uni06B8 uni06B6 uni076A ];
@aLam.medi = [ uni06B5.medi uni06B7.medi uni0644.medi uni06B8.medi uni06B6.medi uni076A.medi ];
@aMem.fina = [ uni0645.fina ];
@aMem.init = [ uni0645.init ];
@aMem.medi = [ uni0645.medi ];
@aMem.isol = [ uni0765 uni0645 uni0766 ];
@aMem.init_dots = [ uni0765.init uni0645.init uni0766.init ];
@aMem.medi_dots = [ uni0765.medi uni0645.medi uni0766.medi ];
@aNon.fina = [ uni0646.fina uni0767.fina uni06BA.fina uni06BC.fina uni06BB.fina uni0768.fina uni06B9.fina uni0769.fina uni06BD.fina ];
@aNon.isol = [ uni0646 uni0767 uni06BA uni06BC uni06BB uni0768 uni06B9 uni0769 uni06BD ];
@aQaf.fina = [ uni06A8.fina uni06A7.fina uni0642.fina uni066F.fina ];
@aQaf.isol = [ uni06A8 uni06A7 uni0642 uni066F ];
@aRaa.fina = [ uni0691.fina uni0692.fina uni0693.fina uni0694.fina uni0695.fina uni0696.fina uni0697.fina uni0698.fina uni0699.fina uni075B.fina uni06EF.fina uni0632.fina uni0771.fina uni0631.fina uni076B.fina uni076C.fina ];
@aRaa.isol = [ uni0691 uni0692 uni0693 uni0694 uni0695 uni0696 uni0697 uni0698 uni0699 uni075B uni06EF uni0632 uni0771 uni0631 uni076B uni076C ];
@aSad.fina = [ uni069D.fina uni06FB.fina uni0636.fina uni069E.fina uni0635.fina ];
@aSad.init = [ uni069D.init uni06FB.init uni0636.init uni069E.init uni0635.init ];
@aSad.isol = [ uni069D uni06FB uni0636 uni069E uni0635 ];
@aSad.medi = [ uni069D.medi uni06FB.medi uni0636.medi uni069E.medi uni0635.medi ];
@aSen.fina = [ uni06FA.fina uni076D.fina uni0633.fina uni077E.fina uni077D.fina uni0634.fina uni0770.fina uni075C.fina uni069A.fina uni069B.fina uni069C.fina ];
@aSen.init = [ uni06FA.init uni076D.init uni0633.init uni077E.init uni077D.init uni0634.init uni0770.init uni075C.init uni069A.init uni069B.init uni069C.init ];
@aSen.isol = [ uni06FA uni076D uni0633 uni077E uni077D uni0634 uni0770 uni075C uni069A uni069B uni069C ];
@aSen.medi = [ uni06FA.medi uni076D.medi uni0633.medi uni077E.medi uni077D.medi uni0634.medi uni0770.medi uni075C.medi uni069A.medi uni069B.medi uni069C.medi ];
@aTaa.fina = [ uni0638.fina uni0637.fina uni069F.fina ];
@aTaa.init = [ uni0638.init uni0637.init uni069F.init ];
@aTaa.isol = [ uni0638 uni0637 uni069F ];
@aTaa.medi = [ uni0638.medi uni0637.medi uni069F.medi ];
@aWaw.fina = [ uni06CB.fina uni0624.fina uni06CA.fina uni06CF.fina uni0778.fina uni06C6.fina uni06C7.fina uni06C4.fina uni06C5.fina uni0676.fina uni0677.fina uni06C8.fina uni06C9.fina uni0779.fina uni0648.fina ];
@aWaw.isol = [ uni06CB uni0624 uni06CA uni06CF uni0778 uni06C6 uni06C7 uni06C4 uni06C5 uni0676 uni0677 uni06C8 uni06C9 uni0779 uni0648 ];
@aYaa.init = [ uni0777.init uni06D1.init uni0775.init uni063D.init uni06D0.init uni0776.init uni06CC.init uni064A.init uni06CE.init ];
@aYaa.fina = [ uni0777.fina uni06D1.fina uni0775.fina uni063F.fina uni0678.fina uni063D.fina uni063E.fina uni06D0.fina uni0649.fina uni0776.fina uni06CD.fina uni06CC.fina uni0626.fina uni0620.fina uni064A.fina uni06CE.fina ];
@aYaa.isol = [ uni0777 uni06D1 uni0775 uni063F uni0678 uni063D uni063E uni06D0 uni0649 uni0776 uni06CD uni06CC uni0626 uni0620 uni064A uni06CE ];
@aYaaBarree.fina = [ uni077B.fina uni077A.fina uni06D2.fina ];
@aYaaBarree.isol = [ uni077B uni077A uni06D2 ];


@aAlf.fina_KafAlf = [ uni0625.fina_KafAlf uni0627.fina_KafAlf uni0774.fina_KafAlf uni0773.fina_KafAlf uni0623.fina_KafAlf uni0622.fina_KafAlf uni0675.fina_KafAlf uni0672.fina_KafAlf uni0673.fina_KafAlf uni0671.fina_KafAlf ];
@aAlf.fina_KafMemAlf = [ uni0625.fina_KafMemAlf uni0627.fina_KafMemAlf uni0774.fina_KafMemAlf uni0773.fina_KafMemAlf uni0623.fina_KafMemAlf uni0622.fina_KafMemAlf uni0675.fina_KafMemAlf uni0672.fina_KafMemAlf uni0673.fina_KafMemAlf uni0671.fina_KafMemAlf ];
@aAlf.fina_LamAlfFina = [ uni0625.fina_LamAlfFina uni0627.fina_LamAlfFina uni0774.fina_LamAlfFina uni0773.fina_LamAlfFina uni0623.fina_LamAlfFina uni0622.fina_LamAlfFina uni0675.fina_LamAlfFina uni0672.fina_LamAlfFina uni0673.fina_LamAlfFina uni0671.fina_LamAlfFina ];
@aAlf.fina_LamAlfIsol = [ uni0625.fina_LamAlfIsol uni0627.fina_LamAlfIsol uni0774.fina_LamAlfIsol uni0773.fina_LamAlfIsol uni0623.fina_LamAlfIsol uni0622.fina_LamAlfIsol uni0675.fina_LamAlfIsol uni0672.fina_LamAlfIsol uni0673.fina_LamAlfIsol uni0671.fina_LamAlfIsol ];
@aAlf.fina_MemAlfFina = [ uni0625.fina_MemAlfFina uni0627.fina_MemAlfFina uni0774.fina_MemAlfFina uni0773.fina_MemAlfFina uni0623.fina_MemAlfFina uni0622.fina_MemAlfFina uni0675.fina_MemAlfFina uni0672.fina_MemAlfFina uni0673.fina_MemAlfFina uni0671.fina_MemAlfFina ];
@aAyn.init_AboveHaa = [ uni06FC.init_AboveHaa uni063A.init_AboveHaa uni075E.init_AboveHaa uni075D.init_AboveHaa uni075F.init_AboveHaa uni06A0.init_AboveHaa uni0639.init_AboveHaa ];
@aAyn.init_AynHaaInit = [ uni06FC.init_AynHaaInit uni063A.init_AynHaaInit uni075E.init_AynHaaInit uni075D.init_AynHaaInit uni075F.init_AynHaaInit uni06A0.init_AynHaaInit uni0639.init_AynHaaInit ];
@aAyn.init_AynMemInit = [ uni06FC.init_AynMemInit uni063A.init_AynMemInit uni075E.init_AynMemInit uni075D.init_AynMemInit uni075F.init_AynMemInit uni06A0.init_AynMemInit uni0639.init_AynMemInit ];
@aAyn.init_AynYaaIsol = [ uni06FC.init_AynYaaIsol uni063A.init_AynYaaIsol uni075E.init_AynYaaIsol uni075D.init_AynYaaIsol uni075F.init_AynYaaIsol uni06A0.init_AynYaaIsol uni0639.init_AynYaaIsol ];
@aAyn.init_Finjani = [ uni06FC.init_Finjani uni063A.init_Finjani uni075E.init_Finjani uni075D.init_Finjani uni075F.init_Finjani uni06A0.init_Finjani uni0639.init_Finjani ];
@aAyn.init_YaaBarree = [ uni06FC.init_YaaBarree uni063A.init_YaaBarree uni075E.init_YaaBarree uni075D.init_YaaBarree uni075F.init_YaaBarree uni06A0.init_YaaBarree uni0639.init_YaaBarree ];
@aAyn.medi_AynYaaFina = [ uni06FC.medi_AynYaaFina uni063A.medi_AynYaaFina uni075E.medi_AynYaaFina uni075D.medi_AynYaaFina uni075F.medi_AynYaaFina uni06A0.medi_AynYaaFina uni0639.medi_AynYaaFina ];
@aBaa.fina_BaaBaaIsol = [ uni0751.fina_BaaBaaIsol uni0750.fina_BaaBaaIsol uni0753.fina_BaaBaaIsol uni0680.fina_BaaBaaIsol uni062A.fina_BaaBaaIsol uni0754.fina_BaaBaaIsol uni062B.fina_BaaBaaIsol uni0679.fina_BaaBaaIsol uni067C.fina_BaaBaaIsol uni0756.fina_BaaBaaIsol uni0752.fina_BaaBaaIsol uni066E.fina_BaaBaaIsol uni067F.fina_BaaBaaIsol uni0755.fina_BaaBaaIsol uni08A0.fina_BaaBaaIsol uni067D.fina_BaaBaaIsol uni067E.fina_BaaBaaIsol uni067B.fina_BaaBaaIsol uni0628.fina_BaaBaaIsol uni067A.fina_BaaBaaIsol ];
@aBaa.init_AboveHaa = [ uni0777.init_AboveHaa uni0680.init_AboveHaa uni0776.init_AboveHaa uni06BC.init_AboveHaa uni0750.init_AboveHaa uni0756.init_AboveHaa uni0768.init_AboveHaa uni06CE.init_AboveHaa uni0775.init_AboveHaa uni06BD.init_AboveHaa uni0626.init_AboveHaa uni066E.init_AboveHaa uni0620.init_AboveHaa uni064A.init_AboveHaa uni06BB.init_AboveHaa uni067F.init_AboveHaa uni0755.init_AboveHaa uni08A0.init_AboveHaa uni067D.init_AboveHaa uni067E.init_AboveHaa uni067B.init_AboveHaa uni0628.init_AboveHaa uni067A.init_AboveHaa uni0751.init_AboveHaa uni0646.init_AboveHaa uni0753.init_AboveHaa uni0752.init_AboveHaa uni062A.init_AboveHaa uni0678.init_AboveHaa uni063D.init_AboveHaa uni062B.init_AboveHaa uni0679.init_AboveHaa uni06B9.init_AboveHaa uni0769.init_AboveHaa uni0649.init_AboveHaa uni067C.init_AboveHaa uni0754.init_AboveHaa uni06D1.init_AboveHaa uni06D0.init_AboveHaa uni06BA.init_AboveHaa uni06CC.init_AboveHaa uni0767.init_AboveHaa ];
@aBaa.init_BaaBaaHaaInit = [ uni0777.init_BaaBaaHaaInit uni0680.init_BaaBaaHaaInit uni0776.init_BaaBaaHaaInit uni06BC.init_BaaBaaHaaInit uni0750.init_BaaBaaHaaInit uni0756.init_BaaBaaHaaInit uni0768.init_BaaBaaHaaInit uni06CE.init_BaaBaaHaaInit uni0775.init_BaaBaaHaaInit uni06BD.init_BaaBaaHaaInit uni0626.init_BaaBaaHaaInit uni066E.init_BaaBaaHaaInit uni0620.init_BaaBaaHaaInit uni064A.init_BaaBaaHaaInit uni06BB.init_BaaBaaHaaInit uni067F.init_BaaBaaHaaInit uni0755.init_BaaBaaHaaInit uni08A0.init_BaaBaaHaaInit uni067D.init_BaaBaaHaaInit uni067E.init_BaaBaaHaaInit uni067B.init_BaaBaaHaaInit uni0628.init_BaaBaaHaaInit uni067A.init_BaaBaaHaaInit uni0751.init_BaaBaaHaaInit uni0646.init_BaaBaaHaaInit uni0753.init_BaaBaaHaaInit uni0752.init_BaaBaaHaaInit uni062A.init_BaaBaaHaaInit uni0678.init_BaaBaaHaaInit uni063D.init_BaaBaaHaaInit uni062B.init_BaaBaaHaaInit uni0679.init_BaaBaaHaaInit uni06B9.init_BaaBaaHaaInit uni0769.init_BaaBaaHaaInit uni0649.init_BaaBaaHaaInit uni067C.init_BaaBaaHaaInit uni0754.init_BaaBaaHaaInit uni06D1.init_BaaBaaHaaInit uni06D0.init_BaaBaaHaaInit uni06BA.init_BaaBaaHaaInit uni06CC.init_BaaBaaHaaInit uni0767.init_BaaBaaHaaInit ];
@aBaa.init_BaaBaaHeh = [ uni0777.init_BaaBaaHeh uni0680.init_BaaBaaHeh uni0776.init_BaaBaaHeh uni06BC.init_BaaBaaHeh uni0750.init_BaaBaaHeh uni0756.init_BaaBaaHeh uni0768.init_BaaBaaHeh uni06CE.init_BaaBaaHeh uni0775.init_BaaBaaHeh uni06BD.init_BaaBaaHeh uni0626.init_BaaBaaHeh uni066E.init_BaaBaaHeh uni0620.init_BaaBaaHeh uni064A.init_BaaBaaHeh uni06BB.init_BaaBaaHeh uni067F.init_BaaBaaHeh uni0755.init_BaaBaaHeh uni08A0.init_BaaBaaHeh uni067D.init_BaaBaaHeh uni067E.init_BaaBaaHeh uni067B.init_BaaBaaHeh uni0628.init_BaaBaaHeh uni067A.init_BaaBaaHeh uni0751.init_BaaBaaHeh uni0646.init_BaaBaaHeh uni0753.init_BaaBaaHeh uni0752.init_BaaBaaHeh uni062A.init_BaaBaaHeh uni0678.init_BaaBaaHeh uni063D.init_BaaBaaHeh uni062B.init_BaaBaaHeh uni0679.init_BaaBaaHeh uni06B9.init_BaaBaaHeh uni0769.init_BaaBaaHeh uni0649.init_BaaBaaHeh uni067C.init_BaaBaaHeh uni0754.init_BaaBaaHeh uni06D1.init_BaaBaaHeh uni06D0.init_BaaBaaHeh uni06BA.init_BaaBaaHeh uni06CC.init_BaaBaaHeh uni0767.init_BaaBaaHeh ];
@aBaa.init_BaaBaaIsol = [ uni0777.init_BaaBaaIsol uni0680.init_BaaBaaIsol uni0776.init_BaaBaaIsol uni06BC.init_BaaBaaIsol uni0750.init_BaaBaaIsol uni0756.init_BaaBaaIsol uni0768.init_BaaBaaIsol uni06CE.init_BaaBaaIsol uni0775.init_BaaBaaIsol uni06BD.init_BaaBaaIsol uni0626.init_BaaBaaIsol uni066E.init_BaaBaaIsol uni0620.init_BaaBaaIsol uni064A.init_BaaBaaIsol uni06BB.init_BaaBaaIsol uni067F.init_BaaBaaIsol uni0755.init_BaaBaaIsol uni08A0.init_BaaBaaIsol uni067D.init_BaaBaaIsol uni067E.init_BaaBaaIsol uni067B.init_BaaBaaIsol uni0628.init_BaaBaaIsol uni067A.init_BaaBaaIsol uni0751.init_BaaBaaIsol uni0646.init_BaaBaaIsol uni0753.init_BaaBaaIsol uni0752.init_BaaBaaIsol uni062A.init_BaaBaaIsol uni0678.init_BaaBaaIsol uni063D.init_BaaBaaIsol uni062B.init_BaaBaaIsol uni0679.init_BaaBaaIsol uni06B9.init_BaaBaaIsol uni0769.init_BaaBaaIsol uni0649.init_BaaBaaIsol uni067C.init_BaaBaaIsol uni0754.init_BaaBaaIsol uni06D1.init_BaaBaaIsol uni06D0.init_BaaBaaIsol uni06BA.init_BaaBaaIsol uni06CC.init_BaaBaaIsol uni0767.init_BaaBaaIsol ];
@aBaa.init_BaaBaaMemInit = [ uni0777.init_BaaBaaMemInit uni0680.init_BaaBaaMemInit uni0776.init_BaaBaaMemInit uni06BC.init_BaaBaaMemInit uni0750.init_BaaBaaMemInit uni0756.init_BaaBaaMemInit uni0768.init_BaaBaaMemInit uni06CE.init_BaaBaaMemInit uni0775.init_BaaBaaMemInit uni06BD.init_BaaBaaMemInit uni0626.init_BaaBaaMemInit uni066E.init_BaaBaaMemInit uni0620.init_BaaBaaMemInit uni064A.init_BaaBaaMemInit uni06BB.init_BaaBaaMemInit uni067F.init_BaaBaaMemInit uni0755.init_BaaBaaMemInit uni08A0.init_BaaBaaMemInit uni067D.init_BaaBaaMemInit uni067E.init_BaaBaaMemInit uni067B.init_BaaBaaMemInit uni0628.init_BaaBaaMemInit uni067A.init_BaaBaaMemInit uni0751.init_BaaBaaMemInit uni0646.init_BaaBaaMemInit uni0753.init_BaaBaaMemInit uni0752.init_BaaBaaMemInit uni062A.init_BaaBaaMemInit uni0678.init_BaaBaaMemInit uni063D.init_BaaBaaMemInit uni062B.init_BaaBaaMemInit uni0679.init_BaaBaaMemInit uni06B9.init_BaaBaaMemInit uni0769.init_BaaBaaMemInit uni0649.init_BaaBaaMemInit uni067C.init_BaaBaaMemInit uni0754.init_BaaBaaMemInit uni06D1.init_BaaBaaMemInit uni06D0.init_BaaBaaMemInit uni06BA.init_BaaBaaMemInit uni06CC.init_BaaBaaMemInit uni0767.init_BaaBaaMemInit ];
@aBaa.init_BaaBaaYaa = [ uni0777.init_BaaBaaYaa uni0680.init_BaaBaaYaa uni0776.init_BaaBaaYaa uni06BC.init_BaaBaaYaa uni0750.init_BaaBaaYaa uni0756.init_BaaBaaYaa uni0768.init_BaaBaaYaa uni06CE.init_BaaBaaYaa uni0775.init_BaaBaaYaa uni06BD.init_BaaBaaYaa uni0626.init_BaaBaaYaa uni066E.init_BaaBaaYaa uni0620.init_BaaBaaYaa uni064A.init_BaaBaaYaa uni06BB.init_BaaBaaYaa uni067F.init_BaaBaaYaa uni0755.init_BaaBaaYaa uni08A0.init_BaaBaaYaa uni067D.init_BaaBaaYaa uni067E.init_BaaBaaYaa uni067B.init_BaaBaaYaa uni0628.init_BaaBaaYaa uni067A.init_BaaBaaYaa uni0751.init_BaaBaaYaa uni0646.init_BaaBaaYaa uni0753.init_BaaBaaYaa uni0752.init_BaaBaaYaa uni062A.init_BaaBaaYaa uni0678.init_BaaBaaYaa uni063D.init_BaaBaaYaa uni062B.init_BaaBaaYaa uni0679.init_BaaBaaYaa uni06B9.init_BaaBaaYaa uni0769.init_BaaBaaYaa uni0649.init_BaaBaaYaa uni067C.init_BaaBaaYaa uni0754.init_BaaBaaYaa uni06D1.init_BaaBaaYaa uni06D0.init_BaaBaaYaa uni06BA.init_BaaBaaYaa uni06CC.init_BaaBaaYaa uni0767.init_BaaBaaYaa ];
@aBaa.init_BaaDal = [ uni0777.init_BaaDal uni0680.init_BaaDal uni0776.init_BaaDal uni06BC.init_BaaDal uni0750.init_BaaDal uni0756.init_BaaDal uni0768.init_BaaDal uni06CE.init_BaaDal uni0775.init_BaaDal uni06BD.init_BaaDal uni0626.init_BaaDal uni066E.init_BaaDal uni0620.init_BaaDal uni064A.init_BaaDal uni06BB.init_BaaDal uni067F.init_BaaDal uni0755.init_BaaDal uni08A0.init_BaaDal uni067D.init_BaaDal uni067E.init_BaaDal uni067B.init_BaaDal uni0628.init_BaaDal uni067A.init_BaaDal uni0751.init_BaaDal uni0646.init_BaaDal uni0753.init_BaaDal uni0752.init_BaaDal uni062A.init_BaaDal uni0678.init_BaaDal uni063D.init_BaaDal uni062B.init_BaaDal uni0679.init_BaaDal uni06B9.init_BaaDal uni0769.init_BaaDal uni0649.init_BaaDal uni067C.init_BaaDal uni0754.init_BaaDal uni06D1.init_BaaDal uni06D0.init_BaaDal uni06BA.init_BaaDal uni06CC.init_BaaDal uni0767.init_BaaDal ];
@aBaa.init_BaaHaaInit = [ uni0777.init_BaaHaaInit uni0680.init_BaaHaaInit uni0776.init_BaaHaaInit uni06BC.init_BaaHaaInit uni0750.init_BaaHaaInit uni0756.init_BaaHaaInit uni0768.init_BaaHaaInit uni06CE.init_BaaHaaInit uni0775.init_BaaHaaInit uni06BD.init_BaaHaaInit uni0626.init_BaaHaaInit uni066E.init_BaaHaaInit uni0620.init_BaaHaaInit uni064A.init_BaaHaaInit uni06BB.init_BaaHaaInit uni067F.init_BaaHaaInit uni0755.init_BaaHaaInit uni08A0.init_BaaHaaInit uni067D.init_BaaHaaInit uni067E.init_BaaHaaInit uni067B.init_BaaHaaInit uni0628.init_BaaHaaInit uni067A.init_BaaHaaInit uni0751.init_BaaHaaInit uni0646.init_BaaHaaInit uni0753.init_BaaHaaInit uni0752.init_BaaHaaInit uni062A.init_BaaHaaInit uni0678.init_BaaHaaInit uni063D.init_BaaHaaInit uni062B.init_BaaHaaInit uni0679.init_BaaHaaInit uni06B9.init_BaaHaaInit uni0769.init_BaaHaaInit uni0649.init_BaaHaaInit uni067C.init_BaaHaaInit uni0754.init_BaaHaaInit uni06D1.init_BaaHaaInit uni06D0.init_BaaHaaInit uni06BA.init_BaaHaaInit uni06CC.init_BaaHaaInit uni0767.init_BaaHaaInit ];
@aBaa.init_BaaHaaMemInit = [ uni0777.init_BaaHaaMemInit uni0680.init_BaaHaaMemInit uni0776.init_BaaHaaMemInit uni06BC.init_BaaHaaMemInit uni0750.init_BaaHaaMemInit uni0756.init_BaaHaaMemInit uni0768.init_BaaHaaMemInit uni06CE.init_BaaHaaMemInit uni0775.init_BaaHaaMemInit uni06BD.init_BaaHaaMemInit uni0626.init_BaaHaaMemInit uni066E.init_BaaHaaMemInit uni0620.init_BaaHaaMemInit uni064A.init_BaaHaaMemInit uni06BB.init_BaaHaaMemInit uni067F.init_BaaHaaMemInit uni0755.init_BaaHaaMemInit uni08A0.init_BaaHaaMemInit uni067D.init_BaaHaaMemInit uni067E.init_BaaHaaMemInit uni067B.init_BaaHaaMemInit uni0628.init_BaaHaaMemInit uni067A.init_BaaHaaMemInit uni0751.init_BaaHaaMemInit uni0646.init_BaaHaaMemInit uni0753.init_BaaHaaMemInit uni0752.init_BaaHaaMemInit uni062A.init_BaaHaaMemInit uni0678.init_BaaHaaMemInit uni063D.init_BaaHaaMemInit uni062B.init_BaaHaaMemInit uni0679.init_BaaHaaMemInit uni06B9.init_BaaHaaMemInit uni0769.init_BaaHaaMemInit uni0649.init_BaaHaaMemInit uni067C.init_BaaHaaMemInit uni0754.init_BaaHaaMemInit uni06D1.init_BaaHaaMemInit uni06D0.init_BaaHaaMemInit uni06BA.init_BaaHaaMemInit uni06CC.init_BaaHaaMemInit uni0767.init_BaaHaaMemInit ];
@aBaa.init_BaaHehInit = [ uni0777.init_BaaHehInit uni0680.init_BaaHehInit uni0776.init_BaaHehInit uni06BC.init_BaaHehInit uni0750.init_BaaHehInit uni0756.init_BaaHehInit uni0768.init_BaaHehInit uni06CE.init_BaaHehInit uni0775.init_BaaHehInit uni06BD.init_BaaHehInit uni0626.init_BaaHehInit uni066E.init_BaaHehInit uni0620.init_BaaHehInit uni064A.init_BaaHehInit uni06BB.init_BaaHehInit uni067F.init_BaaHehInit uni0755.init_BaaHehInit uni08A0.init_BaaHehInit uni067D.init_BaaHehInit uni067E.init_BaaHehInit uni067B.init_BaaHehInit uni0628.init_BaaHehInit uni067A.init_BaaHehInit uni0751.init_BaaHehInit uni0646.init_BaaHehInit uni0753.init_BaaHehInit uni0752.init_BaaHehInit uni062A.init_BaaHehInit uni0678.init_BaaHehInit uni063D.init_BaaHehInit uni062B.init_BaaHehInit uni0679.init_BaaHehInit uni06B9.init_BaaHehInit uni0769.init_BaaHehInit uni0649.init_BaaHehInit uni067C.init_BaaHehInit uni0754.init_BaaHehInit uni06D1.init_BaaHehInit uni06D0.init_BaaHehInit uni06BA.init_BaaHehInit uni06CC.init_BaaHehInit uni0767.init_BaaHehInit ];
@aBaa.init_BaaMemHaaInit = [ uni0777.init_BaaMemHaaInit uni0680.init_BaaMemHaaInit uni0776.init_BaaMemHaaInit uni06BC.init_BaaMemHaaInit uni0750.init_BaaMemHaaInit uni0756.init_BaaMemHaaInit uni0768.init_BaaMemHaaInit uni06CE.init_BaaMemHaaInit uni0775.init_BaaMemHaaInit uni06BD.init_BaaMemHaaInit uni0626.init_BaaMemHaaInit uni066E.init_BaaMemHaaInit uni0620.init_BaaMemHaaInit uni064A.init_BaaMemHaaInit uni06BB.init_BaaMemHaaInit uni067F.init_BaaMemHaaInit uni0755.init_BaaMemHaaInit uni08A0.init_BaaMemHaaInit uni067D.init_BaaMemHaaInit uni067E.init_BaaMemHaaInit uni067B.init_BaaMemHaaInit uni0628.init_BaaMemHaaInit uni067A.init_BaaMemHaaInit uni0751.init_BaaMemHaaInit uni0646.init_BaaMemHaaInit uni0753.init_BaaMemHaaInit uni0752.init_BaaMemHaaInit uni062A.init_BaaMemHaaInit uni0678.init_BaaMemHaaInit uni063D.init_BaaMemHaaInit uni062B.init_BaaMemHaaInit uni0679.init_BaaMemHaaInit uni06B9.init_BaaMemHaaInit uni0769.init_BaaMemHaaInit uni0649.init_BaaMemHaaInit uni067C.init_BaaMemHaaInit uni0754.init_BaaMemHaaInit uni06D1.init_BaaMemHaaInit uni06D0.init_BaaMemHaaInit uni06BA.init_BaaMemHaaInit uni06CC.init_BaaMemHaaInit uni0767.init_BaaMemHaaInit ];
@aBaa.init_BaaMemInit = [ uni0777.init_BaaMemInit uni0680.init_BaaMemInit uni0776.init_BaaMemInit uni06BC.init_BaaMemInit uni0750.init_BaaMemInit uni0756.init_BaaMemInit uni0768.init_BaaMemInit uni06CE.init_BaaMemInit uni0775.init_BaaMemInit uni06BD.init_BaaMemInit uni0626.init_BaaMemInit uni066E.init_BaaMemInit uni0620.init_BaaMemInit uni064A.init_BaaMemInit uni06BB.init_BaaMemInit uni067F.init_BaaMemInit uni0755.init_BaaMemInit uni08A0.init_BaaMemInit uni067D.init_BaaMemInit uni067E.init_BaaMemInit uni067B.init_BaaMemInit uni0628.init_BaaMemInit uni067A.init_BaaMemInit uni0751.init_BaaMemInit uni0646.init_BaaMemInit uni0753.init_BaaMemInit uni0752.init_BaaMemInit uni062A.init_BaaMemInit uni0678.init_BaaMemInit uni063D.init_BaaMemInit uni062B.init_BaaMemInit uni0679.init_BaaMemInit uni06B9.init_BaaMemInit uni0769.init_BaaMemInit uni0649.init_BaaMemInit uni067C.init_BaaMemInit uni0754.init_BaaMemInit uni06D1.init_BaaMemInit uni06D0.init_BaaMemInit uni06BA.init_BaaMemInit uni06CC.init_BaaMemInit uni0767.init_BaaMemInit ];
@aBaa.init_BaaMemIsol = [ uni0777.init_BaaMemIsol uni0680.init_BaaMemIsol uni0776.init_BaaMemIsol uni06BC.init_BaaMemIsol uni0750.init_BaaMemIsol uni0756.init_BaaMemIsol uni0768.init_BaaMemIsol uni06CE.init_BaaMemIsol uni0775.init_BaaMemIsol uni06BD.init_BaaMemIsol uni0626.init_BaaMemIsol uni066E.init_BaaMemIsol uni0620.init_BaaMemIsol uni064A.init_BaaMemIsol uni06BB.init_BaaMemIsol uni067F.init_BaaMemIsol uni0755.init_BaaMemIsol uni08A0.init_BaaMemIsol uni067D.init_BaaMemIsol uni067E.init_BaaMemIsol uni067B.init_BaaMemIsol uni0628.init_BaaMemIsol uni067A.init_BaaMemIsol uni0751.init_BaaMemIsol uni0646.init_BaaMemIsol uni0753.init_BaaMemIsol uni0752.init_BaaMemIsol uni062A.init_BaaMemIsol uni0678.init_BaaMemIsol uni063D.init_BaaMemIsol uni062B.init_BaaMemIsol uni0679.init_BaaMemIsol uni06B9.init_BaaMemIsol uni0769.init_BaaMemIsol uni0649.init_BaaMemIsol uni067C.init_BaaMemIsol uni0754.init_BaaMemIsol uni06D1.init_BaaMemIsol uni06D0.init_BaaMemIsol uni06BA.init_BaaMemIsol uni06CC.init_BaaMemIsol uni0767.init_BaaMemIsol ];
@aBaa.init_BaaNonIsol = [ uni0777.init_BaaNonIsol uni0680.init_BaaNonIsol uni0776.init_BaaNonIsol uni06BC.init_BaaNonIsol uni0750.init_BaaNonIsol uni0756.init_BaaNonIsol uni0768.init_BaaNonIsol uni06CE.init_BaaNonIsol uni0775.init_BaaNonIsol uni06BD.init_BaaNonIsol uni0626.init_BaaNonIsol uni066E.init_BaaNonIsol uni0620.init_BaaNonIsol uni064A.init_BaaNonIsol uni06BB.init_BaaNonIsol uni067F.init_BaaNonIsol uni0755.init_BaaNonIsol uni08A0.init_BaaNonIsol uni067D.init_BaaNonIsol uni067E.init_BaaNonIsol uni067B.init_BaaNonIsol uni0628.init_BaaNonIsol uni067A.init_BaaNonIsol uni0751.init_BaaNonIsol uni0646.init_BaaNonIsol uni0753.init_BaaNonIsol uni0752.init_BaaNonIsol uni062A.init_BaaNonIsol uni0678.init_BaaNonIsol uni063D.init_BaaNonIsol uni062B.init_BaaNonIsol uni0679.init_BaaNonIsol uni06B9.init_BaaNonIsol uni0769.init_BaaNonIsol uni0649.init_BaaNonIsol uni067C.init_BaaNonIsol uni0754.init_BaaNonIsol uni06D1.init_BaaNonIsol uni06D0.init_BaaNonIsol uni06BA.init_BaaNonIsol uni06CC.init_BaaNonIsol uni0767.init_BaaNonIsol ];
@aBaa.init_BaaRaaIsol = [ uni0777.init_BaaRaaIsol uni0680.init_BaaRaaIsol uni0776.init_BaaRaaIsol uni06BC.init_BaaRaaIsol uni0750.init_BaaRaaIsol uni0756.init_BaaRaaIsol uni0768.init_BaaRaaIsol uni06CE.init_BaaRaaIsol uni0775.init_BaaRaaIsol uni06BD.init_BaaRaaIsol uni0626.init_BaaRaaIsol uni066E.init_BaaRaaIsol uni0620.init_BaaRaaIsol uni064A.init_BaaRaaIsol uni06BB.init_BaaRaaIsol uni067F.init_BaaRaaIsol uni0755.init_BaaRaaIsol uni08A0.init_BaaRaaIsol uni067D.init_BaaRaaIsol uni067E.init_BaaRaaIsol uni067B.init_BaaRaaIsol uni0628.init_BaaRaaIsol uni067A.init_BaaRaaIsol uni0751.init_BaaRaaIsol uni0646.init_BaaRaaIsol uni0753.init_BaaRaaIsol uni0752.init_BaaRaaIsol uni062A.init_BaaRaaIsol uni0678.init_BaaRaaIsol uni063D.init_BaaRaaIsol uni062B.init_BaaRaaIsol uni0679.init_BaaRaaIsol uni06B9.init_BaaRaaIsol uni0769.init_BaaRaaIsol uni0649.init_BaaRaaIsol uni067C.init_BaaRaaIsol uni0754.init_BaaRaaIsol uni06D1.init_BaaRaaIsol uni06D0.init_BaaRaaIsol uni06BA.init_BaaRaaIsol uni06CC.init_BaaRaaIsol uni0767.init_BaaRaaIsol ];
@aBaa.init_BaaSenAltInit = [ uni0777.init_BaaSenAltInit uni0680.init_BaaSenAltInit uni0776.init_BaaSenAltInit uni06BC.init_BaaSenAltInit uni0750.init_BaaSenAltInit uni0756.init_BaaSenAltInit uni0768.init_BaaSenAltInit uni06CE.init_BaaSenAltInit uni0775.init_BaaSenAltInit uni06BD.init_BaaSenAltInit uni0626.init_BaaSenAltInit uni066E.init_BaaSenAltInit uni0620.init_BaaSenAltInit uni064A.init_BaaSenAltInit uni06BB.init_BaaSenAltInit uni067F.init_BaaSenAltInit uni0755.init_BaaSenAltInit uni08A0.init_BaaSenAltInit uni067D.init_BaaSenAltInit uni067E.init_BaaSenAltInit uni067B.init_BaaSenAltInit uni0628.init_BaaSenAltInit uni067A.init_BaaSenAltInit uni0751.init_BaaSenAltInit uni0646.init_BaaSenAltInit uni0753.init_BaaSenAltInit uni0752.init_BaaSenAltInit uni062A.init_BaaSenAltInit uni0678.init_BaaSenAltInit uni063D.init_BaaSenAltInit uni062B.init_BaaSenAltInit uni0679.init_BaaSenAltInit uni06B9.init_BaaSenAltInit uni0769.init_BaaSenAltInit uni0649.init_BaaSenAltInit uni067C.init_BaaSenAltInit uni0754.init_BaaSenAltInit uni06D1.init_BaaSenAltInit uni06D0.init_BaaSenAltInit uni06BA.init_BaaSenAltInit uni06CC.init_BaaSenAltInit uni0767.init_BaaSenAltInit ];
@aBaa.init_BaaSenInit = [ uni0777.init_BaaSenInit uni0680.init_BaaSenInit uni0776.init_BaaSenInit uni06BC.init_BaaSenInit uni0750.init_BaaSenInit uni0756.init_BaaSenInit uni0768.init_BaaSenInit uni06CE.init_BaaSenInit uni0775.init_BaaSenInit uni06BD.init_BaaSenInit uni0626.init_BaaSenInit uni066E.init_BaaSenInit uni0620.init_BaaSenInit uni064A.init_BaaSenInit uni06BB.init_BaaSenInit uni067F.init_BaaSenInit uni0755.init_BaaSenInit uni08A0.init_BaaSenInit uni067D.init_BaaSenInit uni067E.init_BaaSenInit uni067B.init_BaaSenInit uni0628.init_BaaSenInit uni067A.init_BaaSenInit uni0751.init_BaaSenInit uni0646.init_BaaSenInit uni0753.init_BaaSenInit uni0752.init_BaaSenInit uni062A.init_BaaSenInit uni0678.init_BaaSenInit uni063D.init_BaaSenInit uni062B.init_BaaSenInit uni0679.init_BaaSenInit uni06B9.init_BaaSenInit uni0769.init_BaaSenInit uni0649.init_BaaSenInit uni067C.init_BaaSenInit uni0754.init_BaaSenInit uni06D1.init_BaaSenInit uni06D0.init_BaaSenInit uni06BA.init_BaaSenInit uni06CC.init_BaaSenInit uni0767.init_BaaSenInit ];
@aBaa.init_BaaYaaIsol = [ uni0777.init_BaaYaaIsol uni0680.init_BaaYaaIsol uni0776.init_BaaYaaIsol uni06BC.init_BaaYaaIsol uni0750.init_BaaYaaIsol uni0756.init_BaaYaaIsol uni0768.init_BaaYaaIsol uni06CE.init_BaaYaaIsol uni0775.init_BaaYaaIsol uni06BD.init_BaaYaaIsol uni0626.init_BaaYaaIsol uni066E.init_BaaYaaIsol uni0620.init_BaaYaaIsol uni064A.init_BaaYaaIsol uni06BB.init_BaaYaaIsol uni067F.init_BaaYaaIsol uni0755.init_BaaYaaIsol uni08A0.init_BaaYaaIsol uni067D.init_BaaYaaIsol uni067E.init_BaaYaaIsol uni067B.init_BaaYaaIsol uni0628.init_BaaYaaIsol uni067A.init_BaaYaaIsol uni0751.init_BaaYaaIsol uni0646.init_BaaYaaIsol uni0753.init_BaaYaaIsol uni0752.init_BaaYaaIsol uni062A.init_BaaYaaIsol uni0678.init_BaaYaaIsol uni063D.init_BaaYaaIsol uni062B.init_BaaYaaIsol uni0679.init_BaaYaaIsol uni06B9.init_BaaYaaIsol uni0769.init_BaaYaaIsol uni0649.init_BaaYaaIsol uni067C.init_BaaYaaIsol uni0754.init_BaaYaaIsol uni06D1.init_BaaYaaIsol uni06D0.init_BaaYaaIsol uni06BA.init_BaaYaaIsol uni06CC.init_BaaYaaIsol uni0767.init_BaaYaaIsol ];
@aBaa.init_High = [ uni0777.init_High uni0680.init_High uni0776.init_High uni06BC.init_High uni0750.init_High uni0756.init_High uni0768.init_High uni06CE.init_High uni0775.init_High uni06BD.init_High uni0626.init_High uni066E.init_High uni0620.init_High uni064A.init_High uni06BB.init_High uni067F.init_High uni0755.init_High uni08A0.init_High uni067D.init_High uni067E.init_High uni067B.init_High uni0628.init_High uni067A.init_High uni0751.init_High uni0646.init_High uni0753.init_High uni0752.init_High uni062A.init_High uni0678.init_High uni063D.init_High uni062B.init_High uni0679.init_High uni06B9.init_High uni0769.init_High uni0649.init_High uni067C.init_High uni0754.init_High uni06D1.init_High uni06D0.init_High uni06BA.init_High uni06CC.init_High uni0767.init_High ];
@aBaa.init_Wide = [ uni0777.init_Wide uni0680.init_Wide uni0776.init_Wide uni06BC.init_Wide uni0750.init_Wide uni0756.init_Wide uni0768.init_Wide uni06CE.init_Wide uni0775.init_Wide uni06BD.init_Wide uni0626.init_Wide uni066E.init_Wide uni0620.init_Wide uni064A.init_Wide uni06BB.init_Wide uni067F.init_Wide uni0755.init_Wide uni08A0.init_Wide uni067D.init_Wide uni067E.init_Wide uni067B.init_Wide uni0628.init_Wide uni067A.init_Wide uni0751.init_Wide uni0646.init_Wide uni0753.init_Wide uni0752.init_Wide uni062A.init_Wide uni0678.init_Wide uni063D.init_Wide uni062B.init_Wide uni0679.init_Wide uni06B9.init_Wide uni0769.init_Wide uni0649.init_Wide uni067C.init_Wide uni0754.init_Wide uni06D1.init_Wide uni06D0.init_Wide uni06BA.init_Wide uni06CC.init_Wide uni0767.init_Wide ];
@aBaa.init_YaaBarree = [ uni0777.init_YaaBarree uni0680.init_YaaBarree uni0776.init_YaaBarree uni06BC.init_YaaBarree uni0750.init_YaaBarree uni0756.init_YaaBarree uni0768.init_YaaBarree uni06CE.init_YaaBarree uni0775.init_YaaBarree uni06BD.init_YaaBarree uni0626.init_YaaBarree uni066E.init_YaaBarree uni0620.init_YaaBarree uni064A.init_YaaBarree uni06BB.init_YaaBarree uni067F.init_YaaBarree uni0755.init_YaaBarree uni08A0.init_YaaBarree uni067D.init_YaaBarree uni067E.init_YaaBarree uni067B.init_YaaBarree uni0628.init_YaaBarree uni067A.init_YaaBarree uni0751.init_YaaBarree uni0646.init_YaaBarree uni0753.init_YaaBarree uni0752.init_YaaBarree uni062A.init_YaaBarree uni0678.init_YaaBarree uni063D.init_YaaBarree uni062B.init_YaaBarree uni0679.init_YaaBarree uni06B9.init_YaaBarree uni0769.init_YaaBarree uni0649.init_YaaBarree uni067C.init_YaaBarree uni0754.init_YaaBarree uni06D1.init_YaaBarree uni06D0.init_YaaBarree uni06BA.init_YaaBarree uni06CC.init_YaaBarree uni0767.init_YaaBarree ];
@aBaa.medi_BaaBaaHaaInit = [ uni0777.medi_BaaBaaHaaInit uni0680.medi_BaaBaaHaaInit uni0776.medi_BaaBaaHaaInit uni06BC.medi_BaaBaaHaaInit uni0750.medi_BaaBaaHaaInit uni0756.medi_BaaBaaHaaInit uni0768.medi_BaaBaaHaaInit uni06CE.medi_BaaBaaHaaInit uni0775.medi_BaaBaaHaaInit uni06BD.medi_BaaBaaHaaInit uni0626.medi_BaaBaaHaaInit uni066E.medi_BaaBaaHaaInit uni0620.medi_BaaBaaHaaInit uni064A.medi_BaaBaaHaaInit uni06BB.medi_BaaBaaHaaInit uni067F.medi_BaaBaaHaaInit uni0755.medi_BaaBaaHaaInit uni08A0.medi_BaaBaaHaaInit uni067D.medi_BaaBaaHaaInit uni067E.medi_BaaBaaHaaInit uni067B.medi_BaaBaaHaaInit uni0628.medi_BaaBaaHaaInit uni067A.medi_BaaBaaHaaInit uni0751.medi_BaaBaaHaaInit uni0646.medi_BaaBaaHaaInit uni0753.medi_BaaBaaHaaInit uni0752.medi_BaaBaaHaaInit uni062A.medi_BaaBaaHaaInit uni0678.medi_BaaBaaHaaInit uni063D.medi_BaaBaaHaaInit uni062B.medi_BaaBaaHaaInit uni0679.medi_BaaBaaHaaInit uni06B9.medi_BaaBaaHaaInit uni0769.medi_BaaBaaHaaInit uni0649.medi_BaaBaaHaaInit uni067C.medi_BaaBaaHaaInit uni0754.medi_BaaBaaHaaInit uni06D1.medi_BaaBaaHaaInit uni06D0.medi_BaaBaaHaaInit uni06BA.medi_BaaBaaHaaInit uni06CC.medi_BaaBaaHaaInit uni0767.medi_BaaBaaHaaInit ];
@aBaa.medi_BaaBaaInit = [ uni0777.medi_BaaBaaInit uni0680.medi_BaaBaaInit uni0776.medi_BaaBaaInit uni06BC.medi_BaaBaaInit uni0750.medi_BaaBaaInit uni0756.medi_BaaBaaInit uni0768.medi_BaaBaaInit uni06CE.medi_BaaBaaInit uni0775.medi_BaaBaaInit uni06BD.medi_BaaBaaInit uni0626.medi_BaaBaaInit uni066E.medi_BaaBaaInit uni0620.medi_BaaBaaInit uni064A.medi_BaaBaaInit uni06BB.medi_BaaBaaInit uni067F.medi_BaaBaaInit uni0755.medi_BaaBaaInit uni08A0.medi_BaaBaaInit uni067D.medi_BaaBaaInit uni067E.medi_BaaBaaInit uni067B.medi_BaaBaaInit uni0628.medi_BaaBaaInit uni067A.medi_BaaBaaInit uni0751.medi_BaaBaaInit uni0646.medi_BaaBaaInit uni0753.medi_BaaBaaInit uni0752.medi_BaaBaaInit uni062A.medi_BaaBaaInit uni0678.medi_BaaBaaInit uni063D.medi_BaaBaaInit uni062B.medi_BaaBaaInit uni0679.medi_BaaBaaInit uni06B9.medi_BaaBaaInit uni0769.medi_BaaBaaInit uni0649.medi_BaaBaaInit uni067C.medi_BaaBaaInit uni0754.medi_BaaBaaInit uni06D1.medi_BaaBaaInit uni06D0.medi_BaaBaaInit uni06BA.medi_BaaBaaInit uni06CC.medi_BaaBaaInit uni0767.medi_BaaBaaInit ];
@aBaa.medi_BaaBaaMemInit = [ uni0777.medi_BaaBaaMemInit uni0680.medi_BaaBaaMemInit uni0776.medi_BaaBaaMemInit uni06BC.medi_BaaBaaMemInit uni0750.medi_BaaBaaMemInit uni0756.medi_BaaBaaMemInit uni0768.medi_BaaBaaMemInit uni06CE.medi_BaaBaaMemInit uni0775.medi_BaaBaaMemInit uni06BD.medi_BaaBaaMemInit uni0626.medi_BaaBaaMemInit uni066E.medi_BaaBaaMemInit uni0620.medi_BaaBaaMemInit uni064A.medi_BaaBaaMemInit uni06BB.medi_BaaBaaMemInit uni067F.medi_BaaBaaMemInit uni0755.medi_BaaBaaMemInit uni08A0.medi_BaaBaaMemInit uni067D.medi_BaaBaaMemInit uni067E.medi_BaaBaaMemInit uni067B.medi_BaaBaaMemInit uni0628.medi_BaaBaaMemInit uni067A.medi_BaaBaaMemInit uni0751.medi_BaaBaaMemInit uni0646.medi_BaaBaaMemInit uni0753.medi_BaaBaaMemInit uni0752.medi_BaaBaaMemInit uni062A.medi_BaaBaaMemInit uni0678.medi_BaaBaaMemInit uni063D.medi_BaaBaaMemInit uni062B.medi_BaaBaaMemInit uni0679.medi_BaaBaaMemInit uni06B9.medi_BaaBaaMemInit uni0769.medi_BaaBaaMemInit uni0649.medi_BaaBaaMemInit uni067C.medi_BaaBaaMemInit uni0754.medi_BaaBaaMemInit uni06D1.medi_BaaBaaMemInit uni06D0.medi_BaaBaaMemInit uni06BA.medi_BaaBaaMemInit uni06CC.medi_BaaBaaMemInit uni0767.medi_BaaBaaMemInit ];
@aBaa.medi_BaaBaaYaa = [ uni0777.medi_BaaBaaYaa uni0680.medi_BaaBaaYaa uni0776.medi_BaaBaaYaa uni06BC.medi_BaaBaaYaa uni0750.medi_BaaBaaYaa uni0756.medi_BaaBaaYaa uni0768.medi_BaaBaaYaa uni06CE.medi_BaaBaaYaa uni0775.medi_BaaBaaYaa uni06BD.medi_BaaBaaYaa uni0626.medi_BaaBaaYaa uni066E.medi_BaaBaaYaa uni0620.medi_BaaBaaYaa uni064A.medi_BaaBaaYaa uni06BB.medi_BaaBaaYaa uni067F.medi_BaaBaaYaa uni0755.medi_BaaBaaYaa uni08A0.medi_BaaBaaYaa uni067D.medi_BaaBaaYaa uni067E.medi_BaaBaaYaa uni067B.medi_BaaBaaYaa uni0628.medi_BaaBaaYaa uni067A.medi_BaaBaaYaa uni0751.medi_BaaBaaYaa uni0646.medi_BaaBaaYaa uni0753.medi_BaaBaaYaa uni0752.medi_BaaBaaYaa uni062A.medi_BaaBaaYaa uni0678.medi_BaaBaaYaa uni063D.medi_BaaBaaYaa uni062B.medi_BaaBaaYaa uni0679.medi_BaaBaaYaa uni06B9.medi_BaaBaaYaa uni0769.medi_BaaBaaYaa uni0649.medi_BaaBaaYaa uni067C.medi_BaaBaaYaa uni0754.medi_BaaBaaYaa uni06D1.medi_BaaBaaYaa uni06D0.medi_BaaBaaYaa uni06BA.medi_BaaBaaYaa uni06CC.medi_BaaBaaYaa uni0767.medi_BaaBaaYaa ];
@aBaa.medi_BaaHehMedi = [ uni0777.medi_BaaHehMedi uni0680.medi_BaaHehMedi uni0776.medi_BaaHehMedi uni06BC.medi_BaaHehMedi uni0750.medi_BaaHehMedi uni0756.medi_BaaHehMedi uni0768.medi_BaaHehMedi uni06CE.medi_BaaHehMedi uni0775.medi_BaaHehMedi uni06BD.medi_BaaHehMedi uni0626.medi_BaaHehMedi uni066E.medi_BaaHehMedi uni0620.medi_BaaHehMedi uni064A.medi_BaaHehMedi uni06BB.medi_BaaHehMedi uni067F.medi_BaaHehMedi uni0755.medi_BaaHehMedi uni08A0.medi_BaaHehMedi uni067D.medi_BaaHehMedi uni067E.medi_BaaHehMedi uni067B.medi_BaaHehMedi uni0628.medi_BaaHehMedi uni067A.medi_BaaHehMedi uni0751.medi_BaaHehMedi uni0646.medi_BaaHehMedi uni0753.medi_BaaHehMedi uni0752.medi_BaaHehMedi uni062A.medi_BaaHehMedi uni0678.medi_BaaHehMedi uni063D.medi_BaaHehMedi uni062B.medi_BaaHehMedi uni0679.medi_BaaHehMedi uni06B9.medi_BaaHehMedi uni0769.medi_BaaHehMedi uni0649.medi_BaaHehMedi uni067C.medi_BaaHehMedi uni0754.medi_BaaHehMedi uni06D1.medi_BaaHehMedi uni06D0.medi_BaaHehMedi uni06BA.medi_BaaHehMedi uni06CC.medi_BaaHehMedi uni0767.medi_BaaHehMedi ];
@aBaa.medi_BaaMemAlfFina = [ uni0777.medi_BaaMemAlfFina uni0680.medi_BaaMemAlfFina uni0776.medi_BaaMemAlfFina uni06BC.medi_BaaMemAlfFina uni0750.medi_BaaMemAlfFina uni0756.medi_BaaMemAlfFina uni0768.medi_BaaMemAlfFina uni06CE.medi_BaaMemAlfFina uni0775.medi_BaaMemAlfFina uni06BD.medi_BaaMemAlfFina uni0626.medi_BaaMemAlfFina uni066E.medi_BaaMemAlfFina uni0620.medi_BaaMemAlfFina uni064A.medi_BaaMemAlfFina uni06BB.medi_BaaMemAlfFina uni067F.medi_BaaMemAlfFina uni0755.medi_BaaMemAlfFina uni08A0.medi_BaaMemAlfFina uni067D.medi_BaaMemAlfFina uni067E.medi_BaaMemAlfFina uni067B.medi_BaaMemAlfFina uni0628.medi_BaaMemAlfFina uni067A.medi_BaaMemAlfFina uni0751.medi_BaaMemAlfFina uni0646.medi_BaaMemAlfFina uni0753.medi_BaaMemAlfFina uni0752.medi_BaaMemAlfFina uni062A.medi_BaaMemAlfFina uni0678.medi_BaaMemAlfFina uni063D.medi_BaaMemAlfFina uni062B.medi_BaaMemAlfFina uni0679.medi_BaaMemAlfFina uni06B9.medi_BaaMemAlfFina uni0769.medi_BaaMemAlfFina uni0649.medi_BaaMemAlfFina uni067C.medi_BaaMemAlfFina uni0754.medi_BaaMemAlfFina uni06D1.medi_BaaMemAlfFina uni06D0.medi_BaaMemAlfFina uni06BA.medi_BaaMemAlfFina uni06CC.medi_BaaMemAlfFina uni0767.medi_BaaMemAlfFina ];
@aBaa.medi_BaaMemFina = [ uni0777.medi_BaaMemFina uni0680.medi_BaaMemFina uni0776.medi_BaaMemFina uni06BC.medi_BaaMemFina uni0750.medi_BaaMemFina uni0756.medi_BaaMemFina uni0768.medi_BaaMemFina uni06CE.medi_BaaMemFina uni0775.medi_BaaMemFina uni06BD.medi_BaaMemFina uni0626.medi_BaaMemFina uni066E.medi_BaaMemFina uni0620.medi_BaaMemFina uni064A.medi_BaaMemFina uni06BB.medi_BaaMemFina uni067F.medi_BaaMemFina uni0755.medi_BaaMemFina uni08A0.medi_BaaMemFina uni067D.medi_BaaMemFina uni067E.medi_BaaMemFina uni067B.medi_BaaMemFina uni0628.medi_BaaMemFina uni067A.medi_BaaMemFina uni0751.medi_BaaMemFina uni0646.medi_BaaMemFina uni0753.medi_BaaMemFina uni0752.medi_BaaMemFina uni062A.medi_BaaMemFina uni0678.medi_BaaMemFina uni063D.medi_BaaMemFina uni062B.medi_BaaMemFina uni0679.medi_BaaMemFina uni06B9.medi_BaaMemFina uni0769.medi_BaaMemFina uni0649.medi_BaaMemFina uni067C.medi_BaaMemFina uni0754.medi_BaaMemFina uni06D1.medi_BaaMemFina uni06D0.medi_BaaMemFina uni06BA.medi_BaaMemFina uni06CC.medi_BaaMemFina uni0767.medi_BaaMemFina ];
@aBaa.medi_BaaNonFina = [ uni0777.medi_BaaNonFina uni0680.medi_BaaNonFina uni0776.medi_BaaNonFina uni06BC.medi_BaaNonFina uni0750.medi_BaaNonFina uni0756.medi_BaaNonFina uni0768.medi_BaaNonFina uni06CE.medi_BaaNonFina uni0775.medi_BaaNonFina uni06BD.medi_BaaNonFina uni0626.medi_BaaNonFina uni066E.medi_BaaNonFina uni0620.medi_BaaNonFina uni064A.medi_BaaNonFina uni06BB.medi_BaaNonFina uni067F.medi_BaaNonFina uni0755.medi_BaaNonFina uni08A0.medi_BaaNonFina uni067D.medi_BaaNonFina uni067E.medi_BaaNonFina uni067B.medi_BaaNonFina uni0628.medi_BaaNonFina uni067A.medi_BaaNonFina uni0751.medi_BaaNonFina uni0646.medi_BaaNonFina uni0753.medi_BaaNonFina uni0752.medi_BaaNonFina uni062A.medi_BaaNonFina uni0678.medi_BaaNonFina uni063D.medi_BaaNonFina uni062B.medi_BaaNonFina uni0679.medi_BaaNonFina uni06B9.medi_BaaNonFina uni0769.medi_BaaNonFina uni0649.medi_BaaNonFina uni067C.medi_BaaNonFina uni0754.medi_BaaNonFina uni06D1.medi_BaaNonFina uni06D0.medi_BaaNonFina uni06BA.medi_BaaNonFina uni06CC.medi_BaaNonFina uni0767.medi_BaaNonFina ];
@aBaa.medi_BaaRaaFina = [ uni0777.medi_BaaRaaFina uni0680.medi_BaaRaaFina uni0776.medi_BaaRaaFina uni06BC.medi_BaaRaaFina uni0750.medi_BaaRaaFina uni0756.medi_BaaRaaFina uni0768.medi_BaaRaaFina uni06CE.medi_BaaRaaFina uni0775.medi_BaaRaaFina uni06BD.medi_BaaRaaFina uni0626.medi_BaaRaaFina uni066E.medi_BaaRaaFina uni0620.medi_BaaRaaFina uni064A.medi_BaaRaaFina uni06BB.medi_BaaRaaFina uni067F.medi_BaaRaaFina uni0755.medi_BaaRaaFina uni08A0.medi_BaaRaaFina uni067D.medi_BaaRaaFina uni067E.medi_BaaRaaFina uni067B.medi_BaaRaaFina uni0628.medi_BaaRaaFina uni067A.medi_BaaRaaFina uni0751.medi_BaaRaaFina uni0646.medi_BaaRaaFina uni0753.medi_BaaRaaFina uni0752.medi_BaaRaaFina uni062A.medi_BaaRaaFina uni0678.medi_BaaRaaFina uni063D.medi_BaaRaaFina uni062B.medi_BaaRaaFina uni0679.medi_BaaRaaFina uni06B9.medi_BaaRaaFina uni0769.medi_BaaRaaFina uni0649.medi_BaaRaaFina uni067C.medi_BaaRaaFina uni0754.medi_BaaRaaFina uni06D1.medi_BaaRaaFina uni06D0.medi_BaaRaaFina uni06BA.medi_BaaRaaFina uni06CC.medi_BaaRaaFina uni0767.medi_BaaRaaFina ];
@aBaa.medi_BaaYaaFina = [ uni0777.medi_BaaYaaFina uni0680.medi_BaaYaaFina uni0776.medi_BaaYaaFina uni06BC.medi_BaaYaaFina uni0750.medi_BaaYaaFina uni0756.medi_BaaYaaFina uni0768.medi_BaaYaaFina uni06CE.medi_BaaYaaFina uni0775.medi_BaaYaaFina uni06BD.medi_BaaYaaFina uni0626.medi_BaaYaaFina uni066E.medi_BaaYaaFina uni0620.medi_BaaYaaFina uni064A.medi_BaaYaaFina uni06BB.medi_BaaYaaFina uni067F.medi_BaaYaaFina uni0755.medi_BaaYaaFina uni08A0.medi_BaaYaaFina uni067D.medi_BaaYaaFina uni067E.medi_BaaYaaFina uni067B.medi_BaaYaaFina uni0628.medi_BaaYaaFina uni067A.medi_BaaYaaFina uni0751.medi_BaaYaaFina uni0646.medi_BaaYaaFina uni0753.medi_BaaYaaFina uni0752.medi_BaaYaaFina uni062A.medi_BaaYaaFina uni0678.medi_BaaYaaFina uni063D.medi_BaaYaaFina uni062B.medi_BaaYaaFina uni0679.medi_BaaYaaFina uni06B9.medi_BaaYaaFina uni0769.medi_BaaYaaFina uni0649.medi_BaaYaaFina uni067C.medi_BaaYaaFina uni0754.medi_BaaYaaFina uni06D1.medi_BaaYaaFina uni06D0.medi_BaaYaaFina uni06BA.medi_BaaYaaFina uni06CC.medi_BaaYaaFina uni0767.medi_BaaYaaFina ];
@aBaa.medi_High = [ uni0777.medi_High uni0680.medi_High uni0776.medi_High uni06BC.medi_High uni0750.medi_High uni0756.medi_High uni0768.medi_High uni06CE.medi_High uni0775.medi_High uni06BD.medi_High uni0626.medi_High uni066E.medi_High uni0620.medi_High uni064A.medi_High uni06BB.medi_High uni067F.medi_High uni0755.medi_High uni08A0.medi_High uni067D.medi_High uni067E.medi_High uni067B.medi_High uni0628.medi_High uni067A.medi_High uni0751.medi_High uni0646.medi_High uni0753.medi_High uni0752.medi_High uni062A.medi_High uni0678.medi_High uni063D.medi_High uni062B.medi_High uni0679.medi_High uni06B9.medi_High uni0769.medi_High uni0649.medi_High uni067C.medi_High uni0754.medi_High uni06D1.medi_High uni06D0.medi_High uni06BA.medi_High uni06CC.medi_High uni0767.medi_High ];
@aBaa.medi_KafBaaInit = [ uni0777.medi_KafBaaInit uni0680.medi_KafBaaInit uni0776.medi_KafBaaInit uni06BC.medi_KafBaaInit uni0750.medi_KafBaaInit uni0756.medi_KafBaaInit uni0768.medi_KafBaaInit uni06CE.medi_KafBaaInit uni0775.medi_KafBaaInit uni06BD.medi_KafBaaInit uni0626.medi_KafBaaInit uni066E.medi_KafBaaInit uni0620.medi_KafBaaInit uni064A.medi_KafBaaInit uni06BB.medi_KafBaaInit uni067F.medi_KafBaaInit uni0755.medi_KafBaaInit uni08A0.medi_KafBaaInit uni067D.medi_KafBaaInit uni067E.medi_KafBaaInit uni067B.medi_KafBaaInit uni0628.medi_KafBaaInit uni067A.medi_KafBaaInit uni0751.medi_KafBaaInit uni0646.medi_KafBaaInit uni0753.medi_KafBaaInit uni0752.medi_KafBaaInit uni062A.medi_KafBaaInit uni0678.medi_KafBaaInit uni063D.medi_KafBaaInit uni062B.medi_KafBaaInit uni0679.medi_KafBaaInit uni06B9.medi_KafBaaInit uni0769.medi_KafBaaInit uni0649.medi_KafBaaInit uni067C.medi_KafBaaInit uni0754.medi_KafBaaInit uni06D1.medi_KafBaaInit uni06D0.medi_KafBaaInit uni06BA.medi_KafBaaInit uni06CC.medi_KafBaaInit uni0767.medi_KafBaaInit ];
@aBaa.medi_KafBaaMedi = [ uni0777.medi_KafBaaMedi uni0680.medi_KafBaaMedi uni0776.medi_KafBaaMedi uni06BC.medi_KafBaaMedi uni0750.medi_KafBaaMedi uni0756.medi_KafBaaMedi uni0768.medi_KafBaaMedi uni06CE.medi_KafBaaMedi uni0775.medi_KafBaaMedi uni06BD.medi_KafBaaMedi uni0626.medi_KafBaaMedi uni066E.medi_KafBaaMedi uni0620.medi_KafBaaMedi uni064A.medi_KafBaaMedi uni06BB.medi_KafBaaMedi uni067F.medi_KafBaaMedi uni0755.medi_KafBaaMedi uni08A0.medi_KafBaaMedi uni067D.medi_KafBaaMedi uni067E.medi_KafBaaMedi uni067B.medi_KafBaaMedi uni0628.medi_KafBaaMedi uni067A.medi_KafBaaMedi uni0751.medi_KafBaaMedi uni0646.medi_KafBaaMedi uni0753.medi_KafBaaMedi uni0752.medi_KafBaaMedi uni062A.medi_KafBaaMedi uni0678.medi_KafBaaMedi uni063D.medi_KafBaaMedi uni062B.medi_KafBaaMedi uni0679.medi_KafBaaMedi uni06B9.medi_KafBaaMedi uni0769.medi_KafBaaMedi uni0649.medi_KafBaaMedi uni067C.medi_KafBaaMedi uni0754.medi_KafBaaMedi uni06D1.medi_KafBaaMedi uni06D0.medi_KafBaaMedi uni06BA.medi_KafBaaMedi uni06CC.medi_KafBaaMedi uni0767.medi_KafBaaMedi ];
@aBaa.medi_LamBaaMemInit = [ uni0777.medi_LamBaaMemInit uni0680.medi_LamBaaMemInit uni0776.medi_LamBaaMemInit uni06BC.medi_LamBaaMemInit uni0750.medi_LamBaaMemInit uni0756.medi_LamBaaMemInit uni0768.medi_LamBaaMemInit uni06CE.medi_LamBaaMemInit uni0775.medi_LamBaaMemInit uni06BD.medi_LamBaaMemInit uni0626.medi_LamBaaMemInit uni066E.medi_LamBaaMemInit uni0620.medi_LamBaaMemInit uni064A.medi_LamBaaMemInit uni06BB.medi_LamBaaMemInit uni067F.medi_LamBaaMemInit uni0755.medi_LamBaaMemInit uni08A0.medi_LamBaaMemInit uni067D.medi_LamBaaMemInit uni067E.medi_LamBaaMemInit uni067B.medi_LamBaaMemInit uni0628.medi_LamBaaMemInit uni067A.medi_LamBaaMemInit uni0751.medi_LamBaaMemInit uni0646.medi_LamBaaMemInit uni0753.medi_LamBaaMemInit uni0752.medi_LamBaaMemInit uni062A.medi_LamBaaMemInit uni0678.medi_LamBaaMemInit uni063D.medi_LamBaaMemInit uni062B.medi_LamBaaMemInit uni0679.medi_LamBaaMemInit uni06B9.medi_LamBaaMemInit uni0769.medi_LamBaaMemInit uni0649.medi_LamBaaMemInit uni067C.medi_LamBaaMemInit uni0754.medi_LamBaaMemInit uni06D1.medi_LamBaaMemInit uni06D0.medi_LamBaaMemInit uni06BA.medi_LamBaaMemInit uni06CC.medi_LamBaaMemInit uni0767.medi_LamBaaMemInit ];
@aBaa.medi_SenBaaMemInit = [ uni0777.medi_SenBaaMemInit uni0680.medi_SenBaaMemInit uni0776.medi_SenBaaMemInit uni06BC.medi_SenBaaMemInit uni0750.medi_SenBaaMemInit uni0756.medi_SenBaaMemInit uni0768.medi_SenBaaMemInit uni06CE.medi_SenBaaMemInit uni0775.medi_SenBaaMemInit uni06BD.medi_SenBaaMemInit uni0626.medi_SenBaaMemInit uni066E.medi_SenBaaMemInit uni0620.medi_SenBaaMemInit uni064A.medi_SenBaaMemInit uni06BB.medi_SenBaaMemInit uni067F.medi_SenBaaMemInit uni0755.medi_SenBaaMemInit uni08A0.medi_SenBaaMemInit uni067D.medi_SenBaaMemInit uni067E.medi_SenBaaMemInit uni067B.medi_SenBaaMemInit uni0628.medi_SenBaaMemInit uni067A.medi_SenBaaMemInit uni0751.medi_SenBaaMemInit uni0646.medi_SenBaaMemInit uni0753.medi_SenBaaMemInit uni0752.medi_SenBaaMemInit uni062A.medi_SenBaaMemInit uni0678.medi_SenBaaMemInit uni063D.medi_SenBaaMemInit uni062B.medi_SenBaaMemInit uni0679.medi_SenBaaMemInit uni06B9.medi_SenBaaMemInit uni0769.medi_SenBaaMemInit uni0649.medi_SenBaaMemInit uni067C.medi_SenBaaMemInit uni0754.medi_SenBaaMemInit uni06D1.medi_SenBaaMemInit uni06D0.medi_SenBaaMemInit uni06BA.medi_SenBaaMemInit uni06CC.medi_SenBaaMemInit uni0767.medi_SenBaaMemInit ];
@aDal.fina_BaaDal = [ uni0690.fina_BaaDal uni06EE.fina_BaaDal uni0689.fina_BaaDal uni0688.fina_BaaDal uni075A.fina_BaaDal uni0630.fina_BaaDal uni062F.fina_BaaDal uni0759.fina_BaaDal uni068C.fina_BaaDal uni068B.fina_BaaDal uni068A.fina_BaaDal uni068F.fina_BaaDal uni068E.fina_BaaDal uni068D.fina_BaaDal ];
@aDal.fina_KafDal = [ uni0690.fina_KafDal uni06EE.fina_KafDal uni0689.fina_KafDal uni0688.fina_KafDal uni075A.fina_KafDal uni0630.fina_KafDal uni062F.fina_KafDal uni0759.fina_KafDal uni068C.fina_KafDal uni068B.fina_KafDal uni068A.fina_KafDal uni068F.fina_KafDal uni068E.fina_KafDal uni068D.fina_KafDal ];
@aDal.fina_LamDal = [ uni0690.fina_LamDal uni06EE.fina_LamDal uni0689.fina_LamDal uni0688.fina_LamDal uni075A.fina_LamDal uni0630.fina_LamDal uni062F.fina_LamDal uni0759.fina_LamDal uni068C.fina_LamDal uni068B.fina_LamDal uni068A.fina_LamDal uni068F.fina_LamDal uni068E.fina_LamDal uni068D.fina_LamDal ];
@aFaa.init_FaaHaaInit = [ uni066F.init_FaaHaaInit uni0761.init_FaaHaaInit uni0760.init_FaaHaaInit uni0642.init_FaaHaaInit uni0641.init_FaaHaaInit uni06A8.init_FaaHaaInit uni06A1.init_FaaHaaInit uni06A2.init_FaaHaaInit uni06A3.init_FaaHaaInit uni06A4.init_FaaHaaInit uni06A5.init_FaaHaaInit uni06A6.init_FaaHaaInit uni06A7.init_FaaHaaInit ];
@aFaa.init_FaaMemInit = [ uni066F.init_FaaMemInit uni0761.init_FaaMemInit uni0760.init_FaaMemInit uni0642.init_FaaMemInit uni0641.init_FaaMemInit uni06A8.init_FaaMemInit uni06A1.init_FaaMemInit uni06A2.init_FaaMemInit uni06A3.init_FaaMemInit uni06A4.init_FaaMemInit uni06A5.init_FaaMemInit uni06A6.init_FaaMemInit uni06A7.init_FaaMemInit ];
@aFaa.init_FaaYaaIsol = [ uni066F.init_FaaYaaIsol uni0761.init_FaaYaaIsol uni0760.init_FaaYaaIsol uni0642.init_FaaYaaIsol uni0641.init_FaaYaaIsol uni06A8.init_FaaYaaIsol uni06A1.init_FaaYaaIsol uni06A2.init_FaaYaaIsol uni06A3.init_FaaYaaIsol uni06A4.init_FaaYaaIsol uni06A5.init_FaaYaaIsol uni06A6.init_FaaYaaIsol uni06A7.init_FaaYaaIsol ];
@aFaa.init_YaaBarree = [ uni066F.init_YaaBarree uni0761.init_YaaBarree uni0760.init_YaaBarree uni0642.init_YaaBarree uni0641.init_YaaBarree uni06A8.init_YaaBarree uni06A1.init_YaaBarree uni06A2.init_YaaBarree uni06A3.init_YaaBarree uni06A4.init_YaaBarree uni06A5.init_YaaBarree uni06A6.init_YaaBarree uni06A7.init_YaaBarree ];
@aFaa.medi_FaaYaaFina = [ uni066F.medi_FaaYaaFina uni0761.medi_FaaYaaFina uni0760.medi_FaaYaaFina uni0642.medi_FaaYaaFina uni0641.medi_FaaYaaFina uni06A8.medi_FaaYaaFina uni06A1.medi_FaaYaaFina uni06A2.medi_FaaYaaFina uni06A3.medi_FaaYaaFina uni06A4.medi_FaaYaaFina uni06A5.medi_FaaYaaFina uni06A6.medi_FaaYaaFina uni06A7.medi_FaaYaaFina ];
@aHaa.fina_AboveHaaIsol2 = [ uni062E.fina_AboveHaaIsol2 uni062D.fina_AboveHaaIsol2 uni0681.fina_AboveHaaIsol2 uni0687.fina_AboveHaaIsol2 uni0685.fina_AboveHaaIsol2 uni062C.fina_AboveHaaIsol2 uni0682.fina_AboveHaaIsol2 uni0757.fina_AboveHaaIsol2 uni0684.fina_AboveHaaIsol2 uni076F.fina_AboveHaaIsol2 uni076E.fina_AboveHaaIsol2 uni0683.fina_AboveHaaIsol2 uni06BF.fina_AboveHaaIsol2 uni077C.fina_AboveHaaIsol2 uni0758.fina_AboveHaaIsol2 uni0772.fina_AboveHaaIsol2 uni0686.fina_AboveHaaIsol2 ];
@aHaa.fina_AboveHaaIsol = [ uni062E.fina_AboveHaaIsol uni062D.fina_AboveHaaIsol uni0681.fina_AboveHaaIsol uni0687.fina_AboveHaaIsol uni0685.fina_AboveHaaIsol uni062C.fina_AboveHaaIsol uni0682.fina_AboveHaaIsol uni0757.fina_AboveHaaIsol uni0684.fina_AboveHaaIsol uni076F.fina_AboveHaaIsol uni076E.fina_AboveHaaIsol uni0683.fina_AboveHaaIsol uni06BF.fina_AboveHaaIsol uni077C.fina_AboveHaaIsol uni0758.fina_AboveHaaIsol uni0772.fina_AboveHaaIsol uni0686.fina_AboveHaaIsol ];
@aHaa.init_AboveHaa = [ uni062E.init_AboveHaa uni062D.init_AboveHaa uni0681.init_AboveHaa uni0687.init_AboveHaa uni0685.init_AboveHaa uni062C.init_AboveHaa uni0682.init_AboveHaa uni0757.init_AboveHaa uni0684.init_AboveHaa uni076F.init_AboveHaa uni076E.init_AboveHaa uni0683.init_AboveHaa uni06BF.init_AboveHaa uni077C.init_AboveHaa uni0758.init_AboveHaa uni0772.init_AboveHaa uni0686.init_AboveHaa ];
@aHaa.init_Finjani = [ uni062E.init_Finjani uni062D.init_Finjani uni0681.init_Finjani uni0687.init_Finjani uni0685.init_Finjani uni062C.init_Finjani uni0682.init_Finjani uni0757.init_Finjani uni0684.init_Finjani uni076F.init_Finjani uni076E.init_Finjani uni0683.init_Finjani uni06BF.init_Finjani uni077C.init_Finjani uni0758.init_Finjani uni0772.init_Finjani uni0686.init_Finjani ];
@aHaa.init_HaaHaaInit = [ uni062E.init_HaaHaaInit uni062D.init_HaaHaaInit uni0681.init_HaaHaaInit uni0687.init_HaaHaaInit uni0685.init_HaaHaaInit uni062C.init_HaaHaaInit uni0682.init_HaaHaaInit uni0757.init_HaaHaaInit uni0684.init_HaaHaaInit uni076F.init_HaaHaaInit uni076E.init_HaaHaaInit uni0683.init_HaaHaaInit uni06BF.init_HaaHaaInit uni077C.init_HaaHaaInit uni0758.init_HaaHaaInit uni0772.init_HaaHaaInit uni0686.init_HaaHaaInit ];
@aHaa.init_HaaMemInit = [ uni062E.init_HaaMemInit uni062D.init_HaaMemInit uni0681.init_HaaMemInit uni0687.init_HaaMemInit uni0685.init_HaaMemInit uni062C.init_HaaMemInit uni0682.init_HaaMemInit uni0757.init_HaaMemInit uni0684.init_HaaMemInit uni076F.init_HaaMemInit uni076E.init_HaaMemInit uni0683.init_HaaMemInit uni06BF.init_HaaMemInit uni077C.init_HaaMemInit uni0758.init_HaaMemInit uni0772.init_HaaMemInit uni0686.init_HaaMemInit ];
@aHaa.init_HaaRaaIsol = [ uni062E.init_HaaRaaIsol uni062D.init_HaaRaaIsol uni0681.init_HaaRaaIsol uni0687.init_HaaRaaIsol uni0685.init_HaaRaaIsol uni062C.init_HaaRaaIsol uni0682.init_HaaRaaIsol uni0757.init_HaaRaaIsol uni0684.init_HaaRaaIsol uni076F.init_HaaRaaIsol uni076E.init_HaaRaaIsol uni0683.init_HaaRaaIsol uni06BF.init_HaaRaaIsol uni077C.init_HaaRaaIsol uni0758.init_HaaRaaIsol uni0772.init_HaaRaaIsol uni0686.init_HaaRaaIsol ];
@aHaa.init_HaaYaaIsol = [ uni062E.init_HaaYaaIsol uni062D.init_HaaYaaIsol uni0681.init_HaaYaaIsol uni0687.init_HaaYaaIsol uni0685.init_HaaYaaIsol uni062C.init_HaaYaaIsol uni0682.init_HaaYaaIsol uni0757.init_HaaYaaIsol uni0684.init_HaaYaaIsol uni076F.init_HaaYaaIsol uni076E.init_HaaYaaIsol uni0683.init_HaaYaaIsol uni06BF.init_HaaYaaIsol uni077C.init_HaaYaaIsol uni0758.init_HaaYaaIsol uni0772.init_HaaYaaIsol uni0686.init_HaaYaaIsol ];
@aHaa.init_YaaBarree = [ uni062E.init_YaaBarree uni062D.init_YaaBarree uni0681.init_YaaBarree uni0687.init_YaaBarree uni0685.init_YaaBarree uni062C.init_YaaBarree uni0682.init_YaaBarree uni0757.init_YaaBarree uni0684.init_YaaBarree uni076F.init_YaaBarree uni076E.init_YaaBarree uni0683.init_YaaBarree uni06BF.init_YaaBarree uni077C.init_YaaBarree uni0758.init_YaaBarree uni0772.init_YaaBarree uni0686.init_YaaBarree ];
@aHaa.medi_1LamHaaHaaInit = [ uni062E.medi_1LamHaaHaaInit uni062D.medi_1LamHaaHaaInit uni0681.medi_1LamHaaHaaInit uni0687.medi_1LamHaaHaaInit uni0685.medi_1LamHaaHaaInit uni062C.medi_1LamHaaHaaInit uni0682.medi_1LamHaaHaaInit uni0757.medi_1LamHaaHaaInit uni0684.medi_1LamHaaHaaInit uni076F.medi_1LamHaaHaaInit uni076E.medi_1LamHaaHaaInit uni0683.medi_1LamHaaHaaInit uni06BF.medi_1LamHaaHaaInit uni077C.medi_1LamHaaHaaInit uni0758.medi_1LamHaaHaaInit uni0772.medi_1LamHaaHaaInit uni0686.medi_1LamHaaHaaInit ];
@aHaa.medi_2LamHaaHaaInit = [ uni062E.medi_2LamHaaHaaInit uni062D.medi_2LamHaaHaaInit uni0681.medi_2LamHaaHaaInit uni0687.medi_2LamHaaHaaInit uni0685.medi_2LamHaaHaaInit uni062C.medi_2LamHaaHaaInit uni0682.medi_2LamHaaHaaInit uni0757.medi_2LamHaaHaaInit uni0684.medi_2LamHaaHaaInit uni076F.medi_2LamHaaHaaInit uni076E.medi_2LamHaaHaaInit uni0683.medi_2LamHaaHaaInit uni06BF.medi_2LamHaaHaaInit uni077C.medi_2LamHaaHaaInit uni0758.medi_2LamHaaHaaInit uni0772.medi_2LamHaaHaaInit uni0686.medi_2LamHaaHaaInit ];
@aHaa.medi_AynHaaInit = [ uni062E.medi_AynHaaInit uni062D.medi_AynHaaInit uni0681.medi_AynHaaInit uni0687.medi_AynHaaInit uni0685.medi_AynHaaInit uni062C.medi_AynHaaInit uni0682.medi_AynHaaInit uni0757.medi_AynHaaInit uni0684.medi_AynHaaInit uni076F.medi_AynHaaInit uni076E.medi_AynHaaInit uni0683.medi_AynHaaInit uni06BF.medi_AynHaaInit uni077C.medi_AynHaaInit uni0758.medi_AynHaaInit uni0772.medi_AynHaaInit uni0686.medi_AynHaaInit ];
@aHaa.medi_BaaBaaHaaInit = [ uni062E.medi_BaaBaaHaaInit uni062D.medi_BaaBaaHaaInit uni0681.medi_BaaBaaHaaInit uni0687.medi_BaaBaaHaaInit uni0685.medi_BaaBaaHaaInit uni062C.medi_BaaBaaHaaInit uni0682.medi_BaaBaaHaaInit uni0757.medi_BaaBaaHaaInit uni0684.medi_BaaBaaHaaInit uni076F.medi_BaaBaaHaaInit uni076E.medi_BaaBaaHaaInit uni0683.medi_BaaBaaHaaInit uni06BF.medi_BaaBaaHaaInit uni077C.medi_BaaBaaHaaInit uni0758.medi_BaaBaaHaaInit uni0772.medi_BaaBaaHaaInit uni0686.medi_BaaBaaHaaInit ];
@aHaa.medi_BaaHaaMemInit = [ uni062E.medi_BaaHaaMemInit uni062D.medi_BaaHaaMemInit uni0681.medi_BaaHaaMemInit uni0687.medi_BaaHaaMemInit uni0685.medi_BaaHaaMemInit uni062C.medi_BaaHaaMemInit uni0682.medi_BaaHaaMemInit uni0757.medi_BaaHaaMemInit uni0684.medi_BaaHaaMemInit uni076F.medi_BaaHaaMemInit uni076E.medi_BaaHaaMemInit uni0683.medi_BaaHaaMemInit uni06BF.medi_BaaHaaMemInit uni077C.medi_BaaHaaMemInit uni0758.medi_BaaHaaMemInit uni0772.medi_BaaHaaMemInit uni0686.medi_BaaHaaMemInit ];
@aHaa.medi_BaaMemHaaInit = [ uni062E.medi_BaaMemHaaInit uni062D.medi_BaaMemHaaInit uni0681.medi_BaaMemHaaInit uni0687.medi_BaaMemHaaInit uni0685.medi_BaaMemHaaInit uni062C.medi_BaaMemHaaInit uni0682.medi_BaaMemHaaInit uni0757.medi_BaaMemHaaInit uni0684.medi_BaaMemHaaInit uni076F.medi_BaaMemHaaInit uni076E.medi_BaaMemHaaInit uni0683.medi_BaaMemHaaInit uni06BF.medi_BaaMemHaaInit uni077C.medi_BaaMemHaaInit uni0758.medi_BaaMemHaaInit uni0772.medi_BaaMemHaaInit uni0686.medi_BaaMemHaaInit ];
@aHaa.medi_FaaHaaInit = [ uni062E.medi_FaaHaaInit uni062D.medi_FaaHaaInit uni0681.medi_FaaHaaInit uni0687.medi_FaaHaaInit uni0685.medi_FaaHaaInit uni062C.medi_FaaHaaInit uni0682.medi_FaaHaaInit uni0757.medi_FaaHaaInit uni0684.medi_FaaHaaInit uni076F.medi_FaaHaaInit uni076E.medi_FaaHaaInit uni0683.medi_FaaHaaInit uni06BF.medi_FaaHaaInit uni077C.medi_FaaHaaInit uni0758.medi_FaaHaaInit uni0772.medi_FaaHaaInit uni0686.medi_FaaHaaInit ];
@aHaa.medi_Finjani = [ uni062E.medi_Finjani uni062D.medi_Finjani uni0681.medi_Finjani uni0687.medi_Finjani uni0685.medi_Finjani uni062C.medi_Finjani uni0682.medi_Finjani uni0757.medi_Finjani uni0684.medi_Finjani uni076F.medi_Finjani uni076E.medi_Finjani uni0683.medi_Finjani uni06BF.medi_Finjani uni077C.medi_Finjani uni0758.medi_Finjani uni0772.medi_Finjani uni0686.medi_Finjani ];
@aHaa.medi_HaaHaaInit = [ uni062E.medi_HaaHaaInit uni062D.medi_HaaHaaInit uni0681.medi_HaaHaaInit uni0687.medi_HaaHaaInit uni0685.medi_HaaHaaInit uni062C.medi_HaaHaaInit uni0682.medi_HaaHaaInit uni0757.medi_HaaHaaInit uni0684.medi_HaaHaaInit uni076F.medi_HaaHaaInit uni076E.medi_HaaHaaInit uni0683.medi_HaaHaaInit uni06BF.medi_HaaHaaInit uni077C.medi_HaaHaaInit uni0758.medi_HaaHaaInit uni0772.medi_HaaHaaInit uni0686.medi_HaaHaaInit ];
@aHaa.medi_LamHaaMemInit = [ uni062E.medi_LamHaaMemInit uni062D.medi_LamHaaMemInit uni0681.medi_LamHaaMemInit uni0687.medi_LamHaaMemInit uni0685.medi_LamHaaMemInit uni062C.medi_LamHaaMemInit uni0682.medi_LamHaaMemInit uni0757.medi_LamHaaMemInit uni0684.medi_LamHaaMemInit uni076F.medi_LamHaaMemInit uni076E.medi_LamHaaMemInit uni0683.medi_LamHaaMemInit uni06BF.medi_LamHaaMemInit uni077C.medi_LamHaaMemInit uni0758.medi_LamHaaMemInit uni0772.medi_LamHaaMemInit uni0686.medi_LamHaaMemInit ];
@aHaa.medi_LamLamHaaInit = [ uni062E.medi_LamLamHaaInit uni062D.medi_LamLamHaaInit uni0681.medi_LamLamHaaInit uni0687.medi_LamLamHaaInit uni0685.medi_LamLamHaaInit uni062C.medi_LamLamHaaInit uni0682.medi_LamLamHaaInit uni0757.medi_LamLamHaaInit uni0684.medi_LamLamHaaInit uni076F.medi_LamLamHaaInit uni076E.medi_LamLamHaaInit uni0683.medi_LamLamHaaInit uni06BF.medi_LamLamHaaInit uni077C.medi_LamLamHaaInit uni0758.medi_LamLamHaaInit uni0772.medi_LamLamHaaInit uni0686.medi_LamLamHaaInit ];
@aHaa.medi_LamMemHaaInit = [ uni062E.medi_LamMemHaaInit uni062D.medi_LamMemHaaInit uni0681.medi_LamMemHaaInit uni0687.medi_LamMemHaaInit uni0685.medi_LamMemHaaInit uni062C.medi_LamMemHaaInit uni0682.medi_LamMemHaaInit uni0757.medi_LamMemHaaInit uni0684.medi_LamMemHaaInit uni076F.medi_LamMemHaaInit uni076E.medi_LamMemHaaInit uni0683.medi_LamMemHaaInit uni06BF.medi_LamMemHaaInit uni077C.medi_LamMemHaaInit uni0758.medi_LamMemHaaInit uni0772.medi_LamMemHaaInit uni0686.medi_LamMemHaaInit ];
@aHaa.medi_MemHaaMemInit = [ uni062E.medi_MemHaaMemInit uni062D.medi_MemHaaMemInit uni0681.medi_MemHaaMemInit uni0687.medi_MemHaaMemInit uni0685.medi_MemHaaMemInit uni062C.medi_MemHaaMemInit uni0682.medi_MemHaaMemInit uni0757.medi_MemHaaMemInit uni0684.medi_MemHaaMemInit uni076F.medi_MemHaaMemInit uni076E.medi_MemHaaMemInit uni0683.medi_MemHaaMemInit uni06BF.medi_MemHaaMemInit uni077C.medi_MemHaaMemInit uni0758.medi_MemHaaMemInit uni0772.medi_MemHaaMemInit uni0686.medi_MemHaaMemInit ];
@aHaa.medi_SadHaaInit = [ uni062E.medi_SadHaaInit uni062D.medi_SadHaaInit uni0681.medi_SadHaaInit uni0687.medi_SadHaaInit uni0685.medi_SadHaaInit uni062C.medi_SadHaaInit uni0682.medi_SadHaaInit uni0757.medi_SadHaaInit uni0684.medi_SadHaaInit uni076F.medi_SadHaaInit uni076E.medi_SadHaaInit uni0683.medi_SadHaaInit uni06BF.medi_SadHaaInit uni077C.medi_SadHaaInit uni0758.medi_SadHaaInit uni0772.medi_SadHaaInit uni0686.medi_SadHaaInit ];
@aHeh.fina_KafHeh = [ uni0647.fina_KafHeh uni06C1.fina_KafHeh uni06C3.fina_KafHeh uni06D5.fina_KafHeh uni0629.fina_KafHeh ];
@aHeh.fina_LamHeh = [ uni0647.fina_LamHeh uni06C1.fina_LamHeh uni06C3.fina_LamHeh uni06D5.fina_LamHeh uni0629.fina_LamHeh ];
@aHeh.init_AboveHaa = [ uni0647.init_AboveHaa uni06C1.init_AboveHaa ];
@aHeh.init_HehHaaInit = [ uni0647.init_HehHaaInit uni06C1.init_HehHaaInit ];
@aHeh.init_HehMemInit = [ uni0647.init_HehMemInit uni06C1.init_HehMemInit ];
@aHeh.init_HehYaaIsol = [ uni0647.init_HehYaaIsol uni06C1.init_HehYaaIsol ];
@aHeh.init_YaaBarree = [ uni0647.init_YaaBarree uni06C1.init_YaaBarree ];
@aHeh.medi_BaaHehMedi = [ uni0647.medi_BaaHehMedi uni06C1.medi_BaaHehMedi ];
@aHeh.medi_LamHehInit = [ uni0647.medi_LamHehInit uni06C1.medi_LamHehInit ];
@aHeh.medi_PostTooth = [ uni0647.medi_PostTooth uni06C1.medi_PostTooth ];
@aHeh.medi_PostToothHehYaa = [ uni0647.medi_PostToothHehYaa uni06C1.medi_PostToothHehYaa ];
@aHeh.medi_HehYaaFina = [ uni0647.medi_HehYaaFina uni06C1.medi_HehYaaFina ];
@aKaf.fina_KafKafFina = [ uni063B.fina_KafKafFina uni063C.fina_KafKafFina uni077F.fina_KafKafFina uni0764.fina_KafKafFina uni0643.fina_KafKafFina uni06B0.fina_KafKafFina uni06B3.fina_KafKafFina uni06B2.fina_KafKafFina uni06AB.fina_KafKafFina uni06AC.fina_KafKafFina uni06AD.fina_KafKafFina uni06AE.fina_KafKafFina uni06AF.fina_KafKafFina uni06A9.fina_KafKafFina uni06B4.fina_KafKafFina uni0763.fina_KafKafFina uni0762.fina_KafKafFina uni06B1.fina_KafKafFina ];
@aKaf.fina_LamKafFina = [ uni063B.fina_LamKafFina uni063C.fina_LamKafFina uni077F.fina_LamKafFina uni0764.fina_LamKafFina uni0643.fina_LamKafFina uni06B0.fina_LamKafFina uni06B3.fina_LamKafFina uni06B2.fina_LamKafFina uni06AB.fina_LamKafFina uni06AC.fina_LamKafFina uni06AD.fina_LamKafFina uni06AE.fina_LamKafFina uni06AF.fina_LamKafFina uni06A9.fina_LamKafFina uni06B4.fina_LamKafFina uni0763.fina_LamKafFina uni0762.fina_LamKafFina uni06B1.fina_LamKafFina ];
@aKaf.fina_LamKafIsol = [ uni063B.fina_LamKafIsol uni063C.fina_LamKafIsol uni077F.fina_LamKafIsol uni0764.fina_LamKafIsol uni0643.fina_LamKafIsol uni06B0.fina_LamKafIsol uni06B3.fina_LamKafIsol uni06B2.fina_LamKafIsol uni06AB.fina_LamKafIsol uni06AC.fina_LamKafIsol uni06AD.fina_LamKafIsol uni06AE.fina_LamKafIsol uni06AF.fina_LamKafIsol uni06A9.fina_LamKafIsol uni06B4.fina_LamKafIsol uni0763.fina_LamKafIsol uni0762.fina_LamKafIsol uni06B1.fina_LamKafIsol ];
@aKaf.init_AboveHaa = [ uni063B.init_AboveHaa uni063C.init_AboveHaa uni077F.init_AboveHaa uni0764.init_AboveHaa uni0643.init_AboveHaa uni06B0.init_AboveHaa uni06B3.init_AboveHaa uni06B2.init_AboveHaa uni06AB.init_AboveHaa uni06AC.init_AboveHaa uni06AD.init_AboveHaa uni06AE.init_AboveHaa uni06AF.init_AboveHaa uni06A9.init_AboveHaa uni06B4.init_AboveHaa uni0763.init_AboveHaa uni0762.init_AboveHaa uni06B1.init_AboveHaa ];
@aKaf.init_KafBaaInit = [ uni063B.init_KafBaaInit uni063C.init_KafBaaInit uni077F.init_KafBaaInit uni0764.init_KafBaaInit uni0643.init_KafBaaInit uni06B0.init_KafBaaInit uni06B3.init_KafBaaInit uni06B2.init_KafBaaInit uni06AB.init_KafBaaInit uni06AC.init_KafBaaInit uni06AD.init_KafBaaInit uni06AE.init_KafBaaInit uni06AF.init_KafBaaInit uni06A9.init_KafBaaInit uni06B4.init_KafBaaInit uni0763.init_KafBaaInit uni0762.init_KafBaaInit uni06B1.init_KafBaaInit ];
@aKaf.init_KafHeh = [ uni063B.init_KafHeh uni063C.init_KafHeh uni077F.init_KafHeh uni0764.init_KafHeh uni0643.init_KafHeh uni06B0.init_KafHeh uni06B3.init_KafHeh uni06B2.init_KafHeh uni06AB.init_KafHeh uni06AC.init_KafHeh uni06AD.init_KafHeh uni06AE.init_KafHeh uni06AF.init_KafHeh uni06A9.init_KafHeh uni06B4.init_KafHeh uni0763.init_KafHeh uni0762.init_KafHeh uni06B1.init_KafHeh ];
@aKaf.init_KafLam = [ uni063B.init_KafLam uni063C.init_KafLam uni077F.init_KafLam uni0764.init_KafLam uni0643.init_KafLam uni06B0.init_KafLam uni06B3.init_KafLam uni06B2.init_KafLam uni06AB.init_KafLam uni06AC.init_KafLam uni06AD.init_KafLam uni06AE.init_KafLam uni06AF.init_KafLam uni06A9.init_KafLam uni06B4.init_KafLam uni0763.init_KafLam uni0762.init_KafLam uni06B1.init_KafLam ];
@aKaf.init_KafMemAlf = [ uni063B.init_KafMemAlf uni063C.init_KafMemAlf uni077F.init_KafMemAlf uni0764.init_KafMemAlf uni0643.init_KafMemAlf uni06B0.init_KafMemAlf uni06B3.init_KafMemAlf uni06B2.init_KafMemAlf uni06AB.init_KafMemAlf uni06AC.init_KafMemAlf uni06AD.init_KafMemAlf uni06AE.init_KafMemAlf uni06AF.init_KafMemAlf uni06A9.init_KafMemAlf uni06B4.init_KafMemAlf uni0763.init_KafMemAlf uni0762.init_KafMemAlf uni06B1.init_KafMemAlf ];
@aKaf.init_KafMemInit = [ uni063B.init_KafMemInit uni063C.init_KafMemInit uni077F.init_KafMemInit uni0764.init_KafMemInit uni0643.init_KafMemInit uni06B0.init_KafMemInit uni06B3.init_KafMemInit uni06B2.init_KafMemInit uni06AB.init_KafMemInit uni06AC.init_KafMemInit uni06AD.init_KafMemInit uni06AE.init_KafMemInit uni06AF.init_KafMemInit uni06A9.init_KafMemInit uni06B4.init_KafMemInit uni0763.init_KafMemInit uni0762.init_KafMemInit uni06B1.init_KafMemInit ];
@aKaf.init_KafMemIsol = [ uni063B.init_KafMemIsol uni063C.init_KafMemIsol uni077F.init_KafMemIsol uni0764.init_KafMemIsol uni0643.init_KafMemIsol uni06B0.init_KafMemIsol uni06B3.init_KafMemIsol uni06B2.init_KafMemIsol uni06AB.init_KafMemIsol uni06AC.init_KafMemIsol uni06AD.init_KafMemIsol uni06AE.init_KafMemIsol uni06AF.init_KafMemIsol uni06A9.init_KafMemIsol uni06B4.init_KafMemIsol uni0763.init_KafMemIsol uni0762.init_KafMemIsol uni06B1.init_KafMemIsol ];
@aKaf.init_KafRaaIsol = [ uni063B.init_KafRaaIsol uni063C.init_KafRaaIsol uni077F.init_KafRaaIsol uni0764.init_KafRaaIsol uni0643.init_KafRaaIsol uni06B0.init_KafRaaIsol uni06B3.init_KafRaaIsol uni06B2.init_KafRaaIsol uni06AB.init_KafRaaIsol uni06AC.init_KafRaaIsol uni06AD.init_KafRaaIsol uni06AE.init_KafRaaIsol uni06AF.init_KafRaaIsol uni06A9.init_KafRaaIsol uni06B4.init_KafRaaIsol uni0763.init_KafRaaIsol uni0762.init_KafRaaIsol uni06B1.init_KafRaaIsol ];
@aKaf.init_KafYaaIsol = [ uni063B.init_KafYaaIsol uni063C.init_KafYaaIsol uni077F.init_KafYaaIsol uni0764.init_KafYaaIsol uni0643.init_KafYaaIsol uni06B0.init_KafYaaIsol uni06B3.init_KafYaaIsol uni06B2.init_KafYaaIsol uni06AB.init_KafYaaIsol uni06AC.init_KafYaaIsol uni06AD.init_KafYaaIsol uni06AE.init_KafYaaIsol uni06AF.init_KafYaaIsol uni06A9.init_KafYaaIsol uni06B4.init_KafYaaIsol uni0763.init_KafYaaIsol uni0762.init_KafYaaIsol uni06B1.init_KafYaaIsol ];
@aKaf.init_YaaBarree = [ uni063B.init_YaaBarree uni063C.init_YaaBarree uni077F.init_YaaBarree uni0764.init_YaaBarree uni0643.init_YaaBarree uni06B0.init_YaaBarree uni06B3.init_YaaBarree uni06B2.init_YaaBarree uni06AB.init_YaaBarree uni06AC.init_YaaBarree uni06AD.init_YaaBarree uni06AE.init_YaaBarree uni06AF.init_YaaBarree uni06A9.init_YaaBarree uni06B4.init_YaaBarree uni0763.init_YaaBarree uni0762.init_YaaBarree uni06B1.init_YaaBarree ];
@aKaf.medi_KafBaaMedi = [ uni063B.medi_KafBaaMedi uni063C.medi_KafBaaMedi uni077F.medi_KafBaaMedi uni0764.medi_KafBaaMedi uni0643.medi_KafBaaMedi uni06B0.medi_KafBaaMedi uni06B3.medi_KafBaaMedi uni06B2.medi_KafBaaMedi uni06AB.medi_KafBaaMedi uni06AC.medi_KafBaaMedi uni06AD.medi_KafBaaMedi uni06AE.medi_KafBaaMedi uni06AF.medi_KafBaaMedi uni06A9.medi_KafBaaMedi uni06B4.medi_KafBaaMedi uni0763.medi_KafBaaMedi uni0762.medi_KafBaaMedi uni06B1.medi_KafBaaMedi ];
@aKaf.medi_KafHeh = [ uni063B.medi_KafHeh uni063C.medi_KafHeh uni077F.medi_KafHeh uni0764.medi_KafHeh uni0643.medi_KafHeh uni06B0.medi_KafHeh uni06B3.medi_KafHeh uni06B2.medi_KafHeh uni06AB.medi_KafHeh uni06AC.medi_KafHeh uni06AD.medi_KafHeh uni06AE.medi_KafHeh uni06AF.medi_KafHeh uni06A9.medi_KafHeh uni06B4.medi_KafHeh uni0763.medi_KafHeh uni0762.medi_KafHeh uni06B1.medi_KafHeh ];
@aKaf.medi_KafLam = [ uni063B.medi_KafLam uni063C.medi_KafLam uni077F.medi_KafLam uni0764.medi_KafLam uni0643.medi_KafLam uni06B0.medi_KafLam uni06B3.medi_KafLam uni06B2.medi_KafLam uni06AB.medi_KafLam uni06AC.medi_KafLam uni06AD.medi_KafLam uni06AE.medi_KafLam uni06AF.medi_KafLam uni06A9.medi_KafLam uni06B4.medi_KafLam uni0763.medi_KafLam uni0762.medi_KafLam uni06B1.medi_KafLam ];
@aKaf.medi_KafMemAlf = [ uni063B.medi_KafMemAlf uni063C.medi_KafMemAlf uni077F.medi_KafMemAlf uni0764.medi_KafMemAlf uni0643.medi_KafMemAlf uni06B0.medi_KafMemAlf uni06B3.medi_KafMemAlf uni06B2.medi_KafMemAlf uni06AB.medi_KafMemAlf uni06AC.medi_KafMemAlf uni06AD.medi_KafMemAlf uni06AE.medi_KafMemAlf uni06AF.medi_KafMemAlf uni06A9.medi_KafMemAlf uni06B4.medi_KafMemAlf uni0763.medi_KafMemAlf uni0762.medi_KafMemAlf uni06B1.medi_KafMemAlf ];
@aKaf.medi_KafMemFina = [ uni063B.medi_KafMemFina uni063C.medi_KafMemFina uni077F.medi_KafMemFina uni0764.medi_KafMemFina uni0643.medi_KafMemFina uni06B0.medi_KafMemFina uni06B3.medi_KafMemFina uni06B2.medi_KafMemFina uni06AB.medi_KafMemFina uni06AC.medi_KafMemFina uni06AD.medi_KafMemFina uni06AE.medi_KafMemFina uni06AF.medi_KafMemFina uni06A9.medi_KafMemFina uni06B4.medi_KafMemFina uni0763.medi_KafMemFina uni0762.medi_KafMemFina uni06B1.medi_KafMemFina ];
@aKaf.medi_KafMemMedi = [ uni063B.medi_KafMemMedi uni063C.medi_KafMemMedi uni077F.medi_KafMemMedi uni0764.medi_KafMemMedi uni0643.medi_KafMemMedi uni06B0.medi_KafMemMedi uni06B3.medi_KafMemMedi uni06B2.medi_KafMemMedi uni06AB.medi_KafMemMedi uni06AC.medi_KafMemMedi uni06AD.medi_KafMemMedi uni06AE.medi_KafMemMedi uni06AF.medi_KafMemMedi uni06A9.medi_KafMemMedi uni06B4.medi_KafMemMedi uni0763.medi_KafMemMedi uni0762.medi_KafMemMedi uni06B1.medi_KafMemMedi ];
@aKaf.medi_KafRaaFina = [ uni063B.medi_KafRaaFina uni063C.medi_KafRaaFina uni077F.medi_KafRaaFina uni0764.medi_KafRaaFina uni0643.medi_KafRaaFina uni06B0.medi_KafRaaFina uni06B3.medi_KafRaaFina uni06B2.medi_KafRaaFina uni06AB.medi_KafRaaFina uni06AC.medi_KafRaaFina uni06AD.medi_KafRaaFina uni06AE.medi_KafRaaFina uni06AF.medi_KafRaaFina uni06A9.medi_KafRaaFina uni06B4.medi_KafRaaFina uni0763.medi_KafRaaFina uni0762.medi_KafRaaFina uni06B1.medi_KafRaaFina ];
@aKaf.medi_KafYaaFina = [ uni063B.medi_KafYaaFina uni063C.medi_KafYaaFina uni077F.medi_KafYaaFina uni0764.medi_KafYaaFina uni0643.medi_KafYaaFina uni06B0.medi_KafYaaFina uni06B3.medi_KafYaaFina uni06B2.medi_KafYaaFina uni06AB.medi_KafYaaFina uni06AC.medi_KafYaaFina uni06AD.medi_KafYaaFina uni06AE.medi_KafYaaFina uni06AF.medi_KafYaaFina uni06A9.medi_KafYaaFina uni06B4.medi_KafYaaFina uni0763.medi_KafYaaFina uni0762.medi_KafYaaFina uni06B1.medi_KafYaaFina ];
@aLam.fina_KafLam = [ uni06B5.fina_KafLam uni06B7.fina_KafLam uni0644.fina_KafLam uni06B8.fina_KafLam uni06B6.fina_KafLam uni076A.fina_KafLam ];
@aLam.fina_KafMemLam = [ uni06B5.fina_KafMemLam uni06B7.fina_KafMemLam uni0644.fina_KafMemLam uni06B8.fina_KafMemLam uni06B6.fina_KafMemLam uni076A.fina_KafMemLam ];
@aLam.fina_LamLamFina = [ uni06B5.fina_LamLamFina uni06B7.fina_LamLamFina uni0644.fina_LamLamFina uni06B8.fina_LamLamFina uni06B6.fina_LamLamFina uni076A.fina_LamLamFina ];
@aLam.fina_LamLamIsol = [ uni06B5.fina_LamLamIsol uni06B7.fina_LamLamIsol uni0644.fina_LamLamIsol uni06B8.fina_LamLamIsol uni06B6.fina_LamLamIsol uni076A.fina_LamLamIsol ];
@aLam.init_LamAlfIsol = [ uni06B5.init_LamAlfIsol uni06B7.init_LamAlfIsol uni0644.init_LamAlfIsol uni06B8.init_LamAlfIsol uni06B6.init_LamAlfIsol uni076A.init_LamAlfIsol ];
@aLam.init_LamBaaMemInit = [ uni06B5.init_LamBaaMemInit uni06B7.init_LamBaaMemInit uni0644.init_LamBaaMemInit uni06B8.init_LamBaaMemInit uni06B6.init_LamBaaMemInit uni076A.init_LamBaaMemInit ];
@aLam.init_LamHaaHaaInit = [ uni06B5.init_LamHaaHaaInit uni06B7.init_LamHaaHaaInit uni0644.init_LamHaaHaaInit uni06B8.init_LamHaaHaaInit uni06B6.init_LamHaaHaaInit uni076A.init_LamHaaHaaInit ];
@aLam.init_LamHaaInit = [ uni06B5.init_LamHaaInit uni06B7.init_LamHaaInit uni0644.init_LamHaaInit uni06B8.init_LamHaaInit uni06B6.init_LamHaaInit uni076A.init_LamHaaInit ];
@aLam.init_LamHaaMemInit = [ uni06B5.init_LamHaaMemInit uni06B7.init_LamHaaMemInit uni0644.init_LamHaaMemInit uni06B8.init_LamHaaMemInit uni06B6.init_LamHaaMemInit uni076A.init_LamHaaMemInit ];
@aLam.init_LamHehInit = [ uni06B5.init_LamHehInit uni06B7.init_LamHehInit uni0644.init_LamHehInit uni06B8.init_LamHehInit uni06B6.init_LamHehInit uni076A.init_LamHehInit ];
@aLam.init_LamHeh = [ uni06B5.init_LamHeh uni06B7.init_LamHeh uni0644.init_LamHeh uni06B8.init_LamHeh uni06B6.init_LamHeh uni076A.init_LamHeh ];
@aLam.init_LamLamHaaInit = [ uni06B5.init_LamLamHaaInit uni06B7.init_LamLamHaaInit uni0644.init_LamLamHaaInit uni06B8.init_LamLamHaaInit uni06B6.init_LamLamHaaInit uni076A.init_LamLamHaaInit ];
@aLam.init_LamLamInit = [ uni06B5.init_LamLamInit uni06B7.init_LamLamInit uni0644.init_LamLamInit uni06B8.init_LamLamInit uni06B6.init_LamLamInit uni076A.init_LamLamInit ];
@aLam.init_LamMemHaaInit = [ uni06B5.init_LamMemHaaInit uni06B7.init_LamMemHaaInit uni0644.init_LamMemHaaInit uni06B8.init_LamMemHaaInit uni06B6.init_LamMemHaaInit uni076A.init_LamMemHaaInit ];
@aLam.init_LamMemInit = [ uni06B5.init_LamMemInit uni06B7.init_LamMemInit uni0644.init_LamMemInit uni06B8.init_LamMemInit uni06B6.init_LamMemInit uni076A.init_LamMemInit ];
@aLam.init_LamMemIsol = [ uni06B5.init_LamMemIsol uni06B7.init_LamMemIsol uni0644.init_LamMemIsol uni06B8.init_LamMemIsol uni06B6.init_LamMemIsol uni076A.init_LamMemIsol ];
@aLam.init_LamRaaIsol = [ uni06B5.init_LamRaaIsol uni06B7.init_LamRaaIsol uni0644.init_LamRaaIsol uni06B8.init_LamRaaIsol uni06B6.init_LamRaaIsol uni076A.init_LamRaaIsol ];
@aLam.init_LamYaaIsol = [ uni06B5.init_LamYaaIsol uni06B7.init_LamYaaIsol uni0644.init_LamYaaIsol uni06B8.init_LamYaaIsol uni06B6.init_LamYaaIsol uni076A.init_LamYaaIsol ];
@aLam.init_YaaBarree = [ uni06B5.init_YaaBarree uni06B7.init_YaaBarree uni0644.init_YaaBarree uni06B8.init_YaaBarree uni06B6.init_YaaBarree uni076A.init_YaaBarree ];
@aLam.medi_KafLamAlf = [ uni06B5.medi_KafLamAlf uni06B7.medi_KafLamAlf uni0644.medi_KafLamAlf uni06B8.medi_KafLamAlf uni06B6.medi_KafLamAlf uni076A.medi_KafLamAlf ];
@aLam.medi_KafLamHehIsol = [ uni06B5.medi_KafLamHehIsol uni06B7.medi_KafLamHehIsol uni0644.medi_KafLamHehIsol uni06B8.medi_KafLamHehIsol uni06B6.medi_KafLamHehIsol uni076A.medi_KafLamHehIsol ];
@aLam.medi_KafLamMemFina = [ uni06B5.medi_KafLamMemFina uni06B7.medi_KafLamMemFina uni0644.medi_KafLamMemFina uni06B8.medi_KafLamMemFina uni06B6.medi_KafLamMemFina uni076A.medi_KafLamMemFina ];
@aLam.medi_KafLamMemMedi = [ uni06B5.medi_KafLamMemMedi uni06B7.medi_KafLamMemMedi uni0644.medi_KafLamMemMedi uni06B8.medi_KafLamMemMedi uni06B6.medi_KafLamMemMedi uni076A.medi_KafLamMemMedi ];
@aLam.medi_KafLam = [ uni06B5.medi_KafLam uni06B7.medi_KafLam uni0644.medi_KafLam uni06B8.medi_KafLam uni06B6.medi_KafLam uni076A.medi_KafLam ];
@aLam.medi_KafLamYaa = [ uni06B5.medi_KafLamYaa uni06B7.medi_KafLamYaa uni0644.medi_KafLamYaa uni06B8.medi_KafLamYaa uni06B6.medi_KafLamYaa uni076A.medi_KafLamYaa ];
@aLam.medi_KafMemLam = [ uni06B5.medi_KafMemLam uni06B7.medi_KafMemLam uni0644.medi_KafMemLam uni06B8.medi_KafMemLam uni06B6.medi_KafMemLam uni076A.medi_KafMemLam ];
@aLam.medi_LamAlfFina = [ uni06B5.medi_LamAlfFina uni06B7.medi_LamAlfFina uni0644.medi_LamAlfFina uni06B8.medi_LamAlfFina uni06B6.medi_LamAlfFina uni076A.medi_LamAlfFina ];
@aLam.medi_LamHeh = [ uni06B5.medi_LamHeh uni06B7.medi_LamHeh uni0644.medi_LamHeh uni06B8.medi_LamHeh uni06B6.medi_LamHeh uni076A.medi_LamHeh ];
@aLam.medi_LamLamAlefFina = [ uni06B5.medi_LamLamAlefFina uni06B7.medi_LamLamAlefFina uni0644.medi_LamLamAlefFina uni06B8.medi_LamLamAlefFina uni06B6.medi_LamLamAlefFina uni076A.medi_LamLamAlefFina ];
@aLam.medi_LamLamAlfIsol = [ uni06B5.medi_LamLamAlfIsol uni06B7.medi_LamLamAlfIsol uni0644.medi_LamLamAlfIsol uni06B8.medi_LamLamAlfIsol uni06B6.medi_LamLamAlfIsol uni076A.medi_LamLamAlfIsol ];
@aLam.medi_LamLamHaaInit = [ uni06B5.medi_LamLamHaaInit uni06B7.medi_LamLamHaaInit uni0644.medi_LamLamHaaInit uni06B8.medi_LamLamHaaInit uni06B6.medi_LamLamHaaInit uni076A.medi_LamLamHaaInit ];
@aLam.medi_LamLamHehFina = [ uni06B5.medi_LamLamHehFina uni06B7.medi_LamLamHehFina uni0644.medi_LamLamHehFina uni06B8.medi_LamLamHehFina uni06B6.medi_LamLamHehFina uni076A.medi_LamLamHehFina ];
@aLam.medi_LamLamHehIsol = [ uni06B5.medi_LamLamHehIsol uni06B7.medi_LamLamHehIsol uni0644.medi_LamLamHehIsol uni06B8.medi_LamLamHehIsol uni06B6.medi_LamLamHehIsol uni076A.medi_LamLamHehIsol ];
@aLam.medi_LamLamInit = [ uni06B5.medi_LamLamInit uni06B7.medi_LamLamInit uni0644.medi_LamLamInit uni06B8.medi_LamLamInit uni06B6.medi_LamLamInit uni076A.medi_LamLamInit ];
@aLam.medi_LamLamMedi2 = [ uni06B5.medi_LamLamMedi2 uni06B7.medi_LamLamMedi2 uni0644.medi_LamLamMedi2 uni06B8.medi_LamLamMedi2 uni06B6.medi_LamLamMedi2 uni076A.medi_LamLamMedi2 ];
@aLam.medi_LamLamMedi = [ uni06B5.medi_LamLamMedi uni06B7.medi_LamLamMedi uni0644.medi_LamLamMedi uni06B8.medi_LamLamMedi uni06B6.medi_LamLamMedi uni076A.medi_LamLamMedi ];
@aLam.medi_LamLamMemInit = [ uni06B5.medi_LamLamMemInit uni06B7.medi_LamLamMemInit uni0644.medi_LamLamMemInit uni06B8.medi_LamLamMemInit uni06B6.medi_LamLamMemInit uni076A.medi_LamLamMemInit ];
@aLam.medi_LamLamMemMedi = [ uni06B5.medi_LamLamMemMedi uni06B7.medi_LamLamMemMedi uni0644.medi_LamLamMemMedi uni06B8.medi_LamLamMemMedi uni06B6.medi_LamLamMemMedi uni076A.medi_LamLamMemMedi ];
@aLam.medi_LamLamYaaFina = [ uni06B5.medi_LamLamYaaFina uni06B7.medi_LamLamYaaFina uni0644.medi_LamLamYaaFina uni06B8.medi_LamLamYaaFina uni06B6.medi_LamLamYaaFina uni076A.medi_LamLamYaaFina ];
@aLam.medi_LamLamYaaIsol = [ uni06B5.medi_LamLamYaaIsol uni06B7.medi_LamLamYaaIsol uni0644.medi_LamLamYaaIsol uni06B8.medi_LamLamYaaIsol uni06B6.medi_LamLamYaaIsol uni076A.medi_LamLamYaaIsol ];
@aLam.medi_LamMemFina = [ uni06B5.medi_LamMemFina uni06B7.medi_LamMemFina uni0644.medi_LamMemFina uni06B8.medi_LamMemFina uni06B6.medi_LamMemFina uni076A.medi_LamMemFina ];
@aLam.medi_LamMemMedi = [ uni06B5.medi_LamMemMedi uni06B7.medi_LamMemMedi uni0644.medi_LamMemMedi uni06B8.medi_LamMemMedi uni06B6.medi_LamMemMedi uni076A.medi_LamMemMedi ];
@aLam.medi_LamQafFina = [ uni06B5.medi_LamQafFina uni06B7.medi_LamQafFina uni0644.medi_LamQafFina uni06B8.medi_LamQafFina uni06B6.medi_LamQafFina uni076A.medi_LamQafFina ];
@aLam.medi_LamWawFina = [ uni06B5.medi_LamWawFina uni06B7.medi_LamWawFina uni0644.medi_LamWawFina uni06B8.medi_LamWawFina uni06B6.medi_LamWawFina uni076A.medi_LamWawFina ];
@aLam.medi_LamYaaFina = [ uni06B5.medi_LamYaaFina uni06B7.medi_LamYaaFina uni0644.medi_LamYaaFina uni06B8.medi_LamYaaFina uni06B6.medi_LamYaaFina uni076A.medi_LamYaaFina ];
@aMem.fina_BaaMemFina = [ uni0645.fina_BaaMemFina ];
@aMem.fina_KafMemFina = [ uni0645.fina_KafMemFina ];
@aMem.fina_KafMemIsol = [ uni0645.fina_KafMemIsol ];
@aMem.fina_LamMemFina = [ uni0645.fina_LamMemFina ];
@aMem.fina_PostTooth =  [ uni0645.fina_PostTooth  ];
@aMem.init_AboveHaa =  [ uni0645.init_AboveHaa  ];
@aMem.init_MemHaaInit =    [ uni0645.init_MemHaaInit    ];
@aMem.init_MemHaaMemInit = [ uni0645.init_MemHaaMemInit ];
@aMem.init_MemMemInit =    [ uni0645.init_MemMemInit    ];
@aMem.init_MemRaaIsol =    [ uni0645.init_MemRaaIsol    ];
@aMem.init_MemHehInit = [ uni0765.init_MemHehInit uni0645.init_MemHehInit uni0766.init_MemHehInit ];
@aMem.init_MemYaaIsol = [ uni0765.init_MemYaaIsol uni0645.init_MemYaaIsol uni0766.init_MemYaaIsol ];
@aMem.init_YaaBarree = [ uni0765.init_YaaBarree uni0645.init_YaaBarree uni0766.init_YaaBarree ];
@aMem.medi_AlfPostTooth =  [ uni0645.medi_AlfPostTooth ];
@aMem.medi_BaaBaaMemInit = [ uni0645.medi_BaaBaaMemInit ];
@aMem.medi_BaaMemAlfFina = [ uni0645.medi_BaaMemAlfFina ];
@aMem.medi_BaaMemHaaInit = [ uni0645.medi_BaaMemHaaInit ];
@aMem.medi_BaaMemInit =    [ uni0645.medi_BaaMemInit    ];
@aMem.medi_KafMemAlf =     [ uni0645.medi_KafMemAlf     ];
@aMem.medi_KafMemMedi =    [ uni0645.medi_KafMemMedi    ];
@aMem.medi_LamBaaMemInit = [ uni0645.medi_LamBaaMemInit ];
@aMem.medi_LamHaaMemInit = [ uni0645.medi_LamHaaMemInit ];
@aMem.medi_LamMemHaaInit = [ uni0645.medi_LamMemHaaInit ];
@aMem.medi_LamMemInit =    [ uni0645.medi_LamMemInit    ];
@aMem.medi_LamMemMedi =    [ uni0765.medi_LamMemMedi uni0645.medi_LamMemMedi uni0766.medi_LamMemMedi ];
@aMem.medi_MemAlfFina =    [ uni0645.medi_MemAlfFina    ];
@aMem.medi_SenBaaMemInit = [ uni0645.medi_SenBaaMemInit ];
@aMem.medi_SenMemInit =    [ uni0645.medi_SenMemInit    ];
@aMem.medi_LamMemInitTatweel = [ uni0645.medi_LamMemInitTatweel ];
@aMem.medi_KafMemMediTatweel = [ uni0645.medi_KafMemMediTatweel ];
@aMem.fina_LamMemFinaExtended = [ uni0645.fina_LamMemFinaExtended ];
@aMem.fina_KafMemFinaExtended = [ uni0645.fina_KafMemFinaExtended ];
@aMem.fina_KafMemIsolExtended = [ uni0645.fina_KafMemIsolExtended ];
@aNon.fina_BaaNonFina = [ uni0646.fina_BaaNonFina uni0767.fina_BaaNonFina uni06BA.fina_BaaNonFina uni06BC.fina_BaaNonFina uni06BB.fina_BaaNonFina uni0768.fina_BaaNonFina uni06B9.fina_BaaNonFina uni0769.fina_BaaNonFina uni06BD.fina_BaaNonFina ];
@aNon.fina_BaaNonIsol = [ uni0646.fina_BaaNonIsol uni0767.fina_BaaNonIsol uni06BA.fina_BaaNonIsol uni06BC.fina_BaaNonIsol uni06BB.fina_BaaNonIsol uni0768.fina_BaaNonIsol uni06B9.fina_BaaNonIsol uni0769.fina_BaaNonIsol uni06BD.fina_BaaNonIsol ];
@aQaf.fina_LamQafFina = [ uni06A8.fina_LamQafFina uni06A7.fina_LamQafFina uni0642.fina_LamQafFina uni066F.fina_LamQafFina ];
@aRaa.fina_BaaRaaFina = [ uni0691.fina_BaaRaaFina uni0692.fina_BaaRaaFina uni0693.fina_BaaRaaFina uni0694.fina_BaaRaaFina uni0695.fina_BaaRaaFina uni0696.fina_BaaRaaFina uni0697.fina_BaaRaaFina uni0698.fina_BaaRaaFina uni0699.fina_BaaRaaFina uni075B.fina_BaaRaaFina uni06EF.fina_BaaRaaFina uni0632.fina_BaaRaaFina uni0771.fina_BaaRaaFina uni0631.fina_BaaRaaFina uni076B.fina_BaaRaaFina uni076C.fina_BaaRaaFina ];
@aRaa.fina_BaaRaaIsol = [ uni0691.fina_BaaRaaIsol uni0692.fina_BaaRaaIsol uni0693.fina_BaaRaaIsol uni0694.fina_BaaRaaIsol uni0695.fina_BaaRaaIsol uni0696.fina_BaaRaaIsol uni0697.fina_BaaRaaIsol uni0698.fina_BaaRaaIsol uni0699.fina_BaaRaaIsol uni075B.fina_BaaRaaIsol uni06EF.fina_BaaRaaIsol uni0632.fina_BaaRaaIsol uni0771.fina_BaaRaaIsol uni0631.fina_BaaRaaIsol uni076B.fina_BaaRaaIsol uni076C.fina_BaaRaaIsol ];
@aRaa.fina_HaaRaaIsol = [ uni0691.fina_HaaRaaIsol uni0692.fina_HaaRaaIsol uni0693.fina_HaaRaaIsol uni0694.fina_HaaRaaIsol uni0695.fina_HaaRaaIsol uni0696.fina_HaaRaaIsol uni0697.fina_HaaRaaIsol uni0698.fina_HaaRaaIsol uni0699.fina_HaaRaaIsol uni075B.fina_HaaRaaIsol uni06EF.fina_HaaRaaIsol uni0632.fina_HaaRaaIsol uni0771.fina_HaaRaaIsol uni0631.fina_HaaRaaIsol uni076B.fina_HaaRaaIsol uni076C.fina_HaaRaaIsol ];
@aRaa.fina_KafRaaFina = [ uni0691.fina_KafRaaFina uni0692.fina_KafRaaFina uni0693.fina_KafRaaFina uni0694.fina_KafRaaFina uni0695.fina_KafRaaFina uni0696.fina_KafRaaFina uni0697.fina_KafRaaFina uni0698.fina_KafRaaFina uni0699.fina_KafRaaFina uni075B.fina_KafRaaFina uni06EF.fina_KafRaaFina uni0632.fina_KafRaaFina uni0771.fina_KafRaaFina uni0631.fina_KafRaaFina uni076B.fina_KafRaaFina uni076C.fina_KafRaaFina ];
@aRaa.fina_KafRaaIsol = [ uni0691.fina_KafRaaIsol uni0692.fina_KafRaaIsol uni0693.fina_KafRaaIsol uni0694.fina_KafRaaIsol uni0695.fina_KafRaaIsol uni0696.fina_KafRaaIsol uni0697.fina_KafRaaIsol uni0698.fina_KafRaaIsol uni0699.fina_KafRaaIsol uni075B.fina_KafRaaIsol uni06EF.fina_KafRaaIsol uni0632.fina_KafRaaIsol uni0771.fina_KafRaaIsol uni0631.fina_KafRaaIsol uni076B.fina_KafRaaIsol uni076C.fina_KafRaaIsol ];
@aRaa.fina_LamRaaIsol = [ uni0691.fina_LamRaaIsol uni0692.fina_LamRaaIsol uni0693.fina_LamRaaIsol uni0694.fina_LamRaaIsol uni0695.fina_LamRaaIsol uni0696.fina_LamRaaIsol uni0697.fina_LamRaaIsol uni0698.fina_LamRaaIsol uni0699.fina_LamRaaIsol uni075B.fina_LamRaaIsol uni06EF.fina_LamRaaIsol uni0632.fina_LamRaaIsol uni0771.fina_LamRaaIsol uni0631.fina_LamRaaIsol uni076B.fina_LamRaaIsol uni076C.fina_LamRaaIsol ];
@aRaa.fina_MemRaaIsol = [ uni0691.fina_MemRaaIsol uni0692.fina_MemRaaIsol uni0693.fina_MemRaaIsol uni0694.fina_MemRaaIsol uni0695.fina_MemRaaIsol uni0696.fina_MemRaaIsol uni0697.fina_MemRaaIsol uni0698.fina_MemRaaIsol uni0699.fina_MemRaaIsol uni075B.fina_MemRaaIsol uni06EF.fina_MemRaaIsol uni0632.fina_MemRaaIsol uni0771.fina_MemRaaIsol uni0631.fina_MemRaaIsol uni076B.fina_MemRaaIsol uni076C.fina_MemRaaIsol ];
@aRaa.fina_PostTooth = [ uni0691.fina_PostTooth uni0692.fina_PostTooth uni0693.fina_PostTooth uni0694.fina_PostTooth uni0695.fina_PostTooth uni0696.fina_PostTooth uni0697.fina_PostTooth uni0698.fina_PostTooth uni0699.fina_PostTooth uni075B.fina_PostTooth uni06EF.fina_PostTooth uni0632.fina_PostTooth uni0771.fina_PostTooth uni0631.fina_PostTooth uni076B.fina_PostTooth uni076C.fina_PostTooth ];
@aSad.init_AboveHaa = [ uni069D.init_AboveHaa uni06FB.init_AboveHaa uni0636.init_AboveHaa uni069E.init_AboveHaa uni0635.init_AboveHaa ];
@aSad.init_PreYaa = [ uni069D.init_PreYaa uni06FB.init_PreYaa uni0636.init_PreYaa uni069E.init_PreYaa uni0635.init_PreYaa ];
@aSad.init_SadHaaInit = [ uni069D.init_SadHaaInit uni06FB.init_SadHaaInit uni0636.init_SadHaaInit uni069E.init_SadHaaInit uni0635.init_SadHaaInit ];
@aSad.init_SadMemInit = [ uni069D.init_SadMemInit uni06FB.init_SadMemInit uni0636.init_SadMemInit uni069E.init_SadMemInit uni0635.init_SadMemInit ];
@aSad.init_SenBaaMemInit = [ uni069D.init_SenBaaMemInit uni06FB.init_SenBaaMemInit uni0636.init_SenBaaMemInit uni069E.init_SenBaaMemInit uni0635.init_SenBaaMemInit ];
@aSad.init_YaaBarree = [ uni069D.init_YaaBarree uni06FB.init_YaaBarree uni0636.init_YaaBarree uni069E.init_YaaBarree uni0635.init_YaaBarree ];
@aSad.medi_PreYaa = [ uni069D.medi_PreYaa uni06FB.medi_PreYaa uni0636.medi_PreYaa uni069E.medi_PreYaa uni0635.medi_PreYaa ];
@aSen.fina_BaaSen = [ uni06FA.fina_BaaSen uni076D.fina_BaaSen uni0633.fina_BaaSen uni077E.fina_BaaSen uni077D.fina_BaaSen uni0634.fina_BaaSen uni0770.fina_BaaSen uni075C.fina_BaaSen uni069A.fina_BaaSen uni069B.fina_BaaSen uni069C.fina_BaaSen ];
@aSen.init_AboveHaa = [ uni06FA.init_AboveHaa uni076D.init_AboveHaa uni0633.init_AboveHaa uni077E.init_AboveHaa uni077D.init_AboveHaa uni0634.init_AboveHaa uni0770.init_AboveHaa uni075C.init_AboveHaa uni069A.init_AboveHaa uni069B.init_AboveHaa uni069C.init_AboveHaa ];
@aSen.init_PreYaa = [ uni06FA.init_PreYaa uni076D.init_PreYaa uni0633.init_PreYaa uni077E.init_PreYaa uni077D.init_PreYaa uni0634.init_PreYaa uni0770.init_PreYaa uni075C.init_PreYaa uni069A.init_PreYaa uni069B.init_PreYaa uni069C.init_PreYaa ];
@aSen.init_SenHaaInit = [ uni06FA.init_SenHaaInit uni076D.init_SenHaaInit uni0633.init_SenHaaInit uni077E.init_SenHaaInit uni077D.init_SenHaaInit uni0634.init_SenHaaInit uni0770.init_SenHaaInit uni075C.init_SenHaaInit uni069A.init_SenHaaInit uni069B.init_SenHaaInit uni069C.init_SenHaaInit ];
@aSen.init_SenMemInit = [ uni06FA.init_SenMemInit uni076D.init_SenMemInit uni0633.init_SenMemInit uni077E.init_SenMemInit uni077D.init_SenMemInit uni0634.init_SenMemInit uni0770.init_SenMemInit uni075C.init_SenMemInit uni069A.init_SenMemInit uni069B.init_SenMemInit uni069C.init_SenMemInit ];
@aSen.init_SenBaaMemInit = [ uni06FA.init_SenBaaMemInit uni076D.init_SenBaaMemInit uni0633.init_SenBaaMemInit uni077E.init_SenBaaMemInit uni077D.init_SenBaaMemInit uni0634.init_SenBaaMemInit uni0770.init_SenBaaMemInit uni075C.init_SenBaaMemInit uni069A.init_SenBaaMemInit uni069B.init_SenBaaMemInit uni069C.init_SenBaaMemInit ];
@aSen.init_YaaBarree = [ uni06FA.init_YaaBarree uni076D.init_YaaBarree uni0633.init_YaaBarree uni077E.init_YaaBarree uni077D.init_YaaBarree uni0634.init_YaaBarree uni0770.init_YaaBarree uni075C.init_YaaBarree uni069A.init_YaaBarree uni069B.init_YaaBarree uni069C.init_YaaBarree ];
@aSen.medi_BaaSenAltInit = [ uni06FA.medi_BaaSenAltInit uni076D.medi_BaaSenAltInit uni0633.medi_BaaSenAltInit uni077E.medi_BaaSenAltInit uni077D.medi_BaaSenAltInit uni0634.medi_BaaSenAltInit uni0770.medi_BaaSenAltInit uni075C.medi_BaaSenAltInit uni069A.medi_BaaSenAltInit uni069B.medi_BaaSenAltInit uni069C.medi_BaaSenAltInit ];
@aSen.medi_BaaSenInit = [ uni06FA.medi_BaaSenInit uni076D.medi_BaaSenInit uni0633.medi_BaaSenInit uni077E.medi_BaaSenInit uni077D.medi_BaaSenInit uni0634.medi_BaaSenInit uni0770.medi_BaaSenInit uni075C.medi_BaaSenInit uni069A.medi_BaaSenInit uni069B.medi_BaaSenInit uni069C.medi_BaaSenInit ];
@aSen.medi_PreYaa = [ uni06FA.medi_PreYaa uni076D.medi_PreYaa uni0633.medi_PreYaa uni077E.medi_PreYaa uni077D.medi_PreYaa uni0634.medi_PreYaa uni0770.medi_PreYaa uni075C.medi_PreYaa uni069A.medi_PreYaa uni069B.medi_PreYaa uni069C.medi_PreYaa ];
@aTaa.init_YaaBarree = [ uni0638.init_YaaBarree uni0637.init_YaaBarree uni069F.init_YaaBarree ];
@aWaw.fina_LamWawFina = [ uni06CB.fina_LamWawFina uni0624.fina_LamWawFina uni06CA.fina_LamWawFina uni06CF.fina_LamWawFina uni0778.fina_LamWawFina uni06C6.fina_LamWawFina uni06C7.fina_LamWawFina uni06C4.fina_LamWawFina uni06C5.fina_LamWawFina uni0676.fina_LamWawFina uni0677.fina_LamWawFina uni06C8.fina_LamWawFina uni06C9.fina_LamWawFina uni0779.fina_LamWawFina uni0648.fina_LamWawFina ];
@aYaa.fina_BaaBaaYaa = [ uni0777.fina_BaaBaaYaa uni06D1.fina_BaaBaaYaa uni0775.fina_BaaBaaYaa uni063F.fina_BaaBaaYaa uni0678.fina_BaaBaaYaa uni063D.fina_BaaBaaYaa uni063E.fina_BaaBaaYaa uni06D0.fina_BaaBaaYaa uni0649.fina_BaaBaaYaa uni0776.fina_BaaBaaYaa uni06CD.fina_BaaBaaYaa uni06CC.fina_BaaBaaYaa uni0626.fina_BaaBaaYaa uni0620.fina_BaaBaaYaa uni064A.fina_BaaBaaYaa uni06CE.fina_BaaBaaYaa ];
@aYaa.fina_BaaYaaFina = [ uni0777.fina_BaaYaaFina uni06D1.fina_BaaYaaFina uni0775.fina_BaaYaaFina uni063F.fina_BaaYaaFina uni0678.fina_BaaYaaFina uni063D.fina_BaaYaaFina uni063E.fina_BaaYaaFina uni06D0.fina_BaaYaaFina uni0649.fina_BaaYaaFina uni0776.fina_BaaYaaFina uni06CD.fina_BaaYaaFina uni06CC.fina_BaaYaaFina uni0626.fina_BaaYaaFina uni0620.fina_BaaYaaFina uni064A.fina_BaaYaaFina uni06CE.fina_BaaYaaFina ];
@aYaa.fina_FaaYaaFina = [ uni0777.fina_FaaYaaFina uni06D1.fina_FaaYaaFina uni0775.fina_FaaYaaFina uni063F.fina_FaaYaaFina uni0678.fina_FaaYaaFina uni063D.fina_FaaYaaFina uni063E.fina_FaaYaaFina uni06D0.fina_FaaYaaFina uni0649.fina_FaaYaaFina uni0776.fina_FaaYaaFina uni06CD.fina_FaaYaaFina uni06CC.fina_FaaYaaFina uni0626.fina_FaaYaaFina uni0620.fina_FaaYaaFina uni064A.fina_FaaYaaFina uni06CE.fina_FaaYaaFina ];
@aYaa.fina_KafYaaFina = [ uni0777.fina_KafYaaFina uni06D1.fina_KafYaaFina uni0775.fina_KafYaaFina uni063F.fina_KafYaaFina uni0678.fina_KafYaaFina uni063D.fina_KafYaaFina uni063E.fina_KafYaaFina uni06D0.fina_KafYaaFina uni0649.fina_KafYaaFina uni0776.fina_KafYaaFina uni06CD.fina_KafYaaFina uni06CC.fina_KafYaaFina uni0626.fina_KafYaaFina uni0620.fina_KafYaaFina uni064A.fina_KafYaaFina uni06CE.fina_KafYaaFina ];
@aYaa.fina_KafYaaIsol = [ uni0777.fina_KafYaaIsol uni06D1.fina_KafYaaIsol uni0775.fina_KafYaaIsol uni063F.fina_KafYaaIsol uni0678.fina_KafYaaIsol uni063D.fina_KafYaaIsol uni063E.fina_KafYaaIsol uni06D0.fina_KafYaaIsol uni0649.fina_KafYaaIsol uni0776.fina_KafYaaIsol uni06CD.fina_KafYaaIsol uni06CC.fina_KafYaaIsol uni0626.fina_KafYaaIsol uni0620.fina_KafYaaIsol uni064A.fina_KafYaaIsol uni06CE.fina_KafYaaIsol ];
@aYaa.fina_LamYaaFina = [ uni0777.fina_LamYaaFina uni06D1.fina_LamYaaFina uni0775.fina_LamYaaFina uni063F.fina_LamYaaFina uni0678.fina_LamYaaFina uni063D.fina_LamYaaFina uni063E.fina_LamYaaFina uni06D0.fina_LamYaaFina uni0649.fina_LamYaaFina uni0776.fina_LamYaaFina uni06CD.fina_LamYaaFina uni06CC.fina_LamYaaFina uni0626.fina_LamYaaFina uni0620.fina_LamYaaFina uni064A.fina_LamYaaFina uni06CE.fina_LamYaaFina ];
@aYaa.fina_PostTooth = [ uni0777.fina_PostTooth uni06D1.fina_PostTooth uni0775.fina_PostTooth uni063F.fina_PostTooth uni0678.fina_PostTooth uni063D.fina_PostTooth uni063E.fina_PostTooth uni06D0.fina_PostTooth uni0649.fina_PostTooth uni0776.fina_PostTooth uni06CD.fina_PostTooth uni06CC.fina_PostTooth uni0626.fina_PostTooth uni0620.fina_PostTooth uni064A.fina_PostTooth uni06CE.fina_PostTooth ];
@aYaaBarree.fina_PostTooth = [ uni077B.fina_PostTooth uni077A.fina_PostTooth uni06D2.fina_PostTooth ];
@aYaaBarree.fina_PostAscender = [ uni077B.fina_PostAscender uni077A.fina_PostAscender uni06D2.fina_PostAscender ];
@aYaaBarree.fina_PostAyn = [ uni077B.fina_PostAyn uni077A.fina_PostAyn uni06D2.fina_PostAyn ];
@aAlf.isol_LowHamza = [ uni0625.LowHamza uni0673.LowHamza ];
@aBaa.init_LD = [ uni0680.init_LD uni06BD.init_LD uni067E.init_LD uni067B.init_LD uni0628.init_LD uni0767.init_LD uni063D.init_LD uni0777.init_LD uni0776.init_LD uni0775.init_LD uni06CC.init_LD uni064A.init_LD uni06CE.init_LD uni0751.init_LD uni0750.init_LD uni0753.init_LD uni0752.init_LD uni0755.init_LD uni08A0.init_LD uni0754.init_LD uni06B9.init_LD uni06D1.init_LD uni06D0.init_LD ];
@aBaa.init_BaaRaaIsolLD = [ uni0680.init_BaaRaaIsolLD uni06BD.init_BaaRaaIsolLD uni067E.init_BaaRaaIsolLD uni067B.init_BaaRaaIsolLD uni0628.init_BaaRaaIsolLD uni0767.init_BaaRaaIsolLD uni063D.init_BaaRaaIsolLD uni0777.init_BaaRaaIsolLD uni0776.init_BaaRaaIsolLD uni0775.init_BaaRaaIsolLD uni06CC.init_BaaRaaIsolLD uni064A.init_BaaRaaIsolLD uni06CE.init_BaaRaaIsolLD uni0751.init_BaaRaaIsolLD uni0750.init_BaaRaaIsolLD uni0753.init_BaaRaaIsolLD uni0752.init_BaaRaaIsolLD uni0755.init_BaaRaaIsolLD uni08A0.init_BaaRaaIsolLD uni0754.init_BaaRaaIsolLD uni06B9.init_BaaRaaIsolLD uni06D1.init_BaaRaaIsolLD uni06D0.init_BaaRaaIsolLD uni0620.init_BaaRaaIsolLD ];
@aBaa.init_BaaDalLD = [ uni0680.init_BaaDalLD uni06BD.init_BaaDalLD uni067E.init_BaaDalLD uni067B.init_BaaDalLD uni0628.init_BaaDalLD uni0767.init_BaaDalLD uni063D.init_BaaDalLD uni0777.init_BaaDalLD uni0776.init_BaaDalLD uni0775.init_BaaDalLD uni06CC.init_BaaDalLD uni064A.init_BaaDalLD uni06CE.init_BaaDalLD uni0751.init_BaaDalLD uni0750.init_BaaDalLD uni0753.init_BaaDalLD uni0752.init_BaaDalLD uni0755.init_BaaDalLD uni08A0.init_BaaDalLD uni0754.init_BaaDalLD uni06B9.init_BaaDalLD uni06D1.init_BaaDalLD uni06D0.init_BaaDalLD uni0620.init_BaaDalLD ];
@aBaa.init_BaaMemHaaInitLD =[ uni0680.init_BaaMemHaaInitLD uni06BD.init_BaaMemHaaInitLD uni067E.init_BaaMemHaaInitLD uni067B.init_BaaMemHaaInitLD uni0628.init_BaaMemHaaInitLD uni0767.init_BaaMemHaaInitLD uni063D.init_BaaMemHaaInitLD uni0777.init_BaaMemHaaInitLD uni0776.init_BaaMemHaaInitLD uni0775.init_BaaMemHaaInitLD uni06CC.init_BaaMemHaaInitLD uni064A.init_BaaMemHaaInitLD uni06CE.init_BaaMemHaaInitLD uni0751.init_BaaMemHaaInitLD uni0750.init_BaaMemHaaInitLD uni0753.init_BaaMemHaaInitLD uni0752.init_BaaMemHaaInitLD uni0755.init_BaaMemHaaInitLD uni08A0.init_BaaMemHaaInitLD uni0754.init_BaaMemHaaInitLD uni06B9.init_BaaMemHaaInitLD uni06D1.init_BaaMemHaaInitLD uni06D0.init_BaaMemHaaInitLD uni0620.init_BaaMemHaaInitLD ];
@aBaa.init_BaaBaaYaaLD = [ uni0680.init_BaaBaaYaaLD uni06BD.init_BaaBaaYaaLD uni067E.init_BaaBaaYaaLD uni067B.init_BaaBaaYaaLD uni0628.init_BaaBaaYaaLD uni0767.init_BaaBaaYaaLD uni063D.init_BaaBaaYaaLD uni0777.init_BaaBaaYaaLD uni0776.init_BaaBaaYaaLD uni0775.init_BaaBaaYaaLD uni06CC.init_BaaBaaYaaLD uni064A.init_BaaBaaYaaLD uni06CE.init_BaaBaaYaaLD uni0751.init_BaaBaaYaaLD uni0750.init_BaaBaaYaaLD uni0753.init_BaaBaaYaaLD uni0752.init_BaaBaaYaaLD uni0755.init_BaaBaaYaaLD uni08A0.init_BaaBaaYaaLD uni0754.init_BaaBaaYaaLD uni06B9.init_BaaBaaYaaLD uni06D1.init_BaaBaaYaaLD uni06D0.init_BaaBaaYaaLD uni0620.init_BaaBaaYaaLD ];
@aBaa.init_BaaNonIsolLD = [ uni0680.init_BaaNonIsolLD uni06BD.init_BaaNonIsolLD uni067E.init_BaaNonIsolLD uni067B.init_BaaNonIsolLD uni0628.init_BaaNonIsolLD uni0767.init_BaaNonIsolLD uni063D.init_BaaNonIsolLD uni0777.init_BaaNonIsolLD uni0776.init_BaaNonIsolLD uni0775.init_BaaNonIsolLD uni06CC.init_BaaNonIsolLD uni064A.init_BaaNonIsolLD uni06CE.init_BaaNonIsolLD uni0751.init_BaaNonIsolLD uni0750.init_BaaNonIsolLD uni0753.init_BaaNonIsolLD uni0752.init_BaaNonIsolLD uni0755.init_BaaNonIsolLD uni08A0.init_BaaNonIsolLD uni0754.init_BaaNonIsolLD uni06B9.init_BaaNonIsolLD uni06D1.init_BaaNonIsolLD uni06D0.init_BaaNonIsolLD uni0620.init_BaaNonIsolLD ];
@aBaa.init_BaaSenInitLD = [ uni0680.init_BaaSenInitLD uni06BD.init_BaaSenInitLD uni067E.init_BaaSenInitLD uni067B.init_BaaSenInitLD uni0628.init_BaaSenInitLD uni0767.init_BaaSenInitLD uni063D.init_BaaSenInitLD uni0777.init_BaaSenInitLD uni0776.init_BaaSenInitLD uni0775.init_BaaSenInitLD uni06CC.init_BaaSenInitLD uni064A.init_BaaSenInitLD uni06CE.init_BaaSenInitLD uni0751.init_BaaSenInitLD uni0750.init_BaaSenInitLD uni0753.init_BaaSenInitLD uni0752.init_BaaSenInitLD uni0755.init_BaaSenInitLD uni08A0.init_BaaSenInitLD uni0754.init_BaaSenInitLD uni06B9.init_BaaSenInitLD uni06D1.init_BaaSenInitLD uni06D0.init_BaaSenInitLD uni0620.init_BaaSenInitLD ];
@aBaa.init_BaaMemInitLD = [ uni0680.init_BaaMemInitLD uni06BD.init_BaaMemInitLD uni067E.init_BaaMemInitLD uni067B.init_BaaMemInitLD uni0628.init_BaaMemInitLD uni0767.init_BaaMemInitLD uni063D.init_BaaMemInitLD uni0777.init_BaaMemInitLD uni0776.init_BaaMemInitLD uni0775.init_BaaMemInitLD uni06CC.init_BaaMemInitLD uni064A.init_BaaMemInitLD uni06CE.init_BaaMemInitLD uni0751.init_BaaMemInitLD uni0750.init_BaaMemInitLD uni0753.init_BaaMemInitLD uni0752.init_BaaMemInitLD uni0755.init_BaaMemInitLD uni08A0.init_BaaMemInitLD uni0754.init_BaaMemInitLD uni06B9.init_BaaMemInitLD uni06D1.init_BaaMemInitLD uni06D0.init_BaaMemInitLD uni0620.init_BaaMemInitLD ];
@aBaa.init_BaaBaaHaaInitLD = [ uni0680.init_BaaBaaHaaInitLD uni06BD.init_BaaBaaHaaInitLD uni067E.init_BaaBaaHaaInitLD uni067B.init_BaaBaaHaaInitLD uni0628.init_BaaBaaHaaInitLD uni0767.init_BaaBaaHaaInitLD uni063D.init_BaaBaaHaaInitLD uni0777.init_BaaBaaHaaInitLD uni0776.init_BaaBaaHaaInitLD uni0775.init_BaaBaaHaaInitLD uni06CC.init_BaaBaaHaaInitLD uni064A.init_BaaBaaHaaInitLD uni06CE.init_BaaBaaHaaInitLD uni0751.init_BaaBaaHaaInitLD uni0750.init_BaaBaaHaaInitLD uni0753.init_BaaBaaHaaInitLD uni0752.init_BaaBaaHaaInitLD uni0755.init_BaaBaaHaaInitLD uni08A0.init_BaaBaaHaaInitLD uni0754.init_BaaBaaHaaInitLD uni06B9.init_BaaBaaHaaInitLD uni06D1.init_BaaBaaHaaInitLD uni06D0.init_BaaBaaHaaInitLD uni0620.init_BaaBaaHaaInitLD ];
@aBaa.init_BaaBaaHehLD = [ uni0680.init_BaaBaaHehLD uni06BD.init_BaaBaaHehLD uni067E.init_BaaBaaHehLD uni067B.init_BaaBaaHehLD uni0628.init_BaaBaaHehLD uni0767.init_BaaBaaHehLD uni063D.init_BaaBaaHehLD uni0777.init_BaaBaaHehLD uni0776.init_BaaBaaHehLD uni0775.init_BaaBaaHehLD uni06CC.init_BaaBaaHehLD uni064A.init_BaaBaaHehLD uni06CE.init_BaaBaaHehLD uni0751.init_BaaBaaHehLD uni0750.init_BaaBaaHehLD uni0753.init_BaaBaaHehLD uni0752.init_BaaBaaHehLD uni0755.init_BaaBaaHehLD uni08A0.init_BaaBaaHehLD uni0754.init_BaaBaaHehLD uni06B9.init_BaaBaaHehLD uni06D1.init_BaaBaaHehLD uni06D0.init_BaaBaaHehLD uni0620.init_BaaBaaHehLD ];
@aBaa.init_BaaBaaIsolLD = [ uni0680.init_BaaBaaIsolLD uni06BD.init_BaaBaaIsolLD uni067E.init_BaaBaaIsolLD uni067B.init_BaaBaaIsolLD uni0628.init_BaaBaaIsolLD uni0767.init_BaaBaaIsolLD uni063D.init_BaaBaaIsolLD uni0777.init_BaaBaaIsolLD uni0776.init_BaaBaaIsolLD uni0775.init_BaaBaaIsolLD uni06CC.init_BaaBaaIsolLD uni064A.init_BaaBaaIsolLD uni06CE.init_BaaBaaIsolLD uni0751.init_BaaBaaIsolLD uni0750.init_BaaBaaIsolLD uni0753.init_BaaBaaIsolLD uni0752.init_BaaBaaIsolLD uni0755.init_BaaBaaIsolLD uni08A0.init_BaaBaaIsolLD uni0754.init_BaaBaaIsolLD uni06B9.init_BaaBaaIsolLD uni06D1.init_BaaBaaIsolLD uni06D0.init_BaaBaaIsolLD uni0620.init_BaaBaaIsolLD ];
@aBaa.init_BaaBaaMemInitLD = [ uni0680.init_BaaBaaMemInitLD uni06BD.init_BaaBaaMemInitLD uni067E.init_BaaBaaMemInitLD uni067B.init_BaaBaaMemInitLD uni0628.init_BaaBaaMemInitLD uni0767.init_BaaBaaMemInitLD uni063D.init_BaaBaaMemInitLD uni0777.init_BaaBaaMemInitLD uni0776.init_BaaBaaMemInitLD uni0775.init_BaaBaaMemInitLD uni06CC.init_BaaBaaMemInitLD uni064A.init_BaaBaaMemInitLD uni06CE.init_BaaBaaMemInitLD uni0751.init_BaaBaaMemInitLD uni0750.init_BaaBaaMemInitLD uni0753.init_BaaBaaMemInitLD uni0752.init_BaaBaaMemInitLD uni0755.init_BaaBaaMemInitLD uni08A0.init_BaaBaaMemInitLD uni0754.init_BaaBaaMemInitLD uni06B9.init_BaaBaaMemInitLD uni06D1.init_BaaBaaMemInitLD uni06D0.init_BaaBaaMemInitLD uni0620.init_BaaBaaMemInitLD ];
@aBaa.init_BaaSenAltInitLD = [ uni0680.init_BaaSenAltInitLD uni06BD.init_BaaSenAltInitLD uni067E.init_BaaSenAltInitLD uni067B.init_BaaSenAltInitLD uni0628.init_BaaSenAltInitLD uni0767.init_BaaSenAltInitLD uni063D.init_BaaSenAltInitLD uni0777.init_BaaSenAltInitLD uni0776.init_BaaSenAltInitLD uni0775.init_BaaSenAltInitLD uni06CC.init_BaaSenAltInitLD uni064A.init_BaaSenAltInitLD uni06CE.init_BaaSenAltInitLD uni0751.init_BaaSenAltInitLD uni0750.init_BaaSenAltInitLD uni0753.init_BaaSenAltInitLD uni0752.init_BaaSenAltInitLD uni0755.init_BaaSenAltInitLD uni08A0.init_BaaSenAltInitLD uni0754.init_BaaSenAltInitLD uni06B9.init_BaaSenAltInitLD uni06D1.init_BaaSenAltInitLD uni06D0.init_BaaSenAltInitLD uni0620.init_BaaSenAltInitLD ];
@aBaa.init_BaaHaaInitLD = [ uni0680.init_BaaHaaInitLD uni06BD.init_BaaHaaInitLD uni067E.init_BaaHaaInitLD uni067B.init_BaaHaaInitLD uni0628.init_BaaHaaInitLD uni0767.init_BaaHaaInitLD uni063D.init_BaaHaaInitLD uni0777.init_BaaHaaInitLD uni0776.init_BaaHaaInitLD uni0775.init_BaaHaaInitLD uni06CC.init_BaaHaaInitLD uni064A.init_BaaHaaInitLD uni06CE.init_BaaHaaInitLD uni0751.init_BaaHaaInitLD uni0750.init_BaaHaaInitLD uni0753.init_BaaHaaInitLD uni0752.init_BaaHaaInitLD uni0755.init_BaaHaaInitLD uni08A0.init_BaaHaaInitLD uni0754.init_BaaHaaInitLD uni06B9.init_BaaHaaInitLD uni06D1.init_BaaHaaInitLD uni06D0.init_BaaHaaInitLD uni0620.init_BaaHaaInitLD ];
@aBaa.init_BaaHaaMemInitLD = [ uni0680.init_BaaHaaMemInitLD uni06BD.init_BaaHaaMemInitLD uni067E.init_BaaHaaMemInitLD uni067B.init_BaaHaaMemInitLD uni0628.init_BaaHaaMemInitLD uni0767.init_BaaHaaMemInitLD uni063D.init_BaaHaaMemInitLD uni0777.init_BaaHaaMemInitLD uni0776.init_BaaHaaMemInitLD uni0775.init_BaaHaaMemInitLD uni06CC.init_BaaHaaMemInitLD uni064A.init_BaaHaaMemInitLD uni06CE.init_BaaHaaMemInitLD uni0751.init_BaaHaaMemInitLD uni0750.init_BaaHaaMemInitLD uni0753.init_BaaHaaMemInitLD uni0752.init_BaaHaaMemInitLD uni0755.init_BaaHaaMemInitLD uni08A0.init_BaaHaaMemInitLD uni0754.init_BaaHaaMemInitLD uni06B9.init_BaaHaaMemInitLD uni06D1.init_BaaHaaMemInitLD uni06D0.init_BaaHaaMemInitLD uni0620.init_BaaHaaMemInitLD ];
@aBaa.init_HighLD = [ uni0680.init_HighLD uni06BD.init_HighLD uni067E.init_HighLD uni067B.init_HighLD uni0628.init_HighLD uni0767.init_HighLD uni063D.init_HighLD uni0777.init_HighLD uni0776.init_HighLD uni0775.init_HighLD uni06CC.init_HighLD uni064A.init_HighLD uni06CE.init_HighLD uni0751.init_HighLD uni0750.init_HighLD uni0753.init_HighLD uni0752.init_HighLD uni0755.init_HighLD uni08A0.init_HighLD uni0754.init_HighLD uni06B9.init_HighLD uni06D1.init_HighLD uni06D0.init_HighLD uni0620.init_HighLD ];
@aBaa.init_WideLD = [ uni0680.init_WideLD uni06BD.init_WideLD uni067E.init_WideLD uni067B.init_WideLD uni0628.init_WideLD uni0767.init_WideLD uni063D.init_WideLD uni0777.init_WideLD uni0776.init_WideLD uni0775.init_WideLD uni06CC.init_WideLD uni064A.init_WideLD uni06CE.init_WideLD uni0751.init_WideLD uni0750.init_WideLD uni0753.init_WideLD uni0752.init_WideLD uni0755.init_WideLD uni08A0.init_WideLD uni0754.init_WideLD uni06B9.init_WideLD uni06D1.init_WideLD uni06D0.init_WideLD uni0620.init_WideLD ];
@aBaa.init_BaaYaaIsolLD = [ uni0680.init_BaaYaaIsolLD uni06BD.init_BaaYaaIsolLD uni067E.init_BaaYaaIsolLD uni067B.init_BaaYaaIsolLD uni0628.init_BaaYaaIsolLD uni0767.init_BaaYaaIsolLD uni063D.init_BaaYaaIsolLD uni0777.init_BaaYaaIsolLD uni0776.init_BaaYaaIsolLD uni0775.init_BaaYaaIsolLD uni06CC.init_BaaYaaIsolLD uni064A.init_BaaYaaIsolLD uni06CE.init_BaaYaaIsolLD uni0751.init_BaaYaaIsolLD uni0750.init_BaaYaaIsolLD uni0753.init_BaaYaaIsolLD uni0752.init_BaaYaaIsolLD uni0755.init_BaaYaaIsolLD uni08A0.init_BaaYaaIsolLD uni0754.init_BaaYaaIsolLD uni06B9.init_BaaYaaIsolLD uni06D1.init_BaaYaaIsolLD uni06D0.init_BaaYaaIsolLD uni0620.init_BaaYaaIsolLD ];
@aBaa.init_BaaMemIsolLD = [ uni0680.init_BaaMemIsolLD uni06BD.init_BaaMemIsolLD uni067E.init_BaaMemIsolLD uni067B.init_BaaMemIsolLD uni0628.init_BaaMemIsolLD uni0767.init_BaaMemIsolLD uni063D.init_BaaMemIsolLD uni0777.init_BaaMemIsolLD uni0776.init_BaaMemIsolLD uni0775.init_BaaMemIsolLD uni06CC.init_BaaMemIsolLD uni064A.init_BaaMemIsolLD uni06CE.init_BaaMemIsolLD uni0751.init_BaaMemIsolLD uni0750.init_BaaMemIsolLD uni0753.init_BaaMemIsolLD uni0752.init_BaaMemIsolLD uni0755.init_BaaMemIsolLD uni08A0.init_BaaMemIsolLD uni0754.init_BaaMemIsolLD uni06B9.init_BaaMemIsolLD uni06D1.init_BaaMemIsolLD uni06D0.init_BaaMemIsolLD uni0620.init_BaaMemIsolLD ];
@aBaa.init_BaaHehInitLD = [ uni0680.init_BaaHehInitLD uni06BD.init_BaaHehInitLD uni067E.init_BaaHehInitLD uni067B.init_BaaHehInitLD uni0628.init_BaaHehInitLD uni0767.init_BaaHehInitLD uni063D.init_BaaHehInitLD uni0777.init_BaaHehInitLD uni0776.init_BaaHehInitLD uni0775.init_BaaHehInitLD uni06CC.init_BaaHehInitLD uni064A.init_BaaHehInitLD uni06CE.init_BaaHehInitLD uni0751.init_BaaHehInitLD uni0750.init_BaaHehInitLD uni0753.init_BaaHehInitLD uni0752.init_BaaHehInitLD uni0755.init_BaaHehInitLD uni08A0.init_BaaHehInitLD uni0754.init_BaaHehInitLD uni06B9.init_BaaHehInitLD uni06D1.init_BaaHehInitLD uni06D0.init_BaaHehInitLD uni0620.init_BaaHehInitLD ];
@aBaaDotBelow = [ uni0680.init uni06BD.init uni067E.init uni067B.init uni0628.init uni0767.init uni063D.init uni0777.init uni0776.init uni0775.init uni06CC.init uni064A.init uni06CE.init uni0751.init uni0750.init uni0753.init uni0752.init uni0755.init uni08A0.init uni0754.init uni06B9.init uni06D1.init uni06D0.init uni0680.init_BaaRaaIsol uni06BD.init_BaaRaaIsol uni067E.init_BaaRaaIsol uni067B.init_BaaRaaIsol uni0628.init_BaaRaaIsol uni0767.init_BaaRaaIsol uni063D.init_BaaRaaIsol uni0777.init_BaaRaaIsol uni0776.init_BaaRaaIsol uni0775.init_BaaRaaIsol uni06CC.init_BaaRaaIsol uni064A.init_BaaRaaIsol uni06CE.init_BaaRaaIsol uni0751.init_BaaRaaIsol uni0750.init_BaaRaaIsol uni0753.init_BaaRaaIsol uni0752.init_BaaRaaIsol uni0755.init_BaaRaaIsol uni08A0.init_BaaRaaIsol uni0754.init_BaaRaaIsol uni06B9.init_BaaRaaIsol uni06D1.init_BaaRaaIsol uni06D0.init_BaaRaaIsol uni0620.init_BaaRaaIsol uni0680.init_BaaDal uni06BD.init_BaaDal uni067E.init_BaaDal uni067B.init_BaaDal uni0628.init_BaaDal uni0767.init_BaaDal uni063D.init_BaaDal uni0777.init_BaaDal uni0776.init_BaaDal uni0775.init_BaaDal uni06CC.init_BaaDal uni064A.init_BaaDal uni06CE.init_BaaDal uni0751.init_BaaDal uni0750.init_BaaDal uni0753.init_BaaDal uni0752.init_BaaDal uni0755.init_BaaDal uni08A0.init_BaaDal uni0754.init_BaaDal uni06B9.init_BaaDal uni06D1.init_BaaDal uni06D0.init_BaaDal uni0620.init_BaaDal uni0680.init_BaaMemHaaInit uni06BD.init_BaaMemHaaInit uni067E.init_BaaMemHaaInit uni067B.init_BaaMemHaaInit uni0628.init_BaaMemHaaInit uni0767.init_BaaMemHaaInit uni063D.init_BaaMemHaaInit uni0777.init_BaaMemHaaInit uni0776.init_BaaMemHaaInit uni0775.init_BaaMemHaaInit uni06CC.init_BaaMemHaaInit uni064A.init_BaaMemHaaInit uni06CE.init_BaaMemHaaInit uni0751.init_BaaMemHaaInit uni0750.init_BaaMemHaaInit uni0753.init_BaaMemHaaInit uni0752.init_BaaMemHaaInit uni0755.init_BaaMemHaaInit uni08A0.init_BaaMemHaaInit uni0754.init_BaaMemHaaInit uni06B9.init_BaaMemHaaInit uni06D1.init_BaaMemHaaInit uni06D0.init_BaaMemHaaInit uni0620.init_BaaMemHaaInit uni0680.init_BaaBaaYaa uni06BD.init_BaaBaaYaa uni067E.init_BaaBaaYaa uni067B.init_BaaBaaYaa uni0628.init_BaaBaaYaa uni0767.init_BaaBaaYaa uni063D.init_BaaBaaYaa uni0777.init_BaaBaaYaa uni0776.init_BaaBaaYaa uni0775.init_BaaBaaYaa uni06CC.init_BaaBaaYaa uni064A.init_BaaBaaYaa uni06CE.init_BaaBaaYaa uni0751.init_BaaBaaYaa uni0750.init_BaaBaaYaa uni0753.init_BaaBaaYaa uni0752.init_BaaBaaYaa uni0755.init_BaaBaaYaa uni08A0.init_BaaBaaYaa uni0754.init_BaaBaaYaa uni06B9.init_BaaBaaYaa uni06D1.init_BaaBaaYaa uni06D0.init_BaaBaaYaa uni0620.init_BaaBaaYaa uni0680.init_BaaNonIsol uni06BD.init_BaaNonIsol uni067E.init_BaaNonIsol uni067B.init_BaaNonIsol uni0628.init_BaaNonIsol uni0767.init_BaaNonIsol uni063D.init_BaaNonIsol uni0777.init_BaaNonIsol uni0776.init_BaaNonIsol uni0775.init_BaaNonIsol uni06CC.init_BaaNonIsol uni064A.init_BaaNonIsol uni06CE.init_BaaNonIsol uni0751.init_BaaNonIsol uni0750.init_BaaNonIsol uni0753.init_BaaNonIsol uni0752.init_BaaNonIsol uni0755.init_BaaNonIsol uni08A0.init_BaaNonIsol uni0754.init_BaaNonIsol uni06B9.init_BaaNonIsol uni06D1.init_BaaNonIsol uni06D0.init_BaaNonIsol uni0620.init_BaaNonIsol uni0680.init_BaaSenInit uni06BD.init_BaaSenInit uni067E.init_BaaSenInit uni067B.init_BaaSenInit uni0628.init_BaaSenInit uni0767.init_BaaSenInit uni063D.init_BaaSenInit uni0777.init_BaaSenInit uni0776.init_BaaSenInit uni0775.init_BaaSenInit uni06CC.init_BaaSenInit uni064A.init_BaaSenInit uni06CE.init_BaaSenInit uni0751.init_BaaSenInit uni0750.init_BaaSenInit uni0753.init_BaaSenInit uni0752.init_BaaSenInit uni0755.init_BaaSenInit uni08A0.init_BaaSenInit uni0754.init_BaaSenInit uni06B9.init_BaaSenInit uni06D1.init_BaaSenInit uni06D0.init_BaaSenInit uni0620.init_BaaSenInit uni0680.init_BaaMemInit uni06BD.init_BaaMemInit uni067E.init_BaaMemInit uni067B.init_BaaMemInit uni0628.init_BaaMemInit uni0767.init_BaaMemInit uni063D.init_BaaMemInit uni0777.init_BaaMemInit uni0776.init_BaaMemInit uni0775.init_BaaMemInit uni06CC.init_BaaMemInit uni064A.init_BaaMemInit uni06CE.init_BaaMemInit uni0751.init_BaaMemInit uni0750.init_BaaMemInit uni0753.init_BaaMemInit uni0752.init_BaaMemInit uni0755.init_BaaMemInit uni08A0.init_BaaMemInit uni0754.init_BaaMemInit uni06B9.init_BaaMemInit uni06D1.init_BaaMemInit uni06D0.init_BaaMemInit uni0620.init_BaaMemInit uni0680.init_BaaBaaHaaInit uni06BD.init_BaaBaaHaaInit uni067E.init_BaaBaaHaaInit uni067B.init_BaaBaaHaaInit uni0628.init_BaaBaaHaaInit uni0767.init_BaaBaaHaaInit uni063D.init_BaaBaaHaaInit uni0777.init_BaaBaaHaaInit uni0776.init_BaaBaaHaaInit uni0775.init_BaaBaaHaaInit uni06CC.init_BaaBaaHaaInit uni064A.init_BaaBaaHaaInit uni06CE.init_BaaBaaHaaInit uni0751.init_BaaBaaHaaInit uni0750.init_BaaBaaHaaInit uni0753.init_BaaBaaHaaInit uni0752.init_BaaBaaHaaInit uni0755.init_BaaBaaHaaInit uni08A0.init_BaaBaaHaaInit uni0754.init_BaaBaaHaaInit uni06B9.init_BaaBaaHaaInit uni06D1.init_BaaBaaHaaInit uni06D0.init_BaaBaaHaaInit uni0620.init_BaaBaaHaaInit uni0680.init_BaaBaaHeh uni06BD.init_BaaBaaHeh uni067E.init_BaaBaaHeh uni067B.init_BaaBaaHeh uni0628.init_BaaBaaHeh uni0767.init_BaaBaaHeh uni063D.init_BaaBaaHeh uni0777.init_BaaBaaHeh uni0776.init_BaaBaaHeh uni0775.init_BaaBaaHeh uni06CC.init_BaaBaaHeh uni064A.init_BaaBaaHeh uni06CE.init_BaaBaaHeh uni0751.init_BaaBaaHeh uni0750.init_BaaBaaHeh uni0753.init_BaaBaaHeh uni0752.init_BaaBaaHeh uni0755.init_BaaBaaHeh uni08A0.init_BaaBaaHeh uni0754.init_BaaBaaHeh uni06B9.init_BaaBaaHeh uni06D1.init_BaaBaaHeh uni06D0.init_BaaBaaHeh uni0620.init_BaaBaaHeh uni0680.init_BaaBaaIsol uni06BD.init_BaaBaaIsol uni067E.init_BaaBaaIsol uni067B.init_BaaBaaIsol uni0628.init_BaaBaaIsol uni0767.init_BaaBaaIsol uni063D.init_BaaBaaIsol uni0777.init_BaaBaaIsol uni0776.init_BaaBaaIsol uni0775.init_BaaBaaIsol uni06CC.init_BaaBaaIsol uni064A.init_BaaBaaIsol uni06CE.init_BaaBaaIsol uni0751.init_BaaBaaIsol uni0750.init_BaaBaaIsol uni0753.init_BaaBaaIsol uni0752.init_BaaBaaIsol uni0755.init_BaaBaaIsol uni08A0.init_BaaBaaIsol uni0754.init_BaaBaaIsol uni06B9.init_BaaBaaIsol uni06D1.init_BaaBaaIsol uni06D0.init_BaaBaaIsol uni0620.init_BaaBaaIsol uni0680.init_BaaBaaMemInit uni06BD.init_BaaBaaMemInit uni067E.init_BaaBaaMemInit uni067B.init_BaaBaaMemInit uni0628.init_BaaBaaMemInit uni0767.init_BaaBaaMemInit uni063D.init_BaaBaaMemInit uni0777.init_BaaBaaMemInit uni0776.init_BaaBaaMemInit uni0775.init_BaaBaaMemInit uni06CC.init_BaaBaaMemInit uni064A.init_BaaBaaMemInit uni06CE.init_BaaBaaMemInit uni0751.init_BaaBaaMemInit uni0750.init_BaaBaaMemInit uni0753.init_BaaBaaMemInit uni0752.init_BaaBaaMemInit uni0755.init_BaaBaaMemInit uni08A0.init_BaaBaaMemInit uni0754.init_BaaBaaMemInit uni06B9.init_BaaBaaMemInit uni06D1.init_BaaBaaMemInit uni06D0.init_BaaBaaMemInit uni0620.init_BaaBaaMemInit uni0680.init_BaaSenAltInit uni06BD.init_BaaSenAltInit uni067E.init_BaaSenAltInit uni067B.init_BaaSenAltInit uni0628.init_BaaSenAltInit uni0767.init_BaaSenAltInit uni063D.init_BaaSenAltInit uni0777.init_BaaSenAltInit uni0776.init_BaaSenAltInit uni0775.init_BaaSenAltInit uni06CC.init_BaaSenAltInit uni064A.init_BaaSenAltInit uni06CE.init_BaaSenAltInit uni0751.init_BaaSenAltInit uni0750.init_BaaSenAltInit uni0753.init_BaaSenAltInit uni0752.init_BaaSenAltInit uni0755.init_BaaSenAltInit uni08A0.init_BaaSenAltInit uni0754.init_BaaSenAltInit uni06B9.init_BaaSenAltInit uni06D1.init_BaaSenAltInit uni06D0.init_BaaSenAltInit uni0620.init_BaaSenAltInit uni0680.init_BaaHaaInit uni06BD.init_BaaHaaInit uni067E.init_BaaHaaInit uni067B.init_BaaHaaInit uni0628.init_BaaHaaInit uni0767.init_BaaHaaInit uni063D.init_BaaHaaInit uni0777.init_BaaHaaInit uni0776.init_BaaHaaInit uni0775.init_BaaHaaInit uni06CC.init_BaaHaaInit uni064A.init_BaaHaaInit uni06CE.init_BaaHaaInit uni0751.init_BaaHaaInit uni0750.init_BaaHaaInit uni0753.init_BaaHaaInit uni0752.init_BaaHaaInit uni0755.init_BaaHaaInit uni08A0.init_BaaHaaInit uni0754.init_BaaHaaInit uni06B9.init_BaaHaaInit uni06D1.init_BaaHaaInit uni06D0.init_BaaHaaInit uni0620.init_BaaHaaInit uni0680.init_BaaHaaMemInit uni06BD.init_BaaHaaMemInit uni067E.init_BaaHaaMemInit uni067B.init_BaaHaaMemInit uni0628.init_BaaHaaMemInit uni0767.init_BaaHaaMemInit uni063D.init_BaaHaaMemInit uni0777.init_BaaHaaMemInit uni0776.init_BaaHaaMemInit uni0775.init_BaaHaaMemInit uni06CC.init_BaaHaaMemInit uni064A.init_BaaHaaMemInit uni06CE.init_BaaHaaMemInit uni0751.init_BaaHaaMemInit uni0750.init_BaaHaaMemInit uni0753.init_BaaHaaMemInit uni0752.init_BaaHaaMemInit uni0755.init_BaaHaaMemInit uni08A0.init_BaaHaaMemInit uni0754.init_BaaHaaMemInit uni06B9.init_BaaHaaMemInit uni06D1.init_BaaHaaMemInit uni06D0.init_BaaHaaMemInit uni0620.init_BaaHaaMemInit uni0680.init_High uni06BD.init_High uni067E.init_High uni067B.init_High uni0628.init_High uni0767.init_High uni063D.init_High uni0777.init_High uni0776.init_High uni0775.init_High uni06CC.init_High uni064A.init_High uni06CE.init_High uni0751.init_High uni0750.init_High uni0753.init_High uni0752.init_High uni0755.init_High uni08A0.init_High uni0754.init_High uni06B9.init_High uni06D1.init_High uni06D0.init_High uni0620.init_High uni0680.init_Wide uni06BD.init_Wide uni067E.init_Wide uni067B.init_Wide uni0628.init_Wide uni0767.init_Wide uni063D.init_Wide uni0777.init_Wide uni0776.init_Wide uni0775.init_Wide uni06CC.init_Wide uni064A.init_Wide uni06CE.init_Wide uni0751.init_Wide uni0750.init_Wide uni0753.init_Wide uni0752.init_Wide uni0755.init_Wide uni08A0.init_Wide uni0754.init_Wide uni06B9.init_Wide uni06D1.init_Wide uni06D0.init_Wide uni0620.init_Wide uni0680.init_BaaYaaIsol uni06BD.init_BaaYaaIsol uni067E.init_BaaYaaIsol uni067B.init_BaaYaaIsol uni0628.init_BaaYaaIsol uni0767.init_BaaYaaIsol uni063D.init_BaaYaaIsol uni0777.init_BaaYaaIsol uni0776.init_BaaYaaIsol uni0775.init_BaaYaaIsol uni06CC.init_BaaYaaIsol uni064A.init_BaaYaaIsol uni06CE.init_BaaYaaIsol uni0751.init_BaaYaaIsol uni0750.init_BaaYaaIsol uni0753.init_BaaYaaIsol uni0752.init_BaaYaaIsol uni0755.init_BaaYaaIsol uni08A0.init_BaaYaaIsol uni0754.init_BaaYaaIsol uni06B9.init_BaaYaaIsol uni06D1.init_BaaYaaIsol uni06D0.init_BaaYaaIsol uni0620.init_BaaYaaIsol uni0680.init_BaaMemIsol uni06BD.init_BaaMemIsol uni067E.init_BaaMemIsol uni067B.init_BaaMemIsol uni0628.init_BaaMemIsol uni0767.init_BaaMemIsol uni063D.init_BaaMemIsol uni0777.init_BaaMemIsol uni0776.init_BaaMemIsol uni0775.init_BaaMemIsol uni06CC.init_BaaMemIsol uni064A.init_BaaMemIsol uni06CE.init_BaaMemIsol uni0751.init_BaaMemIsol uni0750.init_BaaMemIsol uni0753.init_BaaMemIsol uni0752.init_BaaMemIsol uni0755.init_BaaMemIsol uni08A0.init_BaaMemIsol uni0754.init_BaaMemIsol uni06B9.init_BaaMemIsol uni06D1.init_BaaMemIsol uni06D0.init_BaaMemIsol uni0620.init_BaaMemIsol uni0680.init_BaaHehInit uni06BD.init_BaaHehInit uni067E.init_BaaHehInit uni067B.init_BaaHehInit uni0628.init_BaaHehInit uni0767.init_BaaHehInit uni063D.init_BaaHehInit uni0777.init_BaaHehInit uni0776.init_BaaHehInit uni0775.init_BaaHehInit uni06CC.init_BaaHehInit uni064A.init_BaaHehInit uni06CE.init_BaaHehInit uni0751.init_BaaHehInit uni0750.init_BaaHehInit uni0753.init_BaaHehInit uni0752.init_BaaHehInit uni0755.init_BaaHehInit uni08A0.init_BaaHehInit uni0754.init_BaaHehInit uni06B9.init_BaaHehInit uni06D1.init_BaaHehInit uni06D0.init_BaaHehInit uni0620.init_BaaHehInit ];
@aBaaLowDotBelow = [ @aBaa.init_LD @aBaa.init_BaaRaaIsolLD @aBaa.init_BaaDalLD @aBaa.init_BaaMemHaaInitLD @aBaa.init_BaaBaaYaaLD @aBaa.init_BaaNonIsolLD @aBaa.init_BaaSenInitLD @aBaa.init_BaaMemInitLD @aBaa.init_BaaBaaHaaInitLD @aBaa.init_BaaBaaHehLD @aBaa.init_BaaBaaIsolLD @aBaa.init_BaaBaaMemInitLD @aBaa.init_BaaSenAltInitLD @aBaa.init_BaaHaaInitLD @aBaa.init_BaaHaaMemInitLD @aBaa.init_HighLD @aBaa.init_WideLD @aBaa.init_BaaYaaIsolLD @aBaa.init_BaaMemIsolLD @aBaa.init_BaaHehInitLD ];
@AlefHamzaAbove = [ uni0623 uni0672 uni0675 uni0623.fina uni0675.fina uni0672.fina uni0623.fina_LamAlfIsol uni0675.fina_LamAlfIsol uni0672.fina_LamAlfIsol uni0623.fina_LamAlfFina uni0675.fina_LamAlfFina uni0672.fina_LamAlfFina uni0623.fina_KafAlf uni0675.fina_KafAlf uni0672.fina_KafAlf uni0623.fina_KafMemAlf uni0675.fina_KafMemAlf uni0672.fina_KafMemAlf uni0623.fina_MemAlfFina uni0675.fina_MemAlfFina uni0672.fina_MemAlfFina ];
@AlefHamzaBelow = [ uni0625 uni0673 uni0625.fina uni0673.fina uni0625.fina_LamAlfIsol uni0673.fina_LamAlfIsol uni0625.fina_LamAlfFina uni0673.fina_LamAlfFina uni0625.fina_KafAlf uni0673.fina_KafAlf uni0625.fina_KafMemAlf uni0673.fina_KafMemAlf uni0625.fina_MemAlfFina uni0673.fina_MemAlfFina ];

@Digits = [ zero one two three four five six seven eight nine uni0660 uni0661 uni0662 uni0663 uni0664 uni0665 uni0666 uni0667 uni0668 uni0669 uni06F0 uni06F1 uni06F2 uni06F3 uni06F4 uni06F5 uni06F6 uni06F7 uni06F8 uni06F9 uni06F4.urd uni06F6.urd uni06F7.urd ];
@Digits.small = [ zero.small one.small two.small three.small four.small five.small six.small seven.small eight.small nine.small uni0660.small uni0661.small uni0662.small uni0663.small uni0664.small uni0665.small uni0666.small uni0667.small uni0668.small uni0669.small uni06F0.small uni06F1.small uni06F2.small uni06F3.small uni06F4.small uni06F5.small uni06F6.small uni06F7.small uni06F8.small uni06F9.small uni06F4.urd.small uni06F6.urd.small uni06F7.urd.small ];
@Digits.medium = [ zero.medium one.medium two.medium three.medium four.medium five.medium six.medium seven.medium eight.medium nine.medium uni0660.medium uni0661.medium uni0662.medium uni0663.medium uni0664.medium uni0665.medium uni0666.medium uni0667.medium uni0668.medium uni0669.medium uni06F0.medium uni06F1.medium uni06F2.medium uni06F3.medium uni06F4.medium uni06F5.medium uni06F6.medium uni06F7.medium uni06F8.medium uni06F9.medium uni06F4.urd.medium uni06F6.urd.medium uni06F7.urd.medium ];

@Tashkil.above = [ uni0618 uni0619 uni061A uni064B uni064C uni064E uni064F uni0651 uni0652 uni0657 uni0658 uni0659 uni065A uni065B uni065D uni065E uni06DF uni06E0 uni06E1 uni06E2 uni06E4 uni06E8 uni06EB uni06EC uni08F0 uni08F1 uni08F0.small uni064B.small uni064E.small uni08F1.small uni064F.small uni064E.small2 uni0652.small2 uni064C.small ];
@Tashkil.below = [ uni064D uni0650 uni065C uni06EA uni06ED uni08F2 uni0650.small2 ];

@RaaWaw = [ @aRaa.fina @aRaa.isol @aWaw.fina @aWaw.isol @aRaa.fina_BaaRaaIsol @aWaw.fina_LamWawFina @aRaa.fina_BaaRaaFina @aRaa.fina_KafRaaFina @aRaa.fina_KafRaaIsol @aRaa.fina_HaaRaaIsol @aRaa.fina_LamRaaIsol @aRaa.fina_PostTooth ];
feature ccmp {
    sub uni0627 uni065F by uni0673;
} ccmp;

lookup loclPunct {
  sub period         by period.ara;
} loclPunct;

lookup loclUrduDigits {
  sub [uni06F4 uni06F6 uni06F7] by [uni06F4.urd uni06F6.urd uni06F7.urd];
} loclUrduDigits;

lookup loclSindhiDigits {
  sub [uni06F6 uni06F7] by [uni06F6.urd uni06F7.urd];
} loclSindhiDigits;

feature locl {
  script arab;
  language ARA;
  lookup loclPunct;

  language URD exclude_dflt;
  lookup loclUrduDigits;
  lookup loclPunct;

  language SND exclude_dflt;
  lookup loclSindhiDigits;
  lookup loclPunct;
} locl;



feature ccmp {
  script arab;
  language ARA;
  lookup loclPunct;

  language URD exclude_dflt;
  lookup loclUrduDigits;
  lookup loclPunct;

  language SND exclude_dflt;
  lookup loclSindhiDigits;
  lookup loclPunct;
} ccmp;


feature pnum {
  sub zero by zero.prop;
  sub one by one.prop;
  sub two by two.prop;
  sub three by three.prop;
  sub four by four.prop;
  sub five by five.prop;
  sub six by six.prop;
  sub seven by seven.prop;
  sub eight by eight.prop;
  sub nine by nine.prop;
  sub uni0660 by uni0660.prop;
  sub uni0661 by uni0661.prop;
  sub uni0662 by uni0662.prop;
  sub uni0663 by uni0663.prop;
  sub uni0664 by uni0664.prop;
  sub uni0665 by uni0665.prop;
  sub uni0666 by uni0666.prop;
  sub uni0667 by uni0667.prop;
  sub uni0668 by uni0668.prop;
  sub uni0669 by uni0669.prop;
  sub uni06F0 by uni06F0.prop;
  sub uni06F1 by uni06F1.prop;
  sub uni06F2 by uni06F2.prop;
  sub uni06F3 by uni06F3.prop;
  sub uni06F4 by uni06F4.prop;
  sub uni06F5 by uni06F5.prop;
  sub uni06F6 by uni06F6.prop;
  sub uni06F7 by uni06F7.prop;
  sub uni06F8 by uni06F8.prop;
  sub uni06F9 by uni06F9.prop;
  sub uni06F4.urd by uni06F4.urd.prop;
  sub uni06F6.urd by uni06F6.urd.prop;
  sub uni06F7.urd by uni06F7.urd.prop;
} pnum;

feature init {
  lookupflag IgnoreMarks;
  sub uni0620 by uni0620.init;
  sub uni0626 by uni0626.init;
  sub uni0628 by uni0628.init;
  sub uni062A by uni062A.init;
  sub uni062B by uni062B.init;
  sub uni062C by uni062C.init;
  sub uni062D by uni062D.init;
  sub uni062E by uni062E.init;
  sub uni0633 by uni0633.init;
  sub uni0634 by uni0634.init;
  sub uni0635 by uni0635.init;
  sub uni0636 by uni0636.init;
  sub uni0637 by uni0637.init;
  sub uni0638 by uni0638.init;
  sub uni0639 by uni0639.init;
  sub uni063A by uni063A.init;
  sub uni063B by uni063B.init;
  sub uni063C by uni063C.init;
  sub uni063D by uni063D.init;
  sub uni0641 by uni0641.init;
  sub uni0642 by uni0642.init;
  sub uni0643 by uni0643.init;
  sub uni0644 by uni0644.init;
  sub uni0645 by uni0645.init;
  sub uni0646 by uni0646.init;
  sub uni0647 by uni0647.init;
  sub uni0649 by uni0649.init;
  sub uni064A by uni064A.init;
  sub uni066E by uni066E.init;
  sub uni066F by uni066F.init;
  sub uni0678 by uni0678.init;
  sub uni0679 by uni0679.init;
  sub uni067A by uni067A.init;
  sub uni067B by uni067B.init;
  sub uni067C by uni067C.init;
  sub uni067D by uni067D.init;
  sub uni067E by uni067E.init;
  sub uni067F by uni067F.init;
  sub uni0680 by uni0680.init;
  sub uni0681 by uni0681.init;
  sub uni0682 by uni0682.init;
  sub uni0683 by uni0683.init;
  sub uni0684 by uni0684.init;
  sub uni0685 by uni0685.init;
  sub uni0686 by uni0686.init;
  sub uni0687 by uni0687.init;
  sub uni069A by uni069A.init;
  sub uni069B by uni069B.init;
  sub uni069C by uni069C.init;
  sub uni069D by uni069D.init;
  sub uni069E by uni069E.init;
  sub uni069F by uni069F.init;
  sub uni06A0 by uni06A0.init;
  sub uni06A1 by uni06A1.init;
  sub uni06A2 by uni06A2.init;
  sub uni06A3 by uni06A3.init;
  sub uni06A4 by uni06A4.init;
  sub uni06A5 by uni06A5.init;
  sub uni06A6 by uni06A6.init;
  sub uni06A7 by uni06A7.init;
  sub uni06A8 by uni06A8.init;
  sub uni06A9 by uni06A9.init;
  sub uni06AA by uni06AA.init;
  sub uni06AB by uni06AB.init;
  sub uni06AC by uni06AC.init;
  sub uni06AD by uni06AD.init;
  sub uni06AE by uni06AE.init;
  sub uni06AF by uni06AF.init;
  sub uni06B0 by uni06B0.init;
  sub uni06B1 by uni06B1.init;
  sub uni06B2 by uni06B2.init;
  sub uni06B3 by uni06B3.init;
  sub uni06B4 by uni06B4.init;
  sub uni06B5 by uni06B5.init;
  sub uni06B6 by uni06B6.init;
  sub uni06B7 by uni06B7.init;
  sub uni06B8 by uni06B8.init;
  sub uni06B9 by uni06B9.init;
  sub uni06BA by uni06BA.init;
  sub uni06BB by uni06BB.init;
  sub uni06BC by uni06BC.init;
  sub uni06BD by uni06BD.init;
  sub uni06BE by uni06BE.init;
  sub uni06BF by uni06BF.init;
  sub uni06C1 by uni06C1.init;
  sub uni06CC by uni06CC.init;
  sub uni06CE by uni06CE.init;
  sub uni06D0 by uni06D0.init;
  sub uni06D1 by uni06D1.init;
  sub uni06FA by uni06FA.init;
  sub uni06FB by uni06FB.init;
  sub uni06FC by uni06FC.init;
  sub uni06FF by uni06FF.init;
  sub uni0750 by uni0750.init;
  sub uni0751 by uni0751.init;
  sub uni0752 by uni0752.init;
  sub uni0753 by uni0753.init;
  sub uni0754 by uni0754.init;
  sub uni0755 by uni0755.init;
  sub uni0756 by uni0756.init;
  sub uni0757 by uni0757.init;
  sub uni0758 by uni0758.init;
  sub uni075C by uni075C.init;
  sub uni075D by uni075D.init;
  sub uni075E by uni075E.init;
  sub uni075F by uni075F.init;
  sub uni0760 by uni0760.init;
  sub uni0761 by uni0761.init;
  sub uni0762 by uni0762.init;
  sub uni0763 by uni0763.init;
  sub uni0764 by uni0764.init;
  sub uni0765 by uni0765.init;
  sub uni0766 by uni0766.init;
  sub uni0767 by uni0767.init;
  sub uni0768 by uni0768.init;
  sub uni0769 by uni0769.init;
  sub uni076A by uni076A.init;
  sub uni076D by uni076D.init;
  sub uni076E by uni076E.init;
  sub uni076F by uni076F.init;
  sub uni0770 by uni0770.init;
  sub uni0772 by uni0772.init;
  sub uni0775 by uni0775.init;
  sub uni0776 by uni0776.init;
  sub uni0777 by uni0777.init;
  sub uni077C by uni077C.init;
  sub uni077D by uni077D.init;
  sub uni077E by uni077E.init;
  sub uni077F by uni077F.init;
  sub uni08A0 by uni08A0.init;
} init;

feature medi {
  lookupflag IgnoreMarks;
  sub uni0620 by uni0620.medi;
  sub uni0626 by uni0626.medi;
  sub uni0628 by uni0628.medi;
  sub uni062A by uni062A.medi;
  sub uni062B by uni062B.medi;
  sub uni062C by uni062C.medi;
  sub uni062D by uni062D.medi;
  sub uni062E by uni062E.medi;
  sub uni0633 by uni0633.medi;
  sub uni0634 by uni0634.medi;
  sub uni0635 by uni0635.medi;
  sub uni0636 by uni0636.medi;
  sub uni0637 by uni0637.medi;
  sub uni0638 by uni0638.medi;
  sub uni0639 by uni0639.medi;
  sub uni063A by uni063A.medi;
  sub uni063B by uni063B.medi;
  sub uni063C by uni063C.medi;
  sub uni063D by uni063D.medi;
  sub uni0641 by uni0641.medi;
  sub uni0642 by uni0642.medi;
  sub uni0643 by uni0643.medi;
  sub uni0644 by uni0644.medi;
  sub uni0645 by uni0645.medi;
  sub uni0646 by uni0646.medi;
  sub uni0647 by uni0647.medi;
  sub uni0649 by uni0649.medi;
  sub uni064A by uni064A.medi;
  sub uni066E by uni066E.medi;
  sub uni066F by uni066F.medi;
  sub uni0678 by uni0678.medi;
  sub uni0679 by uni0679.medi;
  sub uni067A by uni067A.medi;
  sub uni067B by uni067B.medi;
  sub uni067C by uni067C.medi;
  sub uni067D by uni067D.medi;
  sub uni067E by uni067E.medi;
  sub uni067F by uni067F.medi;
  sub uni0680 by uni0680.medi;
  sub uni0681 by uni0681.medi;
  sub uni0682 by uni0682.medi;
  sub uni0683 by uni0683.medi;
  sub uni0684 by uni0684.medi;
  sub uni0685 by uni0685.medi;
  sub uni0686 by uni0686.medi;
  sub uni0687 by uni0687.medi;
  sub uni069A by uni069A.medi;
  sub uni069B by uni069B.medi;
  sub uni069C by uni069C.medi;
  sub uni069D by uni069D.medi;
  sub uni069E by uni069E.medi;
  sub uni069F by uni069F.medi;
  sub uni06A0 by uni06A0.medi;
  sub uni06A1 by uni06A1.medi;
  sub uni06A2 by uni06A2.medi;
  sub uni06A3 by uni06A3.medi;
  sub uni06A4 by uni06A4.medi;
  sub uni06A5 by uni06A5.medi;
  sub uni06A6 by uni06A6.medi;
  sub uni06A7 by uni06A7.medi;
  sub uni06A8 by uni06A8.medi;
  sub uni06A9 by uni06A9.medi;
  sub uni06AA by uni06AA.medi;
  sub uni06AB by uni06AB.medi;
  sub uni06AC by uni06AC.medi;
  sub uni06AD by uni06AD.medi;
  sub uni06AE by uni06AE.medi;
  sub uni06AF by uni06AF.medi;
  sub uni06B0 by uni06B0.medi;
  sub uni06B1 by uni06B1.medi;
  sub uni06B2 by uni06B2.medi;
  sub uni06B3 by uni06B3.medi;
  sub uni06B4 by uni06B4.medi;
  sub uni06B5 by uni06B5.medi;
  sub uni06B6 by uni06B6.medi;
  sub uni06B7 by uni06B7.medi;
  sub uni06B8 by uni06B8.medi;
  sub uni06B9 by uni06B9.medi;
  sub uni06BA by uni06BA.medi;
  sub uni06BB by uni06BB.medi;
  sub uni06BC by uni06BC.medi;
  sub uni06BD by uni06BD.medi;
  sub uni06BE by uni06BE.medi;
  sub uni06BF by uni06BF.medi;
  sub uni06C1 by uni06C1.medi;
  sub uni06CC by uni06CC.medi;
  sub uni06CE by uni06CE.medi;
  sub uni06D0 by uni06D0.medi;
  sub uni06D1 by uni06D1.medi;
  sub uni06FA by uni06FA.medi;
  sub uni06FB by uni06FB.medi;
  sub uni06FC by uni06FC.medi;
  sub uni06FF by uni06FF.medi;
  sub uni0750 by uni0750.medi;
  sub uni0751 by uni0751.medi;
  sub uni0752 by uni0752.medi;
  sub uni0753 by uni0753.medi;
  sub uni0754 by uni0754.medi;
  sub uni0755 by uni0755.medi;
  sub uni0756 by uni0756.medi;
  sub uni0757 by uni0757.medi;
  sub uni0758 by uni0758.medi;
  sub uni075C by uni075C.medi;
  sub uni075D by uni075D.medi;
  sub uni075E by uni075E.medi;
  sub uni075F by uni075F.medi;
  sub uni0760 by uni0760.medi;
  sub uni0761 by uni0761.medi;
  sub uni0762 by uni0762.medi;
  sub uni0763 by uni0763.medi;
  sub uni0764 by uni0764.medi;
  sub uni0765 by uni0765.medi;
  sub uni0766 by uni0766.medi;
  sub uni0767 by uni0767.medi;
  sub uni0768 by uni0768.medi;
  sub uni0769 by uni0769.medi;
  sub uni076A by uni076A.medi;
  sub uni076D by uni076D.medi;
  sub uni076E by uni076E.medi;
  sub uni076F by uni076F.medi;
  sub uni0770 by uni0770.medi;
  sub uni0772 by uni0772.medi;
  sub uni0775 by uni0775.medi;
  sub uni0776 by uni0776.medi;
  sub uni0777 by uni0777.medi;
  sub uni077C by uni077C.medi;
  sub uni077D by uni077D.medi;
  sub uni077E by uni077E.medi;
  sub uni077F by uni077F.medi;
  sub uni08A0 by uni08A0.medi;
} medi;

feature fina {
  lookupflag IgnoreMarks;
  sub uni0620 by uni0620.fina;

  sub uni0622 by uni0622.fina;
  sub uni0623 by uni0623.fina;
  sub uni0624 by uni0624.fina;
  sub uni0625 by uni0625.fina;
  sub uni0626 by uni0626.fina;
  sub uni0627 by uni0627.fina;
  sub uni0628 by uni0628.fina;
  sub uni0629 by uni0629.fina;
  sub uni062A by uni062A.fina;
  sub uni062B by uni062B.fina;
  sub uni062C by uni062C.fina;
  sub uni062D by uni062D.fina;
  sub uni062E by uni062E.fina;
  sub uni062F by uni062F.fina;
  sub uni0630 by uni0630.fina;
  sub uni0631 by uni0631.fina;
  sub uni0632 by uni0632.fina;
  sub uni0633 by uni0633.fina;
  sub uni0634 by uni0634.fina;
  sub uni0635 by uni0635.fina;
  sub uni0636 by uni0636.fina;
  sub uni0637 by uni0637.fina;
  sub uni0638 by uni0638.fina;
  sub uni0639 by uni0639.fina;
  sub uni063A by uni063A.fina;
  sub uni063B by uni063B.fina;
  sub uni063C by uni063C.fina;
  sub uni063D by uni063D.fina;
  sub uni063E by uni063E.fina;
  sub uni063F by uni063F.fina;
  sub uni0641 by uni0641.fina;
  sub uni0642 by uni0642.fina;
  sub uni0643 by uni0643.fina;
  sub uni0644 by uni0644.fina;
  sub uni0645 by uni0645.fina;
  sub uni0646 by uni0646.fina;
  sub uni0647 by uni0647.fina;
  sub uni0648 by uni0648.fina;
  sub uni0649 by uni0649.fina;
  sub uni064A by uni064A.fina;
  sub uni066E by uni066E.fina;
  sub uni066F by uni066F.fina;
  sub uni0671 by uni0671.fina;
  sub uni0672 by uni0672.fina;
  sub uni0673 by uni0673.fina;
  sub uni0675 by uni0675.fina;
  sub uni0676 by uni0676.fina;
  sub uni0677 by uni0677.fina;
  sub uni0678 by uni0678.fina;
  sub uni0679 by uni0679.fina;
  sub uni067A by uni067A.fina;
  sub uni067B by uni067B.fina;
  sub uni067C by uni067C.fina;
  sub uni067D by uni067D.fina;
  sub uni067E by uni067E.fina;
  sub uni067F by uni067F.fina;
  sub uni0680 by uni0680.fina;
  sub uni0681 by uni0681.fina;
  sub uni0682 by uni0682.fina;
  sub uni0683 by uni0683.fina;
  sub uni0684 by uni0684.fina;
  sub uni0685 by uni0685.fina;
  sub uni0686 by uni0686.fina;
  sub uni0687 by uni0687.fina;
  sub uni0688 by uni0688.fina;
  sub uni0689 by uni0689.fina;
  sub uni068A by uni068A.fina;
  sub uni068B by uni068B.fina;
  sub uni068C by uni068C.fina;
  sub uni068D by uni068D.fina;
  sub uni068E by uni068E.fina;
  sub uni068F by uni068F.fina;
  sub uni0690 by uni0690.fina;
  sub uni0691 by uni0691.fina;
  sub uni0692 by uni0692.fina;
  sub uni0693 by uni0693.fina;
  sub uni0694 by uni0694.fina;
  sub uni0695 by uni0695.fina;
  sub uni0696 by uni0696.fina;
  sub uni0697 by uni0697.fina;
  sub uni0698 by uni0698.fina;
  sub uni0699 by uni0699.fina;
  sub uni069A by uni069A.fina;
  sub uni069B by uni069B.fina;
  sub uni069C by uni069C.fina;
  sub uni069D by uni069D.fina;
  sub uni069E by uni069E.fina;
  sub uni069F by uni069F.fina;
  sub uni06A0 by uni06A0.fina;
  sub uni06A1 by uni06A1.fina;
  sub uni06A2 by uni06A2.fina;
  sub uni06A3 by uni06A3.fina;
  sub uni06A4 by uni06A4.fina;
  sub uni06A5 by uni06A5.fina;
  sub uni06A6 by uni06A6.fina;
  sub uni06A7 by uni06A7.fina;
  sub uni06A8 by uni06A8.fina;
  sub uni06A9 by uni06A9.fina;
  sub uni06AA by uni06AA.fina;
  sub uni06AB by uni06AB.fina;
  sub uni06AC by uni06AC.fina;
  sub uni06AD by uni06AD.fina;
  sub uni06AE by uni06AE.fina;
  sub uni06AF by uni06AF.fina;
  sub uni06B0 by uni06B0.fina;
  sub uni06B1 by uni06B1.fina;
  sub uni06B2 by uni06B2.fina;
  sub uni06B3 by uni06B3.fina;
  sub uni06B4 by uni06B4.fina;
  sub uni06B5 by uni06B5.fina;
  sub uni06B6 by uni06B6.fina;
  sub uni06B7 by uni06B7.fina;
  sub uni06B8 by uni06B8.fina;
  sub uni06B9 by uni06B9.fina;
  sub uni06BA by uni06BA.fina;
  sub uni06BB by uni06BB.fina;
  sub uni06BC by uni06BC.fina;
  sub uni06BD by uni06BD.fina;
  sub uni06BE by uni06BE.fina;
  sub uni06BF by uni06BF.fina;
  sub uni06C0 by uni06D5.fina uni0654;
  sub uni06C1 by uni06C1.fina;
  sub uni06C2 by uni06C1.fina uni0654;
  sub uni06C3 by uni06C3.fina;
  sub uni06C4 by uni06C4.fina;
  sub uni06C5 by uni06C5.fina;
  sub uni06C6 by uni06C6.fina;
  sub uni06C7 by uni06C7.fina;
  sub uni06C8 by uni06C8.fina;
  sub uni06C9 by uni06C9.fina;
  sub uni06CA by uni06CA.fina;
  sub uni06CB by uni06CB.fina;
  sub uni06CC by uni06CC.fina;
  sub uni06CD by uni06CD.fina;
  sub uni06CE by uni06CE.fina;
  sub uni06CF by uni06CF.fina;
  sub uni06D0 by uni06D0.fina;
  sub uni06D1 by uni06D1.fina;
  sub uni06D2 by uni06D2.fina;
    
  sub uni06D3 by uni0626.medi uni06D2.fina;
  sub uni06D5 by uni06D5.fina;
  sub uni06EE by uni06EE.fina;
  sub uni06EF by uni06EF.fina;
  sub uni06FA by uni06FA.fina;
  sub uni06FB by uni06FB.fina;
  sub uni06FC by uni06FC.fina;
  sub uni06FF by uni06FF.fina;
  sub uni0750 by uni0750.fina;
  sub uni0751 by uni0751.fina;
  sub uni0752 by uni0752.fina;
  sub uni0753 by uni0753.fina;
  sub uni0754 by uni0754.fina;
  sub uni0755 by uni0755.fina;
  sub uni0756 by uni0756.fina;
  sub uni0757 by uni0757.fina;
  sub uni0758 by uni0758.fina;
  sub uni0759 by uni0759.fina;
  sub uni075A by uni075A.fina;
  sub uni075B by uni075B.fina;
  sub uni075C by uni075C.fina;
  sub uni075D by uni075D.fina;
  sub uni075E by uni075E.fina;
  sub uni075F by uni075F.fina;
  sub uni0760 by uni0760.fina;
  sub uni0761 by uni0761.fina;
  sub uni0762 by uni0762.fina;
  sub uni0763 by uni0763.fina;
  sub uni0764 by uni0764.fina;
  sub uni0765 by uni0765.fina;
  sub uni0766 by uni0766.fina;
  sub uni0767 by uni0767.fina;
  sub uni0768 by uni0768.fina;
  sub uni0769 by uni0769.fina;
  sub uni076A by uni076A.fina;
  sub uni076B by uni076B.fina;
  sub uni076C by uni076C.fina;
  sub uni076D by uni076D.fina;
  sub uni076E by uni076E.fina;
  sub uni076F by uni076F.fina;
  sub uni0770 by uni0770.fina;
  sub uni0771 by uni0771.fina;
  sub uni0772 by uni0772.fina;
  sub uni0773 by uni0773.fina;
  sub uni0774 by uni0774.fina;
  sub uni0775 by uni0775.fina;
  sub uni0776 by uni0776.fina;
  sub uni0777 by uni0777.fina;
  sub uni0778 by uni0778.fina;
  sub uni0779 by uni0779.fina;
  sub uni077A by uni077A.fina;
  sub uni077B by uni077B.fina;
  sub uni077C by uni077C.fina;
  sub uni077D by uni077D.fina;
  sub uni077E by uni077E.fina;
  sub uni077F by uni077F.fina;
  sub uni08A0 by uni08A0.fina;
} fina;


feature rlig {
  lookupflag IgnoreMarks;
  sub uni0640 uni0640 uni0640 uni0640 by uni0640.4;
  sub uni0640 uni0640 uni0640         by uni0640.3;
  sub uni0640 uni0640                 by uni0640.2;
} rlig;

feature rlig {
  lookupflag IgnoreMarks;
  sub uni0640                         by uni0640.1;
} rlig;

feature rlig {
  lookupflag IgnoreMarks;
  sub uni0640.1 uni0627.fina by uni0627.fina_Tatweel;
  sub uni0640.2 uni0627.fina by uni0627.fina_Tatweel;
  sub uni0640.3 uni0627.fina by uni0627.fina_Tatweel;
  sub uni0640.4 uni0627.fina by uni0627.fina_Tatweel;
} rlig;


feature calt {
  sub [uni064E] uni0670' by uni0670.isol;
} calt;



@before = [@aAyn.init @aAyn.medi @aBaa.init @aBaa.medi @aFaa.init @aFaa.medi
           @aHaa.init @aHaa.medi @aHeh.init @aHeh.medi @aKaf.init @aKaf.medi
           @aLam.init @aLam.medi @aMem.init @aMem.medi @aSad.init @aSad.medi
           @aSen.init @aSen.medi @aTaa.init @aTaa.medi];
@after  = [@aAlf.fina @aAyn.fina @aAyn.medi @aBaa.fina @aBaa.medi @aDal.fina
           @aFaa.fina @aFaa.medi @aHaa.fina @aHaa.medi @aHeh.fina @aHeh.medi
           @aKaf.fina @aKaf.medi @aLam.fina @aLam.medi @aMem.fina @aMem.medi
           @aNon.fina @aQaf.fina @aRaa.fina @aSad.fina @aSad.medi @aSen.fina
           @aSen.medi @aTaa.fina @aTaa.medi @aWaw.fina @aYaa.fina uni0640];

lookup MedialSmallAlef {
  sub uni0670.isol by uni0670.medi;
} MedialSmallAlef;

feature calt {
  lookupflag IgnoreMarks;
  sub @before'
      uni0670.isol' lookup MedialSmallAlef
      @after;
} calt;

@before.isol = [uni0626 uni0628 uni062A uni062B uni062C uni062D uni062E uni0633
                uni0634 uni0635 uni0636 uni0637 uni0638 uni0639 uni063A uni0641
                uni0642 uni0643 uni0644 uni0645 uni0646 uni0647 uni0649 uni064A
                uni06CC];
@before.init = [uni0626.init uni0628.init uni062A.init uni062B.init uni062C.init
                uni062D.init uni062E.init uni0633.init uni0634.init uni0635.init
                uni0636.init uni0637.init uni0638.init uni0639.init uni063A.init
                uni0641.init uni0642.init uni0643.init uni0644.init uni0645.init
                uni0646.init uni0647.init uni0649.init uni064A.init uni06CC.init];
@before.medi = [uni0626.medi uni0628.medi uni062A.medi uni062B.medi uni062C.medi
                uni062D.medi uni062E.medi uni0633.medi uni0634.medi uni0635.medi
                uni0636.medi uni0637.medi uni0638.medi uni0639.medi uni063A.medi
                uni0641.medi uni0642.medi uni0643.medi uni0644.medi uni0645.medi
                uni0646.medi uni0647.medi uni0649.medi uni064A.medi uni06CC.medi];
@before.fina = [uni0626.fina uni0628.fina uni062A.fina uni062B.fina uni062C.fina
                uni062D.fina uni062E.fina uni0633.fina uni0634.fina uni0635.fina
                uni0636.fina uni0637.fina uni0638.fina uni0639.fina uni063A.fina
                uni0641.fina uni0642.fina uni0643.fina uni0644.fina uni0645.fina
                uni0646.fina uni0647.fina uni0649.fina uni064A.fina uni06CC.fina];

@after.isol =  [uni0622 uni0623 uni0624 uni0625 uni0627 uni0629 uni062F uni0630
                uni0631 uni0632 uni0648];
@after.fina =  [uni0622.fina uni0623.fina uni0624.fina uni0625.fina uni0627.fina
                uni0629.fina uni062F.fina uni0630.fina uni0631.fina uni0632.fina
                uni0648.fina];

@before = [@before.isol @before.fina];
@after  = [@after.isol @before.isol @before.init];

@floats.isol = [uni0621 uni06E5 uni06E6 uni0670.isol];
@floats.medi = [uni0621.medi uni06E5.medi uni06E6.medi uni0670.medi];

lookup Before {
  sub @before.isol by @before.init;
  sub @before.fina by @before.medi;
} Before;

lookup After {
  sub @before.isol by @before.fina;
  sub @after.isol  by @after.fina;
  sub @before.init by @before.medi;
} After;

lookup MedialFloats {
  sub @floats.isol by @floats.medi;
} MedialFloats;

lookup FloatHamza {
  sub uni0621 by uni0621.float;
} FloatHamza;

lookup LamAlefIsol {
  sub uni0644 by uni0644.init_LamAlfIsol;
  sub uni0627 by uni0627.fina_LamAlfIsol;
} LamAlefIsol;

lookup LamAlefFina {
  sub uni0644.fina by uni0644.medi_LamAlfFina;
  sub uni0627      by uni0627.fina_LamAlfFina;
} LamAlefFina;

feature calt {
  lookupflag IgnoreMarks;
        sub uni0644'      lookup LamAlefIsol
      uni0621'      lookup FloatHamza
      uni0627'      lookup LamAlefIsol;

  sub uni0644.fina' lookup LamAlefFina
      uni0621'      lookup FloatHamza
      uni0627'      lookup LamAlefFina;

    sub @before'      lookup Before
      @floats.isol' lookup MedialFloats
      @after'       lookup After;

    sub @before'      lookup Before
      @floats.isol' lookup MedialFloats
      @floats.isol' lookup MedialFloats
      @after'       lookup After;
} calt;

feature kern {
  lookupflag IgnoreMarks;
  pos uni0621.float' <-300 0 0 0> @aAlf.fina_LamAlfFina;
} kern;

feature calt {
  sub uni0640.1 uni0670 by uni0670.medi;
} calt;


feature rlig {
  sub uni064E uni064E by uni08F0;   sub uni064F uni064F by uni08F1;   sub uni0650 uni0650 by uni08F2; } rlig;


feature calt {
  lookupflag IgnoreMarks;
  sub [@aHeh.isol @aHeh.fina] [uni06E5]' by [uni06E5.low];
} calt;

feature mark {
    pos [uni06E5.low]' <50 0 250 0> [uni06E4];

    pos [uni0627.fina uni0627.fina_Wide uni0627.fina_KafMemAlf uni0627.fina_MemAlfFina]' <250 0 250 0> [uni06E4];
  pos [uni0627.fina uni0627.fina_Wide uni0627.fina_KafMemAlf uni0627.fina_MemAlfFina]' <350 0 350 0> [uni0653];
  pos [uni0627]' <150 0 300 0> [uni06E4];
} mark;


@EndOfAyah = [uni06DD];
@NumSign   = [uni0600];
@YearSign  = [uni0601];
@FootNote  = [uni0602];
@SafhaSign = [uni0603];

@NumSignW  = [uni0600.alt];

@Digits.all = [@Digits];

lookup digit2small {
  sub @Digits by @Digits.small;
} digit2small;

lookup NumSignW {
  sub @NumSign by @NumSignW;
} NumSignW;

feature calt {
  sub [@EndOfAyah @NumSign @SafhaSign @FootNote]                                           @Digits.all' lookup digit2small;
  sub [@EndOfAyah @NumSign @SafhaSign @FootNote] @Digits.small                             @Digits.all' lookup digit2small;
  sub [@EndOfAyah @NumSign @SafhaSign          ] @Digits.small @Digits.small               @Digits.all' lookup digit2small;
  sub @NumSign                                   @Digits.small @Digits.small @Digits.small @Digits.all' lookup digit2small;
} calt;

feature calt {
  sub @NumSign' lookup NumSignW @Digits.small @Digits.small @Digits.small @Digits.small;
} calt;

lookup digit2medium {
  sub @Digits by @Digits.medium;
} digit2medium;

feature calt {
  sub @YearSign                                              @Digits.all' lookup digit2medium;
  sub @YearSign @Digits.medium                               @Digits.all' lookup digit2medium;
  sub @YearSign @Digits.medium @Digits.medium                @Digits.all' lookup digit2medium;
  sub @YearSign @Digits.medium @Digits.medium @Digits.medium @Digits.all' lookup digit2medium;
} calt;


feature kern {
  pos @EndOfAyah @Digits.small'  <-1610 0 -600 0>;
  pos @NumSign   @Digits.small'  <-1622 0 -600 0>;
  pos @NumSignW  @Digits.small'  <-1922 0 -600 0>;
  pos @YearSign  @Digits.medium' <-2275 0 -900 0>;
  pos @FootNote  @Digits.small'  <-1090 0 -600 0>;
  pos @SafhaSign @Digits.small'  <-1722 0 -600 0>;
} kern;

feature kern {
  pos @EndOfAyah @Digits.small'  <-300 0 0 0> @Digits.small;
  pos @NumSign   @Digits.small'  <-300 0 0 0> @Digits.small;
  pos @NumSignW  @Digits.small'  <-300 0 0 0> @Digits.small;
  pos @YearSign  @Digits.medium' <-450 0 0 0> @Digits.medium;
  pos @FootNote  @Digits.small'  <-300 0 0 0> @Digits.small;
  pos @SafhaSign @Digits.small'  <-300 0 0 0> @Digits.small;
} kern;

feature kern {
  pos @EndOfAyah @Digits.small  @Digits.small'  <-1310 0 -600 0>;
  pos @NumSign   @Digits.small  @Digits.small'  <-1322 0 -600 0>;
  pos @NumSignW  @Digits.small  @Digits.small'  <-1622 0 -600 0>;
  pos @YearSign  @Digits.medium @Digits.medium' <-1825 0 -900 0>;
  pos @FootNote  @Digits.small  @Digits.small'  <-790  0 -600 0>;
  pos @SafhaSign @Digits.small  @Digits.small'  <-1422 0 -600 0>;
} kern;

feature kern {
  pos @EndOfAyah @Digits.small'  <-300 0 0 0> @Digits.small  @Digits.small;
  pos @NumSign   @Digits.small'  <-300 0 0 0> @Digits.small  @Digits.small;
  pos @NumSignW  @Digits.small'  <-300 0 0 0> @Digits.small  @Digits.small;
  pos @YearSign  @Digits.medium' <-450 0 0 0> @Digits.medium @Digits.medium;
  pos @SafhaSign @Digits.small'  <-300 0 0 0> @Digits.small  @Digits.small;
} kern;

feature kern {
  pos @EndOfAyah @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small;
  pos @NumSign   @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small;
  pos @NumSignW  @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small;
  pos @YearSign  @Digits.medium @Digits.medium' <-450 0 0 0> @Digits.medium;
  pos @SafhaSign @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small;
} kern;

feature kern {
  pos @EndOfAyah @Digits.small  @Digits.small  @Digits.small'  <-1010 0 -600 0>;
  pos @NumSign   @Digits.small  @Digits.small  @Digits.small'  <-1022 0 -600 0>;
  pos @NumSignW  @Digits.small  @Digits.small  @Digits.small'  <-1322 0 -600 0>;
  pos @YearSign  @Digits.medium @Digits.medium @Digits.medium' <-1375 0 -900 0>;
  pos @SafhaSign @Digits.small  @Digits.small  @Digits.small'  <-1122 0 -600 0>;
} kern;

feature kern {
  pos @YearSign @Digits.medium' <-450 0 0 0> @Digits.medium @Digits.medium @Digits.medium;
  pos @NumSignW @Digits.small'  <-300 0 0 0> @Digits.small  @Digits.small  @Digits.small ;
} kern;

feature kern {
  pos @YearSign @Digits.medium @Digits.medium' <-450 0 0 0> @Digits.medium @Digits.medium;
  pos @NumSignW @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small  @Digits.small ;
} kern;

feature kern {
  pos @YearSign @Digits.medium @Digits.medium @Digits.medium' <-450 0 0 0> @Digits.medium;
  pos @NumSignW @Digits.small  @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small ;
} kern;

feature kern {
  pos @YearSign @Digits.medium @Digits.medium @Digits.medium @Digits.medium' <-925  0 -900 0>;
  pos @NumSignW @Digits.small  @Digits.small  @Digits.small  @Digits.small'  <-1022 0 -600 0>;
} kern;


feature calt {
  sub @Digits.all' lookup digit2small                             [@EndOfAyah @NumSign @SafhaSign @FootNote];
} calt;

feature calt {
  sub @Digits.all' lookup digit2small @Digits.small               [@EndOfAyah @NumSign @SafhaSign @FootNote];
} calt;

feature calt {
  sub @Digits.all' lookup digit2small @Digits.small @Digits.small [@EndOfAyah @NumSign @SafhaSign          ];
} calt;

feature calt {
  sub @Digits.all' lookup digit2small @Digits.small @Digits.small @Digits.small @NumSign;
} calt;

feature calt {
  sub @Digits.small @Digits.small @Digits.small @Digits.small @NumSign' lookup NumSignW;
} calt;

feature calt {
  sub @Digits.all' lookup digit2medium                                              @YearSign;
} calt;

feature calt {
  sub @Digits.all' lookup digit2medium @Digits.medium                               @YearSign;
} calt;

feature calt {
  sub @Digits.all' lookup digit2medium @Digits.medium @Digits.medium                @YearSign;
} calt;

feature calt {
  sub @Digits.all' lookup digit2medium @Digits.medium @Digits.medium @Digits.medium @YearSign;
} calt;

feature kern {
  pos @Digits.small'  <-1610 0 -600 0> @EndOfAyah;
  pos @Digits.small'  <-1622 0 -600 0> @NumSign  ;
  pos @Digits.small'  <-1922 0 -600 0> @NumSignW ;
  pos @Digits.medium' <-2275 0 -900 0> @YearSign ;
  pos @Digits.small'  <-1090 0 -600 0> @FootNote ;
  pos @Digits.small'  <-1722 0 -600 0> @SafhaSign;
} kern;

feature kern {
  pos @Digits.small  @Digits.small'  <-300 0 0 0> @EndOfAyah;
  pos @Digits.small  @Digits.small'  <-300 0 0 0> @NumSign  ;
  pos @Digits.small  @Digits.small'  <-300 0 0 0> @NumSignW ;
  pos @Digits.medium @Digits.medium' <-450 0 0 0> @YearSign ;
  pos @Digits.small  @Digits.small'  <-300 0 0 0> @FootNote ;
  pos @Digits.small  @Digits.small'  <-300 0 0 0> @SafhaSign;
} kern;

feature kern {
  pos @Digits.small'  <-1310 0 -600 0> @Digits.small  @EndOfAyah;
  pos @Digits.small'  <-1322 0 -600 0> @Digits.small  @NumSign  ;
  pos @Digits.small'  <-1622 0 -600 0> @Digits.small  @NumSignW ;
  pos @Digits.medium' <-1825 0 -900 0> @Digits.medium @YearSign ;
  pos @Digits.small'  <-790  0 -600 0> @Digits.small  @FootNote ;
  pos @Digits.small'  <-1422 0 -600 0> @Digits.small  @SafhaSign;
} kern;

feature kern {
  pos @Digits.small  @Digits.small  @Digits.small'  <-300 0 0 0> @EndOfAyah;
  pos @Digits.small  @Digits.small  @Digits.small'  <-300 0 0 0> @NumSign  ;
  pos @Digits.small  @Digits.small  @Digits.small'  <-300 0 0 0> @NumSignW ;
  pos @Digits.medium @Digits.medium @Digits.medium' <-450 0 0 0> @YearSign ;
  pos @Digits.small  @Digits.small  @Digits.small'  <-300 0 0 0> @SafhaSign;
} kern;

feature kern {
  pos @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small  @EndOfAyah;
  pos @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small  @NumSign  ;
  pos @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small  @NumSignW ;
  pos @Digits.medium @Digits.medium' <-450 0 0 0> @Digits.medium @YearSign ;
  pos @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small  @SafhaSign;
} kern;

feature kern {
  pos @Digits.small'  <-1010 0 -600 0> @Digits.small  @Digits.small  @EndOfAyah;
  pos @Digits.small'  <-1022 0 -600 0> @Digits.small  @Digits.small  @NumSign  ;
  pos @Digits.small'  <-1322 0 -600 0> @Digits.small  @Digits.small  @NumSignW ;
  pos @Digits.medium' <-1375 0 -900 0> @Digits.medium @Digits.medium @YearSign ;
  pos @Digits.small'  <-1122 0 -600 0> @Digits.small  @Digits.small  @SafhaSign;
} kern;

feature kern {
  pos @Digits.medium @Digits.medium @Digits.medium @Digits.medium' <-450 0 0 0> @YearSign;
  pos @Digits.small  @Digits.small  @Digits.small  @Digits.small'  <-300 0 0 0> @NumSignW;
} kern;

feature kern {
  pos @Digits.medium @Digits.medium @Digits.medium' <-450 0 0 0> @Digits.medium @YearSign;
  pos @Digits.small  @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small  @NumSignW;
} kern;

feature kern {
  pos @Digits.medium @Digits.medium' <-450 0 0 0> @Digits.medium @Digits.medium @YearSign;
  pos @Digits.small  @Digits.small'  <-300 0 0 0> @Digits.small  @Digits.small  @NumSignW;
} kern;

feature kern {
  pos @Digits.medium' <-925  0 -900 0> @Digits.medium @Digits.medium @Digits.medium @YearSign;
  pos @Digits.small'  <-1022 0 -600 0> @Digits.small  @Digits.small  @Digits.small  @NumSignW;
} kern;

feature rtlm {
  sub radical by radical.rtlm;
} rtlm;

lookup Lellah {
  sub uni0644.init by uni0644.init_Lellah;
  sub uni0644.medi by uni0644.medi_Lellah;
  sub uni0647.fina by uni0647.fina_Lellah;
  sub uni06C1.fina by uni0647.fina_Lellah;
} Lellah;

lookup Lellah2 {
  sub uni0644.medi by uni0644.medi_Lellah uni0651 uni0670;
} Lellah2;

lookup Lellah3 {
  sub uni0644.medi by uni0644.medi_FaLellah;
} Lellah3;

lookup Lellah4 {
  sub uni0644.init by uni0644.init_Lellah;
} Lellah4;

lookup Lellah5 {
  sub uni0644.medi by uni0644.medi_Lellah;
} Lellah5;

@Li = [uni0644.init];
@Lm = [uni0644.medi];
@Ai = [uni0627 uni0622 uni0671];
@Af = [uni0627.fina];
@Hf = [uni0647.fina uni06C1.fina];
@xF = [uni064E uni0670];

lookup LamLamInitX {
  sub uni0644.init by uni0644.init_LamLamInit;
  sub uni0644.medi by uni0644.medi_LamLamInit;
} LamLamInitX;

@aAlf_isol = [uni0625 uni0774 uni0773 uni0623 uni0675 uni0672 uni0673];
@aAlf_fina = [uni0625.fina uni0774.fina uni0773.fina uni0623.fina uni0622.fina
              uni0675.fina uni0672.fina uni0673.fina];
@aWaw_isol = [uni06CB uni0624 uni06CA uni06CF uni0778 uni06C6 uni06C7 uni06C4
              uni06C5 uni0676 uni0677 uni06C8 uni06C9 uni0779];
@aBaa_init = [uni0777.init uni0680.init uni0776.init uni06BC.init uni0750.init
              uni0756.init uni0768.init uni06CE.init uni0775.init uni06BD.init
              uni0626.init uni066E.init uni0620.init uni064A.init uni06BB.init
              uni067F.init uni0755.init uni067D.init uni067E.init uni067B.init
              uni067A.init uni0751.init uni0646.init uni0753.init uni0752.init
              uni0678.init uni063D.init uni062B.init uni0679.init uni06B9.init
              uni0769.init uni0649.init uni067C.init uni0754.init uni06D1.init
              uni06D0.init uni06BA.init uni06CC.init uni0767.init];
@aFaa_init = [uni066F.init uni0761.init uni0760.init uni0642.init uni06A8.init
              uni06A1.init uni06A2.init uni06A3.init uni06A4.init uni06A5.init
              uni06A6.init uni06A7.init];
@aHeh_init = [uni06C1.init];
@aKaf_init = [uni063B.init uni063C.init uni077F.init uni0764.init uni06AA.init
              uni06B0.init uni06B3.init uni06B2.init uni06AB.init uni06AC.init
              uni06AD.init uni06AE.init uni06AF.init uni06A9.init uni06B4.init
              uni0763.init uni0762.init uni06B1.init];

@Ignore    = [@aAyn.fina @aAyn.isol @aAlf_isol @aAlf_fina @aBaa.fina @aBaa.isol
              @aDal.fina @aDal.isol @aFaa.fina @aFaa.isol @aHaa.fina @aHaa.isol
              @aHeh.fina @aHeh.isol @aKaf.fina @aKaf.isol @aLam.fina @aLam.isol
              @aMem.fina @aMem.isol @aNon.fina @aNon.isol @aQaf.fina @aQaf.isol
              @aRaa.fina @aRaa.isol @aSad.fina @aSad.isol @aSen.fina @aSen.isol
              @aTaa.fina @aTaa.isol @aWaw.fina @aWaw_isol @aYaa.fina @aYaa.isol];

@Ignore2   = [@aAyn.init @aAyn.medi @aBaa_init @aBaa.medi @aFaa_init @aFaa.medi
              @aHaa.init @aHaa.medi @aHeh_init @aHeh.medi @aKaf_init @aKaf.medi
              @aLam.init @aLam.medi @aMem.init @aMem.medi @aSad.init @aSad.medi
              @aSen.init @aSen.medi @aTaa.init @aTaa.medi];

feature calt {
  lookupflag IgnoreMarks;
  sub @Ignore       @Li' lookup LamLamInitX @Lm' lookup LamLamInitX @Hf;
  sub @Ignore  @Ai' @Li' lookup LamLamInitX @Lm' lookup LamLamInitX @Hf;
  sub @Ignore2 @Af' @Li' lookup LamLamInitX @Lm' lookup LamLamInitX @Hf;
} calt;

feature calt {
    sub @Li' lookup Lellah4 @Lm' lookup Lellah2              @Hf' lookup Lellah;
  sub @Li' lookup Lellah  @Lm' lookup Lellah uni0651'      @Hf' lookup Lellah;
  sub @Li' lookup Lellah  @Lm' lookup Lellah uni0651' @xF' @Hf' lookup Lellah;

    sub @Li' lookup Lellah4 uni0650' @Lm' lookup Lellah2              @Hf' lookup Lellah;
  sub @Li' lookup Lellah  uni0650' @Lm' lookup Lellah uni0651'      @Hf' lookup Lellah;
  sub @Li' lookup Lellah  uni0650' @Lm' lookup Lellah uni0651' @xF' @Hf' lookup Lellah;

    sub @Li' lookup Lellah4 uni0651'uni0650' @Lm' lookup Lellah2              @Hf' lookup Lellah;
  sub @Li' lookup Lellah  uni0651'uni0650' @Lm' lookup Lellah uni0651'      @Hf' lookup Lellah;
  sub @Li' lookup Lellah  uni0651'uni0650' @Lm' lookup Lellah uni0651' @xF' @Hf' lookup Lellah;

    sub uni0641.init'          @Lm' lookup Lellah3          @Lm' lookup Lellah2               @Hf' lookup Lellah;
  sub uni0641.init'          @Lm' lookup Lellah3          @Lm' lookup Lellah5 uni0651'      @Hf' lookup Lellah;
  sub uni0641.init'          @Lm' lookup Lellah3          @Lm' lookup Lellah5 uni0651' @xF' @Hf' lookup Lellah;
  sub uni0641.init' uni064E' @Lm' lookup Lellah3          @Lm' lookup Lellah2               @Hf' lookup Lellah;
  sub uni0641.init' uni064E' @Lm' lookup Lellah3          @Lm' lookup Lellah5 uni0651'      @Hf' lookup Lellah;
  sub uni0641.init' uni064E' @Lm' lookup Lellah3          @Lm' lookup Lellah5 uni0651' @xF' @Hf' lookup Lellah;
  sub uni0641.init'          @Lm' lookup Lellah3 uni0650' @Lm' lookup Lellah2               @Hf' lookup Lellah;
  sub uni0641.init'          @Lm' lookup Lellah3 uni0650' @Lm' lookup Lellah5 uni0651'      @Hf' lookup Lellah;
  sub uni0641.init'          @Lm' lookup Lellah3 uni0650' @Lm' lookup Lellah5 uni0651' @xF' @Hf' lookup Lellah;
  sub uni0641.init' uni064E' @Lm' lookup Lellah3 uni0650' @Lm' lookup Lellah2               @Hf' lookup Lellah;
  sub uni0641.init' uni064E' @Lm' lookup Lellah3 uni0650' @Lm' lookup Lellah5 uni0651'      @Hf' lookup Lellah;
  sub uni0641.init' uni064E' @Lm' lookup Lellah3 uni0650' @Lm' lookup Lellah5 uni0651' @xF' @Hf' lookup Lellah;
} calt;

feature calt {
  lookupflag IgnoreMarks;
  sub uni0644.medi_Lellah' @Hf' lookup Lellah;
} calt;

@ShaddaTashkil = [uni064E uni064B uni064C uni064F uni08F0 uni08F1];
@ShaddaTashkil.small = [uni064E.small uni064B.small uni064C.small uni064F.small uni08F0.small uni08F1.small];

lookup SamallTashkil {
  sub @ShaddaTashkil by @ShaddaTashkil.small;
} SamallTashkil;

feature calt {
  sub [uni0651 uni06EC] @ShaddaTashkil' lookup SamallTashkil;
} calt;


feature calt {
  sub @AlefHamzaAbove [uni064E uni064F uni0652]' by [uni064E.small2 uni064F.small uni0652.small2];
  sub @AlefHamzaBelow [uni0650]'                 by [uni0650.small2];
  sub [uni0655]       [uni0650]'                 by [uni0650.small2];
} calt;

feature calt {
  sub [uni0647 uni06D5 uni06C1 uni0647.fina uni06D5.fina uni06C1.fina] [uni0654]' by hamza.above;
} calt;

lookup AboveHaaInit {
  sub @aBaa.init by @aBaa.init_BaaHaaInit;
  sub @aHaa.medi by @aHaa.medi_SadHaaInit;
  sub @aHeh.init by @aHeh.init_HehHaaInit;
  sub @aMem.init by @aMem.init_MemHaaInit;
  sub @aSad.init by @aSad.init_SadHaaInit;
  sub @aSen.init by @aSen.init_SenHaaInit;
} AboveHaaInit;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aBaa.init @aHeh.init @aMem.init @aSad.init @aSen.init]' lookup AboveHaaInit
      [@aHaa.medi]' lookup AboveHaaInit;
} calt;

lookup BaaRaaFina {
  sub @aBaa.medi by @aBaa.medi_BaaRaaFina;
  sub @aRaa.fina by @aRaa.fina_BaaRaaFina;
} BaaRaaFina;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aBaa.medi]' lookup BaaRaaFina
      [@aRaa.fina]' lookup BaaRaaFina;
} calt;

lookup BaaNonFina {
  sub @aBaa.medi by @aBaa.medi_BaaNonFina;
  sub @aNon.fina by @aNon.fina_BaaNonFina;
} BaaNonFina;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aBaa.medi]' lookup BaaNonFina
      [@aNon.fina]' lookup BaaNonFina;
} calt;

lookup BaaMemFina {
  sub @aBaa.medi by @aBaa.medi_BaaMemFina;
  sub @aMem.fina by @aMem.fina_BaaMemFina;
} BaaMemFina;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aBaa.medi]' lookup BaaMemFina
      [@aMem.fina]' lookup BaaMemFina;
} calt;

lookup KafBaaAlfIsol {
  sub @aBaa.medi by @aBaa.medi_KafBaaInit;
  sub @aKaf.init by @aKaf.init_KafBaaInit;
} KafBaaAlfIsol;

lookup KafBaaAlfFina {
  sub @aBaa.medi by @aBaa.medi_KafBaaMedi;
  sub @aKaf.medi by @aKaf.medi_KafBaaMedi;
} KafBaaAlfFina;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aKaf.init]' lookup KafBaaAlfIsol
      [@aBaa.medi]' lookup KafBaaAlfIsol
      [@aAlf.fina @aLam.medi @aLam.fina];

  sub [@aKaf.medi]' lookup KafBaaAlfFina
      [@aBaa.medi]' lookup KafBaaAlfFina
      [@aAlf.fina @aLam.medi @aLam.fina];
} calt;

lookup BaaBaa {
  sub @aBaa.medi by @aBaa.medi_BaaBaaInit;
  sub @aBaa.fina by @aBaa.fina_BaaBaaIsol;
  sub @aBaa.init by @aBaa.init_BaaBaaIsol;
} BaaBaa;

lookup HighBaa {
  sub @aBaa.init by @aBaa.init_High;
  sub @aBaa.medi by @aBaa.medi_High;
} HighBaa;

feature calt {
  lookupflag IgnoreMarks;
    sub [@aSen.init @aSen.medi]'
      [@aBaa.medi]'
      [@aBaa.medi]' lookup HighBaa
      [@aSen.medi @aSen.fina]';

    sub [@aBaa.init]' lookup BaaBaa
      [@aBaa.medi]' lookup BaaBaa
      [@aBaa.medi]' lookup HighBaa
      [@aSen.fina @aSen.medi @aSen.medi_PreYaa];

  sub [@aBaa.medi @aSad.init @aSad.medi @aSen.init @aSen.medi @aBaa.medi_BaaBaaInit]
      [@aBaa.medi]' lookup HighBaa
      [@aBaa.fina @aBaa.medi @aSen.fina @aBaa.medi_BaaHehMedi @aSen.medi @aSen.medi_PreYaa];

  sub [@aBaa.medi]' lookup HighBaa
      [@aSen.fina @aSen.medi @aSen.medi_PreYaa];

  sub [@aBaa.init]' lookup HighBaa
      [@aBaa.medi]' lookup HighBaa
      [@aBaa.medi @aBaa.fina @aSen.medi @aSen.fina];
} calt;

lookup BaaHeh {
  sub @aBaa.init by @aBaa.init_BaaHehInit;
  sub @aMem.init_dots by @aMem.init_MemHehInit;
  sub @aBaa.medi by @aBaa.medi_BaaHehMedi;
  sub @aHeh.medi by @aHeh.medi_BaaHehMedi;
} BaaHeh;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aBaa.init @aBaa.medi @aMem.init_dots]' lookup BaaHeh
      [@aHeh.medi]' lookup BaaHeh;
} calt;

lookup BaaBaaHeh {
  sub @aBaa.init by @aBaa.init_BaaBaaHeh;
} BaaBaaHeh;

feature calt {
  lookupflag IgnoreMarks;
  sub @aBaa.init' lookup BaaBaaHeh
      @aBaa.medi_BaaHehMedi;
} calt;

lookup LamAlfFina {
  sub @aAlf.fina by @aAlf.fina_LamAlfFina;
  sub @aLam.medi by @aLam.medi_LamAlfFina;
} LamAlfFina;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aLam.medi]' lookup LamAlfFina
      [@aAlf.fina]' lookup LamAlfFina;
} calt;

lookup KafLamHeh {
  sub @aLam.medi by @aLam.medi_KafLamHehIsol;
} KafLamHeh;

lookup BaaSenAltInit {
  sub @aBaa.init by @aBaa.init_BaaSenAltInit;
  sub @aRaa.fina by @aRaa.fina_PostTooth;
  sub @aSen.medi by @aSen.medi_BaaSenAltInit;
  sub @aHeh.medi by @aHeh.medi_PostTooth;
  sub @aYaa.fina by @aYaa.fina_PostTooth;
  sub @aMem.fina by @aMem.fina_PostTooth;
} BaaSenAltInit;

lookup LamHaaHaaInit {
  sub @aHaa.medi by @aHaa.medi_1LamHaaHaaInit;
  sub @aLam.init by @aLam.init_LamHaaHaaInit;
} LamHaaHaaInit;

lookup LamHaaHaaInit2 {
  sub @aHaa.medi by @aHaa.medi_2LamHaaHaaInit;
} LamHaaHaaInit2;

lookup KafHeh {
  sub @aKaf.init by @aKaf.init_KafHeh;
  sub @aKaf.medi by @aKaf.medi_KafHeh;
  sub @aHeh.fina by @aHeh.fina_KafHeh;
  sub @aDal.fina by @aDal.fina_KafDal;
} KafHeh;

lookup LamMemFina {
  sub @aLam.medi by @aLam.medi_LamMemFina;
  sub @aMem.fina by @aMem.fina_LamMemFina;
} LamMemFina;

lookup SenMemInit {
  sub @aSen.init by @aSen.init_SenMemInit;
  sub @aSad.init by @aSad.init_SadMemInit;
  sub @aMem.init by @aMem.init_MemMemInit;
  sub @aMem.medi by @aMem.medi_SenMemInit;
} SenMemInit;

lookup AllYaaIsol {
  sub @aKaf.init by @aKaf.init_KafYaaIsol;
  sub @aBaa.init by @aBaa.init_BaaYaaIsol;
  sub @aFaa.init by @aFaa.init_FaaYaaIsol;
  sub @aLam.init by @aLam.init_LamYaaIsol;
  sub @aAyn.init by @aAyn.init_AynYaaIsol;
  sub @aHaa.init by @aHaa.init_HaaYaaIsol;
  sub @aHeh.init by @aHeh.init_HehYaaIsol;
  sub @aMem.init_dots by @aMem.init_MemYaaIsol;
  sub @aYaa.fina by @aYaa.fina_KafYaaIsol;
} AllYaaIsol;

lookup BaaRaaIsol {
  sub @aBaa.init by @aBaa.init_BaaRaaIsol;
  sub @aRaa.fina by @aRaa.fina_BaaRaaIsol;
} BaaRaaIsol;

lookup LamHehIsol {
  sub @aLam.init by @aLam.init_LamHeh;
  sub @aLam.medi by @aLam.medi_LamHeh;
  sub @aLam.medi_LamLamInit by @aLam.medi_LamLamHehIsol;
  sub @aHeh.fina by @aHeh.fina_LamHeh;
  sub @aDal.fina by @aDal.fina_LamDal;
} LamHehIsol;

lookup LamWawFina {
  sub @aLam.medi by @aLam.medi_LamWawFina;
  sub @aWaw.fina by @aWaw.fina_LamWawFina;
} LamWawFina;

lookup FaaYaaFina {
  sub @aFaa.medi by @aFaa.medi_FaaYaaFina;
  sub @aYaa.fina by @aYaa.fina_FaaYaaFina;
} FaaYaaFina;

lookup LamLamHaaInit {
  sub @aHaa.medi by @aHaa.medi_LamLamHaaInit;
  sub @aLam.init by @aLam.init_LamLamHaaInit;
  sub @aLam.medi by @aLam.medi_LamLamHaaInit;
} LamLamHaaInit;

lookup LamBaaMemInit {
  sub @aBaa.medi by @aBaa.medi_LamBaaMemInit;
  sub @aLam.init by @aLam.init_LamBaaMemInit;
  sub @aMem.medi by @aMem.medi_LamBaaMemInit;
} LamBaaMemInit;

lookup KafLamMemMedi {
  sub @aLam.medi by @aLam.medi_KafLamMemMedi;
} KafLamMemMedi;

lookup KafLamMemFina {
  sub @aLam.medi by @aLam.medi_KafLamMemFina;
  sub @aLam.medi_LamMemFina by @aLam.medi_KafLamMemFina;
} KafLamMemFina;

lookup BaaDalIsol {
  sub @aBaa.init by @aBaa.init_BaaDal;
  sub @aDal.fina by @aDal.fina_BaaDal;
} BaaDalIsol;

lookup BaaMemHaaInit {
  sub @aBaa.init by @aBaa.init_BaaMemHaaInit;
  sub @aHaa.medi by @aHaa.medi_BaaMemHaaInit;
  sub @aMem.medi by @aMem.medi_BaaMemHaaInit;
} BaaMemHaaInit;

lookup BaaBaaYaa {
  sub @aBaa.init by @aBaa.init_BaaBaaYaa;
  sub @aBaa.medi by @aBaa.medi_BaaBaaYaa;
  sub @aYaa.fina by @aYaa.fina_BaaBaaYaa;
} BaaBaaYaa;

@LamLamFoo = [@aLam.medi_LamMemMedi @aLam.medi_LamHeh @aLam.medi_LamYaaFina];

lookup LamLamInit {
  sub @aLam.init by @aLam.init_LamLamInit;
  sub @aLam.medi by @aLam.medi_LamLamInit;
  sub @aLam.fina by @aLam.fina_LamLamIsol;
  sub @aKaf.fina by @aKaf.fina_LamKafIsol;
  sub @aLam.medi_LamAlfFina by @aLam.medi_LamLamAlfIsol;
  sub @LamLamFoo by [@aLam.medi_LamLamMemInit @aLam.medi_LamLamHehIsol @aLam.medi_LamLamYaaIsol];
} LamLamInit;

lookup LamLamMedi {
  sub @aLam.medi by @aLam.medi_LamLamMedi2;
  sub @aLam.fina by @aLam.fina_LamLamFina;
  sub @aKaf.fina by @aKaf.fina_LamKafFina;
  sub @aLam.medi_LamAlfFina by @aLam.medi_LamLamAlefFina;
  sub @LamLamFoo by [@aLam.medi_LamLamMemMedi @aLam.medi_LamLamHehFina @aLam.medi_LamLamYaaFina];
} LamLamMedi;

lookup LamLamMedi2 {
  sub @aLam.medi by @aLam.medi_LamLamMedi;
} LamLamMedi2;

lookup LamYaaFina {
  sub @aLam.medi by @aLam.medi_LamYaaFina;
  sub @aYaa.fina by @aYaa.fina_LamYaaFina;
} LamYaaFina;

lookup LamMemHaaInit {
  sub @aHaa.medi by @aHaa.medi_LamMemHaaInit;
  sub @aLam.init by @aLam.init_LamMemHaaInit;
  sub @aMem.medi by @aMem.medi_LamMemHaaInit;
} LamMemHaaInit;

lookup LamMemInit {
  sub @aLam.init by @aLam.init_LamMemInit;
  sub @aMem.medi by @aMem.medi_LamMemInit;
} LamMemInit;

lookup LamAlfIsol {
  sub @aAlf.fina by @aAlf.fina_LamAlfIsol;
  sub @aLam.init by @aLam.init_LamAlfIsol;
} LamAlfIsol;

lookup LamHaaMemInit {
  sub @aHaa.medi by @aHaa.medi_LamHaaMemInit;
  sub @aLam.init by @aLam.init_LamHaaMemInit;
  sub @aMem.medi by @aMem.medi_LamHaaMemInit;
} LamHaaMemInit;

lookup BaaBaaMemInit {
  sub @aBaa.init by @aBaa.init_BaaBaaMemInit;
  sub @aBaa.medi by @aBaa.medi_BaaBaaMemInit;
  sub @aMem.medi by @aMem.medi_BaaBaaMemInit;
} BaaBaaMemInit;

lookup BaaBaaHaaInit {
  sub @aBaa.init by @aBaa.init_BaaBaaHaaInit;
  sub @aBaa.medi by @aBaa.medi_BaaBaaHaaInit;
  sub @aHaa.medi by @aHaa.medi_BaaBaaHaaInit;
} BaaBaaHaaInit;

lookup MemRaaIsol {
  sub @aMem.init by @aMem.init_MemRaaIsol;
  sub @aRaa.fina by @aRaa.fina_MemRaaIsol;
} MemRaaIsol;

lookup HaaHaaInit {
  sub @aHaa.init by @aHaa.init_HaaHaaInit;
  sub @aHaa.medi by @aHaa.medi_HaaHaaInit;
} HaaHaaInit;

lookup KafMemIsol {
  sub @aKaf.init by @aKaf.init_KafMemIsol;
  sub @aLam.init by @aLam.init_LamMemIsol;
  sub @aBaa.init by @aBaa.init_BaaMemIsol;
  sub @aMem.fina by @aMem.fina_KafMemIsol;
} KafMemIsol;

lookup LamQafFina {
  sub @aLam.medi by @aLam.medi_LamQafFina;
  sub @aQaf.fina by @aQaf.fina_LamQafFina;
} LamQafFina;

lookup MemHaaMemInit {
  sub @aHaa.medi by @aHaa.medi_MemHaaMemInit;
  sub @aMem.init by @aMem.init_MemHaaMemInit;
  sub @aMem.init_MemHaaInit by @aMem.init_MemHaaMemInit;
  sub @aHaa.medi_SadHaaInit by @aHaa.medi_MemHaaMemInit;
} MemHaaMemInit;

lookup BaaNonIsol {
  sub @aBaa.init by @aBaa.init_BaaNonIsol;
  sub @aNon.fina by @aNon.fina_BaaNonIsol;
} BaaNonIsol;

lookup KafMemFina {
  sub @aKaf.medi by @aKaf.medi_KafMemFina;
  sub @aMem.fina by @aMem.fina_KafMemFina;
} KafMemFina;

lookup KafLamAlf {
  sub @aLam.medi by @aLam.medi_KafLamAlf;
  sub @aLam.medi_LamAlfFina by @aLam.medi_KafLamAlf;
} KafLamAlf;

lookup BaaSenInit {
  sub @aBaa.init by @aBaa.init_BaaSenInit;
  sub @aSen.fina by @aSen.fina_BaaSen;
  sub @aSen.medi by @aSen.medi_BaaSenInit;
} BaaSenInit;

lookup KafRaaFina {
  sub @aKaf.medi by @aKaf.medi_KafRaaFina;
  sub @aRaa.fina by @aRaa.fina_KafRaaFina;
} KafRaaFina;

lookup LamHehInit {
  sub @aHeh.medi by @aHeh.medi_LamHehInit;
  sub @aLam.init by @aLam.init_LamHehInit;
} LamHehInit;

lookup BaaMemInit {
  sub @aBaa.init by @aBaa.init_BaaMemInit;
  sub @aMem.medi by @aMem.medi_BaaMemInit;
} BaaMemInit;

lookup KafLam {
  sub @aKaf.init by @aKaf.init_KafLam;
  sub @aKaf.medi by @aKaf.medi_KafLam;
  sub @aKaf.fina by @aKaf.fina_KafKafFina;
  sub @aLam.medi by @aLam.medi_KafLam;
  sub @aLam.fina by @aLam.fina_KafLam;
  sub @aAlf.fina by @aAlf.fina_KafAlf;
} KafLam;

lookup KafRaaIsol {
  sub @aKaf.init by @aKaf.init_KafRaaIsol;
  sub @aRaa.fina by @aRaa.fina_KafRaaIsol;
} KafRaaIsol;

lookup AynHaaInit {
  sub @aAyn.init by @aAyn.init_AynHaaInit;
  sub @aHaa.medi by @aHaa.medi_AynHaaInit;
} AynHaaInit;

lookup KafYaaFina {
  sub @aKaf.medi by @aKaf.medi_KafYaaFina;
  sub @aAyn.medi by @aAyn.medi_AynYaaFina;
  sub @aYaa.fina by @aYaa.fina_KafYaaFina;
} KafYaaFina;

lookup LamMemMedi {
  sub @aLam.medi by @aLam.medi_LamMemMedi;
  sub @aMem.medi_dots by @aMem.medi_LamMemMedi;
} LamMemMedi;

lookup SenBaaMemInit {
  sub @aSen.init by @aSen.init_SenBaaMemInit;
  sub @aSad.init by @aSad.init_SenBaaMemInit;
  sub @aBaa.medi by @aBaa.medi_SenBaaMemInit;
  sub @aMem.medi by @aMem.medi_SenBaaMemInit;
} SenBaaMemInit;

lookup HaaRaaIsol {
  sub @aHaa.init by @aHaa.init_HaaRaaIsol;
  sub @aRaa.fina by @aRaa.fina_HaaRaaIsol;
} HaaRaaIsol;

lookup LamRaaIsol {
  sub @aLam.init by @aLam.init_LamRaaIsol;
  sub @aRaa.fina by @aRaa.fina_LamRaaIsol;
} LamRaaIsol;

lookup KafMemAlf {
  sub @aKaf.medi by @aKaf.medi_KafMemAlf;
  sub @aKaf.init by @aKaf.init_KafMemAlf;
  sub @aMem.medi by @aMem.medi_KafMemAlf;
  sub @aAlf.fina by @aAlf.fina_KafMemAlf;
  sub @aLam.fina by @aLam.fina_KafMemLam;
  sub @aLam.medi by @aLam.medi_KafMemLam;
} KafMemAlf;

lookup BaaHaaMemInit {
  sub @aBaa.init by @aBaa.init_BaaHaaMemInit;
  sub @aHaa.medi by @aHaa.medi_BaaHaaMemInit;
  sub @aHaa.medi_SadHaaInit by @aHaa.medi_BaaHaaMemInit;
  sub @aBaa.init_BaaHaaInit by @aBaa.init_BaaHaaMemInit;
} BaaHaaMemInit;

lookup AboveHaaIsol {
  sub @aAyn.init by @aAyn.init_AboveHaa;
  sub @aBaa.init by @aBaa.init_AboveHaa;
  sub @aFaa.init by @aFaa.init_FaaHaaInit;
  sub @aHaa.init by @aHaa.init_AboveHaa;
  sub @aHeh.init by @aHeh.init_AboveHaa;
  sub @aKaf.init by @aKaf.init_AboveHaa;
  sub @aLam.init by @aLam.init_LamHaaInit;
  sub @aMem.init by @aMem.init_AboveHaa;
  sub @aSad.init by @aSad.init_AboveHaa;
  sub @aSen.init by @aSen.init_AboveHaa;
  sub @aHaa.fina by @aHaa.fina_AboveHaaIsol;
} AboveHaaIsol;

lookup AboveHaaIsol2 {
  sub @aHaa.fina by @aHaa.fina_AboveHaaIsol2;
  sub @aHaa.medi by @aHaa.medi_FaaHaaInit;
} AboveHaaIsol2;

lookup SenYaaFina {
  sub @aRaa.fina by @aRaa.fina_PostTooth;
  sub @aSad.init by @aSad.init_PreYaa;
  sub @aSad.medi by @aSad.medi_PreYaa;
  sub @aSen.init by @aSen.init_PreYaa;
  sub @aSen.medi by @aSen.medi_PreYaa;
  sub @aYaa.fina by @aYaa.fina_PostTooth;
} SenYaaFina;

lookup KafMemInit {
  sub @aKaf.init by @aKaf.init_KafMemInit;
  sub @aKaf.medi by @aKaf.medi_KafMemMedi;
  sub @aAyn.init by @aAyn.init_AynMemInit;
  sub @aFaa.init by @aFaa.init_FaaMemInit;
  sub @aHaa.init by @aHaa.init_HaaMemInit;
  sub @aHeh.init by @aHeh.init_HehMemInit;
  sub @aMem.medi by @aMem.medi_KafMemMedi;
} KafMemInit;

lookup LamMemFinaExtended {
  sub @aMem.fina by @aMem.fina_LamMemFinaExtended;
} LamMemFinaExtended;

feature calt {
  sub @aLam.medi' lookup LamMemFina
      @Tashkil.above'
      @aMem.fina' lookup LamMemFinaExtended
      @Tashkil.above';
  sub @aLam.medi' lookup LamMemFina
      @Tashkil.above' @Tashkil.above'
      @aMem.fina' lookup LamMemFinaExtended
      @Tashkil.above';
  sub @aLam.medi' lookup LamMemFina
      @Tashkil.above' @Tashkil.above'
      @aMem.fina' lookup LamMemFinaExtended
      @Tashkil.above' @Tashkil.above';
  sub @aLam.medi' lookup LamMemFina
      @Tashkil.above'
      @aMem.fina' lookup LamMemFinaExtended
      @Tashkil.above' @Tashkil.above';
  sub @aLam.medi' lookup LamMemFina
      @Tashkil.below' @Tashkil.above'
      @aMem.fina' lookup LamMemFinaExtended
      @Tashkil.above';
  sub @aLam.medi' lookup LamMemFina
      @Tashkil.above' @Tashkil.below'
      @aMem.fina' lookup LamMemFinaExtended
      @Tashkil.above';
  sub @aLam.medi' lookup LamMemFina
      @Tashkil.below'
      @aMem.fina' lookup LamMemFinaExtended
      @Tashkil.below';
} calt;

lookup MemExtended {
  sub @aMem.fina by @aMem.fina_KafMemIsolExtended;
} MemExtended;

feature calt {
  sub [@aLam.init @aKaf.init]' lookup KafMemIsol
      @Tashkil.above'
      @aMem.fina' lookup MemExtended
      @Tashkil.above';
  sub [@aLam.init @aKaf.init]' lookup KafMemIsol
      @Tashkil.above' @Tashkil.above'
      @aMem.fina' lookup MemExtended
      @Tashkil.above';
  sub [@aLam.init @aKaf.init]' lookup KafMemIsol
      @Tashkil.above' @Tashkil.above'
      @aMem.fina' lookup MemExtended
      @Tashkil.above' @Tashkil.above';
  sub [@aLam.init @aKaf.init]' lookup KafMemIsol
      @Tashkil.above'
      @aMem.fina' lookup MemExtended
      @Tashkil.above' @Tashkil.above';
  sub [@aLam.init @aKaf.init]' lookup KafMemIsol
      @Tashkil.below' @Tashkil.above'
      @aMem.fina' lookup MemExtended
      @Tashkil.above';
  sub [@aLam.init @aKaf.init]' lookup KafMemIsol
      @Tashkil.above' @Tashkil.below'
      @aMem.fina' lookup MemExtended
      @Tashkil.above';
  sub [@aLam.init @aKaf.init]' lookup KafMemIsol
      @Tashkil.below'
      @aMem.fina' lookup MemExtended
      @Tashkil.below';
} calt;

lookup LamMemInitTatweel {
  sub @aMem.medi by @aMem.medi_LamMemInitTatweel;
} LamMemInitTatweel;

feature calt {
  sub @aLam.init' lookup LamMemInit
      @Tashkil.above'
      @aMem.medi' lookup LamMemInitTatweel
      @Tashkil.above';
  sub @aLam.init' lookup LamMemInit
      @Tashkil.above' @Tashkil.above'
      @aMem.medi' lookup LamMemInitTatweel
      @Tashkil.above';
  sub @aLam.init' lookup LamMemInit
      @Tashkil.above' @Tashkil.above'
      @aMem.medi' lookup LamMemInitTatweel
      @Tashkil.above' @Tashkil.above';
  sub @aLam.init' lookup LamMemInit
      @Tashkil.above'
      @aMem.medi' lookup LamMemInitTatweel
      @Tashkil.above' @Tashkil.above';
  sub @aLam.init' lookup LamMemInit
      @Tashkil.below' @Tashkil.above'
      @aMem.medi' lookup LamMemInitTatweel
      @Tashkil.above';
  sub @aLam.init' lookup LamMemInit
      @Tashkil.above' @Tashkil.below'
      @aMem.medi' lookup LamMemInitTatweel
      @Tashkil.above';
  sub @aLam.init' lookup LamMemInit
      @Tashkil.below'
      @aMem.medi' lookup LamMemInitTatweel
      @Tashkil.below';
} calt;

lookup KafMemFinaExtended {
  sub @aMem.fina by @aMem.fina_KafMemFinaExtended;
} KafMemFinaExtended;

feature calt {
  sub @aKaf.medi' lookup KafMemFina
      @Tashkil.above'
      @aMem.fina' lookup KafMemFinaExtended
      @Tashkil.above';
  sub @aKaf.medi' lookup KafMemFina
      @Tashkil.above' @Tashkil.above'
      @aMem.fina' lookup KafMemFinaExtended
      @Tashkil.above';
  sub @aKaf.medi' lookup KafMemFina
      @Tashkil.above' @Tashkil.above'
      @aMem.fina' lookup KafMemFinaExtended
      @Tashkil.above' @Tashkil.above';
  sub @aKaf.medi' lookup KafMemFina
      @Tashkil.above'
      @aMem.fina' lookup KafMemFinaExtended
      @Tashkil.above' @Tashkil.above';
  sub @aKaf.medi' lookup KafMemFina
      @Tashkil.below' @Tashkil.above'
      @aMem.fina' lookup KafMemFinaExtended
      @Tashkil.above;
  sub @aKaf.medi' lookup KafMemFina
      @Tashkil.above' @Tashkil.below'
      @aMem.fina' lookup KafMemFinaExtended
      @Tashkil.above';
  sub @aKaf.medi' lookup KafMemFina
      @Tashkil.below'
      @aMem.fina' lookup KafMemFinaExtended
      @Tashkil.below';
} calt;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aKaf.init @aKaf.medi]' lookup KafLam
      [@aLam.medi]' lookup KafLamHeh
      [@aHeh.fina @aDal.fina]' lookup LamHehIsol;

  sub [@aKaf.init @aKaf.medi]' lookup KafMemAlf
      [@aMem.medi]' lookup KafMemAlf
      [@aLam.medi @aLam.fina @aAlf.fina]' lookup KafMemAlf;

  sub [@aBaa.init]' lookup BaaSenAltInit
      [@aSen.medi]' lookup BaaSenAltInit
      [@aHeh.medi @aRaa.fina @aYaa.fina @aMem.fina]' lookup BaaSenAltInit;

  sub [@aLam.init]' lookup LamHaaHaaInit
      [@aHaa.medi]' lookup LamHaaHaaInit
      [@aHaa.medi]' lookup LamHaaHaaInit2;

  sub [@aKaf.init @aKaf.medi]' lookup KafHeh
      [@aHeh.fina @aDal.fina]' lookup KafHeh;

  sub [@aLam.medi]' lookup LamMemFina
      [@aMem.fina]' lookup LamMemFina;

  sub [@aSen.init @aSad.init @aMem.init]' lookup SenMemInit
      [@aMem.medi]' lookup SenMemInit;

  sub [@aKaf.init @aBaa.init @aFaa.init @aLam.init @aAyn.init @aHaa.init @aHeh.init @aMem.init_dots]' lookup AllYaaIsol
      [@aYaa.fina]' lookup AllYaaIsol;

  sub [@aBaa.init]' lookup BaaRaaIsol
      [@aRaa.fina]' lookup BaaRaaIsol;

  sub [@aLam.init @aLam.medi @aLam.medi_LamLamInit]' lookup LamHehIsol
      [@aHeh.fina @aDal.fina]' lookup LamHehIsol;

  sub [@aLam.medi]' lookup LamWawFina
      [@aWaw.fina]' lookup LamWawFina;

  sub [@aFaa.medi]' lookup FaaYaaFina
      [@aYaa.fina]' lookup FaaYaaFina;

  sub [@aLam.init]' lookup LamLamHaaInit
      [@aLam.medi]' lookup LamLamHaaInit
      [@aHaa.medi]' lookup LamLamHaaInit;

  sub [@aLam.init]' lookup LamBaaMemInit
      [@aBaa.medi]' lookup LamBaaMemInit
      [@aMem.medi]' lookup LamBaaMemInit;

  sub [@aKaf.init @aKaf.medi]' lookup KafLam
      [@aLam.medi]' lookup KafLamMemMedi
      [@aMem.medi_dots]' lookup LamMemMedi;

  sub [@aKaf.init @aKaf.medi]' lookup KafLam
      [@aLam.medi @aLam.medi_LamMemFina]' lookup KafLamMemFina
      [@aMem.fina @aMem.fina_LamMemFinaExtended]' lookup LamMemFina;

  sub [@aBaa.init]' lookup BaaDalIsol
      [@aDal.fina]' lookup BaaDalIsol;

  sub [@aBaa.init]' lookup BaaMemHaaInit
      [@aMem.medi]' lookup BaaMemHaaInit
      [@aHaa.medi]' lookup BaaMemHaaInit;

  sub [@aBaa.init]' lookup BaaBaaYaa
      [@aBaa.medi]' lookup BaaBaaYaa
      [@aYaa.fina]' lookup BaaBaaYaa;

  sub [@aLam.medi]' lookup LamYaaFina
      [@aYaa.fina]' lookup LamYaaFina;

  sub [@aLam.init]' lookup LamMemHaaInit
      [@aMem.medi]' lookup LamMemHaaInit
      [@aHaa.medi]' lookup LamMemHaaInit;

  sub [@aLam.init]' lookup LamMemInit
      [@aMem.medi]' lookup LamMemInit;

  sub [@aLam.init]' lookup LamAlfIsol
      [@aAlf.fina]' lookup LamAlfIsol;

  sub [@aLam.init]' lookup LamHaaMemInit
      [@aHaa.medi]' lookup LamHaaMemInit
      [@aMem.medi]' lookup LamHaaMemInit;

  sub [@aBaa.init]' lookup BaaBaaMemInit
      [@aBaa.medi]' lookup BaaBaaMemInit
      [@aMem.medi]' lookup BaaBaaMemInit;

  sub [@aBaa.init]' lookup BaaBaaHaaInit
      [@aBaa.medi]' lookup BaaBaaHaaInit
      [@aHaa.medi]' lookup BaaBaaHaaInit;

  sub [@aMem.init]' lookup MemRaaIsol
      [@aRaa.fina]' lookup MemRaaIsol;

  sub [@aAyn.init]'
      [@aRaa.fina]' lookup MemRaaIsol;

  sub [@aHaa.init]' lookup HaaHaaInit
      [@aHaa.medi]' lookup HaaHaaInit;

  sub [@aKaf.init @aLam.init @aBaa.init]' lookup KafMemIsol
      [@aMem.fina]' lookup KafMemIsol;

  sub [@aLam.medi]' lookup LamQafFina
      [@aQaf.fina]' lookup LamQafFina;

  sub [@aMem.init @aMem.init_MemHaaInit]' lookup MemHaaMemInit
      [@aHaa.medi @aHaa.medi_SadHaaInit]' lookup MemHaaMemInit
      [@aMem.medi]' lookup KafMemInit;

  sub [@aBaa.init]' lookup BaaNonIsol
      [@aNon.fina]' lookup BaaNonIsol;

  sub [@aKaf.medi]' lookup KafMemFina
      [@aMem.fina]' lookup KafMemFina;

  sub [@aKaf.init @aKaf.medi]' lookup KafLam
      [@aLam.medi @aLam.medi_LamAlfFina]' lookup KafLamAlf
      [@aAlf.fina @aAlf.fina_LamAlfFina];

  sub [@aKaf.init @aKaf.medi]' lookup KafLam
      [@aLam.medi @aLam.medi_LamAlfFina]' lookup KafLamAlf
              uni0621.float'
      [@aAlf.fina @aAlf.fina_LamAlfFina];

  sub [@aBaa.init]' lookup BaaSenInit
      [@aSen.medi]' lookup BaaSenInit;

  sub [@aBaa.init]' lookup BaaSenAltInit
      [@aSen.fina]' lookup BaaSenInit;

  sub [@aKaf.medi]' lookup KafRaaFina
      [@aRaa.fina]' lookup KafRaaFina;

  sub [@aLam.init]' lookup LamHehInit
      [@aHeh.medi]' lookup LamHehInit;

  sub [@aBaa.init]' lookup BaaMemInit
      [@aMem.medi]' lookup BaaMemInit;

  sub [@aKaf.init @aKaf.medi]' lookup KafLam
      [@aLam.medi @aLam.fina @aAlf.fina @aKaf.fina]' lookup KafLam;

  sub [@aKaf.init]' lookup KafRaaIsol
      [@aRaa.fina]' lookup KafRaaIsol;

  sub [@aAyn.init]' lookup AynHaaInit
      [@aHaa.medi]' lookup AynHaaInit;

  sub [@aKaf.medi @aAyn.medi]' lookup KafYaaFina
      [@aYaa.fina]' lookup KafYaaFina;

  sub [@aLam.medi]' lookup LamMemMedi
      [@aMem.medi_dots]' lookup LamMemMedi;

  sub [@aSen.init @aSad.init]' lookup SenBaaMemInit
      [@aBaa.medi]' lookup SenBaaMemInit
      [@aMem.medi]' lookup SenBaaMemInit;

  sub [@aBaa.init]' lookup BaaBaa
      [@aBaa.medi @aBaa.fina]' lookup BaaBaa;

  sub [@aHaa.init]' lookup HaaRaaIsol
      [@aRaa.fina]' lookup HaaRaaIsol;

  sub [@aLam.init]' lookup LamRaaIsol
      [@aRaa.fina]' lookup LamRaaIsol;

  sub [@aBaa.init @aBaa.init_BaaHaaInit]' lookup BaaHaaMemInit
      [@aHaa.medi @aHaa.medi_SadHaaInit]' lookup BaaHaaMemInit
      [@aMem.medi]' lookup KafMemInit;

  sub [@aAyn.init @aBaa.init @aHaa.init @aHeh.init @aMem.init @aSad.init @aSen.init]' lookup AboveHaaIsol
      [@aHaa.fina]' lookup AboveHaaIsol;

  sub [@aFaa.init @aLam.init @aKaf.init]' lookup AboveHaaIsol
      [@aHaa.medi @aHaa.fina]' lookup AboveHaaIsol2;

  sub [@aSen.init @aSad.init @aSen.medi @aSad.medi]' lookup SenYaaFina
      [@aYaa.fina @aRaa.fina]' lookup SenYaaFina;
} calt;

lookup ToothYaaBarree {
  sub @aSen.init by @aSen.init_YaaBarree;
  sub @aSad.init by @aSad.init_YaaBarree;
  sub @aYaaBarree.fina by @aYaaBarree.fina_PostTooth;
} ToothYaaBarree;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aSen.init @aSad.init]' lookup ToothYaaBarree
      [@aYaaBarree.fina]'      lookup ToothYaaBarree;
} calt;

lookup AscenderYaaBarree {
  sub @aBaa.init by @aBaa.init_YaaBarree;
  sub @aFaa.init by @aFaa.init_YaaBarree;
  sub @aLam.init by @aLam.init_YaaBarree;
  sub @aKaf.init by @aKaf.init_YaaBarree;
  sub @aYaaBarree.fina by @aYaaBarree.fina_PostAscender;
} AscenderYaaBarree;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aBaa.init @aFaa.init @aLam.init @aKaf.init]' lookup AscenderYaaBarree
      [@aYaaBarree.fina]'                            lookup AscenderYaaBarree;
} calt;

lookup AynYaaBarree {
  sub @aHaa.init by @aHaa.init_YaaBarree;
  sub @aHeh.init by @aHeh.init_YaaBarree;
  sub uni06BE.init by uni06BE.init_YaaBarree;
  sub @aAyn.init by @aAyn.init_YaaBarree;
  sub @aTaa.init by @aTaa.init_YaaBarree;
  sub @aMem.init_dots by @aMem.init_YaaBarree;
  sub @aYaaBarree.fina by @aYaaBarree.fina_PostAyn;
} AynYaaBarree;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aHaa.init @aHeh.init @aAyn.init @aTaa.init @aMem.init_dots uni06BE.init]' lookup AynYaaBarree
       @aYaaBarree.fina'                                                          lookup AynYaaBarree;
} calt;

lookup FaaMemTatweel {
  sub @aMem.medi by @aMem.medi_KafMemMediTatweel;
} FaaMemTatweel;

feature calt {
  sub @aFaa.init' lookup KafMemInit
      @Tashkil.above'
      @aMem.medi' lookup FaaMemTatweel
      @Tashkil.above';
  sub @aFaa.init' lookup KafMemInit
      @Tashkil.above' @Tashkil.above'
      @aMem.medi' lookup FaaMemTatweel
      @Tashkil.above';
  sub @aFaa.init' lookup KafMemInit
      @Tashkil.above' @Tashkil.above'
      @aMem.medi' lookup FaaMemTatweel
      @Tashkil.above' @Tashkil.above';
  sub @aFaa.init' lookup KafMemInit
      @Tashkil.above'
      @aMem.medi' lookup FaaMemTatweel
      @Tashkil.above' @Tashkil.above';
  sub @aFaa.init' lookup KafMemInit
      @Tashkil.below' @Tashkil.above'
      @aMem.medi' lookup FaaMemTatweel
      @Tashkil.above';
  sub @aFaa.init' lookup KafMemInit
      @Tashkil.above' @Tashkil.below'
      @aMem.medi' lookup FaaMemTatweel
      @Tashkil.above';
  sub @aFaa.init' lookup KafMemInit
      @Tashkil.below'
      @aMem.medi' lookup FaaMemTatweel
      @Tashkil.below';
} calt;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aKaf.init @aKaf.medi @aAyn.init @aFaa.init @aHaa.init @aHeh.init]' lookup KafMemInit
      [@aMem.medi]' lookup KafMemInit;
} calt;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aLam.init]' lookup LamLamInit
      [@LamLamFoo @aLam.medi @aKaf.fina @aLam.fina @aLam.medi_LamAlfFina]' lookup LamLamInit;

  sub [@aLam.medi]' lookup LamLamMedi2
      [@LamLamFoo @aLam.medi @aKaf.fina @aLam.fina @aLam.medi_LamAlfFina]' lookup LamLamMedi;
} calt;

lookup HehMediTooth {
  sub @aHeh.medi by @aHeh.medi_PostTooth;
  sub @aSad.init by @aSad.init_PreYaa;
  sub @aSad.medi by @aSad.medi_PreYaa;
  sub @aSen.init by @aSen.init_PreYaa;
  sub @aSen.medi by @aSen.medi_PreYaa;
} HehMediTooth;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aSad.init @aSad.medi @aSen.init @aSen.medi]' lookup HehMediTooth
      [@aHeh.medi]' lookup HehMediTooth;
} calt;

feature calt {
  sub @Tashkil.above [uni0627.fina]' [uni0653 uni06E4] by [uni0627.fina_Wide];
} calt;

feature calt {
  lookupflag IgnoreMarks;
  sub @aFaa.init
     [uni0671.fina uni0623.fina]'
  by [uni0671.fina_Wide uni0623.fina_Wide];
} calt;

feature calt {
  sub @aFaa.init
     [uni0671.fina_Wide uni0623.fina_Wide]'
  by [uni0671.fina uni0623.fina];
} calt;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aAyn.init @aHaa.init @aHaa.medi]'
      [@aAlf.fina @aDal.fina @aHeh.fina @aLam.fina @aLam.medi @aLam.medi_LamMemFina
       @aLam.medi_LamWawFina @aLam.medi_LamHeh @aLam.medi_LamYaaFina @aKaf.fina
       @aLam.medi_LamQafFina @aBaa.medi_BaaRaaFina @aLam.medi_LamAlfFina
       @aLam.medi_LamMemMedi @aLam.medi_LamLamMedi @aBaa.medi_BaaNonFina @aBaa.medi_High
       @aKaf.medi @aKaf.medi_KafMemAlf @aKaf.medi_KafMemMedi @aKaf.medi_KafMemFina
       @aKaf.medi_KafLam @aKaf.medi_KafHeh @aKaf.medi_KafBaaMedi @aKaf.medi_KafRaaFina
       @aKaf.medi_KafYaaFina]
  by  [@aAyn.init_Finjani @aHaa.init_Finjani @aHaa.medi_Finjani];
} calt;

lookup BaaYaaFina {
  sub @aBaa.medi by @aBaa.medi_BaaYaaFina;
  sub @aYaa.fina by @aYaa.fina_BaaYaaFina;
} BaaYaaFina;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aBaa.medi]' lookup BaaYaaFina
      [@aYaa.fina]' lookup BaaYaaFina;
} calt;

lookup ToothMem {
  sub @aMem.fina by @aMem.fina_PostTooth;
  sub @aSad.init by @aSad.init_PreYaa;
  sub @aSad.medi by @aSad.medi_PreYaa;
  sub @aSen.init by @aSen.init_PreYaa;
  sub @aSen.medi by @aSen.medi_PreYaa;
} ToothMem;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aSen.init @aSen.medi @aSad.init @aSad.medi]' lookup ToothMem
      [@aMem.fina]' lookup ToothMem;
} calt;

lookup KafLamYaa {
  sub @aLam.medi_KafLam by @aLam.medi_KafLamYaa;
} KafLamYaa;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aLam.medi_KafLam]' lookup KafLamYaa
      [@aYaa.fina]' lookup LamYaaFina;
} calt;

lookup LamKafInit {

    sub uni06B5.init by uni06B5.init uni0640.1;
  sub uni06B7.init by uni06B7.init uni0640.1;
  sub uni0644.init by uni0644.init uni0640.1;
  sub uni06B8.init by uni06B8.init uni0640.1;
  sub uni06B6.init by uni06B6.init uni0640.1;
  sub uni076A.init by uni076A.init uni0640.1;

    sub uni06B5.medi by uni06B5.medi uni0640.1;
  sub uni06B7.medi by uni06B7.medi uni0640.1;
  sub uni0644.medi by uni0644.medi uni0640.1;
  sub uni06B8.medi by uni06B8.medi uni0640.1;
  sub uni06B6.medi by uni06B6.medi uni0640.1;
  sub uni076A.medi by uni076A.medi uni0640.1;

    sub uni06B5.medi_KafLam by uni06B5.medi_KafLam uni0640.1;
  sub uni06B7.medi_KafLam by uni06B7.medi_KafLam uni0640.1;
  sub uni0644.medi_KafLam by uni0644.medi_KafLam uni0640.1;
  sub uni06B8.medi_KafLam by uni06B8.medi_KafLam uni0640.1;
  sub uni06B6.medi_KafLam by uni06B6.medi_KafLam uni0640.1;
  sub uni076A.medi_KafLam by uni076A.medi_KafLam uni0640.1;

    sub uni06B5.medi_KafMemLam by uni06B5.medi_KafMemLam uni0640.1;
  sub uni06B7.medi_KafMemLam by uni06B7.medi_KafMemLam uni0640.1;
  sub uni0644.medi_KafMemLam by uni0644.medi_KafMemLam uni0640.1;
  sub uni06B8.medi_KafMemLam by uni06B8.medi_KafMemLam uni0640.1;
  sub uni06B6.medi_KafMemLam by uni06B6.medi_KafMemLam uni0640.1;
  sub uni076A.medi_KafMemLam by uni076A.medi_KafMemLam uni0640.1;

    sub uni06B5.medi_LamLamInit by uni06B5.medi_LamLamInit uni0640.1;
  sub uni06B7.medi_LamLamInit by uni06B7.medi_LamLamInit uni0640.1;
  sub uni0644.medi_LamLamInit by uni0644.medi_LamLamInit uni0640.1;
  sub uni06B8.medi_LamLamInit by uni06B8.medi_LamLamInit uni0640.1;
  sub uni06B6.medi_LamLamInit by uni06B6.medi_LamLamInit uni0640.1;
  sub uni076A.medi_LamLamInit by uni076A.medi_LamLamInit uni0640.1;

  sub @aMem.medi_LamMemInit   by @aMem.medi_LamMemInitTatweel;
} LamKafInit;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aLam.init @aLam.medi @aLam.medi_KafLam @aLam.medi_KafMemLam
       @aLam.medi_LamLamInit @aMem.medi_LamMemInit]' lookup LamKafInit
      [@aKaf.medi @aKaf.medi_KafHeh @aKaf.medi_KafMemFina @aKaf.medi_KafRaaFina
       @aKaf.medi_KafYaaFina @aKaf.medi_KafMemAlf];
} calt;

lookup LamAynTatweel {

    sub uni06B5.init by uni06B5.init uni0640.2;
  sub uni06B7.init by uni06B7.init uni0640.2;
  sub uni0644.init by uni0644.init uni0640.2;
  sub uni06B8.init by uni06B8.init uni0640.2;
  sub uni06B6.init by uni06B6.init uni0640.2;
  sub uni076A.init by uni076A.init uni0640.2;

    sub uni06B5.medi by uni06B5.medi uni0640.2;
  sub uni06B7.medi by uni06B7.medi uni0640.2;
  sub uni0644.medi by uni0644.medi uni0640.2;
  sub uni06B8.medi by uni06B8.medi uni0640.2;
  sub uni06B6.medi by uni06B6.medi uni0640.2;
  sub uni076A.medi by uni076A.medi uni0640.2;

    sub uni06B5.medi_KafLam by uni06B5.medi_KafLam uni0640.2;
  sub uni06B7.medi_KafLam by uni06B7.medi_KafLam uni0640.2;
  sub uni0644.medi_KafLam by uni0644.medi_KafLam uni0640.2;
  sub uni06B8.medi_KafLam by uni06B8.medi_KafLam uni0640.2;
  sub uni06B6.medi_KafLam by uni06B6.medi_KafLam uni0640.2;
  sub uni076A.medi_KafLam by uni076A.medi_KafLam uni0640.2;

    sub uni06B5.medi_KafMemLam by uni06B5.medi_KafMemLam uni0640.2;
  sub uni06B7.medi_KafMemLam by uni06B7.medi_KafMemLam uni0640.2;
  sub uni0644.medi_KafMemLam by uni0644.medi_KafMemLam uni0640.2;
  sub uni06B8.medi_KafMemLam by uni06B8.medi_KafMemLam uni0640.2;
  sub uni06B6.medi_KafMemLam by uni06B6.medi_KafMemLam uni0640.2;
  sub uni076A.medi_KafMemLam by uni076A.medi_KafMemLam uni0640.2;

    sub uni06B5.medi_LamLamInit by uni06B5.medi_LamLamInit uni0640.2;
  sub uni06B7.medi_LamLamInit by uni06B7.medi_LamLamInit uni0640.2;
  sub uni0644.medi_LamLamInit by uni0644.medi_LamLamInit uni0640.2;
  sub uni06B8.medi_LamLamInit by uni06B8.medi_LamLamInit uni0640.2;
  sub uni06B6.medi_LamLamInit by uni06B6.medi_LamLamInit uni0640.2;
  sub uni076A.medi_LamLamInit by uni076A.medi_LamLamInit uni0640.2;

  sub @aMem.medi_LamMemInit   by @aMem.medi_LamMemInitTatweel;
} LamAynTatweel;

feature calt {
  sub [@aLam.init @aLam.medi @aLam.medi_KafLam @aLam.medi_KafMemLam
       @aLam.medi_LamLamInit @aMem.medi_LamMemInit]' lookup LamAynTatweel
      [uni064D uni0650 uni065C]
       @aAyn.fina;
  sub [@aLam.init @aLam.medi @aLam.medi_KafLam @aLam.medi_KafMemLam
       @aLam.medi_LamLamInit @aMem.medi_LamMemInit]' lookup LamAynTatweel
      [uni0651]
      [uni064D uni0650 uni065C]
       @aAyn.fina;
} calt;


lookup HehYaaFina {
  sub @aHeh.medi_BaaHehMedi by @aHeh.medi_HehYaaFina;
  sub @aHeh.medi_PostTooth by @aHeh.medi_PostToothHehYaa;
} HehYaaFina;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aHeh.medi_PostTooth @aHeh.medi_BaaHehMedi]' lookup HehYaaFina
      [@aYaa.fina]'                                 lookup BaaYaaFina;
} calt;

feature calt {
  lookupflag IgnoreMarks;
  sub @RaaWaw [uni0625 uni0673]' by @aAlf.isol_LowHamza;
} calt;


lookup FaaHaaKaf {
  sub uni062E.medi_FaaHaaInit by uni062E.medi_FaaHaaInit uni0640.1;
  sub uni062D.medi_FaaHaaInit by uni062D.medi_FaaHaaInit uni0640.1;
  sub uni0681.medi_FaaHaaInit by uni0681.medi_FaaHaaInit uni0640.1;
  sub uni0687.medi_FaaHaaInit by uni0687.medi_FaaHaaInit uni0640.1;
  sub uni0685.medi_FaaHaaInit by uni0685.medi_FaaHaaInit uni0640.1;
  sub uni062C.medi_FaaHaaInit by uni062C.medi_FaaHaaInit uni0640.1;
  sub uni0682.medi_FaaHaaInit by uni0682.medi_FaaHaaInit uni0640.1;
  sub uni0757.medi_FaaHaaInit by uni0757.medi_FaaHaaInit uni0640.1;
  sub uni0684.medi_FaaHaaInit by uni0684.medi_FaaHaaInit uni0640.1;
  sub uni076F.medi_FaaHaaInit by uni076F.medi_FaaHaaInit uni0640.1;
  sub uni076E.medi_FaaHaaInit by uni076E.medi_FaaHaaInit uni0640.1;
  sub uni0683.medi_FaaHaaInit by uni0683.medi_FaaHaaInit uni0640.1;
  sub uni06BF.medi_FaaHaaInit by uni06BF.medi_FaaHaaInit uni0640.1;
  sub uni077C.medi_FaaHaaInit by uni077C.medi_FaaHaaInit uni0640.1;
  sub uni0758.medi_FaaHaaInit by uni0758.medi_FaaHaaInit uni0640.1;
  sub uni0772.medi_FaaHaaInit by uni0772.medi_FaaHaaInit uni0640.1;
  sub uni0686.medi_FaaHaaInit by uni0686.medi_FaaHaaInit uni0640.1;
} FaaHaaKaf;

feature calt {
  lookupflag IgnoreMarks;
  sub @aFaa.init_FaaHaaInit
      @aHaa.medi_FaaHaaInit' lookup FaaHaaKaf
     [@aKaf.medi_KafHeh @aKaf.medi_KafMemFina @aKaf.medi_KafRaaFina @aKaf.medi_KafYaaFina];
} calt;

feature calt {
  lookupflag IgnoreMarks;
  sub [@aBaa.init]' [@aAlf.fina] by [@aBaa.init_Wide];
} calt;

feature calt {
  sub [@aBaa.init_Wide]'
      [uni0622.fina uni0623.fina uni0627.fina uni0671.fina uni0672.fina
       uni0675.fina uni0773.fina uni0774.fina]
   by [@aBaa.init];
} calt;

feature calt {
  lookupflag IgnoreMarks;
  script arab;
  language ARA;
  sub uni062A.init_BaaBaaIsol'
      [uni062A.medi_BaaBaaInit uni062B.medi_BaaBaaInit]
   by uni067A.init_BaaBaaIsol;

  sub uni062A.init_High'
      [uni062A.medi_High uni062B.medi_High]
   by uni067A.init_High;
} calt;
feature ss01 {
  lookupflag IgnoreMarks;
  sub @RaaWaw @aBaaDotBelow' by @aBaaLowDotBelow;
} ss01;

lookup MemAlfFina {
  sub @aAlf.fina by @aAlf.fina_MemAlfFina;
  sub @aMem.medi by @aMem.medi_MemAlfFina;

  sub @aSad.medi by @aSad.medi_PreYaa;
  sub @aSen.medi by @aSen.medi_PreYaa;

  sub @aBaa.init_BaaSenInit by @aBaa.init_BaaSenAltInit;
  sub @aSen.medi_BaaSenInit by @aSen.medi_BaaSenAltInit;
} MemAlfFina;

lookup MemAlfFinaPostTooth {
  sub @aMem.medi by @aMem.medi_AlfPostTooth;
} MemAlfFinaPostTooth;

lookup BaaMemAlfFina {
  sub @aBaa.medi by @aBaa.medi_BaaMemAlfFina;
  sub @aMem.medi by @aMem.medi_BaaMemAlfFina;
} BaaMemAlfFina;

@NoMemAlfFina = [@aMem.medi @aMem.medi_LamBaaMemInit @aMem.medi_LamMemInit
                 @aMem.medi_LamHaaMemInit @aMem.medi_BaaMemInit
                 @aMem.medi_LamMemMedi @aMem.medi_SenBaaMemInit
                 @aMem.medi_BaaBaaMemInit @aMem.medi_LamMemInitTatweel
                 @aMem.medi_KafMemMedi @aMem.medi_SenMemInit
                 @aHeh.medi_BaaHehMedi @aHeh.medi_PostTooth];

feature ss02 {
  lookupflag IgnoreMarks;
  ignore sub @NoMemAlfFina @aMem.medi' @aAlf.fina';

  sub @aBaa.medi' lookup BaaMemAlfFina
      @aMem.medi' lookup BaaMemAlfFina
      @aAlf.fina' lookup MemAlfFina;

  sub [@aSad.medi @aSen.medi]' lookup MemAlfFina
      @aMem.medi' lookup MemAlfFinaPostTooth
      @aAlf.fina' lookup MemAlfFina;

  sub @aBaa.init_BaaSenInit' lookup MemAlfFina
      @aSen.medi_BaaSenInit' lookup MemAlfFina
      @aMem.medi'            lookup MemAlfFinaPostTooth
      @aAlf.fina'            lookup MemAlfFina;

  sub @aMem.medi' lookup MemAlfFina
      @aAlf.fina' lookup MemAlfFina;
} ss02;

lookup localsym {
  sub at by at.ara;
} localsym;

feature ss03 {
  script arab;
  language ARA;
  lookup localsym;

  script arab;
  language dflt;
  lookup localsym;
} ss03;
@Dal_1 = [@aDal.fina @aDal.fina_BaaDal @aDal.fina_KafDal @aDal.fina_LamDal
          @aDal.isol];
@1st_2 = [@RaaWaw @AlefHamzaBelow];
@1st_3 = [@aRaa.fina_MemRaaIsol];
@1st_4 = [@aHeh.isol @aHeh.fina @aHeh.fina_KafHeh @aHeh.fina_LamHeh];

@Alf_1 = [uni0627.fina_LamAlfFina uni0625.fina_LamAlfFina];
@Alf_2 = [@aAlf.fina @aAlf.fina_KafAlf @aAlf.fina_KafMemAlf
          @aAlf.fina_LamAlfFina @aAlf.fina_LamAlfIsol @aAlf.fina_MemAlfFina];

@Kaf_1 = [@aKaf.init_KafHeh
          @aKaf.init_KafMemIsol];
@Kaf_2 = [@aKaf.init_KafBaaInit @aKaf.init_KafMemAlf];
@Kaf_3 = [@aKaf.init @aKaf.init_KafLam];
@Kaf_4 = [@aKaf.init_KafYaaIsol];
@Kaf_5 = [@aKaf.init_KafMemInit @aKaf.init_KafRaaIsol];
@Raa_1 = [@RaaWaw @aDal.fina @aDal.isol @aDal.fina_BaaDal @aDal.fina_KafDal
          @aDal.fina_LamDal];
@2nd_3 = [@aSen.init_SenHaaInit @aSad.init_SadHaaInit @aLam.init_LamHaaHaaInit];
@2nd_4 = [@aSad.init_AboveHaa @aSen.init_AboveHaa @aHeh.init_AboveHaa
          @aHaa.init_AboveHaa @aMem.init_AboveHaa @aKaf.init_AboveHaa
          @aAyn.init_AboveHaa];
@Teh_m = [uni0629.fina_LamHeh uni0629.fina_KafHeh           uni06C3.fina_LamHeh uni06C3.fina_KafHeh];

feature kern {
    pos @RaaWaw' <-600 0 -600 0>  @Kaf_1;
  pos @RaaWaw' <-300 0 -300 0>  @Kaf_4;
  pos @Dal_1'  <-600 0 -600 0>  @Kaf_1;
  pos @Dal_1'  <-400 0 -400 0>  @Kaf_4;
} kern;

feature kern {
  lookupflag IgnoreMarks;
  pos @RaaWaw' <-600 0 -600 0>  @Kaf_5;
  pos @RaaWaw' <-300 0 -300 0> [@Kaf_2 @Kaf_3];

  pos @RaaWaw' <-100 0 -100 0> @2nd_3;

  pos @Dal_1'  <-600 0 -600 0>  @Kaf_5;
  pos @Dal_1'  <-400 0 -400 0> [@Kaf_2 @Kaf_3];

  pos @1st_3' <-400 0 -400 0> [@Kaf_2 @Kaf_3 @Kaf_4];
  pos @1st_3' <-800 0 -800 0> [@Kaf_1 @Kaf_5];
  pos @1st_3' <-200 0 -200 0> @2nd_3;
  pos @1st_3' <-400 0 -400 0> @2nd_4;

    pos @Teh_m' < 150 0  150 0> exclam;

    pos @1st_2' < 170 0  170 0> @aBaaDotBelow;
} kern;

feature kern {
  pos @Alf_1' <-500 0 -500 0> [@Kaf_1 @Kaf_5];
  pos @Alf_1' <-200 0 -200 0> @Kaf_2;
  pos @Alf_1' <-250 0 -250 0> @Kaf_3;
  pos @Alf_1' <-300 0 -300 0> @Kaf_4;

      pos @Raa_1' < 200 0  200 0> uni064E uni0670.isol [uni0653 uni06E4];

  pos [uni0621]' < 200 0  200 0> uni064E uni0670.isol;
} kern;

feature kern {
  lookupflag IgnoreMarks;
  pos @RaaWaw' <80 0 80 0> @aYaa.init;
} kern;

feature kern {
  lookupflag IgnoreMarks;
      pos @1st_4 uni200C [@Kaf_1 @Kaf_2 @Kaf_3 @Kaf_4 @Kaf_5]' -500;
    pos @1st_4 space   [@Kaf_1 @Kaf_2 @Kaf_3 @Kaf_4 @Kaf_5]' -500;
    pos @1st_4         [@Kaf_1 @Kaf_2 @Kaf_3 @Kaf_4 @Kaf_5]' -500;
} kern;


feature kern {
  pos uni0661.prop uni0662.prop -100;
  pos uni0661.prop uni0663.prop -100;
  pos uni0661.prop uni0666.prop -200;
  pos uni0661.prop uni0669.prop -100;

  pos uni0662.prop uni0667.prop  100;
  pos uni0662.prop uni0668.prop  -50;

  pos uni0663.prop uni0667.prop  100;

  pos uni0664.prop uni0661.prop  -50;
  pos uni0664.prop uni0668.prop   50;

  pos uni0666.prop uni0661.prop  -50;

  pos uni0667.prop uni0661.prop  -50;
  pos uni0667.prop uni0668.prop  -50;

  pos uni0668.prop uni0661.prop -100;
  pos uni0668.prop uni0662.prop -100;
  pos uni0668.prop uni0663.prop -100;
  pos uni0668.prop uni0666.prop -100;
  pos uni0668.prop uni0667.prop -100;

  pos uni0669.prop uni0668.prop   50;
  pos uni0669.prop uni0660.prop   50;

  pos uni0660.prop uni0667.prop   50;
  pos uni0660.prop uni0668.prop   50;
} kern;


# GSUB 


lookup Latincomposition {
  lookupflag 0;
    sub \A \gravecomb  by \Agrave;
    sub \A \acutecomb  by \Aacute;
    sub \A \uni0302  by \Acircumflex;
    sub \A \tildecomb  by \Atilde;
    sub \A \uni0308  by \Adieresis;
    sub \A \uni030A  by \Aring;
    sub \C \uni0327  by \Ccedilla;
    sub \E \gravecomb  by \Egrave;
    sub \E \acutecomb  by \Eacute;
    sub \E \uni0302  by \Ecircumflex;
    sub \E \uni0308  by \Edieresis;
    sub \I \gravecomb  by \Igrave;
    sub \I \acutecomb  by \Iacute;
    sub \I \uni0302  by \Icircumflex;
    sub \I \uni0308  by \Idieresis;
    sub \N \tildecomb  by \Ntilde;
    sub \O \gravecomb  by \Ograve;
    sub \O \acutecomb  by \Oacute;
    sub \O \uni0302  by \Ocircumflex;
    sub \O \tildecomb  by \Otilde;
    sub \O \uni0308  by \Odieresis;
    sub \U \gravecomb  by \Ugrave;
    sub \U \acutecomb  by \Uacute;
    sub \U \uni0302  by \Ucircumflex;
    sub \U \uni0308  by \Udieresis;
    sub \Y \acutecomb  by \Yacute;
    sub \a \gravecomb  by \agrave;
    sub \a \acutecomb  by \aacute;
    sub \a \uni0302  by \acircumflex;
    sub \a \tildecomb  by \atilde;
    sub \a \uni0308  by \adieresis;
    sub \a \uni030A  by \aring;
    sub \c \uni0327  by \ccedilla;
    sub \e \gravecomb  by \egrave;
    sub \e \acutecomb  by \eacute;
    sub \e \uni0302  by \ecircumflex;
    sub \e \uni0308  by \edieresis;
    sub \i \gravecomb  by \igrave;
    sub \i \acutecomb  by \iacute;
    sub \i \uni0302  by \icircumflex;
    sub \i \uni0308  by \idieresis;
    sub \n \tildecomb  by \ntilde;
    sub \o \gravecomb  by \ograve;
    sub \o \acutecomb  by \oacute;
    sub \o \uni0302  by \ocircumflex;
    sub \o \tildecomb  by \otilde;
    sub \o \uni0308  by \odieresis;
    sub \u \gravecomb  by \ugrave;
    sub \u \acutecomb  by \uacute;
    sub \u \uni0302  by \ucircumflex;
    sub \u \uni0308  by \udieresis;
    sub \y \acutecomb  by \yacute;
    sub \y \uni0308  by \ydieresis;
    sub \A \uni0304  by \Amacron;
    sub \a \uni0304  by \amacron;
    sub \A \uni0306  by \Abreve;
    sub \a \uni0306  by \abreve;
    sub \A \uni0328  by \Aogonek;
    sub \a \uni0328  by \aogonek;
    sub \C \acutecomb  by \Cacute;
    sub \c \acutecomb  by \cacute;
    sub \C \uni0302  by \Ccircumflex;
    sub \c \uni0302  by \ccircumflex;
    sub \C \uni0307  by \Cdotaccent;
    sub \c \uni0307  by \cdotaccent;
    sub \C \uni030C  by \Ccaron;
    sub \c \uni030C  by \ccaron;
    sub \D \uni030C  by \Dcaron;
    sub \d \uni030C  by \dcaron;
    sub \E \uni0304  by \Emacron;
    sub \e \uni0304  by \emacron;
    sub \E \uni0306  by \Ebreve;
    sub \e \uni0306  by \ebreve;
    sub \E \uni0307  by \Edotaccent;
    sub \e \uni0307  by \edotaccent;
    sub \E \uni0328  by \Eogonek;
    sub \e \uni0328  by \eogonek;
    sub \E \uni030C  by \Ecaron;
    sub \e \uni030C  by \ecaron;
    sub \G \uni0302  by \Gcircumflex;
    sub \g \uni0302  by \gcircumflex;
    sub \G \uni0306  by \Gbreve;
    sub \g \uni0306  by \gbreve;
    sub \G \uni0307  by \Gdotaccent;
    sub \g \uni0307  by \gdotaccent;
    sub \G \uni0327  by \uni0122;
    sub \g \uni0327  by \uni0123;
    sub \H \uni0302  by \Hcircumflex;
    sub \h \uni0302  by \hcircumflex;
    sub \I \tildecomb  by \Itilde;
    sub \i \tildecomb  by \itilde;
    sub \I \uni0304  by \Imacron;
    sub \i \uni0304  by \imacron;
    sub \I \uni0306  by \Ibreve;
    sub \i \uni0306  by \ibreve;
    sub \I \uni0328  by \Iogonek;
    sub \i \uni0328  by \iogonek;
    sub \I \uni0307  by \Idotaccent;
    sub \J \uni0302  by \Jcircumflex;
    sub \j \uni0302  by \jcircumflex;
    sub \K \uni0327  by \uni0136;
    sub \k \uni0327  by \uni0137;
    sub \L \acutecomb  by \Lacute;
    sub \l \acutecomb  by \lacute;
    sub \L \uni0327  by \uni013B;
    sub \l \uni0327  by \uni013C;
    sub \L \uni030C  by \Lcaron;
    sub \l \uni030C  by \lcaron;
    sub \N \acutecomb  by \Nacute;
    sub \n \acutecomb  by \nacute;
    sub \N \uni0327  by \uni0145;
    sub \n \uni0327  by \uni0146;
    sub \N \uni030C  by \Ncaron;
    sub \n \uni030C  by \ncaron;
    sub \O \uni0304  by \Omacron;
    sub \o \uni0304  by \omacron;
    sub \O \uni0306  by \Obreve;
    sub \o \uni0306  by \obreve;
    sub \O \uni030B  by \Ohungarumlaut;
    sub \o \uni030B  by \ohungarumlaut;
    sub \R \acutecomb  by \Racute;
    sub \r \acutecomb  by \racute;
    sub \R \uni0327  by \uni0156;
    sub \r \uni0327  by \uni0157;
    sub \R \uni030C  by \Rcaron;
    sub \r \uni030C  by \rcaron;
    sub \S \acutecomb  by \Sacute;
    sub \s \acutecomb  by \sacute;
    sub \S \uni0302  by \Scircumflex;
    sub \s \uni0302  by \scircumflex;
    sub \S \uni0327  by \Scedilla;
    sub \s \uni0327  by \scedilla;
    sub \S \uni030C  by \Scaron;
    sub \s \uni030C  by \scaron;
    sub \T \uni0327  by \uni0162;
    sub \t \uni0327  by \uni0163;
    sub \T \uni030C  by \Tcaron;
    sub \t \uni030C  by \tcaron;
    sub \U \tildecomb  by \Utilde;
    sub \u \tildecomb  by \utilde;
    sub \U \uni0304  by \Umacron;
    sub \u \uni0304  by \umacron;
    sub \U \uni0306  by \Ubreve;
    sub \u \uni0306  by \ubreve;
    sub \U \uni030A  by \Uring;
    sub \u \uni030A  by \uring;
    sub \U \uni030B  by \Uhungarumlaut;
    sub \u \uni030B  by \uhungarumlaut;
    sub \U \uni0328  by \Uogonek;
    sub \u \uni0328  by \uogonek;
    sub \W \uni0302  by \Wcircumflex;
    sub \w \uni0302  by \wcircumflex;
    sub \Y \uni0302  by \Ycircumflex;
    sub \y \uni0302  by \ycircumflex;
    sub \Y \uni0308  by \Ydieresis;
    sub \Z \acutecomb  by \Zacute;
    sub \z \acutecomb  by \zacute;
    sub \Z \uni0307  by \Zdotaccent;
    sub \z \uni0307  by \zdotaccent;
    sub \Z \uni030C  by \Zcaron;
    sub \z \uni030C  by \zcaron;
    sub \B \uni0307  by \uni1E02;
    sub \b \uni0307  by \uni1E03;
    sub \D \uni0307  by \uni1E0A;
    sub \d \uni0307  by \uni1E0B;
    sub \F \uni0307  by \uni1E1E;
    sub \f \uni0307  by \uni1E1F;
    sub \M \uni0307  by \uni1E40;
    sub \m \uni0307  by \uni1E41;
    sub \P \uni0307  by \uni1E56;
    sub \p \uni0307  by \uni1E57;
    sub \S \uni0307  by \uni1E60;
    sub \s \uni0307  by \uni1E61;
    sub \T \uni0307  by \uni1E6A;
    sub \t \uni0307  by \uni1E6B;
    sub \D \dotbelowcomb  by \uni1E0C;
    sub \d \dotbelowcomb  by \uni1E0D;
    sub \D \uni0331  by \uni1E0E;
    sub \d \uni0331  by \uni1E0F;
    sub \D \uni0327  by \uni1E10;
    sub \d \uni0327  by \uni1E11;
    sub \H \dotbelowcomb  by \uni1E24;
    sub \h \dotbelowcomb  by \uni1E25;
    sub \H \uni0327  by \uni1E28;
    sub \h \uni0327  by \uni1E29;
    sub \H \uni032E  by \uni1E2A;
    sub \h \uni032E  by \uni1E2B;
    sub \S \dotbelowcomb  by \uni1E62;
    sub \s \dotbelowcomb  by \uni1E63;
    sub \T \dotbelowcomb  by \uni1E6C;
    sub \t \dotbelowcomb  by \uni1E6D;
    sub \T \uni0331  by \uni1E6E;
    sub \t \uni0331  by \uni1E6F;
    sub \W \gravecomb  by \Wgrave;
    sub \w \gravecomb  by \wgrave;
    sub \W \acutecomb  by \Wacute;
    sub \w \acutecomb  by \wacute;
    sub \W \uni0308  by \Wdieresis;
    sub \w \uni0308  by \wdieresis;
    sub \Z \dotbelowcomb  by \uni1E92;
    sub \z \dotbelowcomb  by \uni1E93;
    sub \h \uni0331  by \uni1E96;
    sub \t \uni0308  by \uni1E97;
    sub \Y \gravecomb  by \Ygrave;
    sub \y \gravecomb  by \ygrave;
    sub \G \uni030C  by \Gcaron;
    sub \g \uni030C  by \gcaron;
} Latincomposition;

feature ccmp {

  script DFLT;
     language dflt ;
      lookup Latincomposition;

  script arab;
     language dflt ;
      lookup Latincomposition;
     language ARA  exclude_dflt;
      lookup Latincomposition;
     language SND  exclude_dflt;
      lookup Latincomposition;
     language URD  exclude_dflt;
      lookup Latincomposition;

  script latn;
     language dflt ;
      lookup Latincomposition;
     language TRK  exclude_dflt;
      lookup Latincomposition;
} ccmp;

# GPOS 


lookup kernHorizontalKerninginLatinloo {
  lookupflag 0;
    @kc1_first_0 = [\parenleft ];
    @kc1_first_1 = [\bracketleft ];
    @kc1_first_2 = [\braceleft ];
    @kc1_second_0 = [];
    @kc1_second_1 = [\zero \zero.prop ];
    @kc1_second_2 = [\J \Jcircumflex ];
    @kc1_second_3 = [\C \G \O \Q \Ccedilla \Ograve \Oacute \Ocircumflex \Otilde \Odieresis \Oslash 
	\Cacute \Ccircumflex \Cdotaccent \Ccaron \Gcircumflex \Gbreve 
	\Gdotaccent \uni0122 \Omacron \Obreve \Ohungarumlaut \OE ];
    @kc1_second_4 = [\S \Sacute \Scircumflex \Scedilla \Scaron \uni1E60 \uni1E62 ];
    @kc1_second_5 = [\a \agrave \aacute \acircumflex \atilde \adieresis \aring \ae \amacron \abreve 
	\aogonek ];
    @kc1_second_6 = [\c \ccedilla \cacute \ccircumflex \cdotaccent \ccaron ];
    @kc1_second_7 = [\d \dcaron \dcroat \uni1E0B \uni1E0D \uni1E0F \uni1E11 ];
    @kc1_second_8 = [\m \n \r \ntilde \kgreenlandic \nacute \uni0146 \ncaron \eng \racute \uni0157 
	\rcaron \uni1E41 ];
    @kc1_second_9 = [\e \o \egrave \eacute \ecircumflex \edieresis \eth \ograve \oacute \ocircumflex 
	\otilde \odieresis \oslash \emacron \ebreve \edotaccent \eogonek 
	\ecaron \omacron \obreve \ohungarumlaut \oe ];
    @kc1_second_10 = [\s \sacute \scircumflex \scedilla \scaron \uni1E61 \uni1E63 ];
    @kc1_second_11 = [\t \uni0163 \tcaron \tbar \uni1E6B \uni1E6D \uni1E6F \uni1E97 ];
    @kc1_second_12 = [\u \ugrave \uacute \ucircumflex \udieresis \utilde \umacron \ubreve \uring 
	\uhungarumlaut \uogonek ];
    @kc1_second_13 = [\v ];
    @kc1_second_14 = [\w \wcircumflex \wgrave \wacute \wdieresis ];
    @kc1_second_15 = [\y \yacute \ydieresis \ycircumflex \ygrave ];
    @kc1_second_16 = [\z \zacute \zdotaccent \zcaron \uni1E93 ];
    @kc1_second_17 = [];
    @kc1_second_18 = [];
    @kc1_second_19 = [];
    @kc1_second_20 = [];
    @kc1_second_21 = [];
    @kc1_second_22 = [];
    @kc1_second_23 = [];
    @kc1_second_24 = [];
    @kc1_second_25 = [];
    @kc1_second_26 = [];
    @kc1_second_27 = [];
    @kc1_second_28 = [\A \Agrave \Aacute \Acircumflex \Atilde \Adieresis \Aring \Amacron \Abreve 
	\Aogonek ];
    @kc1_second_29 = [\U \Ugrave \Uacute \Ucircumflex \Udieresis \Utilde \Umacron \Ubreve \Uring 
	\Uhungarumlaut \Uogonek ];
    @kc1_second_30 = [\f \germandbls \longs \uni1E1F \f_f \f_i \f_f_i \f_l \f_f_l \f_b \f_f_b \f_k \f_f_k 
	\f_h \f_f_h \f_j \f_f_j ];
    @kc1_second_31 = [\i \igrave \iacute \icircumflex \idieresis \itilde \imacron \ibreve \iogonek 
	\dotlessi \ij ];
    @kc1_second_32 = [\AE ];
    pos @kc1_first_0 @kc1_second_1 -42;
    pos @kc1_first_0 @kc1_second_2 200;
    pos @kc1_first_0 @kc1_second_3 -46;
    pos @kc1_first_0 @kc1_second_4 -24;
    pos @kc1_first_0 @kc1_second_5 -50;
    pos @kc1_first_0 @kc1_second_6 -62;
    pos @kc1_first_0 @kc1_second_7 -58;
    pos @kc1_first_0 @kc1_second_8 -28;
    pos @kc1_first_0 @kc1_second_9 -62;
    pos @kc1_first_0 @kc1_second_10 -44;
    pos @kc1_first_0 @kc1_second_11 -42;
    pos @kc1_first_0 @kc1_second_12 -46;
    pos @kc1_first_0 @kc1_second_13 -52;
    pos @kc1_first_0 @kc1_second_14 -52;
    pos @kc1_first_0 @kc1_second_15 -32;
    pos @kc1_first_0 @kc1_second_16 -38;
    pos @kc1_first_0 @kc1_second_17 -20;
    pos @kc1_first_0 @kc1_second_18 -32;
    pos @kc1_first_0 @kc1_second_19 128;
    pos @kc1_first_0 @kc1_second_20 -32;
    pos @kc1_first_0 @kc1_second_21 -62;
    pos @kc1_first_0 @kc1_second_22 -46;
    pos @kc1_first_0 @kc1_second_23 -50;
    pos @kc1_first_0 @kc1_second_24 -52;
    pos @kc1_first_0 @kc1_second_25 -40;
    pos @kc1_first_0 @kc1_second_26 -40;
    pos @kc1_first_0 @kc1_second_27 -34;
    pos @kc1_first_1 @kc1_second_1 -42;
    pos @kc1_first_1 @kc1_second_2 222;
    pos @kc1_first_1 @kc1_second_3 -44;
    pos @kc1_first_1 @kc1_second_4 -34;
    pos @kc1_first_1 @kc1_second_5 -52;
    pos @kc1_first_1 @kc1_second_6 -58;
    pos @kc1_first_1 @kc1_second_7 -54;
    pos @kc1_first_1 @kc1_second_8 -38;
    pos @kc1_first_1 @kc1_second_9 -56;
    pos @kc1_first_1 @kc1_second_10 -50;
    pos @kc1_first_1 @kc1_second_11 -42;
    pos @kc1_first_1 @kc1_second_12 -42;
    pos @kc1_first_1 @kc1_second_13 -46;
    pos @kc1_first_1 @kc1_second_14 -46;
    pos @kc1_first_1 @kc1_second_15 -32;
    pos @kc1_first_1 @kc1_second_16 -46;
    pos @kc1_first_1 @kc1_second_17 -42;
    pos @kc1_first_1 @kc1_second_18 -44;
    pos @kc1_first_1 @kc1_second_19 136;
    pos @kc1_first_1 @kc1_second_20 -42;
    pos @kc1_first_1 @kc1_second_21 -56;
    pos @kc1_first_1 @kc1_second_22 -50;
    pos @kc1_first_1 @kc1_second_23 -46;
    pos @kc1_first_1 @kc1_second_24 -50;
    pos @kc1_first_1 @kc1_second_25 -38;
    pos @kc1_first_1 @kc1_second_26 -40;
    pos @kc1_first_1 @kc1_second_27 -44;
    pos @kc1_first_1 @kc1_second_28 -30;
    pos @kc1_first_1 @kc1_second_29 -24;
    pos @kc1_first_1 @kc1_second_30 -28;
    pos @kc1_first_1 @kc1_second_31 -26;
    pos @kc1_first_1 @kc1_second_32 -26;
    pos @kc1_first_2 @kc1_second_2 138;
    pos @kc1_first_2 @kc1_second_6 -20;
    pos @kc1_first_2 @kc1_second_9 -20;
    pos @kc1_first_2 @kc1_second_17 -24;
    pos @kc1_first_2 @kc1_second_19 66;
    pos @kc1_first_2 @kc1_second_21 -20;
  subtable;
    @kc2_first_0 = [\longs ];
    @kc2_second_0 = [];
    @kc2_second_1 = [\w \wcircumflex \wgrave \wacute \wdieresis ];
    @kc2_second_2 = [\y \yacute \ydieresis \ycircumflex \ygrave ];
    pos @kc2_first_0 @kc2_second_1 -10;
    pos @kc2_first_0 @kc2_second_2 -10;
  subtable;
    @kc3_first_0 = [\quotedbl \quotesingle ];
    @kc3_first_1 = [\asterisk ];
    @kc3_first_2 = [\hyphen \endash \emdash \uni2015 ];
    @kc3_first_3 = [\comma \period \quotesinglbase \quotedblbase ];
    @kc3_first_4 = [\slash ];
    @kc3_first_5 = [\colon \semicolon ];
    @kc3_first_6 = [\backslash ];
    @kc3_first_7 = [\quoteleft \quotedblleft ];
    @kc3_first_8 = [\quoteright \quotedblright ];
    @kc3_first_9 = [\guillemotleft \guilsinglleft ];
    @kc3_first_10 = [\guillemotright \guilsinglright ];
    @kc3_second_0 = [];
    @kc3_second_1 = [\ampersand ];
    @kc3_second_2 = [\comma \period \quotesinglbase \quotedblbase \ellipsis ];
    @kc3_second_3 = [\slash ];
    @kc3_second_4 = [\four.prop ];
    @kc3_second_5 = [\six.prop ];
    @kc3_second_6 = [\at ];
    @kc3_second_7 = [\A \Agrave \Aacute \Acircumflex \Atilde \Adieresis \Aring \Amacron \Abreve 
	\Aogonek ];
    @kc3_second_8 = [\a \agrave \aacute \acircumflex \atilde \adieresis \aring \ae \amacron \abreve 
	\aogonek ];
    @kc3_second_9 = [\c \ccedilla \cacute \ccircumflex \cdotaccent \ccaron ];
    @kc3_second_10 = [\d \dcaron \dcroat \uni1E0B \uni1E0D \uni1E0F \uni1E11 ];
    @kc3_second_11 = [\g \gcircumflex \gbreve \gdotaccent \uni0123 ];
    @kc3_second_12 = [\e \o \egrave \eacute \ecircumflex \edieresis \eth \ograve \oacute \ocircumflex 
	\otilde \odieresis \oslash \emacron \ebreve \edotaccent \eogonek 
	\ecaron \omacron \obreve \ohungarumlaut \oe ];
    @kc3_second_13 = [\q ];
    @kc3_second_14 = [\s \sacute \scircumflex \scedilla \scaron \uni1E61 \uni1E63 ];
    @kc3_second_15 = [\AE ];
    @kc3_second_16 = [];
    @kc3_second_17 = [\guillemotleft \guilsinglleft ];
    @kc3_second_18 = [];
    @kc3_second_19 = [];
    @kc3_second_20 = [];
    @kc3_second_21 = [\v ];
    @kc3_second_22 = [\w \wcircumflex \wgrave \wacute \wdieresis ];
    @kc3_second_23 = [\y \yacute \ydieresis \ycircumflex \ygrave ];
    @kc3_second_24 = [];
    @kc3_second_25 = [];
    @kc3_second_26 = [\B \D \E \F \H \I \K \L \P \R \Egrave \Eacute \Ecircumflex \Edieresis \Igrave \Iacute 
	\Icircumflex \Idieresis \Eth \Thorn \Dcaron \Dcroat \Emacron \Ebreve 
	\Edotaccent \Eogonek \Ecaron \Hcircumflex \Hbar \Itilde \Imacron 
	\Ibreve \Iogonek \Idotaccent \IJ \uni0136 \Lacute \uni013B \Lcaron \Ldot 
	\Lslash \Racute \uni0156 \Rcaron \uni1E02 \uni1E0A \uni1E0C \uni1E0E 
	\uni1E10 \uni1E1E \uni1E24 \uni1E28 \uni1E2A \uni1E56 ];
    @kc3_second_27 = [\J \Jcircumflex ];
    @kc3_second_28 = [\M \uni1E40 ];
    @kc3_second_29 = [\N \Ntilde \Nacute \uni0145 \Ncaron \Eng ];
    @kc3_second_30 = [\T \uni0162 \Tcaron \Tbar \uni1E6A \uni1E6C \uni1E6E ];
    @kc3_second_31 = [\V ];
    @kc3_second_32 = [\W \Wcircumflex \Wgrave \Wacute \Wdieresis ];
    @kc3_second_33 = [\X ];
    @kc3_second_34 = [\Y \Yacute \Ycircumflex \Ydieresis \Ygrave ];
    @kc3_second_35 = [\Z \Zacute \Zdotaccent \Zcaron \uni1E92 ];
    @kc3_second_36 = [\f \germandbls \longs \uni1E1F \f_f \f_i \f_f_i \f_l \f_f_l \f_b \f_f_b \f_k \f_f_k 
	\f_h \f_f_h \f_j \f_f_j ];
    @kc3_second_37 = [\h \k \hcircumflex \hbar \uni0137 \uni1E25 \uni1E29 \uni1E2B \uni1E96 ];
    @kc3_second_38 = [\i \igrave \iacute \icircumflex \idieresis \itilde \imacron \ibreve \iogonek 
	\dotlessi \ij ];
    @kc3_second_39 = [\j \jcircumflex \uni0237 ];
    @kc3_second_40 = [\l \lacute \uni013C \lcaron \ldot \lslash ];
    @kc3_second_41 = [\m \n \r \ntilde \kgreenlandic \nacute \uni0146 \ncaron \eng \racute \uni0157 
	\rcaron \uni1E41 ];
    @kc3_second_42 = [\x ];
    @kc3_second_43 = [\z \zacute \zdotaccent \zcaron \uni1E93 ];
    @kc3_second_44 = [];
    @kc3_second_45 = [];
    @kc3_second_46 = [];
    @kc3_second_47 = [];
    @kc3_second_48 = [];
    @kc3_second_49 = [];
    @kc3_second_50 = [];
    @kc3_second_51 = [];
    @kc3_second_52 = [];
    @kc3_second_53 = [\quotedbl \quotesingle ];
    @kc3_second_54 = [\C \G \O \Q \Ccedilla \Ograve \Oacute \Ocircumflex \Otilde \Odieresis \Oslash 
	\Cacute \Ccircumflex \Cdotaccent \Ccaron \Gcircumflex \Gbreve 
	\Gdotaccent \uni0122 \Omacron \Obreve \Ohungarumlaut \OE ];
    @kc3_second_55 = [\U \Ugrave \Uacute \Ucircumflex \Udieresis \Utilde \Umacron \Ubreve \Uring 
	\Uhungarumlaut \Uogonek ];
    @kc3_second_56 = [\quoteleft \quotedblleft ];
    @kc3_second_57 = [\quoteright \quotedblright ];
    @kc3_second_58 = [\zero \zero.prop ];
    @kc3_second_59 = [\S \Sacute \Scircumflex \Scedilla \Scaron \uni1E60 \uni1E62 ];
    @kc3_second_60 = [\p \uni1E57 ];
    @kc3_second_61 = [\t \uni0163 \tcaron \tbar \uni1E6B \uni1E6D \uni1E6F \uni1E97 ];
    @kc3_second_62 = [\u \ugrave \uacute \ucircumflex \udieresis \utilde \umacron \ubreve \uring 
	\uhungarumlaut \uogonek ];
    @kc3_second_63 = [];
    @kc3_second_64 = [\hyphen \endash \emdash \uni2015 ];
    pos @kc3_first_0 @kc3_second_1 -42;
    pos @kc3_first_0 @kc3_second_2 -160;
    pos @kc3_first_0 @kc3_second_3 -82;
    pos @kc3_first_0 @kc3_second_4 -90;
    pos @kc3_first_0 @kc3_second_5 -26;
    pos @kc3_first_0 @kc3_second_6 -42;
    pos @kc3_first_0 @kc3_second_7 -100;
    pos @kc3_first_0 @kc3_second_8 -24;
    pos @kc3_first_0 @kc3_second_9 -40;
    pos @kc3_first_0 @kc3_second_10 -56;
    pos @kc3_first_0 @kc3_second_11 -22;
    pos @kc3_first_0 @kc3_second_12 -30;
    pos @kc3_first_0 @kc3_second_13 -40;
    pos @kc3_first_0 @kc3_second_14 -16;
    pos @kc3_first_0 @kc3_second_15 -152;
    pos @kc3_first_0 @kc3_second_16 -20;
    pos @kc3_first_0 @kc3_second_17 -78;
    pos @kc3_first_0 @kc3_second_18 -134;
    pos @kc3_first_0 @kc3_second_19 -48;
    pos @kc3_first_0 @kc3_second_20 -26;
    pos @kc3_first_1 @kc3_second_7 -80;
    pos @kc3_first_1 @kc3_second_9 -16;
    pos @kc3_first_1 @kc3_second_10 -24;
    pos @kc3_first_1 @kc3_second_15 -134;
    pos @kc3_first_1 @kc3_second_19 -38;
    pos @kc3_first_1 @kc3_second_21 48;
    pos @kc3_first_1 @kc3_second_22 46;
    pos @kc3_first_1 @kc3_second_23 44;
    pos @kc3_first_1 @kc3_second_24 28;
    pos @kc3_first_1 @kc3_second_25 18;
    pos @kc3_first_2 @kc3_second_7 -36;
    pos @kc3_first_2 @kc3_second_15 -64;
    pos @kc3_first_2 @kc3_second_18 -54;
    pos @kc3_first_2 @kc3_second_19 -58;
    pos @kc3_first_2 @kc3_second_21 -40;
    pos @kc3_first_2 @kc3_second_22 -38;
    pos @kc3_first_2 @kc3_second_23 -40;
    pos @kc3_first_2 @kc3_second_24 -34;
    pos @kc3_first_2 @kc3_second_25 -58;
    pos @kc3_first_2 @kc3_second_26 -20;
    pos @kc3_first_2 @kc3_second_27 -16;
    pos @kc3_first_2 @kc3_second_28 -26;
    pos @kc3_first_2 @kc3_second_29 -28;
    pos @kc3_first_2 @kc3_second_30 -124;
    pos @kc3_first_2 @kc3_second_31 -84;
    pos @kc3_first_2 @kc3_second_32 -66;
    pos @kc3_first_2 @kc3_second_33 -60;
    pos @kc3_first_2 @kc3_second_34 -122;
    pos @kc3_first_2 @kc3_second_35 -54;
    pos @kc3_first_2 @kc3_second_36 -22;
    pos @kc3_first_2 @kc3_second_37 -18;
    pos @kc3_first_2 @kc3_second_38 -26;
    pos @kc3_first_2 @kc3_second_39 -16;
    pos @kc3_first_2 @kc3_second_40 -18;
    pos @kc3_first_2 @kc3_second_41 -20;
    pos @kc3_first_2 @kc3_second_42 -52;
    pos @kc3_first_2 @kc3_second_43 -20;
    pos @kc3_first_2 @kc3_second_44 -28;
    pos @kc3_first_2 @kc3_second_45 -24;
    pos @kc3_first_2 @kc3_second_46 -32;
    pos @kc3_first_2 @kc3_second_47 -32;
    pos @kc3_first_2 @kc3_second_48 -42;
    pos @kc3_first_2 @kc3_second_49 -18;
    pos @kc3_first_2 @kc3_second_50 -40;
    pos @kc3_first_2 @kc3_second_51 -66;
    pos @kc3_first_2 @kc3_second_52 -28;
    pos @kc3_first_3 @kc3_second_21 -68;
    pos @kc3_first_3 @kc3_second_22 -68;
    pos @kc3_first_3 @kc3_second_23 -66;
    pos @kc3_first_3 @kc3_second_24 -48;
    pos @kc3_first_3 @kc3_second_25 -32;
    pos @kc3_first_3 @kc3_second_30 -90;
    pos @kc3_first_3 @kc3_second_31 -128;
    pos @kc3_first_3 @kc3_second_32 -106;
    pos @kc3_first_3 @kc3_second_34 -86;
    pos @kc3_first_3 @kc3_second_48 -44;
    pos @kc3_first_3 @kc3_second_50 -62;
    pos @kc3_first_3 @kc3_second_53 -160;
    pos @kc3_first_3 @kc3_second_54 -32;
    pos @kc3_first_3 @kc3_second_55 -46;
    pos @kc3_first_3 @kc3_second_56 -184;
    pos @kc3_first_3 @kc3_second_57 -192;
    pos @kc3_first_4 @kc3_second_7 -96;
    pos @kc3_first_4 @kc3_second_8 -78;
    pos @kc3_first_4 @kc3_second_9 -90;
    pos @kc3_first_4 @kc3_second_10 -84;
    pos @kc3_first_4 @kc3_second_11 -80;
    pos @kc3_first_4 @kc3_second_12 -84;
    pos @kc3_first_4 @kc3_second_14 -72;
    pos @kc3_first_4 @kc3_second_15 -110;
    pos @kc3_first_4 @kc3_second_19 -120;
    pos @kc3_first_4 @kc3_second_20 -80;
    pos @kc3_first_4 @kc3_second_21 -30;
    pos @kc3_first_4 @kc3_second_22 -30;
    pos @kc3_first_4 @kc3_second_23 -30;
    pos @kc3_first_4 @kc3_second_36 -34;
    pos @kc3_first_4 @kc3_second_41 -50;
    pos @kc3_first_4 @kc3_second_42 -22;
    pos @kc3_first_4 @kc3_second_43 -58;
    pos @kc3_first_4 @kc3_second_44 -42;
    pos @kc3_first_4 @kc3_second_45 -40;
    pos @kc3_first_4 @kc3_second_47 -42;
    pos @kc3_first_4 @kc3_second_48 -36;
    pos @kc3_first_4 @kc3_second_49 -36;
    pos @kc3_first_4 @kc3_second_52 -50;
    pos @kc3_first_4 @kc3_second_54 -36;
    pos @kc3_first_4 @kc3_second_58 -32;
    pos @kc3_first_4 @kc3_second_59 -20;
    pos @kc3_first_4 @kc3_second_60 -34;
    pos @kc3_first_4 @kc3_second_61 -32;
    pos @kc3_first_4 @kc3_second_62 -34;
    pos @kc3_first_4 @kc3_second_63 -68;
    pos @kc3_first_5 @kc3_second_30 -70;
    pos @kc3_first_5 @kc3_second_31 -62;
    pos @kc3_first_5 @kc3_second_32 -48;
    pos @kc3_first_5 @kc3_second_34 -76;
    pos @kc3_first_5 @kc3_second_55 -16;
    pos @kc3_first_6 @kc3_second_7 24;
    pos @kc3_first_6 @kc3_second_15 34;
    pos @kc3_first_6 @kc3_second_21 -38;
    pos @kc3_first_6 @kc3_second_22 -36;
    pos @kc3_first_6 @kc3_second_23 -36;
    pos @kc3_first_6 @kc3_second_24 -26;
    pos @kc3_first_6 @kc3_second_25 -20;
    pos @kc3_first_6 @kc3_second_30 -52;
    pos @kc3_first_6 @kc3_second_31 -74;
    pos @kc3_first_6 @kc3_second_32 -64;
    pos @kc3_first_6 @kc3_second_33 20;
    pos @kc3_first_6 @kc3_second_34 -64;
    pos @kc3_first_6 @kc3_second_48 -22;
    pos @kc3_first_6 @kc3_second_53 -82;
    pos @kc3_first_6 @kc3_second_55 -32;
    pos @kc3_first_7 @kc3_second_2 -184;
    pos @kc3_first_7 @kc3_second_7 -98;
    pos @kc3_first_7 @kc3_second_8 -28;
    pos @kc3_first_7 @kc3_second_9 -46;
    pos @kc3_first_7 @kc3_second_10 -60;
    pos @kc3_first_7 @kc3_second_11 -28;
    pos @kc3_first_7 @kc3_second_12 -36;
    pos @kc3_first_7 @kc3_second_13 -46;
    pos @kc3_first_7 @kc3_second_14 -20;
    pos @kc3_first_7 @kc3_second_15 -152;
    pos @kc3_first_7 @kc3_second_16 -24;
    pos @kc3_first_7 @kc3_second_18 -134;
    pos @kc3_first_7 @kc3_second_19 -50;
    pos @kc3_first_7 @kc3_second_20 -30;
    pos @kc3_first_7 @kc3_second_34 16;
    pos @kc3_first_8 @kc3_second_2 -192;
    pos @kc3_first_8 @kc3_second_7 -110;
    pos @kc3_first_8 @kc3_second_8 -42;
    pos @kc3_first_8 @kc3_second_9 -60;
    pos @kc3_first_8 @kc3_second_10 -60;
    pos @kc3_first_8 @kc3_second_11 -40;
    pos @kc3_first_8 @kc3_second_12 -48;
    pos @kc3_first_8 @kc3_second_13 -60;
    pos @kc3_first_8 @kc3_second_14 -34;
    pos @kc3_first_8 @kc3_second_15 -158;
    pos @kc3_first_8 @kc3_second_16 -38;
    pos @kc3_first_8 @kc3_second_17 -112;
    pos @kc3_first_8 @kc3_second_18 -140;
    pos @kc3_first_8 @kc3_second_19 -62;
    pos @kc3_first_8 @kc3_second_20 -42;
    pos @kc3_first_8 @kc3_second_31 32;
    pos @kc3_first_8 @kc3_second_32 28;
    pos @kc3_first_8 @kc3_second_34 54;
    pos @kc3_first_8 @kc3_second_41 -24;
    pos @kc3_first_8 @kc3_second_43 -18;
    pos @kc3_first_8 @kc3_second_63 -24;
    pos @kc3_first_8 @kc3_second_64 -36;
    pos @kc3_first_9 @kc3_second_30 -80;
    pos @kc3_first_9 @kc3_second_31 -64;
    pos @kc3_first_9 @kc3_second_32 -50;
    pos @kc3_first_9 @kc3_second_34 -88;
    pos @kc3_first_10 @kc3_second_7 -42;
    pos @kc3_first_10 @kc3_second_15 -64;
    pos @kc3_first_10 @kc3_second_18 -56;
    pos @kc3_first_10 @kc3_second_19 -44;
    pos @kc3_first_10 @kc3_second_21 -28;
    pos @kc3_first_10 @kc3_second_22 -26;
    pos @kc3_first_10 @kc3_second_23 -26;
    pos @kc3_first_10 @kc3_second_25 -40;
    pos @kc3_first_10 @kc3_second_26 -32;
    pos @kc3_first_10 @kc3_second_27 -28;
    pos @kc3_first_10 @kc3_second_28 -36;
    pos @kc3_first_10 @kc3_second_29 -38;
    pos @kc3_first_10 @kc3_second_30 -124;
    pos @kc3_first_10 @kc3_second_31 -94;
    pos @kc3_first_10 @kc3_second_32 -76;
    pos @kc3_first_10 @kc3_second_33 -62;
    pos @kc3_first_10 @kc3_second_34 -130;
    pos @kc3_first_10 @kc3_second_35 -56;
    pos @kc3_first_10 @kc3_second_42 -30;
    pos @kc3_first_10 @kc3_second_46 -20;
    pos @kc3_first_10 @kc3_second_50 -26;
    pos @kc3_first_10 @kc3_second_51 -38;
    pos @kc3_first_10 @kc3_second_53 -80;
    pos @kc3_first_10 @kc3_second_55 -22;
    pos @kc3_first_10 @kc3_second_57 -74;
  subtable;
    @kc4_first_0 = [\a \agrave \aacute \acircumflex \atilde \adieresis \aring \amacron \abreve 
	\aogonek ];
    @kc4_first_1 = [\b \uni1E03 \f_b \f_f_b ];
    @kc4_first_2 = [\c \ccedilla \cacute \ccircumflex \cdotaccent \ccaron ];
    @kc4_first_3 = [\d \dcaron \dcroat \uni1E0B \uni1E0D \uni1E0F \uni1E11 ];
    @kc4_first_4 = [\e \ae \egrave \eacute \ecircumflex \edieresis \emacron \ebreve \edotaccent 
	\eogonek \ecaron \oe ];
    @kc4_first_5 = [\f \uni1E1F \f_f ];
    @kc4_first_6 = [\g \gcircumflex \gbreve \gdotaccent \uni0123 ];
    @kc4_first_7 = [\i \igrave \iacute \icircumflex \idieresis \itilde \imacron \ibreve \iogonek 
	\dotlessi \f_i \f_f_i ];
    @kc4_first_8 = [\j \ij \jcircumflex \uni0237 \f_j \f_f_j ];
    @kc4_first_9 = [\k \uni0137 \kgreenlandic \f_k \f_f_k ];
    @kc4_first_10 = [\l \lacute \uni013C \lcaron \lslash \f_l \f_f_l ];
    @kc4_first_11 = [\h \m \n \ntilde \hcircumflex \hbar \nacute \uni0146 \ncaron \uni1E25 \uni1E29 
	\uni1E2B \uni1E41 \uni1E96 \f_h \f_f_h ];
    @kc4_first_12 = [\o \ograve \oacute \ocircumflex \otilde \odieresis \oslash \omacron \obreve 
	\ohungarumlaut ];
    @kc4_first_13 = [\p \thorn \uni1E57 ];
    @kc4_first_14 = [\q ];
    @kc4_first_15 = [\r \racute \uni0157 \rcaron ];
    @kc4_first_16 = [\s \sacute \scircumflex \scedilla \scaron \uni1E61 \uni1E63 ];
    @kc4_first_17 = [\t \uni0163 \tcaron \tbar \uni1E6B \uni1E6D \uni1E6F \uni1E97 ];
    @kc4_first_18 = [\u \ugrave \uacute \ucircumflex \udieresis \utilde \umacron \ubreve \uring 
	\uhungarumlaut \uogonek ];
    @kc4_first_19 = [\v ];
    @kc4_first_20 = [\w \wcircumflex \wgrave \wacute \wdieresis ];
    @kc4_first_21 = [\x ];
    @kc4_first_22 = [\y \yacute \ydieresis \ycircumflex \ygrave ];
    @kc4_first_23 = [\z \zacute \zdotaccent \zcaron \uni1E93 ];
    @kc4_first_24 = [\eth ];
    @kc4_first_25 = [\ldot ];
    @kc4_first_26 = [\eng ];
    @kc4_first_27 = [];
    @kc4_first_28 = [];
    @kc4_second_0 = [];
    @kc4_second_1 = [\parenright ];
    @kc4_second_2 = [\C \G \O \Q \Ccedilla \Ograve \Oacute \Ocircumflex \Otilde \Odieresis \Oslash 
	\Cacute \Ccircumflex \Cdotaccent \Ccaron \Gcircumflex \Gbreve 
	\Gdotaccent \uni0122 \Omacron \Obreve \Ohungarumlaut \OE ];
    @kc4_second_3 = [\U \Ugrave \Uacute \Ucircumflex \Udieresis \Utilde \Umacron \Ubreve \Uring 
	\Uhungarumlaut \Uogonek ];
    @kc4_second_4 = [\Y \Yacute \Ycircumflex \Ydieresis \Ygrave ];
    @kc4_second_5 = [\backslash ];
    @kc4_second_6 = [\bracketright ];
    @kc4_second_7 = [\v ];
    @kc4_second_8 = [\w \wcircumflex \wgrave \wacute \wdieresis ];
    @kc4_second_9 = [\y \yacute \ydieresis \ycircumflex \ygrave ];
    @kc4_second_10 = [\quotedbl \quotesingle ];
    @kc4_second_11 = [\asterisk ];
    @kc4_second_12 = [\A \Agrave \Aacute \Acircumflex \Atilde \Adieresis \Aring \Amacron \Abreve 
	\Aogonek ];
    @kc4_second_13 = [\B \D \E \F \H \I \K \L \P \R \Egrave \Eacute \Ecircumflex \Edieresis \Igrave \Iacute 
	\Icircumflex \Idieresis \Eth \Thorn \Dcaron \Dcroat \Emacron \Ebreve 
	\Edotaccent \Eogonek \Ecaron \Hcircumflex \Hbar \Itilde \Imacron 
	\Ibreve \Iogonek \Idotaccent \IJ \uni0136 \Lacute \uni013B \Lcaron \Ldot 
	\Lslash \Racute \uni0156 \Rcaron \uni1E02 \uni1E0A \uni1E0C \uni1E0E 
	\uni1E10 \uni1E1E \uni1E24 \uni1E28 \uni1E2A \uni1E56 ];
    @kc4_second_14 = [\x ];
    @kc4_second_15 = [\quoteleft \quotedblleft ];
    @kc4_second_16 = [\quoteright \quotedblright ];
    @kc4_second_17 = [];
    @kc4_second_18 = [\hyphen \endash \emdash \uni2015 ];
    @kc4_second_19 = [\d \dcaron \dcroat \uni1E0B \uni1E0D \uni1E0F \uni1E11 ];
    @kc4_second_20 = [\c \ccedilla \cacute \ccircumflex \cdotaccent \ccaron ];
    @kc4_second_21 = [\g \gcircumflex \gbreve \gdotaccent \uni0123 ];
    @kc4_second_22 = [\e \o \egrave \eacute \ecircumflex \edieresis \eth \ograve \oacute \ocircumflex 
	\otilde \odieresis \oslash \emacron \ebreve \edotaccent \eogonek 
	\ecaron \omacron \obreve \ohungarumlaut \oe ];
    @kc4_second_23 = [\q ];
    @kc4_second_24 = [\guillemotleft \guilsinglleft ];
    @kc4_second_25 = [\J \Jcircumflex ];
    @kc4_second_26 = [\T \uni0162 \Tcaron \Tbar \uni1E6A \uni1E6C \uni1E6E ];
    @kc4_second_27 = [\V ];
    @kc4_second_28 = [\W \Wcircumflex \Wgrave \Wacute \Wdieresis ];
    @kc4_second_29 = [\registered ];
    @kc4_second_30 = [];
    @kc4_second_31 = [];
    @kc4_second_32 = [\braceright ];
    @kc4_second_33 = [\h \k \hcircumflex \hbar \uni0137 \uni1E25 \uni1E29 \uni1E2B \uni1E96 ];
    @kc4_second_34 = [\l \lacute \uni013C \lcaron \ldot \lslash ];
    @kc4_second_35 = [\j \jcircumflex \uni0237 ];
    @kc4_second_36 = [\ampersand ];
    @kc4_second_37 = [\comma \period \quotesinglbase \quotedblbase \ellipsis ];
    @kc4_second_38 = [\slash ];
    @kc4_second_39 = [\question ];
    @kc4_second_40 = [\a \agrave \aacute \acircumflex \atilde \adieresis \aring \ae \amacron \abreve 
	\aogonek ];
    @kc4_second_41 = [\s \sacute \scircumflex \scedilla \scaron \uni1E61 \uni1E63 ];
    @kc4_second_42 = [\p \uni1E57 ];
    @kc4_second_43 = [\t \uni0163 \tcaron \tbar \uni1E6B \uni1E6D \uni1E6F \uni1E97 ];
    @kc4_second_44 = [\u \ugrave \uacute \ucircumflex \udieresis \utilde \umacron \ubreve \uring 
	\uhungarumlaut \uogonek ];
    pos @kc4_first_0 @kc4_second_1 -26;
    pos @kc4_first_0 @kc4_second_2 -12;
    pos @kc4_first_0 @kc4_second_3 -44;
    pos @kc4_first_0 @kc4_second_4 -150;
    pos @kc4_first_0 @kc4_second_5 -78;
    pos @kc4_first_0 @kc4_second_6 -38;
    pos @kc4_first_0 @kc4_second_7 -22;
    pos @kc4_first_0 @kc4_second_8 -22;
    pos @kc4_first_0 @kc4_second_9 -22;
    pos @kc4_first_1 @kc4_second_1 -60;
    pos @kc4_first_1 @kc4_second_3 -48;
    pos @kc4_first_1 @kc4_second_4 -190;
    pos @kc4_first_1 @kc4_second_5 -76;
    pos @kc4_first_1 @kc4_second_6 -54;
    pos @kc4_first_1 @kc4_second_7 -18;
    pos @kc4_first_1 @kc4_second_8 -18;
    pos @kc4_first_1 @kc4_second_9 -18;
    pos @kc4_first_1 @kc4_second_10 -52;
    pos @kc4_first_1 @kc4_second_11 -22;
    pos @kc4_first_1 @kc4_second_12 -28;
    pos @kc4_first_1 @kc4_second_13 -42;
    pos @kc4_first_1 @kc4_second_14 -50;
    pos @kc4_first_1 @kc4_second_15 -52;
    pos @kc4_first_1 @kc4_second_16 -48;
    pos @kc4_first_1 @kc4_second_17 -26;
    pos @kc4_first_2 @kc4_second_1 -36;
    pos @kc4_first_2 @kc4_second_2 -18;
    pos @kc4_first_2 @kc4_second_3 -40;
    pos @kc4_first_2 @kc4_second_4 -176;
    pos @kc4_first_2 @kc4_second_5 -46;
    pos @kc4_first_2 @kc4_second_6 -40;
    pos @kc4_first_2 @kc4_second_13 -38;
    pos @kc4_first_2 @kc4_second_18 -38;
    pos @kc4_first_2 @kc4_second_19 -8;
    pos @kc4_first_3 @kc4_second_2 -12;
    pos @kc4_first_3 @kc4_second_3 -20;
    pos @kc4_first_4 @kc4_second_1 -48;
    pos @kc4_first_4 @kc4_second_2 -12;
    pos @kc4_first_4 @kc4_second_3 -46;
    pos @kc4_first_4 @kc4_second_4 -174;
    pos @kc4_first_4 @kc4_second_5 -66;
    pos @kc4_first_4 @kc4_second_6 -48;
    pos @kc4_first_4 @kc4_second_13 -28;
    pos @kc4_first_5 @kc4_second_1 38;
    pos @kc4_first_5 @kc4_second_3 38;
    pos @kc4_first_5 @kc4_second_4 118;
    pos @kc4_first_5 @kc4_second_6 16;
    pos @kc4_first_5 @kc4_second_18 -54;
    pos @kc4_first_5 @kc4_second_19 -32;
    pos @kc4_first_5 @kc4_second_20 -32;
    pos @kc4_first_5 @kc4_second_21 -8;
    pos @kc4_first_5 @kc4_second_22 -22;
    pos @kc4_first_5 @kc4_second_23 -30;
    pos @kc4_first_5 @kc4_second_24 -38;
    pos @kc4_first_6 @kc4_second_3 -28;
    pos @kc4_first_6 @kc4_second_4 -146;
    pos @kc4_first_6 @kc4_second_5 -36;
    pos @kc4_first_6 @kc4_second_18 -26;
    pos @kc4_first_6 @kc4_second_19 -8;
    pos @kc4_first_6 @kc4_second_20 -8;
    pos @kc4_first_6 @kc4_second_23 -8;
    pos @kc4_first_7 @kc4_second_2 -20;
    pos @kc4_first_7 @kc4_second_3 -40;
    pos @kc4_first_7 @kc4_second_4 -24;
    pos @kc4_first_7 @kc4_second_5 -22;
    pos @kc4_first_7 @kc4_second_6 -30;
    pos @kc4_first_7 @kc4_second_18 -20;
    pos @kc4_first_8 @kc4_second_3 -24;
    pos @kc4_first_8 @kc4_second_4 -16;
    pos @kc4_first_8 @kc4_second_13 -16;
    pos @kc4_first_9 @kc4_second_2 -56;
    pos @kc4_first_9 @kc4_second_3 -42;
    pos @kc4_first_9 @kc4_second_4 -146;
    pos @kc4_first_9 @kc4_second_5 -30;
    pos @kc4_first_9 @kc4_second_12 14;
    pos @kc4_first_9 @kc4_second_18 -86;
    pos @kc4_first_9 @kc4_second_19 -88;
    pos @kc4_first_9 @kc4_second_20 -92;
    pos @kc4_first_9 @kc4_second_21 -16;
    pos @kc4_first_9 @kc4_second_22 -88;
    pos @kc4_first_9 @kc4_second_23 -82;
    pos @kc4_first_9 @kc4_second_24 -52;
    pos @kc4_first_10 @kc4_second_2 -20;
    pos @kc4_first_10 @kc4_second_3 -30;
    pos @kc4_first_10 @kc4_second_4 -10;
    pos @kc4_first_10 @kc4_second_7 -10;
    pos @kc4_first_10 @kc4_second_8 -10;
    pos @kc4_first_10 @kc4_second_9 -12;
    pos @kc4_first_10 @kc4_second_18 -16;
    pos @kc4_first_11 @kc4_second_1 -22;
    pos @kc4_first_11 @kc4_second_2 -20;
    pos @kc4_first_11 @kc4_second_3 -52;
    pos @kc4_first_11 @kc4_second_4 -148;
    pos @kc4_first_11 @kc4_second_5 -84;
    pos @kc4_first_11 @kc4_second_6 -40;
    pos @kc4_first_11 @kc4_second_7 -26;
    pos @kc4_first_11 @kc4_second_8 -26;
    pos @kc4_first_11 @kc4_second_9 -26;
    pos @kc4_first_11 @kc4_second_10 -20;
    pos @kc4_first_11 @kc4_second_15 -18;
    pos @kc4_first_11 @kc4_second_16 -18;
    pos @kc4_first_11 @kc4_second_17 -16;
    pos @kc4_first_11 @kc4_second_25 -38;
    pos @kc4_first_11 @kc4_second_26 -120;
    pos @kc4_first_11 @kc4_second_27 -128;
    pos @kc4_first_11 @kc4_second_28 -96;
    pos @kc4_first_11 @kc4_second_29 -20;
    pos @kc4_first_11 @kc4_second_30 -24;
    pos @kc4_first_11 @kc4_second_31 18;
    pos @kc4_first_12 @kc4_second_1 -62;
    pos @kc4_first_12 @kc4_second_3 -50;
    pos @kc4_first_12 @kc4_second_4 -190;
    pos @kc4_first_12 @kc4_second_5 -80;
    pos @kc4_first_12 @kc4_second_6 -56;
    pos @kc4_first_12 @kc4_second_7 -22;
    pos @kc4_first_12 @kc4_second_8 -20;
    pos @kc4_first_12 @kc4_second_9 -22;
    pos @kc4_first_12 @kc4_second_10 -30;
    pos @kc4_first_12 @kc4_second_11 -16;
    pos @kc4_first_12 @kc4_second_12 -20;
    pos @kc4_first_12 @kc4_second_13 -40;
    pos @kc4_first_12 @kc4_second_14 -48;
    pos @kc4_first_12 @kc4_second_15 -30;
    pos @kc4_first_12 @kc4_second_16 -30;
    pos @kc4_first_12 @kc4_second_17 -16;
    pos @kc4_first_12 @kc4_second_32 -20;
    pos @kc4_first_13 @kc4_second_1 -64;
    pos @kc4_first_13 @kc4_second_3 -50;
    pos @kc4_first_13 @kc4_second_4 -192;
    pos @kc4_first_13 @kc4_second_5 -78;
    pos @kc4_first_13 @kc4_second_6 -58;
    pos @kc4_first_13 @kc4_second_7 -18;
    pos @kc4_first_13 @kc4_second_8 -18;
    pos @kc4_first_13 @kc4_second_9 -16;
    pos @kc4_first_13 @kc4_second_10 -26;
    pos @kc4_first_13 @kc4_second_12 -30;
    pos @kc4_first_13 @kc4_second_13 -42;
    pos @kc4_first_13 @kc4_second_14 -50;
    pos @kc4_first_13 @kc4_second_15 -26;
    pos @kc4_first_13 @kc4_second_16 -26;
    pos @kc4_first_13 @kc4_second_32 -22;
    pos @kc4_first_13 @kc4_second_33 -8;
    pos @kc4_first_13 @kc4_second_34 -8;
    pos @kc4_first_14 @kc4_second_3 -36;
    pos @kc4_first_14 @kc4_second_4 -146;
    pos @kc4_first_14 @kc4_second_13 -22;
    pos @kc4_first_14 @kc4_second_35 86;
    pos @kc4_first_15 @kc4_second_1 -50;
    pos @kc4_first_15 @kc4_second_3 -30;
    pos @kc4_first_15 @kc4_second_4 -158;
    pos @kc4_first_15 @kc4_second_5 -40;
    pos @kc4_first_15 @kc4_second_6 -46;
    pos @kc4_first_15 @kc4_second_12 -48;
    pos @kc4_first_15 @kc4_second_13 -34;
    pos @kc4_first_15 @kc4_second_18 -48;
    pos @kc4_first_15 @kc4_second_19 -16;
    pos @kc4_first_15 @kc4_second_20 -16;
    pos @kc4_first_15 @kc4_second_22 -8;
    pos @kc4_first_15 @kc4_second_23 -16;
    pos @kc4_first_15 @kc4_second_36 -32;
    pos @kc4_first_15 @kc4_second_37 -24;
    pos @kc4_first_15 @kc4_second_38 -20;
    pos @kc4_first_16 @kc4_second_1 -50;
    pos @kc4_first_16 @kc4_second_3 -54;
    pos @kc4_first_16 @kc4_second_4 -150;
    pos @kc4_first_16 @kc4_second_5 -64;
    pos @kc4_first_16 @kc4_second_6 -52;
    pos @kc4_first_16 @kc4_second_13 -24;
    pos @kc4_first_17 @kc4_second_1 -26;
    pos @kc4_first_17 @kc4_second_3 -20;
    pos @kc4_first_17 @kc4_second_4 -106;
    pos @kc4_first_17 @kc4_second_5 -30;
    pos @kc4_first_17 @kc4_second_6 -34;
    pos @kc4_first_17 @kc4_second_18 -38;
    pos @kc4_first_17 @kc4_second_24 -40;
    pos @kc4_first_18 @kc4_second_1 -32;
    pos @kc4_first_18 @kc4_second_2 -16;
    pos @kc4_first_18 @kc4_second_3 -44;
    pos @kc4_first_18 @kc4_second_4 -134;
    pos @kc4_first_18 @kc4_second_5 -68;
    pos @kc4_first_18 @kc4_second_6 -44;
    pos @kc4_first_18 @kc4_second_18 -18;
    pos @kc4_first_19 @kc4_second_1 -52;
    pos @kc4_first_19 @kc4_second_3 -22;
    pos @kc4_first_19 @kc4_second_4 -134;
    pos @kc4_first_19 @kc4_second_5 -28;
    pos @kc4_first_19 @kc4_second_6 -46;
    pos @kc4_first_19 @kc4_second_12 -86;
    pos @kc4_first_19 @kc4_second_13 -36;
    pos @kc4_first_19 @kc4_second_18 -46;
    pos @kc4_first_19 @kc4_second_19 -40;
    pos @kc4_first_19 @kc4_second_20 -40;
    pos @kc4_first_19 @kc4_second_21 -16;
    pos @kc4_first_19 @kc4_second_22 -26;
    pos @kc4_first_19 @kc4_second_23 -40;
    pos @kc4_first_19 @kc4_second_24 -32;
    pos @kc4_first_19 @kc4_second_33 -10;
    pos @kc4_first_19 @kc4_second_34 -10;
    pos @kc4_first_19 @kc4_second_36 -34;
    pos @kc4_first_19 @kc4_second_37 -70;
    pos @kc4_first_19 @kc4_second_38 -40;
    pos @kc4_first_19 @kc4_second_39 -22;
    pos @kc4_first_19 @kc4_second_40 -16;
    pos @kc4_first_19 @kc4_second_41 -10;
    pos @kc4_first_20 @kc4_second_1 -52;
    pos @kc4_first_20 @kc4_second_3 -24;
    pos @kc4_first_20 @kc4_second_4 -136;
    pos @kc4_first_20 @kc4_second_5 -28;
    pos @kc4_first_20 @kc4_second_6 -48;
    pos @kc4_first_20 @kc4_second_12 -86;
    pos @kc4_first_20 @kc4_second_13 -36;
    pos @kc4_first_20 @kc4_second_18 -48;
    pos @kc4_first_20 @kc4_second_19 -42;
    pos @kc4_first_20 @kc4_second_20 -42;
    pos @kc4_first_20 @kc4_second_21 -18;
    pos @kc4_first_20 @kc4_second_22 -28;
    pos @kc4_first_20 @kc4_second_23 -44;
    pos @kc4_first_20 @kc4_second_24 -34;
    pos @kc4_first_20 @kc4_second_33 -12;
    pos @kc4_first_20 @kc4_second_34 -12;
    pos @kc4_first_20 @kc4_second_36 -36;
    pos @kc4_first_20 @kc4_second_37 -70;
    pos @kc4_first_20 @kc4_second_38 -42;
    pos @kc4_first_20 @kc4_second_39 -24;
    pos @kc4_first_20 @kc4_second_40 -16;
    pos @kc4_first_20 @kc4_second_41 -10;
    pos @kc4_first_21 @kc4_second_2 -46;
    pos @kc4_first_21 @kc4_second_3 -30;
    pos @kc4_first_21 @kc4_second_4 -132;
    pos @kc4_first_21 @kc4_second_5 -24;
    pos @kc4_first_21 @kc4_second_18 -58;
    pos @kc4_first_21 @kc4_second_19 -52;
    pos @kc4_first_21 @kc4_second_20 -52;
    pos @kc4_first_21 @kc4_second_21 -14;
    pos @kc4_first_21 @kc4_second_22 -48;
    pos @kc4_first_21 @kc4_second_23 -54;
    pos @kc4_first_21 @kc4_second_24 -38;
    pos @kc4_first_22 @kc4_second_1 -50;
    pos @kc4_first_22 @kc4_second_3 -24;
    pos @kc4_first_22 @kc4_second_4 -136;
    pos @kc4_first_22 @kc4_second_5 -26;
    pos @kc4_first_22 @kc4_second_6 -44;
    pos @kc4_first_22 @kc4_second_12 -86;
    pos @kc4_first_22 @kc4_second_13 -36;
    pos @kc4_first_22 @kc4_second_18 -46;
    pos @kc4_first_22 @kc4_second_19 -40;
    pos @kc4_first_22 @kc4_second_20 -40;
    pos @kc4_first_22 @kc4_second_21 -16;
    pos @kc4_first_22 @kc4_second_22 -26;
    pos @kc4_first_22 @kc4_second_23 -40;
    pos @kc4_first_22 @kc4_second_24 -32;
    pos @kc4_first_22 @kc4_second_33 -12;
    pos @kc4_first_22 @kc4_second_34 -12;
    pos @kc4_first_22 @kc4_second_36 -36;
    pos @kc4_first_22 @kc4_second_37 -70;
    pos @kc4_first_22 @kc4_second_38 -40;
    pos @kc4_first_22 @kc4_second_39 -22;
    pos @kc4_first_22 @kc4_second_40 -16;
    pos @kc4_first_22 @kc4_second_41 -10;
    pos @kc4_first_23 @kc4_second_1 -42;
    pos @kc4_first_23 @kc4_second_3 -50;
    pos @kc4_first_23 @kc4_second_4 -162;
    pos @kc4_first_23 @kc4_second_5 -52;
    pos @kc4_first_23 @kc4_second_6 -48;
    pos @kc4_first_23 @kc4_second_13 -16;
    pos @kc4_first_23 @kc4_second_18 -34;
    pos @kc4_first_23 @kc4_second_24 -26;
    pos @kc4_first_24 @kc4_second_10 -20;
    pos @kc4_first_24 @kc4_second_15 -18;
    pos @kc4_first_24 @kc4_second_16 -16;
    pos @kc4_first_24 @kc4_second_33 -14;
    pos @kc4_first_25 @kc4_second_10 -36;
    pos @kc4_first_25 @kc4_second_15 -36;
    pos @kc4_first_25 @kc4_second_16 -34;
    pos @kc4_first_25 @kc4_second_37 -44;
    pos @kc4_first_26 @kc4_second_10 -24;
    pos @kc4_first_26 @kc4_second_15 -24;
    pos @kc4_first_26 @kc4_second_16 -22;
    pos @kc4_first_27 @kc4_second_7 88;
    pos @kc4_first_27 @kc4_second_8 88;
    pos @kc4_first_27 @kc4_second_9 88;
    pos @kc4_first_27 @kc4_second_11 80;
    pos @kc4_first_27 @kc4_second_14 44;
    pos @kc4_first_27 @kc4_second_35 18;
    pos @kc4_first_27 @kc4_second_42 26;
    pos @kc4_first_27 @kc4_second_43 30;
    pos @kc4_first_27 @kc4_second_44 54;
    pos @kc4_first_28 @kc4_second_7 36;
    pos @kc4_first_28 @kc4_second_8 36;
    pos @kc4_first_28 @kc4_second_9 36;
    pos @kc4_first_28 @kc4_second_11 92;
    pos @kc4_first_28 @kc4_second_16 34;
    pos @kc4_first_28 @kc4_second_18 -22;
    pos @kc4_first_28 @kc4_second_19 -8;
    pos @kc4_first_28 @kc4_second_20 -8;
    pos @kc4_first_28 @kc4_second_23 -8;
  subtable;
    @kc5_first_0 = [\A \Agrave \Aacute \Acircumflex \Atilde \Adieresis \Aring \Amacron \Abreve 
	\Aogonek ];
    @kc5_first_1 = [\B \uni1E02 ];
    @kc5_first_2 = [\C \Ccedilla \Cacute \Ccircumflex \Cdotaccent \Ccaron ];
    @kc5_first_3 = [\E \AE \Egrave \Eacute \Ecircumflex \Edieresis \Emacron \Ebreve \Edotaccent 
	\Eogonek \Ecaron \OE ];
    @kc5_first_4 = [\F \uni1E1E ];
    @kc5_first_5 = [\G \Gcircumflex \Gbreve \Gdotaccent \uni0122 ];
    @kc5_first_6 = [\H \I \Igrave \Iacute \Icircumflex \Idieresis \Hcircumflex \Hbar \Itilde \Imacron 
	\Ibreve \Iogonek \Idotaccent \uni1E24 \uni1E28 \uni1E2A ];
    @kc5_first_7 = [\J \IJ \Jcircumflex ];
    @kc5_first_8 = [\K \uni0136 ];
    @kc5_first_9 = [\L \Lacute \uni013B \Lcaron \Ldot \Lslash ];
    @kc5_first_10 = [\M \uni1E40 ];
    @kc5_first_11 = [\N \Ntilde \Nacute \uni0145 \Ncaron \Eng ];
    @kc5_first_12 = [\D \O \Q \Eth \Ograve \Oacute \Ocircumflex \Otilde \Odieresis \Oslash \Dcaron 
	\Dcroat \Omacron \Obreve \Ohungarumlaut \uni1E0A \uni1E0C \uni1E0E 
	\uni1E10 ];
    @kc5_first_13 = [\P \uni1E56 ];
    @kc5_first_14 = [\R \Racute \uni0156 \Rcaron ];
    @kc5_first_15 = [\S \Sacute \Scircumflex \Scedilla \Scaron \uni1E60 \uni1E62 ];
    @kc5_first_16 = [\T \uni0162 \Tcaron \Tbar \uni1E6A \uni1E6C \uni1E6E ];
    @kc5_first_17 = [\U \Ugrave \Uacute \Ucircumflex \Udieresis \Utilde \Umacron \Ubreve \Uring 
	\Uhungarumlaut \Uogonek ];
    @kc5_first_18 = [\V ];
    @kc5_first_19 = [\W \Wcircumflex \Wgrave \Wacute \Wdieresis ];
    @kc5_first_20 = [\X ];
    @kc5_first_21 = [\Y \Yacute \Ycircumflex \Ydieresis \Ygrave ];
    @kc5_first_22 = [\Z \Zacute \Zdotaccent \Zcaron \uni1E92 ];
    @kc5_first_23 = [\Thorn ];
    @kc5_first_24 = [];
    @kc5_first_25 = [];
    @kc5_second_0 = [];
    @kc5_second_1 = [\quotedbl \quotesingle ];
    @kc5_second_2 = [\asterisk ];
    @kc5_second_3 = [\hyphen \endash \emdash \uni2015 ];
    @kc5_second_4 = [\zero \zero.prop ];
    @kc5_second_5 = [\two.prop ];
    @kc5_second_6 = [\J \Jcircumflex ];
    @kc5_second_7 = [\C \G \O \Q \Ccedilla \Ograve \Oacute \Ocircumflex \Otilde \Odieresis \Oslash 
	\Cacute \Ccircumflex \Cdotaccent \Ccaron \Gcircumflex \Gbreve 
	\Gdotaccent \uni0122 \Omacron \Obreve \Ohungarumlaut \OE ];
    @kc5_second_8 = [\T \uni0162 \Tcaron \Tbar \uni1E6A \uni1E6C \uni1E6E ];
    @kc5_second_9 = [\U \Ugrave \Uacute \Ucircumflex \Udieresis \Utilde \Umacron \Ubreve \Uring 
	\Uhungarumlaut \Uogonek ];
    @kc5_second_10 = [\V ];
    @kc5_second_11 = [\W \Wcircumflex \Wgrave \Wacute \Wdieresis ];
    @kc5_second_12 = [\Y \Yacute \Ycircumflex \Ydieresis \Ygrave ];
    @kc5_second_13 = [\backslash ];
    @kc5_second_14 = [\bracketright ];
    @kc5_second_15 = [\c \ccedilla \cacute \ccircumflex \cdotaccent \ccaron ];
    @kc5_second_16 = [\d \dcaron \dcroat \uni1E0B \uni1E0D \uni1E0F \uni1E11 ];
    @kc5_second_17 = [\e \o \egrave \eacute \ecircumflex \edieresis \eth \ograve \oacute \ocircumflex 
	\otilde \odieresis \oslash \emacron \ebreve \edotaccent \eogonek 
	\ecaron \omacron \obreve \ohungarumlaut \oe ];
    @kc5_second_18 = [\q ];
    @kc5_second_19 = [\t \uni0163 \tcaron \tbar \uni1E6B \uni1E6D \uni1E6F \uni1E97 ];
    @kc5_second_20 = [\u \ugrave \uacute \ucircumflex \udieresis \utilde \umacron \ubreve \uring 
	\uhungarumlaut \uogonek ];
    @kc5_second_21 = [\v ];
    @kc5_second_22 = [\w \wcircumflex \wgrave \wacute \wdieresis ];
    @kc5_second_23 = [\y \yacute \ydieresis \ycircumflex \ygrave ];
    @kc5_second_24 = [\registered ];
    @kc5_second_25 = [\quoteleft \quotedblleft ];
    @kc5_second_26 = [\quoteright \quotedblright ];
    @kc5_second_27 = [\guillemotleft \guilsinglleft ];
    @kc5_second_28 = [];
    @kc5_second_29 = [];
    @kc5_second_30 = [];
    @kc5_second_31 = [];
    @kc5_second_32 = [];
    @kc5_second_33 = [];
    @kc5_second_34 = [];
    @kc5_second_35 = [];
    @kc5_second_36 = [\parenright ];
    @kc5_second_37 = [\A \Agrave \Aacute \Acircumflex \Atilde \Adieresis \Aring \Amacron \Abreve 
	\Aogonek ];
    @kc5_second_38 = [\B \D \E \F \H \I \K \L \P \R \Egrave \Eacute \Ecircumflex \Edieresis \Igrave \Iacute 
	\Icircumflex \Idieresis \Eth \Thorn \Dcaron \Dcroat \Emacron \Ebreve 
	\Edotaccent \Eogonek \Ecaron \Hcircumflex \Hbar \Itilde \Imacron 
	\Ibreve \Iogonek \Idotaccent \IJ \uni0136 \Lacute \uni013B \Lcaron \Ldot 
	\Lslash \Racute \uni0156 \Rcaron \uni1E02 \uni1E0A \uni1E0C \uni1E0E 
	\uni1E10 \uni1E1E \uni1E24 \uni1E28 \uni1E2A \uni1E56 ];
    @kc5_second_39 = [\M \uni1E40 ];
    @kc5_second_40 = [\N \Ntilde \Nacute \uni0145 \Ncaron \Eng ];
    @kc5_second_41 = [\X ];
    @kc5_second_42 = [\f \germandbls \longs \uni1E1F \f_f \f_i \f_f_i \f_l \f_f_l \f_b \f_f_b \f_k \f_f_k 
	\f_h \f_f_h \f_j \f_f_j ];
    @kc5_second_43 = [\h \k \hcircumflex \hbar \uni0137 \uni1E25 \uni1E29 \uni1E2B \uni1E96 ];
    @kc5_second_44 = [\i \igrave \iacute \icircumflex \idieresis \itilde \imacron \ibreve \iogonek 
	\dotlessi \ij ];
    @kc5_second_45 = [\j \jcircumflex \uni0237 ];
    @kc5_second_46 = [\l \lacute \uni013C \lcaron \ldot \lslash ];
    @kc5_second_47 = [\m \n \r \ntilde \kgreenlandic \nacute \uni0146 \ncaron \eng \racute \uni0157 
	\rcaron \uni1E41 ];
    @kc5_second_48 = [\x ];
    @kc5_second_49 = [\z \zacute \zdotaccent \zcaron \uni1E93 ];
    @kc5_second_50 = [\AE ];
    @kc5_second_51 = [];
    @kc5_second_52 = [];
    @kc5_second_53 = [];
    @kc5_second_54 = [];
    @kc5_second_55 = [];
    @kc5_second_56 = [];
    @kc5_second_57 = [];
    @kc5_second_58 = [\ampersand ];
    @kc5_second_59 = [\comma \period \quotesinglbase \quotedblbase \ellipsis ];
    @kc5_second_60 = [\slash ];
    @kc5_second_61 = [\four.prop ];
    @kc5_second_62 = [\colon \semicolon ];
    @kc5_second_63 = [\a \agrave \aacute \acircumflex \atilde \adieresis \aring \ae \amacron \abreve 
	\aogonek ];
    @kc5_second_64 = [\g \gcircumflex \gbreve \gdotaccent \uni0123 ];
    @kc5_second_65 = [\p \uni1E57 ];
    @kc5_second_66 = [\s \sacute \scircumflex \scedilla \scaron \uni1E61 \uni1E63 ];
    @kc5_second_67 = [];
    @kc5_second_68 = [\seven \seven.prop ];
    @kc5_second_69 = [\Z \Zacute \Zdotaccent \Zcaron \uni1E92 ];
    @kc5_second_70 = [\one.prop ];
    @kc5_second_71 = [\three.prop ];
    @kc5_second_72 = [\five.prop ];
    @kc5_second_73 = [\six.prop ];
    @kc5_second_74 = [\guillemotright \guilsinglright ];
    @kc5_second_75 = [\b \thorn \uni1E03 ];
    @kc5_second_76 = [\eight.prop ];
    @kc5_second_77 = [\S \Sacute \Scircumflex \Scedilla \Scaron \uni1E60 \uni1E62 ];
    @kc5_second_78 = [\braceright ];
    pos @kc5_first_0 @kc5_second_1 -102;
    pos @kc5_first_0 @kc5_second_2 -82;
    pos @kc5_first_0 @kc5_second_3 -32;
    pos @kc5_first_0 @kc5_second_4 -20;
    pos @kc5_first_0 @kc5_second_5 24;
    pos @kc5_first_0 @kc5_second_6 -20;
    pos @kc5_first_0 @kc5_second_7 -44;
    pos @kc5_first_0 @kc5_second_8 -104;
    pos @kc5_first_0 @kc5_second_9 -76;
    pos @kc5_first_0 @kc5_second_10 -128;
    pos @kc5_first_0 @kc5_second_11 -108;
    pos @kc5_first_0 @kc5_second_12 -120;
    pos @kc5_first_0 @kc5_second_13 -96;
    pos @kc5_first_0 @kc5_second_14 -32;
    pos @kc5_first_0 @kc5_second_15 -20;
    pos @kc5_first_0 @kc5_second_16 -18;
    pos @kc5_first_0 @kc5_second_17 -22;
    pos @kc5_first_0 @kc5_second_18 -18;
    pos @kc5_first_0 @kc5_second_19 -16;
    pos @kc5_first_0 @kc5_second_20 -24;
    pos @kc5_first_0 @kc5_second_21 -88;
    pos @kc5_first_0 @kc5_second_22 -88;
    pos @kc5_first_0 @kc5_second_23 -86;
    pos @kc5_first_0 @kc5_second_24 -68;
    pos @kc5_first_0 @kc5_second_25 -96;
    pos @kc5_first_0 @kc5_second_26 -90;
    pos @kc5_first_0 @kc5_second_27 -48;
    pos @kc5_first_0 @kc5_second_28 -94;
    pos @kc5_first_0 @kc5_second_29 -10;
    pos @kc5_first_0 @kc5_second_30 -20;
    pos @kc5_first_0 @kc5_second_31 -58;
    pos @kc5_first_0 @kc5_second_32 -36;
    pos @kc5_first_0 @kc5_second_33 -72;
    pos @kc5_first_0 @kc5_second_34 -62;
    pos @kc5_first_0 @kc5_second_35 -58;
    pos @kc5_first_1 @kc5_second_6 -12;
    pos @kc5_first_1 @kc5_second_8 -16;
    pos @kc5_first_1 @kc5_second_9 -12;
    pos @kc5_first_1 @kc5_second_10 -44;
    pos @kc5_first_1 @kc5_second_11 -34;
    pos @kc5_first_1 @kc5_second_12 -56;
    pos @kc5_first_1 @kc5_second_13 -28;
    pos @kc5_first_1 @kc5_second_14 -44;
    pos @kc5_first_1 @kc5_second_19 -10;
    pos @kc5_first_1 @kc5_second_21 -38;
    pos @kc5_first_1 @kc5_second_22 -36;
    pos @kc5_first_1 @kc5_second_23 -36;
    pos @kc5_first_1 @kc5_second_29 -10;
    pos @kc5_first_1 @kc5_second_31 -24;
    pos @kc5_first_1 @kc5_second_33 -16;
    pos @kc5_first_1 @kc5_second_34 -12;
    pos @kc5_first_1 @kc5_second_35 -22;
    pos @kc5_first_1 @kc5_second_36 -40;
    pos @kc5_first_1 @kc5_second_37 -20;
    pos @kc5_first_1 @kc5_second_38 -12;
    pos @kc5_first_1 @kc5_second_39 -14;
    pos @kc5_first_1 @kc5_second_40 -14;
    pos @kc5_first_1 @kc5_second_41 -70;
    pos @kc5_first_1 @kc5_second_42 -16;
    pos @kc5_first_1 @kc5_second_43 -14;
    pos @kc5_first_1 @kc5_second_44 -18;
    pos @kc5_first_1 @kc5_second_45 -12;
    pos @kc5_first_1 @kc5_second_46 -14;
    pos @kc5_first_1 @kc5_second_47 -16;
    pos @kc5_first_1 @kc5_second_48 -50;
    pos @kc5_first_1 @kc5_second_49 -20;
    pos @kc5_first_1 @kc5_second_50 -30;
    pos @kc5_first_1 @kc5_second_51 -28;
    pos @kc5_first_1 @kc5_second_52 -26;
    pos @kc5_first_1 @kc5_second_53 -10;
    pos @kc5_first_1 @kc5_second_54 -14;
    pos @kc5_first_1 @kc5_second_55 -12;
    pos @kc5_first_1 @kc5_second_56 -46;
    pos @kc5_first_1 @kc5_second_57 -12;
    pos @kc5_first_2 @kc5_second_21 -30;
    pos @kc5_first_2 @kc5_second_22 -28;
    pos @kc5_first_2 @kc5_second_23 -28;
    pos @kc5_first_2 @kc5_second_31 -20;
    pos @kc5_first_2 @kc5_second_44 -10;
    pos @kc5_first_3 @kc5_second_10 -10;
    pos @kc5_first_3 @kc5_second_11 -10;
    pos @kc5_first_3 @kc5_second_12 -12;
    pos @kc5_first_3 @kc5_second_14 -24;
    pos @kc5_first_3 @kc5_second_19 -12;
    pos @kc5_first_3 @kc5_second_20 -10;
    pos @kc5_first_3 @kc5_second_21 -52;
    pos @kc5_first_3 @kc5_second_22 -50;
    pos @kc5_first_3 @kc5_second_23 -50;
    pos @kc5_first_3 @kc5_second_29 -12;
    pos @kc5_first_3 @kc5_second_31 -40;
    pos @kc5_first_3 @kc5_second_32 -10;
    pos @kc5_first_3 @kc5_second_33 -32;
    pos @kc5_first_3 @kc5_second_34 -22;
    pos @kc5_first_3 @kc5_second_35 -30;
    pos @kc5_first_3 @kc5_second_45 -10;
    pos @kc5_first_3 @kc5_second_49 -12;
    pos @kc5_first_4 @kc5_second_3 -18;
    pos @kc5_first_4 @kc5_second_15 -56;
    pos @kc5_first_4 @kc5_second_16 -54;
    pos @kc5_first_4 @kc5_second_17 -48;
    pos @kc5_first_4 @kc5_second_18 -54;
    pos @kc5_first_4 @kc5_second_19 -12;
    pos @kc5_first_4 @kc5_second_20 -16;
    pos @kc5_first_4 @kc5_second_21 -16;
    pos @kc5_first_4 @kc5_second_22 -16;
    pos @kc5_first_4 @kc5_second_23 -16;
    pos @kc5_first_4 @kc5_second_27 -22;
    pos @kc5_first_4 @kc5_second_29 -22;
    pos @kc5_first_4 @kc5_second_30 -34;
    pos @kc5_first_4 @kc5_second_32 -16;
    pos @kc5_first_4 @kc5_second_37 -96;
    pos @kc5_first_4 @kc5_second_42 -22;
    pos @kc5_first_4 @kc5_second_44 -10;
    pos @kc5_first_4 @kc5_second_47 -34;
    pos @kc5_first_4 @kc5_second_48 -28;
    pos @kc5_first_4 @kc5_second_49 -42;
    pos @kc5_first_4 @kc5_second_50 -142;
    pos @kc5_first_4 @kc5_second_51 -112;
    pos @kc5_first_4 @kc5_second_52 -142;
    pos @kc5_first_4 @kc5_second_53 -26;
    pos @kc5_first_4 @kc5_second_54 -38;
    pos @kc5_first_4 @kc5_second_55 -26;
    pos @kc5_first_4 @kc5_second_56 -12;
    pos @kc5_first_4 @kc5_second_57 -24;
    pos @kc5_first_4 @kc5_second_58 -22;
    pos @kc5_first_4 @kc5_second_59 -96;
    pos @kc5_first_4 @kc5_second_60 -42;
    pos @kc5_first_4 @kc5_second_61 -26;
    pos @kc5_first_4 @kc5_second_62 -16;
    pos @kc5_first_4 @kc5_second_63 -62;
    pos @kc5_first_4 @kc5_second_64 -54;
    pos @kc5_first_4 @kc5_second_65 -12;
    pos @kc5_first_4 @kc5_second_66 -52;
    pos @kc5_first_4 @kc5_second_67 -40;
    pos @kc5_first_5 @kc5_second_8 -30;
    pos @kc5_first_5 @kc5_second_10 -38;
    pos @kc5_first_5 @kc5_second_11 -26;
    pos @kc5_first_5 @kc5_second_12 -54;
    pos @kc5_first_5 @kc5_second_13 -20;
    pos @kc5_first_5 @kc5_second_14 -34;
    pos @kc5_first_5 @kc5_second_21 -22;
    pos @kc5_first_5 @kc5_second_22 -20;
    pos @kc5_first_5 @kc5_second_23 -22;
    pos @kc5_first_5 @kc5_second_33 -10;
    pos @kc5_first_5 @kc5_second_35 -16;
    pos @kc5_first_5 @kc5_second_36 -32;
    pos @kc5_first_5 @kc5_second_37 -12;
    pos @kc5_first_5 @kc5_second_48 -22;
    pos @kc5_first_5 @kc5_second_50 -10;
    pos @kc5_first_5 @kc5_second_51 -10;
    pos @kc5_first_5 @kc5_second_52 -18;
    pos @kc5_first_5 @kc5_second_54 -10;
    pos @kc5_first_5 @kc5_second_56 -12;
    pos @kc5_first_6 @kc5_second_3 -22;
    pos @kc5_first_6 @kc5_second_7 -18;
    pos @kc5_first_6 @kc5_second_15 -42;
    pos @kc5_first_6 @kc5_second_16 -40;
    pos @kc5_first_6 @kc5_second_17 -40;
    pos @kc5_first_6 @kc5_second_18 -40;
    pos @kc5_first_6 @kc5_second_19 -30;
    pos @kc5_first_6 @kc5_second_20 -26;
    pos @kc5_first_6 @kc5_second_21 -34;
    pos @kc5_first_6 @kc5_second_22 -34;
    pos @kc5_first_6 @kc5_second_23 -34;
    pos @kc5_first_6 @kc5_second_27 -36;
    pos @kc5_first_6 @kc5_second_29 -22;
    pos @kc5_first_6 @kc5_second_30 -32;
    pos @kc5_first_6 @kc5_second_31 -26;
    pos @kc5_first_6 @kc5_second_32 -28;
    pos @kc5_first_6 @kc5_second_33 -22;
    pos @kc5_first_6 @kc5_second_34 -16;
    pos @kc5_first_6 @kc5_second_35 -16;
    pos @kc5_first_6 @kc5_second_45 -10;
    pos @kc5_first_6 @kc5_second_63 -18;
    pos @kc5_first_6 @kc5_second_64 -24;
    pos @kc5_first_6 @kc5_second_65 -14;
    pos @kc5_first_6 @kc5_second_66 -14;
    pos @kc5_first_6 @kc5_second_67 -10;
    pos @kc5_first_7 @kc5_second_3 -16;
    pos @kc5_first_7 @kc5_second_7 -18;
    pos @kc5_first_7 @kc5_second_15 -50;
    pos @kc5_first_7 @kc5_second_16 -48;
    pos @kc5_first_7 @kc5_second_17 -44;
    pos @kc5_first_7 @kc5_second_18 -48;
    pos @kc5_first_7 @kc5_second_19 -40;
    pos @kc5_first_7 @kc5_second_20 -32;
    pos @kc5_first_7 @kc5_second_21 -32;
    pos @kc5_first_7 @kc5_second_22 -32;
    pos @kc5_first_7 @kc5_second_23 -32;
    pos @kc5_first_7 @kc5_second_27 -32;
    pos @kc5_first_7 @kc5_second_29 -32;
    pos @kc5_first_7 @kc5_second_30 -34;
    pos @kc5_first_7 @kc5_second_31 -26;
    pos @kc5_first_7 @kc5_second_32 -32;
    pos @kc5_first_7 @kc5_second_33 -18;
    pos @kc5_first_7 @kc5_second_34 -14;
    pos @kc5_first_7 @kc5_second_35 -16;
    pos @kc5_first_7 @kc5_second_37 -20;
    pos @kc5_first_7 @kc5_second_42 -38;
    pos @kc5_first_7 @kc5_second_44 -22;
    pos @kc5_first_7 @kc5_second_45 -14;
    pos @kc5_first_7 @kc5_second_47 -36;
    pos @kc5_first_7 @kc5_second_48 -12;
    pos @kc5_first_7 @kc5_second_49 -44;
    pos @kc5_first_7 @kc5_second_50 -18;
    pos @kc5_first_7 @kc5_second_51 -18;
    pos @kc5_first_7 @kc5_second_52 -38;
    pos @kc5_first_7 @kc5_second_53 -32;
    pos @kc5_first_7 @kc5_second_54 -34;
    pos @kc5_first_7 @kc5_second_55 -30;
    pos @kc5_first_7 @kc5_second_57 -30;
    pos @kc5_first_7 @kc5_second_58 -22;
    pos @kc5_first_7 @kc5_second_59 -16;
    pos @kc5_first_7 @kc5_second_63 -44;
    pos @kc5_first_7 @kc5_second_64 -46;
    pos @kc5_first_7 @kc5_second_65 -28;
    pos @kc5_first_7 @kc5_second_66 -44;
    pos @kc5_first_7 @kc5_second_67 -32;
    pos @kc5_first_8 @kc5_second_3 -72;
    pos @kc5_first_8 @kc5_second_5 24;
    pos @kc5_first_8 @kc5_second_7 -106;
    pos @kc5_first_8 @kc5_second_15 -68;
    pos @kc5_first_8 @kc5_second_16 -62;
    pos @kc5_first_8 @kc5_second_17 -78;
    pos @kc5_first_8 @kc5_second_18 -58;
    pos @kc5_first_8 @kc5_second_19 -18;
    pos @kc5_first_8 @kc5_second_20 -26;
    pos @kc5_first_8 @kc5_second_21 -148;
    pos @kc5_first_8 @kc5_second_22 -148;
    pos @kc5_first_8 @kc5_second_23 -144;
    pos @kc5_first_8 @kc5_second_27 -78;
    pos @kc5_first_8 @kc5_second_30 -72;
    pos @kc5_first_8 @kc5_second_31 -108;
    pos @kc5_first_8 @kc5_second_32 -30;
    pos @kc5_first_8 @kc5_second_33 -130;
    pos @kc5_first_8 @kc5_second_34 -114;
    pos @kc5_first_8 @kc5_second_35 -76;
    pos @kc5_first_8 @kc5_second_64 -10;
    pos @kc5_first_9 @kc5_second_1 -138;
    pos @kc5_first_9 @kc5_second_2 -158;
    pos @kc5_first_9 @kc5_second_6 -12;
    pos @kc5_first_9 @kc5_second_8 -150;
    pos @kc5_first_9 @kc5_second_9 -24;
    pos @kc5_first_9 @kc5_second_10 -156;
    pos @kc5_first_9 @kc5_second_11 -112;
    pos @kc5_first_9 @kc5_second_12 -134;
    pos @kc5_first_9 @kc5_second_13 -82;
    pos @kc5_first_9 @kc5_second_14 -36;
    pos @kc5_first_9 @kc5_second_21 -92;
    pos @kc5_first_9 @kc5_second_22 -92;
    pos @kc5_first_9 @kc5_second_23 -90;
    pos @kc5_first_9 @kc5_second_24 -50;
    pos @kc5_first_9 @kc5_second_25 -142;
    pos @kc5_first_9 @kc5_second_26 -146;
    pos @kc5_first_9 @kc5_second_28 -154;
    pos @kc5_first_9 @kc5_second_31 -90;
    pos @kc5_first_9 @kc5_second_33 -74;
    pos @kc5_first_9 @kc5_second_34 -58;
    pos @kc5_first_9 @kc5_second_35 -78;
    pos @kc5_first_9 @kc5_second_36 -26;
    pos @kc5_first_9 @kc5_second_68 -26;
    pos @kc5_first_10 @kc5_second_3 -22;
    pos @kc5_first_10 @kc5_second_7 -20;
    pos @kc5_first_10 @kc5_second_15 -38;
    pos @kc5_first_10 @kc5_second_16 -36;
    pos @kc5_first_10 @kc5_second_17 -38;
    pos @kc5_first_10 @kc5_second_18 -36;
    pos @kc5_first_10 @kc5_second_19 -28;
    pos @kc5_first_10 @kc5_second_20 -26;
    pos @kc5_first_10 @kc5_second_21 -38;
    pos @kc5_first_10 @kc5_second_22 -40;
    pos @kc5_first_10 @kc5_second_23 -40;
    pos @kc5_first_10 @kc5_second_24 -20;
    pos @kc5_first_10 @kc5_second_27 -38;
    pos @kc5_first_10 @kc5_second_29 -20;
    pos @kc5_first_10 @kc5_second_30 -28;
    pos @kc5_first_10 @kc5_second_31 -28;
    pos @kc5_first_10 @kc5_second_32 -30;
    pos @kc5_first_10 @kc5_second_33 -26;
    pos @kc5_first_10 @kc5_second_34 -20;
    pos @kc5_first_10 @kc5_second_35 -18;
    pos @kc5_first_10 @kc5_second_45 -10;
    pos @kc5_first_10 @kc5_second_63 -14;
    pos @kc5_first_10 @kc5_second_64 -18;
    pos @kc5_first_10 @kc5_second_65 -10;
    pos @kc5_first_10 @kc5_second_66 -10;
    pos @kc5_first_11 @kc5_second_3 -22;
    pos @kc5_first_11 @kc5_second_7 -20;
    pos @kc5_first_11 @kc5_second_15 -50;
    pos @kc5_first_11 @kc5_second_16 -48;
    pos @kc5_first_11 @kc5_second_17 -44;
    pos @kc5_first_11 @kc5_second_18 -48;
    pos @kc5_first_11 @kc5_second_19 -40;
    pos @kc5_first_11 @kc5_second_20 -38;
    pos @kc5_first_11 @kc5_second_21 -38;
    pos @kc5_first_11 @kc5_second_22 -38;
    pos @kc5_first_11 @kc5_second_23 -36;
    pos @kc5_first_11 @kc5_second_27 -38;
    pos @kc5_first_11 @kc5_second_29 -34;
    pos @kc5_first_11 @kc5_second_30 -36;
    pos @kc5_first_11 @kc5_second_31 -32;
    pos @kc5_first_11 @kc5_second_32 -38;
    pos @kc5_first_11 @kc5_second_33 -24;
    pos @kc5_first_11 @kc5_second_34 -18;
    pos @kc5_first_11 @kc5_second_35 -22;
    pos @kc5_first_11 @kc5_second_37 -20;
    pos @kc5_first_11 @kc5_second_42 -40;
    pos @kc5_first_11 @kc5_second_44 -20;
    pos @kc5_first_11 @kc5_second_45 -12;
    pos @kc5_first_11 @kc5_second_47 -38;
    pos @kc5_first_11 @kc5_second_48 -14;
    pos @kc5_first_11 @kc5_second_49 -48;
    pos @kc5_first_11 @kc5_second_50 -16;
    pos @kc5_first_11 @kc5_second_51 -16;
    pos @kc5_first_11 @kc5_second_52 -34;
    pos @kc5_first_11 @kc5_second_53 -34;
    pos @kc5_first_11 @kc5_second_54 -36;
    pos @kc5_first_11 @kc5_second_55 -32;
    pos @kc5_first_11 @kc5_second_57 -34;
    pos @kc5_first_11 @kc5_second_58 -24;
    pos @kc5_first_11 @kc5_second_59 -18;
    pos @kc5_first_11 @kc5_second_63 -50;
    pos @kc5_first_11 @kc5_second_64 -44;
    pos @kc5_first_11 @kc5_second_65 -28;
    pos @kc5_first_11 @kc5_second_66 -50;
    pos @kc5_first_11 @kc5_second_67 -36;
    pos @kc5_first_12 @kc5_second_6 -18;
    pos @kc5_first_12 @kc5_second_8 -10;
    pos @kc5_first_12 @kc5_second_9 -14;
    pos @kc5_first_12 @kc5_second_10 -50;
    pos @kc5_first_12 @kc5_second_11 -36;
    pos @kc5_first_12 @kc5_second_12 -72;
    pos @kc5_first_12 @kc5_second_13 -34;
    pos @kc5_first_12 @kc5_second_14 -44;
    pos @kc5_first_12 @kc5_second_36 -46;
    pos @kc5_first_12 @kc5_second_37 -42;
    pos @kc5_first_12 @kc5_second_38 -18;
    pos @kc5_first_12 @kc5_second_39 -22;
    pos @kc5_first_12 @kc5_second_40 -22;
    pos @kc5_first_12 @kc5_second_41 -66;
    pos @kc5_first_12 @kc5_second_43 -22;
    pos @kc5_first_12 @kc5_second_46 -22;
    pos @kc5_first_12 @kc5_second_48 -44;
    pos @kc5_first_12 @kc5_second_50 -74;
    pos @kc5_first_12 @kc5_second_51 -66;
    pos @kc5_first_12 @kc5_second_52 -54;
    pos @kc5_first_12 @kc5_second_56 -28;
    pos @kc5_first_12 @kc5_second_59 -34;
    pos @kc5_first_12 @kc5_second_69 -10;
    pos @kc5_first_13 @kc5_second_2 12;
    pos @kc5_first_13 @kc5_second_3 -88;
    pos @kc5_first_13 @kc5_second_12 -12;
    pos @kc5_first_13 @kc5_second_14 -34;
    pos @kc5_first_13 @kc5_second_15 -74;
    pos @kc5_first_13 @kc5_second_16 -74;
    pos @kc5_first_13 @kc5_second_17 -66;
    pos @kc5_first_13 @kc5_second_18 -74;
    pos @kc5_first_13 @kc5_second_27 -50;
    pos @kc5_first_13 @kc5_second_30 -44;
    pos @kc5_first_13 @kc5_second_36 -24;
    pos @kc5_first_13 @kc5_second_37 -92;
    pos @kc5_first_13 @kc5_second_38 -10;
    pos @kc5_first_13 @kc5_second_39 -18;
    pos @kc5_first_13 @kc5_second_40 -12;
    pos @kc5_first_13 @kc5_second_41 -44;
    pos @kc5_first_13 @kc5_second_50 -162;
    pos @kc5_first_13 @kc5_second_51 -146;
    pos @kc5_first_13 @kc5_second_52 -164;
    pos @kc5_first_13 @kc5_second_58 -42;
    pos @kc5_first_13 @kc5_second_59 -142;
    pos @kc5_first_13 @kc5_second_60 -50;
    pos @kc5_first_13 @kc5_second_61 -38;
    pos @kc5_first_13 @kc5_second_63 -38;
    pos @kc5_first_13 @kc5_second_64 -44;
    pos @kc5_first_13 @kc5_second_66 -30;
    pos @kc5_first_13 @kc5_second_67 -18;
    pos @kc5_first_14 @kc5_second_2 -32;
    pos @kc5_first_14 @kc5_second_3 -18;
    pos @kc5_first_14 @kc5_second_5 60;
    pos @kc5_first_14 @kc5_second_6 -12;
    pos @kc5_first_14 @kc5_second_7 -30;
    pos @kc5_first_14 @kc5_second_8 -46;
    pos @kc5_first_14 @kc5_second_9 -40;
    pos @kc5_first_14 @kc5_second_10 -92;
    pos @kc5_first_14 @kc5_second_11 -66;
    pos @kc5_first_14 @kc5_second_12 -84;
    pos @kc5_first_14 @kc5_second_13 -56;
    pos @kc5_first_14 @kc5_second_14 -20;
    pos @kc5_first_14 @kc5_second_15 -18;
    pos @kc5_first_14 @kc5_second_16 -18;
    pos @kc5_first_14 @kc5_second_17 -22;
    pos @kc5_first_14 @kc5_second_18 -16;
    pos @kc5_first_14 @kc5_second_20 -14;
    pos @kc5_first_14 @kc5_second_21 -72;
    pos @kc5_first_14 @kc5_second_22 -70;
    pos @kc5_first_14 @kc5_second_23 -72;
    pos @kc5_first_14 @kc5_second_24 -40;
    pos @kc5_first_14 @kc5_second_27 -72;
    pos @kc5_first_14 @kc5_second_28 -32;
    pos @kc5_first_14 @kc5_second_30 -20;
    pos @kc5_first_14 @kc5_second_31 -42;
    pos @kc5_first_14 @kc5_second_32 -22;
    pos @kc5_first_14 @kc5_second_33 -46;
    pos @kc5_first_14 @kc5_second_34 -32;
    pos @kc5_first_14 @kc5_second_35 -28;
    pos @kc5_first_14 @kc5_second_60 36;
    pos @kc5_first_14 @kc5_second_61 -20;
    pos @kc5_first_14 @kc5_second_64 -10;
    pos @kc5_first_14 @kc5_second_70 10;
    pos @kc5_first_14 @kc5_second_71 34;
    pos @kc5_first_14 @kc5_second_72 36;
    pos @kc5_first_15 @kc5_second_10 -10;
    pos @kc5_first_15 @kc5_second_11 -10;
    pos @kc5_first_15 @kc5_second_12 -14;
    pos @kc5_first_15 @kc5_second_14 -32;
    pos @kc5_first_15 @kc5_second_19 -12;
    pos @kc5_first_15 @kc5_second_20 -10;
    pos @kc5_first_15 @kc5_second_21 -44;
    pos @kc5_first_15 @kc5_second_22 -42;
    pos @kc5_first_15 @kc5_second_23 -40;
    pos @kc5_first_15 @kc5_second_29 -12;
    pos @kc5_first_15 @kc5_second_31 -22;
    pos @kc5_first_15 @kc5_second_33 -16;
    pos @kc5_first_15 @kc5_second_34 -10;
    pos @kc5_first_15 @kc5_second_35 -16;
    pos @kc5_first_15 @kc5_second_36 -20;
    pos @kc5_first_15 @kc5_second_42 -12;
    pos @kc5_first_15 @kc5_second_44 -16;
    pos @kc5_first_15 @kc5_second_45 -12;
    pos @kc5_first_15 @kc5_second_47 -16;
    pos @kc5_first_15 @kc5_second_48 -26;
    pos @kc5_first_15 @kc5_second_49 -20;
    pos @kc5_first_15 @kc5_second_52 -12;
    pos @kc5_first_15 @kc5_second_54 -10;
    pos @kc5_first_15 @kc5_second_57 -12;
    pos @kc5_first_16 @kc5_second_3 -126;
    pos @kc5_first_16 @kc5_second_7 -12;
    pos @kc5_first_16 @kc5_second_15 -204;
    pos @kc5_first_16 @kc5_second_16 -202;
    pos @kc5_first_16 @kc5_second_17 -198;
    pos @kc5_first_16 @kc5_second_18 -202;
    pos @kc5_first_16 @kc5_second_19 -36;
    pos @kc5_first_16 @kc5_second_20 -136;
    pos @kc5_first_16 @kc5_second_21 -114;
    pos @kc5_first_16 @kc5_second_22 -112;
    pos @kc5_first_16 @kc5_second_23 -110;
    pos @kc5_first_16 @kc5_second_27 -128;
    pos @kc5_first_16 @kc5_second_29 -88;
    pos @kc5_first_16 @kc5_second_30 -158;
    pos @kc5_first_16 @kc5_second_31 -104;
    pos @kc5_first_16 @kc5_second_32 -80;
    pos @kc5_first_16 @kc5_second_33 -82;
    pos @kc5_first_16 @kc5_second_34 -72;
    pos @kc5_first_16 @kc5_second_35 -76;
    pos @kc5_first_16 @kc5_second_37 -104;
    pos @kc5_first_16 @kc5_second_42 -46;
    pos @kc5_first_16 @kc5_second_44 -38;
    pos @kc5_first_16 @kc5_second_45 -14;
    pos @kc5_first_16 @kc5_second_47 -146;
    pos @kc5_first_16 @kc5_second_48 -120;
    pos @kc5_first_16 @kc5_second_49 -158;
    pos @kc5_first_16 @kc5_second_50 -152;
    pos @kc5_first_16 @kc5_second_51 -140;
    pos @kc5_first_16 @kc5_second_52 -168;
    pos @kc5_first_16 @kc5_second_53 -90;
    pos @kc5_first_16 @kc5_second_54 -108;
    pos @kc5_first_16 @kc5_second_55 -84;
    pos @kc5_first_16 @kc5_second_56 -74;
    pos @kc5_first_16 @kc5_second_57 -102;
    pos @kc5_first_16 @kc5_second_58 -44;
    pos @kc5_first_16 @kc5_second_59 -88;
    pos @kc5_first_16 @kc5_second_60 -54;
    pos @kc5_first_16 @kc5_second_61 -82;
    pos @kc5_first_16 @kc5_second_62 -70;
    pos @kc5_first_16 @kc5_second_63 -172;
    pos @kc5_first_16 @kc5_second_64 -172;
    pos @kc5_first_16 @kc5_second_65 -134;
    pos @kc5_first_16 @kc5_second_66 -164;
    pos @kc5_first_16 @kc5_second_67 -148;
    pos @kc5_first_16 @kc5_second_73 -40;
    pos @kc5_first_16 @kc5_second_74 -80;
    pos @kc5_first_17 @kc5_second_3 -18;
    pos @kc5_first_17 @kc5_second_7 -12;
    pos @kc5_first_17 @kc5_second_15 -56;
    pos @kc5_first_17 @kc5_second_16 -56;
    pos @kc5_first_17 @kc5_second_17 -52;
    pos @kc5_first_17 @kc5_second_18 -56;
    pos @kc5_first_17 @kc5_second_19 -38;
    pos @kc5_first_17 @kc5_second_20 -28;
    pos @kc5_first_17 @kc5_second_21 -24;
    pos @kc5_first_17 @kc5_second_22 -24;
    pos @kc5_first_17 @kc5_second_23 -24;
    pos @kc5_first_17 @kc5_second_27 -30;
    pos @kc5_first_17 @kc5_second_29 -36;
    pos @kc5_first_17 @kc5_second_30 -40;
    pos @kc5_first_17 @kc5_second_31 -20;
    pos @kc5_first_17 @kc5_second_32 -26;
    pos @kc5_first_17 @kc5_second_33 -12;
    pos @kc5_first_17 @kc5_second_35 -12;
    pos @kc5_first_17 @kc5_second_37 -74;
    pos @kc5_first_17 @kc5_second_42 -44;
    pos @kc5_first_17 @kc5_second_44 -16;
    pos @kc5_first_17 @kc5_second_47 -46;
    pos @kc5_first_17 @kc5_second_48 -28;
    pos @kc5_first_17 @kc5_second_49 -56;
    pos @kc5_first_17 @kc5_second_50 -78;
    pos @kc5_first_17 @kc5_second_51 -78;
    pos @kc5_first_17 @kc5_second_52 -74;
    pos @kc5_first_17 @kc5_second_53 -38;
    pos @kc5_first_17 @kc5_second_54 -48;
    pos @kc5_first_17 @kc5_second_55 -38;
    pos @kc5_first_17 @kc5_second_56 -16;
    pos @kc5_first_17 @kc5_second_57 -40;
    pos @kc5_first_17 @kc5_second_58 -24;
    pos @kc5_first_17 @kc5_second_59 -46;
    pos @kc5_first_17 @kc5_second_60 -34;
    pos @kc5_first_17 @kc5_second_62 -18;
    pos @kc5_first_17 @kc5_second_63 -60;
    pos @kc5_first_17 @kc5_second_64 -54;
    pos @kc5_first_17 @kc5_second_65 -24;
    pos @kc5_first_17 @kc5_second_66 -58;
    pos @kc5_first_17 @kc5_second_67 -42;
    pos @kc5_first_17 @kc5_second_75 16;
    pos @kc5_first_18 @kc5_second_1 16;
    pos @kc5_first_18 @kc5_second_3 -94;
    pos @kc5_first_18 @kc5_second_4 -28;
    pos @kc5_first_18 @kc5_second_7 -46;
    pos @kc5_first_18 @kc5_second_15 -156;
    pos @kc5_first_18 @kc5_second_16 -154;
    pos @kc5_first_18 @kc5_second_17 -142;
    pos @kc5_first_18 @kc5_second_18 -156;
    pos @kc5_first_18 @kc5_second_19 -74;
    pos @kc5_first_18 @kc5_second_20 -82;
    pos @kc5_first_18 @kc5_second_21 -76;
    pos @kc5_first_18 @kc5_second_22 -78;
    pos @kc5_first_18 @kc5_second_23 -76;
    pos @kc5_first_18 @kc5_second_27 -100;
    pos @kc5_first_18 @kc5_second_28 24;
    pos @kc5_first_18 @kc5_second_29 -66;
    pos @kc5_first_18 @kc5_second_30 -124;
    pos @kc5_first_18 @kc5_second_31 -74;
    pos @kc5_first_18 @kc5_second_32 -70;
    pos @kc5_first_18 @kc5_second_33 -46;
    pos @kc5_first_18 @kc5_second_34 -40;
    pos @kc5_first_18 @kc5_second_35 -44;
    pos @kc5_first_18 @kc5_second_37 -126;
    pos @kc5_first_18 @kc5_second_42 -70;
    pos @kc5_first_18 @kc5_second_43 42;
    pos @kc5_first_18 @kc5_second_44 -16;
    pos @kc5_first_18 @kc5_second_46 42;
    pos @kc5_first_18 @kc5_second_47 -126;
    pos @kc5_first_18 @kc5_second_48 -78;
    pos @kc5_first_18 @kc5_second_49 -132;
    pos @kc5_first_18 @kc5_second_50 -186;
    pos @kc5_first_18 @kc5_second_51 -166;
    pos @kc5_first_18 @kc5_second_52 -220;
    pos @kc5_first_18 @kc5_second_53 -74;
    pos @kc5_first_18 @kc5_second_54 -92;
    pos @kc5_first_18 @kc5_second_55 -72;
    pos @kc5_first_18 @kc5_second_56 -46;
    pos @kc5_first_18 @kc5_second_57 -92;
    pos @kc5_first_18 @kc5_second_58 -58;
    pos @kc5_first_18 @kc5_second_59 -130;
    pos @kc5_first_18 @kc5_second_60 -76;
    pos @kc5_first_18 @kc5_second_61 -78;
    pos @kc5_first_18 @kc5_second_62 -62;
    pos @kc5_first_18 @kc5_second_63 -162;
    pos @kc5_first_18 @kc5_second_64 -140;
    pos @kc5_first_18 @kc5_second_65 -96;
    pos @kc5_first_18 @kc5_second_66 -150;
    pos @kc5_first_18 @kc5_second_67 -126;
    pos @kc5_first_18 @kc5_second_73 -60;
    pos @kc5_first_18 @kc5_second_74 -62;
    pos @kc5_first_18 @kc5_second_75 76;
    pos @kc5_first_18 @kc5_second_76 -26;
    pos @kc5_first_18 @kc5_second_77 -16;
    pos @kc5_first_19 @kc5_second_3 -70;
    pos @kc5_first_19 @kc5_second_4 -24;
    pos @kc5_first_19 @kc5_second_7 -38;
    pos @kc5_first_19 @kc5_second_15 -114;
    pos @kc5_first_19 @kc5_second_16 -114;
    pos @kc5_first_19 @kc5_second_17 -108;
    pos @kc5_first_19 @kc5_second_18 -114;
    pos @kc5_first_19 @kc5_second_19 -72;
    pos @kc5_first_19 @kc5_second_20 -68;
    pos @kc5_first_19 @kc5_second_21 -56;
    pos @kc5_first_19 @kc5_second_22 -58;
    pos @kc5_first_19 @kc5_second_23 -56;
    pos @kc5_first_19 @kc5_second_27 -80;
    pos @kc5_first_19 @kc5_second_29 -66;
    pos @kc5_first_19 @kc5_second_30 -92;
    pos @kc5_first_19 @kc5_second_31 -62;
    pos @kc5_first_19 @kc5_second_32 -64;
    pos @kc5_first_19 @kc5_second_33 -34;
    pos @kc5_first_19 @kc5_second_34 -30;
    pos @kc5_first_19 @kc5_second_35 -32;
    pos @kc5_first_19 @kc5_second_37 -106;
    pos @kc5_first_19 @kc5_second_42 -64;
    pos @kc5_first_19 @kc5_second_43 30;
    pos @kc5_first_19 @kc5_second_44 -18;
    pos @kc5_first_19 @kc5_second_46 30;
    pos @kc5_first_19 @kc5_second_47 -88;
    pos @kc5_first_19 @kc5_second_48 -54;
    pos @kc5_first_19 @kc5_second_49 -98;
    pos @kc5_first_19 @kc5_second_50 -164;
    pos @kc5_first_19 @kc5_second_51 -144;
    pos @kc5_first_19 @kc5_second_52 -158;
    pos @kc5_first_19 @kc5_second_53 -70;
    pos @kc5_first_19 @kc5_second_54 -70;
    pos @kc5_first_19 @kc5_second_55 -70;
    pos @kc5_first_19 @kc5_second_56 -44;
    pos @kc5_first_19 @kc5_second_57 -72;
    pos @kc5_first_19 @kc5_second_58 -54;
    pos @kc5_first_19 @kc5_second_59 -102;
    pos @kc5_first_19 @kc5_second_60 -66;
    pos @kc5_first_19 @kc5_second_61 -60;
    pos @kc5_first_19 @kc5_second_62 -50;
    pos @kc5_first_19 @kc5_second_63 -130;
    pos @kc5_first_19 @kc5_second_64 -108;
    pos @kc5_first_19 @kc5_second_65 -62;
    pos @kc5_first_19 @kc5_second_66 -118;
    pos @kc5_first_19 @kc5_second_67 -96;
    pos @kc5_first_19 @kc5_second_73 -52;
    pos @kc5_first_19 @kc5_second_74 -50;
    pos @kc5_first_19 @kc5_second_75 64;
    pos @kc5_first_19 @kc5_second_76 -24;
    pos @kc5_first_19 @kc5_second_77 -14;
    pos @kc5_first_20 @kc5_second_3 -48;
    pos @kc5_first_20 @kc5_second_5 12;
    pos @kc5_first_20 @kc5_second_7 -62;
    pos @kc5_first_20 @kc5_second_15 -58;
    pos @kc5_first_20 @kc5_second_16 -52;
    pos @kc5_first_20 @kc5_second_17 -64;
    pos @kc5_first_20 @kc5_second_18 -48;
    pos @kc5_first_20 @kc5_second_19 -24;
    pos @kc5_first_20 @kc5_second_20 -26;
    pos @kc5_first_20 @kc5_second_21 -122;
    pos @kc5_first_20 @kc5_second_22 -120;
    pos @kc5_first_20 @kc5_second_23 -120;
    pos @kc5_first_20 @kc5_second_27 -62;
    pos @kc5_first_20 @kc5_second_30 -56;
    pos @kc5_first_20 @kc5_second_31 -86;
    pos @kc5_first_20 @kc5_second_32 -30;
    pos @kc5_first_20 @kc5_second_33 -94;
    pos @kc5_first_20 @kc5_second_34 -74;
    pos @kc5_first_20 @kc5_second_35 -44;
    pos @kc5_first_21 @kc5_second_3 -112;
    pos @kc5_first_21 @kc5_second_4 -36;
    pos @kc5_first_21 @kc5_second_7 -72;
    pos @kc5_first_21 @kc5_second_15 -184;
    pos @kc5_first_21 @kc5_second_16 -182;
    pos @kc5_first_21 @kc5_second_17 -180;
    pos @kc5_first_21 @kc5_second_18 -182;
    pos @kc5_first_21 @kc5_second_19 -122;
    pos @kc5_first_21 @kc5_second_20 -140;
    pos @kc5_first_21 @kc5_second_21 -122;
    pos @kc5_first_21 @kc5_second_22 -124;
    pos @kc5_first_21 @kc5_second_23 -124;
    pos @kc5_first_21 @kc5_second_27 -120;
    pos @kc5_first_21 @kc5_second_29 -120;
    pos @kc5_first_21 @kc5_second_30 -150;
    pos @kc5_first_21 @kc5_second_31 -118;
    pos @kc5_first_21 @kc5_second_32 -112;
    pos @kc5_first_21 @kc5_second_33 -74;
    pos @kc5_first_21 @kc5_second_34 -66;
    pos @kc5_first_21 @kc5_second_35 -70;
    pos @kc5_first_21 @kc5_second_37 -104;
    pos @kc5_first_21 @kc5_second_42 -74;
    pos @kc5_first_21 @kc5_second_43 22;
    pos @kc5_first_21 @kc5_second_44 -22;
    pos @kc5_first_21 @kc5_second_46 22;
    pos @kc5_first_21 @kc5_second_47 -142;
    pos @kc5_first_21 @kc5_second_48 -116;
    pos @kc5_first_21 @kc5_second_49 -158;
    pos @kc5_first_21 @kc5_second_50 -170;
    pos @kc5_first_21 @kc5_second_51 -154;
    pos @kc5_first_21 @kc5_second_52 -164;
    pos @kc5_first_21 @kc5_second_53 -114;
    pos @kc5_first_21 @kc5_second_54 -124;
    pos @kc5_first_21 @kc5_second_55 -106;
    pos @kc5_first_21 @kc5_second_56 -76;
    pos @kc5_first_21 @kc5_second_57 -124;
    pos @kc5_first_21 @kc5_second_58 -56;
    pos @kc5_first_21 @kc5_second_59 -72;
    pos @kc5_first_21 @kc5_second_60 -58;
    pos @kc5_first_21 @kc5_second_61 -88;
    pos @kc5_first_21 @kc5_second_62 -64;
    pos @kc5_first_21 @kc5_second_63 -162;
    pos @kc5_first_21 @kc5_second_64 -154;
    pos @kc5_first_21 @kc5_second_65 -128;
    pos @kc5_first_21 @kc5_second_66 -154;
    pos @kc5_first_21 @kc5_second_67 -132;
    pos @kc5_first_21 @kc5_second_73 -70;
    pos @kc5_first_21 @kc5_second_74 -78;
    pos @kc5_first_21 @kc5_second_75 56;
    pos @kc5_first_21 @kc5_second_76 -28;
    pos @kc5_first_21 @kc5_second_77 -16;
    pos @kc5_first_22 @kc5_second_2 -24;
    pos @kc5_first_22 @kc5_second_19 -20;
    pos @kc5_first_22 @kc5_second_20 -20;
    pos @kc5_first_22 @kc5_second_21 -84;
    pos @kc5_first_22 @kc5_second_22 -84;
    pos @kc5_first_22 @kc5_second_23 -80;
    pos @kc5_first_22 @kc5_second_24 -20;
    pos @kc5_first_22 @kc5_second_29 -20;
    pos @kc5_first_22 @kc5_second_31 -74;
    pos @kc5_first_22 @kc5_second_32 -18;
    pos @kc5_first_22 @kc5_second_33 -68;
    pos @kc5_first_22 @kc5_second_34 -52;
    pos @kc5_first_22 @kc5_second_35 -66;
    pos @kc5_first_22 @kc5_second_44 -10;
    pos @kc5_first_22 @kc5_second_45 -16;
    pos @kc5_first_22 @kc5_second_47 -10;
    pos @kc5_first_22 @kc5_second_49 -16;
    pos @kc5_first_22 @kc5_second_53 -10;
    pos @kc5_first_22 @kc5_second_55 -10;
    pos @kc5_first_22 @kc5_second_57 -12;
    pos @kc5_first_22 @kc5_second_64 -10;
    pos @kc5_first_22 @kc5_second_65 -14;
    pos @kc5_first_23 @kc5_second_1 -32;
    pos @kc5_first_23 @kc5_second_6 -18;
    pos @kc5_first_23 @kc5_second_8 -30;
    pos @kc5_first_23 @kc5_second_9 -12;
    pos @kc5_first_23 @kc5_second_10 -66;
    pos @kc5_first_23 @kc5_second_11 -46;
    pos @kc5_first_23 @kc5_second_12 -90;
    pos @kc5_first_23 @kc5_second_25 -26;
    pos @kc5_first_23 @kc5_second_37 -46;
    pos @kc5_first_23 @kc5_second_38 -20;
    pos @kc5_first_23 @kc5_second_39 -24;
    pos @kc5_first_23 @kc5_second_40 -24;
    pos @kc5_first_23 @kc5_second_41 -128;
    pos @kc5_first_23 @kc5_second_43 -14;
    pos @kc5_first_23 @kc5_second_50 -80;
    pos @kc5_first_23 @kc5_second_52 -66;
    pos @kc5_first_23 @kc5_second_59 -92;
    pos @kc5_first_23 @kc5_second_69 -20;
    pos @kc5_first_24 @kc5_second_10 12;
    pos @kc5_first_24 @kc5_second_11 10;
    pos @kc5_first_24 @kc5_second_12 26;
    pos @kc5_first_24 @kc5_second_15 -30;
    pos @kc5_first_24 @kc5_second_16 -30;
    pos @kc5_first_24 @kc5_second_17 -24;
    pos @kc5_first_24 @kc5_second_18 -30;
    pos @kc5_first_24 @kc5_second_19 -10;
    pos @kc5_first_24 @kc5_second_29 -18;
    pos @kc5_first_24 @kc5_second_30 -14;
    pos @kc5_first_24 @kc5_second_32 -10;
    pos @kc5_first_24 @kc5_second_37 -70;
    pos @kc5_first_24 @kc5_second_42 -24;
    pos @kc5_first_24 @kc5_second_47 -16;
    pos @kc5_first_24 @kc5_second_48 -10;
    pos @kc5_first_24 @kc5_second_49 -26;
    pos @kc5_first_24 @kc5_second_50 -76;
    pos @kc5_first_24 @kc5_second_51 -76;
    pos @kc5_first_24 @kc5_second_52 -78;
    pos @kc5_first_24 @kc5_second_53 -20;
    pos @kc5_first_24 @kc5_second_54 -32;
    pos @kc5_first_24 @kc5_second_55 -22;
    pos @kc5_first_24 @kc5_second_57 -12;
    pos @kc5_first_24 @kc5_second_59 -50;
    pos @kc5_first_24 @kc5_second_60 -26;
    pos @kc5_first_24 @kc5_second_63 -30;
    pos @kc5_first_24 @kc5_second_64 -30;
    pos @kc5_first_24 @kc5_second_66 -26;
    pos @kc5_first_24 @kc5_second_67 -16;
    pos @kc5_first_25 @kc5_second_3 -24;
    pos @kc5_first_25 @kc5_second_6 10;
    pos @kc5_first_25 @kc5_second_7 -16;
    pos @kc5_first_25 @kc5_second_8 40;
    pos @kc5_first_25 @kc5_second_9 28;
    pos @kc5_first_25 @kc5_second_10 102;
    pos @kc5_first_25 @kc5_second_11 98;
    pos @kc5_first_25 @kc5_second_12 110;
    pos @kc5_first_25 @kc5_second_13 78;
    pos @kc5_first_25 @kc5_second_14 116;
    pos @kc5_first_25 @kc5_second_15 -56;
    pos @kc5_first_25 @kc5_second_16 -56;
    pos @kc5_first_25 @kc5_second_17 -52;
    pos @kc5_first_25 @kc5_second_18 -56;
    pos @kc5_first_25 @kc5_second_19 -48;
    pos @kc5_first_25 @kc5_second_20 -36;
    pos @kc5_first_25 @kc5_second_21 -32;
    pos @kc5_first_25 @kc5_second_22 -32;
    pos @kc5_first_25 @kc5_second_23 -32;
    pos @kc5_first_25 @kc5_second_27 -34;
    pos @kc5_first_25 @kc5_second_29 -42;
    pos @kc5_first_25 @kc5_second_30 -42;
    pos @kc5_first_25 @kc5_second_31 -28;
    pos @kc5_first_25 @kc5_second_32 -34;
    pos @kc5_first_25 @kc5_second_33 -18;
    pos @kc5_first_25 @kc5_second_34 -14;
    pos @kc5_first_25 @kc5_second_35 -18;
    pos @kc5_first_25 @kc5_second_36 112;
    pos @kc5_first_25 @kc5_second_37 -80;
    pos @kc5_first_25 @kc5_second_40 18;
    pos @kc5_first_25 @kc5_second_41 22;
    pos @kc5_first_25 @kc5_second_42 -44;
    pos @kc5_first_25 @kc5_second_44 -16;
    pos @kc5_first_25 @kc5_second_47 -50;
    pos @kc5_first_25 @kc5_second_48 -38;
    pos @kc5_first_25 @kc5_second_49 -66;
    pos @kc5_first_25 @kc5_second_50 -92;
    pos @kc5_first_25 @kc5_second_51 -78;
    pos @kc5_first_25 @kc5_second_52 -84;
    pos @kc5_first_25 @kc5_second_53 -46;
    pos @kc5_first_25 @kc5_second_54 -50;
    pos @kc5_first_25 @kc5_second_55 -44;
    pos @kc5_first_25 @kc5_second_56 -24;
    pos @kc5_first_25 @kc5_second_57 -48;
    pos @kc5_first_25 @kc5_second_58 -26;
    pos @kc5_first_25 @kc5_second_59 -46;
    pos @kc5_first_25 @kc5_second_60 -34;
    pos @kc5_first_25 @kc5_second_62 -22;
    pos @kc5_first_25 @kc5_second_63 -68;
    pos @kc5_first_25 @kc5_second_64 -54;
    pos @kc5_first_25 @kc5_second_65 -32;
    pos @kc5_first_25 @kc5_second_66 -64;
    pos @kc5_first_25 @kc5_second_67 -48;
    pos @kc5_first_25 @kc5_second_74 -24;
    pos @kc5_first_25 @kc5_second_78 46;
  subtable;
    @kc6_first_0 = [\ampersand ];
    @kc6_first_1 = [\zero \zero.prop ];
    @kc6_first_2 = [\one.prop ];
    @kc6_first_3 = [\two.prop ];
    @kc6_first_4 = [\three.prop ];
    @kc6_first_5 = [\seven.prop ];
    @kc6_first_6 = [\eight.prop ];
    @kc6_first_7 = [\nine.prop ];
    @kc6_first_8 = [\exclamdown ];
    @kc6_first_9 = [\registered ];
    @kc6_first_10 = [\questiondown ];
    @kc6_first_11 = [\uni02BC ];
    @kc6_first_12 = [\fraction ];
    @kc6_first_13 = [];
    @kc6_second_0 = [];
    @kc6_second_1 = [\quotedbl \quotesingle ];
    @kc6_second_2 = [\T \uni0162 \Tcaron \Tbar \uni1E6A \uni1E6C \uni1E6E ];
    @kc6_second_3 = [\V ];
    @kc6_second_4 = [\W \Wcircumflex \Wgrave \Wacute \Wdieresis ];
    @kc6_second_5 = [\Y \Yacute \Ycircumflex \Ydieresis \Ygrave ];
    @kc6_second_6 = [\parenright ];
    @kc6_second_7 = [\A \Agrave \Aacute \Acircumflex \Atilde \Adieresis \Aring \Amacron \Abreve 
	\Aogonek ];
    @kc6_second_8 = [\backslash ];
    @kc6_second_9 = [\bracketright ];
    @kc6_second_10 = [\X ];
    @kc6_second_11 = [\comma \period \quotesinglbase \quotedblbase \ellipsis ];
    @kc6_second_12 = [\M \uni1E40 ];
    @kc6_second_13 = [\J \Jcircumflex ];
    @kc6_second_14 = [\U \Ugrave \Uacute \Ucircumflex \Udieresis \Utilde \Umacron \Ubreve \Uring 
	\Uhungarumlaut \Uogonek ];
    @kc6_second_15 = [];
    @kc6_second_16 = [\N \Ntilde \Nacute \uni0145 \Ncaron \Eng ];
    @kc6_second_17 = [\AE ];
    @kc6_second_18 = [];
    @kc6_second_19 = [\B \D \E \F \H \I \K \L \P \R \Egrave \Eacute \Ecircumflex \Edieresis \Igrave \Iacute 
	\Icircumflex \Idieresis \Eth \Thorn \Dcaron \Dcroat \Emacron \Ebreve 
	\Edotaccent \Eogonek \Ecaron \Hcircumflex \Hbar \Itilde \Imacron 
	\Ibreve \Iogonek \Idotaccent \IJ \uni0136 \Lacute \uni013B \Lcaron \Ldot 
	\Lslash \Racute \uni0156 \Rcaron \uni1E02 \uni1E0A \uni1E0C \uni1E0E 
	\uni1E10 \uni1E1E \uni1E24 \uni1E28 \uni1E2A \uni1E56 ];
    @kc6_second_20 = [\e \o \egrave \eacute \ecircumflex \edieresis \eth \ograve \oacute \ocircumflex 
	\otilde \odieresis \oslash \emacron \ebreve \edotaccent \eogonek 
	\ecaron \omacron \obreve \ohungarumlaut \oe ];
    @kc6_second_21 = [];
    @kc6_second_22 = [\zero \zero.prop ];
    @kc6_second_23 = [\d \dcaron \dcroat \uni1E0B \uni1E0D \uni1E0F \uni1E11 ];
    pos @kc6_first_0 @kc6_second_1 -74;
    pos @kc6_first_0 @kc6_second_2 -60;
    pos @kc6_first_0 @kc6_second_3 -56;
    pos @kc6_first_0 @kc6_second_4 -44;
    pos @kc6_first_0 @kc6_second_5 -68;
    pos @kc6_first_1 @kc6_second_3 -34;
    pos @kc6_first_1 @kc6_second_4 -28;
    pos @kc6_first_1 @kc6_second_5 -44;
    pos @kc6_first_1 @kc6_second_6 -42;
    pos @kc6_first_1 @kc6_second_7 -20;
    pos @kc6_first_1 @kc6_second_8 -30;
    pos @kc6_first_1 @kc6_second_9 -42;
    pos @kc6_first_2 @kc6_second_7 18;
    pos @kc6_first_2 @kc6_second_10 12;
    pos @kc6_first_3 @kc6_second_3 -28;
    pos @kc6_first_3 @kc6_second_4 -22;
    pos @kc6_first_3 @kc6_second_5 -32;
    pos @kc6_first_4 @kc6_second_3 -30;
    pos @kc6_first_4 @kc6_second_4 -26;
    pos @kc6_first_4 @kc6_second_5 -34;
    pos @kc6_first_5 @kc6_second_3 60;
    pos @kc6_first_5 @kc6_second_4 56;
    pos @kc6_first_5 @kc6_second_5 72;
    pos @kc6_first_5 @kc6_second_7 -82;
    pos @kc6_first_5 @kc6_second_11 -114;
    pos @kc6_first_6 @kc6_second_3 -26;
    pos @kc6_first_6 @kc6_second_4 -22;
    pos @kc6_first_6 @kc6_second_5 -28;
    pos @kc6_first_7 @kc6_second_3 -20;
    pos @kc6_first_7 @kc6_second_5 -26;
    pos @kc6_first_7 @kc6_second_7 -52;
    pos @kc6_first_7 @kc6_second_11 -68;
    pos @kc6_first_7 @kc6_second_12 -22;
    pos @kc6_first_8 @kc6_second_2 -92;
    pos @kc6_first_8 @kc6_second_3 -82;
    pos @kc6_first_8 @kc6_second_4 -66;
    pos @kc6_first_8 @kc6_second_5 -96;
    pos @kc6_first_8 @kc6_second_13 136;
    pos @kc6_first_8 @kc6_second_14 -36;
    pos @kc6_first_8 @kc6_second_15 32;
    pos @kc6_first_9 @kc6_second_5 -26;
    pos @kc6_first_9 @kc6_second_7 -66;
    pos @kc6_first_9 @kc6_second_12 -28;
    pos @kc6_first_9 @kc6_second_16 -22;
    pos @kc6_first_9 @kc6_second_17 -102;
    pos @kc6_first_9 @kc6_second_18 -76;
    pos @kc6_first_10 @kc6_second_2 -92;
    pos @kc6_first_10 @kc6_second_3 -74;
    pos @kc6_first_10 @kc6_second_4 -60;
    pos @kc6_first_10 @kc6_second_5 -100;
    pos @kc6_first_10 @kc6_second_13 168;
    pos @kc6_first_10 @kc6_second_14 -28;
    pos @kc6_first_10 @kc6_second_16 -22;
    pos @kc6_first_10 @kc6_second_18 -26;
    pos @kc6_first_10 @kc6_second_19 -20;
    pos @kc6_first_11 @kc6_second_18 -48;
    pos @kc6_first_11 @kc6_second_20 -28;
    pos @kc6_first_11 @kc6_second_21 -24;
    pos @kc6_first_12 @kc6_second_22 -46;
    pos @kc6_first_13 @kc6_second_7 -70;
    pos @kc6_first_13 @kc6_second_17 -118;
    pos @kc6_first_13 @kc6_second_18 -28;
    pos @kc6_first_13 @kc6_second_23 -16;
} kernHorizontalKerninginLatinloo;

lookup cursCursiveAttachment {
  lookupflag RightToLeft, IgnoreMarks;
    pos cursive \aSad.init_AboveHaa <anchor NULL> <anchor 0 424>;
    pos cursive \aAyn.medi_AynYaaFina <anchor NULL> <anchor 0 0>;
    pos cursive \aSen.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \aMem.init_MemHaaInit <anchor NULL> <anchor 0 521>;
    pos cursive \aKaf.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \aMem.fina_KafMemFina <anchor 462 217> <anchor NULL>;
    pos cursive \aSen.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \aAyn.init_AynHaaInit <anchor NULL> <anchor 0 500>;
    pos cursive \aLam.medi_LamMemMedi <anchor NULL> <anchor 0 170>;
    pos cursive \aMem.medi_LamMemMedi <anchor 485 170> <anchor NULL>;
    pos cursive \aHaa.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \aBaa.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \aHaa.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \aHaa.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \aHeh.init_AboveHaa <anchor NULL> <anchor 0 382>;
    pos cursive \aHaa.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \aAyn.init_AboveHaa <anchor NULL> <anchor -65 420>;
    pos cursive \aMem.init_AboveHaa <anchor NULL> <anchor 0 411>;
    pos cursive \aLam.medi_KafLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \aLam.medi_KafLamYaa <anchor NULL> <anchor 0 -145>;
    pos cursive \aMem.medi_MemAlfFina <anchor NULL> <anchor 0 -209>;
    pos cursive \aAlf.fina_MemAlfFina <anchor 398 -209> <anchor NULL>;
    pos cursive \aLam.medi_LamLamMemInit <anchor NULL> <anchor 85 170>;
    pos cursive \aLam.medi_LamLamYaaIsol <anchor NULL> <anchor 0 -145>;
    pos cursive \aLam.medi_LamLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \aLam.medi_LamLamYaaFina <anchor NULL> <anchor 0 -145>;
    pos cursive \uni069D.init_AboveHaa <anchor NULL> <anchor 0 424>;
    pos cursive \uni06FB.init_AboveHaa <anchor NULL> <anchor 0 424>;
    pos cursive \uni0636.init_AboveHaa <anchor NULL> <anchor 0 424>;
    pos cursive \uni069E.init_AboveHaa <anchor NULL> <anchor 0 424>;
    pos cursive \uni0635.init_AboveHaa <anchor NULL> <anchor 0 424>;
    pos cursive \uni06FC.medi_AynYaaFina <anchor NULL> <anchor 0 0>;
    pos cursive \uni063A.medi_AynYaaFina <anchor NULL> <anchor 0 0>;
    pos cursive \uni075E.medi_AynYaaFina <anchor NULL> <anchor 0 0>;
    pos cursive \uni075D.medi_AynYaaFina <anchor NULL> <anchor 0 0>;
    pos cursive \uni075F.medi_AynYaaFina <anchor NULL> <anchor 0 0>;
    pos cursive \uni06A0.medi_AynYaaFina <anchor NULL> <anchor 0 0>;
    pos cursive \uni0639.medi_AynYaaFina <anchor NULL> <anchor 0 0>;
    pos cursive \uni06FA.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni076D.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni0633.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni077E.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni077D.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni0634.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni0770.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni075C.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni069A.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni069B.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni069C.init_AboveHaa <anchor NULL> <anchor 0 415>;
    pos cursive \uni0645.init_MemHaaInit <anchor NULL> <anchor 0 521>;
    pos cursive \uni063B.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni063C.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni077F.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni0764.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni0643.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06B0.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06B3.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06B2.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06AB.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06AC.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06AD.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06AE.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06AF.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06A9.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06B4.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni0763.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni0762.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni06B1.medi_KafMemFina <anchor NULL> <anchor 0 217>;
    pos cursive \uni0645.fina_KafMemFina <anchor 462 217> <anchor NULL>;
    pos cursive \uni06FA.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni076D.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni0633.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni077E.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni077D.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni0634.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni0770.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni075C.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni069A.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni069B.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni069C.init_SenHaaInit <anchor NULL> <anchor 0 457>;
    pos cursive \uni06FC.init_AynHaaInit <anchor NULL> <anchor 0 500>;
    pos cursive \uni063A.init_AynHaaInit <anchor NULL> <anchor 0 500>;
    pos cursive \uni075E.init_AynHaaInit <anchor NULL> <anchor 0 500>;
    pos cursive \uni075D.init_AynHaaInit <anchor NULL> <anchor 0 500>;
    pos cursive \uni075F.init_AynHaaInit <anchor NULL> <anchor 0 500>;
    pos cursive \uni06A0.init_AynHaaInit <anchor NULL> <anchor 0 500>;
    pos cursive \uni0639.init_AynHaaInit <anchor NULL> <anchor 0 500>;
    pos cursive \uni06B5.medi_LamMemMedi <anchor NULL> <anchor 0 170>;
    pos cursive \uni06B7.medi_LamMemMedi <anchor NULL> <anchor 0 170>;
    pos cursive \uni0644.medi_LamMemMedi <anchor NULL> <anchor 0 170>;
    pos cursive \uni06B8.medi_LamMemMedi <anchor NULL> <anchor 0 170>;
    pos cursive \uni06B6.medi_LamMemMedi <anchor NULL> <anchor 0 170>;
    pos cursive \uni076A.medi_LamMemMedi <anchor NULL> <anchor 0 170>;
    pos cursive \uni0765.medi_LamMemMedi <anchor 485 170> <anchor NULL>;
    pos cursive \uni0645.medi_LamMemMedi <anchor 485 170> <anchor NULL>;
    pos cursive \uni0766.medi_LamMemMedi <anchor 485 170> <anchor NULL>;
    pos cursive \uni062E.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni062D.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0681.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0687.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0685.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni062C.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0682.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0757.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0684.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni076F.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni076E.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0683.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni06BF.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni077C.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0758.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0772.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0686.medi_SadHaaInit <anchor 113 481> <anchor NULL>;
    pos cursive \uni0777.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0680.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0776.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06BC.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0750.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0756.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0768.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06CE.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0775.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06BD.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0626.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni066E.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0620.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni064A.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06BB.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni067F.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0755.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni067D.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni067E.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni067B.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0628.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni067A.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0751.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0646.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0753.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0752.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni062A.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0678.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni063D.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni062B.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0679.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06B9.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0769.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0649.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni067C.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0754.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06D1.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06D0.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06BA.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06CC.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0767.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni062E.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni062D.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0681.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0687.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0685.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni062C.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0682.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0757.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0684.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni076F.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni076E.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0683.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni06BF.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni077C.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0758.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0772.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0686.fina_AboveHaaIsol <anchor 213 420> <anchor NULL>;
    pos cursive \uni0647.init_AboveHaa <anchor NULL> <anchor 0 382>;
    pos cursive \uni06C1.init_AboveHaa <anchor NULL> <anchor 0 382>;
    pos cursive \uni062E.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni062D.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0681.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0687.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0685.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni062C.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0682.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0757.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0684.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni076F.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni076E.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0683.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06BF.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni077C.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0758.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0772.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni0686.init_AboveHaa <anchor NULL> <anchor 0 420>;
    pos cursive \uni06FC.init_AboveHaa <anchor NULL> <anchor -65 420>;
    pos cursive \uni063A.init_AboveHaa <anchor NULL> <anchor -65 420>;
    pos cursive \uni075E.init_AboveHaa <anchor NULL> <anchor -65 420>;
    pos cursive \uni075D.init_AboveHaa <anchor NULL> <anchor -65 420>;
    pos cursive \uni075F.init_AboveHaa <anchor NULL> <anchor -65 420>;
    pos cursive \uni06A0.init_AboveHaa <anchor NULL> <anchor -65 420>;
    pos cursive \uni0639.init_AboveHaa <anchor NULL> <anchor -65 420>;
    pos cursive \uni0645.init_AboveHaa <anchor NULL> <anchor 0 411>;
    pos cursive \uni06B5.medi_KafLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B7.medi_KafLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni0644.medi_KafLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B8.medi_KafLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B6.medi_KafLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni076A.medi_KafLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B5.medi_KafLamYaa <anchor NULL> <anchor 0 -145>;
    pos cursive \uni06B7.medi_KafLamYaa <anchor NULL> <anchor 0 -145>;
    pos cursive \uni0644.medi_KafLamYaa <anchor NULL> <anchor 0 -145>;
    pos cursive \uni06B8.medi_KafLamYaa <anchor NULL> <anchor 0 -145>;
    pos cursive \uni06B6.medi_KafLamYaa <anchor NULL> <anchor 0 -145>;
    pos cursive \uni076A.medi_KafLamYaa <anchor NULL> <anchor 0 -145>;
    pos cursive \uni0645.medi_MemAlfFina <anchor NULL> <anchor 0 -209>;
    pos cursive \uni0625.fina_MemAlfFina <anchor 398 -209> <anchor NULL>;
    pos cursive \uni0627.fina_MemAlfFina <anchor 398 -209> <anchor NULL>;
    pos cursive \uni0774.fina_MemAlfFina <anchor 605 -209> <anchor NULL>;
    pos cursive \uni0773.fina_MemAlfFina <anchor 604 -209> <anchor NULL>;
    pos cursive \uni0623.fina_MemAlfFina <anchor 605 -209> <anchor NULL>;
    pos cursive \uni0622.fina_MemAlfFina <anchor 598 -209> <anchor NULL>;
    pos cursive \uni0675.fina_MemAlfFina <anchor 398 -209> <anchor NULL>;
    pos cursive \uni0672.fina_MemAlfFina <anchor 398 -209> <anchor NULL>;
    pos cursive \uni0673.fina_MemAlfFina <anchor 398 -209> <anchor NULL>;
    pos cursive \uni0671.fina_MemAlfFina <anchor 508 -209> <anchor NULL>;
    pos cursive \uni06B5.medi_LamLamMemInit <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B7.medi_LamLamMemInit <anchor NULL> <anchor 85 170>;
    pos cursive \uni0644.medi_LamLamMemInit <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B8.medi_LamLamMemInit <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B6.medi_LamLamMemInit <anchor NULL> <anchor 85 170>;
    pos cursive \uni076A.medi_LamLamMemInit <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B5.medi_LamLamYaaIsol <anchor NULL> <anchor 0 -145>;
    pos cursive \uni06B7.medi_LamLamYaaIsol <anchor NULL> <anchor 0 -145>;
    pos cursive \uni0644.medi_LamLamYaaIsol <anchor NULL> <anchor 0 -145>;
    pos cursive \uni06B8.medi_LamLamYaaIsol <anchor NULL> <anchor 0 -145>;
    pos cursive \uni06B6.medi_LamLamYaaIsol <anchor NULL> <anchor 0 -145>;
    pos cursive \uni076A.medi_LamLamYaaIsol <anchor NULL> <anchor 0 -145>;
    pos cursive \uni06B5.medi_LamLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B7.medi_LamLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni0644.medi_LamLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B8.medi_LamLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B6.medi_LamLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni076A.medi_LamLamMemMedi <anchor NULL> <anchor 85 170>;
    pos cursive \uni06B5.medi_LamLamYaaFina <anchor NULL> <anchor 0 -145>;
    pos cursive \uni06B7.medi_LamLamYaaFina <anchor NULL> <anchor 0 -145>;
    pos cursive \uni0644.medi_LamLamYaaFina <anchor NULL> <anchor 0 -145>;
    pos cursive \uni06B8.medi_LamLamYaaFina <anchor NULL> <anchor 0 -145>;
    pos cursive \uni06B6.medi_LamLamYaaFina <anchor NULL> <anchor 0 -145>;
    pos cursive \uni076A.medi_LamLamYaaFina <anchor NULL> <anchor 0 -145>;
    pos cursive \uni062E.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni062D.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0681.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0687.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0685.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni062C.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0682.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0757.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0684.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni076F.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni076E.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0683.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni06BF.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni077C.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0758.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0772.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0686.medi_1LamHaaHaaInit <anchor NULL> <anchor 0 473>;
    pos cursive \uni0680.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni06BD.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni067E.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni067B.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0628.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0767.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni063D.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0777.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0776.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0775.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni06CC.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni064A.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni06CE.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0751.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0750.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0753.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0752.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0755.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni0754.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni06B9.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni06D1.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni06D0.init_BaaBaaIsolLD <anchor NULL> <anchor 0 169>;
    pos cursive \uni08A0.init_AboveHaa <anchor NULL> <anchor 0 420>;
} cursCursiveAttachment;

lookup markGenericMarkAnchor {
  lookupflag 0;
  markClass [\uni0654 ] <anchor -1 830> @MarkAbove;
  markClass [\uni0653 ] <anchor -1 1570> @MarkAbove;
  markClass [\uni065A \uni065B ] <anchor 0 840> @MarkAbove;
  markClass [\uni064B ] <anchor 529 1640> @MarkAbove;
  markClass [\uni064C \uni0618 ] <anchor 509 1640> @MarkAbove;
  markClass [\uni064E \uni0659 \uni065E \uni0657 ] <anchor 549 1640> @MarkAbove;
  markClass [\uni064F \uni065D ] <anchor 569 1640> @MarkAbove;
  markClass [\uni0651 ] <anchor 429 1640> @MarkAbove;
  markClass [\uni0652 \uni0658 ] <anchor 369 1640> @MarkAbove;
  markClass [\uni0674 ] <anchor -400 830> @MarkAbove;
  markClass [\uni0670 ] <anchor 389 1640> @MarkAbove;
  markClass [\uni0615 \uni0617 ] <anchor 478 2600> @MarkAbove;
  markClass [\uni0619 ] <anchor 499 1640> @MarkAbove;
  markClass [\uni06E1 \uni06DF \uni06E0 ] <anchor 368 1640> @MarkAbove;
  markClass [\uni06E4 ] <anchor 0 1600> @MarkAbove;
  markClass [\uni06E2 \uni06E7 ] <anchor 318 1640> @MarkAbove;
  markClass [\uni06D6 \uni06D7 ] <anchor 499 2600> @MarkAbove;
  markClass [\uni06DB ] <anchor 379 2600> @MarkAbove;
  markClass [\uni06DC ] <anchor 589 2600> @MarkAbove;
  markClass [\uni06DA ] <anchor 399 2600> @MarkAbove;
  markClass [\uni06D9 ] <anchor 389 2600> @MarkAbove;
  markClass [\uni06E8 ] <anchor 288 1640> @MarkAbove;
  markClass [\uni06EC ] <anchor 228 1640> @MarkAbove;
  markClass [\uni06D8 ] <anchor 429 2600> @MarkAbove;
  markClass [\uni06EB ] <anchor 179 250> @MarkAbove;
  markClass [\uni0616 ] <anchor 439 1640> @MarkAbove;
  markClass [\uni030A ] <anchor 254 1640> @MarkAbove;
  pos base [\uni25CC ] <anchor 696 1210> mark @MarkAbove;
  pos base [\uni0640.1 ] <anchor 190 1640> mark @MarkAbove;
  pos base [\uni0640.2 ] <anchor 359 1640> mark @MarkAbove;
  pos base [\uni0640.3 ] <anchor 750 1640> mark @MarkAbove;
  pos base [\uni0640.4 ] <anchor 1250 1640> mark @MarkAbove;
  subtable;
  markClass [\uni0655 \uni065F ] <anchor 0 -460> @MarkBelow;
  markClass [\uni064D \uni0650 ] <anchor 450 -670> @MarkBelow;
  markClass [\uni0656 ] <anchor 280 -670> @MarkBelow;
  markClass [\uni061A ] <anchor 510 -670> @MarkBelow;
  markClass [\uni06ED ] <anchor 540 -640> @MarkBelow;
  markClass [\uni06E3 ] <anchor 550 -190> @MarkBelow;
  markClass [\uni06EA ] <anchor 520 -760> @MarkBelow;
  markClass [\uni065C ] <anchor 193 -670> @MarkBelow;
  markClass [\uni0325 ] <anchor 256 -620> @MarkBelow;
  pos base [\uni25CC ] <anchor 696 -510> mark @MarkBelow;
  pos base [\uni0640.1 ] <anchor 190 -670> mark @MarkBelow;
  pos base [\uni0640.2 ] <anchor 350 -670> mark @MarkBelow;
  pos base [\uni0640.3 ] <anchor 750 -670> mark @MarkBelow;
  pos base [\uni0640.4 ] <anchor 1250 -670> mark @MarkBelow;
} markGenericMarkAnchor;

lookup markHamzaAbove {
  lookupflag 0;
  markClass [\uni0654 \hamza.above.wavy \damma.mark \hamzadamma.mark \hamza.above ] <anchor 0 950> @HamzaAbove;
  markClass [\hamza.kaf ] <anchor 0 0> @HamzaAbove;
  markClass [\uni0670 ] <anchor 313 1984> @HamzaAbove;
  markClass [\hamza.wasl ] <anchor 490 1370> @HamzaAbove;
  markClass [\aAlf.dagger ] <anchor 0 1050> @HamzaAbove;
  pos base [\aAlf.fina \aAlf.fina_Narrow ] <anchor 126 1540> mark @HamzaAbove;
  pos base [\aAlf.isol ] <anchor 166 1630> mark @HamzaAbove;
  pos base [\aAyn.init ] <anchor 300 1100> mark @HamzaAbove;
  pos base [\aAyn.isol ] <anchor 370 1230> mark @HamzaAbove;
  pos base [\aAyn.medi ] <anchor 260 1000> mark @HamzaAbove;
  pos base [\aBaa.fina \aBaa.isol ] <anchor 945 870> mark @HamzaAbove;
  pos base [\aBaa.init ] <anchor 175 800> mark @HamzaAbove;
  pos base [\aBaa.medi ] <anchor 250 740> mark @HamzaAbove;
  pos base [\aDal.fina ] <anchor 685 1300> mark @HamzaAbove;
  pos base [\aDal.isol ] <anchor 420 1200> mark @HamzaAbove;
  pos base [\aFaa.fina ] <anchor 1400 1000> mark @HamzaAbove;
  pos base [\aFaa.init ] <anchor 160 1150> mark @HamzaAbove;
  pos base [\aFaa.isol ] <anchor 1300 1350> mark @HamzaAbove;
  pos base [\aFaa.medi ] <anchor 400 1200> mark @HamzaAbove;
  pos base [\aHaa.fina \aHaa.isol ] <anchor 700 900> mark @HamzaAbove;
  pos base [\aHaa.init ] <anchor 620 950> mark @HamzaAbove;
  pos base [\aHaa.medi ] <anchor 650 950> mark @HamzaAbove;
  pos base [\aHeh.fina \uni0647.fina \uni06C1.fina \uni06D5.fina ] <anchor 400 1100> mark @HamzaAbove;
  pos base [\aHeh.isol \uni0647 \uni06C1 \uni06D5 ] <anchor 368 1090> mark @HamzaAbove;
  pos base [\aKaf.fina ] <anchor 730 626> mark @HamzaAbove;
  pos base [\aKaf.isol ] <anchor 420 620> mark @HamzaAbove;
  pos base [\aNon.fina.alt ] <anchor 1982 166> mark @HamzaAbove;
  pos base [\aNon.fina ] <anchor 680 500> mark @HamzaAbove;
  pos base [\aNon.isol.alt ] <anchor 2200 560> mark @HamzaAbove;
  pos base [\aNon.isol ] <anchor 550 1000> mark @HamzaAbove;
  pos base [\aQaf.fina ] <anchor 960 740> mark @HamzaAbove;
  pos base [\aQaf.isol ] <anchor 960 1100> mark @HamzaAbove;
  pos base [\aRaa.fina.alt2 \aRaa.fina_MemRaaIsol ] <anchor 729 728> mark @HamzaAbove;
  pos base [\aRaa.fina ] <anchor 658 814> mark @HamzaAbove;
  pos base [\aRaa.isol ] <anchor 378 938> mark @HamzaAbove;
  pos base [\aSad.fina ] <anchor 1684 1026> mark @HamzaAbove;
  pos base [\aSad.init ] <anchor 634 894> mark @HamzaAbove;
  pos base [\aSad.isol ] <anchor 1681 1110> mark @HamzaAbove;
  pos base [\aSad.medi ] <anchor 712 987> mark @HamzaAbove;
  pos base [\aSen.fina ] <anchor 1595 750> mark @HamzaAbove;
  pos base [\aSen.init ] <anchor 650 900> mark @HamzaAbove;
  pos base [\aSen.isol ] <anchor 1532 1070> mark @HamzaAbove;
  pos base [\aSen.medi ] <anchor 595 777> mark @HamzaAbove;
  pos base [\aWaw.fina ] <anchor 703 773> mark @HamzaAbove;
  pos base [\aWaw.isol ] <anchor 710 900> mark @HamzaAbove;
  pos base [\aYaa.fina ] <anchor 330 500> mark @HamzaAbove;
  pos base [\aYaa.isol ] <anchor 385 828> mark @HamzaAbove;
  pos base [\aBaa.init_BaaRaaIsol ] <anchor 244 950> mark @HamzaAbove;
  pos base [\aRaa.fina_BaaRaaIsol ] <anchor 642 902> mark @HamzaAbove;
  pos base [\aWaw.fina_LamWawFina ] <anchor 705 820> mark @HamzaAbove;
  pos base [\aFaa.medi_FaaYaaFina ] <anchor 498 1200> mark @HamzaAbove;
  pos base [\aYaa.fina_FaaYaaFina ] <anchor 270 380> mark @HamzaAbove;
  pos base [\aHaa.medi_LamLamHaaInit ] <anchor 100 900> mark @HamzaAbove;
  pos base [\aBaa.medi_BaaMemFina ] <anchor 367 800> mark @HamzaAbove;
  pos base [\aSad.init_AboveHaa ] <anchor 928 1410> mark @HamzaAbove;
  pos base [\aBaa.medi_LamBaaMemInit ] <anchor 17 1230> mark @HamzaAbove;
  pos base [\aBaa.init_BaaDal ] <anchor 110 1024> mark @HamzaAbove;
  pos base [\aDal.fina_BaaDal ] <anchor 632 1275> mark @HamzaAbove;
  pos base [\aBaa.init_BaaMemHaaInit ] <anchor 91 1726> mark @HamzaAbove;
  pos base [\aHaa.medi_BaaMemHaaInit ] <anchor 182 1100> mark @HamzaAbove;
  pos base [\aBaa.init_BaaBaaYaa ] <anchor 129 1265> mark @HamzaAbove;
  pos base [\aBaa.medi_BaaBaaYaa ] <anchor 134 1173> mark @HamzaAbove;
  pos base [\aYaa.fina_BaaBaaYaa ] <anchor 326 795> mark @HamzaAbove;
  pos base [\aYaa.fina_LamYaaFina ] <anchor 382 535> mark @HamzaAbove;
  pos base [\aBaa.medi_KafBaaInit \aBaa.medi_KafBaaMedi ] <anchor 150 1300> mark @HamzaAbove;
  pos base [\aAlf.fina_LamAlfIsol ] <anchor 435 1609> mark @HamzaAbove;
  pos base [\aHaa.medi_LamHaaMemInit ] <anchor 741 1100> mark @HamzaAbove;
  pos base [\aBaa.medi_BaaBaaInit ] <anchor 250 600> mark @HamzaAbove;
  pos base [\aFaa.init_FaaHaaInit ] <anchor 135 1510> mark @HamzaAbove;
  pos base [\aHaa.medi_FaaHaaInit ] <anchor 900 900> mark @HamzaAbove;
  pos base [\aHaa.init_HaaHaaInit ] <anchor 1107 1210> mark @HamzaAbove;
  pos base [\aQaf.fina_LamQafFina ] <anchor 974 886> mark @HamzaAbove;
  pos base [\aBaa.init_BaaNonIsol ] <anchor 204 1040> mark @HamzaAbove;
  pos base [\aNon.fina_BaaNonIsol ] <anchor 660 907> mark @HamzaAbove;
  pos base [\aBaa.init_BaaSenInit ] <anchor 120 1400> mark @HamzaAbove;
  pos base [\aSen.medi_BaaSenInit ] <anchor 630 840> mark @HamzaAbove;
  pos base [\aBaa.medi_BaaRaaFina ] <anchor 280 1000> mark @HamzaAbove;
  pos base [\aRaa.fina_BaaRaaFina ] <anchor 654 939> mark @HamzaAbove;
  pos base [\aRaa.fina_KafRaaFina ] <anchor 500 500> mark @HamzaAbove;
  pos base [\aHaa.medi_MemHaaMemInit ] <anchor 30 1295> mark @HamzaAbove;
  pos base [\aBaa.init_BaaMemInit ] <anchor 132 1210> mark @HamzaAbove;
  pos base [\aRaa.fina_KafRaaIsol ] <anchor 500 550> mark @HamzaAbove;
  pos base [\aAyn.init_AynHaaInit ] <anchor 428 1535> mark @HamzaAbove;
  pos base [\aYaa.fina_KafYaaFina ] <anchor 297 515> mark @HamzaAbove;
  pos base [\aHaa.medi_LamMemHaaInit ] <anchor 253 1000> mark @HamzaAbove;
  pos base [\aAlf.fina_LamAlfFina ] <anchor 279 1500> mark @HamzaAbove;
  pos base [\aBaa.init_BaaBaaHaaInit ] <anchor 195 1260> mark @HamzaAbove;
  pos base [\aBaa.medi_BaaBaaHaaInit ] <anchor 526 1227> mark @HamzaAbove;
  pos base [\aHaa.medi_BaaBaaHaaInit ] <anchor 156 1000> mark @HamzaAbove;
  pos base [\aBaa.medi_SenBaaMemInit ] <anchor -104 1250> mark @HamzaAbove;
  pos base [\aBaa.init_BaaBaaIsol ] <anchor 270 825> mark @HamzaAbove;
  pos base [\aBaa.fina_BaaBaaIsol ] <anchor 867 870> mark @HamzaAbove;
  pos base [\aBaa.init_BaaBaaMemInit ] <anchor 187 1280> mark @HamzaAbove;
  pos base [\aBaa.medi_BaaBaaMemInit ] <anchor -64 1128> mark @HamzaAbove;
  pos base [\aBaa.medi_BaaNonFina ] <anchor 169 884> mark @HamzaAbove;
  pos base [\aNon.fina_BaaNonFina ] <anchor 636 700> mark @HamzaAbove;
  pos base [\aHaa.init_HaaRaaIsol ] <anchor 709 970> mark @HamzaAbove;
  pos base [\aRaa.fina_HaaRaaIsol ] <anchor 700 734> mark @HamzaAbove;
  pos base [\aRaa.fina_LamRaaIsol ] <anchor 671 944> mark @HamzaAbove;
  pos base [\aSad.init_SadHaaInit ] <anchor 872 1411> mark @HamzaAbove;
  pos base [\aHaa.medi_SadHaaInit ] <anchor 180 1000> mark @HamzaAbove;
  pos base [\aBaa.medi_BaaYaaFina ] <anchor 200 800> mark @HamzaAbove;
  pos base [\aYaa.fina_BaaYaaFina ] <anchor 355 552> mark @HamzaAbove;
  pos base [\aBaa.init_BaaSenAltInit ] <anchor 120 1262> mark @HamzaAbove;
  pos base [\aSen.medi_BaaSenAltInit ] <anchor 224 937> mark @HamzaAbove;
  pos base [\aRaa.fina_PostTooth ] <anchor 666 696> mark @HamzaAbove;
  pos base [\aYaa.fina_PostTooth ] <anchor 355 550> mark @HamzaAbove;
  pos base [\aBaa.init_AboveHaa ] <anchor 669 1427> mark @HamzaAbove;
  pos base [\aBaa.init_BaaHaaInit ] <anchor 750 1400> mark @HamzaAbove;
  pos base [\aBaa.init_BaaHaaMemInit ] <anchor 438 -235> mark @HamzaAbove;
  pos base [\aHaa.medi_BaaHaaMemInit ] <anchor -74 1285> mark @HamzaAbove;
  pos base [\aHaa.fina_AboveHaaIsol ] <anchor 327 1110> mark @HamzaAbove;
  pos base [\aHaa.medi_1LamHaaHaaInit ] <anchor 1011 1210> mark @HamzaAbove;
  pos base [\aHaa.medi_2LamHaaHaaInit ] <anchor 80 1210> mark @HamzaAbove;
  pos base [\aAyn.init_Finjani ] <anchor 229 1034> mark @HamzaAbove;
  pos base [\aHaa.init_Finjani ] <anchor 370 922> mark @HamzaAbove;
  pos base [\aHaa.medi_Finjani ] <anchor 340 955> mark @HamzaAbove;
  pos base [\aSen.medi_PreYaa ] <anchor 225 777> mark @HamzaAbove;
  pos base [\aSad.init_PreYaa ] <anchor 300 1000> mark @HamzaAbove;
  pos base [\aSad.medi_PreYaa ] <anchor 275 980> mark @HamzaAbove;
  pos base [\aBaa.init_High ] <anchor 145 800> mark @HamzaAbove;
  pos base [\aBaa.medi_High ] <anchor 195 1124> mark @HamzaAbove;
  pos base [\aBaa.init_Wide ] <anchor 235 1025> mark @HamzaAbove;
  pos base [\aHaa.medi_HaaHaaInit ] <anchor 82 1210> mark @HamzaAbove;
  pos base [\aHaa.medi_AynHaaInit ] <anchor 60 1000> mark @HamzaAbove;
  pos base [\aHaa.init_AboveHaa ] <anchor 1267 1339> mark @HamzaAbove;
  pos base [\aAyn.init_AboveHaa ] <anchor 428 1485> mark @HamzaAbove;
  pos base [\aHaa.fina_AboveHaaIsol2 ] <anchor 1057 790> mark @HamzaAbove;
  pos base [\uni06D2 \aYaaBarree.isol ] <anchor 700 1100> mark @HamzaAbove;
  pos base [\aKaf.fina_KafKafFina ] <anchor 732 679> mark @HamzaAbove;
  pos base [\aAlf.fina_KafAlf ] <anchor 126 1569> mark @HamzaAbove;
  pos base [\aAlf.fina_KafMemAlf ] <anchor 120 1588> mark @HamzaAbove;
  pos base [\aHeh.fina_KafHeh \uni0647.fina_KafHeh \uni06C1.fina_KafHeh \uni06D5.fina_KafHeh ] <anchor 170 870> mark @HamzaAbove;
  pos base [\aDal.fina_KafDal ] <anchor 290 1075> mark @HamzaAbove;
  pos base [\aHeh.fina_LamHeh \uni0647.fina_LamHeh \uni06C1.fina_LamHeh \uni06D5.fina_LamHeh ] <anchor 167 824> mark @HamzaAbove;
  pos base [\aDal.fina_LamDal ] <anchor 592 1074> mark @HamzaAbove;
  pos base [\aAyn.init_AynMemInit ] <anchor 340 1471> mark @HamzaAbove;
  pos base [\aFaa.init_FaaMemInit ] <anchor 5 1448> mark @HamzaAbove;
  pos base [\aHaa.init_HaaMemInit ] <anchor 792 1153> mark @HamzaAbove;
  pos base [\aSen.init_SenMemInit ] <anchor 509 1204> mark @HamzaAbove;
  pos base [\aSad.init_SadMemInit ] <anchor 515 1434> mark @HamzaAbove;
  pos base [\aBaa.init_BaaYaaIsol ] <anchor 123 1300> mark @HamzaAbove;
  pos base [\aHaa.init_HaaYaaIsol ] <anchor 779 1170> mark @HamzaAbove;
  pos base [\aFaa.init_FaaYaaIsol ] <anchor -1 1430> mark @HamzaAbove;
  pos base [\aAyn.init_AynYaaIsol ] <anchor 526 1395> mark @HamzaAbove;
  pos base [\aYaa.fina_KafYaaIsol ] <anchor 270 730> mark @HamzaAbove;
  pos base [\aBaa.init_BaaMemIsol ] <anchor 446 1131> mark @HamzaAbove;
  pos base [\aBaa.medi_BaaMemAlfFina ] <anchor 201 800> mark @HamzaAbove;
  pos base [\aAlf.fina_MemAlfFina ] <anchor 100 1500> mark @HamzaAbove;
  pos base [\aBaa.init_BaaHehInit ] <anchor 463 900> mark @HamzaAbove;
  pos base [\aBaa.medi_BaaHehMedi ] <anchor 355 900> mark @HamzaAbove;
  pos base [\aKaf.fina_LamKafIsol ] <anchor 668 581> mark @HamzaAbove;
  pos base [\aKaf.fina_LamKafFina ] <anchor 744 583> mark @HamzaAbove;
  pos base [\aYaaBarree.fina_PostTooth \uni06D2.fina_PostTooth ] <anchor 600 1100> mark @HamzaAbove;
  pos base [\aSad.init_YaaBarree ] <anchor 348 1285> mark @HamzaAbove;
  pos base [\aYaaBarree.fina_PostAscender \uni06D2.fina_PostAscender \aYaaBarree.fina_PostAyn \uni06D2.fina_PostAyn ] <anchor 236 773> mark @HamzaAbove;
  pos base [\aBaa.init_YaaBarree ] <anchor -297 1524> mark @HamzaAbove;
  pos base [\aFaa.init_YaaBarree ] <anchor -319 1561> mark @HamzaAbove;
  pos base [\aHaa.init_YaaBarree ] <anchor 1117 1089> mark @HamzaAbove;
  pos base [\aAyn.init_YaaBarree ] <anchor 466 1465> mark @HamzaAbove;
  pos base [\aBaa.init_BaaBaaHeh ] <anchor 180 700> mark @HamzaAbove;
} markHamzaAbove;

lookup markTashkilAboveBase {
  lookupflag 0;
  markClass [\uni0654 \uni065A \uni065B ] <anchor 0 950> @TashkilAbove;
  markClass [\uni0653 ] <anchor 0 1640> @TashkilAbove;
  markClass [\uni064B ] <anchor 529 1640> @TashkilAbove;
  markClass [\uni064C ] <anchor 510 1640> @TashkilAbove;
  markClass [\uni064E \uni0659 \uni065E \uni0657 ] <anchor 556 1640> @TashkilAbove;
  markClass [\uni064F \uni065D ] <anchor 570 1640> @TashkilAbove;
  markClass [\uni0651 ] <anchor 427 1640> @TashkilAbove;
  markClass [\uni0652 \uni06E1 \uni06DF \uni06E0 \uni0658 ] <anchor 372 1640> @TashkilAbove;
  markClass [\uni0674 ] <anchor -400 950> @TashkilAbove;
  markClass [\uni0670 ] <anchor 395 1640> @TashkilAbove;
  markClass [\uni064F.small ] <anchor 0 1870> @TashkilAbove;
  markClass [\uni0652.small2 ] <anchor -50 2206> @TashkilAbove;
  markClass [\uni0619 ] <anchor 500 1640> @TashkilAbove;
  markClass [\uni0618 ] <anchor 506 1640> @TashkilAbove;
  markClass [\uni08F0 ] <anchor 499 1640> @TashkilAbove;
  markClass [\uni08F1 ] <anchor 490 1640> @TashkilAbove;
  markClass [\uni064E.small2 ] <anchor -40 2185> @TashkilAbove;
  markClass [\uni06E4 ] <anchor 0 1658> @TashkilAbove;
  markClass [\uni06E2 \uni06E7 ] <anchor 320 1640> @TashkilAbove;
  markClass [\uni06EC ] <anchor 224 1640> @TashkilAbove;
  markClass [\uni06EB ] <anchor 181 250> @TashkilAbove;
  markClass [\uni0616 ] <anchor 437 1640> @TashkilAbove;
  markClass [\uni030A ] <anchor 254 1640> @TashkilAbove;
  pos base [\aAlf.fina.alt \uni0627.fina_Tatweel ] <anchor 750 1500> mark @TashkilAbove;
  pos base [\aAlf.fina \aAlf.fina_Narrow ] <anchor 500 1400> mark @TashkilAbove;
  pos base [\aAlf.isol \uni0625 \uni0627 \uni0673 ] <anchor 200 1550> mark @TashkilAbove;
  pos base [\aAyn.fina \aLam.fina \aLam.isol \aWaw.fina \aYaa.fina \uni06D2 \uni077A \uni077B \aLam.fina_KafLam \aLam.fina_LamLamIsol \aLam.fina_LamLamFina \uni06FC.fina \uni063A.fina \uni075E.fina \uni075D.fina \uni075F.fina \uni06A0.fina \uni0639.fina \uni06B5.fina \uni06B7.fina \uni0644.fina \uni06B8.fina \uni06B6.fina \uni076A.fina \uni06B7 \uni0644 \uni06B8 \uni06B6 \uni076A \uni0777.fina \uni06D1.fina \uni0775.fina \uni063F.fina \uni063D.fina \uni063E.fina \uni06D0.fina \uni0649.fina \uni0776.fina \uni06CD.fina \uni06CC.fina \uni0620.fina \uni064A.fina \uni06CE.fina \uni06B5.fina_KafLam \uni06B7.fina_KafLam \uni0644.fina_KafLam \uni06B8.fina_KafLam \uni06B6.fina_KafLam \uni076A.fina_KafLam \uni06B5.fina_LamLamIsol \uni06B7.fina_LamLamIsol \uni0644.fina_LamLamIsol \uni06B8.fina_LamLamIsol \uni06B6.fina_LamLamIsol \uni076A.fina_LamLamIsol \uni06B5.fina_LamLamFina \uni06B7.fina_LamLamFina \uni0644.fina_LamLamFina \uni06B8.fina_LamLamFina \uni06B6.fina_LamLamFina \uni076A.fina_LamLamFina \aYaaBarree.isol \aAyn.init_YaaBarree \aMem.init_YaaBarree \uni0765.init_YaaBarree \uni0645.init_YaaBarree \uni0766.init_YaaBarree \uni06FC.init_YaaBarree \uni063A.init_YaaBarree \uni075D.init_YaaBarree \uni0639.init_YaaBarree \aHeh.init_YaaBarree \uni0647.init_YaaBarree \uni06C1.init_YaaBarree \aHehKnotted.init_YaaBarree \uni06BE.init_YaaBarree ] <anchor 600 1640> mark @TashkilAbove;
  pos base [\aAyn.init \aHeh.medi \aMem.fina.alt \aMem.init \aMem.isol \aMem.medi \aRaa.isol \aLam.init_LamAlfIsol \aKaf.medi_KafMemFina \aYaa.fina_KafYaaIsol \uni06FC.init \uni063A.init \uni075E.init \uni075D.init \uni075F.init \uni06A0.init \uni0639.init \uni0647.medi \uni06C1.medi \uni0765.init \uni0645.init \uni0766.init \uni0765 \uni0645 \uni0766 \uni0765.medi \uni0645.medi \uni0766.medi \uni0691 \uni0692 \uni0693 \uni0694 \uni0695 \uni0696 \uni0697 \uni0698 \uni0699 \uni075B \uni06EF \uni0632 \uni0631 \uni076B \uni076C \uni0644.init_LamAlfIsol \uni06B8.init_LamAlfIsol \uni076A.init_LamAlfIsol \uni063C.medi_KafMemFina \uni0764.medi_KafMemFina \uni0643.medi_KafMemFina \uni06B0.medi_KafMemFina \uni06B3.medi_KafMemFina \uni06B2.medi_KafMemFina \uni06AB.medi_KafMemFina \uni06AE.medi_KafMemFina \uni06AF.medi_KafMemFina \uni06A9.medi_KafMemFina \uni0777.fina_KafYaaIsol \uni06D1.fina_KafYaaIsol \uni0775.fina_KafYaaIsol \uni063F.fina_KafYaaIsol \uni063D.fina_KafYaaIsol \uni063E.fina_KafYaaIsol \uni06D0.fina_KafYaaIsol \uni0649.fina_KafYaaIsol \uni0776.fina_KafYaaIsol \uni06CD.fina_KafYaaIsol \uni06CC.fina_KafYaaIsol \uni0620.fina_KafYaaIsol \uni064A.fina_KafYaaIsol \uni06CE.fina_KafYaaIsol \uni0645.fina_LamMemFinaExtended \uni0645.fina_KafMemIsolExtended \uni06BE.medi \uni06FF.medi ] <anchor 400 1640> mark @TashkilAbove;
  pos base [\aAyn.isol \aRaa.fina \aSen.medi \aTaa.medi \aRaa.fina_KafRaaFina \aRaa.fina_KafRaaIsol \uni06FC \uni063A \uni075E \uni075D \uni075F \uni06A0 \uni0639 \uni06FA.medi \uni076D.medi \uni0633.medi \uni077E.medi \uni077D.medi \uni0634.medi \uni075C.medi \uni069A.medi \uni069B.medi \uni069C.medi \uni0638.medi \uni0637.medi \uni069F.medi \uni0677.fina \aYaaBarree.fina_PostTooth \uni077B.fina_PostTooth \uni077A.fina_PostTooth \uni06D2.fina_PostTooth ] <anchor 500 1640> mark @TashkilAbove;
  pos base [\aAyn.medi \aDal.fina_LamDal \aMem.init_MemYaaIsol \uni06FC.medi \uni063A.medi \uni075E.medi \uni075D.medi \uni075F.medi \uni06A0.medi \uni0639.medi \uni0690.fina_LamDal \uni06EE.fina_LamDal \uni0689.fina_LamDal \uni075A.fina_LamDal \uni0630.fina_LamDal \uni062F.fina_LamDal \uni068C.fina_LamDal \uni068A.fina_LamDal \uni068F.fina_LamDal \uni068E.fina_LamDal \uni068D.fina_LamDal \uni0765.init_MemYaaIsol \uni0645.init_MemYaaIsol \uni0766.init_MemYaaIsol ] <anchor 350 1640> mark @TashkilAbove;
  pos base [\aBaa.fina \aBaa.isol \uni0751.fina \uni0750.fina \uni0753.fina \uni0680.fina \uni062A.fina \uni0754.fina \uni062B.fina \uni0679.fina \uni067C.fina \uni0756.fina \uni0752.fina \uni066E.fina \uni067F.fina \uni0755.fina \uni067D.fina \uni067E.fina \uni067B.fina \uni0628.fina \uni067A.fina \uni0751 \uni0750 \uni0753 \uni0680 \uni062A \uni0754 \uni062B \uni0679 \uni067C \uni0756 \uni0752 \uni066E \uni067F \uni0755 \uni067D \uni067E \uni067B \uni0628 \uni067A \uni08A0.fina \uni08A0 ] <anchor 945 1640> mark @TashkilAbove;
  pos base [\aBaa.init \aLam.medi_LamAlfFina \uni0777.init \uni0680.init \uni0776.init \uni06BC.init \uni0750.init \uni0756.init \uni06CE.init \uni0775.init \uni06BD.init \uni0626.init \uni066E.init \uni0620.init \uni064A.init \uni06BB.init \uni067F.init \uni0755.init \uni067D.init \uni067E.init \uni067B.init \uni0628.init \uni067A.init \uni0751.init \uni0646.init \uni0753.init \uni0752.init \uni062A.init \uni0678.init \uni063D.init \uni062B.init \uni0679.init \uni06B9.init \uni0769.init \uni0649.init \uni067C.init \uni0754.init \uni06D1.init \uni06D0.init \uni06BA.init \uni06CC.init \uni0767.init \uni0644.medi_LamAlfFina \uni06B8.medi_LamAlfFina \uni076A.medi_LamAlfFina \uni08A0.init \uni08A0.init_LD ] <anchor 120 1640> mark @TashkilAbove;
  pos base [\aBaa.medi \aLam.medi_LamMemFina \aBaa.medi_BaaBaaInit \aMem.init_MemHaaMemInit \aHaa.medi_MemHaaMemInit \aKaf.medi_KafYaaFina \uni0644.medi_LamMemFina \uni06B8.medi_LamMemFina \uni076A.medi_LamMemFina \uni0777.medi_BaaBaaInit \uni0680.medi_BaaBaaInit \uni0776.medi_BaaBaaInit \uni06BC.medi_BaaBaaInit \uni0750.medi_BaaBaaInit \uni0756.medi_BaaBaaInit \uni0768.medi_BaaBaaInit \uni06CE.medi_BaaBaaInit \uni0775.medi_BaaBaaInit \uni06BD.medi_BaaBaaInit \uni066E.medi_BaaBaaInit \uni0620.medi_BaaBaaInit \uni064A.medi_BaaBaaInit \uni06BB.medi_BaaBaaInit \uni067F.medi_BaaBaaInit \uni0755.medi_BaaBaaInit \uni067D.medi_BaaBaaInit \uni067E.medi_BaaBaaInit \uni067B.medi_BaaBaaInit \uni0628.medi_BaaBaaInit \uni067A.medi_BaaBaaInit \uni0751.medi_BaaBaaInit \uni0646.medi_BaaBaaInit \uni0753.medi_BaaBaaInit \uni0752.medi_BaaBaaInit \uni062A.medi_BaaBaaInit \uni063D.medi_BaaBaaInit \uni062B.medi_BaaBaaInit \uni0679.medi_BaaBaaInit \uni06B9.medi_BaaBaaInit \uni0769.medi_BaaBaaInit \uni0649.medi_BaaBaaInit \uni067C.medi_BaaBaaInit \uni0754.medi_BaaBaaInit \uni06D1.medi_BaaBaaInit \uni06D0.medi_BaaBaaInit \uni06BA.medi_BaaBaaInit \uni06CC.medi_BaaBaaInit \uni0767.medi_BaaBaaInit \uni0645.init_MemHaaMemInit \uni062E.medi_MemHaaMemInit \uni062D.medi_MemHaaMemInit \uni0681.medi_MemHaaMemInit \uni0687.medi_MemHaaMemInit \uni062C.medi_MemHaaMemInit \uni0682.medi_MemHaaMemInit \uni0757.medi_MemHaaMemInit \uni0684.medi_MemHaaMemInit \uni076F.medi_MemHaaMemInit \uni076E.medi_MemHaaMemInit \uni0683.medi_MemHaaMemInit \uni06BF.medi_MemHaaMemInit \uni077C.medi_MemHaaMemInit \uni0758.medi_MemHaaMemInit \uni0686.medi_MemHaaMemInit \uni063C.medi_KafYaaFina \uni0764.medi_KafYaaFina \uni0643.medi_KafYaaFina \uni06B0.medi_KafYaaFina \uni06B3.medi_KafYaaFina \uni06B2.medi_KafYaaFina \uni06AB.medi_KafYaaFina \uni06AC.medi_KafYaaFina \uni06AE.medi_KafYaaFina \uni06AF.medi_KafYaaFina \uni06A9.medi_KafYaaFina \uni0762.medi_KafYaaFina \uni0777.init_BaaBaaIsol \uni0680.init_BaaBaaIsol \uni0776.init_BaaBaaIsol \uni06BC.init_BaaBaaIsol \uni0750.init_BaaBaaIsol \uni0756.init_BaaBaaIsol \uni0768.init_BaaBaaIsol \uni06CE.init_BaaBaaIsol \uni0775.init_BaaBaaIsol \uni06BD.init_BaaBaaIsol \uni0626.init_BaaBaaIsol \uni066E.init_BaaBaaIsol \uni0620.init_BaaBaaIsol \uni064A.init_BaaBaaIsol \uni06BB.init_BaaBaaIsol \uni067F.init_BaaBaaIsol \uni0755.init_BaaBaaIsol \uni067D.init_BaaBaaIsol \uni067E.init_BaaBaaIsol \uni067B.init_BaaBaaIsol \uni0628.init_BaaBaaIsol \uni067A.init_BaaBaaIsol \uni0751.init_BaaBaaIsol \uni0646.init_BaaBaaIsol \uni0753.init_BaaBaaIsol \uni0752.init_BaaBaaIsol \uni062A.init_BaaBaaIsol \uni0678.init_BaaBaaIsol \uni063D.init_BaaBaaIsol \uni062B.init_BaaBaaIsol \uni0679.init_BaaBaaIsol \uni06B9.init_BaaBaaIsol \uni0769.init_BaaBaaIsol \uni0649.init_BaaBaaIsol \uni067C.init_BaaBaaIsol \uni0754.init_BaaBaaIsol \uni06D1.init_BaaBaaIsol \uni06D0.init_BaaBaaIsol \uni06BA.init_BaaBaaIsol \uni06CC.init_BaaBaaIsol \uni0767.init_BaaBaaIsol \uni0645.medi_LamMemInitTatweel \uni0680.init_BaaBaaIsolLD \uni06BD.init_BaaBaaIsolLD \uni067E.init_BaaBaaIsolLD \uni067B.init_BaaBaaIsolLD \uni0628.init_BaaBaaIsolLD \uni0767.init_BaaBaaIsolLD \uni063D.init_BaaBaaIsolLD \uni0777.init_BaaBaaIsolLD \uni0776.init_BaaBaaIsolLD \uni0775.init_BaaBaaIsolLD \uni06CC.init_BaaBaaIsolLD \uni064A.init_BaaBaaIsolLD \uni06CE.init_BaaBaaIsolLD \uni0751.init_BaaBaaIsolLD \uni0750.init_BaaBaaIsolLD \uni0753.init_BaaBaaIsolLD \uni0752.init_BaaBaaIsolLD \uni0755.init_BaaBaaIsolLD \uni0754.init_BaaBaaIsolLD \uni06B9.init_BaaBaaIsolLD \uni06D1.init_BaaBaaIsolLD \uni06D0.init_BaaBaaIsolLD \aYaaBarree.fina_PostAscender \uni077B.fina_PostAscender \uni077A.fina_PostAscender \uni06D2.fina_PostAscender \aYaaBarree.fina_PostAyn \uni077B.fina_PostAyn \uni077A.fina_PostAyn \uni06D2.fina_PostAyn \uni08A0.medi \uni08A0.medi_BaaBaaInit ] <anchor 250 1640> mark @TashkilAbove;
  pos base [\aDal.fina \uni06EE.fina \uni0689.fina \uni075A.fina \uni0630.fina \uni062F.fina \uni068C.fina \uni068A.fina \uni068D.fina ] <anchor 475 1640> mark @TashkilAbove;
  pos base [\aDal.isol \aKaf.isol \aHeh.init_HehYaaIsol \uni0690 \uni06EE \uni0689 \uni075A \uni0630 \uni062F \uni068C \uni068A \uni068F \uni068E \uni068D \uni063B \uni063C \uni077F \uni0764 \uni0643 \uni06B0 \uni06B3 \uni06B2 \uni06AB \uni06AC \uni06AE \uni06AF \uni06A9 \uni0762 \uni06B1 \uni0647.init_HehYaaIsol \uni06C1.init_HehYaaIsol ] <anchor 420 1640> mark @TashkilAbove;
  pos base [\aFaa.fina \uni0760.fina \uni0761.fina \uni0641.fina \uni06A1.fina \uni06A2.fina \uni06A3.fina \uni06A4.fina \uni06A5.fina \uni06A6.fina ] <anchor 1400 1640> mark @TashkilAbove;
  pos base [\aFaa.init \aHaa.medi_2LamHaaHaaInit \uni066F.init \uni0761.init \uni0760.init \uni0642.init \uni0641.init \uni06A8.init \uni06A1.init \uni06A2.init \uni06A3.init \uni06A4.init \uni06A5.init \uni06A6.init \uni06A7.init \uni062E.medi_2LamHaaHaaInit \uni062D.medi_2LamHaaHaaInit \uni0681.medi_2LamHaaHaaInit \uni0687.medi_2LamHaaHaaInit \uni0685.medi_2LamHaaHaaInit \uni062C.medi_2LamHaaHaaInit \uni0682.medi_2LamHaaHaaInit \uni0757.medi_2LamHaaHaaInit \uni0684.medi_2LamHaaHaaInit \uni076F.medi_2LamHaaHaaInit \uni076E.medi_2LamHaaHaaInit \uni0683.medi_2LamHaaHaaInit \uni06BF.medi_2LamHaaHaaInit \uni077C.medi_2LamHaaHaaInit \uni0758.medi_2LamHaaHaaInit \uni0686.medi_2LamHaaHaaInit ] <anchor 160 1640> mark @TashkilAbove;
  pos base [\aFaa.isol \uni0760 \uni0761 \uni0641 \uni06A1 \uni06A2 \uni06A3 \uni06A5 ] <anchor 1300 1640> mark @TashkilAbove;
  pos base [\aFaa.medi \uni066F.medi \uni0761.medi \uni0760.medi \uni0642.medi \uni0641.medi \uni06A8.medi \uni06A1.medi \uni06A2.medi \uni06A3.medi \uni06A4.medi \uni06A5.medi \uni06A6.medi \uni06A7.medi \uni06BE.init \uni06FF.init ] <anchor 450 1640> mark @TashkilAbove;
  pos base [\aHaa.fina \aHaa.isol \aQaf.fina \aSad.medi \aTaa.init \aYaa.fina_FaaYaaFina \uni06BE \uni06FF \uni062E.fina \uni062D.fina \uni0681.fina \uni0687.fina \uni0685.fina \uni062C.fina \uni0682.fina \uni0757.fina \uni0684.fina \uni076F.fina \uni076E.fina \uni0683.fina \uni06BF.fina \uni077C.fina \uni0758.fina \uni0772.fina \uni0686.fina \uni062E \uni062D \uni0681 \uni0687 \uni0685 \uni062C \uni0682 \uni0757 \uni0684 \uni076F \uni076E \uni0683 \uni06BF \uni077C \uni0758 \uni0772 \uni0686 \uni06A8.fina \uni06A7.fina \uni0642.fina \uni066F.fina \uni069D.medi \uni06FB.medi \uni0636.medi \uni069E.medi \uni0638.init \uni0637.init \uni069F.init \uni0777.fina_FaaYaaFina \uni06D1.fina_FaaYaaFina \uni0775.fina_FaaYaaFina \uni063F.fina_FaaYaaFina \uni0678.fina_FaaYaaFina \uni063D.fina_FaaYaaFina \uni063E.fina_FaaYaaFina \uni06D0.fina_FaaYaaFina \uni0649.fina_FaaYaaFina \uni0776.fina_FaaYaaFina \uni06CD.fina_FaaYaaFina \uni06CC.fina_FaaYaaFina \uni0626.fina_FaaYaaFina \uni0620.fina_FaaYaaFina \uni064A.fina_FaaYaaFina \uni06CE.fina_FaaYaaFina \aTaa.init_YaaBaree \uni0638.init_YaaBarree \uni0637.init_YaaBarree \aHehKnotted.isol ] <anchor 700 1640> mark @TashkilAbove;
  pos base [\aHaa.init ] <anchor 620 1640> mark @TashkilAbove;
  pos base [\aHaa.medi \aKaf.init \aSen.init \uni062E.medi \uni062D.medi \uni0681.medi \uni0687.medi \uni0685.medi \uni062C.medi \uni0682.medi \uni0757.medi \uni0684.medi \uni076F.medi \uni076E.medi \uni0683.medi \uni06BF.medi \uni077C.medi \uni0758.medi \uni0772.medi \uni0686.medi \uni063B.init \uni063C.init \uni077F.init \uni0764.init \uni0643.init \uni06B0.init \uni06B3.init \uni06B2.init \uni06AB.init \uni06AC.init \uni06AD.init \uni06AE.init \uni06AF.init \uni06A9.init \uni06B4.init \uni0763.init \uni0762.init \uni06B1.init \uni06FA.init \uni076D.init \uni0633.init \uni077E.init \uni077D.init \uni0634.init \uni075C.init \uni069A.init \uni069B.init \uni069C.init \uni06FA.init_SenBaaMemInit \uni076D.init_SenBaaMemInit \uni0633.init_SenBaaMemInit \uni077E.init_SenBaaMemInit \uni077D.init_SenBaaMemInit \uni0634.init_SenBaaMemInit \uni075C.init_SenBaaMemInit \uni069A.init_SenBaaMemInit \uni069B.init_SenBaaMemInit \uni069C.init_SenBaaMemInit ] <anchor 650 1640> mark @TashkilAbove;
  pos base [\aHeh.fina \aKaf.medi_KafRaaFina \uni0647.fina \uni06C1.fina \uni06C3.fina \uni06D5.fina \uni0629.fina \uni063C.medi_KafRaaFina \uni0764.medi_KafRaaFina \uni0643.medi_KafRaaFina \uni06B0.medi_KafRaaFina \uni06B3.medi_KafRaaFina \uni06B2.medi_KafRaaFina \uni06AB.medi_KafRaaFina \uni06AE.medi_KafRaaFina \uni06AF.medi_KafRaaFina \uni06A9.medi_KafRaaFina ] <anchor 290 1640> mark @TashkilAbove;
  pos base [\aHeh.init \aBaa.medi_BaaMemFina \aKaf.init_KafRaaIsol \aSad.init_PreYaa \aLam.medi_KafLamMemMedi \aHeh.init_HehMemInit \aLam.medi_LamLamMemInit \aLam.medi_LamLamMemMedi \uni0647.init \uni06C1.init \uni06CB.fina \uni0624.fina \uni06CA.fina \uni06CF.fina \uni0778.fina \uni06C6.fina \uni06C7.fina \uni06C4.fina \uni06C5.fina \uni0676.fina \uni06C8.fina \uni06C9.fina \uni0779.fina \uni0648.fina \uni0777.medi_BaaMemFina \uni0680.medi_BaaMemFina \uni0776.medi_BaaMemFina \uni06BC.medi_BaaMemFina \uni0750.medi_BaaMemFina \uni0756.medi_BaaMemFina \uni0768.medi_BaaMemFina \uni06CE.medi_BaaMemFina \uni0775.medi_BaaMemFina \uni06BD.medi_BaaMemFina \uni0626.medi_BaaMemFina \uni066E.medi_BaaMemFina \uni0620.medi_BaaMemFina \uni064A.medi_BaaMemFina \uni06BB.medi_BaaMemFina \uni067F.medi_BaaMemFina \uni0755.medi_BaaMemFina \uni067D.medi_BaaMemFina \uni067E.medi_BaaMemFina \uni067B.medi_BaaMemFina \uni0628.medi_BaaMemFina \uni067A.medi_BaaMemFina \uni0751.medi_BaaMemFina \uni0646.medi_BaaMemFina \uni0753.medi_BaaMemFina \uni0752.medi_BaaMemFina \uni062A.medi_BaaMemFina \uni0678.medi_BaaMemFina \uni063D.medi_BaaMemFina \uni062B.medi_BaaMemFina \uni0679.medi_BaaMemFina \uni06B9.medi_BaaMemFina \uni0769.medi_BaaMemFina \uni0649.medi_BaaMemFina \uni067C.medi_BaaMemFina \uni0754.medi_BaaMemFina \uni06D1.medi_BaaMemFina \uni06D0.medi_BaaMemFina \uni06BA.medi_BaaMemFina \uni06CC.medi_BaaMemFina \uni0767.medi_BaaMemFina \uni063C.init_KafRaaIsol \uni0764.init_KafRaaIsol \uni0643.init_KafRaaIsol \uni06B0.init_KafRaaIsol \uni06B3.init_KafRaaIsol \uni06B2.init_KafRaaIsol \uni06AB.init_KafRaaIsol \uni06AE.init_KafRaaIsol \uni06AF.init_KafRaaIsol \uni06A9.init_KafRaaIsol \uni069D.init_PreYaa \uni06FB.init_PreYaa \uni0636.init_PreYaa \uni069E.init_PreYaa \uni0635.init_PreYaa \uni0777.init_Wide \uni0680.init_Wide \uni06BC.init_Wide \uni0750.init_Wide \uni0756.init_Wide \uni06CE.init_Wide \uni06BD.init_Wide \uni066E.init_Wide \uni0620.init_Wide \uni064A.init_Wide \uni067F.init_Wide \uni0755.init_Wide \uni067D.init_Wide \uni067E.init_Wide \uni067B.init_Wide \uni0628.init_Wide \uni067A.init_Wide \uni0751.init_Wide \uni0646.init_Wide \uni0753.init_Wide \uni0752.init_Wide \uni062A.init_Wide \uni063D.init_Wide \uni062B.init_Wide \uni06B9.init_Wide \uni0649.init_Wide \uni067C.init_Wide \uni0754.init_Wide \uni06D1.init_Wide \uni06D0.init_Wide \uni06BA.init_Wide \uni06CC.init_Wide \uni0767.init_Wide \uni0644.medi_KafLamMemMedi \uni06B8.medi_KafLamMemMedi \uni06B6.medi_KafLamMemMedi \uni076A.medi_KafLamMemMedi \uni0647.init_HehMemInit \uni06C1.init_HehMemInit \uni0644.medi_LamLamMemInit \uni06B8.medi_LamLamMemInit \uni06B6.medi_LamLamMemInit \uni076A.medi_LamLamMemInit \uni0644.medi_LamLamMemMedi \uni06B8.medi_LamLamMemMedi \uni06B6.medi_LamLamMemMedi \uni076A.medi_LamLamMemMedi \uni08A0.medi_BaaMemFina ] <anchor 300 1640> mark @TashkilAbove;
  pos base [\aHeh.isol \aKaf.init_KafBaaInit \uni0647 \uni06C2 \uni06C0 \uni06C1 \uni06C3 \uni06D5 \uni0629 \uni063C.init_KafBaaInit \uni0764.init_KafBaaInit \uni0643.init_KafBaaInit \uni06B0.init_KafBaaInit \uni06B3.init_KafBaaInit \uni06B2.init_KafBaaInit \uni06AB.init_KafBaaInit \uni06AE.init_KafBaaInit \uni06AF.init_KafBaaInit \uni06A9.init_KafBaaInit ] <anchor 368 1640> mark @TashkilAbove;
  pos base [\aKaf.fina \aWaw.isol \uni063B.fina \uni063C.fina \uni077F.fina \uni0764.fina \uni0643.fina \uni06B0.fina \uni06B3.fina \uni06B2.fina \uni06AB.fina \uni06AC.fina \uni06AE.fina \uni06AF.fina \uni06A9.fina \uni0762.fina \uni06B1.fina ] <anchor 730 1640> mark @TashkilAbove;
  pos base [\aKaf.init.alt \uni06AA.init ] <anchor 564 1640> mark @TashkilAbove;
  pos base [\aKaf.medi \uni063B.medi \uni063C.medi \uni077F.medi \uni0764.medi \uni0643.medi \uni06B0.medi \uni06B3.medi \uni06B2.medi \uni06AB.medi \uni06AC.medi \uni06AD.medi \uni06AE.medi \uni06AF.medi \uni06A9.medi \uni06B4.medi \uni0763.medi \uni0762.medi \uni06B1.medi ] <anchor 559 1640> mark @TashkilAbove;
  pos base [\aLam.init \uni0644.init \uni06B8.init \uni076A.init ] <anchor 80 1640> mark @TashkilAbove;
  pos base [\aLam.medi \aLam.medi_LamWawFina \aMem.medi_BaaBaaMemInit \aLam.init_LamRaaIsol \aLam.medi_KafLam \aLam.medi_KafLamMemFina \uni0644.medi \uni06B8.medi \uni076A.medi \uni0644.medi_LamWawFina \uni06B8.medi_LamWawFina \uni076A.medi_LamWawFina \uni0645.medi_BaaBaaMemInit \uni0644.init_LamRaaIsol \uni06B8.init_LamRaaIsol \uni076A.init_LamRaaIsol \uni0644.medi_KafLam \uni06B8.medi_KafLam \uni076A.medi_KafLam \uni06B5.medi_KafLamMemFina \uni0644.medi_KafLamMemFina \uni06B8.medi_KafLamMemFina \uni076A.medi_KafLamMemFina ] <anchor 130 1640> mark @TashkilAbove;
  pos base [\aMem.fina \uni0765.fina \uni0645.fina \uni0766.fina ] <anchor 474 1640> mark @TashkilAbove;
  pos base [\aNon.fina.alt ] <anchor 1982 1640> mark @TashkilAbove;
  pos base [\aNon.fina \uni0646.fina \uni0767.fina \uni06BA.fina \uni06BC.fina \uni06BB.fina \uni0768.fina \uni06B9.fina \uni0769.fina \uni06BD.fina ] <anchor 680 1640> mark @TashkilAbove;
  pos base [\aNon.isol.alt ] <anchor 2200 1640> mark @TashkilAbove;
  pos base [\aNon.isol \uni0646 \uni0767 \uni06BA \uni06BC \uni06BB \uni06B9 \uni06BD ] <anchor 550 1640> mark @TashkilAbove;
  pos base [\aQaf.isol \uni06A8 \uni06A7 \uni0642 \uni066F ] <anchor 900 1640> mark @TashkilAbove;
  pos base [\aRaa.fina.alt2 ] <anchor 508 1640> mark @TashkilAbove;
  pos base [\aSad.fina \aSad.isol \uni069D.fina \uni06FB.fina \uni0636.fina \uni069E.fina \uni0635.fina \uni069D \uni06FB \uni0636 \uni069E \uni0635 ] <anchor 1680 1640> mark @TashkilAbove;
  pos base [\aSad.init \uni069D.init \uni06FB.init \uni0636.init \uni069E.init \uni0635.init \uni0677 \uni069D.init_SenBaaMemInit \uni06FB.init_SenBaaMemInit \uni0636.init_SenBaaMemInit \uni069E.init_SenBaaMemInit \uni0635.init_SenBaaMemInit ] <anchor 630 1640> mark @TashkilAbove;
  pos base [\aSen.fina \aSen.isol \uni06FA.fina \uni076D.fina \uni0633.fina \uni077E.fina \uni077D.fina \uni0634.fina \uni075C.fina \uni069A.fina \uni069B.fina \uni069C.fina \uni06FA \uni076D \uni0633 \uni077E \uni077D \uni0634 \uni075C \uni069A \uni069B \uni069C ] <anchor 1500 1640> mark @TashkilAbove;
  pos base [\aTaa.fina \uni0638.fina \uni0637.fina \uni069F.fina ] <anchor 1000 1640> mark @TashkilAbove;
  pos base [\aTaa.isol \uni0638 \uni0637 \uni069F ] <anchor 1100 1640> mark @TashkilAbove;
  pos base [\aYaa.isol \uni0777 \uni06D1 \uni0775 \uni063F \uni063D \uni063E \uni06D0 \uni0649 \uni0776 \uni06CD \uni06CC \uni0620 \uni064A \uni06CE ] <anchor 800 1640> mark @TashkilAbove;
  pos base [\uni0621 ] <anchor 300 700> mark @TashkilAbove;
  pos base [\aMem.fina_LamMemFina \aMem.fina_BaaMemFina \aBaa.medi_BaaYaaFina \aHeh.fina_LamHeh \aMem.medi_KafMemMedi \aMem.medi_SenMemInit \uni0691.fina \uni0692.fina \uni0693.fina \uni0694.fina \uni0695.fina \uni0696.fina \uni0697.fina \uni0698.fina \uni0699.fina \uni075B.fina \uni06EF.fina \uni0632.fina \uni0631.fina \uni076B.fina \uni076C.fina \uni0645.fina_LamMemFina \uni0645.fina_BaaMemFina \uni0691.fina_KafRaaFina \uni0692.fina_KafRaaFina \uni0693.fina_KafRaaFina \uni0694.fina_KafRaaFina \uni0695.fina_KafRaaFina \uni0696.fina_KafRaaFina \uni0697.fina_KafRaaFina \uni0698.fina_KafRaaFina \uni0699.fina_KafRaaFina \uni075B.fina_KafRaaFina \uni06EF.fina_KafRaaFina \uni0632.fina_KafRaaFina \uni0771.fina_KafRaaFina \uni0631.fina_KafRaaFina \uni076B.fina_KafRaaFina \uni0691.fina_KafRaaIsol \uni0692.fina_KafRaaIsol \uni0693.fina_KafRaaIsol \uni0694.fina_KafRaaIsol \uni0695.fina_KafRaaIsol \uni0696.fina_KafRaaIsol \uni0697.fina_KafRaaIsol \uni0698.fina_KafRaaIsol \uni0699.fina_KafRaaIsol \uni075B.fina_KafRaaIsol \uni06EF.fina_KafRaaIsol \uni0632.fina_KafRaaIsol \uni0771.fina_KafRaaIsol \uni0631.fina_KafRaaIsol \uni076B.fina_KafRaaIsol \uni0777.medi_BaaYaaFina \uni0680.medi_BaaYaaFina \uni0776.medi_BaaYaaFina \uni06BC.medi_BaaYaaFina \uni0750.medi_BaaYaaFina \uni0756.medi_BaaYaaFina \uni06CE.medi_BaaYaaFina \uni0775.medi_BaaYaaFina \uni06BD.medi_BaaYaaFina \uni066E.medi_BaaYaaFina \uni0620.medi_BaaYaaFina \uni064A.medi_BaaYaaFina \uni06BB.medi_BaaYaaFina \uni067F.medi_BaaYaaFina \uni0755.medi_BaaYaaFina \uni067D.medi_BaaYaaFina \uni067E.medi_BaaYaaFina \uni067B.medi_BaaYaaFina \uni0628.medi_BaaYaaFina \uni067A.medi_BaaYaaFina \uni0751.medi_BaaYaaFina \uni0646.medi_BaaYaaFina \uni0753.medi_BaaYaaFina \uni0752.medi_BaaYaaFina \uni062A.medi_BaaYaaFina \uni063D.medi_BaaYaaFina \uni062B.medi_BaaYaaFina \uni0679.medi_BaaYaaFina \uni06B9.medi_BaaYaaFina \uni0769.medi_BaaYaaFina \uni0649.medi_BaaYaaFina \uni067C.medi_BaaYaaFina \uni0754.medi_BaaYaaFina \uni06D1.medi_BaaYaaFina \uni06D0.medi_BaaYaaFina \uni06BA.medi_BaaYaaFina \uni06CC.medi_BaaYaaFina \uni0767.medi_BaaYaaFina \uni0647.fina_LamHeh \uni06C1.fina_LamHeh \uni06C3.fina_LamHeh \uni06D5.fina_LamHeh \uni0629.fina_LamHeh \uni0645.medi_KafMemMedi \uni0645.medi_SenMemInit \uni0644.medi_LamLamMedi \uni06B8.medi_LamLamMedi \uni076A.medi_LamLamMedi \uni08A0.medi_BaaYaaFina ] <anchor 200 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaRaaIsol \uni0777.init_BaaRaaIsol \uni0680.init_BaaRaaIsol \uni0776.init_BaaRaaIsol \uni06BC.init_BaaRaaIsol \uni0750.init_BaaRaaIsol \uni0756.init_BaaRaaIsol \uni06CE.init_BaaRaaIsol \uni0775.init_BaaRaaIsol \uni06BD.init_BaaRaaIsol \uni066E.init_BaaRaaIsol \uni0620.init_BaaRaaIsol \uni064A.init_BaaRaaIsol \uni06BB.init_BaaRaaIsol \uni067F.init_BaaRaaIsol \uni0755.init_BaaRaaIsol \uni067D.init_BaaRaaIsol \uni067E.init_BaaRaaIsol \uni067B.init_BaaRaaIsol \uni0628.init_BaaRaaIsol \uni067A.init_BaaRaaIsol \uni0751.init_BaaRaaIsol \uni0646.init_BaaRaaIsol \uni0753.init_BaaRaaIsol \uni0752.init_BaaRaaIsol \uni062A.init_BaaRaaIsol \uni063D.init_BaaRaaIsol \uni062B.init_BaaRaaIsol \uni0679.init_BaaRaaIsol \uni06B9.init_BaaRaaIsol \uni0769.init_BaaRaaIsol \uni0649.init_BaaRaaIsol \uni067C.init_BaaRaaIsol \uni0754.init_BaaRaaIsol \uni06D1.init_BaaRaaIsol \uni06D0.init_BaaRaaIsol \uni06BA.init_BaaRaaIsol \uni06CC.init_BaaRaaIsol \uni0767.init_BaaRaaIsol \uni0680.init_BaaRaaIsolLD \uni06BD.init_BaaRaaIsolLD \uni067E.init_BaaRaaIsolLD \uni067B.init_BaaRaaIsolLD \uni0628.init_BaaRaaIsolLD \uni0767.init_BaaRaaIsolLD \uni063D.init_BaaRaaIsolLD \uni0777.init_BaaRaaIsolLD \uni0776.init_BaaRaaIsolLD \uni0775.init_BaaRaaIsolLD \uni06CC.init_BaaRaaIsolLD \uni064A.init_BaaRaaIsolLD \uni06CE.init_BaaRaaIsolLD \uni0751.init_BaaRaaIsolLD \uni0750.init_BaaRaaIsolLD \uni0753.init_BaaRaaIsolLD \uni0752.init_BaaRaaIsolLD \uni0755.init_BaaRaaIsolLD \uni0754.init_BaaRaaIsolLD \uni06B9.init_BaaRaaIsolLD \uni06D1.init_BaaRaaIsolLD \uni06D0.init_BaaRaaIsolLD \uni0620.init_BaaRaaIsolLD \uni08A0.init_BaaRaaIsol \uni08A0.init_BaaRaaIsolLD ] <anchor 244 1640> mark @TashkilAbove;
  pos base [\aRaa.fina_BaaRaaIsol \aKaf.medi_KafMemAlf \uni063B.medi_KafMemAlf \uni063C.medi_KafMemAlf \uni077F.medi_KafMemAlf \uni0764.medi_KafMemAlf \uni0643.medi_KafMemAlf \uni06B0.medi_KafMemAlf \uni06B3.medi_KafMemAlf \uni06B2.medi_KafMemAlf \uni06AB.medi_KafMemAlf \uni06AC.medi_KafMemAlf \uni06AE.medi_KafMemAlf \uni06AF.medi_KafMemAlf \uni06A9.medi_KafMemAlf \uni0762.medi_KafMemAlf \uni06B1.medi_KafMemAlf ] <anchor 640 1640> mark @TashkilAbove;
  pos base [\aWaw.fina_LamWawFina ] <anchor 605 1640> mark @TashkilAbove;
  pos base [\aLam.init_LamHaaInit \aLam.init_LamHaaMemInit \uni0644.init_LamHaaInit \uni06B8.init_LamHaaInit \uni076A.init_LamHaaInit \uni0644.init_LamHaaMemInit \uni06B8.init_LamHaaMemInit \uni076A.init_LamHaaMemInit ] <anchor 510 1640> mark @TashkilAbove;
  pos base [\aFaa.medi_FaaYaaFina \uni066F.medi_FaaYaaFina \uni0761.medi_FaaYaaFina \uni0760.medi_FaaYaaFina \uni0642.medi_FaaYaaFina \uni0641.medi_FaaYaaFina \uni06A8.medi_FaaYaaFina \uni06A1.medi_FaaYaaFina \uni06A2.medi_FaaYaaFina \uni06A3.medi_FaaYaaFina \uni06A4.medi_FaaYaaFina \uni06A5.medi_FaaYaaFina \uni06A6.medi_FaaYaaFina \uni06A7.medi_FaaYaaFina ] <anchor 548 1640> mark @TashkilAbove;
  pos base [\aLam.init_LamLamHaaInit \uni0644.init_LamLamHaaInit \uni06B8.init_LamLamHaaInit \uni076A.init_LamLamHaaInit ] <anchor 219 1640> mark @TashkilAbove;
  pos base [\aLam.medi_LamLamHaaInit \aAyn.init_AynHaaInit \uni0644.medi_LamLamHaaInit \uni06B8.medi_LamLamHaaInit \uni076A.medi_LamLamHaaInit \uni0639.init_AynHaaInit ] <anchor 478 1640> mark @TashkilAbove;
  pos base [\aHaa.medi_LamLamHaaInit \aLam.init_LamMemInit \uni0777.medi \uni0680.medi \uni0776.medi \uni06BC.medi \uni0750.medi \uni0756.medi \uni06CE.medi \uni0775.medi \uni06BD.medi \uni0626.medi \uni066E.medi \uni0620.medi \uni064A.medi \uni06BB.medi \uni067F.medi \uni0755.medi \uni067D.medi \uni067E.medi \uni067B.medi \uni0628.medi \uni067A.medi \uni0751.medi \uni0646.medi \uni0753.medi \uni0752.medi \uni062A.medi \uni0678.medi \uni063D.medi \uni062B.medi \uni0679.medi \uni06B9.medi \uni0769.medi \uni0649.medi \uni067C.medi \uni0754.medi \uni06D1.medi \uni06D0.medi \uni06BA.medi \uni06CC.medi \uni0767.medi \uni062E.medi_LamLamHaaInit \uni062D.medi_LamLamHaaInit \uni0681.medi_LamLamHaaInit \uni0687.medi_LamLamHaaInit \uni0685.medi_LamLamHaaInit \uni062C.medi_LamLamHaaInit \uni0682.medi_LamLamHaaInit \uni0757.medi_LamLamHaaInit \uni0684.medi_LamLamHaaInit \uni076F.medi_LamLamHaaInit \uni076E.medi_LamLamHaaInit \uni0683.medi_LamLamHaaInit \uni06BF.medi_LamLamHaaInit \uni077C.medi_LamLamHaaInit \uni0758.medi_LamLamHaaInit \uni0686.medi_LamLamHaaInit \uni0644.init_LamMemInit \uni06B8.init_LamMemInit \uni076A.init_LamMemInit ] <anchor 170 1640> mark @TashkilAbove;
  pos base [\aSad.init_AboveHaa \uni069D.init_AboveHaa \uni06FB.init_AboveHaa \uni0636.init_AboveHaa \uni0635.init_AboveHaa ] <anchor 928 1640> mark @TashkilAbove;
  pos base [\aLam.init_LamBaaMemInit \aMem.medi_BaaMemInit \aLam.medi_LamLamInit \aLam.medi_LamLamMedi2 \uni0644.init_LamBaaMemInit \uni06B8.init_LamBaaMemInit \uni076A.init_LamBaaMemInit \uni0645.medi_BaaMemInit \uni0644.medi_LamLamInit \uni06B8.medi_LamLamInit \uni076A.medi_LamLamInit \uni0644.medi_LamLamMedi2 \uni06B8.medi_LamLamMedi2 \uni076A.medi_LamLamMedi2 \uni0627.fina_Wide \aBaa.init_YaaBarree \aFaa.init_YaaBarree \uni0777.init_YaaBarree \uni0680.init_YaaBarree \uni0750.init_YaaBarree \uni0756.init_YaaBarree \uni06CE.init_YaaBarree \uni06BD.init_YaaBarree \uni066E.init_YaaBarree \uni0620.init_YaaBarree \uni064A.init_YaaBarree \uni0755.init_YaaBarree \uni067E.init_YaaBarree \uni067B.init_YaaBarree \uni0628.init_YaaBarree \uni0752.init_YaaBarree \uni063D.init_YaaBarree \uni0649.init_YaaBarree \uni06D1.init_YaaBarree \uni06D0.init_YaaBarree \uni06CC.init_YaaBarree \uni066F.init_YaaBarree \uni0761.init_YaaBarree \uni0760.init_YaaBarree \uni0642.init_YaaBarree \uni0641.init_YaaBarree \uni06A8.init_YaaBarree \uni06A1.init_YaaBarree \uni06A2.init_YaaBarree \uni06A3.init_YaaBarree \uni06A4.init_YaaBarree \uni06A5.init_YaaBarree \uni06A6.init_YaaBarree \uni06A7.init_YaaBarree \uni08A0.init_YaaBarree ] <anchor 150 1640> mark @TashkilAbove;
  pos base [\aBaa.medi_LamBaaMemInit \aBaa.medi_KafBaaInit \aBaa.medi_KafBaaMedi \aLam.medi_KafLamYaa \aFaa.init_FaaMemInit \aLam.medi_LamLamYaaIsol \aLam.medi_LamLamYaaFina \uni0777.medi_LamBaaMemInit \uni0680.medi_LamBaaMemInit \uni06BC.medi_LamBaaMemInit \uni0750.medi_LamBaaMemInit \uni0756.medi_LamBaaMemInit \uni06CE.medi_LamBaaMemInit \uni06BD.medi_LamBaaMemInit \uni066E.medi_LamBaaMemInit \uni0620.medi_LamBaaMemInit \uni064A.medi_LamBaaMemInit \uni0755.medi_LamBaaMemInit \uni067D.medi_LamBaaMemInit \uni067E.medi_LamBaaMemInit \uni067B.medi_LamBaaMemInit \uni0628.medi_LamBaaMemInit \uni067A.medi_LamBaaMemInit \uni0751.medi_LamBaaMemInit \uni0646.medi_LamBaaMemInit \uni0753.medi_LamBaaMemInit \uni0752.medi_LamBaaMemInit \uni062A.medi_LamBaaMemInit \uni063D.medi_LamBaaMemInit \uni062B.medi_LamBaaMemInit \uni06B9.medi_LamBaaMemInit \uni0649.medi_LamBaaMemInit \uni067C.medi_LamBaaMemInit \uni0754.medi_LamBaaMemInit \uni06D1.medi_LamBaaMemInit \uni06D0.medi_LamBaaMemInit \uni06BA.medi_LamBaaMemInit \uni06CC.medi_LamBaaMemInit \uni0767.medi_LamBaaMemInit \uni0777.medi_KafBaaInit \uni0680.medi_KafBaaInit \uni06BC.medi_KafBaaInit \uni0750.medi_KafBaaInit \uni0756.medi_KafBaaInit \uni06CE.medi_KafBaaInit \uni06BD.medi_KafBaaInit \uni066E.medi_KafBaaInit \uni0620.medi_KafBaaInit \uni064A.medi_KafBaaInit \uni0755.medi_KafBaaInit \uni067E.medi_KafBaaInit \uni067B.medi_KafBaaInit \uni0628.medi_KafBaaInit \uni0646.medi_KafBaaInit \uni0753.medi_KafBaaInit \uni0752.medi_KafBaaInit \uni062A.medi_KafBaaInit \uni063D.medi_KafBaaInit \uni06B9.medi_KafBaaInit \uni0769.medi_KafBaaInit \uni0649.medi_KafBaaInit \uni067C.medi_KafBaaInit \uni0754.medi_KafBaaInit \uni06D1.medi_KafBaaInit \uni06D0.medi_KafBaaInit \uni06BA.medi_KafBaaInit \uni06CC.medi_KafBaaInit \uni0767.medi_KafBaaInit \uni0777.medi_KafBaaMedi \uni0680.medi_KafBaaMedi \uni06BC.medi_KafBaaMedi \uni0750.medi_KafBaaMedi \uni0756.medi_KafBaaMedi \uni06CE.medi_KafBaaMedi \uni06BD.medi_KafBaaMedi \uni066E.medi_KafBaaMedi \uni0620.medi_KafBaaMedi \uni064A.medi_KafBaaMedi \uni0755.medi_KafBaaMedi \uni067E.medi_KafBaaMedi \uni067B.medi_KafBaaMedi \uni0628.medi_KafBaaMedi \uni0646.medi_KafBaaMedi \uni0753.medi_KafBaaMedi \uni0752.medi_KafBaaMedi \uni062A.medi_KafBaaMedi \uni063D.medi_KafBaaMedi \uni06B9.medi_KafBaaMedi \uni0769.medi_KafBaaMedi \uni0649.medi_KafBaaMedi \uni067C.medi_KafBaaMedi \uni0754.medi_KafBaaMedi \uni06D1.medi_KafBaaMedi \uni06D0.medi_KafBaaMedi \uni06BA.medi_KafBaaMedi \uni06CC.medi_KafBaaMedi \uni0767.medi_KafBaaMedi \uni0644.medi_KafLamYaa \uni06B8.medi_KafLamYaa \uni076A.medi_KafLamYaa \uni066F.init_FaaMemInit \uni0761.init_FaaMemInit \uni0760.init_FaaMemInit \uni0642.init_FaaMemInit \uni0641.init_FaaMemInit \uni06A1.init_FaaMemInit \uni06A2.init_FaaMemInit \uni06A3.init_FaaMemInit \uni06A5.init_FaaMemInit \uni06A7.init_FaaMemInit \uni0644.medi_LamLamYaaIsol \uni06B8.medi_LamLamYaaIsol \uni076A.medi_LamLamYaaIsol \uni0644.medi_LamLamYaaFina \uni06B8.medi_LamLamYaaFina \uni076A.medi_LamLamYaaFina \uni08A0.medi_LamBaaMemInit \uni08A0.medi_KafBaaInit \uni08A0.medi_KafBaaMedi ] <anchor 50 1640> mark @TashkilAbove;
  pos base [\aMem.medi_LamBaaMemInit \aLam.medi_LamYaaFina \aAlf.fina_LamAlfIsol \aHaa.medi_FaaHaaInit \aAlf.fina_LamAlfFina \aHeh.init_AboveHaa \aLam.medi_KafLamAlf \aAlf.fina_KafAlf \aLam.medi_KafMemLam \aAlf.fina_KafMemAlf \aLam.init_LamYaaIsol \aLam.medi_LamLamMedi \uni0645.medi_LamBaaMemInit \uni0644.medi_LamYaaFina \uni06B8.medi_LamYaaFina \uni076A.medi_LamYaaFina \uni0625.fina_LamAlfIsol \uni0673.fina_LamAlfIsol \uni062E.medi_FaaHaaInit \uni062D.medi_FaaHaaInit \uni0681.medi_FaaHaaInit \uni0687.medi_FaaHaaInit \uni0685.medi_FaaHaaInit \uni062C.medi_FaaHaaInit \uni0682.medi_FaaHaaInit \uni0757.medi_FaaHaaInit \uni0684.medi_FaaHaaInit \uni076F.medi_FaaHaaInit \uni076E.medi_FaaHaaInit \uni0683.medi_FaaHaaInit \uni06BF.medi_FaaHaaInit \uni077C.medi_FaaHaaInit \uni0758.medi_FaaHaaInit \uni0772.medi_FaaHaaInit \uni0686.medi_FaaHaaInit \uni0625.fina_LamAlfFina \uni0673.fina_LamAlfFina \uni0647.init_AboveHaa \uni06C1.init_AboveHaa \uni0644.medi_KafLamAlf \uni06B8.medi_KafLamAlf \uni076A.medi_KafLamAlf \uni0625.fina_KafAlf \uni0627.fina_KafAlf \uni0673.fina_KafAlf \uni0644.medi_KafMemLam \uni06B8.medi_KafMemLam \uni076A.medi_KafMemLam \uni0625.fina_KafMemAlf \uni0673.fina_KafMemAlf \uni0644.init_LamYaaIsol \uni06B8.init_LamYaaIsol \uni076A.init_LamYaaIsol ] <anchor 0 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaDal \uni0777.init_BaaDal \uni0680.init_BaaDal \uni0776.init_BaaDal \uni06BC.init_BaaDal \uni0750.init_BaaDal \uni0756.init_BaaDal \uni06CE.init_BaaDal \uni0775.init_BaaDal \uni06BD.init_BaaDal \uni066E.init_BaaDal \uni0620.init_BaaDal \uni064A.init_BaaDal \uni06BB.init_BaaDal \uni067F.init_BaaDal \uni0755.init_BaaDal \uni067D.init_BaaDal \uni067E.init_BaaDal \uni067B.init_BaaDal \uni0628.init_BaaDal \uni067A.init_BaaDal \uni0751.init_BaaDal \uni0646.init_BaaDal \uni0753.init_BaaDal \uni0752.init_BaaDal \uni062A.init_BaaDal \uni063D.init_BaaDal \uni062B.init_BaaDal \uni0679.init_BaaDal \uni06B9.init_BaaDal \uni0649.init_BaaDal \uni067C.init_BaaDal \uni0754.init_BaaDal \uni06D1.init_BaaDal \uni06D0.init_BaaDal \uni06BA.init_BaaDal \uni06CC.init_BaaDal \uni0767.init_BaaDal \uni0680.init_BaaDalLD \uni06BD.init_BaaDalLD \uni067E.init_BaaDalLD \uni067B.init_BaaDalLD \uni0628.init_BaaDalLD \uni0767.init_BaaDalLD \uni063D.init_BaaDalLD \uni0777.init_BaaDalLD \uni0776.init_BaaDalLD \uni0775.init_BaaDalLD \uni06CC.init_BaaDalLD \uni064A.init_BaaDalLD \uni06CE.init_BaaDalLD \uni0751.init_BaaDalLD \uni0750.init_BaaDalLD \uni0753.init_BaaDalLD \uni0752.init_BaaDalLD \uni0755.init_BaaDalLD \uni0754.init_BaaDalLD \uni06B9.init_BaaDalLD \uni06D1.init_BaaDalLD \uni06D0.init_BaaDalLD \uni0620.init_BaaDalLD \uni08A0.init_BaaDal \uni08A0.init_BaaDalLD ] <anchor 155 1640> mark @TashkilAbove;
  pos base [\aDal.fina_BaaDal \uni06EE.fina_BaaDal \uni0689.fina_BaaDal \uni075A.fina_BaaDal \uni0630.fina_BaaDal \uni062F.fina_BaaDal \uni068C.fina_BaaDal \uni068A.fina_BaaDal \uni068D.fina_BaaDal ] <anchor 432 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaMemHaaInit \uni0777.init_BaaMemHaaInit \uni0680.init_BaaMemHaaInit \uni0750.init_BaaMemHaaInit \uni06BD.init_BaaMemHaaInit \uni066E.init_BaaMemHaaInit \uni0620.init_BaaMemHaaInit \uni064A.init_BaaMemHaaInit \uni0755.init_BaaMemHaaInit \uni067E.init_BaaMemHaaInit \uni067B.init_BaaMemHaaInit \uni0628.init_BaaMemHaaInit \uni0752.init_BaaMemHaaInit \uni0649.init_BaaMemHaaInit \uni06D1.init_BaaMemHaaInit \uni06D0.init_BaaMemHaaInit \uni06CC.init_BaaMemHaaInit \uni0680.init_BaaMemHaaInitLD \uni06BD.init_BaaMemHaaInitLD \uni067E.init_BaaMemHaaInitLD \uni067B.init_BaaMemHaaInitLD \uni0628.init_BaaMemHaaInitLD \uni0777.init_BaaMemHaaInitLD \uni06CC.init_BaaMemHaaInitLD \uni064A.init_BaaMemHaaInitLD \uni0750.init_BaaMemHaaInitLD \uni0752.init_BaaMemHaaInitLD \uni0755.init_BaaMemHaaInitLD \uni06D1.init_BaaMemHaaInitLD \uni06D0.init_BaaMemHaaInitLD \uni0620.init_BaaMemHaaInitLD \uni08A0.init_BaaMemHaaInit \uni08A0.init_BaaMemHaaInitLD ] <anchor 251 1640> mark @TashkilAbove;
  pos base [\aMem.medi_BaaMemHaaInit \aHeh.fina_KafHeh \uni0645.medi_BaaMemHaaInit \uni0647.fina_KafHeh \uni06C1.fina_KafHeh \uni06C3.fina_KafHeh \uni06D5.fina_KafHeh \uni0629.fina_KafHeh ] <anchor 330 1640> mark @TashkilAbove;
  pos base [\aHaa.medi_BaaMemHaaInit \uni062E.medi_BaaMemHaaInit \uni062D.medi_BaaMemHaaInit \uni0681.medi_BaaMemHaaInit \uni0687.medi_BaaMemHaaInit \uni0685.medi_BaaMemHaaInit \uni062C.medi_BaaMemHaaInit \uni0682.medi_BaaMemHaaInit \uni0757.medi_BaaMemHaaInit \uni0684.medi_BaaMemHaaInit \uni076F.medi_BaaMemHaaInit \uni076E.medi_BaaMemHaaInit \uni0683.medi_BaaMemHaaInit \uni06BF.medi_BaaMemHaaInit \uni077C.medi_BaaMemHaaInit \uni0758.medi_BaaMemHaaInit \uni0686.medi_BaaMemHaaInit ] <anchor 182 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaBaaYaa \uni0777.init_BaaBaaYaa \uni0680.init_BaaBaaYaa \uni06BC.init_BaaBaaYaa \uni0750.init_BaaBaaYaa \uni0756.init_BaaBaaYaa \uni06CE.init_BaaBaaYaa \uni06BD.init_BaaBaaYaa \uni066E.init_BaaBaaYaa \uni0620.init_BaaBaaYaa \uni064A.init_BaaBaaYaa \uni06BB.init_BaaBaaYaa \uni0755.init_BaaBaaYaa \uni067E.init_BaaBaaYaa \uni067B.init_BaaBaaYaa \uni0628.init_BaaBaaYaa \uni067A.init_BaaBaaYaa \uni0646.init_BaaBaaYaa \uni0753.init_BaaBaaYaa \uni0752.init_BaaBaaYaa \uni062A.init_BaaBaaYaa \uni063D.init_BaaBaaYaa \uni0679.init_BaaBaaYaa \uni06B9.init_BaaBaaYaa \uni0649.init_BaaBaaYaa \uni067C.init_BaaBaaYaa \uni0754.init_BaaBaaYaa \uni06D1.init_BaaBaaYaa \uni06D0.init_BaaBaaYaa \uni06BA.init_BaaBaaYaa \uni06CC.init_BaaBaaYaa \uni0767.init_BaaBaaYaa \uni0680.init_BaaBaaYaaLD \uni06BD.init_BaaBaaYaaLD \uni067E.init_BaaBaaYaaLD \uni067B.init_BaaBaaYaaLD \uni0628.init_BaaBaaYaaLD \uni0767.init_BaaBaaYaaLD \uni063D.init_BaaBaaYaaLD \uni0777.init_BaaBaaYaaLD \uni06CC.init_BaaBaaYaaLD \uni064A.init_BaaBaaYaaLD \uni06CE.init_BaaBaaYaaLD \uni0750.init_BaaBaaYaaLD \uni0753.init_BaaBaaYaaLD \uni0752.init_BaaBaaYaaLD \uni0755.init_BaaBaaYaaLD \uni0754.init_BaaBaaYaaLD \uni06B9.init_BaaBaaYaaLD \uni06D1.init_BaaBaaYaaLD \uni06D0.init_BaaBaaYaaLD \uni0620.init_BaaBaaYaaLD \uni08A0.init_BaaBaaYaa \uni08A0.init_BaaBaaYaaLD \uni0620.init_BaaBaaHeh ] <anchor 129 1640> mark @TashkilAbove;
  pos base [\aBaa.medi_BaaBaaYaa \uni0777.medi_BaaBaaYaa \uni0680.medi_BaaBaaYaa \uni0776.medi_BaaBaaYaa \uni06BC.medi_BaaBaaYaa \uni0750.medi_BaaBaaYaa \uni0756.medi_BaaBaaYaa \uni06CE.medi_BaaBaaYaa \uni0775.medi_BaaBaaYaa \uni06BD.medi_BaaBaaYaa \uni066E.medi_BaaBaaYaa \uni0620.medi_BaaBaaYaa \uni064A.medi_BaaBaaYaa \uni06BB.medi_BaaBaaYaa \uni067F.medi_BaaBaaYaa \uni0755.medi_BaaBaaYaa \uni067D.medi_BaaBaaYaa \uni067E.medi_BaaBaaYaa \uni067B.medi_BaaBaaYaa \uni0628.medi_BaaBaaYaa \uni067A.medi_BaaBaaYaa \uni0751.medi_BaaBaaYaa \uni0646.medi_BaaBaaYaa \uni0753.medi_BaaBaaYaa \uni0752.medi_BaaBaaYaa \uni062A.medi_BaaBaaYaa \uni063D.medi_BaaBaaYaa \uni062B.medi_BaaBaaYaa \uni0679.medi_BaaBaaYaa \uni06B9.medi_BaaBaaYaa \uni0649.medi_BaaBaaYaa \uni067C.medi_BaaBaaYaa \uni0754.medi_BaaBaaYaa \uni06D1.medi_BaaBaaYaa \uni06D0.medi_BaaBaaYaa \uni06BA.medi_BaaBaaYaa \uni06CC.medi_BaaBaaYaa \uni0767.medi_BaaBaaYaa \uni08A0.medi_BaaBaaYaa ] <anchor 196 1640> mark @TashkilAbove;
  pos base [\aYaa.fina_BaaBaaYaa \uni0777.fina_BaaBaaYaa \uni06D1.fina_BaaBaaYaa \uni0775.fina_BaaBaaYaa \uni063F.fina_BaaBaaYaa \uni063D.fina_BaaBaaYaa \uni063E.fina_BaaBaaYaa \uni06D0.fina_BaaBaaYaa \uni0649.fina_BaaBaaYaa \uni0776.fina_BaaBaaYaa \uni06CD.fina_BaaBaaYaa \uni06CC.fina_BaaBaaYaa \uni0620.fina_BaaBaaYaa \uni064A.fina_BaaBaaYaa \uni06CE.fina_BaaBaaYaa ] <anchor 461 1640> mark @TashkilAbove;
  pos base [\aYaa.fina_LamYaaFina \aBaa.medi_BaaHehMedi \uni0777.fina_LamYaaFina \uni06D1.fina_LamYaaFina \uni0775.fina_LamYaaFina \uni063F.fina_LamYaaFina \uni063D.fina_LamYaaFina \uni063E.fina_LamYaaFina \uni06D0.fina_LamYaaFina \uni0649.fina_LamYaaFina \uni0776.fina_LamYaaFina \uni06CD.fina_LamYaaFina \uni06CC.fina_LamYaaFina \uni0620.fina_LamYaaFina \uni064A.fina_LamYaaFina \uni06CE.fina_LamYaaFina \uni0777.medi_BaaHehMedi \uni0680.medi_BaaHehMedi \uni0776.medi_BaaHehMedi \uni06BC.medi_BaaHehMedi \uni0750.medi_BaaHehMedi \uni0756.medi_BaaHehMedi \uni06CE.medi_BaaHehMedi \uni0775.medi_BaaHehMedi \uni06BD.medi_BaaHehMedi \uni066E.medi_BaaHehMedi \uni0620.medi_BaaHehMedi \uni064A.medi_BaaHehMedi \uni06BB.medi_BaaHehMedi \uni067F.medi_BaaHehMedi \uni0755.medi_BaaHehMedi \uni067D.medi_BaaHehMedi \uni067E.medi_BaaHehMedi \uni067B.medi_BaaHehMedi \uni0628.medi_BaaHehMedi \uni067A.medi_BaaHehMedi \uni0751.medi_BaaHehMedi \uni0646.medi_BaaHehMedi \uni0753.medi_BaaHehMedi \uni0752.medi_BaaHehMedi \uni062A.medi_BaaHehMedi \uni063D.medi_BaaHehMedi \uni062B.medi_BaaHehMedi \uni0679.medi_BaaHehMedi \uni06B9.medi_BaaHehMedi \uni0769.medi_BaaHehMedi \uni0649.medi_BaaHehMedi \uni067C.medi_BaaHehMedi \uni0754.medi_BaaHehMedi \uni06D1.medi_BaaHehMedi \uni06D0.medi_BaaHehMedi \uni06BA.medi_BaaHehMedi \uni06CC.medi_BaaHehMedi \uni0767.medi_BaaHehMedi \uni08A0.medi_BaaHehMedi ] <anchor 390 1640> mark @TashkilAbove;
  pos base [\aMem.medi_LamMemInit \uni0645.medi_LamMemInit ] <anchor 101 1640> mark @TashkilAbove;
  pos base [\aHaa.medi_LamHaaMemInit \uni062E.medi_LamHaaMemInit \uni062D.medi_LamHaaMemInit \uni0681.medi_LamHaaMemInit \uni0687.medi_LamHaaMemInit \uni0685.medi_LamHaaMemInit \uni062C.medi_LamHaaMemInit \uni0682.medi_LamHaaMemInit \uni0757.medi_LamHaaMemInit \uni0684.medi_LamHaaMemInit \uni076F.medi_LamHaaMemInit \uni076E.medi_LamHaaMemInit \uni0683.medi_LamHaaMemInit \uni06BF.medi_LamHaaMemInit \uni077C.medi_LamHaaMemInit \uni0758.medi_LamHaaMemInit \uni0686.medi_LamHaaMemInit \uni0691.fina_LamRaaIsol \uni0692.fina_LamRaaIsol \uni0693.fina_LamRaaIsol \uni0694.fina_LamRaaIsol \uni0695.fina_LamRaaIsol \uni0696.fina_LamRaaIsol \uni0697.fina_LamRaaIsol \uni0698.fina_LamRaaIsol \uni0699.fina_LamRaaIsol \uni075B.fina_LamRaaIsol \uni06EF.fina_LamRaaIsol \uni0632.fina_LamRaaIsol \uni0631.fina_LamRaaIsol \uni076B.fina_LamRaaIsol ] <anchor 151 1640> mark @TashkilAbove;
  pos base [\aMem.medi_LamHaaMemInit \aLam.medi_LamMemMedi \aAlf.fina_MemAlfFina \uni0627.fina \uni0627.fina_LamAlfIsol \uni0645.medi_LamHaaMemInit \uni0627.fina_LamAlfFina \uni0644.medi_LamMemMedi \uni06B8.medi_LamMemMedi \uni06B6.medi_LamMemMedi \uni076A.medi_LamMemMedi \uni0627.fina_KafMemAlf \uni0625.fina_MemAlfFina \uni0627.fina_MemAlfFina \uni0673.fina_MemAlfFina \uni0627.fina_Narrow ] <anchor 100 1640> mark @TashkilAbove;
  pos base [\aAyn.medi_AynYaaFina \uni06FC.medi_AynYaaFina \uni063A.medi_AynYaaFina \uni075E.medi_AynYaaFina \uni075D.medi_AynYaaFina \uni075F.medi_AynYaaFina \uni06A0.medi_AynYaaFina \uni0639.medi_AynYaaFina ] <anchor 421 1640> mark @TashkilAbove;
  pos base [\aMem.init_MemRaaIsol \aYaa.fina_KafYaaFina \uni0624 \uni0765.init_MemRaaIsol \uni0645.init_MemRaaIsol \uni0766.init_MemRaaIsol \uni0777.fina_KafYaaFina \uni06D1.fina_KafYaaFina \uni0775.fina_KafYaaFina \uni063F.fina_KafYaaFina \uni063D.fina_KafYaaFina \uni063E.fina_KafYaaFina \uni06D0.fina_KafYaaFina \uni0649.fina_KafYaaFina \uni0776.fina_KafYaaFina \uni06CD.fina_KafYaaFina \uni06CC.fina_KafYaaFina \uni0620.fina_KafYaaFina \uni064A.fina_KafYaaFina \uni06CE.fina_KafYaaFina \uni0765.init_MemHehInit \aMem.init_MemHehInit \uni0645.init_MemHehInit \uni0766.init_MemHehInit ] <anchor 380 1640> mark @TashkilAbove;
  pos base [\aRaa.fina_MemRaaIsol ] <anchor 508 840> mark @TashkilAbove;
  pos base [\aFaa.init_FaaHaaInit \uni066F.init_FaaHaaInit \uni0761.init_FaaHaaInit \uni0760.init_FaaHaaInit \uni0641.init_FaaHaaInit \uni06A1.init_FaaHaaInit \uni06A2.init_FaaHaaInit \uni06A3.init_FaaHaaInit \uni06A5.init_FaaHaaInit \uni06A7.init_FaaHaaInit ] <anchor 535 1640> mark @TashkilAbove;
  pos base [\aHaa.init_HaaHaaInit \uni062E.init_HaaHaaInit \uni062D.init_HaaHaaInit \uni0681.init_HaaHaaInit \uni0687.init_HaaHaaInit \uni0685.init_HaaHaaInit \uni062C.init_HaaHaaInit \uni0682.init_HaaHaaInit \uni0757.init_HaaHaaInit \uni0684.init_HaaHaaInit \uni076F.init_HaaHaaInit \uni076E.init_HaaHaaInit \uni0683.init_HaaHaaInit \uni06BF.init_HaaHaaInit \uni077C.init_HaaHaaInit \uni0758.init_HaaHaaInit \uni0686.init_HaaHaaInit ] <anchor 817 1640> mark @TashkilAbove;
  pos base [\aLam.medi_LamQafFina \aMem.medi_LamMemMedi \aHaa.medi_AynHaaInit \uni0644.medi_LamQafFina \uni06B8.medi_LamQafFina \uni076A.medi_LamQafFina \uni062E.medi_AynHaaInit \uni062D.medi_AynHaaInit \uni0681.medi_AynHaaInit \uni0687.medi_AynHaaInit \uni0685.medi_AynHaaInit \uni062C.medi_AynHaaInit \uni0682.medi_AynHaaInit \uni0757.medi_AynHaaInit \uni0684.medi_AynHaaInit \uni076F.medi_AynHaaInit \uni076E.medi_AynHaaInit \uni0683.medi_AynHaaInit \uni06BF.medi_AynHaaInit \uni077C.medi_AynHaaInit \uni0758.medi_AynHaaInit \uni0772.medi_AynHaaInit \uni0686.medi_AynHaaInit ] <anchor 140 1640> mark @TashkilAbove;
  pos base [\aQaf.fina_LamQafFina \uni06A8.fina_LamQafFina \uni06A7.fina_LamQafFina \uni0642.fina_LamQafFina \uni066F.fina_LamQafFina ] <anchor 704 1640> mark @TashkilAbove;
  pos base [\aSen.init_AboveHaa \uni076D.init_AboveHaa \uni0633.init_AboveHaa \uni077E.init_AboveHaa \uni069A.init_AboveHaa \uni069B.init_AboveHaa ] <anchor 768 1640> mark @TashkilAbove;
  pos base [\aMem.init_MemHaaInit \uni0645.init_MemHaaInit ] <anchor 580 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaNonIsol \aBaa.init_BaaBaaIsol \uni0777.init_BaaNonIsol \uni0680.init_BaaNonIsol \uni0776.init_BaaNonIsol \uni06BC.init_BaaNonIsol \uni0750.init_BaaNonIsol \uni0756.init_BaaNonIsol \uni06CE.init_BaaNonIsol \uni0775.init_BaaNonIsol \uni06BD.init_BaaNonIsol \uni066E.init_BaaNonIsol \uni0620.init_BaaNonIsol \uni064A.init_BaaNonIsol \uni06BB.init_BaaNonIsol \uni067F.init_BaaNonIsol \uni0755.init_BaaNonIsol \uni067D.init_BaaNonIsol \uni067E.init_BaaNonIsol \uni067B.init_BaaNonIsol \uni0628.init_BaaNonIsol \uni067A.init_BaaNonIsol \uni0751.init_BaaNonIsol \uni0646.init_BaaNonIsol \uni0753.init_BaaNonIsol \uni0752.init_BaaNonIsol \uni062A.init_BaaNonIsol \uni063D.init_BaaNonIsol \uni062B.init_BaaNonIsol \uni0679.init_BaaNonIsol \uni06B9.init_BaaNonIsol \uni0649.init_BaaNonIsol \uni067C.init_BaaNonIsol \uni0754.init_BaaNonIsol \uni06D1.init_BaaNonIsol \uni06D0.init_BaaNonIsol \uni06BA.init_BaaNonIsol \uni06CC.init_BaaNonIsol \uni0767.init_BaaNonIsol \uni0680.init_BaaNonIsolLD \uni06BD.init_BaaNonIsolLD \uni067E.init_BaaNonIsolLD \uni067B.init_BaaNonIsolLD \uni0628.init_BaaNonIsolLD \uni0767.init_BaaNonIsolLD \uni063D.init_BaaNonIsolLD \uni0777.init_BaaNonIsolLD \uni0776.init_BaaNonIsolLD \uni0775.init_BaaNonIsolLD \uni06CC.init_BaaNonIsolLD \uni064A.init_BaaNonIsolLD \uni06CE.init_BaaNonIsolLD \uni0751.init_BaaNonIsolLD \uni0750.init_BaaNonIsolLD \uni0753.init_BaaNonIsolLD \uni0752.init_BaaNonIsolLD \uni0755.init_BaaNonIsolLD \uni0754.init_BaaNonIsolLD \uni06B9.init_BaaNonIsolLD \uni06D1.init_BaaNonIsolLD \uni06D0.init_BaaNonIsolLD \uni0620.init_BaaNonIsolLD \uni0620.init_BaaBaaIsolLD \uni08A0.init_BaaNonIsol \uni08A0.init_BaaBaaIsol \uni08A0.init_BaaNonIsolLD \uni08A0.init_BaaBaaIsolLD ] <anchor 1 1640> mark @TashkilAbove;
  pos base [\aNon.fina_BaaNonIsol \uni0646.fina_BaaNonIsol \uni0767.fina_BaaNonIsol \uni06BA.fina_BaaNonIsol \uni06BC.fina_BaaNonIsol \uni06BB.fina_BaaNonIsol \uni06B9.fina_BaaNonIsol \uni0769.fina_BaaNonIsol \uni06BD.fina_BaaNonIsol ] <anchor 660 1640> mark @TashkilAbove;
  pos base [\aMem.fina_KafMemFina \aMem.medi_KafMemAlf \uni0645.fina_KafMemFina \uni0645.medi_KafMemAlf ] <anchor 220 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaSenInit \uni0777.init_BaaSenInit \uni0680.init_BaaSenInit \uni06BC.init_BaaSenInit \uni0750.init_BaaSenInit \uni06BD.init_BaaSenInit \uni066E.init_BaaSenInit \uni0620.init_BaaSenInit \uni064A.init_BaaSenInit \uni0755.init_BaaSenInit \uni067E.init_BaaSenInit \uni067B.init_BaaSenInit \uni0628.init_BaaSenInit \uni0646.init_BaaSenInit \uni0753.init_BaaSenInit \uni0752.init_BaaSenInit \uni062A.init_BaaSenInit \uni06B9.init_BaaSenInit \uni0649.init_BaaSenInit \uni067C.init_BaaSenInit \uni0754.init_BaaSenInit \uni06D1.init_BaaSenInit \uni06D0.init_BaaSenInit \uni06BA.init_BaaSenInit \uni06CC.init_BaaSenInit \uni0767.init_BaaSenInit \uni0680.init_BaaSenInitLD \uni06BD.init_BaaSenInitLD \uni067E.init_BaaSenInitLD \uni067B.init_BaaSenInitLD \uni0628.init_BaaSenInitLD \uni0767.init_BaaSenInitLD \uni0777.init_BaaSenInitLD \uni06CC.init_BaaSenInitLD \uni064A.init_BaaSenInitLD \uni0750.init_BaaSenInitLD \uni0753.init_BaaSenInitLD \uni0752.init_BaaSenInitLD \uni0755.init_BaaSenInitLD \uni0754.init_BaaSenInitLD \uni06B9.init_BaaSenInitLD \uni06D1.init_BaaSenInitLD \uni06D0.init_BaaSenInitLD \uni0620.init_BaaSenInitLD \uni08A0.init_BaaSenInit \uni08A0.init_BaaSenInitLD ] <anchor 10 1640> mark @TashkilAbove;
  pos base [\aSen.medi_BaaSenInit \aMem.fina_PostTooth \uni06FA.medi_BaaSenInit \uni076D.medi_BaaSenInit \uni0633.medi_BaaSenInit \uni077E.medi_BaaSenInit \uni077D.medi_BaaSenInit \uni0634.medi_BaaSenInit \uni075C.medi_BaaSenInit \uni069A.medi_BaaSenInit \uni069B.medi_BaaSenInit \uni069C.medi_BaaSenInit \uni0645.fina_PostTooth ] <anchor 460 1640> mark @TashkilAbove;
  pos base [\aBaa.medi_BaaRaaFina \aLam.init_LamHehInit \aLam.init_LamMemHaaInit \uni0777.medi_BaaRaaFina \uni0680.medi_BaaRaaFina \uni0776.medi_BaaRaaFina \uni06BC.medi_BaaRaaFina \uni0750.medi_BaaRaaFina \uni0756.medi_BaaRaaFina \uni06CE.medi_BaaRaaFina \uni0775.medi_BaaRaaFina \uni06BD.medi_BaaRaaFina \uni066E.medi_BaaRaaFina \uni0620.medi_BaaRaaFina \uni064A.medi_BaaRaaFina \uni06BB.medi_BaaRaaFina \uni067F.medi_BaaRaaFina \uni0755.medi_BaaRaaFina \uni067D.medi_BaaRaaFina \uni067E.medi_BaaRaaFina \uni067B.medi_BaaRaaFina \uni0628.medi_BaaRaaFina \uni067A.medi_BaaRaaFina \uni0751.medi_BaaRaaFina \uni0646.medi_BaaRaaFina \uni0753.medi_BaaRaaFina \uni0752.medi_BaaRaaFina \uni062A.medi_BaaRaaFina \uni063D.medi_BaaRaaFina \uni062B.medi_BaaRaaFina \uni0679.medi_BaaRaaFina \uni06B9.medi_BaaRaaFina \uni0769.medi_BaaRaaFina \uni0649.medi_BaaRaaFina \uni067C.medi_BaaRaaFina \uni0754.medi_BaaRaaFina \uni06D1.medi_BaaRaaFina \uni06D0.medi_BaaRaaFina \uni06BA.medi_BaaRaaFina \uni06CC.medi_BaaRaaFina \uni0767.medi_BaaRaaFina \uni0644.init_LamHehInit \uni06B8.init_LamHehInit \uni076A.init_LamHehInit \uni0644.init_LamMemHaaInit \uni06B8.init_LamMemHaaInit \uni076A.init_LamMemHaaInit \uni08A0.medi_BaaRaaFina ] <anchor 230 1640> mark @TashkilAbove;
  pos base [\aRaa.fina_BaaRaaFina ] <anchor 654 1640> mark @TashkilAbove;
  pos base [\aHeh.medi_LamHehInit \uni0647.medi_LamHehInit \uni06C1.medi_LamHehInit ] <anchor 240 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaMemInit \aHeh.medi_BaaHehMedi \uni0777.init_BaaMemInit \uni0680.init_BaaMemInit \uni06BC.init_BaaMemInit \uni0750.init_BaaMemInit \uni0756.init_BaaMemInit \uni06CE.init_BaaMemInit \uni0775.init_BaaMemInit \uni06BD.init_BaaMemInit \uni066E.init_BaaMemInit \uni0620.init_BaaMemInit \uni064A.init_BaaMemInit \uni067F.init_BaaMemInit \uni0755.init_BaaMemInit \uni067D.init_BaaMemInit \uni067E.init_BaaMemInit \uni067B.init_BaaMemInit \uni0628.init_BaaMemInit \uni067A.init_BaaMemInit \uni0751.init_BaaMemInit \uni0646.init_BaaMemInit \uni0753.init_BaaMemInit \uni0752.init_BaaMemInit \uni062A.init_BaaMemInit \uni063D.init_BaaMemInit \uni062B.init_BaaMemInit \uni06B9.init_BaaMemInit \uni0649.init_BaaMemInit \uni067C.init_BaaMemInit \uni0754.init_BaaMemInit \uni06D1.init_BaaMemInit \uni06D0.init_BaaMemInit \uni06BA.init_BaaMemInit \uni06CC.init_BaaMemInit \uni0767.init_BaaMemInit \uni0647.medi_BaaHehMedi \uni06C1.medi_BaaHehMedi \uni0680.init_BaaMemInitLD \uni06BD.init_BaaMemInitLD \uni067E.init_BaaMemInitLD \uni067B.init_BaaMemInitLD \uni0628.init_BaaMemInitLD \uni0767.init_BaaMemInitLD \uni063D.init_BaaMemInitLD \uni0777.init_BaaMemInitLD \uni0775.init_BaaMemInitLD \uni06CC.init_BaaMemInitLD \uni064A.init_BaaMemInitLD \uni06CE.init_BaaMemInitLD \uni0751.init_BaaMemInitLD \uni0750.init_BaaMemInitLD \uni0753.init_BaaMemInitLD \uni0752.init_BaaMemInitLD \uni0755.init_BaaMemInitLD \uni0754.init_BaaMemInitLD \uni06B9.init_BaaMemInitLD \uni06D1.init_BaaMemInitLD \uni06D0.init_BaaMemInitLD \uni0620.init_BaaMemInitLD \uni08A0.init_BaaMemInit \uni08A0.init_BaaMemInitLD ] <anchor 212 1640> mark @TashkilAbove;
  pos base [\aSen.init_SenHaaInit \uni06FA.init_SenHaaInit \uni076D.init_SenHaaInit \uni0633.init_SenHaaInit \uni077E.init_SenHaaInit \uni0634.init_SenHaaInit \uni075C.init_SenHaaInit \uni069A.init_SenHaaInit \uni069B.init_SenHaaInit \uni069C.init_SenHaaInit ] <anchor 852 1640> mark @TashkilAbove;
  pos base [\aMem.medi_LamMemHaaInit \uni0645.medi_LamMemHaaInit ] <anchor 410 1640> mark @TashkilAbove;
  pos base [\aHaa.medi_LamMemHaaInit \uni062E.medi_LamMemHaaInit \uni062D.medi_LamMemHaaInit \uni0681.medi_LamMemHaaInit \uni0687.medi_LamMemHaaInit \uni0685.medi_LamMemHaaInit \uni062C.medi_LamMemHaaInit \uni0682.medi_LamMemHaaInit \uni0757.medi_LamMemHaaInit \uni0684.medi_LamMemHaaInit \uni076F.medi_LamMemHaaInit \uni076E.medi_LamMemHaaInit \uni0683.medi_LamMemHaaInit \uni06BF.medi_LamMemHaaInit \uni077C.medi_LamMemHaaInit \uni0758.medi_LamMemHaaInit \uni0772.medi_LamMemHaaInit \uni0686.medi_LamMemHaaInit ] <anchor 113 1640> mark @TashkilAbove;
  pos base [\uni0644.init_Lellah \uni0644.medi_FaLellah ] <anchor 90 1300> mark @TashkilAbove;
  pos base [\uni0644.medi_Lellah ] <anchor 150 1200> mark @TashkilAbove;
  pos base [\uni0647.fina_Lellah ] <anchor 180 850> mark @TashkilAbove;
  pos base [\aBaa.init_BaaBaaHaaInit \aBaa.medi_High \uni0777.init_BaaBaaHaaInit \uni0680.init_BaaBaaHaaInit \uni06BC.init_BaaBaaHaaInit \uni0750.init_BaaBaaHaaInit \uni0756.init_BaaBaaHaaInit \uni06CE.init_BaaBaaHaaInit \uni06BD.init_BaaBaaHaaInit \uni066E.init_BaaBaaHaaInit \uni0620.init_BaaBaaHaaInit \uni064A.init_BaaBaaHaaInit \uni0755.init_BaaBaaHaaInit \uni067D.init_BaaBaaHaaInit \uni067E.init_BaaBaaHaaInit \uni067B.init_BaaBaaHaaInit \uni0628.init_BaaBaaHaaInit \uni067A.init_BaaBaaHaaInit \uni0646.init_BaaBaaHaaInit \uni0753.init_BaaBaaHaaInit \uni0752.init_BaaBaaHaaInit \uni062A.init_BaaBaaHaaInit \uni063D.init_BaaBaaHaaInit \uni06B9.init_BaaBaaHaaInit \uni0649.init_BaaBaaHaaInit \uni067C.init_BaaBaaHaaInit \uni0754.init_BaaBaaHaaInit \uni06D1.init_BaaBaaHaaInit \uni06D0.init_BaaBaaHaaInit \uni06BA.init_BaaBaaHaaInit \uni06CC.init_BaaBaaHaaInit \uni0767.init_BaaBaaHaaInit \uni0777.medi_High \uni0680.medi_High \uni0776.medi_High \uni06BC.medi_High \uni0750.medi_High \uni0756.medi_High \uni06CE.medi_High \uni0775.medi_High \uni06BD.medi_High \uni066E.medi_High \uni0620.medi_High \uni064A.medi_High \uni06BB.medi_High \uni067F.medi_High \uni0755.medi_High \uni067D.medi_High \uni067E.medi_High \uni067B.medi_High \uni0628.medi_High \uni067A.medi_High \uni0751.medi_High \uni0646.medi_High \uni0753.medi_High \uni0752.medi_High \uni062A.medi_High \uni063D.medi_High \uni062B.medi_High \uni0679.medi_High \uni06B9.medi_High \uni0649.medi_High \uni067C.medi_High \uni0754.medi_High \uni06D1.medi_High \uni06D0.medi_High \uni06BA.medi_High \uni06CC.medi_High \uni0767.medi_High \uni0680.init_BaaBaaHaaInitLD \uni06BD.init_BaaBaaHaaInitLD \uni067E.init_BaaBaaHaaInitLD \uni067B.init_BaaBaaHaaInitLD \uni0628.init_BaaBaaHaaInitLD \uni0767.init_BaaBaaHaaInitLD \uni063D.init_BaaBaaHaaInitLD \uni0777.init_BaaBaaHaaInitLD \uni06CC.init_BaaBaaHaaInitLD \uni064A.init_BaaBaaHaaInitLD \uni06CE.init_BaaBaaHaaInitLD \uni0750.init_BaaBaaHaaInitLD \uni0753.init_BaaBaaHaaInitLD \uni0752.init_BaaBaaHaaInitLD \uni0755.init_BaaBaaHaaInitLD \uni0754.init_BaaBaaHaaInitLD \uni06B9.init_BaaBaaHaaInitLD \uni06D1.init_BaaBaaHaaInitLD \uni06D0.init_BaaBaaHaaInitLD \uni0620.init_BaaBaaHaaInitLD \uni08A0.init_BaaBaaHaaInit \uni08A0.medi_High \uni08A0.init_BaaBaaHaaInitLD ] <anchor 195 1640> mark @TashkilAbove;
  pos base [\aBaa.medi_BaaBaaHaaInit \aNon.fina_BaaNonFina \aAyn.init_AynYaaIsol \uni0777.medi_BaaBaaHaaInit \uni0680.medi_BaaBaaHaaInit \uni06BC.medi_BaaBaaHaaInit \uni0750.medi_BaaBaaHaaInit \uni0756.medi_BaaBaaHaaInit \uni06CE.medi_BaaBaaHaaInit \uni06BD.medi_BaaBaaHaaInit \uni066E.medi_BaaBaaHaaInit \uni0620.medi_BaaBaaHaaInit \uni064A.medi_BaaBaaHaaInit \uni067F.medi_BaaBaaHaaInit \uni0755.medi_BaaBaaHaaInit \uni067D.medi_BaaBaaHaaInit \uni067E.medi_BaaBaaHaaInit \uni067B.medi_BaaBaaHaaInit \uni0628.medi_BaaBaaHaaInit \uni067A.medi_BaaBaaHaaInit \uni0751.medi_BaaBaaHaaInit \uni0646.medi_BaaBaaHaaInit \uni0753.medi_BaaBaaHaaInit \uni0752.medi_BaaBaaHaaInit \uni062A.medi_BaaBaaHaaInit \uni063D.medi_BaaBaaHaaInit \uni062B.medi_BaaBaaHaaInit \uni06B9.medi_BaaBaaHaaInit \uni0649.medi_BaaBaaHaaInit \uni067C.medi_BaaBaaHaaInit \uni0754.medi_BaaBaaHaaInit \uni06D1.medi_BaaBaaHaaInit \uni06D0.medi_BaaBaaHaaInit \uni06BA.medi_BaaBaaHaaInit \uni06CC.medi_BaaBaaHaaInit \uni0767.medi_BaaBaaHaaInit \uni0646.fina_BaaNonFina \uni0767.fina_BaaNonFina \uni06BA.fina_BaaNonFina \uni06BC.fina_BaaNonFina \uni06BB.fina_BaaNonFina \uni06B9.fina_BaaNonFina \uni0769.fina_BaaNonFina \uni06BD.fina_BaaNonFina \uni06FC.init_AynYaaIsol \uni063A.init_AynYaaIsol \uni075D.init_AynYaaIsol \uni0639.init_AynYaaIsol \uni08A0.medi_BaaBaaHaaInit ] <anchor 526 1640> mark @TashkilAbove;
  pos base [\aHaa.medi_BaaBaaHaaInit \aBaa.medi_BaaBaaMemInit \uni062E.medi_BaaBaaHaaInit \uni062D.medi_BaaBaaHaaInit \uni0681.medi_BaaBaaHaaInit \uni0687.medi_BaaBaaHaaInit \uni0685.medi_BaaBaaHaaInit \uni062C.medi_BaaBaaHaaInit \uni0682.medi_BaaBaaHaaInit \uni0757.medi_BaaBaaHaaInit \uni0684.medi_BaaBaaHaaInit \uni076F.medi_BaaBaaHaaInit \uni076E.medi_BaaBaaHaaInit \uni0683.medi_BaaBaaHaaInit \uni06BF.medi_BaaBaaHaaInit \uni077C.medi_BaaBaaHaaInit \uni0758.medi_BaaBaaHaaInit \uni0772.medi_BaaBaaHaaInit \uni0686.medi_BaaBaaHaaInit \uni0777.medi_BaaBaaMemInit \uni0680.medi_BaaBaaMemInit \uni0776.medi_BaaBaaMemInit \uni06BC.medi_BaaBaaMemInit \uni0750.medi_BaaBaaMemInit \uni0756.medi_BaaBaaMemInit \uni06CE.medi_BaaBaaMemInit \uni0775.medi_BaaBaaMemInit \uni06BD.medi_BaaBaaMemInit \uni066E.medi_BaaBaaMemInit \uni0620.medi_BaaBaaMemInit \uni064A.medi_BaaBaaMemInit \uni06BB.medi_BaaBaaMemInit \uni067F.medi_BaaBaaMemInit \uni0755.medi_BaaBaaMemInit \uni067D.medi_BaaBaaMemInit \uni067E.medi_BaaBaaMemInit \uni067B.medi_BaaBaaMemInit \uni0628.medi_BaaBaaMemInit \uni067A.medi_BaaBaaMemInit \uni0751.medi_BaaBaaMemInit \uni0646.medi_BaaBaaMemInit \uni0753.medi_BaaBaaMemInit \uni0752.medi_BaaBaaMemInit \uni062A.medi_BaaBaaMemInit \uni063D.medi_BaaBaaMemInit \uni062B.medi_BaaBaaMemInit \uni0679.medi_BaaBaaMemInit \uni06B9.medi_BaaBaaMemInit \uni0649.medi_BaaBaaMemInit \uni067C.medi_BaaBaaMemInit \uni0754.medi_BaaBaaMemInit \uni06D1.medi_BaaBaaMemInit \uni06D0.medi_BaaBaaMemInit \uni06BA.medi_BaaBaaMemInit \uni06CC.medi_BaaBaaMemInit \uni0767.medi_BaaBaaMemInit \uni08A0.medi_BaaBaaMemInit ] <anchor 156 1640> mark @TashkilAbove;
  pos base [\aBaa.medi_SenBaaMemInit \aYaa.fina_PostTooth \uni0777.medi_SenBaaMemInit \uni0680.medi_SenBaaMemInit \uni06BC.medi_SenBaaMemInit \uni0750.medi_SenBaaMemInit \uni0756.medi_SenBaaMemInit \uni06CE.medi_SenBaaMemInit \uni06BD.medi_SenBaaMemInit \uni066E.medi_SenBaaMemInit \uni0620.medi_SenBaaMemInit \uni064A.medi_SenBaaMemInit \uni0755.medi_SenBaaMemInit \uni067D.medi_SenBaaMemInit \uni067E.medi_SenBaaMemInit \uni067B.medi_SenBaaMemInit \uni0628.medi_SenBaaMemInit \uni067A.medi_SenBaaMemInit \uni0751.medi_SenBaaMemInit \uni0646.medi_SenBaaMemInit \uni0753.medi_SenBaaMemInit \uni0752.medi_SenBaaMemInit \uni062A.medi_SenBaaMemInit \uni063D.medi_SenBaaMemInit \uni062B.medi_SenBaaMemInit \uni06B9.medi_SenBaaMemInit \uni0649.medi_SenBaaMemInit \uni067C.medi_SenBaaMemInit \uni0754.medi_SenBaaMemInit \uni06D1.medi_SenBaaMemInit \uni06D0.medi_SenBaaMemInit \uni06BA.medi_SenBaaMemInit \uni06CC.medi_SenBaaMemInit \uni0767.medi_SenBaaMemInit \uni0777.fina_PostTooth \uni06D1.fina_PostTooth \uni0775.fina_PostTooth \uni063F.fina_PostTooth \uni063D.fina_PostTooth \uni063E.fina_PostTooth \uni06D0.fina_PostTooth \uni0649.fina_PostTooth \uni0776.fina_PostTooth \uni06CD.fina_PostTooth \uni06CC.fina_PostTooth \uni0620.fina_PostTooth \uni064A.fina_PostTooth \uni06CE.fina_PostTooth \uni08A0.medi_SenBaaMemInit ] <anchor 496 1640> mark @TashkilAbove;
  pos base [\aMem.medi_SenBaaMemInit \uni0645.medi_SenBaaMemInit ] <anchor 293 1640> mark @TashkilAbove;
  pos base [\aBaa.fina_BaaBaaIsol \uni0751.fina_BaaBaaIsol \uni0750.fina_BaaBaaIsol \uni0753.fina_BaaBaaIsol \uni0680.fina_BaaBaaIsol \uni062A.fina_BaaBaaIsol \uni0754.fina_BaaBaaIsol \uni062B.fina_BaaBaaIsol \uni0679.fina_BaaBaaIsol \uni067C.fina_BaaBaaIsol \uni0756.fina_BaaBaaIsol \uni0752.fina_BaaBaaIsol \uni066E.fina_BaaBaaIsol \uni067F.fina_BaaBaaIsol \uni0755.fina_BaaBaaIsol \uni067D.fina_BaaBaaIsol \uni067E.fina_BaaBaaIsol \uni067B.fina_BaaBaaIsol \uni0628.fina_BaaBaaIsol \uni067A.fina_BaaBaaIsol \uni08A0.fina_BaaBaaIsol ] <anchor 867 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaBaaMemInit \uni0777.init_BaaBaaMemInit \uni0680.init_BaaBaaMemInit \uni06BC.init_BaaBaaMemInit \uni0750.init_BaaBaaMemInit \uni06BD.init_BaaBaaMemInit \uni066E.init_BaaBaaMemInit \uni0620.init_BaaBaaMemInit \uni064A.init_BaaBaaMemInit \uni0755.init_BaaBaaMemInit \uni067E.init_BaaBaaMemInit \uni067B.init_BaaBaaMemInit \uni0628.init_BaaBaaMemInit \uni067A.init_BaaBaaMemInit \uni0646.init_BaaBaaMemInit \uni0753.init_BaaBaaMemInit \uni0752.init_BaaBaaMemInit \uni062A.init_BaaBaaMemInit \uni06B9.init_BaaBaaMemInit \uni0649.init_BaaBaaMemInit \uni067C.init_BaaBaaMemInit \uni0754.init_BaaBaaMemInit \uni06D1.init_BaaBaaMemInit \uni06D0.init_BaaBaaMemInit \uni06BA.init_BaaBaaMemInit \uni06CC.init_BaaBaaMemInit \uni0767.init_BaaBaaMemInit \uni0680.init_BaaBaaMemInitLD \uni06BD.init_BaaBaaMemInitLD \uni067E.init_BaaBaaMemInitLD \uni067B.init_BaaBaaMemInitLD \uni0628.init_BaaBaaMemInitLD \uni0767.init_BaaBaaMemInitLD \uni0777.init_BaaBaaMemInitLD \uni06CC.init_BaaBaaMemInitLD \uni064A.init_BaaBaaMemInitLD \uni0750.init_BaaBaaMemInitLD \uni0753.init_BaaBaaMemInitLD \uni0752.init_BaaBaaMemInitLD \uni0755.init_BaaBaaMemInitLD \uni0754.init_BaaBaaMemInitLD \uni06B9.init_BaaBaaMemInitLD \uni06D1.init_BaaBaaMemInitLD \uni06D0.init_BaaBaaMemInitLD \uni0620.init_BaaBaaMemInitLD \uni08A0.init_BaaBaaMemInit \uni08A0.init_BaaBaaMemInitLD ] <anchor 187 1640> mark @TashkilAbove;
  pos base [\aKaf.medi_KafBaaMedi \uni063C.medi_KafBaaMedi \uni0764.medi_KafBaaMedi \uni0643.medi_KafBaaMedi \uni06B0.medi_KafBaaMedi \uni06B3.medi_KafBaaMedi \uni06B2.medi_KafBaaMedi \uni06AB.medi_KafBaaMedi \uni06AE.medi_KafBaaMedi \uni06AF.medi_KafBaaMedi \uni06A9.medi_KafBaaMedi ] <anchor 438 1640> mark @TashkilAbove;
  pos base [\aBaa.medi_BaaNonFina \aFaa.init_FaaYaaIsol \uni0777.medi_BaaNonFina \uni0680.medi_BaaNonFina \uni0776.medi_BaaNonFina \uni06BC.medi_BaaNonFina \uni0750.medi_BaaNonFina \uni0756.medi_BaaNonFina \uni06CE.medi_BaaNonFina \uni0775.medi_BaaNonFina \uni06BD.medi_BaaNonFina \uni066E.medi_BaaNonFina \uni0620.medi_BaaNonFina \uni064A.medi_BaaNonFina \uni06BB.medi_BaaNonFina \uni067F.medi_BaaNonFina \uni0755.medi_BaaNonFina \uni067D.medi_BaaNonFina \uni067E.medi_BaaNonFina \uni067B.medi_BaaNonFina \uni0628.medi_BaaNonFina \uni067A.medi_BaaNonFina \uni0751.medi_BaaNonFina \uni0646.medi_BaaNonFina \uni0753.medi_BaaNonFina \uni0752.medi_BaaNonFina \uni062A.medi_BaaNonFina \uni063D.medi_BaaNonFina \uni062B.medi_BaaNonFina \uni0679.medi_BaaNonFina \uni06B9.medi_BaaNonFina \uni0769.medi_BaaNonFina \uni0649.medi_BaaNonFina \uni067C.medi_BaaNonFina \uni0754.medi_BaaNonFina \uni06D1.medi_BaaNonFina \uni06D0.medi_BaaNonFina \uni06BA.medi_BaaNonFina \uni06CC.medi_BaaNonFina \uni0767.medi_BaaNonFina \uni066F.init_FaaYaaIsol \uni0761.init_FaaYaaIsol \uni0760.init_FaaYaaIsol \uni0642.init_FaaYaaIsol \uni0641.init_FaaYaaIsol \uni06A1.init_FaaYaaIsol \uni06A2.init_FaaYaaIsol \uni06A3.init_FaaYaaIsol \uni06A5.init_FaaYaaIsol \uni06A7.init_FaaYaaIsol \uni08A0.medi_BaaNonFina ] <anchor -1 1640> mark @TashkilAbove;
  pos base [\aHaa.init_HaaRaaIsol \uni062E.init_HaaRaaIsol \uni062D.init_HaaRaaIsol \uni0681.init_HaaRaaIsol \uni0687.init_HaaRaaIsol \uni0685.init_HaaRaaIsol \uni062C.init_HaaRaaIsol \uni0682.init_HaaRaaIsol \uni0757.init_HaaRaaIsol \uni0684.init_HaaRaaIsol \uni076F.init_HaaRaaIsol \uni076E.init_HaaRaaIsol \uni0683.init_HaaRaaIsol \uni06BF.init_HaaRaaIsol \uni077C.init_HaaRaaIsol \uni0758.init_HaaRaaIsol \uni0772.init_HaaRaaIsol \uni0686.init_HaaRaaIsol ] <anchor 469 1640> mark @TashkilAbove;
  pos base [\aRaa.fina_HaaRaaIsol ] <anchor 703 1640> mark @TashkilAbove;
  pos base [\aHeh.init_HehHaaInit \uni0647.init_HehHaaInit \uni06C1.init_HehHaaInit \uni0777.init_BaaHaaInit \uni0680.init_BaaHaaInit \uni06BC.init_BaaHaaInit \uni0750.init_BaaHaaInit \uni06BD.init_BaaHaaInit \uni066E.init_BaaHaaInit \uni0620.init_BaaHaaInit \uni064A.init_BaaHaaInit \uni0755.init_BaaHaaInit \uni067E.init_BaaHaaInit \uni067B.init_BaaHaaInit \uni0628.init_BaaHaaInit \uni0646.init_BaaHaaInit \uni0753.init_BaaHaaInit \uni0752.init_BaaHaaInit \uni062A.init_BaaHaaInit \uni06B9.init_BaaHaaInit \uni0649.init_BaaHaaInit \uni067C.init_BaaHaaInit \uni0754.init_BaaHaaInit \uni06D1.init_BaaHaaInit \uni06D0.init_BaaHaaInit \uni06BA.init_BaaHaaInit \uni06CC.init_BaaHaaInit \uni0767.init_BaaHaaInit \uni0680.init_BaaHaaInitLD \uni06BD.init_BaaHaaInitLD \uni067E.init_BaaHaaInitLD \uni067B.init_BaaHaaInitLD \uni0628.init_BaaHaaInitLD \uni0767.init_BaaHaaInitLD \uni0777.init_BaaHaaInitLD \uni06CC.init_BaaHaaInitLD \uni064A.init_BaaHaaInitLD \uni0750.init_BaaHaaInitLD \uni0753.init_BaaHaaInitLD \uni0752.init_BaaHaaInitLD \uni0755.init_BaaHaaInitLD \uni0754.init_BaaHaaInitLD \uni06B9.init_BaaHaaInitLD \uni06D1.init_BaaHaaInitLD \uni06D0.init_BaaHaaInitLD \uni0620.init_BaaHaaInitLD ] <anchor 710 1640> mark @TashkilAbove;
  pos base [\aRaa.fina_LamRaaIsol ] <anchor 451 1640> mark @TashkilAbove;
  pos base [\aSad.init_SadHaaInit \uni069D.init_SadHaaInit \uni06FB.init_SadHaaInit \uni0636.init_SadHaaInit \uni0635.init_SadHaaInit ] <anchor 872 1640> mark @TashkilAbove;
  pos base [\aHaa.medi_SadHaaInit \aMem.fina_KafMemIsol \uni062E.medi_SadHaaInit \uni062D.medi_SadHaaInit \uni0681.medi_SadHaaInit \uni0687.medi_SadHaaInit \uni0685.medi_SadHaaInit \uni062C.medi_SadHaaInit \uni0682.medi_SadHaaInit \uni0757.medi_SadHaaInit \uni0684.medi_SadHaaInit \uni076F.medi_SadHaaInit \uni076E.medi_SadHaaInit \uni0683.medi_SadHaaInit \uni06BF.medi_SadHaaInit \uni077C.medi_SadHaaInit \uni0758.medi_SadHaaInit \uni0772.medi_SadHaaInit \uni0686.medi_SadHaaInit \uni0645.fina_KafMemIsol ] <anchor 180 1640> mark @TashkilAbove;
  pos base [\aYaa.fina_BaaYaaFina \uni0777.fina_BaaYaaFina \uni06D1.fina_BaaYaaFina \uni0775.fina_BaaYaaFina \uni063F.fina_BaaYaaFina \uni063D.fina_BaaYaaFina \uni063E.fina_BaaYaaFina \uni06D0.fina_BaaYaaFina \uni0649.fina_BaaYaaFina \uni0776.fina_BaaYaaFina \uni06CD.fina_BaaYaaFina \uni06CC.fina_BaaYaaFina \uni0620.fina_BaaYaaFina \uni064A.fina_BaaYaaFina \uni06CE.fina_BaaYaaFina ] <anchor 392 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaSenAltInit \uni0777.init_BaaSenAltInit \uni0680.init_BaaSenAltInit \uni06BC.init_BaaSenAltInit \uni0750.init_BaaSenAltInit \uni0756.init_BaaSenAltInit \uni06CE.init_BaaSenAltInit \uni06BD.init_BaaSenAltInit \uni066E.init_BaaSenAltInit \uni0620.init_BaaSenAltInit \uni064A.init_BaaSenAltInit \uni0755.init_BaaSenAltInit \uni067D.init_BaaSenAltInit \uni067E.init_BaaSenAltInit \uni067B.init_BaaSenAltInit \uni0628.init_BaaSenAltInit \uni067A.init_BaaSenAltInit \uni0646.init_BaaSenAltInit \uni0753.init_BaaSenAltInit \uni0752.init_BaaSenAltInit \uni062A.init_BaaSenAltInit \uni063D.init_BaaSenAltInit \uni06B9.init_BaaSenAltInit \uni0769.init_BaaSenAltInit \uni0649.init_BaaSenAltInit \uni067C.init_BaaSenAltInit \uni0754.init_BaaSenAltInit \uni06D1.init_BaaSenAltInit \uni06D0.init_BaaSenAltInit \uni06BA.init_BaaSenAltInit \uni06CC.init_BaaSenAltInit \uni0767.init_BaaSenAltInit \uni0680.init_BaaSenAltInitLD \uni06BD.init_BaaSenAltInitLD \uni067E.init_BaaSenAltInitLD \uni067B.init_BaaSenAltInitLD \uni0628.init_BaaSenAltInitLD \uni0767.init_BaaSenAltInitLD \uni063D.init_BaaSenAltInitLD \uni0777.init_BaaSenAltInitLD \uni06CC.init_BaaSenAltInitLD \uni064A.init_BaaSenAltInitLD \uni06CE.init_BaaSenAltInitLD \uni0751.init_BaaSenAltInitLD \uni0750.init_BaaSenAltInitLD \uni0753.init_BaaSenAltInitLD \uni0752.init_BaaSenAltInitLD \uni0755.init_BaaSenAltInitLD \uni0754.init_BaaSenAltInitLD \uni06B9.init_BaaSenAltInitLD \uni06D1.init_BaaSenAltInitLD \uni06D0.init_BaaSenAltInitLD \uni0620.init_BaaSenAltInitLD \uni08A0.init_BaaSenAltInit \uni08A0.init_BaaSenAltInitLD ] <anchor 143 1640> mark @TashkilAbove;
  pos base [\aSen.medi_BaaSenAltInit \uni06FA.medi_BaaSenAltInit \uni076D.medi_BaaSenAltInit \uni0633.medi_BaaSenAltInit \uni077E.medi_BaaSenAltInit \uni077D.medi_BaaSenAltInit \uni0634.medi_BaaSenAltInit \uni075C.medi_BaaSenAltInit \uni069A.medi_BaaSenAltInit \uni069B.medi_BaaSenAltInit \uni069C.medi_BaaSenAltInit ] <anchor 14 1640> mark @TashkilAbove;
  pos base [\aRaa.fina_PostTooth ] <anchor 666 1640> mark @TashkilAbove;
  pos base [\aBaa.init_AboveHaa \uni0777.init_AboveHaa \uni0680.init_AboveHaa \uni06BC.init_AboveHaa \uni0750.init_AboveHaa \uni06BD.init_AboveHaa \uni066E.init_AboveHaa \uni0620.init_AboveHaa \uni064A.init_AboveHaa \uni0755.init_AboveHaa \uni067E.init_AboveHaa \uni067B.init_AboveHaa \uni0628.init_AboveHaa \uni0646.init_AboveHaa \uni0753.init_AboveHaa \uni0752.init_AboveHaa \uni062A.init_AboveHaa \uni06B9.init_AboveHaa \uni0649.init_AboveHaa \uni067C.init_AboveHaa \uni0754.init_AboveHaa \uni06D1.init_AboveHaa \uni06D0.init_AboveHaa \uni06BA.init_AboveHaa \uni06CC.init_AboveHaa \uni0767.init_AboveHaa \uni08A0.init_AboveHaa ] <anchor 849 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaHaaInit \aKaf.fina_KafKafFina \aKaf.fina_LamKafFina \uni063C.fina_KafKafFina \uni0764.fina_KafKafFina \uni0643.fina_KafKafFina \uni06B0.fina_KafKafFina \uni06B3.fina_KafKafFina \uni06B2.fina_KafKafFina \uni06AB.fina_KafKafFina \uni06AC.fina_KafKafFina \uni06AE.fina_KafKafFina \uni06AF.fina_KafKafFina \uni06A9.fina_KafKafFina \uni0762.fina_KafKafFina \uni063B.fina_LamKafFina \uni063C.fina_LamKafFina \uni077F.fina_LamKafFina \uni0764.fina_LamKafFina \uni0643.fina_LamKafFina \uni06B0.fina_LamKafFina \uni06B3.fina_LamKafFina \uni06B2.fina_LamKafFina \uni06AB.fina_LamKafFina \uni06AC.fina_LamKafFina \uni06AE.fina_LamKafFina \uni06AF.fina_LamKafFina \uni06A9.fina_LamKafFina \uni0762.fina_LamKafFina \uni06B1.fina_LamKafFina \uni08A0.init_BaaHaaInit \uni08A0.init_BaaHaaInitLD ] <anchor 750 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaHaaMemInit \uni0777.init_BaaHaaMemInit \uni0680.init_BaaHaaMemInit \uni0750.init_BaaHaaMemInit \uni06BD.init_BaaHaaMemInit \uni066E.init_BaaHaaMemInit \uni0620.init_BaaHaaMemInit \uni064A.init_BaaHaaMemInit \uni0755.init_BaaHaaMemInit \uni067E.init_BaaHaaMemInit \uni067B.init_BaaHaaMemInit \uni0628.init_BaaHaaMemInit \uni0752.init_BaaHaaMemInit \uni0649.init_BaaHaaMemInit \uni06D1.init_BaaHaaMemInit \uni06D0.init_BaaHaaMemInit \uni06CC.init_BaaHaaMemInit \uni0680.init_BaaHaaMemInitLD \uni06BD.init_BaaHaaMemInitLD \uni067E.init_BaaHaaMemInitLD \uni067B.init_BaaHaaMemInitLD \uni0628.init_BaaHaaMemInitLD \uni063D.init_BaaHaaMemInitLD \uni0777.init_BaaHaaMemInitLD \uni06CC.init_BaaHaaMemInitLD \uni064A.init_BaaHaaMemInitLD \uni06CE.init_BaaHaaMemInitLD \uni0750.init_BaaHaaMemInitLD \uni0752.init_BaaHaaMemInitLD \uni0755.init_BaaHaaMemInitLD \uni06D1.init_BaaHaaMemInitLD \uni06D0.init_BaaHaaMemInitLD \uni0620.init_BaaHaaMemInitLD \uni08A0.init_BaaHaaMemInit \uni08A0.init_BaaHaaMemInitLD ] <anchor 364 1640> mark @TashkilAbove;
  pos base [\aHaa.medi_BaaHaaMemInit \uni062E.medi_BaaHaaMemInit \uni062D.medi_BaaHaaMemInit \uni0681.medi_BaaHaaMemInit \uni0687.medi_BaaHaaMemInit \uni062C.medi_BaaHaaMemInit \uni0682.medi_BaaHaaMemInit \uni0757.medi_BaaHaaMemInit \uni0684.medi_BaaHaaMemInit \uni076F.medi_BaaHaaMemInit \uni076E.medi_BaaHaaMemInit \uni0683.medi_BaaHaaMemInit \uni06BF.medi_BaaHaaMemInit \uni077C.medi_BaaHaaMemInit \uni0758.medi_BaaHaaMemInit \uni0686.medi_BaaHaaMemInit ] <anchor 316 1640> mark @TashkilAbove;
  pos base [\aHaa.fina_AboveHaaIsol \uni062E.fina_AboveHaaIsol \uni062D.fina_AboveHaaIsol \uni0681.fina_AboveHaaIsol \uni0687.fina_AboveHaaIsol \uni0685.fina_AboveHaaIsol \uni062C.fina_AboveHaaIsol \uni0682.fina_AboveHaaIsol \uni0757.fina_AboveHaaIsol \uni0684.fina_AboveHaaIsol \uni076F.fina_AboveHaaIsol \uni076E.fina_AboveHaaIsol \uni0683.fina_AboveHaaIsol \uni06BF.fina_AboveHaaIsol \uni077C.fina_AboveHaaIsol \uni0758.fina_AboveHaaIsol \uni0686.fina_AboveHaaIsol ] <anchor 327 1640> mark @TashkilAbove;
  pos base [\aLam.init_LamHaaHaaInit \uni0644.init_LamHaaHaaInit \uni06B8.init_LamHaaHaaInit \uni076A.init_LamHaaHaaInit ] <anchor 590 1640> mark @TashkilAbove;
  pos base [\aHaa.medi_1LamHaaHaaInit \uni062E.medi_1LamHaaHaaInit \uni062D.medi_1LamHaaHaaInit \uni0681.medi_1LamHaaHaaInit \uni0687.medi_1LamHaaHaaInit \uni0685.medi_1LamHaaHaaInit \uni062C.medi_1LamHaaHaaInit \uni0682.medi_1LamHaaHaaInit \uni0757.medi_1LamHaaHaaInit \uni0684.medi_1LamHaaHaaInit \uni076F.medi_1LamHaaHaaInit \uni076E.medi_1LamHaaHaaInit \uni0683.medi_1LamHaaHaaInit \uni06BF.medi_1LamHaaHaaInit \uni077C.medi_1LamHaaHaaInit \uni0758.medi_1LamHaaHaaInit \uni0686.medi_1LamHaaHaaInit \aSen.init_YaaBarree \uni06FA.init_YaaBarree \uni076D.init_YaaBarree \uni0633.init_YaaBarree \uni077E.init_YaaBarree \uni077D.init_YaaBarree \uni0634.init_YaaBarree \uni075C.init_YaaBarree \uni069A.init_YaaBarree \uni069B.init_YaaBarree \uni069C.init_YaaBarree ] <anchor 391 1640> mark @TashkilAbove;
  pos base [\aAyn.init_Finjani \uni06FC.init_Finjani \uni063A.init_Finjani \uni075E.init_Finjani \uni075D.init_Finjani \uni075F.init_Finjani \uni06A0.init_Finjani \uni0639.init_Finjani ] <anchor 229 1640> mark @TashkilAbove;
  pos base [\aHaa.init_Finjani \aKaf.init_KafHeh \uni062E.init_Finjani \uni062D.init_Finjani \uni0681.init_Finjani \uni0687.init_Finjani \uni0685.init_Finjani \uni062C.init_Finjani \uni0682.init_Finjani \uni0757.init_Finjani \uni0684.init_Finjani \uni076F.init_Finjani \uni076E.init_Finjani \uni0683.init_Finjani \uni06BF.init_Finjani \uni077C.init_Finjani \uni0758.init_Finjani \uni0772.init_Finjani \uni0686.init_Finjani \uni063C.init_KafHeh \uni0764.init_KafHeh \uni0643.init_KafHeh \uni06B0.init_KafHeh \uni06B3.init_KafHeh \uni06B2.init_KafHeh \uni06AB.init_KafHeh \uni06AE.init_KafHeh \uni06AF.init_KafHeh \uni06A9.init_KafHeh ] <anchor 370 1640> mark @TashkilAbove;
  pos base [\aHaa.medi_Finjani \uni0691.fina_BaaRaaIsol \uni0692.fina_BaaRaaIsol \uni0693.fina_BaaRaaIsol \uni0694.fina_BaaRaaIsol \uni0695.fina_BaaRaaIsol \uni0696.fina_BaaRaaIsol \uni0697.fina_BaaRaaIsol \uni0698.fina_BaaRaaIsol \uni0699.fina_BaaRaaIsol \uni075B.fina_BaaRaaIsol \uni06EF.fina_BaaRaaIsol \uni0632.fina_BaaRaaIsol \uni0631.fina_BaaRaaIsol \uni076B.fina_BaaRaaIsol \uni062E.medi_Finjani \uni062D.medi_Finjani \uni0681.medi_Finjani \uni0687.medi_Finjani \uni0685.medi_Finjani \uni062C.medi_Finjani \uni0682.medi_Finjani \uni0757.medi_Finjani \uni0684.medi_Finjani \uni076F.medi_Finjani \uni076E.medi_Finjani \uni0683.medi_Finjani \uni06BF.medi_Finjani \uni077C.medi_Finjani \uni0758.medi_Finjani \uni0772.medi_Finjani \uni0686.medi_Finjani ] <anchor 340 1640> mark @TashkilAbove;
  pos base [\aSen.init_PreYaa \uni06FA.init_PreYaa \uni076D.init_PreYaa \uni0633.init_PreYaa \uni077E.init_PreYaa \uni077D.init_PreYaa \uni0634.init_PreYaa \uni075C.init_PreYaa \uni069A.init_PreYaa \uni069B.init_PreYaa \uni069C.init_PreYaa ] <anchor 221 1640> mark @TashkilAbove;
  pos base [\aSen.medi_PreYaa \aMem.medi_AlfPostTooth \aLam.init_LamLamInit \uni06FA.medi_PreYaa \uni076D.medi_PreYaa \uni0633.medi_PreYaa \uni077E.medi_PreYaa \uni077D.medi_PreYaa \uni0634.medi_PreYaa \uni075C.medi_PreYaa \uni069A.medi_PreYaa \uni069B.medi_PreYaa \uni069C.medi_PreYaa \uni0645.medi_AlfPostTooth \uni0644.init_LamLamInit \uni06B8.init_LamLamInit ] <anchor 225 1640> mark @TashkilAbove;
  pos base [\aSad.medi_PreYaa \uni069D.medi_PreYaa \uni06FB.medi_PreYaa \uni0636.medi_PreYaa \uni069E.medi_PreYaa \uni0635.medi_PreYaa ] <anchor 275 1640> mark @TashkilAbove;
  pos base [\aBaa.init_High \uni0777.init_High \uni0680.init_High \uni0776.init_High \uni06BC.init_High \uni0750.init_High \uni0756.init_High \uni06CE.init_High \uni0775.init_High \uni06BD.init_High \uni066E.init_High \uni0620.init_High \uni064A.init_High \uni06BB.init_High \uni067F.init_High \uni0755.init_High \uni067D.init_High \uni067E.init_High \uni067B.init_High \uni0628.init_High \uni067A.init_High \uni0751.init_High \uni0646.init_High \uni0753.init_High \uni0752.init_High \uni062A.init_High \uni063D.init_High \uni062B.init_High \uni0679.init_High \uni06B9.init_High \uni0769.init_High \uni0649.init_High \uni067C.init_High \uni0754.init_High \uni06D1.init_High \uni06D0.init_High \uni06BA.init_High \uni06CC.init_High \uni0767.init_High \uni0680.init_HighLD \uni06BD.init_HighLD \uni067E.init_HighLD \uni067B.init_HighLD \uni0628.init_HighLD \uni0767.init_HighLD \uni063D.init_HighLD \uni0777.init_HighLD \uni0776.init_HighLD \uni0775.init_HighLD \uni06CC.init_HighLD \uni064A.init_HighLD \uni06CE.init_HighLD \uni0751.init_HighLD \uni0750.init_HighLD \uni0753.init_HighLD \uni0752.init_HighLD \uni0755.init_HighLD \uni0754.init_HighLD \uni06B9.init_HighLD \uni06D1.init_HighLD \uni06D0.init_HighLD \uni0620.init_HighLD \uni08A0.init_High \uni08A0.init_HighLD ] <anchor 95 1640> mark @TashkilAbove;
  pos base [\aKaf.isol.alt \uni06AA ] <anchor 1108 1640> mark @TashkilAbove;
  pos base [\aKaf.medi.alt \uni06AA.medi ] <anchor 534 1640> mark @TashkilAbove;
  pos base [\aKaf.fina.alt \uni06AA.fina ] <anchor 1104 1640> mark @TashkilAbove;
  pos base [\aSen.fina_BaaSen \uni06FA.fina_BaaSen \uni076D.fina_BaaSen \uni0633.fina_BaaSen \uni077E.fina_BaaSen \uni077D.fina_BaaSen \uni0634.fina_BaaSen \uni075C.fina_BaaSen \uni069A.fina_BaaSen \uni069B.fina_BaaSen \uni069C.fina_BaaSen ] <anchor 1174 1640> mark @TashkilAbove;
  pos base [\aBaa.init_Wide \uni0776.init_Wide \uni0775.init_Wide \uni06BB.init_Wide \uni0679.init_Wide \uni0680.init_WideLD \uni06BD.init_WideLD \uni067E.init_WideLD \uni067B.init_WideLD \uni0628.init_WideLD \uni0767.init_WideLD \uni063D.init_WideLD \uni0777.init_WideLD \uni0776.init_WideLD \uni0775.init_WideLD \uni06CC.init_WideLD \uni064A.init_WideLD \uni06CE.init_WideLD \uni0751.init_WideLD \uni0750.init_WideLD \uni0753.init_WideLD \uni0752.init_WideLD \uni0755.init_WideLD \uni0754.init_WideLD \uni06B9.init_WideLD \uni06D1.init_WideLD \uni06D0.init_WideLD \uni0620.init_WideLD \uni08A0.init_Wide \uni08A0.init_WideLD ] <anchor 235 1640> mark @TashkilAbove;
  pos base [\aHaa.medi_HaaHaaInit \uni062E.medi_HaaHaaInit \uni062D.medi_HaaHaaInit \uni0681.medi_HaaHaaInit \uni0687.medi_HaaHaaInit \uni0685.medi_HaaHaaInit \uni062C.medi_HaaHaaInit \uni0682.medi_HaaHaaInit \uni0757.medi_HaaHaaInit \uni0684.medi_HaaHaaInit \uni076F.medi_HaaHaaInit \uni076E.medi_HaaHaaInit \uni0683.medi_HaaHaaInit \uni06BF.medi_HaaHaaInit \uni077C.medi_HaaHaaInit \uni0758.medi_HaaHaaInit \uni0686.medi_HaaHaaInit ] <anchor 302 1640> mark @TashkilAbove;
  pos base [\aMem.medi_LamMemInitTatweel \aMem.medi_MemAlfFina \uni0645.medi_MemAlfFina ] <anchor 110 1640> mark @TashkilAbove;
  pos base [\aHaa.init_AboveHaa \uni062E.init_AboveHaa \uni062D.init_AboveHaa \uni0681.init_AboveHaa \uni0687.init_AboveHaa \uni062C.init_AboveHaa \uni0757.init_AboveHaa \uni0684.init_AboveHaa \uni076F.init_AboveHaa \uni076E.init_AboveHaa \uni0683.init_AboveHaa \uni06BF.init_AboveHaa \uni077C.init_AboveHaa \uni0758.init_AboveHaa \uni0686.init_AboveHaa ] <anchor 837 1640> mark @TashkilAbove;
  pos base [\aAyn.init_AboveHaa \uni06FC.init_AboveHaa \uni063A.init_AboveHaa \uni075D.init_AboveHaa \uni0639.init_AboveHaa ] <anchor 748 1690> mark @TashkilAbove;
  pos base [\aHaa.fina_AboveHaaIsol2 \uni062E.fina_AboveHaaIsol2 \uni062D.fina_AboveHaaIsol2 \uni0681.fina_AboveHaaIsol2 \uni0687.fina_AboveHaaIsol2 \uni0685.fina_AboveHaaIsol2 \uni062C.fina_AboveHaaIsol2 \uni0682.fina_AboveHaaIsol2 \uni0757.fina_AboveHaaIsol2 \uni0684.fina_AboveHaaIsol2 \uni076F.fina_AboveHaaIsol2 \uni076E.fina_AboveHaaIsol2 \uni0683.fina_AboveHaaIsol2 \uni06BF.fina_AboveHaaIsol2 \uni077C.fina_AboveHaaIsol2 \uni0758.fina_AboveHaaIsol2 \uni0772.fina_AboveHaaIsol2 \uni0686.fina_AboveHaaIsol2 ] <anchor 197 1640> mark @TashkilAbove;
  pos base [\aMem.init_AboveHaa \uni0635.medi \uni0645.init_AboveHaa ] <anchor 740 1640> mark @TashkilAbove;
  pos base [\aKaf.init_AboveHaa \uni063C.init_AboveHaa \uni0764.init_AboveHaa \uni0643.init_AboveHaa \uni06B0.init_AboveHaa \uni06B3.init_AboveHaa \uni06B2.init_AboveHaa \uni06AB.init_AboveHaa \uni06AE.init_AboveHaa \uni06AF.init_AboveHaa \uni06A9.init_AboveHaa ] <anchor 615 1693> mark @TashkilAbove;
  pos base [\aKaf.init_KafLam \uni063B.init_KafLam \uni063C.init_KafLam \uni077F.init_KafLam \uni0764.init_KafLam \uni0643.init_KafLam \uni06B0.init_KafLam \uni06B3.init_KafLam \uni06B2.init_KafLam \uni06AB.init_KafLam \uni06AC.init_KafLam \uni06AE.init_KafLam \uni06AF.init_KafLam \uni06A9.init_KafLam \uni0762.init_KafLam \uni06B1.init_KafLam ] <anchor 407 1640> mark @TashkilAbove;
  pos base [\aKaf.medi_KafLam \uni063B.medi_KafLam \uni063C.medi_KafLam \uni077F.medi_KafLam \uni0764.medi_KafLam \uni0643.medi_KafLam \uni06B0.medi_KafLam \uni06B3.medi_KafLam \uni06B2.medi_KafLam \uni06AB.medi_KafLam \uni06AC.medi_KafLam \uni06AE.medi_KafLam \uni06AF.medi_KafLam \uni06A9.medi_KafLam \uni0762.medi_KafLam \uni06B1.medi_KafLam ] <anchor 437 1640> mark @TashkilAbove;
  pos base [\aLam.medi_KafLamHehIsol \aLam.medi_LamLamHehIsol \aLam.medi_LamLamHehFina \uni0644.medi_KafLamHehIsol \uni06B8.medi_KafLamHehIsol \uni076A.medi_KafLamHehIsol \uni0644.medi_LamLamHehIsol \uni06B8.medi_LamLamHehIsol \uni076A.medi_LamLamHehIsol \uni0644.medi_LamLamHehFina \uni06B8.medi_LamLamHehFina \uni076A.medi_LamLamHehFina ] <anchor 176 1640> mark @TashkilAbove;
  pos base [\aKaf.init_KafMemAlf \uni063B.init_KafMemAlf \uni063C.init_KafMemAlf \uni077F.init_KafMemAlf \uni0764.init_KafMemAlf \uni0643.init_KafMemAlf \uni06B0.init_KafMemAlf \uni06B3.init_KafMemAlf \uni06B2.init_KafMemAlf \uni06AB.init_KafMemAlf \uni06AC.init_KafMemAlf \uni06AE.init_KafMemAlf \uni06AF.init_KafMemAlf \uni06A9.init_KafMemAlf \uni0762.init_KafMemAlf \uni06B1.init_KafMemAlf ] <anchor 725 1640> mark @TashkilAbove;
  pos base [\aLam.fina_KafMemLam \uni06CB \uni06CA \uni06CF \uni0778 \uni06C6 \uni06C7 \uni06C4 \uni06C5 \uni0676 \uni06C8 \uni06C9 \uni0779 \uni0648 \uni06B7.fina_KafMemLam \uni0644.fina_KafMemLam \uni06B8.fina_KafMemLam \uni06B6.fina_KafMemLam \uni076A.fina_KafMemLam ] <anchor 430 1640> mark @TashkilAbove;
  pos base [\aKaf.medi_KafHeh \uni063C.medi_KafHeh \uni0764.medi_KafHeh \uni0643.medi_KafHeh \uni06B0.medi_KafHeh \uni06B3.medi_KafHeh \uni06B2.medi_KafHeh \uni06AB.medi_KafHeh \uni06AC.medi_KafHeh \uni06AE.medi_KafHeh \uni06AF.medi_KafHeh \uni06A9.medi_KafHeh \uni0762.medi_KafHeh ] <anchor 352 1640> mark @TashkilAbove;
  pos base [\aDal.fina_KafDal \uni0690.fina_KafDal \uni06EE.fina_KafDal \uni0689.fina_KafDal \uni0688.fina_KafDal \uni075A.fina_KafDal \uni0630.fina_KafDal \uni062F.fina_KafDal \uni0759.fina_KafDal \uni068C.fina_KafDal \uni068B.fina_KafDal \uni068A.fina_KafDal \uni068F.fina_KafDal \uni068E.fina_KafDal \uni068D.fina_KafDal ] <anchor 371 1640> mark @TashkilAbove;
  pos base [\aLam.init_LamHeh \uni0644.init_LamHeh \uni06B8.init_LamHeh \uni076A.init_LamHeh ] <anchor -75 1640> mark @TashkilAbove;
  pos base [\aLam.medi_LamHeh \uni0644.medi_LamHeh \uni06B8.medi_LamHeh \uni076A.medi_LamHeh ] <anchor -7 1640> mark @TashkilAbove;
  pos base [\aKaf.medi_KafMemMedi \uni063C.medi_KafMemMedi \uni0764.medi_KafMemMedi \uni0643.medi_KafMemMedi \uni06B0.medi_KafMemMedi \uni06B3.medi_KafMemMedi \uni06B2.medi_KafMemMedi \uni06AB.medi_KafMemMedi \uni06AC.medi_KafMemMedi \uni06AE.medi_KafMemMedi \uni06AF.medi_KafMemMedi \uni06A9.medi_KafMemMedi \uni0762.medi_KafMemMedi ] <anchor 400 1750> mark @TashkilAbove;
  pos base [\aKaf.init_KafMemInit \uni063C.init_KafMemInit \uni0764.init_KafMemInit \uni0643.init_KafMemInit \uni06B0.init_KafMemInit \uni06B3.init_KafMemInit \uni06B2.init_KafMemInit \uni06AB.init_KafMemInit \uni06AC.init_KafMemInit \uni06AE.init_KafMemInit \uni06AF.init_KafMemInit \uni06A9.init_KafMemInit \uni0762.init_KafMemInit ] <anchor 300 1700> mark @TashkilAbove;
  pos base [\aAyn.init_AynMemInit \uni06FC.init_AynMemInit \uni063A.init_AynMemInit \uni0639.init_AynMemInit ] <anchor 336 1640> mark @TashkilAbove;
  pos base [\aHaa.init_HaaMemInit \uni062E.init_HaaMemInit \uni062D.init_HaaMemInit \uni0681.init_HaaMemInit \uni0687.init_HaaMemInit \uni0685.init_HaaMemInit \uni062C.init_HaaMemInit \uni0682.init_HaaMemInit \uni0757.init_HaaMemInit \uni0684.init_HaaMemInit \uni076F.init_HaaMemInit \uni076E.init_HaaMemInit \uni0683.init_HaaMemInit \uni06BF.init_HaaMemInit \uni077C.init_HaaMemInit \uni0758.init_HaaMemInit \uni0686.init_HaaMemInit ] <anchor 512 1640> mark @TashkilAbove;
  pos base [\aSen.init_SenMemInit \uni06FA.init_SenMemInit \uni076D.init_SenMemInit \uni0633.init_SenMemInit \uni077E.init_SenMemInit \uni077D.init_SenMemInit \uni0634.init_SenMemInit \uni075C.init_SenMemInit \uni069A.init_SenMemInit \uni069B.init_SenMemInit \uni069C.init_SenMemInit ] <anchor 509 1640> mark @TashkilAbove;
  pos base [\aSad.init_SadMemInit \uni069D.init_SadMemInit \uni06FB.init_SadMemInit \uni0636.init_SadMemInit \uni0635.init_SadMemInit ] <anchor 665 1640> mark @TashkilAbove;
  pos base [\aMem.init_MemMemInit \uni0645.init_MemMemInit ] <anchor 324 1640> mark @TashkilAbove;
  pos base [\aKaf.init_KafYaaIsol \uni063C.init_KafYaaIsol \uni0764.init_KafYaaIsol \uni0643.init_KafYaaIsol \uni06B0.init_KafYaaIsol \uni06B3.init_KafYaaIsol \uni06B2.init_KafYaaIsol \uni06AB.init_KafYaaIsol \uni06AC.init_KafYaaIsol \uni06AE.init_KafYaaIsol \uni06AF.init_KafYaaIsol \uni06A9.init_KafYaaIsol \uni0762.init_KafYaaIsol ] <anchor 100 1750> mark @TashkilAbove;
  pos base [\aBaa.init_BaaYaaIsol \uni0777.init_BaaYaaIsol \uni0680.init_BaaYaaIsol \uni06BC.init_BaaYaaIsol \uni0750.init_BaaYaaIsol \uni06BD.init_BaaYaaIsol \uni066E.init_BaaYaaIsol \uni0620.init_BaaYaaIsol \uni064A.init_BaaYaaIsol \uni0755.init_BaaYaaIsol \uni067E.init_BaaYaaIsol \uni067B.init_BaaYaaIsol \uni0628.init_BaaYaaIsol \uni067A.init_BaaYaaIsol \uni0646.init_BaaYaaIsol \uni0753.init_BaaYaaIsol \uni0752.init_BaaYaaIsol \uni062A.init_BaaYaaIsol \uni06B9.init_BaaYaaIsol \uni0649.init_BaaYaaIsol \uni067C.init_BaaYaaIsol \uni0754.init_BaaYaaIsol \uni06D1.init_BaaYaaIsol \uni06D0.init_BaaYaaIsol \uni06BA.init_BaaYaaIsol \uni06CC.init_BaaYaaIsol \uni0767.init_BaaYaaIsol \uni0680.init_BaaYaaIsolLD \uni06BD.init_BaaYaaIsolLD \uni067E.init_BaaYaaIsolLD \uni067B.init_BaaYaaIsolLD \uni0628.init_BaaYaaIsolLD \uni0767.init_BaaYaaIsolLD \uni0777.init_BaaYaaIsolLD \uni06CC.init_BaaYaaIsolLD \uni064A.init_BaaYaaIsolLD \uni0750.init_BaaYaaIsolLD \uni0753.init_BaaYaaIsolLD \uni0752.init_BaaYaaIsolLD \uni0755.init_BaaYaaIsolLD \uni0754.init_BaaYaaIsolLD \uni06B9.init_BaaYaaIsolLD \uni06D1.init_BaaYaaIsolLD \uni06D0.init_BaaYaaIsolLD \uni0620.init_BaaYaaIsolLD \uni08A0.init_BaaYaaIsol \uni08A0.init_BaaYaaIsolLD ] <anchor 33 1640> mark @TashkilAbove;
  pos base [\aHaa.init_HaaYaaIsol \uni062E.init_HaaYaaIsol \uni062D.init_HaaYaaIsol \uni0681.init_HaaYaaIsol \uni0687.init_HaaYaaIsol \uni0685.init_HaaYaaIsol \uni062C.init_HaaYaaIsol \uni0682.init_HaaYaaIsol \uni0757.init_HaaYaaIsol \uni0684.init_HaaYaaIsol \uni076F.init_HaaYaaIsol \uni076E.init_HaaYaaIsol \uni0683.init_HaaYaaIsol \uni06BF.init_HaaYaaIsol \uni077C.init_HaaYaaIsol \uni0758.init_HaaYaaIsol \uni0772.init_HaaYaaIsol \uni0686.init_HaaYaaIsol ] <anchor 539 1640> mark @TashkilAbove;
  pos base [\aKaf.init_KafMemIsol \uni063C.init_KafMemIsol \uni0764.init_KafMemIsol \uni0643.init_KafMemIsol \uni06B0.init_KafMemIsol \uni06B3.init_KafMemIsol \uni06B2.init_KafMemIsol \uni06AB.init_KafMemIsol \uni06AE.init_KafMemIsol \uni06AF.init_KafMemIsol \uni06A9.init_KafMemIsol ] <anchor 350 1750> mark @TashkilAbove;
  pos base [\aLam.init_LamMemIsol \uni0644.init_LamMemIsol \uni06B8.init_LamMemIsol \uni076A.init_LamMemIsol ] <anchor 200 1710> mark @TashkilAbove;
  pos base [\aBaa.init_BaaMemIsol \uni0777.init_BaaMemIsol \uni0680.init_BaaMemIsol \uni0776.init_BaaMemIsol \uni06BC.init_BaaMemIsol \uni0750.init_BaaMemIsol \uni0756.init_BaaMemIsol \uni06CE.init_BaaMemIsol \uni0775.init_BaaMemIsol \uni06BD.init_BaaMemIsol \uni066E.init_BaaMemIsol \uni0620.init_BaaMemIsol \uni064A.init_BaaMemIsol \uni06BB.init_BaaMemIsol \uni067F.init_BaaMemIsol \uni0755.init_BaaMemIsol \uni067D.init_BaaMemIsol \uni067E.init_BaaMemIsol \uni067B.init_BaaMemIsol \uni0628.init_BaaMemIsol \uni067A.init_BaaMemIsol \uni0751.init_BaaMemIsol \uni0646.init_BaaMemIsol \uni0753.init_BaaMemIsol \uni0752.init_BaaMemIsol \uni062A.init_BaaMemIsol \uni063D.init_BaaMemIsol \uni062B.init_BaaMemIsol \uni0679.init_BaaMemIsol \uni06B9.init_BaaMemIsol \uni0649.init_BaaMemIsol \uni067C.init_BaaMemIsol \uni0754.init_BaaMemIsol \uni06D1.init_BaaMemIsol \uni06D0.init_BaaMemIsol \uni06BA.init_BaaMemIsol \uni06CC.init_BaaMemIsol \uni0767.init_BaaMemIsol \uni0680.init_BaaMemIsolLD \uni06BD.init_BaaMemIsolLD \uni067E.init_BaaMemIsolLD \uni067B.init_BaaMemIsolLD \uni0628.init_BaaMemIsolLD \uni0767.init_BaaMemIsolLD \uni063D.init_BaaMemIsolLD \uni0777.init_BaaMemIsolLD \uni0776.init_BaaMemIsolLD \uni0775.init_BaaMemIsolLD \uni06CC.init_BaaMemIsolLD \uni064A.init_BaaMemIsolLD \uni06CE.init_BaaMemIsolLD \uni0751.init_BaaMemIsolLD \uni0750.init_BaaMemIsolLD \uni0753.init_BaaMemIsolLD \uni0752.init_BaaMemIsolLD \uni0755.init_BaaMemIsolLD \uni0754.init_BaaMemIsolLD \uni06B9.init_BaaMemIsolLD \uni06D1.init_BaaMemIsolLD \uni06D0.init_BaaMemIsolLD \uni0620.init_BaaMemIsolLD \uni08A0.init_BaaMemIsol \uni08A0.init_BaaMemIsolLD ] <anchor 446 1640> mark @TashkilAbove;
  pos base [\aBaa.medi_BaaMemAlfFina \uni0777.medi_BaaMemAlfFina \uni0680.medi_BaaMemAlfFina \uni0776.medi_BaaMemAlfFina \uni06BC.medi_BaaMemAlfFina \uni0750.medi_BaaMemAlfFina \uni0756.medi_BaaMemAlfFina \uni06CE.medi_BaaMemAlfFina \uni0775.medi_BaaMemAlfFina \uni06BD.medi_BaaMemAlfFina \uni066E.medi_BaaMemAlfFina \uni0620.medi_BaaMemAlfFina \uni064A.medi_BaaMemAlfFina \uni06BB.medi_BaaMemAlfFina \uni067F.medi_BaaMemAlfFina \uni0755.medi_BaaMemAlfFina \uni067D.medi_BaaMemAlfFina \uni067E.medi_BaaMemAlfFina \uni067B.medi_BaaMemAlfFina \uni0628.medi_BaaMemAlfFina \uni067A.medi_BaaMemAlfFina \uni0751.medi_BaaMemAlfFina \uni0646.medi_BaaMemAlfFina \uni0753.medi_BaaMemAlfFina \uni0752.medi_BaaMemAlfFina \uni062A.medi_BaaMemAlfFina \uni063D.medi_BaaMemAlfFina \uni062B.medi_BaaMemAlfFina \uni0679.medi_BaaMemAlfFina \uni06B9.medi_BaaMemAlfFina \uni0769.medi_BaaMemAlfFina \uni0649.medi_BaaMemAlfFina \uni067C.medi_BaaMemAlfFina \uni0754.medi_BaaMemAlfFina \uni06D1.medi_BaaMemAlfFina \uni06D0.medi_BaaMemAlfFina \uni06BA.medi_BaaMemAlfFina \uni06CC.medi_BaaMemAlfFina \uni0767.medi_BaaMemAlfFina \uni08A0.medi_BaaMemAlfFina ] <anchor 383 1640> mark @TashkilAbove;
  pos base [\aMem.medi_BaaMemAlfFina \aLam.medi_LamLamAlfIsol \aLam.medi_LamLamAlefFina \uni0645.medi_BaaMemAlfFina \uni0644.medi_LamLamAlfIsol \uni06B8.medi_LamLamAlfIsol \uni076A.medi_LamLamAlfIsol \uni0644.medi_LamLamAlefFina \uni06B8.medi_LamLamAlefFina \uni076A.medi_LamLamAlefFina ] <anchor 70 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaHehInit \uni0777.init_BaaHehInit \uni0680.init_BaaHehInit \uni0776.init_BaaHehInit \uni06BC.init_BaaHehInit \uni0750.init_BaaHehInit \uni0756.init_BaaHehInit \uni06CE.init_BaaHehInit \uni0775.init_BaaHehInit \uni06BD.init_BaaHehInit \uni066E.init_BaaHehInit \uni0620.init_BaaHehInit \uni064A.init_BaaHehInit \uni06BB.init_BaaHehInit \uni067F.init_BaaHehInit \uni0755.init_BaaHehInit \uni067D.init_BaaHehInit \uni067E.init_BaaHehInit \uni067B.init_BaaHehInit \uni0628.init_BaaHehInit \uni067A.init_BaaHehInit \uni0751.init_BaaHehInit \uni0646.init_BaaHehInit \uni0753.init_BaaHehInit \uni0752.init_BaaHehInit \uni062A.init_BaaHehInit \uni063D.init_BaaHehInit \uni062B.init_BaaHehInit \uni0679.init_BaaHehInit \uni06B9.init_BaaHehInit \uni0769.init_BaaHehInit \uni0649.init_BaaHehInit \uni067C.init_BaaHehInit \uni0754.init_BaaHehInit \uni06D1.init_BaaHehInit \uni06D0.init_BaaHehInit \uni06BA.init_BaaHehInit \uni06CC.init_BaaHehInit \uni0767.init_BaaHehInit \uni0680.init_BaaHehInitLD \uni06BD.init_BaaHehInitLD \uni067E.init_BaaHehInitLD \uni067B.init_BaaHehInitLD \uni0628.init_BaaHehInitLD \uni0767.init_BaaHehInitLD \uni063D.init_BaaHehInitLD \uni0777.init_BaaHehInitLD \uni0776.init_BaaHehInitLD \uni0775.init_BaaHehInitLD \uni06CC.init_BaaHehInitLD \uni064A.init_BaaHehInitLD \uni06CE.init_BaaHehInitLD \uni0751.init_BaaHehInitLD \uni0750.init_BaaHehInitLD \uni0753.init_BaaHehInitLD \uni0752.init_BaaHehInitLD \uni0755.init_BaaHehInitLD \uni0754.init_BaaHehInitLD \uni06B9.init_BaaHehInitLD \uni06D1.init_BaaHehInitLD \uni06D0.init_BaaHehInitLD \uni0620.init_BaaHehInitLD \uni08A0.init_BaaHehInit \uni08A0.init_BaaHehInitLD ] <anchor 543 1640> mark @TashkilAbove;
  pos base [\aHeh.medi_PostTooth \uni0647.medi_PostTooth \uni06C1.medi_PostTooth ] <anchor 382 1640> mark @TashkilAbove;
  pos base [\aKaf.fina_LamKafIsol \uni063B.fina_LamKafIsol \uni063C.fina_LamKafIsol \uni077F.fina_LamKafIsol \uni0764.fina_LamKafIsol \uni0643.fina_LamKafIsol \uni06B0.fina_LamKafIsol \uni06B3.fina_LamKafIsol \uni06B2.fina_LamKafIsol \uni06AB.fina_LamKafIsol \uni06AC.fina_LamKafIsol \uni06AE.fina_LamKafIsol \uni06AF.fina_LamKafIsol \uni06A9.fina_LamKafIsol \uni0762.fina_LamKafIsol \uni06B1.fina_LamKafIsol ] <anchor 720 1640> mark @TashkilAbove;
  pos base [\uni0774.fina \uni0774.fina_Narrow ] <anchor 247 1890> mark @TashkilAbove;
  pos base [\uni0773.fina \uni0773.fina_Narrow ] <anchor 186 1790> mark @TashkilAbove;
  pos base [\uni0623.fina \uni0623.fina_Narrow \uni0623.fina_Wide ] <anchor 261 2150> mark @TashkilAbove;
  pos base [\uni0675.fina \uni0675.fina_Narrow ] <anchor 476 2060> mark @TashkilAbove;
  pos base [\uni0672.fina \uni0672.fina_KafAlf \uni0672.fina_Narrow ] <anchor 76 2150> mark @TashkilAbove;
  pos base [\uni0774 \uni0775.init_BaaBaaMemInit \uni0775.init_BaaBaaMemInitLD ] <anchor 187 1780> mark @TashkilAbove;
  pos base [\uni0773 ] <anchor 126 1780> mark @TashkilAbove;
  pos base [\uni0623 ] <anchor 191 2150> mark @TashkilAbove;
  pos base [\uni0675 ] <anchor 516 2150> mark @TashkilAbove;
  pos base [\uni0672 ] <anchor 116 2150> mark @TashkilAbove;
  pos base [\uni0768.init ] <anchor 85 1810> mark @TashkilAbove;
  pos base [\uni0768.medi ] <anchor 160 1780> mark @TashkilAbove;
  pos base [\uni0690.fina ] <anchor 625 1760> mark @TashkilAbove;
  pos base [\uni0688.fina \uni0759.fina \uni068B.fina ] <anchor 645 1850> mark @TashkilAbove;
  pos base [\uni068F.fina ] <anchor 635 1710> mark @TashkilAbove;
  pos base [\uni068E.fina ] <anchor 566 1717> mark @TashkilAbove;
  pos base [\uni0688 \uni0759 \uni068B ] <anchor 380 1750> mark @TashkilAbove;
  pos base [\uni06A4 ] <anchor 1181 1767> mark @TashkilAbove;
  pos base [\uni06A6 ] <anchor 1240 1810> mark @TashkilAbove;
  pos base [\uni062E.init \uni062D.init \uni0681.init \uni0687.init \uni0685.init \uni062C.init \uni0682.init \uni0757.init \uni0684.init \uni076F.init \uni076E.init \uni0683.init \uni06BF.init \uni077C.init \uni0758.init \uni0772.init \uni0686.init ] <anchor 610 1640> mark @TashkilAbove;
  pos base [\uni06AD.fina \uni06B4.fina \uni0763.fina ] <anchor 711 1863> mark @TashkilAbove;
  pos base [\uni06AD \uni06B4 \uni0763 ] <anchor 401 1857> mark @TashkilAbove;
  pos base [\uni06B5.init ] <anchor 80 2017> mark @TashkilAbove;
  pos base [\uni06B7.init ] <anchor -48 2084> mark @TashkilAbove;
  pos base [\uni06B6.init ] <anchor 30 1827> mark @TashkilAbove;
  pos base [\uni06B5 ] <anchor 662 1878> mark @TashkilAbove;
  pos base [\uni06B5.medi ] <anchor 130 1980> mark @TashkilAbove;
  pos base [\uni06B7.medi ] <anchor 2 2047> mark @TashkilAbove;
  pos base [\uni06B6.medi ] <anchor 80 1790> mark @TashkilAbove;
  pos base [\uni0768 ] <anchor 460 2040> mark @TashkilAbove;
  pos base [\uni0769 ] <anchor 500 1840> mark @TashkilAbove;
  pos base [\uni0771.fina ] <anchor 297 1774> mark @TashkilAbove;
  pos base [\uni0771 ] <anchor 317 1898> mark @TashkilAbove;
  pos base [\uni0770.fina ] <anchor 1535 1710> mark @TashkilAbove;
  pos base [\uni0770.init ] <anchor 590 1860> mark @TashkilAbove;
  pos base [\uni0770 ] <anchor 1472 2030> mark @TashkilAbove;
  pos base [\uni0770.medi ] <anchor 535 1737> mark @TashkilAbove;
  pos base [\uni0678.fina ] <anchor 850 1200> mark @TashkilAbove;
  pos base [\uni0626.fina ] <anchor 280 1020> mark @TashkilAbove;
  pos base [\uni0678 ] <anchor 1420 1610> mark @TashkilAbove;
  pos base [\uni0626 ] <anchor 335 1348> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamMemFina ] <anchor 170 2075> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamMemFina ] <anchor 42 2142> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamMemFina ] <anchor 120 1885> mark @TashkilAbove;
  pos base [\uni0768.init_BaaRaaIsol ] <anchor 154 1860> mark @TashkilAbove;
  pos base [\uni0626.init_BaaRaaIsol ] <anchor 194 1470> mark @TashkilAbove;
  pos base [\uni0678.init_BaaRaaIsol ] <anchor 394 1470> mark @TashkilAbove;
  pos base [\uni0771.fina_BaaRaaIsol ] <anchor 281 1862> mark @TashkilAbove;
  pos base [\uni076C.fina_BaaRaaIsol ] <anchor 292 1422> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamWawFina ] <anchor 220 2025> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamWawFina ] <anchor 92 2092> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamWawFina ] <anchor 170 1835> mark @TashkilAbove;
  pos base [\uni06CB.fina_LamWawFina \uni0624.fina_LamWawFina \uni06CA.fina_LamWawFina \uni06CF.fina_LamWawFina \uni0778.fina_LamWawFina \uni06C6.fina_LamWawFina \uni06C7.fina_LamWawFina \uni06C4.fina_LamWawFina \uni06C5.fina_LamWawFina \uni0676.fina_LamWawFina \uni06C8.fina_LamWawFina \uni06C9.fina_LamWawFina \uni0779.fina_LamWawFina \uni0648.fina_LamWawFina ] <anchor 305 1640> mark @TashkilAbove;
  pos base [\uni0677.fina_LamWawFina ] <anchor 505 1640> mark @TashkilAbove;
  pos base [\uni06B5.init_LamHaaInit ] <anchor 310 2050> mark @TashkilAbove;
  pos base [\uni06B7.init_LamHaaInit ] <anchor 182 2117> mark @TashkilAbove;
  pos base [\uni06B6.init_LamHaaInit ] <anchor 260 1860> mark @TashkilAbove;
  pos base [\uni06B5.init_LamLamHaaInit ] <anchor 9 2066> mark @TashkilAbove;
  pos base [\uni06B7.init_LamLamHaaInit ] <anchor -119 2133> mark @TashkilAbove;
  pos base [\uni06B6.init_LamLamHaaInit ] <anchor -41 1876> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamLamHaaInit ] <anchor 418 2081> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamLamHaaInit ] <anchor 290 2148> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamLamHaaInit ] <anchor 368 1891> mark @TashkilAbove;
  pos base [\uni0772.medi_LamLamHaaInit ] <anchor 130 1750> mark @TashkilAbove;
  pos base [\uni069E.init_AboveHaa ] <anchor 809 1827> mark @TashkilAbove;
  pos base [\uni06B5.init_LamBaaMemInit ] <anchor 170 2035> mark @TashkilAbove;
  pos base [\uni06B7.init_LamBaaMemInit ] <anchor 80 2062> mark @TashkilAbove;
  pos base [\uni06B6.init_LamBaaMemInit ] <anchor 100 1845> mark @TashkilAbove;
  pos base [\uni0776.medi_LamBaaMemInit ] <anchor 77 1730> mark @TashkilAbove;
  pos base [\uni0768.medi_LamBaaMemInit ] <anchor -73 2140> mark @TashkilAbove;
  pos base [\uni0775.medi_LamBaaMemInit ] <anchor 17 1730> mark @TashkilAbove;
  pos base [\uni0626.medi_LamBaaMemInit ] <anchor -33 1750> mark @TashkilAbove;
  pos base [\uni06BB.medi_LamBaaMemInit \uni0679.medi_LamBaaMemInit ] <anchor -123 1680> mark @TashkilAbove;
  pos base [\uni067F.medi_LamBaaMemInit ] <anchor -43 1690> mark @TashkilAbove;
  pos base [\uni0678.medi_LamBaaMemInit ] <anchor 67 1750> mark @TashkilAbove;
  pos base [\uni0769.medi_LamBaaMemInit ] <anchor -33 1940> mark @TashkilAbove;
  pos base [\uni0768.init_BaaDal ] <anchor 65 1936> mark @TashkilAbove;
  pos base [\uni0626.init_BaaDal ] <anchor 60 1544> mark @TashkilAbove;
  pos base [\uni0678.init_BaaDal ] <anchor 260 1544> mark @TashkilAbove;
  pos base [\uni0769.init_BaaDal ] <anchor 105 1736> mark @TashkilAbove;
  pos base [\uni0690.fina_BaaDal ] <anchor 573 1735> mark @TashkilAbove;
  pos base [\uni0688.fina_BaaDal \uni0759.fina_BaaDal \uni068B.fina_BaaDal ] <anchor 584 1850> mark @TashkilAbove;
  pos base [\uni068F.fina_BaaDal ] <anchor 583 1685> mark @TashkilAbove;
  pos base [\uni068E.fina_BaaDal ] <anchor 514 1692> mark @TashkilAbove;
  pos base [\uni0776.init_BaaMemHaaInit \uni0776.init_BaaMemHaaInitLD ] <anchor 151 2226> mark @TashkilAbove;
  pos base [\uni06BC.init_BaaMemHaaInit \uni0646.init_BaaMemHaaInit \uni06B9.init_BaaMemHaaInit \uni0754.init_BaaMemHaaInit \uni06BA.init_BaaMemHaaInit \uni0767.init_BaaMemHaaInit \uni0767.init_BaaMemHaaInitLD \uni0754.init_BaaMemHaaInitLD \uni06B9.init_BaaMemHaaInitLD ] <anchor 41 1886> mark @TashkilAbove;
  pos base [\uni0756.init_BaaMemHaaInit \uni06CE.init_BaaMemHaaInit \uni063D.init_BaaMemHaaInit \uni063D.init_BaaMemHaaInitLD \uni06CE.init_BaaMemHaaInitLD ] <anchor 91 2176> mark @TashkilAbove;
  pos base [\uni0768.init_BaaMemHaaInit ] <anchor 1 2636> mark @TashkilAbove;
  pos base [\uni0775.init_BaaMemHaaInit \uni0775.init_BaaMemHaaInitLD ] <anchor 91 2226> mark @TashkilAbove;
  pos base [\uni0626.init_BaaMemHaaInit ] <anchor 41 2246> mark @TashkilAbove;
  pos base [\uni06BB.init_BaaMemHaaInit \uni0679.init_BaaMemHaaInit ] <anchor 51 2176> mark @TashkilAbove;
  pos base [\uni067F.init_BaaMemHaaInit ] <anchor 31 2186> mark @TashkilAbove;
  pos base [\uni067D.init_BaaMemHaaInit ] <anchor 41 2136> mark @TashkilAbove;
  pos base [\uni067A.init_BaaMemHaaInit ] <anchor 51 2076> mark @TashkilAbove;
  pos base [\uni0751.init_BaaMemHaaInit \uni062B.init_BaaMemHaaInit \uni0751.init_BaaMemHaaInitLD ] <anchor -28 2143> mark @TashkilAbove;
  pos base [\uni0753.init_BaaMemHaaInit \uni062A.init_BaaMemHaaInit \uni067C.init_BaaMemHaaInit \uni0753.init_BaaMemHaaInitLD ] <anchor 71 1936> mark @TashkilAbove;
  pos base [\uni0678.init_BaaMemHaaInit ] <anchor 241 2246> mark @TashkilAbove;
  pos base [\uni0769.init_BaaMemHaaInit ] <anchor 41 2436> mark @TashkilAbove;
  pos base [\uni0772.medi_BaaMemHaaInit ] <anchor 142 1750> mark @TashkilAbove;
  pos base [\uni0776.init_BaaBaaYaa \uni0776.init_BaaBaaYaaLD ] <anchor 189 1765> mark @TashkilAbove;
  pos base [\uni0768.init_BaaBaaYaa ] <anchor 39 2175> mark @TashkilAbove;
  pos base [\uni0775.init_BaaBaaYaa \uni0775.init_BaaBaaYaaLD ] <anchor 129 1765> mark @TashkilAbove;
  pos base [\uni0626.init_BaaBaaYaa ] <anchor 79 1785> mark @TashkilAbove;
  pos base [\uni067F.init_BaaBaaYaa ] <anchor 39 1725> mark @TashkilAbove;
  pos base [\uni067D.init_BaaBaaYaa ] <anchor 49 1675> mark @TashkilAbove;
  pos base [\uni0751.init_BaaBaaYaa \uni062B.init_BaaBaaYaa \uni0751.init_BaaBaaYaaLD ] <anchor -20 1682> mark @TashkilAbove;
  pos base [\uni0678.init_BaaBaaYaa ] <anchor 324 1785> mark @TashkilAbove;
  pos base [\uni0769.init_BaaBaaYaa ] <anchor 79 1975> mark @TashkilAbove;
  pos base [\uni0768.medi_BaaBaaYaa ] <anchor 44 2083> mark @TashkilAbove;
  pos base [\uni0626.medi_BaaBaaYaa ] <anchor 84 1693> mark @TashkilAbove;
  pos base [\uni0678.medi_BaaBaaYaa ] <anchor 207 1693> mark @TashkilAbove;
  pos base [\uni0769.medi_BaaBaaYaa ] <anchor 84 1883> mark @TashkilAbove;
  pos base [\uni0678.fina_BaaBaaYaa ] <anchor 643 1315> mark @TashkilAbove;
  pos base [\uni0626.fina_BaaBaaYaa ] <anchor 276 1315> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamYaaFina ] <anchor 46 1974> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamYaaFina ] <anchor -82 2041> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamYaaFina ] <anchor -4 1784> mark @TashkilAbove;
  pos base [\uni0678.fina_LamYaaFina ] <anchor 632 1055> mark @TashkilAbove;
  pos base [\uni0626.fina_LamYaaFina ] <anchor 332 1055> mark @TashkilAbove;
  pos base [\uni063B.init_KafBaaInit \uni077F.init_KafBaaInit \uni06B1.init_KafBaaInit ] <anchor 151 1980> mark @TashkilAbove;
  pos base [\uni06AC.init_KafBaaInit \uni0762.init_KafBaaInit ] <anchor 10 1930> mark @TashkilAbove;
  pos base [\uni06AD.init_KafBaaInit \uni06B4.init_KafBaaInit \uni0763.init_KafBaaInit ] <anchor 52 2187> mark @TashkilAbove;
  pos base [\uni0776.medi_KafBaaInit \uni0776.medi_KafBaaMedi ] <anchor 110 1860> mark @TashkilAbove;
  pos base [\uni0768.medi_KafBaaInit \uni0768.medi_KafBaaMedi ] <anchor -40 2270> mark @TashkilAbove;
  pos base [\uni0775.medi_KafBaaInit \uni0775.medi_KafBaaMedi ] <anchor 50 1860> mark @TashkilAbove;
  pos base [\uni0626.medi_KafBaaInit \uni0626.medi_KafBaaMedi ] <anchor 100 1720> mark @TashkilAbove;
  pos base [\uni06BB.medi_KafBaaInit \uni0679.medi_KafBaaInit \uni06BB.medi_KafBaaMedi \uni0679.medi_KafBaaMedi ] <anchor 140 1860> mark @TashkilAbove;
  pos base [\uni067F.medi_KafBaaInit \uni067F.medi_KafBaaMedi ] <anchor 101 1820> mark @TashkilAbove;
  pos base [\uni067D.medi_KafBaaInit \uni067D.medi_KafBaaMedi ] <anchor 111 1770> mark @TashkilAbove;
  pos base [\uni067A.medi_KafBaaInit \uni067A.medi_KafBaaMedi ] <anchor 10 1710> mark @TashkilAbove;
  pos base [\uni0751.medi_KafBaaInit \uni062B.medi_KafBaaInit \uni0751.medi_KafBaaMedi \uni062B.medi_KafBaaMedi ] <anchor 42 1777> mark @TashkilAbove;
  pos base [\uni0678.medi_KafBaaInit \uni0678.medi_KafBaaMedi ] <anchor 230 1780> mark @TashkilAbove;
  pos base [\uni06B5.init_LamMemInit ] <anchor -109 2008> mark @TashkilAbove;
  pos base [\uni06B7.init_LamMemInit ] <anchor -237 2075> mark @TashkilAbove;
  pos base [\uni06B6.init_LamMemInit ] <anchor -159 1818> mark @TashkilAbove;
  pos base [\uni06B5.init_LamAlfIsol ] <anchor 111 1838> mark @TashkilAbove;
  pos base [\uni06B7.init_LamAlfIsol ] <anchor 262 2067> mark @TashkilAbove;
  pos base [\uni06B6.init_LamAlfIsol ] <anchor 340 1875> mark @TashkilAbove;
  pos base [\uni0774.fina_LamAlfIsol ] <anchor 190 1800> mark @TashkilAbove;
  pos base [\uni0773.fina_LamAlfIsol ] <anchor 129 1800> mark @TashkilAbove;
  pos base [\uni0623.fina_LamAlfIsol ] <anchor 175 2129> mark @TashkilAbove;
  pos base [\uni0675.fina_LamAlfIsol ] <anchor 545 2129> mark @TashkilAbove;
  pos base [\uni0672.fina_LamAlfIsol ] <anchor 285 2129> mark @TashkilAbove;
  pos base [\uni06B5.init_LamHaaMemInit ] <anchor 154 2038> mark @TashkilAbove;
  pos base [\uni06B7.init_LamHaaMemInit ] <anchor 100 2105> mark @TashkilAbove;
  pos base [\uni06B6.init_LamHaaMemInit ] <anchor 100 1848> mark @TashkilAbove;
  pos base [\uni0772.medi_LamHaaMemInit ] <anchor 701 1750> mark @TashkilAbove;
  pos base [\uni0626.medi_BaaBaaInit ] <anchor 200 1120> mark @TashkilAbove;
  pos base [\uni0678.medi_BaaBaaInit ] <anchor 300 1120> mark @TashkilAbove;
  pos base [\uni0691.fina_MemRaaIsol ] <anchor 588 1378> mark @TashkilAbove;
  pos base [\uni0692.fina_MemRaaIsol ] <anchor 588 428> mark @TashkilAbove;
  pos base [\uni0693.fina_MemRaaIsol \uni0694.fina_MemRaaIsol \uni0695.fina_MemRaaIsol \uni0696.fina_MemRaaIsol \uni075B.fina_MemRaaIsol \uni0631.fina_MemRaaIsol ] <anchor 520 690> mark @TashkilAbove;
  pos base [\uni0697.fina_MemRaaIsol ] <anchor 719 1062> mark @TashkilAbove;
  pos base [\uni0698.fina_MemRaaIsol ] <anchor 620 1269> mark @TashkilAbove;
  pos base [\uni0699.fina_MemRaaIsol ] <anchor 679 1312> mark @TashkilAbove;
  pos base [\uni06EF.fina_MemRaaIsol ] <anchor 628 628> mark @TashkilAbove;
  pos base [\uni0632.fina_MemRaaIsol ] <anchor 578 1012> mark @TashkilAbove;
  pos base [\uni0771.fina_MemRaaIsol ] <anchor 719 832> mark @TashkilAbove;
  pos base [\uni076B.fina_MemRaaIsol ] <anchor 588 1202> mark @TashkilAbove;
  pos base [\uni076C.fina_MemRaaIsol ] <anchor 579 1520> mark @TashkilAbove;
  pos base [\uni0642.init_FaaHaaInit ] <anchor 76 1720> mark @TashkilAbove;
  pos base [\uni06A8.init_FaaHaaInit \uni06A4.init_FaaHaaInit ] <anchor -23 1927> mark @TashkilAbove;
  pos base [\uni06A6.init_FaaHaaInit ] <anchor 36 1970> mark @TashkilAbove;
  pos base [\uni0772.init_HaaHaaInit ] <anchor 1067 1860> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamQafFina ] <anchor 274 2048> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamQafFina ] <anchor 146 2115> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamQafFina ] <anchor 224 1858> mark @TashkilAbove;
  pos base [\uni06FA.init_AboveHaa \uni0634.init_AboveHaa \uni069C.init_AboveHaa ] <anchor 759 1680> mark @TashkilAbove;
  pos base [\uni077D.init_AboveHaa ] <anchor 878 1763> mark @TashkilAbove;
  pos base [\uni0770.init_AboveHaa ] <anchor 818 2223> mark @TashkilAbove;
  pos base [\uni075C.init_AboveHaa ] <anchor 818 1723> mark @TashkilAbove;
  pos base [\uni0768.init_BaaNonIsol ] <anchor 114 1950> mark @TashkilAbove;
  pos base [\uni0626.init_BaaNonIsol ] <anchor 154 1560> mark @TashkilAbove;
  pos base [\uni0678.init_BaaNonIsol ] <anchor 354 1560> mark @TashkilAbove;
  pos base [\uni0769.init_BaaNonIsol ] <anchor 154 1750> mark @TashkilAbove;
  pos base [\uni0768.fina_BaaNonIsol ] <anchor 570 1817> mark @TashkilAbove;
  pos base [\uni063B.medi_KafMemFina \uni077F.medi_KafMemFina \uni06B1.medi_KafMemFina ] <anchor -169 1807> mark @TashkilAbove;
  pos base [\uni06AC.medi_KafMemFina \uni0762.medi_KafMemFina ] <anchor -200 1757> mark @TashkilAbove;
  pos base [\uni06AD.medi_KafMemFina \uni06B4.medi_KafMemFina \uni0763.medi_KafMemFina ] <anchor -268 2014> mark @TashkilAbove;
  pos base [\uni0776.init_BaaSenInit \uni0776.init_BaaSenInitLD ] <anchor 180 1900> mark @TashkilAbove;
  pos base [\uni0756.init_BaaSenInit \uni06CE.init_BaaSenInit \uni063D.init_BaaSenInit \uni063D.init_BaaSenInitLD \uni06CE.init_BaaSenInitLD ] <anchor 120 1850> mark @TashkilAbove;
  pos base [\uni0768.init_BaaSenInit ] <anchor 30 2310> mark @TashkilAbove;
  pos base [\uni0775.init_BaaSenInit \uni0775.init_BaaSenInitLD ] <anchor 120 1900> mark @TashkilAbove;
  pos base [\uni0626.init_BaaSenInit ] <anchor 70 1920> mark @TashkilAbove;
  pos base [\uni06BB.init_BaaSenInit \uni0679.init_BaaSenInit ] <anchor 80 1850> mark @TashkilAbove;
  pos base [\uni067F.init_BaaSenInit ] <anchor 60 1860> mark @TashkilAbove;
  pos base [\uni067D.init_BaaSenInit ] <anchor 70 1810> mark @TashkilAbove;
  pos base [\uni067A.init_BaaSenInit ] <anchor 80 1750> mark @TashkilAbove;
  pos base [\uni0751.init_BaaSenInit \uni062B.init_BaaSenInit \uni0751.init_BaaSenInitLD ] <anchor 1 1817> mark @TashkilAbove;
  pos base [\uni0678.init_BaaSenInit ] <anchor 270 1920> mark @TashkilAbove;
  pos base [\uni0769.init_BaaSenInit ] <anchor 70 2110> mark @TashkilAbove;
  pos base [\uni0770.medi_BaaSenInit ] <anchor 571 1800> mark @TashkilAbove;
  pos base [\uni0768.medi_BaaRaaFina ] <anchor 190 1910> mark @TashkilAbove;
  pos base [\uni0626.medi_BaaRaaFina ] <anchor 230 1520> mark @TashkilAbove;
  pos base [\uni0678.medi_BaaRaaFina ] <anchor 330 1520> mark @TashkilAbove;
  pos base [\uni0691.fina_BaaRaaFina \uni0692.fina_BaaRaaFina \uni0693.fina_BaaRaaFina \uni0694.fina_BaaRaaFina \uni0695.fina_BaaRaaFina \uni0696.fina_BaaRaaFina \uni0697.fina_BaaRaaFina \uni0698.fina_BaaRaaFina \uni0699.fina_BaaRaaFina \uni075B.fina_BaaRaaFina \uni06EF.fina_BaaRaaFina \uni0632.fina_BaaRaaFina \uni0631.fina_BaaRaaFina \uni076B.fina_BaaRaaFina ] <anchor 354 1640> mark @TashkilAbove;
  pos base [\uni0771.fina_BaaRaaFina ] <anchor 294 1899> mark @TashkilAbove;
  pos base [\uni076C.fina_BaaRaaFina ] <anchor 304 1459> mark @TashkilAbove;
  pos base [\uni063B.medi_KafRaaFina \uni077F.medi_KafRaaFina \uni06B1.medi_KafRaaFina ] <anchor -229 1790> mark @TashkilAbove;
  pos base [\uni06AC.medi_KafRaaFina \uni0762.medi_KafRaaFina ] <anchor -260 1740> mark @TashkilAbove;
  pos base [\uni06AD.medi_KafRaaFina \uni06B4.medi_KafRaaFina \uni0763.medi_KafRaaFina ] <anchor -328 1997> mark @TashkilAbove;
  pos base [\uni076C.fina_KafRaaFina ] <anchor 150 1020> mark @TashkilAbove;
  pos base [\uni06B5.init_LamHehInit ] <anchor -70 1990> mark @TashkilAbove;
  pos base [\uni06B7.init_LamHehInit ] <anchor -198 2057> mark @TashkilAbove;
  pos base [\uni06B6.init_LamHehInit ] <anchor -120 1800> mark @TashkilAbove;
  pos base [\uni0685.medi_MemHaaMemInit ] <anchor -78 1712> mark @TashkilAbove;
  pos base [\uni0772.medi_MemHaaMemInit ] <anchor -10 1945> mark @TashkilAbove;
  pos base [\uni0776.init_BaaMemInit \uni0776.init_BaaMemInitLD ] <anchor 192 1710> mark @TashkilAbove;
  pos base [\uni0768.init_BaaMemInit ] <anchor 42 2120> mark @TashkilAbove;
  pos base [\uni0626.init_BaaMemInit ] <anchor 82 1730> mark @TashkilAbove;
  pos base [\uni06BB.init_BaaMemInit \uni0679.init_BaaMemInit ] <anchor 92 1730> mark @TashkilAbove;
  pos base [\uni0678.init_BaaMemInit ] <anchor 282 1730> mark @TashkilAbove;
  pos base [\uni0769.init_BaaMemInit ] <anchor 82 1920> mark @TashkilAbove;
  pos base [\uni077D.init_SenHaaInit ] <anchor 852 1763> mark @TashkilAbove;
  pos base [\uni0770.init_SenHaaInit ] <anchor 792 2123> mark @TashkilAbove;
  pos base [\uni063B.init_KafRaaIsol \uni077F.init_KafRaaIsol \uni06B1.init_KafRaaIsol ] <anchor -219 1850> mark @TashkilAbove;
  pos base [\uni06AC.init_KafRaaIsol \uni0762.init_KafRaaIsol ] <anchor -250 1800> mark @TashkilAbove;
  pos base [\uni06AD.init_KafRaaIsol \uni06B4.init_KafRaaIsol \uni0763.init_KafRaaIsol ] <anchor -318 2057> mark @TashkilAbove;
  pos base [\uni076C.fina_KafRaaIsol ] <anchor 150 1070> mark @TashkilAbove;
  pos base [\uni06FC.init_AynHaaInit \uni063A.init_AynHaaInit ] <anchor 378 1695> mark @TashkilAbove;
  pos base [\uni075E.init_AynHaaInit ] <anchor 378 1945> mark @TashkilAbove;
  pos base [\uni075D.init_AynHaaInit ] <anchor 408 1745> mark @TashkilAbove;
  pos base [\uni075F.init_AynHaaInit ] <anchor 388 1885> mark @TashkilAbove;
  pos base [\uni06A0.init_AynHaaInit ] <anchor 309 1952> mark @TashkilAbove;
  pos base [\uni063B.medi_KafYaaFina \uni077F.medi_KafYaaFina \uni06B1.medi_KafYaaFina ] <anchor -154 1697> mark @TashkilAbove;
  pos base [\uni06AD.medi_KafYaaFina \uni06B4.medi_KafYaaFina \uni0763.medi_KafYaaFina ] <anchor -253 1904> mark @TashkilAbove;
  pos base [\uni0678.fina_KafYaaFina ] <anchor 641 691> mark @TashkilAbove;
  pos base [\uni0626.fina_KafYaaFina ] <anchor 247 1035> mark @TashkilAbove;
  pos base [\uni06B5.init_LamMemHaaInit ] <anchor -38 2108> mark @TashkilAbove;
  pos base [\uni06B7.init_LamMemHaaInit ] <anchor -166 2175> mark @TashkilAbove;
  pos base [\uni06B6.init_LamMemHaaInit ] <anchor -88 1918> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamAlfFina ] <anchor 175 2050> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamAlfFina ] <anchor 52 2017> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamAlfFina ] <anchor 125 1840> mark @TashkilAbove;
  pos base [\uni0774.fina_LamAlfFina ] <anchor 98 1640> mark @TashkilAbove;
  pos base [\uni0773.fina_LamAlfFina ] <anchor 97 1640> mark @TashkilAbove;
  pos base [\uni0623.fina_LamAlfFina ] <anchor 173 2020> mark @TashkilAbove;
  pos base [\uni0675.fina_LamAlfFina ] <anchor 549 2020> mark @TashkilAbove;
  pos base [\uni0672.fina_LamAlfFina ] <anchor 229 2020> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamMemMedi ] <anchor -75 1748> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamMemMedi ] <anchor -203 1815> mark @TashkilAbove;
  pos base [\uni0765.medi_LamMemMedi \uni0645.medi_LamMemMedi \uni0766.medi_LamMemMedi \uni0645.medi_KafMemMediTatweel ] <anchor 210 1640> mark @TashkilAbove;
  pos base [\uni0776.init_BaaBaaHaaInit \uni0776.init_BaaBaaHaaInitLD ] <anchor 255 1760> mark @TashkilAbove;
  pos base [\uni0768.init_BaaBaaHaaInit ] <anchor 105 2170> mark @TashkilAbove;
  pos base [\uni0775.init_BaaBaaHaaInit \uni0775.init_BaaBaaHaaInitLD ] <anchor 195 1760> mark @TashkilAbove;
  pos base [\uni0626.init_BaaBaaHaaInit ] <anchor 145 1780> mark @TashkilAbove;
  pos base [\uni06BB.init_BaaBaaHaaInit \uni0679.init_BaaBaaHaaInit ] <anchor 155 1710> mark @TashkilAbove;
  pos base [\uni067F.init_BaaBaaHaaInit ] <anchor 145 1720> mark @TashkilAbove;
  pos base [\uni0751.init_BaaBaaHaaInit \uni062B.init_BaaBaaHaaInit \uni0751.init_BaaBaaHaaInitLD ] <anchor 86 1677> mark @TashkilAbove;
  pos base [\uni0678.init_BaaBaaHaaInit ] <anchor 345 1780> mark @TashkilAbove;
  pos base [\uni0769.init_BaaBaaHaaInit ] <anchor 145 1970> mark @TashkilAbove;
  pos base [\uni0776.medi_BaaBaaHaaInit ] <anchor 586 1727> mark @TashkilAbove;
  pos base [\uni0768.medi_BaaBaaHaaInit ] <anchor 436 2137> mark @TashkilAbove;
  pos base [\uni0775.medi_BaaBaaHaaInit ] <anchor 526 1727> mark @TashkilAbove;
  pos base [\uni0626.medi_BaaBaaHaaInit \uni0678.medi_BaaBaaHaaInit ] <anchor 476 1747> mark @TashkilAbove;
  pos base [\uni06BB.medi_BaaBaaHaaInit \uni0679.medi_BaaBaaHaaInit ] <anchor 486 1677> mark @TashkilAbove;
  pos base [\uni0769.medi_BaaBaaHaaInit ] <anchor 476 1937> mark @TashkilAbove;
  pos base [\uni0776.medi_SenBaaMemInit ] <anchor -44 1750> mark @TashkilAbove;
  pos base [\uni0768.medi_SenBaaMemInit ] <anchor -194 2160> mark @TashkilAbove;
  pos base [\uni0775.medi_SenBaaMemInit ] <anchor -104 1750> mark @TashkilAbove;
  pos base [\uni0626.medi_SenBaaMemInit ] <anchor -154 1770> mark @TashkilAbove;
  pos base [\uni06BB.medi_SenBaaMemInit \uni0679.medi_SenBaaMemInit ] <anchor -144 1700> mark @TashkilAbove;
  pos base [\uni067F.medi_SenBaaMemInit ] <anchor -164 1710> mark @TashkilAbove;
  pos base [\uni0678.medi_SenBaaMemInit ] <anchor -54 1770> mark @TashkilAbove;
  pos base [\uni0769.medi_SenBaaMemInit ] <anchor -154 1960> mark @TashkilAbove;
  pos base [\uni0776.init_BaaBaaMemInit \uni0776.init_BaaBaaMemInitLD ] <anchor 247 1780> mark @TashkilAbove;
  pos base [\uni0756.init_BaaBaaMemInit \uni06CE.init_BaaBaaMemInit \uni063D.init_BaaBaaMemInit \uni063D.init_BaaBaaMemInitLD \uni06CE.init_BaaBaaMemInitLD ] <anchor 187 1730> mark @TashkilAbove;
  pos base [\uni0768.init_BaaBaaMemInit ] <anchor 97 2190> mark @TashkilAbove;
  pos base [\uni0626.init_BaaBaaMemInit ] <anchor 137 1800> mark @TashkilAbove;
  pos base [\uni06BB.init_BaaBaaMemInit \uni0679.init_BaaBaaMemInit ] <anchor 147 1730> mark @TashkilAbove;
  pos base [\uni067F.init_BaaBaaMemInit ] <anchor 127 1740> mark @TashkilAbove;
  pos base [\uni067D.init_BaaBaaMemInit ] <anchor 137 1690> mark @TashkilAbove;
  pos base [\uni0751.init_BaaBaaMemInit \uni062B.init_BaaBaaMemInit \uni0751.init_BaaBaaMemInitLD ] <anchor 68 1697> mark @TashkilAbove;
  pos base [\uni0678.init_BaaBaaMemInit ] <anchor 337 1800> mark @TashkilAbove;
  pos base [\uni0769.init_BaaBaaMemInit ] <anchor 137 1990> mark @TashkilAbove;
  pos base [\uni0768.medi_BaaBaaMemInit ] <anchor -154 2038> mark @TashkilAbove;
  pos base [\uni0626.medi_BaaBaaMemInit ] <anchor -114 1648> mark @TashkilAbove;
  pos base [\uni0678.medi_BaaBaaMemInit ] <anchor -14 1648> mark @TashkilAbove;
  pos base [\uni0769.medi_BaaBaaMemInit ] <anchor -114 1838> mark @TashkilAbove;
  pos base [\uni063B.medi_KafBaaMedi \uni077F.medi_KafBaaMedi \uni06B1.medi_KafBaaMedi ] <anchor 211 2020> mark @TashkilAbove;
  pos base [\uni06AC.medi_KafBaaMedi \uni0762.medi_KafBaaMedi ] <anchor 390 1820> mark @TashkilAbove;
  pos base [\uni06AD.medi_KafBaaMedi \uni06B4.medi_KafBaaMedi \uni0763.medi_KafBaaMedi ] <anchor 112 2227> mark @TashkilAbove;
  pos base [\uni0768.medi_BaaNonFina ] <anchor 79 1794> mark @TashkilAbove;
  pos base [\uni0626.medi_BaaNonFina ] <anchor 119 1404> mark @TashkilAbove;
  pos base [\uni0678.medi_BaaNonFina ] <anchor 219 1404> mark @TashkilAbove;
  pos base [\uni0768.fina_BaaNonFina ] <anchor 546 1764> mark @TashkilAbove;
  pos base [\uni0691.fina_HaaRaaIsol \uni0692.fina_HaaRaaIsol \uni0693.fina_HaaRaaIsol \uni0694.fina_HaaRaaIsol \uni0695.fina_HaaRaaIsol \uni0696.fina_HaaRaaIsol \uni0697.fina_HaaRaaIsol \uni0698.fina_HaaRaaIsol \uni0699.fina_HaaRaaIsol \uni075B.fina_HaaRaaIsol \uni06EF.fina_HaaRaaIsol \uni0632.fina_HaaRaaIsol \uni0631.fina_HaaRaaIsol \uni076B.fina_HaaRaaIsol ] <anchor 403 1640> mark @TashkilAbove;
  pos base [\uni0771.fina_HaaRaaIsol ] <anchor 293 1744> mark @TashkilAbove;
  pos base [\uni076C.fina_HaaRaaIsol ] <anchor 300 1304> mark @TashkilAbove;
  pos base [\uni06B5.init_LamRaaIsol ] <anchor 138 2144> mark @TashkilAbove;
  pos base [\uni06B7.init_LamRaaIsol ] <anchor 10 2211> mark @TashkilAbove;
  pos base [\uni06B6.init_LamRaaIsol ] <anchor 88 1954> mark @TashkilAbove;
  pos base [\uni0771.fina_LamRaaIsol ] <anchor 311 1906> mark @TashkilAbove;
  pos base [\uni076C.fina_LamRaaIsol ] <anchor 321 1464> mark @TashkilAbove;
  pos base [\uni069E.init_SadHaaInit ] <anchor 753 1828> mark @TashkilAbove;
  pos base [\uni0768.medi_BaaYaaFina ] <anchor 110 1710> mark @TashkilAbove;
  pos base [\uni0626.medi_BaaYaaFina ] <anchor 150 1320> mark @TashkilAbove;
  pos base [\uni0678.medi_BaaYaaFina ] <anchor 250 1320> mark @TashkilAbove;
  pos base [\uni0678.fina_BaaYaaFina ] <anchor 605 1072> mark @TashkilAbove;
  pos base [\uni0626.fina_BaaYaaFina ] <anchor 305 1072> mark @TashkilAbove;
  pos base [\uni0776.init_BaaSenAltInit ] <anchor 180 1762> mark @TashkilAbove;
  pos base [\uni0768.init_BaaSenAltInit ] <anchor 30 2171> mark @TashkilAbove;
  pos base [\uni0775.init_BaaSenAltInit ] <anchor 120 1762> mark @TashkilAbove;
  pos base [\uni0626.init_BaaSenAltInit ] <anchor 70 1782> mark @TashkilAbove;
  pos base [\uni06BB.init_BaaSenAltInit \uni0679.init_BaaSenAltInit ] <anchor 80 1712> mark @TashkilAbove;
  pos base [\uni067F.init_BaaSenAltInit ] <anchor 60 1722> mark @TashkilAbove;
  pos base [\uni0751.init_BaaSenAltInit \uni062B.init_BaaSenAltInit ] <anchor 1 1679> mark @TashkilAbove;
  pos base [\uni0678.init_BaaSenAltInit ] <anchor 270 1782> mark @TashkilAbove;
  pos base [\uni0770.medi_BaaSenAltInit ] <anchor 165 1699> mark @TashkilAbove;
  pos base [\uni0691.fina_PostTooth \uni0692.fina_PostTooth \uni0693.fina_PostTooth \uni0694.fina_PostTooth \uni0695.fina_PostTooth \uni0696.fina_PostTooth \uni0697.fina_PostTooth \uni0698.fina_PostTooth \uni0699.fina_PostTooth \uni075B.fina_PostTooth \uni06EF.fina_PostTooth \uni0632.fina_PostTooth \uni0771.fina_PostTooth \uni0631.fina_PostTooth \uni076B.fina_PostTooth ] <anchor 366 1640> mark @TashkilAbove;
  pos base [\uni076C.fina_PostTooth ] <anchor 316 1216> mark @TashkilAbove;
  pos base [\uni0678.fina_PostTooth ] <anchor 605 1070> mark @TashkilAbove;
  pos base [\uni0626.fina_PostTooth ] <anchor 305 1070> mark @TashkilAbove;
  pos base [\uni0776.init_AboveHaa ] <anchor 729 1927> mark @TashkilAbove;
  pos base [\uni0756.init_AboveHaa \uni06CE.init_AboveHaa \uni063D.init_AboveHaa ] <anchor 669 1877> mark @TashkilAbove;
  pos base [\uni0768.init_AboveHaa ] <anchor 579 2337> mark @TashkilAbove;
  pos base [\uni0775.init_AboveHaa ] <anchor 669 1927> mark @TashkilAbove;
  pos base [\uni0626.init_AboveHaa ] <anchor 619 1947> mark @TashkilAbove;
  pos base [\uni06BB.init_AboveHaa \uni0679.init_AboveHaa ] <anchor 629 1877> mark @TashkilAbove;
  pos base [\uni067F.init_AboveHaa ] <anchor 609 1887> mark @TashkilAbove;
  pos base [\uni067D.init_AboveHaa ] <anchor 619 1837> mark @TashkilAbove;
  pos base [\uni067A.init_AboveHaa ] <anchor 629 1777> mark @TashkilAbove;
  pos base [\uni0751.init_AboveHaa \uni062B.init_AboveHaa ] <anchor 550 1844> mark @TashkilAbove;
  pos base [\uni0678.init_AboveHaa ] <anchor 819 1947> mark @TashkilAbove;
  pos base [\uni0769.init_AboveHaa ] <anchor 619 2137> mark @TashkilAbove;
  pos base [\uni0776.init_BaaHaaInit \uni0776.init_BaaHaaInitLD ] <anchor 770 1900> mark @TashkilAbove;
  pos base [\uni0756.init_BaaHaaInit \uni06CE.init_BaaHaaInit \uni063D.init_BaaHaaInit \uni063D.init_BaaHaaInitLD \uni06CE.init_BaaHaaInitLD ] <anchor 710 1850> mark @TashkilAbove;
  pos base [\uni0768.init_BaaHaaInit ] <anchor 620 2310> mark @TashkilAbove;
  pos base [\uni0775.init_BaaHaaInit \uni0775.init_BaaHaaInitLD ] <anchor 710 1900> mark @TashkilAbove;
  pos base [\uni0626.init_BaaHaaInit ] <anchor 660 1920> mark @TashkilAbove;
  pos base [\uni06BB.init_BaaHaaInit \uni0679.init_BaaHaaInit ] <anchor 670 1850> mark @TashkilAbove;
  pos base [\uni067F.init_BaaHaaInit ] <anchor 650 1860> mark @TashkilAbove;
  pos base [\uni067D.init_BaaHaaInit ] <anchor 660 1810> mark @TashkilAbove;
  pos base [\uni067A.init_BaaHaaInit ] <anchor 670 1750> mark @TashkilAbove;
  pos base [\uni0751.init_BaaHaaInit \uni062B.init_BaaHaaInit \uni0751.init_BaaHaaInitLD ] <anchor 591 1817> mark @TashkilAbove;
  pos base [\uni0678.init_BaaHaaInit ] <anchor 860 1920> mark @TashkilAbove;
  pos base [\uni0769.init_BaaHaaInit ] <anchor 660 2110> mark @TashkilAbove;
  pos base [\uni0776.init_BaaHaaMemInit \uni0776.init_BaaHaaMemInitLD ] <anchor 324 2036> mark @TashkilAbove;
  pos base [\uni06BC.init_BaaHaaMemInit \uni0646.init_BaaHaaMemInit \uni06B9.init_BaaHaaMemInit \uni0754.init_BaaHaaMemInit \uni06BA.init_BaaHaaMemInit \uni0767.init_BaaHaaMemInit \uni0767.init_BaaHaaMemInitLD \uni0754.init_BaaHaaMemInitLD \uni06B9.init_BaaHaaMemInitLD ] <anchor 214 1696> mark @TashkilAbove;
  pos base [\uni0756.init_BaaHaaMemInit \uni06CE.init_BaaHaaMemInit \uni063D.init_BaaHaaMemInit ] <anchor 266 1878> mark @TashkilAbove;
  pos base [\uni0768.init_BaaHaaMemInit ] <anchor 174 2446> mark @TashkilAbove;
  pos base [\uni0775.init_BaaHaaMemInit \uni0775.init_BaaHaaMemInitLD ] <anchor 264 2036> mark @TashkilAbove;
  pos base [\uni0626.init_BaaHaaMemInit ] <anchor 230 1940> mark @TashkilAbove;
  pos base [\uni06BB.init_BaaHaaMemInit \uni0679.init_BaaHaaMemInit ] <anchor 224 1986> mark @TashkilAbove;
  pos base [\uni067F.init_BaaHaaMemInit ] <anchor 204 1996> mark @TashkilAbove;
  pos base [\uni067D.init_BaaHaaMemInit ] <anchor 214 1946> mark @TashkilAbove;
  pos base [\uni067A.init_BaaHaaMemInit ] <anchor 224 1886> mark @TashkilAbove;
  pos base [\uni0751.init_BaaHaaMemInit \uni062B.init_BaaHaaMemInit \uni0751.init_BaaHaaMemInitLD ] <anchor 145 1953> mark @TashkilAbove;
  pos base [\uni0753.init_BaaHaaMemInit \uni062A.init_BaaHaaMemInit \uni067C.init_BaaHaaMemInit \uni0753.init_BaaHaaMemInitLD ] <anchor 244 1746> mark @TashkilAbove;
  pos base [\uni0678.init_BaaHaaMemInit ] <anchor 588 1940> mark @TashkilAbove;
  pos base [\uni0769.init_BaaHaaMemInit ] <anchor 214 2246> mark @TashkilAbove;
  pos base [\uni0685.medi_BaaHaaMemInit ] <anchor -182 1702> mark @TashkilAbove;
  pos base [\uni0772.medi_BaaHaaMemInit ] <anchor -114 1935> mark @TashkilAbove;
  pos base [\uni0772.fina_AboveHaaIsol ] <anchor 287 1760> mark @TashkilAbove;
  pos base [\uni06B5.init_LamHaaHaaInit ] <anchor 267 2125> mark @TashkilAbove;
  pos base [\uni06B7.init_LamHaaHaaInit ] <anchor 139 2192> mark @TashkilAbove;
  pos base [\uni06B6.init_LamHaaHaaInit ] <anchor 217 1935> mark @TashkilAbove;
  pos base [\uni0770.init_PreYaa ] <anchor 161 1830> mark @TashkilAbove;
  pos base [\uni0770.medi_PreYaa ] <anchor 166 1737> mark @TashkilAbove;
  pos base [\uni0768.init_High ] <anchor 5 1810> mark @TashkilAbove;
  pos base [\uni0626.init_High ] <anchor 95 1320> mark @TashkilAbove;
  pos base [\uni0678.init_High ] <anchor 295 1320> mark @TashkilAbove;
  pos base [\uni0768.medi_High ] <anchor 105 2034> mark @TashkilAbove;
  pos base [\uni0626.medi_High ] <anchor 145 1644> mark @TashkilAbove;
  pos base [\uni0678.medi_High ] <anchor 245 1644> mark @TashkilAbove;
  pos base [\uni0769.medi_High ] <anchor 145 1834> mark @TashkilAbove;
  pos base [\uni0770.fina_BaaSen ] <anchor 1534 1712> mark @TashkilAbove;
  pos base [\uni0768.init_Wide ] <anchor 145 1935> mark @TashkilAbove;
  pos base [\uni0626.init_Wide ] <anchor 185 1640> mark @TashkilAbove;
  pos base [\uni0678.init_Wide ] <anchor 385 1640> mark @TashkilAbove;
  pos base [\uni0769.init_Wide ] <anchor 185 1735> mark @TashkilAbove;
  pos base [\uni0772.medi_HaaHaaInit ] <anchor 42 1860> mark @TashkilAbove;
  pos base [\uni0685.init_AboveHaa ] <anchor 1159 1756> mark @TashkilAbove;
  pos base [\uni0682.init_AboveHaa ] <anchor 1227 1689> mark @TashkilAbove;
  pos base [\uni0772.init_AboveHaa ] <anchor 1227 1989> mark @TashkilAbove;
  pos base [\uni075E.init_AboveHaa ] <anchor 378 1895> mark @TashkilAbove;
  pos base [\uni075F.init_AboveHaa ] <anchor 388 1835> mark @TashkilAbove;
  pos base [\uni06A0.init_AboveHaa ] <anchor 309 1902> mark @TashkilAbove;
  pos base [\uni063B.init_AboveHaa \uni077F.init_AboveHaa \uni06B1.init_AboveHaa ] <anchor -51 1853> mark @TashkilAbove;
  pos base [\uni06AC.init_AboveHaa \uni0762.init_AboveHaa ] <anchor -82 1803> mark @TashkilAbove;
  pos base [\uni06AD.init_AboveHaa \uni06B4.init_AboveHaa \uni0763.init_AboveHaa ] <anchor -150 2060> mark @TashkilAbove;
  pos base [\uni06AD.init_KafLam \uni06B4.init_KafLam \uni0763.init_KafLam ] <anchor -85 1798> mark @TashkilAbove;
  pos base [\uni063B.fina_KafKafFina \uni077F.fina_KafKafFina \uni06B1.fina_KafKafFina ] <anchor 812 1709> mark @TashkilAbove;
  pos base [\uni06AD.fina_KafKafFina \uni06B4.fina_KafKafFina \uni0763.fina_KafKafFina ] <anchor 713 1916> mark @TashkilAbove;
  pos base [\uni06B5.medi_KafLam ] <anchor 110 2000> mark @TashkilAbove;
  pos base [\uni06B7.medi_KafLam ] <anchor -18 2067> mark @TashkilAbove;
  pos base [\uni06B6.medi_KafLam ] <anchor 60 1810> mark @TashkilAbove;
  pos base [\uni06B5.medi_KafLamMemMedi \uni06B5.medi_LamLamMemInit \uni06B5.medi_LamLamMemMedi ] <anchor 12 1778> mark @TashkilAbove;
  pos base [\uni06B7.medi_KafLamMemMedi \uni06B7.medi_LamLamMemInit \uni06B7.medi_LamLamMemMedi ] <anchor -116 1845> mark @TashkilAbove;
  pos base [\uni06AD.medi_KafLam \uni06B4.medi_KafLam \uni0763.medi_KafLam ] <anchor -55 1867> mark @TashkilAbove;
  pos base [\uni06B5.medi_KafLamHehIsol \uni06B5.medi_LamLamHehIsol \uni06B5.medi_LamLamHehFina ] <anchor 11 1918> mark @TashkilAbove;
  pos base [\uni06B7.medi_KafLamHehIsol \uni06B7.medi_LamLamHehIsol \uni06B7.medi_LamLamHehFina ] <anchor -117 1985> mark @TashkilAbove;
  pos base [\uni06B6.medi_KafLamHehIsol \uni06B6.medi_LamLamHehIsol \uni06B6.medi_LamLamHehFina ] <anchor -39 1728> mark @TashkilAbove;
  pos base [\uni06B5.medi_KafLamYaa ] <anchor 126 2096> mark @TashkilAbove;
  pos base [\uni06B7.medi_KafLamYaa ] <anchor -2 2163> mark @TashkilAbove;
  pos base [\uni06B6.medi_KafLamYaa ] <anchor 76 1906> mark @TashkilAbove;
  pos base [\uni06B5.medi_KafLamAlf ] <anchor -57 1831> mark @TashkilAbove;
  pos base [\uni06B7.medi_KafLamAlf ] <anchor 101 2084> mark @TashkilAbove;
  pos base [\uni06B6.medi_KafLamAlf ] <anchor 174 1907> mark @TashkilAbove;
  pos base [\uni0774.fina_KafAlf ] <anchor 246 1819> mark @TashkilAbove;
  pos base [\uni0773.fina_KafAlf ] <anchor 186 1819> mark @TashkilAbove;
  pos base [\uni0623.fina_KafAlf ] <anchor 228 2150> mark @TashkilAbove;
  pos base [\uni0675.fina_KafAlf ] <anchor 476 2089> mark @TashkilAbove;
  pos base [\uni06AD.init_KafMemAlf \uni06B4.init_KafMemAlf \uni0763.init_KafMemAlf \uni06AD.medi_KafMemAlf \uni06B4.medi_KafMemAlf \uni0763.medi_KafMemAlf ] <anchor -106 1808> mark @TashkilAbove;
  pos base [\uni06B5.medi_KafMemLam ] <anchor 120 2100> mark @TashkilAbove;
  pos base [\uni06B7.medi_KafMemLam ] <anchor -8 2167> mark @TashkilAbove;
  pos base [\uni06B6.medi_KafMemLam ] <anchor 70 1910> mark @TashkilAbove;
  pos base [\uni06B5.fina_KafMemLam ] <anchor 630 1729> mark @TashkilAbove;
  pos base [\uni0774.fina_KafMemAlf ] <anchor 247 1938> mark @TashkilAbove;
  pos base [\uni0773.fina_KafMemAlf ] <anchor 186 1838> mark @TashkilAbove;
  pos base [\uni0623.fina_KafMemAlf ] <anchor 260 2150> mark @TashkilAbove;
  pos base [\uni0675.fina_KafMemAlf ] <anchor 470 2108> mark @TashkilAbove;
  pos base [\uni0672.fina_KafMemAlf ] <anchor 70 2150> mark @TashkilAbove;
  pos base [\uni063B.init_KafHeh \uni077F.init_KafHeh \uni06B1.init_KafHeh ] <anchor -49 1824> mark @TashkilAbove;
  pos base [\uni06AC.init_KafHeh \uni0762.init_KafHeh ] <anchor -80 1774> mark @TashkilAbove;
  pos base [\uni06AD.init_KafHeh \uni06B4.init_KafHeh \uni0763.init_KafHeh ] <anchor -148 2031> mark @TashkilAbove;
  pos base [\uni063B.medi_KafHeh \uni077F.medi_KafHeh \uni06B1.medi_KafHeh ] <anchor -146 1696> mark @TashkilAbove;
  pos base [\uni06AD.medi_KafHeh \uni06B4.medi_KafHeh \uni0763.medi_KafHeh ] <anchor -245 1903> mark @TashkilAbove;
  pos base [\uni06B5.init_LamHeh ] <anchor -75 1974> mark @TashkilAbove;
  pos base [\uni06B7.init_LamHeh ] <anchor -203 2041> mark @TashkilAbove;
  pos base [\uni06B6.init_LamHeh ] <anchor -125 1784> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamHeh ] <anchor -7 2000> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamHeh ] <anchor -135 2067> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamHeh ] <anchor -57 1810> mark @TashkilAbove;
  pos base [\uni0688.fina_LamDal \uni0759.fina_LamDal \uni068B.fina_LamDal ] <anchor 452 1674> mark @TashkilAbove;
  pos base [\uni063B.medi_KafMemMedi \uni077F.medi_KafMemMedi \uni06B1.medi_KafMemMedi ] <anchor -240 1833> mark @TashkilAbove;
  pos base [\uni06AD.medi_KafMemMedi \uni06B4.medi_KafMemMedi \uni0763.medi_KafMemMedi ] <anchor -339 2040> mark @TashkilAbove;
  pos base [\uni063B.init_KafMemInit \uni077F.init_KafMemInit \uni06B1.init_KafMemInit ] <anchor -211 1780> mark @TashkilAbove;
  pos base [\uni06AD.init_KafMemInit \uni06B4.init_KafMemInit \uni0763.init_KafMemInit ] <anchor -310 1987> mark @TashkilAbove;
  pos base [\uni075E.init_AynMemInit ] <anchor 290 1881> mark @TashkilAbove;
  pos base [\uni075D.init_AynMemInit ] <anchor 320 1681> mark @TashkilAbove;
  pos base [\uni075F.init_AynMemInit ] <anchor 300 1821> mark @TashkilAbove;
  pos base [\uni06A0.init_AynMemInit ] <anchor 221 1888> mark @TashkilAbove;
  pos base [\uni06A8.init_FaaMemInit \uni06A4.init_FaaMemInit ] <anchor -114 1865> mark @TashkilAbove;
  pos base [\uni06A6.init_FaaMemInit ] <anchor -55 1908> mark @TashkilAbove;
  pos base [\uni0772.init_HaaMemInit ] <anchor 752 1803> mark @TashkilAbove;
  pos base [\uni0770.init_SenMemInit ] <anchor 460 2064> mark @TashkilAbove;
  pos base [\uni069E.init_SadMemInit ] <anchor 396 1851> mark @TashkilAbove;
  pos base [\uni063B.init_KafYaaIsol \uni077F.init_KafYaaIsol ] <anchor -261 1832> mark @TashkilAbove;
  pos base [\uni06AD.init_KafYaaIsol \uni0763.init_KafYaaIsol ] <anchor -360 2039> mark @TashkilAbove;
  pos base [\uni06B4.init_KafYaaIsol ] <anchor -440 2039> mark @TashkilAbove;
  pos base [\uni06B1.init_KafYaaIsol ] <anchor -341 1832> mark @TashkilAbove;
  pos base [\uni0776.init_BaaYaaIsol \uni0776.init_BaaYaaIsolLD ] <anchor 183 1800> mark @TashkilAbove;
  pos base [\uni0756.init_BaaYaaIsol \uni06CE.init_BaaYaaIsol \uni063D.init_BaaYaaIsol \uni063D.init_BaaYaaIsolLD \uni06CE.init_BaaYaaIsolLD ] <anchor 123 1750> mark @TashkilAbove;
  pos base [\uni0768.init_BaaYaaIsol ] <anchor 33 2210> mark @TashkilAbove;
  pos base [\uni0775.init_BaaYaaIsol \uni0775.init_BaaYaaIsolLD ] <anchor 123 1800> mark @TashkilAbove;
  pos base [\uni0626.init_BaaYaaIsol ] <anchor 73 1820> mark @TashkilAbove;
  pos base [\uni06BB.init_BaaYaaIsol \uni0679.init_BaaYaaIsol ] <anchor 83 1750> mark @TashkilAbove;
  pos base [\uni067F.init_BaaYaaIsol ] <anchor 24 1760> mark @TashkilAbove;
  pos base [\uni067D.init_BaaYaaIsol ] <anchor 34 1710> mark @TashkilAbove;
  pos base [\uni0751.init_BaaYaaIsol \uni062B.init_BaaYaaIsol \uni0751.init_BaaYaaIsolLD ] <anchor -35 1717> mark @TashkilAbove;
  pos base [\uni0678.init_BaaYaaIsol ] <anchor 250 1820> mark @TashkilAbove;
  pos base [\uni0769.init_BaaYaaIsol ] <anchor 73 2010> mark @TashkilAbove;
  pos base [\uni06A8.init_FaaYaaIsol \uni06A4.init_FaaYaaIsol ] <anchor -120 1847> mark @TashkilAbove;
  pos base [\uni06A6.init_FaaYaaIsol ] <anchor -61 1890> mark @TashkilAbove;
  pos base [\uni075E.init_AynYaaIsol ] <anchor 475 1805> mark @TashkilAbove;
  pos base [\uni075F.init_AynYaaIsol ] <anchor 486 1745> mark @TashkilAbove;
  pos base [\uni06A0.init_AynYaaIsol ] <anchor 406 1812> mark @TashkilAbove;
  pos base [\uni06B5.init_LamYaaIsol ] <anchor -13 2018> mark @TashkilAbove;
  pos base [\uni06B7.init_LamYaaIsol ] <anchor -141 2085> mark @TashkilAbove;
  pos base [\uni06B6.init_LamYaaIsol ] <anchor -63 1828> mark @TashkilAbove;
  pos base [\uni0678.fina_KafYaaIsol ] <anchor 658 845> mark @TashkilAbove;
  pos base [\uni0626.fina_KafYaaIsol ] <anchor 220 1250> mark @TashkilAbove;
  pos base [\uni063B.init_KafMemIsol \uni077F.init_KafMemIsol \uni06B1.init_KafMemIsol ] <anchor -267 1934> mark @TashkilAbove;
  pos base [\uni06AC.init_KafMemIsol \uni0762.init_KafMemIsol ] <anchor -298 1884> mark @TashkilAbove;
  pos base [\uni06AD.init_KafMemIsol \uni06B4.init_KafMemIsol \uni0763.init_KafMemIsol ] <anchor -366 2141> mark @TashkilAbove;
  pos base [\uni06B5.init_LamMemIsol ] <anchor 30 2144> mark @TashkilAbove;
  pos base [\uni06B7.init_LamMemIsol ] <anchor -98 2211> mark @TashkilAbove;
  pos base [\uni06B6.init_LamMemIsol ] <anchor -20 1954> mark @TashkilAbove;
  pos base [\uni0768.init_BaaMemIsol ] <anchor 356 2041> mark @TashkilAbove;
  pos base [\uni0626.init_BaaMemIsol ] <anchor 396 1651> mark @TashkilAbove;
  pos base [\uni0678.init_BaaMemIsol ] <anchor 596 1651> mark @TashkilAbove;
  pos base [\uni0769.init_BaaMemIsol ] <anchor 396 1841> mark @TashkilAbove;
  pos base [\uni0768.medi_BaaMemAlfFina ] <anchor 111 1710> mark @TashkilAbove;
  pos base [\uni0626.medi_BaaMemAlfFina ] <anchor 151 1320> mark @TashkilAbove;
  pos base [\uni0678.medi_BaaMemAlfFina ] <anchor 251 1320> mark @TashkilAbove;
  pos base [\uni0774.fina_MemAlfFina ] <anchor 247 1850> mark @TashkilAbove;
  pos base [\uni0773.fina_MemAlfFina ] <anchor 186 1770> mark @TashkilAbove;
  pos base [\uni0623.fina_MemAlfFina ] <anchor 258 2150> mark @TashkilAbove;
  pos base [\uni0675.fina_MemAlfFina ] <anchor 450 2020> mark @TashkilAbove;
  pos base [\uni0672.fina_MemAlfFina ] <anchor 50 2150> mark @TashkilAbove;
  pos base [\uni0768.init_BaaHehInit ] <anchor 373 1810> mark @TashkilAbove;
  pos base [\uni0626.init_BaaHehInit ] <anchor 413 1420> mark @TashkilAbove;
  pos base [\uni0678.init_BaaHehInit ] <anchor 613 1420> mark @TashkilAbove;
  pos base [\uni0768.medi_BaaHehMedi ] <anchor 265 1810> mark @TashkilAbove;
  pos base [\uni0626.medi_BaaHehMedi ] <anchor 305 1420> mark @TashkilAbove;
  pos base [\uni0678.medi_BaaHehMedi ] <anchor 405 1420> mark @TashkilAbove;
  pos base [\uni06B7.medi_KafLamMemFina ] <anchor 78 2163> mark @TashkilAbove;
  pos base [\uni06B6.medi_KafLamMemFina ] <anchor 156 1906> mark @TashkilAbove;
  pos base [\uni06B5.init_LamLamInit ] <anchor 75 1911> mark @TashkilAbove;
  pos base [\uni06B7.init_LamLamInit ] <anchor -53 1978> mark @TashkilAbove;
  pos base [\uni06B6.init_LamLamInit ] <anchor 25 1721> mark @TashkilAbove;
  pos base [\uni076A.init_LamLamInit ] <anchor 245 1640> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamLamInit \uni06B5.medi_LamLamMedi2 ] <anchor 134 1912> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamLamInit \uni06B7.medi_LamLamMedi2 ] <anchor 6 1979> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamLamInit \uni06B6.medi_LamLamMedi2 ] <anchor 84 1722> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamLamAlfIsol \uni06B5.medi_LamLamAlefFina ] <anchor 112 2000> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamLamAlfIsol \uni06B7.medi_LamLamAlefFina ] <anchor -11 1973> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamLamAlfIsol \uni06B6.medi_LamLamAlefFina ] <anchor 62 1796> mark @TashkilAbove;
  pos base [\uni06AD.fina_LamKafIsol \uni06B4.fina_LamKafIsol \uni0763.fina_LamKafIsol ] <anchor 649 1818> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamLamMedi ] <anchor 80 1944> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamLamMedi ] <anchor -48 2011> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamLamMedi ] <anchor 30 1754> mark @TashkilAbove;
  pos base [\uni06AD.fina_LamKafFina \uni06B4.fina_LamKafFina \uni0763.fina_LamKafFina ] <anchor 725 1820> mark @TashkilAbove;
  pos base [\uni06B5.medi_LamLamYaaIsol \uni06B5.medi_LamLamYaaFina ] <anchor 136 1919> mark @TashkilAbove;
  pos base [\uni06B7.medi_LamLamYaaIsol \uni06B7.medi_LamLamYaaFina ] <anchor 8 1986> mark @TashkilAbove;
  pos base [\uni06B6.medi_LamLamYaaIsol \uni06B6.medi_LamLamYaaFina ] <anchor 86 1729> mark @TashkilAbove;
  pos base [\uni0772.medi_1LamHaaHaaInit ] <anchor 971 1860> mark @TashkilAbove;
  pos base [\uni0772.medi_2LamHaaHaaInit ] <anchor 40 1860> mark @TashkilAbove;
  pos base [\uni0680.init_LD \uni06BD.init_LD \uni067E.init_LD \uni067B.init_LD \uni0628.init_LD \uni0767.init_LD \uni063D.init_LD \uni0777.init_LD \uni0776.init_LD \uni0775.init_LD \uni06CC.init_LD \uni064A.init_LD \uni06CE.init_LD \uni0751.init_LD \uni0750.init_LD \uni0753.init_LD \uni0752.init_LD \uni0755.init_LD \uni0754.init_LD \uni06B9.init_LD \uni06D1.init_LD \uni06D0.init_LD \uni0620.initLD ] <anchor 190 1640> mark @TashkilAbove;
  pos base [\uni0776.init_BaaSenAltInitLD ] <anchor 203 1748> mark @TashkilAbove;
  pos base [\uni0775.init_BaaSenAltInitLD ] <anchor 143 1748> mark @TashkilAbove;
  pos base [\uni0670.isol ] <anchor 143 1550> mark @TashkilAbove;
  pos base [\uni0670.medi ] <anchor 200 1400> mark @TashkilAbove;
  pos base [\uni0621.medi ] <anchor 260 1640> mark @TashkilAbove;
  pos base [\uni0621.float ] <anchor 2 1640> mark @TashkilAbove;
  pos base [\uni06E5 ] <anchor 400 855> mark @TashkilAbove;
  pos base [\uni06E6 ] <anchor 419 855> mark @TashkilAbove;
  pos base [\uni06E5.medi \uni06E6.medi ] <anchor 300 1400> mark @TashkilAbove;
  pos base [\uni0645.fina_KafMemFinaExtended ] <anchor 490 1640> mark @TashkilAbove;
  pos base [\uni0770.init_SenBaaMemInit ] <anchor 590 1900> mark @TashkilAbove;
  pos base [\aSad.init_YaaBarree \uni069D.init_YaaBarree \uni06FB.init_YaaBarree \uni0636.init_YaaBarree \uni0635.init_YaaBarree ] <anchor 348 1640> mark @TashkilAbove;
  pos base [\uni0770.init_YaaBarree ] <anchor 331 2015> mark @TashkilAbove;
  pos base [\uni069E.init_YaaBarree ] <anchor 229 1702> mark @TashkilAbove;
  pos base [\uni06E5.low ] <anchor 308 325> mark @TashkilAbove;
  pos base [\aLam.init_YaaBarree \uni06B5.init_YaaBarree \uni0644.init_YaaBarree \uni06B8.init_YaaBarree \uni076A.init_YaaBarree ] <anchor 150 1735> mark @TashkilAbove;
  pos base [\aKaf.init_YaaBarree \uni063B.init_YaaBarree \uni063C.init_YaaBarree \uni077F.init_YaaBarree \uni0764.init_YaaBarree \uni0643.init_YaaBarree \uni06AB.init_YaaBarree \uni06AC.init_YaaBarree \uni06AD.init_YaaBarree \uni06AE.init_YaaBarree \uni06A9.init_YaaBarree \uni0763.init_YaaBarree \uni0762.init_YaaBarree ] <anchor 130 1985> mark @TashkilAbove;
  pos base [\uni06B0.init_YaaBarree \uni06B3.init_YaaBarree \uni06B2.init_YaaBarree \uni06AF.init_YaaBarree \uni06B4.init_YaaBarree \uni06B1.init_YaaBarree ] <anchor 130 2200> mark @TashkilAbove;
  pos base [\uni0776.init_YaaBarree ] <anchor -237 2024> mark @TashkilAbove;
  pos base [\uni06BC.init_YaaBarree \uni0646.init_YaaBarree \uni06B9.init_YaaBarree \uni0769.init_YaaBarree \uni0754.init_YaaBarree \uni06BA.init_YaaBarree \uni0767.init_YaaBarree ] <anchor -347 1684> mark @TashkilAbove;
  pos base [\uni0768.init_YaaBarree ] <anchor -387 2434> mark @TashkilAbove;
  pos base [\uni0775.init_YaaBarree ] <anchor -297 2024> mark @TashkilAbove;
  pos base [\uni0626.init_YaaBarree ] <anchor -347 874> mark @TashkilAbove;
  pos base [\uni06BB.init_YaaBarree \uni0679.init_YaaBarree ] <anchor -337 1974> mark @TashkilAbove;
  pos base [\uni067F.init_YaaBarree ] <anchor -357 1984> mark @TashkilAbove;
  pos base [\uni067D.init_YaaBarree ] <anchor -347 1934> mark @TashkilAbove;
  pos base [\uni067A.init_YaaBarree ] <anchor -337 1874> mark @TashkilAbove;
  pos base [\uni0751.init_YaaBarree \uni062B.init_YaaBarree ] <anchor -416 1941> mark @TashkilAbove;
  pos base [\uni0753.init_YaaBarree \uni062A.init_YaaBarree \uni067C.init_YaaBarree ] <anchor -317 1734> mark @TashkilAbove;
  pos base [\uni0678.init_YaaBarree ] <anchor -147 874> mark @TashkilAbove;
  pos base [\uni06B7.init_YaaBarree ] <anchor -241 2180> mark @TashkilAbove;
  pos base [\uni06B6.init_YaaBarree ] <anchor -163 1923> mark @TashkilAbove;
  pos base [\aHaa.init_YaaBarree \uni062E.init_YaaBarree \uni062D.init_YaaBarree \uni0681.init_YaaBarree \uni0687.init_YaaBarree \uni0685.init_YaaBarree \uni062C.init_YaaBarree \uni0682.init_YaaBarree \uni0757.init_YaaBarree \uni0684.init_YaaBarree \uni076F.init_YaaBarree \uni076E.init_YaaBarree \uni0683.init_YaaBarree \uni06BF.init_YaaBarree \uni077C.init_YaaBarree \uni0758.init_YaaBarree \uni0686.init_YaaBarree ] <anchor 641 1640> mark @TashkilAbove;
  pos base [\uni0772.init_YaaBarree ] <anchor 1077 1739> mark @TashkilAbove;
  pos base [\uni075E.init_YaaBarree ] <anchor 415 1875> mark @TashkilAbove;
  pos base [\uni075F.init_YaaBarree ] <anchor 426 1815> mark @TashkilAbove;
  pos base [\uni06A0.init_YaaBarree ] <anchor 346 1882> mark @TashkilAbove;
  pos base [\uni069F.init_YaaBarree ] <anchor 700 1680> mark @TashkilAbove;
  pos base [\uni06BE.fina \uni06FF.fina \aHehKnotted.fina ] <anchor 670 1640> mark @TashkilAbove;
  pos base [\aHeh.medi_HehYaaFina \uni0647.medi_HehYaaFina \uni06C1.medi_HehYaaFina ] <anchor 770 1640> mark @TashkilAbove;
  pos base [\uni0647.medi_PostToothHehYaa \uni06C1.medi_PostToothHehYaa ] <anchor 884 1640> mark @TashkilAbove;
  pos base [\aBaa.init_BaaBaaHeh \uni0777.init_BaaBaaHeh \uni0680.init_BaaBaaHeh \uni0776.init_BaaBaaHeh \uni06BC.init_BaaBaaHeh \uni0750.init_BaaBaaHeh \uni0756.init_BaaBaaHeh \uni0768.init_BaaBaaHeh \uni06CE.init_BaaBaaHeh \uni0775.init_BaaBaaHeh \uni06BD.init_BaaBaaHeh \uni0626.init_BaaBaaHeh \uni066E.init_BaaBaaHeh \uni064A.init_BaaBaaHeh \uni06BB.init_BaaBaaHeh \uni067F.init_BaaBaaHeh \uni0755.init_BaaBaaHeh \uni067D.init_BaaBaaHeh \uni067E.init_BaaBaaHeh \uni067B.init_BaaBaaHeh \uni0628.init_BaaBaaHeh \uni067A.init_BaaBaaHeh \uni0751.init_BaaBaaHeh \uni0646.init_BaaBaaHeh \uni0753.init_BaaBaaHeh \uni0752.init_BaaBaaHeh \uni062A.init_BaaBaaHeh \uni063D.init_BaaBaaHeh \uni062B.init_BaaBaaHeh \uni0679.init_BaaBaaHeh \uni06B9.init_BaaBaaHeh \uni0769.init_BaaBaaHeh \uni0649.init_BaaBaaHeh \uni067C.init_BaaBaaHeh \uni0754.init_BaaBaaHeh \uni06D1.init_BaaBaaHeh \uni06D0.init_BaaBaaHeh \uni06BA.init_BaaBaaHeh \uni06CC.init_BaaBaaHeh \uni0767.init_BaaBaaHeh \uni0680.init_BaaBaaHehLD \uni06BD.init_BaaBaaHehLD \uni067E.init_BaaBaaHehLD \uni067B.init_BaaBaaHehLD \uni0628.init_BaaBaaHehLD \uni0767.init_BaaBaaHehLD \uni063D.init_BaaBaaHehLD \uni0777.init_BaaBaaHehLD \uni0776.init_BaaBaaHehLD \uni0775.init_BaaBaaHehLD \uni06CC.init_BaaBaaHehLD \uni064A.init_BaaBaaHehLD \uni06CE.init_BaaBaaHehLD \uni0751.init_BaaBaaHehLD \uni0750.init_BaaBaaHehLD \uni0753.init_BaaBaaHehLD \uni0752.init_BaaBaaHehLD \uni0755.init_BaaBaaHehLD \uni0754.init_BaaBaaHehLD \uni06B9.init_BaaBaaHehLD \uni06D1.init_BaaBaaHehLD \uni06D0.init_BaaBaaHehLD ] <anchor 179 1640> mark @TashkilAbove;
  pos base [\uni0678.init_BaaBaaHeh ] <anchor 529 2060> mark @TashkilAbove;
} markTashkilAboveBase;

lookup markTashkilBelowBase {
  lookupflag 0;
  markClass [\uni0655 \uni065F ] <anchor 0 -500> @TashkilBelow;
  markClass [\uni064D ] <anchor 499 -670> @TashkilBelow;
  markClass [\uni0650 \uni061A \uni06ED ] <anchor 534 -670> @TashkilBelow;
  markClass [\uni0656 ] <anchor 319 -670> @TashkilBelow;
  markClass [\uni0650.small2 ] <anchor 0 -555> @TashkilBelow;
  markClass [\uni08F2 ] <anchor 670 -569> @TashkilBelow;
  markClass [\uni065C ] <anchor 194 -670> @TashkilBelow;
  markClass [\uni0325 ] <anchor 256 -620> @TashkilBelow;
  pos base [\aAlf.fina.alt \uni0774.fina_MemAlfFina \uni0623.fina_MemAlfFina \uni0627.fina_Tatweel ] <anchor 477 -670> mark @TashkilBelow;
  pos base [\aAlf.fina \aAlf.isol \uni0627.fina \uni0675.fina \uni0672.fina \uni0645.medi_LamMemInitTatweel \aAlf.fina_Narrow \uni0627.fina_Narrow \uni0672.fina_Narrow \uni0675.fina_Narrow ] <anchor 250 -670> mark @TashkilBelow;
  pos base [\aAyn.fina \uni063A.fina \uni075E.fina \uni075D.fina \uni075F.fina \uni06A0.fina \uni0639.fina ] <anchor 700 -50> mark @TashkilBelow;
  pos base [\aAyn.init \aNon.fina_BaaNonFina \uni06FC.init \uni063A.init \uni075E.init \uni075D.init \uni075F.init \uni06A0.init \uni0639.init \uni0646.fina_BaaNonFina \uni06BA.fina_BaaNonFina \uni06BC.fina_BaaNonFina \uni06BB.fina_BaaNonFina \uni0768.fina_BaaNonFina \uni0769.fina_BaaNonFina \uni06BD.fina_BaaNonFina ] <anchor 490 -670> mark @TashkilBelow;
  pos base [\aAyn.isol \uni063A \uni075E \uni075D \uni075F \uni06A0 \uni0639 ] <anchor 720 200> mark @TashkilBelow;
  pos base [\aAyn.medi \aFaa.init \aHaa.medi_LamLamHaaInit \aKaf.init_KafBaaInit \aSen.medi_BaaSenAltInit \aKaf.init_KafYaaIsol \aMem.medi_MemAlfFina \uni06FC.medi \uni063A.medi \uni075E.medi \uni075D.medi \uni075F.medi \uni06A0.medi \uni0639.medi \uni066F.init \uni0760.init \uni0642.init \uni0641.init \uni06A8.init \uni06A1.init \uni06A2.init \uni06A3.init \uni06A4.init \uni06A6.init \uni06A7.init \uni062E.medi_LamLamHaaInit \uni062D.medi_LamLamHaaInit \uni0681.medi_LamLamHaaInit \uni0687.medi_LamLamHaaInit \uni0685.medi_LamLamHaaInit \uni062C.medi_LamLamHaaInit \uni0682.medi_LamLamHaaInit \uni0757.medi_LamLamHaaInit \uni0684.medi_LamLamHaaInit \uni076E.medi_LamLamHaaInit \uni0683.medi_LamLamHaaInit \uni06BF.medi_LamLamHaaInit \uni0758.medi_LamLamHaaInit \uni0772.medi_LamLamHaaInit \uni0686.medi_LamLamHaaInit \uni063B.init_KafBaaInit \uni063C.init_KafBaaInit \uni077F.init_KafBaaInit \uni0764.init_KafBaaInit \uni0643.init_KafBaaInit \uni06B0.init_KafBaaInit \uni06B3.init_KafBaaInit \uni06B2.init_KafBaaInit \uni06AB.init_KafBaaInit \uni06AC.init_KafBaaInit \uni06AD.init_KafBaaInit \uni06AE.init_KafBaaInit \uni06AF.init_KafBaaInit \uni06A9.init_KafBaaInit \uni06B4.init_KafBaaInit \uni0763.init_KafBaaInit \uni0762.init_KafBaaInit \uni06B1.init_KafBaaInit \uni06FA.medi_BaaSenAltInit \uni076D.medi_BaaSenAltInit \uni0633.medi_BaaSenAltInit \uni077E.medi_BaaSenAltInit \uni077D.medi_BaaSenAltInit \uni0634.medi_BaaSenAltInit \uni0770.medi_BaaSenAltInit \uni075C.medi_BaaSenAltInit \uni069A.medi_BaaSenAltInit \uni069B.medi_BaaSenAltInit \uni069C.medi_BaaSenAltInit \uni0761.init_FaaMemInit \uni0760.init_FaaMemInit \uni06A2.init_FaaMemInit \uni06A3.init_FaaMemInit \uni06A5.init_FaaMemInit \uni063B.init_KafYaaIsol \uni077F.init_KafYaaIsol \uni0643.init_KafYaaIsol \uni06B0.init_KafYaaIsol \uni06B3.init_KafYaaIsol \uni06B2.init_KafYaaIsol \uni06AB.init_KafYaaIsol \uni06AC.init_KafYaaIsol \uni06AD.init_KafYaaIsol \uni06AF.init_KafYaaIsol \uni06A9.init_KafYaaIsol \uni06B4.init_KafYaaIsol \uni0763.init_KafYaaIsol \uni0762.init_KafYaaIsol \uni06B1.init_KafYaaIsol \uni0645.medi_MemAlfFina \uni0621.medi \uni06E5.medi \uni06E6.medi ] <anchor 300 -670> mark @TashkilBelow;
  pos base [\aBaa.fina \aSen.fina_BaaSen \uni0751.fina \uni062A.fina \uni0754.fina \uni062B.fina \uni0679.fina \uni067C.fina \uni0756.fina \uni066E.fina \uni067F.fina \uni067D.fina \uni0628.fina \uni067A.fina \uni06FA.fina_BaaSen \uni076D.fina_BaaSen \uni0633.fina_BaaSen \uni077E.fina_BaaSen \uni077D.fina_BaaSen \uni0634.fina_BaaSen \uni0770.fina_BaaSen \uni075C.fina_BaaSen \uni069A.fina_BaaSen ] <anchor 1040 -670> mark @TashkilBelow;
  pos base [\aBaa.init \aLam.medi_LamLamInit \aLam.medi_LamLamMedi2 \uni0776.init \uni06BC.init \uni0750.init \uni0756.init \uni0768.init \uni06CE.init \uni0775.init \uni0626.init \uni066E.init \uni0620.init \uni064A.init \uni06BB.init \uni067F.init \uni0755.init \uni067D.init \uni0628.init \uni067A.init \uni0751.init \uni0646.init \uni062A.init \uni0678.init \uni063D.init \uni062B.init \uni0679.init \uni06B9.init \uni0769.init \uni0649.init \uni067C.init \uni0754.init \uni06BA.init \uni06CC.init \uni0767.init \uni06B5.medi_LamLamInit \uni06B7.medi_LamLamInit \uni0644.medi_LamLamInit \uni06B8.medi_LamLamInit \uni06B6.medi_LamLamInit \uni076A.medi_LamLamInit \uni06B5.medi_LamLamMedi2 \uni06B7.medi_LamLamMedi2 \uni0644.medi_LamLamMedi2 \uni06B8.medi_LamLamMedi2 \uni06B6.medi_LamLamMedi2 \uni076A.medi_LamLamMedi2 ] <anchor 170 -670> mark @TashkilBelow;
  pos base [\aBaa.isol \uni0751 \uni062A \uni0754 \uni062B \uni0679 \uni067C \uni0756 \uni066E \uni067F \uni067D \uni0628 \uni067A ] <anchor 1100 -670> mark @TashkilBelow;
  pos base [\aBaa.medi \aLam.init_LamYaaIsol \uni0680.medi \uni0776.medi \uni06BC.medi \uni0750.medi \uni0756.medi \uni0768.medi \uni06CE.medi \uni0775.medi \uni06BD.medi \uni0626.medi \uni066E.medi \uni0620.medi \uni064A.medi \uni06BB.medi \uni067F.medi \uni067D.medi \uni067E.medi \uni0628.medi \uni067A.medi \uni0751.medi \uni0646.medi \uni0753.medi \uni0752.medi \uni062A.medi \uni0678.medi \uni063D.medi \uni062B.medi \uni0679.medi \uni06B9.medi \uni0769.medi \uni0649.medi \uni067C.medi \uni0754.medi \uni06D1.medi \uni06BA.medi \uni06CC.medi \uni0767.medi \uni06B5.init_LamYaaIsol \uni06B7.init_LamYaaIsol \uni0644.init_LamYaaIsol \uni06B6.init_LamYaaIsol \uni076A.init_LamYaaIsol ] <anchor 240 -670> mark @TashkilBelow;
  pos base [\aDal.fina \aDal.isol \aMem.medi \aTaa.init \aLam.medi_LamLamHaaInit \aDal.fina_BaaDal \aKaf.medi_KafYaaFina \aKaf.medi_KafMemMedi \aHeh.init_HehMemInit \uni0690.fina \uni06EE.fina \uni0689.fina \uni0688.fina \uni075A.fina \uni0630.fina \uni062F.fina \uni0759.fina \uni068C.fina \uni068B.fina \uni068A.fina \uni068F.fina \uni068E.fina \uni068D.fina \uni0690 \uni06EE \uni0689 \uni0688 \uni0630 \uni062F \uni0759 \uni068C \uni068B \uni068A \uni068F \uni068E \uni068D \uni0765.medi \uni0645.medi \uni0766.medi \uni0691.fina \uni0692.fina \uni0693.fina \uni0697.fina \uni0698.fina \uni0699.fina \uni075B.fina \uni06EF.fina \uni0632.fina \uni0771.fina \uni0631.fina \uni076B.fina \uni076C.fina \uni0638.init \uni0637.init \uni069F.init \uni06CB.fina \uni0624.fina \uni06CA.fina \uni06CF.fina \uni0778.fina \uni06C6.fina \uni06C7.fina \uni06C4.fina \uni06C5.fina \uni0676.fina \uni06C8.fina \uni06C9.fina \uni0779.fina \uni0648.fina \uni06B5.medi_LamLamHaaInit \uni06B7.medi_LamLamHaaInit \uni0644.medi_LamLamHaaInit \uni06B8.medi_LamLamHaaInit \uni06B6.medi_LamLamHaaInit \uni076A.medi_LamLamHaaInit \uni0690.fina_BaaDal \uni06EE.fina_BaaDal \uni0689.fina_BaaDal \uni0688.fina_BaaDal \uni0630.fina_BaaDal \uni062F.fina_BaaDal \uni0759.fina_BaaDal \uni068C.fina_BaaDal \uni068B.fina_BaaDal \uni068A.fina_BaaDal \uni068F.fina_BaaDal \uni068E.fina_BaaDal \uni068D.fina_BaaDal \uni063B.medi_KafYaaFina \uni063C.medi_KafYaaFina \uni077F.medi_KafYaaFina \uni0764.medi_KafYaaFina \uni0643.medi_KafYaaFina \uni06B0.medi_KafYaaFina \uni06B3.medi_KafYaaFina \uni06B2.medi_KafYaaFina \uni06AB.medi_KafYaaFina \uni06AC.medi_KafYaaFina \uni06AD.medi_KafYaaFina \uni06AE.medi_KafYaaFina \uni06AF.medi_KafYaaFina \uni06A9.medi_KafYaaFina \uni06B4.medi_KafYaaFina \uni0763.medi_KafYaaFina \uni0762.medi_KafYaaFina \uni06B1.medi_KafYaaFina \uni063B.medi_KafMemMedi \uni063C.medi_KafMemMedi \uni077F.medi_KafMemMedi \uni0764.medi_KafMemMedi \uni0643.medi_KafMemMedi \uni06B0.medi_KafMemMedi \uni06B3.medi_KafMemMedi \uni06B2.medi_KafMemMedi \uni06AB.medi_KafMemMedi \uni06AC.medi_KafMemMedi \uni06AD.medi_KafMemMedi \uni06AE.medi_KafMemMedi \uni06AF.medi_KafMemMedi \uni06A9.medi_KafMemMedi \uni06B4.medi_KafMemMedi \uni0763.medi_KafMemMedi \uni0762.medi_KafMemMedi \uni06B1.medi_KafMemMedi \uni0647.init_HehMemInit \uni06C1.init_HehMemInit \aYaaBarree.fina_PostTooth \uni077B.fina_PostTooth \uni077A.fina_PostTooth \uni06D2.fina_PostTooth ] <anchor 500 -670> mark @TashkilBelow;
  pos base [\aFaa.fina \aNon.isol.alt \uni0760.fina \uni0641.fina \uni06A1.fina \uni06A2.fina \uni06A3.fina \uni06A4.fina \uni06A6.fina ] <anchor 1000 -670> mark @TashkilBelow;
  pos base [\aFaa.isol \uni0760 \uni0761 \uni0641 \uni06A1 \uni06A2 \uni06A3 \uni06A4 \uni06A5 \uni06A6 ] <anchor 1078 -670> mark @TashkilBelow;
  pos base [\aFaa.medi \uni066F.medi \uni0760.medi \uni0642.medi \uni0641.medi \uni06A8.medi \uni06A1.medi \uni06A2.medi \uni06A3.medi \uni06A4.medi \uni06A6.medi \uni06A7.medi ] <anchor 330 -670> mark @TashkilBelow;
  pos base [\aHaa.fina \uni062E.fina \uni062D.fina \uni0681.fina \uni0685.fina \uni0682.fina \uni0757.fina \uni0772.fina ] <anchor 800 -50> mark @TashkilBelow;
  pos base [\aHaa.init \aSen.medi \uni06FA.medi \uni076D.medi \uni0633.medi \uni077E.medi \uni077D.medi \uni0634.medi \uni0770.medi \uni075C.medi \uni069A.medi ] <anchor 620 -670> mark @TashkilBelow;
  pos base [\aHaa.isol \uni062E \uni062D \uni0681 \uni0685 \uni0682 \uni0757 \uni0772 ] <anchor 800 100> mark @TashkilBelow;
  pos base [\aHaa.medi \aMem.fina.alt \aNon.isol \aQaf.fina \aSen.init \aKaf.medi_KafMemFina \aHaa.init_Finjani \aMem.init_MemYaaIsol \uni062E.medi \uni062D.medi \uni0681.medi \uni0687.medi \uni0685.medi \uni062C.medi \uni0682.medi \uni0757.medi \uni0684.medi \uni076E.medi \uni0683.medi \uni06BF.medi \uni0758.medi \uni0772.medi \uni0686.medi \uni0646 \uni0767 \uni06BA \uni06BC \uni06BB \uni0768 \uni06B9 \uni0769 \uni06BD \uni06A8.fina \uni06A7.fina \uni0642.fina \uni066F.fina \uni06FA.init \uni076D.init \uni0633.init \uni077E.init \uni077D.init \uni0634.init \uni0770.init \uni075C.init \uni069A.init \uni069B.init \uni069C.init \uni063B.medi_KafMemFina \uni063C.medi_KafMemFina \uni077F.medi_KafMemFina \uni0764.medi_KafMemFina \uni0643.medi_KafMemFina \uni06B0.medi_KafMemFina \uni06B3.medi_KafMemFina \uni06B2.medi_KafMemFina \uni06AB.medi_KafMemFina \uni06AC.medi_KafMemFina \uni06AD.medi_KafMemFina \uni06AE.medi_KafMemFina \uni06AF.medi_KafMemFina \uni06A9.medi_KafMemFina \uni06B4.medi_KafMemFina \uni0763.medi_KafMemFina \uni0762.medi_KafMemFina \uni06B1.medi_KafMemFina \uni062E.init_Finjani \uni062D.init_Finjani \uni0681.init_Finjani \uni0687.init_Finjani \uni0685.init_Finjani \uni062C.init_Finjani \uni0682.init_Finjani \uni0757.init_Finjani \uni0684.init_Finjani \uni076E.init_Finjani \uni0683.init_Finjani \uni0772.init_Finjani \uni0765.init_MemYaaIsol \uni0645.init_MemYaaIsol \uni0766.init_MemYaaIsol \uni0645.fina_LamMemFinaExtended \uni0645.fina_KafMemFinaExtended \uni0645.fina_KafMemIsolExtended \uni06FA.init_SenBaaMemInit \uni076D.init_SenBaaMemInit \uni0633.init_SenBaaMemInit \uni077E.init_SenBaaMemInit \uni077D.init_SenBaaMemInit \uni0634.init_SenBaaMemInit \uni0770.init_SenBaaMemInit \uni075C.init_SenBaaMemInit \uni069A.init_SenBaaMemInit \uni069B.init_SenBaaMemInit \uni069C.init_SenBaaMemInit \aYaaBarree.isol \aMem.init_YaaBarree \uni0765.init_YaaBarree \uni0645.init_YaaBarree \uni0766.init_YaaBarree \aHeh.init_YaaBarree \uni0647.init_YaaBarree \uni06C1.init_YaaBarree \aHehKnotted.init_YaaBarree \uni06BE.init_YaaBarree ] <anchor 600 -670> mark @TashkilBelow;
  pos base [\aHeh.fina \uni0647.fina \uni06C1.fina \uni06C3.fina \uni06D5.fina \uni0629.fina \uni0774.fina_LamAlfIsol \uni0765.init_MemHehInit \aMem.init_MemHehInit \uni0645.init_MemHehInit \uni0766.init_MemHehInit ] <anchor 390 -670> mark @TashkilBelow;
  pos base [\aHeh.init \aHeh.medi \aMem.init \aBaa.init_BaaMemHaaInit \aAyn.medi_AynYaaFina \aKaf.medi_KafRaaFina \aLam.medi_KafLamMemMedi \aKaf.init_KafHeh \aHeh.fina_KafHeh \aHeh.fina_LamHeh \aKaf.init_KafMemInit \aMem.medi_KafMemMedi \aHeh.medi_PostTooth \aLam.medi_LamLamMemInit \aLam.medi_LamLamMemMedi \uni0647.init \uni06C1.init \uni0647.medi \uni06C1.medi \uni0765.init \uni0645.init \uni0766.init \uni0691.fina_BaaRaaIsol \uni0692.fina_BaaRaaIsol \uni0693.fina_BaaRaaIsol \uni0697.fina_BaaRaaIsol \uni0698.fina_BaaRaaIsol \uni0699.fina_BaaRaaIsol \uni075B.fina_BaaRaaIsol \uni06EF.fina_BaaRaaIsol \uni0632.fina_BaaRaaIsol \uni0771.fina_BaaRaaIsol \uni0631.fina_BaaRaaIsol \uni076B.fina_BaaRaaIsol \uni076C.fina_BaaRaaIsol \uni0680.init_BaaMemHaaInit \uni0776.init_BaaMemHaaInit \uni06BC.init_BaaMemHaaInit \uni0750.init_BaaMemHaaInit \uni0756.init_BaaMemHaaInit \uni0768.init_BaaMemHaaInit \uni06CE.init_BaaMemHaaInit \uni0775.init_BaaMemHaaInit \uni06BD.init_BaaMemHaaInit \uni0626.init_BaaMemHaaInit \uni066E.init_BaaMemHaaInit \uni0620.init_BaaMemHaaInit \uni064A.init_BaaMemHaaInit \uni06BB.init_BaaMemHaaInit \uni067F.init_BaaMemHaaInit \uni067D.init_BaaMemHaaInit \uni067E.init_BaaMemHaaInit \uni0628.init_BaaMemHaaInit \uni067A.init_BaaMemHaaInit \uni0751.init_BaaMemHaaInit \uni0646.init_BaaMemHaaInit \uni0753.init_BaaMemHaaInit \uni0752.init_BaaMemHaaInit \uni062A.init_BaaMemHaaInit \uni0678.init_BaaMemHaaInit \uni063D.init_BaaMemHaaInit \uni062B.init_BaaMemHaaInit \uni0679.init_BaaMemHaaInit \uni06B9.init_BaaMemHaaInit \uni0769.init_BaaMemHaaInit \uni0649.init_BaaMemHaaInit \uni067C.init_BaaMemHaaInit \uni0754.init_BaaMemHaaInit \uni06D1.init_BaaMemHaaInit \uni06BA.init_BaaMemHaaInit \uni06CC.init_BaaMemHaaInit \uni0767.init_BaaMemHaaInit \uni06FC.medi_AynYaaFina \uni063A.medi_AynYaaFina \uni075E.medi_AynYaaFina \uni075D.medi_AynYaaFina \uni075F.medi_AynYaaFina \uni06A0.medi_AynYaaFina \uni0639.medi_AynYaaFina \uni063B.medi_KafRaaFina \uni063C.medi_KafRaaFina \uni077F.medi_KafRaaFina \uni0764.medi_KafRaaFina \uni0643.medi_KafRaaFina \uni06B0.medi_KafRaaFina \uni06B3.medi_KafRaaFina \uni06B2.medi_KafRaaFina \uni06AB.medi_KafRaaFina \uni06AC.medi_KafRaaFina \uni06AD.medi_KafRaaFina \uni06AE.medi_KafRaaFina \uni06AF.medi_KafRaaFina \uni06A9.medi_KafRaaFina \uni06B4.medi_KafRaaFina \uni0763.medi_KafRaaFina \uni0762.medi_KafRaaFina \uni06B1.medi_KafRaaFina \uni0691.fina_HaaRaaIsol \uni0692.fina_HaaRaaIsol \uni0693.fina_HaaRaaIsol \uni0697.fina_HaaRaaIsol \uni0698.fina_HaaRaaIsol \uni0699.fina_HaaRaaIsol \uni075B.fina_HaaRaaIsol \uni06EF.fina_HaaRaaIsol \uni0632.fina_HaaRaaIsol \uni0771.fina_HaaRaaIsol \uni0631.fina_HaaRaaIsol \uni076B.fina_HaaRaaIsol \uni076C.fina_HaaRaaIsol \uni06B5.medi_KafLamMemMedi \uni06B7.medi_KafLamMemMedi \uni0644.medi_KafLamMemMedi \uni06B8.medi_KafLamMemMedi \uni06B6.medi_KafLamMemMedi \uni076A.medi_KafLamMemMedi \uni063B.init_KafHeh \uni077F.init_KafHeh \uni0643.init_KafHeh \uni06B0.init_KafHeh \uni06B3.init_KafHeh \uni06B2.init_KafHeh \uni06AB.init_KafHeh \uni06AC.init_KafHeh \uni06AD.init_KafHeh \uni06AF.init_KafHeh \uni06A9.init_KafHeh \uni06B4.init_KafHeh \uni0763.init_KafHeh \uni0762.init_KafHeh \uni06B1.init_KafHeh \uni0647.fina_KafHeh \uni06C1.fina_KafHeh \uni06C3.fina_KafHeh \uni06D5.fina_KafHeh \uni0629.fina_KafHeh \uni0647.fina_LamHeh \uni06C1.fina_LamHeh \uni06C3.fina_LamHeh \uni06D5.fina_LamHeh \uni0629.fina_LamHeh \uni063B.init_KafMemInit \uni063C.init_KafMemInit \uni077F.init_KafMemInit \uni0764.init_KafMemInit \uni0643.init_KafMemInit \uni06B0.init_KafMemInit \uni06B3.init_KafMemInit \uni06B2.init_KafMemInit \uni06AB.init_KafMemInit \uni06AC.init_KafMemInit \uni06AD.init_KafMemInit \uni06AE.init_KafMemInit \uni06AF.init_KafMemInit \uni06A9.init_KafMemInit \uni06B4.init_KafMemInit \uni0763.init_KafMemInit \uni0762.init_KafMemInit \uni06B1.init_KafMemInit \uni0645.medi_KafMemMedi \uni0647.medi_PostTooth \uni06C1.medi_PostTooth \uni06B5.medi_LamLamMemInit \uni06B7.medi_LamLamMemInit \uni0644.medi_LamLamMemInit \uni06B8.medi_LamLamMemInit \uni06B6.medi_LamLamMemInit \uni076A.medi_LamLamMemInit \uni06B5.medi_LamLamMemMedi \uni06B7.medi_LamLamMemMedi \uni0644.medi_LamLamMemMedi \uni06B8.medi_LamLamMemMedi \uni06B6.medi_LamLamMemMedi \uni076A.medi_LamLamMemMedi \uni06BE.init \uni06FF.init \uni06BE.medi \uni06FF.medi ] <anchor 400 -670> mark @TashkilBelow;
  pos base [\aHeh.isol \aKaf.init_KafRaaIsol \uni0647 \uni06C2 \uni06C0 \uni06C1 \uni06C3 \uni06D5 \uni0629 \uni063B.init_KafRaaIsol \uni063C.init_KafRaaIsol \uni077F.init_KafRaaIsol \uni0764.init_KafRaaIsol \uni0643.init_KafRaaIsol \uni06B0.init_KafRaaIsol \uni06B3.init_KafRaaIsol \uni06B2.init_KafRaaIsol \uni06AB.init_KafRaaIsol \uni06AC.init_KafRaaIsol \uni06AD.init_KafRaaIsol \uni06AE.init_KafRaaIsol \uni06AF.init_KafRaaIsol \uni06A9.init_KafRaaIsol \uni06B4.init_KafRaaIsol \uni0763.init_KafRaaIsol \uni0762.init_KafRaaIsol \uni06B1.init_KafRaaIsol ] <anchor 410 -670> mark @TashkilBelow;
  pos base [\aKaf.fina \aRaa.fina \aSad.medi \aTaa.fina \aWaw.fina \uni063B.fina \uni063C.fina \uni077F.fina \uni0764.fina \uni0643.fina \uni06B0.fina \uni06B3.fina \uni06B2.fina \uni06AB.fina \uni06AC.fina \uni06AD.fina \uni06AE.fina \uni06AF.fina \uni06A9.fina \uni06B4.fina \uni0763.fina \uni0762.fina \uni06B1.fina \uni069D.medi \uni06FB.medi \uni0636.medi \uni069E.medi \uni0635.medi \uni0638.fina \uni0637.fina \uni069F.fina ] <anchor 800 -670> mark @TashkilBelow;
  pos base [\aKaf.init.alt \uni06AA.init ] <anchor 1500 -670> mark @TashkilBelow;
  pos base [\aKaf.init \aMem.medi_BaaMemHaaInit \uni063B.init \uni063C.init \uni077F.init \uni0764.init \uni0643.init \uni06B0.init \uni06B3.init \uni06B2.init \uni06AB.init \uni06AC.init \uni06AD.init \uni06AE.init \uni06AF.init \uni06A9.init \uni06B4.init \uni0763.init \uni0762.init \uni06B1.init \uni0645.medi_BaaMemHaaInit ] <anchor 550 -670> mark @TashkilBelow;
  pos base [\aKaf.isol \aMem.fina \aQaf.isol \aSad.fina \aSad.init \aSad.isol \aSen.isol \aYaa.isol \aRaa.fina_BaaRaaIsol \aRaa.fina_HaaRaaIsol \uni06BE \uni06FF \aKaf.medi_KafMemAlf \uni063B \uni063C \uni077F \uni0764 \uni0643 \uni06B0 \uni06B3 \uni06B2 \uni06AB \uni06AC \uni06AD \uni06AE \uni06AF \uni06A9 \uni06B4 \uni0763 \uni0762 \uni06B1 \uni0765.fina \uni0645.fina \uni0766.fina \uni06A8 \uni06A7 \uni0642 \uni066F \uni069D.fina \uni06FB.fina \uni0636.fina \uni069E.fina \uni0635.fina \uni069D.init \uni06FB.init \uni0636.init \uni069E.init \uni0635.init \uni069D \uni06FB \uni0636 \uni069E \uni0635 \uni06FA \uni076D \uni0633 \uni077E \uni077D \uni0634 \uni0770 \uni075C \uni069A \uni069B \uni069C \uni0677.fina \uni0775 \uni063F \uni0678 \uni063D \uni063E \uni0649 \uni0776 \uni06CD \uni06CC \uni0626 \uni06CE \uni063B.medi_KafMemAlf \uni063C.medi_KafMemAlf \uni077F.medi_KafMemAlf \uni0764.medi_KafMemAlf \uni0643.medi_KafMemAlf \uni06B0.medi_KafMemAlf \uni06B3.medi_KafMemAlf \uni06B2.medi_KafMemAlf \uni06AB.medi_KafMemAlf \uni06AC.medi_KafMemAlf \uni06AD.medi_KafMemAlf \uni06AE.medi_KafMemAlf \uni06AF.medi_KafMemAlf \uni06A9.medi_KafMemAlf \uni06B4.medi_KafMemAlf \uni0763.medi_KafMemAlf \uni0762.medi_KafMemAlf \uni06B1.medi_KafMemAlf \uni069D.init_SenBaaMemInit \uni06FB.init_SenBaaMemInit \uni0636.init_SenBaaMemInit \uni069E.init_SenBaaMemInit \uni0635.init_SenBaaMemInit \aTaa.init_YaaBaree \uni0638.init_YaaBarree \uni0637.init_YaaBarree \uni069F.init_YaaBarree \aHehKnotted.isol ] <anchor 700 -670> mark @TashkilBelow;
  pos base [\aKaf.medi \uni063B.medi \uni063C.medi \uni077F.medi \uni0764.medi \uni0643.medi \uni06B0.medi \uni06B3.medi \uni06B2.medi \uni06AB.medi \uni06AC.medi \uni06AD.medi \uni06AE.medi \uni06AF.medi \uni06A9.medi \uni06B4.medi \uni0763.medi \uni0762.medi \uni06B1.medi ] <anchor 609 -670> mark @TashkilBelow;
  pos base [\aLam.fina \aLam.fina_KafLam \aLam.fina_LamLamIsol \aLam.fina_LamLamFina \uni06B5.fina \uni06B7.fina \uni0644.fina \uni06B6.fina \uni076A.fina \uni06B5.fina_KafLam \uni06B7.fina_KafLam \uni0644.fina_KafLam \uni06B6.fina_KafLam \uni076A.fina_KafLam \uni06B5.fina_LamLamIsol \uni06B7.fina_LamLamIsol \uni0644.fina_LamLamIsol \uni06B6.fina_LamLamIsol \uni076A.fina_LamLamIsol \uni06B5.fina_LamLamFina \uni06B7.fina_LamLamFina \uni0644.fina_LamLamFina \uni06B6.fina_LamLamFina \uni076A.fina_LamLamFina ] <anchor 600 -770> mark @TashkilBelow;
  pos base [\aLam.init \aLam.medi_KafLamAlf \uni06B5.init \uni06B7.init \uni0644.init \uni06B8.init \uni06B6.init \uni076A.init \uni06B5.medi_KafLamAlf \uni06B7.medi_KafLamAlf \uni0644.medi_KafLamAlf \uni06B8.medi_KafLamAlf \uni06B6.medi_KafLamAlf \uni076A.medi_KafLamAlf ] <anchor 140 -670> mark @TashkilBelow;
  pos base [\aLam.isol \aQaf.fina_LamQafFina \aMem.fina_KafMemIsol \uni06B5 \uni06B7 \uni0644 \uni06B6 \uni076A \uni06A8.fina_LamQafFina \uni06A7.fina_LamQafFina \uni0642.fina_LamQafFina \uni066F.fina_LamQafFina \uni0645.fina_KafMemIsol ] <anchor 580 -670> mark @TashkilBelow;
  pos base [\aLam.medi \aMem.medi_LamHaaMemInit \uni06B5.medi \uni06B7.medi \uni0644.medi \uni06B8.medi \uni06B6.medi \uni076A.medi \uni0645.medi_LamHaaMemInit ] <anchor 190 -670> mark @TashkilBelow;
  pos base [\aMem.isol \aNon.fina \uni0765 \uni0645 \uni0766 \uni0646.fina \uni06BA.fina \uni06BC.fina \uni06BB.fina \uni0768.fina \uni0769.fina \uni06BD.fina ] <anchor 650 -800> mark @TashkilBelow;
  pos base [\aNon.fina.alt ] <anchor 1000 -870> mark @TashkilBelow;
  pos base [\aRaa.fina.alt2 \aRaa.fina_MemRaaIsol ] <anchor 530 -670> mark @TashkilBelow;
  pos base [\aRaa.isol \uni0691 \uni0692 \uni0695 \uni0697 \uni0698 \uni0699 \uni075B \uni06EF \uni0632 \uni0771 \uni0631 \uni076B \uni076C ] <anchor 508 -670> mark @TashkilBelow;
  pos base [\aSen.fina \uni06FA.fina \uni076D.fina \uni0633.fina \uni077E.fina \uni077D.fina \uni0634.fina \uni0770.fina \uni075C.fina \uni069A.fina \uni069B.fina \uni069C.fina ] <anchor 640 -800> mark @TashkilBelow;
  pos base [\aTaa.isol \aAyn.init_AynHaaInit \aAyn.init_AynYaaIsol \uni0638 \uni0637 \uni069F \uni06FC.init_AynHaaInit \uni063A.init_AynHaaInit \uni075E.init_AynHaaInit \uni075D.init_AynHaaInit \uni075F.init_AynHaaInit \uni06A0.init_AynHaaInit \uni0639.init_AynHaaInit \uni06FC.init_AynYaaIsol \uni063A.init_AynYaaIsol \uni075E.init_AynYaaIsol \uni075D.init_AynYaaIsol \uni075F.init_AynYaaIsol \uni06A0.init_AynYaaIsol \uni0639.init_AynYaaIsol ] <anchor 900 -670> mark @TashkilBelow;
  pos base [\aTaa.medi \aLam.medi_LamMemFina \aBaa.medi_BaaMemFina \uni0638.medi \uni0637.medi \uni069F.medi \uni06B5.medi_LamMemFina \uni06B7.medi_LamMemFina \uni0644.medi_LamMemFina \uni06B8.medi_LamMemFina \uni06B6.medi_LamMemFina \uni076A.medi_LamMemFina \uni06BC.medi_BaaMemFina \uni0756.medi_BaaMemFina \uni0768.medi_BaaMemFina \uni0626.medi_BaaMemFina \uni066E.medi_BaaMemFina \uni06BB.medi_BaaMemFina \uni067F.medi_BaaMemFina \uni067D.medi_BaaMemFina \uni067A.medi_BaaMemFina \uni0646.medi_BaaMemFina \uni062A.medi_BaaMemFina \uni0678.medi_BaaMemFina \uni062B.medi_BaaMemFina \uni0679.medi_BaaMemFina \uni0769.medi_BaaMemFina \uni0649.medi_BaaMemFina \uni067C.medi_BaaMemFina \uni06BA.medi_BaaMemFina \uni0691.fina_KafRaaIsol \uni0692.fina_KafRaaIsol \uni0693.fina_KafRaaIsol \uni0697.fina_KafRaaIsol \uni0698.fina_KafRaaIsol \uni0699.fina_KafRaaIsol \uni075B.fina_KafRaaIsol \uni06EF.fina_KafRaaIsol \uni0632.fina_KafRaaIsol \uni0771.fina_KafRaaIsol \uni0631.fina_KafRaaIsol \uni076B.fina_KafRaaIsol \uni076C.fina_KafRaaIsol ] <anchor 450 -670> mark @TashkilBelow;
  pos base [\aWaw.isol \uni0750.fina_BaaBaaIsol ] <anchor 810 -670> mark @TashkilBelow;
  pos base [\aYaa.fina \uni0775.fina \uni063F.fina \uni0678.fina \uni063D.fina \uni063E.fina \uni0649.fina \uni0776.fina \uni06CD.fina \uni06CC.fina \uni0626.fina \uni06CE.fina ] <anchor 700 -770> mark @TashkilBelow;
  pos base [\uni0621 ] <anchor 430 60> mark @TashkilBelow;
  pos base [\aMem.fina_LamMemFina \uni0645.fina_LamMemFina \uni0773.fina_LamAlfFina ] <anchor 417 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaRaaIsol \uni0680.init_BaaRaaIsol \uni0776.init_BaaRaaIsol \uni06BC.init_BaaRaaIsol \uni0750.init_BaaRaaIsol \uni0756.init_BaaRaaIsol \uni0768.init_BaaRaaIsol \uni06CE.init_BaaRaaIsol \uni0775.init_BaaRaaIsol \uni06BD.init_BaaRaaIsol \uni0626.init_BaaRaaIsol \uni066E.init_BaaRaaIsol \uni0620.init_BaaRaaIsol \uni064A.init_BaaRaaIsol \uni06BB.init_BaaRaaIsol \uni067F.init_BaaRaaIsol \uni0755.init_BaaRaaIsol \uni067D.init_BaaRaaIsol \uni067E.init_BaaRaaIsol \uni067B.init_BaaRaaIsol \uni0628.init_BaaRaaIsol \uni067A.init_BaaRaaIsol \uni0751.init_BaaRaaIsol \uni0646.init_BaaRaaIsol \uni0753.init_BaaRaaIsol \uni0752.init_BaaRaaIsol \uni062A.init_BaaRaaIsol \uni0678.init_BaaRaaIsol \uni063D.init_BaaRaaIsol \uni062B.init_BaaRaaIsol \uni0679.init_BaaRaaIsol \uni06B9.init_BaaRaaIsol \uni0769.init_BaaRaaIsol \uni0649.init_BaaRaaIsol \uni067C.init_BaaRaaIsol \uni0754.init_BaaRaaIsol \uni06D1.init_BaaRaaIsol \uni06D0.init_BaaRaaIsol \uni06BA.init_BaaRaaIsol \uni06CC.init_BaaRaaIsol \uni0767.init_BaaRaaIsol \uni08A0.init_BaaRaaIsol ] <anchor 194 -670> mark @TashkilBelow;
  pos base [\aLam.medi_LamWawFina \aLam.init_LamLamHaaInit \aMem.medi_LamBaaMemInit \aBaa.medi_KafBaaInit \aHeh.medi_LamHehInit \aHeh.medi_BaaHehMedi \uni06B5.medi_LamWawFina \uni06B7.medi_LamWawFina \uni0644.medi_LamWawFina \uni06B8.medi_LamWawFina \uni06B6.medi_LamWawFina \uni076A.medi_LamWawFina \uni06B5.init_LamLamHaaInit \uni06B7.init_LamLamHaaInit \uni0644.init_LamLamHaaInit \uni06B8.init_LamLamHaaInit \uni06B6.init_LamLamHaaInit \uni076A.init_LamLamHaaInit \uni0645.medi_LamBaaMemInit \uni0680.medi_KafBaaInit \uni0776.medi_KafBaaInit \uni06BC.medi_KafBaaInit \uni0750.medi_KafBaaInit \uni0756.medi_KafBaaInit \uni0768.medi_KafBaaInit \uni06CE.medi_KafBaaInit \uni0775.medi_KafBaaInit \uni06BD.medi_KafBaaInit \uni0626.medi_KafBaaInit \uni066E.medi_KafBaaInit \uni0620.medi_KafBaaInit \uni064A.medi_KafBaaInit \uni06BB.medi_KafBaaInit \uni067F.medi_KafBaaInit \uni0755.medi_KafBaaInit \uni067D.medi_KafBaaInit \uni067E.medi_KafBaaInit \uni0628.medi_KafBaaInit \uni067A.medi_KafBaaInit \uni0751.medi_KafBaaInit \uni0646.medi_KafBaaInit \uni0753.medi_KafBaaInit \uni0752.medi_KafBaaInit \uni062A.medi_KafBaaInit \uni0678.medi_KafBaaInit \uni063D.medi_KafBaaInit \uni062B.medi_KafBaaInit \uni0679.medi_KafBaaInit \uni06B9.medi_KafBaaInit \uni0769.medi_KafBaaInit \uni0649.medi_KafBaaInit \uni067C.medi_KafBaaInit \uni0754.medi_KafBaaInit \uni06D1.medi_KafBaaInit \uni06BA.medi_KafBaaInit \uni06CC.medi_KafBaaInit \uni0767.medi_KafBaaInit \uni0647.medi_LamHehInit \uni06C1.medi_LamHehInit \uni0647.medi_BaaHehMedi \uni06C1.medi_BaaHehMedi ] <anchor 200 -670> mark @TashkilBelow;
  pos base [\aWaw.fina_LamWawFina ] <anchor 740 -670> mark @TashkilBelow;
  pos base [\aLam.init_LamHaaInit \uni06B5.init_LamHaaInit \uni06B7.init_LamHaaInit \uni0644.init_LamHaaInit \uni06B8.init_LamHaaInit \uni06B6.init_LamHaaInit \uni076A.init_LamHaaInit ] <anchor 841 -670> mark @TashkilBelow;
  pos base [\aFaa.medi_FaaYaaFina \uni066F.medi_FaaYaaFina \uni0642.medi_FaaYaaFina \uni0641.medi_FaaYaaFina \uni06A8.medi_FaaYaaFina \uni06A1.medi_FaaYaaFina \uni06A4.medi_FaaYaaFina \uni06A6.medi_FaaYaaFina \uni06A7.medi_FaaYaaFina ] <anchor 518 -670> mark @TashkilBelow;
  pos base [\aYaa.fina_FaaYaaFina \uni0775.fina_FaaYaaFina \uni063F.fina_FaaYaaFina \uni0678.fina_FaaYaaFina \uni063D.fina_FaaYaaFina \uni063E.fina_FaaYaaFina \uni0649.fina_FaaYaaFina \uni0776.fina_FaaYaaFina \uni06CD.fina_FaaYaaFina \uni06CC.fina_FaaYaaFina \uni0626.fina_FaaYaaFina \uni06CE.fina_FaaYaaFina ] <anchor 700 -800> mark @TashkilBelow;
  pos base [\aMem.fina_BaaMemFina \uni0645.fina_BaaMemFina ] <anchor 550 -1200> mark @TashkilBelow;
  pos base [\aSad.init_AboveHaa \uni069D.init_AboveHaa \uni06FB.init_AboveHaa \uni0636.init_AboveHaa \uni069E.init_AboveHaa \uni0635.init_AboveHaa ] <anchor 1200 -280> mark @TashkilBelow;
  pos base [\aLam.init_LamBaaMemInit \aKaf.medi_KafBaaMedi \uni06B5.init_LamBaaMemInit \uni06B7.init_LamBaaMemInit \uni0644.init_LamBaaMemInit \uni06B8.init_LamBaaMemInit \uni06B6.init_LamBaaMemInit \uni076A.init_LamBaaMemInit \uni063D.medi_BaaRaaFina \uni063B.medi_KafBaaMedi \uni063C.medi_KafBaaMedi \uni077F.medi_KafBaaMedi \uni0764.medi_KafBaaMedi \uni0643.medi_KafBaaMedi \uni06B0.medi_KafBaaMedi \uni06B3.medi_KafBaaMedi \uni06B2.medi_KafBaaMedi \uni06AB.medi_KafBaaMedi \uni06AC.medi_KafBaaMedi \uni06AD.medi_KafBaaMedi \uni06AE.medi_KafBaaMedi \uni06AF.medi_KafBaaMedi \uni06A9.medi_KafBaaMedi \uni06B4.medi_KafBaaMedi \uni0763.medi_KafBaaMedi \uni0762.medi_KafBaaMedi \uni06B1.medi_KafBaaMedi ] <anchor 360 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_LamBaaMemInit \aBaa.medi_BaaRaaFina \uni0776.medi_LamBaaMemInit \uni06BC.medi_LamBaaMemInit \uni0750.medi_LamBaaMemInit \uni0756.medi_LamBaaMemInit \uni0768.medi_LamBaaMemInit \uni06CE.medi_LamBaaMemInit \uni0775.medi_LamBaaMemInit \uni0626.medi_LamBaaMemInit \uni066E.medi_LamBaaMemInit \uni0620.medi_LamBaaMemInit \uni064A.medi_LamBaaMemInit \uni06BB.medi_LamBaaMemInit \uni067F.medi_LamBaaMemInit \uni067D.medi_LamBaaMemInit \uni0628.medi_LamBaaMemInit \uni067A.medi_LamBaaMemInit \uni0751.medi_LamBaaMemInit \uni0646.medi_LamBaaMemInit \uni062A.medi_LamBaaMemInit \uni0678.medi_LamBaaMemInit \uni063D.medi_LamBaaMemInit \uni062B.medi_LamBaaMemInit \uni0679.medi_LamBaaMemInit \uni06B9.medi_LamBaaMemInit \uni0769.medi_LamBaaMemInit \uni0649.medi_LamBaaMemInit \uni067C.medi_LamBaaMemInit \uni0754.medi_LamBaaMemInit \uni06BA.medi_LamBaaMemInit \uni06CC.medi_LamBaaMemInit \uni0767.medi_LamBaaMemInit \uni0776.medi_BaaRaaFina \uni06BC.medi_BaaRaaFina \uni0750.medi_BaaRaaFina \uni0756.medi_BaaRaaFina \uni0768.medi_BaaRaaFina \uni06CE.medi_BaaRaaFina \uni0775.medi_BaaRaaFina \uni0626.medi_BaaRaaFina \uni066E.medi_BaaRaaFina \uni0620.medi_BaaRaaFina \uni064A.medi_BaaRaaFina \uni06BB.medi_BaaRaaFina \uni067F.medi_BaaRaaFina \uni067D.medi_BaaRaaFina \uni0628.medi_BaaRaaFina \uni067A.medi_BaaRaaFina \uni0751.medi_BaaRaaFina \uni0646.medi_BaaRaaFina \uni062A.medi_BaaRaaFina \uni0678.medi_BaaRaaFina \uni062B.medi_BaaRaaFina \uni0679.medi_BaaRaaFina \uni06B9.medi_BaaRaaFina \uni0769.medi_BaaRaaFina \uni0649.medi_BaaRaaFina \uni067C.medi_BaaRaaFina \uni0754.medi_BaaRaaFina \uni06BA.medi_BaaRaaFina \uni06CC.medi_BaaRaaFina \uni0767.medi_BaaRaaFina ] <anchor 290 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaDal \uni0776.init_BaaDal \uni06BC.init_BaaDal \uni0750.init_BaaDal \uni0756.init_BaaDal \uni0768.init_BaaDal \uni06CE.init_BaaDal \uni0775.init_BaaDal \uni0626.init_BaaDal \uni066E.init_BaaDal \uni0620.init_BaaDal \uni064A.init_BaaDal \uni06BB.init_BaaDal \uni067F.init_BaaDal \uni067D.init_BaaDal \uni0628.init_BaaDal \uni067A.init_BaaDal \uni0751.init_BaaDal \uni0646.init_BaaDal \uni062A.init_BaaDal \uni0678.init_BaaDal \uni063D.init_BaaDal \uni062B.init_BaaDal \uni0679.init_BaaDal \uni06B9.init_BaaDal \uni0769.init_BaaDal \uni0649.init_BaaDal \uni067C.init_BaaDal \uni0754.init_BaaDal \uni06BA.init_BaaDal \uni06CC.init_BaaDal \uni0767.init_BaaDal ] <anchor 80 -670> mark @TashkilBelow;
  pos base [\aHaa.medi_BaaMemHaaInit \uni062E.medi_BaaMemHaaInit \uni062D.medi_BaaMemHaaInit \uni0681.medi_BaaMemHaaInit \uni0687.medi_BaaMemHaaInit \uni0685.medi_BaaMemHaaInit \uni062C.medi_BaaMemHaaInit \uni0682.medi_BaaMemHaaInit \uni0757.medi_BaaMemHaaInit \uni0684.medi_BaaMemHaaInit \uni076E.medi_BaaMemHaaInit \uni0683.medi_BaaMemHaaInit \uni06BF.medi_BaaMemHaaInit \uni0758.medi_BaaMemHaaInit \uni0772.medi_BaaMemHaaInit \uni0686.medi_BaaMemHaaInit ] <anchor 312 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaBaaYaa \uni0680.init_BaaBaaYaa \uni0776.init_BaaBaaYaa \uni06BC.init_BaaBaaYaa \uni0750.init_BaaBaaYaa \uni0756.init_BaaBaaYaa \uni0768.init_BaaBaaYaa \uni06CE.init_BaaBaaYaa \uni0775.init_BaaBaaYaa \uni06BD.init_BaaBaaYaa \uni0626.init_BaaBaaYaa \uni066E.init_BaaBaaYaa \uni0620.init_BaaBaaYaa \uni064A.init_BaaBaaYaa \uni06BB.init_BaaBaaYaa \uni067F.init_BaaBaaYaa \uni0755.init_BaaBaaYaa \uni067D.init_BaaBaaYaa \uni067E.init_BaaBaaYaa \uni067B.init_BaaBaaYaa \uni0628.init_BaaBaaYaa \uni067A.init_BaaBaaYaa \uni0751.init_BaaBaaYaa \uni0646.init_BaaBaaYaa \uni0753.init_BaaBaaYaa \uni0752.init_BaaBaaYaa \uni062A.init_BaaBaaYaa \uni0678.init_BaaBaaYaa \uni063D.init_BaaBaaYaa \uni062B.init_BaaBaaYaa \uni0679.init_BaaBaaYaa \uni06B9.init_BaaBaaYaa \uni0769.init_BaaBaaYaa \uni0649.init_BaaBaaYaa \uni067C.init_BaaBaaYaa \uni0754.init_BaaBaaYaa \uni06D1.init_BaaBaaYaa \uni06D0.init_BaaBaaYaa \uni06BA.init_BaaBaaYaa \uni06CC.init_BaaBaaYaa \uni0767.init_BaaBaaYaa \uni08A0.init_BaaBaaYaa \uni0620.init_BaaBaaHeh ] <anchor 147 -760> mark @TashkilBelow;
  pos base [\aBaa.medi_BaaBaaYaa \uni06BC.medi_BaaBaaYaa \uni0756.medi_BaaBaaYaa \uni0768.medi_BaaBaaYaa \uni0626.medi_BaaBaaYaa \uni066E.medi_BaaBaaYaa \uni06BB.medi_BaaBaaYaa \uni067F.medi_BaaBaaYaa \uni067D.medi_BaaBaaYaa \uni0628.medi_BaaBaaYaa \uni067A.medi_BaaBaaYaa \uni0751.medi_BaaBaaYaa \uni0646.medi_BaaBaaYaa \uni062A.medi_BaaBaaYaa \uni0678.medi_BaaBaaYaa \uni062B.medi_BaaBaaYaa \uni0679.medi_BaaBaaYaa \uni06B9.medi_BaaBaaYaa \uni0769.medi_BaaBaaYaa \uni0649.medi_BaaBaaYaa \uni067C.medi_BaaBaaYaa \uni06BA.medi_BaaBaaYaa ] <anchor 515 -760> mark @TashkilBelow;
  pos base [\aYaa.fina_BaaBaaYaa \uni0775.fina_BaaBaaYaa \uni063F.fina_BaaBaaYaa \uni0678.fina_BaaBaaYaa \uni063D.fina_BaaBaaYaa \uni063E.fina_BaaBaaYaa \uni0649.fina_BaaBaaYaa \uni0776.fina_BaaBaaYaa \uni06CD.fina_BaaBaaYaa \uni06CC.fina_BaaBaaYaa \uni0626.fina_BaaBaaYaa \uni06CE.fina_BaaBaaYaa ] <anchor 659 -760> mark @TashkilBelow;
  pos base [\aLam.medi_LamYaaFina \aLam.medi_LamMemMedi \aAlf.fina_MemAlfFina \uni06B5.medi_LamYaaFina \uni06B7.medi_LamYaaFina \uni0644.medi_LamYaaFina \uni06B6.medi_LamYaaFina \uni076A.medi_LamYaaFina \uni06B5.medi_LamMemMedi \uni06B7.medi_LamMemMedi \uni0644.medi_LamMemMedi \uni06B8.medi_LamMemMedi \uni06B6.medi_LamMemMedi \uni076A.medi_LamMemMedi \uni0627.fina_MemAlfFina \uni0675.fina_MemAlfFina \uni0672.fina_MemAlfFina ] <anchor 270 -670> mark @TashkilBelow;
  pos base [\aYaa.fina_LamYaaFina \uni0775.fina_LamYaaFina \uni063F.fina_LamYaaFina \uni0678.fina_LamYaaFina \uni063D.fina_LamYaaFina \uni063E.fina_LamYaaFina \uni0649.fina_LamYaaFina \uni0776.fina_LamYaaFina \uni06CD.fina_LamYaaFina \uni06CC.fina_LamYaaFina \uni0626.fina_LamYaaFina \uni06CE.fina_LamYaaFina ] <anchor 649 -760> mark @TashkilBelow;
  pos base [\aLam.init_LamMemInit \aMem.medi_LamMemInit \aLam.medi_LamQafFina \aLam.medi_LamAlfFina \aMem.medi_BaaBaaMemInit \aSen.init_PreYaa \aAlf.fina_KafAlf \aAlf.fina_KafMemAlf \aLam.medi_LamLamMedi \uni06B5.init_LamMemInit \uni06B7.init_LamMemInit \uni0644.init_LamMemInit \uni06B8.init_LamMemInit \uni06B6.init_LamMemInit \uni076A.init_LamMemInit \uni0645.medi_LamMemInit \uni06B5.medi_LamQafFina \uni06B7.medi_LamQafFina \uni0644.medi_LamQafFina \uni06B8.medi_LamQafFina \uni06B6.medi_LamQafFina \uni076A.medi_LamQafFina \uni06B5.medi_LamAlfFina \uni06B7.medi_LamAlfFina \uni0644.medi_LamAlfFina \uni06B8.medi_LamAlfFina \uni06B6.medi_LamAlfFina \uni076A.medi_LamAlfFina \uni0645.medi_BaaBaaMemInit \uni06FA.init_PreYaa \uni076D.init_PreYaa \uni0633.init_PreYaa \uni077E.init_PreYaa \uni077D.init_PreYaa \uni0634.init_PreYaa \uni0770.init_PreYaa \uni075C.init_PreYaa \uni069A.init_PreYaa \uni069B.init_PreYaa \uni069C.init_PreYaa \uni0627.fina_KafAlf \uni0675.fina_KafAlf \uni0672.fina_KafAlf \uni0627.fina_KafMemAlf \uni0675.fina_KafMemAlf \uni0672.fina_KafMemAlf \uni06B5.medi_LamLamMedi \uni06B7.medi_LamLamMedi \uni0644.medi_LamLamMedi \uni06B6.medi_LamLamMedi \uni076A.medi_LamLamMedi ] <anchor 0 -670> mark @TashkilBelow;
  pos base [\aLam.init_LamAlfIsol \uni06B5.init_LamAlfIsol \uni06B7.init_LamAlfIsol \uni0644.init_LamAlfIsol \uni06B8.init_LamAlfIsol \uni06B6.init_LamAlfIsol \uni076A.init_LamAlfIsol ] <anchor 240 -730> mark @TashkilBelow;
  pos base [\aAlf.fina_LamAlfIsol \uni0627.fina_LamAlfIsol \uni0623.fina_LamAlfIsol \uni0675.fina_LamAlfIsol \uni0672.fina_LamAlfIsol ] <anchor 310 -670> mark @TashkilBelow;
  pos base [\aLam.init_LamHaaMemInit \aKaf.init_KafMemAlf \uni06B5.init_LamHaaMemInit \uni06B7.init_LamHaaMemInit \uni0644.init_LamHaaMemInit \uni06B8.init_LamHaaMemInit \uni06B6.init_LamHaaMemInit \uni076A.init_LamHaaMemInit \uni063B.init_KafMemAlf \uni063C.init_KafMemAlf \uni077F.init_KafMemAlf \uni0764.init_KafMemAlf \uni0643.init_KafMemAlf \uni06B0.init_KafMemAlf \uni06B3.init_KafMemAlf \uni06B2.init_KafMemAlf \uni06AB.init_KafMemAlf \uni06AC.init_KafMemAlf \uni06AD.init_KafMemAlf \uni06AE.init_KafMemAlf \uni06AF.init_KafMemAlf \uni06A9.init_KafMemAlf \uni06B4.init_KafMemAlf \uni0763.init_KafMemAlf \uni0762.init_KafMemAlf \uni06B1.init_KafMemAlf ] <anchor 660 -670> mark @TashkilBelow;
  pos base [\aHaa.medi_LamHaaMemInit \aHaa.medi_FaaHaaInit \uni062E.medi_LamHaaMemInit \uni062D.medi_LamHaaMemInit \uni0681.medi_LamHaaMemInit \uni0687.medi_LamHaaMemInit \uni0685.medi_LamHaaMemInit \uni062C.medi_LamHaaMemInit \uni0682.medi_LamHaaMemInit \uni0757.medi_LamHaaMemInit \uni0684.medi_LamHaaMemInit \uni076F.medi_LamHaaMemInit \uni076E.medi_LamHaaMemInit \uni0683.medi_LamHaaMemInit \uni06BF.medi_LamHaaMemInit \uni0758.medi_LamHaaMemInit \uni0772.medi_LamHaaMemInit \uni0686.medi_LamHaaMemInit \uni062E.medi_FaaHaaInit \uni062D.medi_FaaHaaInit \uni0681.medi_FaaHaaInit \uni0687.medi_FaaHaaInit \uni0685.medi_FaaHaaInit \uni062C.medi_FaaHaaInit \uni0682.medi_FaaHaaInit \uni0757.medi_FaaHaaInit \uni0684.medi_FaaHaaInit \uni076E.medi_FaaHaaInit \uni0683.medi_FaaHaaInit \uni06BF.medi_FaaHaaInit \uni0758.medi_FaaHaaInit \uni0772.medi_FaaHaaInit \uni0686.medi_FaaHaaInit ] <anchor 261 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_BaaBaaInit \aMem.medi_BaaMemInit \aLam.init_LamHeh \aFaa.init_FaaMemInit \uni0680.medi_BaaBaaInit \uni0776.medi_BaaBaaInit \uni06BC.medi_BaaBaaInit \uni0750.medi_BaaBaaInit \uni0756.medi_BaaBaaInit \uni0768.medi_BaaBaaInit \uni06CE.medi_BaaBaaInit \uni0775.medi_BaaBaaInit \uni06BD.medi_BaaBaaInit \uni0626.medi_BaaBaaInit \uni066E.medi_BaaBaaInit \uni0620.medi_BaaBaaInit \uni064A.medi_BaaBaaInit \uni06BB.medi_BaaBaaInit \uni067F.medi_BaaBaaInit \uni067D.medi_BaaBaaInit \uni067E.medi_BaaBaaInit \uni067B.medi_BaaBaaInit \uni0628.medi_BaaBaaInit \uni067A.medi_BaaBaaInit \uni0751.medi_BaaBaaInit \uni0646.medi_BaaBaaInit \uni0753.medi_BaaBaaInit \uni0752.medi_BaaBaaInit \uni062A.medi_BaaBaaInit \uni0678.medi_BaaBaaInit \uni063D.medi_BaaBaaInit \uni062B.medi_BaaBaaInit \uni0679.medi_BaaBaaInit \uni06B9.medi_BaaBaaInit \uni0769.medi_BaaBaaInit \uni0649.medi_BaaBaaInit \uni067C.medi_BaaBaaInit \uni0754.medi_BaaBaaInit \uni06D1.medi_BaaBaaInit \uni06D0.medi_BaaBaaInit \uni06BA.medi_BaaBaaInit \uni06CC.medi_BaaBaaInit \uni0767.medi_BaaBaaInit \uni0645.medi_BaaMemInit \uni06B5.init_LamHeh \uni06B7.init_LamHeh \uni0644.init_LamHeh \uni06B6.init_LamHeh \uni076A.init_LamHeh \uni066F.init_FaaMemInit \uni0642.init_FaaMemInit \uni0641.init_FaaMemInit \uni06A8.init_FaaMemInit \uni06A1.init_FaaMemInit \uni06A4.init_FaaMemInit \uni06A6.init_FaaMemInit \uni06A7.init_FaaMemInit ] <anchor 100 -670> mark @TashkilBelow;
  pos base [\aMem.init_MemRaaIsol \uni0765.init_MemRaaIsol \uni0645.init_MemRaaIsol \uni0766.init_MemRaaIsol ] <anchor 230 -670> mark @TashkilBelow;
  pos base [\aFaa.init_FaaHaaInit \uni066F.init_FaaHaaInit \uni0761.init_FaaHaaInit \uni0760.init_FaaHaaInit \uni0642.init_FaaHaaInit \uni0641.init_FaaHaaInit \uni06A8.init_FaaHaaInit \uni06A1.init_FaaHaaInit \uni06A2.init_FaaHaaInit \uni06A3.init_FaaHaaInit \uni06A4.init_FaaHaaInit \uni06A5.init_FaaHaaInit \uni06A6.init_FaaHaaInit \uni06A7.init_FaaHaaInit ] <anchor 680 -670> mark @TashkilBelow;
  pos base [\aHaa.init_HaaHaaInit \uni062E.init_HaaHaaInit \uni062D.init_HaaHaaInit \uni0681.init_HaaHaaInit \uni0687.init_HaaHaaInit \uni0685.init_HaaHaaInit \uni062C.init_HaaHaaInit \uni0682.init_HaaHaaInit \uni0757.init_HaaHaaInit \uni0684.init_HaaHaaInit \uni076E.init_HaaHaaInit \uni0683.init_HaaHaaInit \uni06BF.init_HaaHaaInit \uni0758.init_HaaHaaInit \uni0772.init_HaaHaaInit \uni0686.init_HaaHaaInit ] <anchor 1007 -670> mark @TashkilBelow;
  pos base [\aSen.init_AboveHaa \uni06FA.init_AboveHaa \uni076D.init_AboveHaa \uni0633.init_AboveHaa \uni077E.init_AboveHaa \uni077D.init_AboveHaa \uni0634.init_AboveHaa \uni0770.init_AboveHaa \uni075C.init_AboveHaa \uni069A.init_AboveHaa ] <anchor 1030 -290> mark @TashkilBelow;
  pos base [\aMem.init_MemHaaInit \uni0645.init_MemHaaInit ] <anchor 820 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaNonIsol \uni0680.init_BaaNonIsol \uni0776.init_BaaNonIsol \uni06BC.init_BaaNonIsol \uni0750.init_BaaNonIsol \uni0756.init_BaaNonIsol \uni0768.init_BaaNonIsol \uni06CE.init_BaaNonIsol \uni0775.init_BaaNonIsol \uni06BD.init_BaaNonIsol \uni0626.init_BaaNonIsol \uni066E.init_BaaNonIsol \uni0620.init_BaaNonIsol \uni064A.init_BaaNonIsol \uni06BB.init_BaaNonIsol \uni067F.init_BaaNonIsol \uni067D.init_BaaNonIsol \uni067E.init_BaaNonIsol \uni067B.init_BaaNonIsol \uni0628.init_BaaNonIsol \uni067A.init_BaaNonIsol \uni0751.init_BaaNonIsol \uni0646.init_BaaNonIsol \uni0753.init_BaaNonIsol \uni0752.init_BaaNonIsol \uni062A.init_BaaNonIsol \uni0678.init_BaaNonIsol \uni063D.init_BaaNonIsol \uni062B.init_BaaNonIsol \uni0679.init_BaaNonIsol \uni06B9.init_BaaNonIsol \uni0769.init_BaaNonIsol \uni0649.init_BaaNonIsol \uni067C.init_BaaNonIsol \uni0754.init_BaaNonIsol \uni06D1.init_BaaNonIsol \uni06D0.init_BaaNonIsol \uni06BA.init_BaaNonIsol \uni06CC.init_BaaNonIsol \uni0767.init_BaaNonIsol ] <anchor 212 -670> mark @TashkilBelow;
  pos base [\aNon.fina_BaaNonIsol \aSen.medi_BaaSenInit \aRaa.fina_LamRaaIsol \uni0646.fina_BaaNonIsol \uni06BA.fina_BaaNonIsol \uni06BC.fina_BaaNonIsol \uni06BB.fina_BaaNonIsol \uni0768.fina_BaaNonIsol \uni0769.fina_BaaNonIsol \uni06BD.fina_BaaNonIsol \uni06FA.medi_BaaSenInit \uni076D.medi_BaaSenInit \uni0633.medi_BaaSenInit \uni077E.medi_BaaSenInit \uni077D.medi_BaaSenInit \uni0634.medi_BaaSenInit \uni0770.medi_BaaSenInit \uni075C.medi_BaaSenInit \uni069A.medi_BaaSenInit \uni069B.medi_BaaSenInit \uni069C.medi_BaaSenInit \uni06BE.fina \uni06FF.fina \aHehKnotted.fina ] <anchor 670 -670> mark @TashkilBelow;
  pos base [\aMem.fina_KafMemFina \uni0645.fina_KafMemFina \uni0691.fina_BaaRaaFina \uni0692.fina_BaaRaaFina \uni0693.fina_BaaRaaFina \uni0697.fina_BaaRaaFina \uni0698.fina_BaaRaaFina \uni0699.fina_BaaRaaFina \uni075B.fina_BaaRaaFina \uni06EF.fina_BaaRaaFina \uni0632.fina_BaaRaaFina \uni0771.fina_BaaRaaFina \uni0631.fina_BaaRaaFina \uni076B.fina_BaaRaaFina \uni076C.fina_BaaRaaFina \uni0691.fina_PostTooth \uni0692.fina_PostTooth \uni0693.fina_PostTooth \uni0697.fina_PostTooth \uni0698.fina_PostTooth \uni0699.fina_PostTooth \uni075B.fina_PostTooth \uni06EF.fina_PostTooth \uni0632.fina_PostTooth \uni0771.fina_PostTooth \uni0631.fina_PostTooth \uni076B.fina_PostTooth \uni076C.fina_PostTooth ] <anchor 470 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaSenInit \uni0680.init_BaaSenInit \uni0776.init_BaaSenInit \uni06BC.init_BaaSenInit \uni0750.init_BaaSenInit \uni0756.init_BaaSenInit \uni0768.init_BaaSenInit \uni06CE.init_BaaSenInit \uni0775.init_BaaSenInit \uni06BD.init_BaaSenInit \uni0626.init_BaaSenInit \uni066E.init_BaaSenInit \uni0620.init_BaaSenInit \uni064A.init_BaaSenInit \uni06BB.init_BaaSenInit \uni067F.init_BaaSenInit \uni0755.init_BaaSenInit \uni067D.init_BaaSenInit \uni067E.init_BaaSenInit \uni067B.init_BaaSenInit \uni0628.init_BaaSenInit \uni067A.init_BaaSenInit \uni0751.init_BaaSenInit \uni0646.init_BaaSenInit \uni0753.init_BaaSenInit \uni0752.init_BaaSenInit \uni062A.init_BaaSenInit \uni0678.init_BaaSenInit \uni063D.init_BaaSenInit \uni062B.init_BaaSenInit \uni0679.init_BaaSenInit \uni06B9.init_BaaSenInit \uni0769.init_BaaSenInit \uni0649.init_BaaSenInit \uni067C.init_BaaSenInit \uni0754.init_BaaSenInit \uni06D1.init_BaaSenInit \uni06D0.init_BaaSenInit \uni06BA.init_BaaSenInit \uni06CC.init_BaaSenInit \uni0767.init_BaaSenInit \uni08A0.init_BaaSenInit ] <anchor 296 -670> mark @TashkilBelow;
  pos base [\aRaa.fina_BaaRaaFina \aRaa.fina_PostTooth ] <anchor 770 -670> mark @TashkilBelow;
  pos base [\aRaa.fina_KafRaaFina ] <anchor 730 -670> mark @TashkilBelow;
  pos base [\aLam.init_LamHehInit \uni06B5.init_LamHehInit \uni06B7.init_LamHehInit \uni0644.init_LamHehInit \uni06B8.init_LamHehInit \uni06B6.init_LamHehInit \uni076A.init_LamHehInit ] <anchor 180 -670> mark @TashkilBelow;
  pos base [\aMem.init_MemHaaMemInit \uni0645.init_MemHaaMemInit ] <anchor 311 -670> mark @TashkilBelow;
  pos base [\aHaa.medi_MemHaaMemInit \uni062E.medi_MemHaaMemInit \uni062D.medi_MemHaaMemInit \uni0681.medi_MemHaaMemInit \uni0687.medi_MemHaaMemInit \uni0685.medi_MemHaaMemInit \uni062C.medi_MemHaaMemInit \uni0682.medi_MemHaaMemInit \uni0757.medi_MemHaaMemInit \uni0684.medi_MemHaaMemInit \uni076F.medi_MemHaaMemInit \uni076E.medi_MemHaaMemInit \uni0683.medi_MemHaaMemInit \uni06BF.medi_MemHaaMemInit \uni0758.medi_MemHaaMemInit \uni0772.medi_MemHaaMemInit \uni0686.medi_MemHaaMemInit ] <anchor 318 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaMemInit \uni0776.init_BaaMemInit \uni06BC.init_BaaMemInit \uni0750.init_BaaMemInit \uni0756.init_BaaMemInit \uni0768.init_BaaMemInit \uni06CE.init_BaaMemInit \uni0775.init_BaaMemInit \uni0626.init_BaaMemInit \uni066E.init_BaaMemInit \uni0620.init_BaaMemInit \uni064A.init_BaaMemInit \uni06BB.init_BaaMemInit \uni067F.init_BaaMemInit \uni067D.init_BaaMemInit \uni0628.init_BaaMemInit \uni067A.init_BaaMemInit \uni0751.init_BaaMemInit \uni0646.init_BaaMemInit \uni062A.init_BaaMemInit \uni0678.init_BaaMemInit \uni063D.init_BaaMemInit \uni062B.init_BaaMemInit \uni0679.init_BaaMemInit \uni06B9.init_BaaMemInit \uni0769.init_BaaMemInit \uni0649.init_BaaMemInit \uni067C.init_BaaMemInit \uni0754.init_BaaMemInit \uni06BA.init_BaaMemInit \uni06CC.init_BaaMemInit \uni0767.init_BaaMemInit ] <anchor 292 -670> mark @TashkilBelow;
  pos base [\aSen.init_SenHaaInit \uni06FA.init_SenHaaInit \uni076D.init_SenHaaInit \uni0633.init_SenHaaInit \uni077E.init_SenHaaInit \uni077D.init_SenHaaInit \uni0634.init_SenHaaInit \uni0770.init_SenHaaInit \uni075C.init_SenHaaInit \uni069A.init_SenHaaInit \uni069B.init_SenHaaInit \uni069C.init_SenHaaInit ] <anchor 930 -670> mark @TashkilBelow;
  pos base [\aRaa.fina_KafRaaIsol \aLam.init_LamHaaHaaInit \aKaf.fina_LamKafFina \uni06B5.init_LamHaaHaaInit \uni06B7.init_LamHaaHaaInit \uni0644.init_LamHaaHaaInit \uni06B6.init_LamHaaHaaInit \uni076A.init_LamHaaHaaInit \uni063B.fina_LamKafFina \uni063C.fina_LamKafFina \uni077F.fina_LamKafFina \uni0764.fina_LamKafFina \uni0643.fina_LamKafFina \uni06B0.fina_LamKafFina \uni06B3.fina_LamKafFina \uni06B2.fina_LamKafFina \uni06AB.fina_LamKafFina \uni06AC.fina_LamKafFina \uni06AD.fina_LamKafFina \uni06AE.fina_LamKafFina \uni06AF.fina_LamKafFina \uni06A9.fina_LamKafFina \uni06B4.fina_LamKafFina \uni0763.fina_LamKafFina \uni0762.fina_LamKafFina \uni06B1.fina_LamKafFina ] <anchor 750 -670> mark @TashkilBelow;
  pos base [\aYaa.fina_KafYaaFina \uni0775.fina_KafYaaFina \uni063F.fina_KafYaaFina \uni0678.fina_KafYaaFina \uni063D.fina_KafYaaFina \uni063E.fina_KafYaaFina \uni0649.fina_KafYaaFina \uni0776.fina_KafYaaFina \uni06CD.fina_KafYaaFina \uni06CC.fina_KafYaaFina \uni0626.fina_KafYaaFina \uni06CE.fina_KafYaaFina ] <anchor 739 -670> mark @TashkilBelow;
  pos base [\aLam.init_LamMemHaaInit \uni06B5.init_LamMemHaaInit \uni06B7.init_LamMemHaaInit \uni0644.init_LamMemHaaInit \uni06B8.init_LamMemHaaInit \uni06B6.init_LamMemHaaInit \uni076A.init_LamMemHaaInit ] <anchor 420 -670> mark @TashkilBelow;
  pos base [\aMem.medi_LamMemHaaInit \uni0645.medi_LamMemHaaInit ] <anchor 520 -670> mark @TashkilBelow;
  pos base [\aHaa.medi_LamMemHaaInit \uni062E.medi_LamMemHaaInit \uni062D.medi_LamMemHaaInit \uni0681.medi_LamMemHaaInit \uni0687.medi_LamMemHaaInit \uni0685.medi_LamMemHaaInit \uni062C.medi_LamMemHaaInit \uni0682.medi_LamMemHaaInit \uni0757.medi_LamMemHaaInit \uni0684.medi_LamMemHaaInit \uni076E.medi_LamMemHaaInit \uni0683.medi_LamMemHaaInit \uni06BF.medi_LamMemHaaInit \uni0758.medi_LamMemHaaInit \uni0772.medi_LamMemHaaInit \uni0686.medi_LamMemHaaInit ] <anchor 203 -670> mark @TashkilBelow;
  pos base [\aAlf.fina_LamAlfFina \uni0627.fina_LamAlfFina \uni0623.fina_LamAlfFina \uni0675.fina_LamAlfFina \uni0672.fina_LamAlfFina ] <anchor 320 -670> mark @TashkilBelow;
  pos base [\aMem.medi_LamMemMedi \aBaa.init_High \aLam.medi_KafLamYaa \aLam.medi_LamLamYaaIsol \aLam.medi_LamLamYaaFina \uni0765.medi_LamMemMedi \uni0645.medi_LamMemMedi \uni0766.medi_LamMemMedi \uni0680.init_High \uni0776.init_High \uni06BC.init_High \uni0750.init_High \uni0756.init_High \uni0768.init_High \uni06CE.init_High \uni0775.init_High \uni06BD.init_High \uni0626.init_High \uni066E.init_High \uni0620.init_High \uni064A.init_High \uni06BB.init_High \uni067F.init_High \uni067D.init_High \uni067E.init_High \uni0628.init_High \uni067A.init_High \uni0751.init_High \uni0646.init_High \uni0753.init_High \uni0752.init_High \uni062A.init_High \uni0678.init_High \uni063D.init_High \uni062B.init_High \uni0679.init_High \uni06B9.init_High \uni0769.init_High \uni0649.init_High \uni067C.init_High \uni0754.init_High \uni06D1.init_High \uni06BA.init_High \uni06CC.init_High \uni0767.init_High \uni06B5.medi_KafLamYaa \uni06B7.medi_KafLamYaa \uni0644.medi_KafLamYaa \uni06B6.medi_KafLamYaa \uni076A.medi_KafLamYaa \uni06B5.medi_LamLamYaaIsol \uni06B7.medi_LamLamYaaIsol \uni0644.medi_LamLamYaaIsol \uni06B6.medi_LamLamYaaIsol \uni076A.medi_LamLamYaaIsol \uni06B5.medi_LamLamYaaFina \uni06B7.medi_LamLamYaaFina \uni0644.medi_LamLamYaaFina \uni06B6.medi_LamLamYaaFina \uni076A.medi_LamLamYaaFina ] <anchor 220 -670> mark @TashkilBelow;
  pos base [\uni0644.init_Lellah \uni0644.medi_FaLellah ] <anchor 100 -300> mark @TashkilBelow;
  pos base [\uni0644.medi_Lellah ] <anchor 140 -300> mark @TashkilBelow;
  pos base [\uni0647.fina_Lellah ] <anchor 380 -300> mark @TashkilBelow;
  pos base [\aBaa.init_BaaBaaHaaInit \uni0680.init_BaaBaaHaaInit \uni0776.init_BaaBaaHaaInit \uni06BC.init_BaaBaaHaaInit \uni0750.init_BaaBaaHaaInit \uni0756.init_BaaBaaHaaInit \uni0768.init_BaaBaaHaaInit \uni06CE.init_BaaBaaHaaInit \uni0775.init_BaaBaaHaaInit \uni06BD.init_BaaBaaHaaInit \uni0626.init_BaaBaaHaaInit \uni066E.init_BaaBaaHaaInit \uni0620.init_BaaBaaHaaInit \uni064A.init_BaaBaaHaaInit \uni06BB.init_BaaBaaHaaInit \uni067F.init_BaaBaaHaaInit \uni0755.init_BaaBaaHaaInit \uni067D.init_BaaBaaHaaInit \uni067E.init_BaaBaaHaaInit \uni067B.init_BaaBaaHaaInit \uni0628.init_BaaBaaHaaInit \uni067A.init_BaaBaaHaaInit \uni0751.init_BaaBaaHaaInit \uni0646.init_BaaBaaHaaInit \uni0753.init_BaaBaaHaaInit \uni0752.init_BaaBaaHaaInit \uni062A.init_BaaBaaHaaInit \uni0678.init_BaaBaaHaaInit \uni063D.init_BaaBaaHaaInit \uni062B.init_BaaBaaHaaInit \uni0679.init_BaaBaaHaaInit \uni06B9.init_BaaBaaHaaInit \uni0769.init_BaaBaaHaaInit \uni0649.init_BaaBaaHaaInit \uni067C.init_BaaBaaHaaInit \uni0754.init_BaaBaaHaaInit \uni06D1.init_BaaBaaHaaInit \uni06D0.init_BaaBaaHaaInit \uni06BA.init_BaaBaaHaaInit \uni06CC.init_BaaBaaHaaInit \uni0767.init_BaaBaaHaaInit \uni08A0.init_BaaBaaHaaInit ] <anchor 95 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_BaaBaaHaaInit \uni0776.medi_BaaBaaHaaInit \uni06BC.medi_BaaBaaHaaInit \uni0750.medi_BaaBaaHaaInit \uni0756.medi_BaaBaaHaaInit \uni0768.medi_BaaBaaHaaInit \uni06CE.medi_BaaBaaHaaInit \uni0775.medi_BaaBaaHaaInit \uni0626.medi_BaaBaaHaaInit \uni066E.medi_BaaBaaHaaInit \uni0620.medi_BaaBaaHaaInit \uni064A.medi_BaaBaaHaaInit \uni06BB.medi_BaaBaaHaaInit \uni067F.medi_BaaBaaHaaInit \uni067D.medi_BaaBaaHaaInit \uni0628.medi_BaaBaaHaaInit \uni067A.medi_BaaBaaHaaInit \uni0751.medi_BaaBaaHaaInit \uni0646.medi_BaaBaaHaaInit \uni062A.medi_BaaBaaHaaInit \uni0678.medi_BaaBaaHaaInit \uni063D.medi_BaaBaaHaaInit \uni062B.medi_BaaBaaHaaInit \uni0679.medi_BaaBaaHaaInit \uni06B9.medi_BaaBaaHaaInit \uni0769.medi_BaaBaaHaaInit \uni0649.medi_BaaBaaHaaInit \uni067C.medi_BaaBaaHaaInit \uni0754.medi_BaaBaaHaaInit \uni06BA.medi_BaaBaaHaaInit \uni06CC.medi_BaaBaaHaaInit \uni0767.medi_BaaBaaHaaInit ] <anchor 599 -670> mark @TashkilBelow;
  pos base [\aHaa.medi_BaaBaaHaaInit \uni062E.medi_BaaBaaHaaInit \uni062D.medi_BaaBaaHaaInit \uni0681.medi_BaaBaaHaaInit \uni0687.medi_BaaBaaHaaInit \uni0685.medi_BaaBaaHaaInit \uni062C.medi_BaaBaaHaaInit \uni0682.medi_BaaBaaHaaInit \uni0757.medi_BaaBaaHaaInit \uni0684.medi_BaaBaaHaaInit \uni076E.medi_BaaBaaHaaInit \uni0683.medi_BaaBaaHaaInit \uni06BF.medi_BaaBaaHaaInit \uni0758.medi_BaaBaaHaaInit \uni0772.medi_BaaBaaHaaInit \uni0686.medi_BaaBaaHaaInit ] <anchor 346 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_SenBaaMemInit \uni0680.medi_SenBaaMemInit \uni0776.medi_SenBaaMemInit \uni06BC.medi_SenBaaMemInit \uni0750.medi_SenBaaMemInit \uni0756.medi_SenBaaMemInit \uni0768.medi_SenBaaMemInit \uni06CE.medi_SenBaaMemInit \uni0775.medi_SenBaaMemInit \uni06BD.medi_SenBaaMemInit \uni0626.medi_SenBaaMemInit \uni066E.medi_SenBaaMemInit \uni0620.medi_SenBaaMemInit \uni064A.medi_SenBaaMemInit \uni06BB.medi_SenBaaMemInit \uni067F.medi_SenBaaMemInit \uni0755.medi_SenBaaMemInit \uni067D.medi_SenBaaMemInit \uni067E.medi_SenBaaMemInit \uni067B.medi_SenBaaMemInit \uni0628.medi_SenBaaMemInit \uni067A.medi_SenBaaMemInit \uni0751.medi_SenBaaMemInit \uni0646.medi_SenBaaMemInit \uni0753.medi_SenBaaMemInit \uni0752.medi_SenBaaMemInit \uni062A.medi_SenBaaMemInit \uni0678.medi_SenBaaMemInit \uni063D.medi_SenBaaMemInit \uni062B.medi_SenBaaMemInit \uni0679.medi_SenBaaMemInit \uni06B9.medi_SenBaaMemInit \uni0769.medi_SenBaaMemInit \uni0649.medi_SenBaaMemInit \uni067C.medi_SenBaaMemInit \uni0754.medi_SenBaaMemInit \uni06D1.medi_SenBaaMemInit \uni06D0.medi_SenBaaMemInit \uni06BA.medi_SenBaaMemInit \uni06CC.medi_SenBaaMemInit \uni0767.medi_SenBaaMemInit \uni08A0.medi_SenBaaMemInit ] <anchor 478 -670> mark @TashkilBelow;
  pos base [\aMem.medi_SenBaaMemInit \uni0645.medi_SenBaaMemInit \uni0645.medi_KafMemMediTatweel ] <anchor 260 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaBaaIsol \aLam.medi_KafLam \aMem.medi_KafMemAlf \aLam.medi_KafMemLam \uni0776.init_BaaBaaIsol \uni06BC.init_BaaBaaIsol \uni0750.init_BaaBaaIsol \uni0756.init_BaaBaaIsol \uni0768.init_BaaBaaIsol \uni06CE.init_BaaBaaIsol \uni0775.init_BaaBaaIsol \uni0626.init_BaaBaaIsol \uni066E.init_BaaBaaIsol \uni0620.init_BaaBaaIsol \uni064A.init_BaaBaaIsol \uni06BB.init_BaaBaaIsol \uni067F.init_BaaBaaIsol \uni0755.init_BaaBaaIsol \uni067D.init_BaaBaaIsol \uni0628.init_BaaBaaIsol \uni067A.init_BaaBaaIsol \uni0751.init_BaaBaaIsol \uni0646.init_BaaBaaIsol \uni062A.init_BaaBaaIsol \uni0678.init_BaaBaaIsol \uni063D.init_BaaBaaIsol \uni062B.init_BaaBaaIsol \uni0679.init_BaaBaaIsol \uni06B9.init_BaaBaaIsol \uni0769.init_BaaBaaIsol \uni0649.init_BaaBaaIsol \uni067C.init_BaaBaaIsol \uni0754.init_BaaBaaIsol \uni06BA.init_BaaBaaIsol \uni06CC.init_BaaBaaIsol \uni0767.init_BaaBaaIsol \uni06B5.medi_KafLam \uni06B7.medi_KafLam \uni0644.medi_KafLam \uni06B8.medi_KafLam \uni06B6.medi_KafLam \uni076A.medi_KafLam \uni0645.medi_KafMemAlf \uni06B5.medi_KafMemLam \uni06B7.medi_KafMemLam \uni0644.medi_KafMemLam \uni06B8.medi_KafMemLam \uni06B6.medi_KafMemLam \uni076A.medi_KafMemLam ] <anchor 150 -670> mark @TashkilBelow;
  pos base [\aBaa.fina_BaaBaaIsol \uni0751.fina_BaaBaaIsol \uni062A.fina_BaaBaaIsol \uni0754.fina_BaaBaaIsol \uni062B.fina_BaaBaaIsol \uni0679.fina_BaaBaaIsol \uni067C.fina_BaaBaaIsol \uni0756.fina_BaaBaaIsol \uni066E.fina_BaaBaaIsol \uni067F.fina_BaaBaaIsol \uni067D.fina_BaaBaaIsol \uni0628.fina_BaaBaaIsol \uni067A.fina_BaaBaaIsol ] <anchor 950 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaBaaMemInit \uni0680.init_BaaBaaMemInit \uni0776.init_BaaBaaMemInit \uni06BC.init_BaaBaaMemInit \uni0750.init_BaaBaaMemInit \uni0756.init_BaaBaaMemInit \uni0768.init_BaaBaaMemInit \uni06CE.init_BaaBaaMemInit \uni0775.init_BaaBaaMemInit \uni06BD.init_BaaBaaMemInit \uni0626.init_BaaBaaMemInit \uni066E.init_BaaBaaMemInit \uni0620.init_BaaBaaMemInit \uni064A.init_BaaBaaMemInit \uni06BB.init_BaaBaaMemInit \uni067F.init_BaaBaaMemInit \uni0755.init_BaaBaaMemInit \uni067D.init_BaaBaaMemInit \uni067E.init_BaaBaaMemInit \uni067B.init_BaaBaaMemInit \uni0628.init_BaaBaaMemInit \uni067A.init_BaaBaaMemInit \uni0751.init_BaaBaaMemInit \uni0646.init_BaaBaaMemInit \uni0753.init_BaaBaaMemInit \uni0752.init_BaaBaaMemInit \uni062A.init_BaaBaaMemInit \uni0678.init_BaaBaaMemInit \uni063D.init_BaaBaaMemInit \uni062B.init_BaaBaaMemInit \uni0679.init_BaaBaaMemInit \uni06B9.init_BaaBaaMemInit \uni0769.init_BaaBaaMemInit \uni0649.init_BaaBaaMemInit \uni067C.init_BaaBaaMemInit \uni0754.init_BaaBaaMemInit \uni06D1.init_BaaBaaMemInit \uni06D0.init_BaaBaaMemInit \uni06BA.init_BaaBaaMemInit \uni06CC.init_BaaBaaMemInit \uni0767.init_BaaBaaMemInit \uni08A0.init_BaaBaaMemInit ] <anchor 177 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_BaaBaaMemInit \uni0680.medi_BaaBaaMemInit \uni0776.medi_BaaBaaMemInit \uni06BC.medi_BaaBaaMemInit \uni0750.medi_BaaBaaMemInit \uni0756.medi_BaaBaaMemInit \uni0768.medi_BaaBaaMemInit \uni06CE.medi_BaaBaaMemInit \uni0775.medi_BaaBaaMemInit \uni0626.medi_BaaBaaMemInit \uni066E.medi_BaaBaaMemInit \uni0620.medi_BaaBaaMemInit \uni064A.medi_BaaBaaMemInit \uni06BB.medi_BaaBaaMemInit \uni067F.medi_BaaBaaMemInit \uni067D.medi_BaaBaaMemInit \uni0628.medi_BaaBaaMemInit \uni067A.medi_BaaBaaMemInit \uni0751.medi_BaaBaaMemInit \uni0646.medi_BaaBaaMemInit \uni062A.medi_BaaBaaMemInit \uni0678.medi_BaaBaaMemInit \uni063D.medi_BaaBaaMemInit \uni062B.medi_BaaBaaMemInit \uni0679.medi_BaaBaaMemInit \uni06B9.medi_BaaBaaMemInit \uni0769.medi_BaaBaaMemInit \uni0649.medi_BaaBaaMemInit \uni067C.medi_BaaBaaMemInit \uni0754.medi_BaaBaaMemInit \uni06BA.medi_BaaBaaMemInit \uni06CC.medi_BaaBaaMemInit \uni0767.medi_BaaBaaMemInit ] <anchor 76 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_KafBaaMedi \uni0680.medi_KafBaaMedi \uni0776.medi_KafBaaMedi \uni06BC.medi_KafBaaMedi \uni0750.medi_KafBaaMedi \uni0756.medi_KafBaaMedi \uni0768.medi_KafBaaMedi \uni06CE.medi_KafBaaMedi \uni0775.medi_KafBaaMedi \uni06BD.medi_KafBaaMedi \uni0626.medi_KafBaaMedi \uni066E.medi_KafBaaMedi \uni0620.medi_KafBaaMedi \uni064A.medi_KafBaaMedi \uni06BB.medi_KafBaaMedi \uni067F.medi_KafBaaMedi \uni0755.medi_KafBaaMedi \uni067D.medi_KafBaaMedi \uni067E.medi_KafBaaMedi \uni0628.medi_KafBaaMedi \uni067A.medi_KafBaaMedi \uni0751.medi_KafBaaMedi \uni0646.medi_KafBaaMedi \uni0753.medi_KafBaaMedi \uni0752.medi_KafBaaMedi \uni062A.medi_KafBaaMedi \uni0678.medi_KafBaaMedi \uni063D.medi_KafBaaMedi \uni062B.medi_KafBaaMedi \uni0679.medi_KafBaaMedi \uni06B9.medi_KafBaaMedi \uni0769.medi_KafBaaMedi \uni0649.medi_KafBaaMedi \uni067C.medi_KafBaaMedi \uni0754.medi_KafBaaMedi \uni06D1.medi_KafBaaMedi \uni06BA.medi_KafBaaMedi \uni06CC.medi_KafBaaMedi \uni0767.medi_KafBaaMedi \uni0774.fina_KafAlf \uni0773.fina_KafAlf ] <anchor 210 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_BaaNonFina \uni0680.medi_BaaNonFina \uni0776.medi_BaaNonFina \uni06BC.medi_BaaNonFina \uni0750.medi_BaaNonFina \uni0756.medi_BaaNonFina \uni0768.medi_BaaNonFina \uni06CE.medi_BaaNonFina \uni0775.medi_BaaNonFina \uni0626.medi_BaaNonFina \uni066E.medi_BaaNonFina \uni0620.medi_BaaNonFina \uni064A.medi_BaaNonFina \uni06BB.medi_BaaNonFina \uni067F.medi_BaaNonFina \uni067D.medi_BaaNonFina \uni0628.medi_BaaNonFina \uni067A.medi_BaaNonFina \uni0751.medi_BaaNonFina \uni0646.medi_BaaNonFina \uni062A.medi_BaaNonFina \uni0678.medi_BaaNonFina \uni063D.medi_BaaNonFina \uni062B.medi_BaaNonFina \uni0679.medi_BaaNonFina \uni06B9.medi_BaaNonFina \uni0769.medi_BaaNonFina \uni0649.medi_BaaNonFina \uni067C.medi_BaaNonFina \uni0754.medi_BaaNonFina \uni06BA.medi_BaaNonFina \uni06CC.medi_BaaNonFina \uni0767.medi_BaaNonFina ] <anchor 39 -670> mark @TashkilBelow;
  pos base [\aHaa.init_HaaRaaIsol \uni062E.init_HaaRaaIsol \uni062D.init_HaaRaaIsol \uni0681.init_HaaRaaIsol \uni0687.init_HaaRaaIsol \uni0685.init_HaaRaaIsol \uni062C.init_HaaRaaIsol \uni0682.init_HaaRaaIsol \uni0757.init_HaaRaaIsol \uni0684.init_HaaRaaIsol \uni076E.init_HaaRaaIsol \uni0683.init_HaaRaaIsol \uni06BF.init_HaaRaaIsol \uni0758.init_HaaRaaIsol \uni0772.init_HaaRaaIsol \uni0686.init_HaaRaaIsol ] <anchor 589 -670> mark @TashkilBelow;
  pos base [\aHeh.init_HehHaaInit \uni0647.init_HehHaaInit \uni06C1.init_HehHaaInit ] <anchor 920 -670> mark @TashkilBelow;
  pos base [\aLam.init_LamRaaIsol \aHaa.medi_2LamHaaHaaInit \uni06B5.init_LamRaaIsol \uni06B7.init_LamRaaIsol \uni0644.init_LamRaaIsol \uni06B8.init_LamRaaIsol \uni06B6.init_LamRaaIsol \uni076A.init_LamRaaIsol \uni062E.medi_2LamHaaHaaInit \uni062D.medi_2LamHaaHaaInit \uni0681.medi_2LamHaaHaaInit \uni0687.medi_2LamHaaHaaInit \uni0685.medi_2LamHaaHaaInit \uni062C.medi_2LamHaaHaaInit \uni0682.medi_2LamHaaHaaInit \uni0757.medi_2LamHaaHaaInit \uni0684.medi_2LamHaaHaaInit \uni076E.medi_2LamHaaHaaInit \uni0683.medi_2LamHaaHaaInit \uni0772.medi_2LamHaaHaaInit ] <anchor 160 -670> mark @TashkilBelow;
  pos base [\aSad.init_SadHaaInit \uni069D.init_SadHaaInit \uni06FB.init_SadHaaInit \uni0636.init_SadHaaInit \uni069E.init_SadHaaInit \uni0635.init_SadHaaInit ] <anchor 1020 -670> mark @TashkilBelow;
  pos base [\aHaa.medi_SadHaaInit \uni062E.medi_SadHaaInit \uni062D.medi_SadHaaInit \uni0681.medi_SadHaaInit \uni0687.medi_SadHaaInit \uni0685.medi_SadHaaInit \uni062C.medi_SadHaaInit \uni0682.medi_SadHaaInit \uni0757.medi_SadHaaInit \uni0684.medi_SadHaaInit \uni076E.medi_SadHaaInit \uni0683.medi_SadHaaInit \uni06BF.medi_SadHaaInit \uni0758.medi_SadHaaInit \uni0772.medi_SadHaaInit \uni0686.medi_SadHaaInit ] <anchor 340 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_BaaYaaFina \uni06BC.medi_BaaYaaFina \uni0756.medi_BaaYaaFina \uni0768.medi_BaaYaaFina \uni0626.medi_BaaYaaFina \uni066E.medi_BaaYaaFina \uni06BB.medi_BaaYaaFina \uni067F.medi_BaaYaaFina \uni067D.medi_BaaYaaFina \uni0628.medi_BaaYaaFina \uni067A.medi_BaaYaaFina \uni0751.medi_BaaYaaFina \uni0646.medi_BaaYaaFina \uni062A.medi_BaaYaaFina \uni0678.medi_BaaYaaFina \uni062B.medi_BaaYaaFina \uni0679.medi_BaaYaaFina \uni06B9.medi_BaaYaaFina \uni0769.medi_BaaYaaFina \uni0649.medi_BaaYaaFina \uni067C.medi_BaaYaaFina \uni06BA.medi_BaaYaaFina ] <anchor 450 -940> mark @TashkilBelow;
  pos base [\aYaa.fina_BaaYaaFina \aYaa.fina_PostTooth \uni0775.fina_BaaYaaFina \uni063F.fina_BaaYaaFina \uni0678.fina_BaaYaaFina \uni063D.fina_BaaYaaFina \uni063E.fina_BaaYaaFina \uni0649.fina_BaaYaaFina \uni0776.fina_BaaYaaFina \uni06CD.fina_BaaYaaFina \uni06CC.fina_BaaYaaFina \uni0626.fina_BaaYaaFina \uni06CE.fina_BaaYaaFina \uni0775.fina_PostTooth \uni063F.fina_PostTooth \uni0678.fina_PostTooth \uni063D.fina_PostTooth \uni063E.fina_PostTooth \uni0649.fina_PostTooth \uni0776.fina_PostTooth \uni06CD.fina_PostTooth \uni06CC.fina_PostTooth \uni0626.fina_PostTooth \uni06CE.fina_PostTooth ] <anchor 754 -740> mark @TashkilBelow;
  pos base [\aBaa.init_BaaSenAltInit \uni0777.init_BaaSenAltInit \uni0680.init_BaaSenAltInit \uni0776.init_BaaSenAltInit \uni06BC.init_BaaSenAltInit \uni0750.init_BaaSenAltInit \uni0756.init_BaaSenAltInit \uni0768.init_BaaSenAltInit \uni06CE.init_BaaSenAltInit \uni0775.init_BaaSenAltInit \uni0626.init_BaaSenAltInit \uni066E.init_BaaSenAltInit \uni0620.init_BaaSenAltInit \uni064A.init_BaaSenAltInit \uni06BB.init_BaaSenAltInit \uni067F.init_BaaSenAltInit \uni0755.init_BaaSenAltInit \uni067D.init_BaaSenAltInit \uni0628.init_BaaSenAltInit \uni067A.init_BaaSenAltInit \uni0751.init_BaaSenAltInit \uni0646.init_BaaSenAltInit \uni062A.init_BaaSenAltInit \uni0678.init_BaaSenAltInit \uni063D.init_BaaSenAltInit \uni062B.init_BaaSenAltInit \uni0679.init_BaaSenAltInit \uni06B9.init_BaaSenAltInit \uni0769.init_BaaSenAltInit \uni0649.init_BaaSenAltInit \uni067C.init_BaaSenAltInit \uni0754.init_BaaSenAltInit \uni06BA.init_BaaSenAltInit \uni06CC.init_BaaSenAltInit \uni0767.init_BaaSenAltInit ] <anchor 262 -670> mark @TashkilBelow;
  pos base [\aBaa.init_AboveHaa \uni0680.init_AboveHaa \uni0776.init_AboveHaa \uni06BC.init_AboveHaa \uni0750.init_AboveHaa \uni0756.init_AboveHaa \uni0768.init_AboveHaa \uni06CE.init_AboveHaa \uni0775.init_AboveHaa \uni06BD.init_AboveHaa \uni0626.init_AboveHaa \uni066E.init_AboveHaa \uni0620.init_AboveHaa \uni064A.init_AboveHaa \uni06BB.init_AboveHaa \uni067F.init_AboveHaa \uni0755.init_AboveHaa \uni067D.init_AboveHaa \uni067E.init_AboveHaa \uni067B.init_AboveHaa \uni0628.init_AboveHaa \uni067A.init_AboveHaa \uni0751.init_AboveHaa \uni0646.init_AboveHaa \uni0753.init_AboveHaa \uni0752.init_AboveHaa \uni062A.init_AboveHaa \uni0678.init_AboveHaa \uni063D.init_AboveHaa \uni062B.init_AboveHaa \uni0679.init_AboveHaa \uni06B9.init_AboveHaa \uni0769.init_AboveHaa \uni0649.init_AboveHaa \uni067C.init_AboveHaa \uni0754.init_AboveHaa \uni06D1.init_AboveHaa \uni06D0.init_AboveHaa \uni06BA.init_AboveHaa \uni06CC.init_AboveHaa \uni0767.init_AboveHaa \uni08A0.init_AboveHaa ] <anchor 974 -410> mark @TashkilBelow;
  pos base [\aBaa.init_BaaHaaInit ] <anchor 980 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaHaaMemInit \uni0680.init_BaaHaaMemInit \uni0776.init_BaaHaaMemInit \uni06BC.init_BaaHaaMemInit \uni0750.init_BaaHaaMemInit \uni0756.init_BaaHaaMemInit \uni0768.init_BaaHaaMemInit \uni06CE.init_BaaHaaMemInit \uni0775.init_BaaHaaMemInit \uni06BD.init_BaaHaaMemInit \uni0626.init_BaaHaaMemInit \uni066E.init_BaaHaaMemInit \uni0620.init_BaaHaaMemInit \uni064A.init_BaaHaaMemInit \uni06BB.init_BaaHaaMemInit \uni067F.init_BaaHaaMemInit \uni0755.init_BaaHaaMemInit \uni067D.init_BaaHaaMemInit \uni067E.init_BaaHaaMemInit \uni067B.init_BaaHaaMemInit \uni0628.init_BaaHaaMemInit \uni067A.init_BaaHaaMemInit \uni0751.init_BaaHaaMemInit \uni0646.init_BaaHaaMemInit \uni0753.init_BaaHaaMemInit \uni0752.init_BaaHaaMemInit \uni062A.init_BaaHaaMemInit \uni0678.init_BaaHaaMemInit \uni063D.init_BaaHaaMemInit \uni062B.init_BaaHaaMemInit \uni0679.init_BaaHaaMemInit \uni06B9.init_BaaHaaMemInit \uni0769.init_BaaHaaMemInit \uni0649.init_BaaHaaMemInit \uni067C.init_BaaHaaMemInit \uni0754.init_BaaHaaMemInit \uni06D1.init_BaaHaaMemInit \uni06D0.init_BaaHaaMemInit \uni06BA.init_BaaHaaMemInit \uni06CC.init_BaaHaaMemInit \uni0767.init_BaaHaaMemInit \uni08A0.init_BaaHaaMemInit ] <anchor 438 -670> mark @TashkilBelow;
  pos base [\aHaa.medi_BaaHaaMemInit \aMem.medi_AlfPostTooth \uni062E.medi_BaaHaaMemInit \uni062D.medi_BaaHaaMemInit \uni0681.medi_BaaHaaMemInit \uni0687.medi_BaaHaaMemInit \uni0685.medi_BaaHaaMemInit \uni062C.medi_BaaHaaMemInit \uni0682.medi_BaaHaaMemInit \uni0757.medi_BaaHaaMemInit \uni0684.medi_BaaHaaMemInit \uni076E.medi_BaaHaaMemInit \uni0683.medi_BaaHaaMemInit \uni06BF.medi_BaaHaaMemInit \uni0758.medi_BaaHaaMemInit \uni0772.medi_BaaHaaMemInit \uni0686.medi_BaaHaaMemInit \uni0645.medi_AlfPostTooth ] <anchor 355 -670> mark @TashkilBelow;
  pos base [\aHaa.fina_AboveHaaIsol \aHaa.fina_AboveHaaIsol2 \uni062E.fina_AboveHaaIsol \uni062D.fina_AboveHaaIsol \uni0681.fina_AboveHaaIsol \uni0685.fina_AboveHaaIsol \uni0682.fina_AboveHaaIsol \uni0757.fina_AboveHaaIsol \uni0772.fina_AboveHaaIsol \uni062E.fina_AboveHaaIsol2 \uni062D.fina_AboveHaaIsol2 \uni0681.fina_AboveHaaIsol2 \uni0685.fina_AboveHaaIsol2 \uni0682.fina_AboveHaaIsol2 \uni0757.fina_AboveHaaIsol2 \uni0772.fina_AboveHaaIsol2 ] <anchor 817 100> mark @TashkilBelow;
  pos base [\aHaa.medi_1LamHaaHaaInit \uni0774.fina \uni062E.medi_1LamHaaHaaInit \uni062D.medi_1LamHaaHaaInit \uni0681.medi_1LamHaaHaaInit \uni0687.medi_1LamHaaHaaInit \uni0685.medi_1LamHaaHaaInit \uni062C.medi_1LamHaaHaaInit \uni0682.medi_1LamHaaHaaInit \uni0757.medi_1LamHaaHaaInit \uni0684.medi_1LamHaaHaaInit \uni076E.medi_1LamHaaHaaInit \uni0683.medi_1LamHaaHaaInit \uni0772.medi_1LamHaaHaaInit \uni0774.fina_Narrow ] <anchor 461 -670> mark @TashkilBelow;
  pos base [\aAyn.init_Finjani \uni06FC.init_Finjani \uni063A.init_Finjani \uni075E.init_Finjani \uni075D.init_Finjani \uni075F.init_Finjani \uni06A0.init_Finjani \uni0639.init_Finjani ] <anchor 421 -670> mark @TashkilBelow;
  pos base [\aHaa.medi_Finjani \uni062E.medi_Finjani \uni062D.medi_Finjani \uni0681.medi_Finjani \uni0687.medi_Finjani \uni0685.medi_Finjani \uni062C.medi_Finjani \uni0682.medi_Finjani \uni0757.medi_Finjani \uni0684.medi_Finjani \uni076E.medi_Finjani \uni0683.medi_Finjani \uni06BF.medi_Finjani \uni0758.medi_Finjani \uni0772.medi_Finjani \uni0686.medi_Finjani ] <anchor 570 -670> mark @TashkilBelow;
  pos base [\aSen.medi_PreYaa \aSad.init_PreYaa \aSad.medi_PreYaa \uni0680.medi_BaaRaaFina \uni0691.fina_LamRaaIsol \uni0692.fina_LamRaaIsol \uni0693.fina_LamRaaIsol \uni0697.fina_LamRaaIsol \uni0698.fina_LamRaaIsol \uni0699.fina_LamRaaIsol \uni075B.fina_LamRaaIsol \uni06EF.fina_LamRaaIsol \uni0632.fina_LamRaaIsol \uni0771.fina_LamRaaIsol \uni0631.fina_LamRaaIsol \uni076B.fina_LamRaaIsol \uni076C.fina_LamRaaIsol \uni06FA.medi_PreYaa \uni076D.medi_PreYaa \uni0633.medi_PreYaa \uni077E.medi_PreYaa \uni077D.medi_PreYaa \uni0634.medi_PreYaa \uni0770.medi_PreYaa \uni075C.medi_PreYaa \uni069A.medi_PreYaa \uni069B.medi_PreYaa \uni069C.medi_PreYaa \uni069D.init_PreYaa \uni06FB.init_PreYaa \uni0636.init_PreYaa \uni069E.init_PreYaa \uni0635.init_PreYaa \uni069D.medi_PreYaa \uni06FB.medi_PreYaa \uni0636.medi_PreYaa \uni069E.medi_PreYaa \uni0635.medi_PreYaa ] <anchor 370 -670> mark @TashkilBelow;
  pos base [\aKaf.isol.alt \uni06AA ] <anchor 2812 -670> mark @TashkilBelow;
  pos base [\aKaf.medi.alt \uni06AA.medi ] <anchor 2238 -670> mark @TashkilBelow;
  pos base [\aKaf.fina.alt \uni06AA.fina ] <anchor 2808 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_High \uni0776.medi_High \uni06BC.medi_High \uni0750.medi_High \uni0756.medi_High \uni0768.medi_High \uni06CE.medi_High \uni0775.medi_High \uni0626.medi_High \uni066E.medi_High \uni0620.medi_High \uni064A.medi_High \uni06BB.medi_High \uni067F.medi_High \uni067D.medi_High \uni0628.medi_High \uni067A.medi_High \uni0751.medi_High \uni0646.medi_High \uni062A.medi_High \uni0678.medi_High \uni063D.medi_High \uni062B.medi_High \uni0679.medi_High \uni06B9.medi_High \uni0769.medi_High \uni0649.medi_High \uni067C.medi_High \uni0754.medi_High \uni06BA.medi_High \uni06CC.medi_High \uni0767.medi_High ] <anchor 277 -670> mark @TashkilBelow;
  pos base [\aMem.fina_PostTooth \uni0645.fina_PostTooth ] <anchor 690 -670> mark @TashkilBelow;
  pos base [\aBaa.init_Wide \uni0750.init_Wide \uni0756.init_Wide \uni0768.init_Wide \uni0626.init_Wide \uni066E.init_Wide \uni06BB.init_Wide \uni067F.init_Wide \uni067D.init_Wide \uni067A.init_Wide \uni0646.init_Wide \uni062A.init_Wide \uni0678.init_Wide \uni062B.init_Wide \uni0679.init_Wide \uni0769.init_Wide \uni0649.init_Wide \uni06BA.init_Wide ] <anchor 215 -670> mark @TashkilBelow;
  pos base [\aHaa.medi_HaaHaaInit \uni062E.medi_HaaHaaInit \uni062D.medi_HaaHaaInit \uni0681.medi_HaaHaaInit \uni0685.medi_HaaHaaInit \uni0682.medi_HaaHaaInit \uni0757.medi_HaaHaaInit \uni0772.medi_HaaHaaInit ] <anchor 302 -670> mark @TashkilBelow;
  pos base [\aHaa.medi_AynHaaInit \uni06CB.fina_LamWawFina \uni0624.fina_LamWawFina \uni06CA.fina_LamWawFina \uni06CF.fina_LamWawFina \uni0778.fina_LamWawFina \uni06C6.fina_LamWawFina \uni06C7.fina_LamWawFina \uni06C4.fina_LamWawFina \uni06C5.fina_LamWawFina \uni0676.fina_LamWawFina \uni06C8.fina_LamWawFina \uni06C9.fina_LamWawFina \uni0779.fina_LamWawFina \uni0648.fina_LamWawFina \uni062E.medi_AynHaaInit \uni062D.medi_AynHaaInit \uni0681.medi_AynHaaInit \uni0687.medi_AynHaaInit \uni0685.medi_AynHaaInit \uni062C.medi_AynHaaInit \uni0682.medi_AynHaaInit \uni0757.medi_AynHaaInit \uni0684.medi_AynHaaInit \uni076E.medi_AynHaaInit \uni0683.medi_AynHaaInit \uni06BF.medi_AynHaaInit \uni0758.medi_AynHaaInit \uni0772.medi_AynHaaInit \uni0686.medi_AynHaaInit ] <anchor 440 -670> mark @TashkilBelow;
  pos base [\aMem.medi_LamMemInitTatweel ] <anchor 110 -670> mark @TashkilBelow;
  pos base [\aHeh.init_AboveHaa \aHaa.init_AboveHaa \aAyn.init_AboveHaa \aMem.init_AboveHaa \uni0647.init_AboveHaa \uni06C1.init_AboveHaa \uni062E.init_AboveHaa \uni062D.init_AboveHaa \uni0681.init_AboveHaa \uni0685.init_AboveHaa \uni062C.init_AboveHaa \uni0682.init_AboveHaa \uni0757.init_AboveHaa \uni0772.init_AboveHaa \uni06FC.init_AboveHaa \uni063A.init_AboveHaa \uni075E.init_AboveHaa \uni075D.init_AboveHaa \uni075F.init_AboveHaa \uni06A0.init_AboveHaa \uni0639.init_AboveHaa \uni0645.init_AboveHaa ] <anchor 944 -260> mark @TashkilBelow;
  pos base [\aKaf.init_AboveHaa \uni063B.init_AboveHaa \uni063C.init_AboveHaa \uni077F.init_AboveHaa \uni0764.init_AboveHaa \uni0643.init_AboveHaa \uni06B0.init_AboveHaa \uni06B3.init_AboveHaa \uni06B2.init_AboveHaa \uni06AB.init_AboveHaa \uni06AC.init_AboveHaa \uni06AD.init_AboveHaa \uni06AE.init_AboveHaa \uni06AF.init_AboveHaa \uni06A9.init_AboveHaa \uni06B4.init_AboveHaa \uni0763.init_AboveHaa \uni0762.init_AboveHaa \uni06B1.init_AboveHaa ] <anchor 942 -260> mark @TashkilBelow;
  pos base [\uni06D2 \uni077A \uni077B ] <anchor 600 -640> mark @TashkilBelow;
  pos base [\aKaf.init_KafLam \uni063B.init_KafLam \uni077F.init_KafLam \uni0643.init_KafLam \uni06B0.init_KafLam \uni06B3.init_KafLam \uni06B2.init_KafLam \uni06AB.init_KafLam \uni06AC.init_KafLam \uni06AD.init_KafLam \uni06AF.init_KafLam \uni06A9.init_KafLam \uni06B4.init_KafLam \uni0763.init_KafLam \uni0762.init_KafLam \uni06B1.init_KafLam ] <anchor 287 -670> mark @TashkilBelow;
  pos base [\aKaf.fina_KafKafFina \uni0750 \uni063B.fina_KafKafFina \uni063C.fina_KafKafFina \uni077F.fina_KafKafFina \uni0764.fina_KafKafFina \uni0643.fina_KafKafFina \uni06B0.fina_KafKafFina \uni06B3.fina_KafKafFina \uni06B2.fina_KafKafFina \uni06AB.fina_KafKafFina \uni06AC.fina_KafKafFina \uni06AD.fina_KafKafFina \uni06AE.fina_KafKafFina \uni06AF.fina_KafKafFina \uni06A9.fina_KafKafFina \uni06B4.fina_KafKafFina \uni0763.fina_KafKafFina \uni0762.fina_KafKafFina \uni06B1.fina_KafKafFina ] <anchor 910 -670> mark @TashkilBelow;
  pos base [\aKaf.medi_KafLam \uni063B.medi_KafLam \uni063C.medi_KafLam \uni077F.medi_KafLam \uni0764.medi_KafLam \uni0643.medi_KafLam \uni06B0.medi_KafLam \uni06B3.medi_KafLam \uni06B2.medi_KafLam \uni06AB.medi_KafLam \uni06AC.medi_KafLam \uni06AD.medi_KafLam \uni06AE.medi_KafLam \uni06AF.medi_KafLam \uni06A9.medi_KafLam \uni06B4.medi_KafLam \uni0763.medi_KafLam \uni0762.medi_KafLam \uni06B1.medi_KafLam ] <anchor 487 -670> mark @TashkilBelow;
  pos base [\aLam.medi_KafLamHehIsol \aLam.medi_LamLamHehIsol \aLam.medi_LamLamHehFina \uni06B5.medi_KafLamHehIsol \uni06B7.medi_KafLamHehIsol \uni0644.medi_KafLamHehIsol \uni06B6.medi_KafLamHehIsol \uni076A.medi_KafLamHehIsol \uni06B5.medi_LamLamHehIsol \uni06B7.medi_LamLamHehIsol \uni0644.medi_LamLamHehIsol \uni06B6.medi_LamLamHehIsol \uni076A.medi_LamLamHehIsol \uni06B5.medi_LamLamHehFina \uni06B7.medi_LamLamHehFina \uni0644.medi_LamLamHehFina \uni06B6.medi_LamLamHehFina \uni076A.medi_LamLamHehFina ] <anchor 146 -670> mark @TashkilBelow;
  pos base [\aLam.fina_KafMemLam \uni06B5.fina_KafMemLam \uni06B7.fina_KafMemLam \uni0644.fina_KafMemLam \uni06B6.fina_KafMemLam \uni076A.fina_KafMemLam ] <anchor 620 -900> mark @TashkilBelow;
  pos base [\aKaf.medi_KafHeh \uni063B.medi_KafHeh \uni063C.medi_KafHeh \uni077F.medi_KafHeh \uni0764.medi_KafHeh \uni0643.medi_KafHeh \uni06B0.medi_KafHeh \uni06B3.medi_KafHeh \uni06B2.medi_KafHeh \uni06AB.medi_KafHeh \uni06AC.medi_KafHeh \uni06AD.medi_KafHeh \uni06AE.medi_KafHeh \uni06AF.medi_KafHeh \uni06A9.medi_KafHeh \uni06B4.medi_KafHeh \uni0763.medi_KafHeh \uni0762.medi_KafHeh \uni06B1.medi_KafHeh ] <anchor 402 -670> mark @TashkilBelow;
  pos base [\aDal.fina_KafDal \uni0690.fina_KafDal \uni06EE.fina_KafDal \uni0689.fina_KafDal \uni0688.fina_KafDal \uni0630.fina_KafDal \uni062F.fina_KafDal \uni0759.fina_KafDal \uni068C.fina_KafDal \uni068B.fina_KafDal \uni068A.fina_KafDal \uni068F.fina_KafDal \uni068E.fina_KafDal \uni068D.fina_KafDal ] <anchor 516 -670> mark @TashkilBelow;
  pos base [\aLam.medi_LamHeh \uni06B5.medi_LamHeh \uni06B7.medi_LamHeh \uni0644.medi_LamHeh \uni06B6.medi_LamHeh \uni076A.medi_LamHeh ] <anchor 63 -670> mark @TashkilBelow;
  pos base [\aDal.fina_LamDal \uni062C.medi_HaaHaaInit \uni0690.fina_LamDal \uni06EE.fina_LamDal \uni0689.fina_LamDal \uni0688.fina_LamDal \uni0630.fina_LamDal \uni062F.fina_LamDal \uni0759.fina_LamDal \uni068C.fina_LamDal \uni068B.fina_LamDal \uni068A.fina_LamDal \uni068F.fina_LamDal \uni068E.fina_LamDal \uni068D.fina_LamDal ] <anchor 442 -670> mark @TashkilBelow;
  pos base [\aAyn.init_AynMemInit \uni06FC.init_AynMemInit \uni063A.init_AynMemInit \uni075E.init_AynMemInit \uni075D.init_AynMemInit \uni075F.init_AynMemInit \uni06A0.init_AynMemInit \uni0639.init_AynMemInit ] <anchor 636 -670> mark @TashkilBelow;
  pos base [\aHaa.init_HaaMemInit \uni062E.init_HaaMemInit \uni062D.init_HaaMemInit \uni0681.init_HaaMemInit \uni0687.init_HaaMemInit \uni0685.init_HaaMemInit \uni062C.init_HaaMemInit \uni0682.init_HaaMemInit \uni0757.init_HaaMemInit \uni0684.init_HaaMemInit \uni076E.init_HaaMemInit \uni0683.init_HaaMemInit \uni06BF.init_HaaMemInit \uni0758.init_HaaMemInit \uni0772.init_HaaMemInit \uni0686.init_HaaMemInit ] <anchor 681 -670> mark @TashkilBelow;
  pos base [\aSen.init_SenMemInit \uni06FA.init_SenMemInit \uni076D.init_SenMemInit \uni0633.init_SenMemInit \uni077E.init_SenMemInit \uni077D.init_SenMemInit \uni0634.init_SenMemInit \uni0770.init_SenMemInit \uni075C.init_SenMemInit \uni069A.init_SenMemInit \uni069B.init_SenMemInit \uni069C.init_SenMemInit ] <anchor 613 -670> mark @TashkilBelow;
  pos base [\aSad.init_SadMemInit \uni069D.init_SadMemInit \uni06FB.init_SadMemInit \uni0636.init_SadMemInit \uni069E.init_SadMemInit \uni0635.init_SadMemInit ] <anchor 812 -670> mark @TashkilBelow;
  pos base [\aMem.init_MemMemInit \uni0645.init_MemMemInit ] <anchor 374 -670> mark @TashkilBelow;
  pos base [\aMem.medi_SenMemInit \uni0645.medi_SenMemInit ] <anchor 306 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaYaaIsol \uni0776.init_BaaYaaIsol \uni06BC.init_BaaYaaIsol \uni0750.init_BaaYaaIsol \uni0756.init_BaaYaaIsol \uni0768.init_BaaYaaIsol \uni06CE.init_BaaYaaIsol \uni0775.init_BaaYaaIsol \uni0626.init_BaaYaaIsol \uni066E.init_BaaYaaIsol \uni0620.init_BaaYaaIsol \uni064A.init_BaaYaaIsol \uni06BB.init_BaaYaaIsol \uni067F.init_BaaYaaIsol \uni067D.init_BaaYaaIsol \uni0628.init_BaaYaaIsol \uni067A.init_BaaYaaIsol \uni0751.init_BaaYaaIsol \uni0646.init_BaaYaaIsol \uni062A.init_BaaYaaIsol \uni0678.init_BaaYaaIsol \uni063D.init_BaaYaaIsol \uni062B.init_BaaYaaIsol \uni0679.init_BaaYaaIsol \uni06B9.init_BaaYaaIsol \uni0769.init_BaaYaaIsol \uni0649.init_BaaYaaIsol \uni067C.init_BaaYaaIsol \uni0754.init_BaaYaaIsol \uni06BA.init_BaaYaaIsol \uni06CC.init_BaaYaaIsol \uni0767.init_BaaYaaIsol ] <anchor 343 -800> mark @TashkilBelow;
  pos base [\aHaa.init_HaaYaaIsol \uni062E.init_HaaYaaIsol \uni062D.init_HaaYaaIsol \uni0681.init_HaaYaaIsol \uni0687.init_HaaYaaIsol \uni0685.init_HaaYaaIsol \uni062C.init_HaaYaaIsol \uni0682.init_HaaYaaIsol \uni0757.init_HaaYaaIsol \uni0684.init_HaaYaaIsol \uni076F.init_HaaYaaIsol \uni076E.init_HaaYaaIsol \uni0683.init_HaaYaaIsol \uni06BF.init_HaaYaaIsol \uni077C.init_HaaYaaIsol \uni0758.init_HaaYaaIsol \uni0772.init_HaaYaaIsol \uni0686.init_HaaYaaIsol ] <anchor 659 -670> mark @TashkilBelow;
  pos base [\aFaa.init_FaaYaaIsol \uni066F.init_FaaYaaIsol \uni0761.init_FaaYaaIsol \uni0760.init_FaaYaaIsol \uni0642.init_FaaYaaIsol \uni0641.init_FaaYaaIsol \uni06A8.init_FaaYaaIsol \uni06A1.init_FaaYaaIsol \uni06A2.init_FaaYaaIsol \uni06A3.init_FaaYaaIsol \uni06A4.init_FaaYaaIsol \uni06A5.init_FaaYaaIsol \uni06A6.init_FaaYaaIsol \uni06A7.init_FaaYaaIsol ] <anchor 250 -810> mark @TashkilBelow;
  pos base [\aHeh.init_HehYaaIsol \uni06CB \uni0624 \uni06CA \uni06CF \uni0778 \uni06C6 \uni06C7 \uni06C4 \uni06C5 \uni0676 \uni06C8 \uni06C9 \uni0779 \uni0648 \uni0647.init_HehYaaIsol \uni06C1.init_HehYaaIsol ] <anchor 510 -670> mark @TashkilBelow;
  pos base [\aYaa.fina_KafYaaIsol \uni0775.fina_KafYaaIsol \uni063F.fina_KafYaaIsol \uni0678.fina_KafYaaIsol \uni063D.fina_KafYaaIsol \uni063E.fina_KafYaaIsol \uni0649.fina_KafYaaIsol \uni0776.fina_KafYaaIsol \uni06CD.fina_KafYaaIsol \uni06CC.fina_KafYaaIsol \uni0626.fina_KafYaaIsol \uni06CE.fina_KafYaaIsol ] <anchor 728 -670> mark @TashkilBelow;
  pos base [\aKaf.init_KafMemIsol \uni063B.init_KafMemIsol \uni063C.init_KafMemIsol \uni077F.init_KafMemIsol \uni0764.init_KafMemIsol \uni0643.init_KafMemIsol \uni06B0.init_KafMemIsol \uni06B3.init_KafMemIsol \uni06B2.init_KafMemIsol \uni06AB.init_KafMemIsol \uni06AC.init_KafMemIsol \uni06AD.init_KafMemIsol \uni06AE.init_KafMemIsol \uni06AF.init_KafMemIsol \uni06A9.init_KafMemIsol \uni06B4.init_KafMemIsol \uni0763.init_KafMemIsol \uni0762.init_KafMemIsol \uni06B1.init_KafMemIsol ] <anchor 650 -670> mark @TashkilBelow;
  pos base [\aLam.init_LamMemIsol \uni06B5.init_LamMemIsol \uni06B7.init_LamMemIsol \uni0644.init_LamMemIsol \uni06B6.init_LamMemIsol \uni076A.init_LamMemIsol ] <anchor 270 0> mark @TashkilBelow;
  pos base [\aBaa.init_BaaMemIsol \uni0680.init_BaaMemIsol \uni0776.init_BaaMemIsol \uni06BC.init_BaaMemIsol \uni0750.init_BaaMemIsol \uni0756.init_BaaMemIsol \uni0768.init_BaaMemIsol \uni06CE.init_BaaMemIsol \uni0775.init_BaaMemIsol \uni06BD.init_BaaMemIsol \uni0626.init_BaaMemIsol \uni066E.init_BaaMemIsol \uni0620.init_BaaMemIsol \uni064A.init_BaaMemIsol \uni06BB.init_BaaMemIsol \uni067F.init_BaaMemIsol \uni067D.init_BaaMemIsol \uni067E.init_BaaMemIsol \uni0628.init_BaaMemIsol \uni067A.init_BaaMemIsol \uni0751.init_BaaMemIsol \uni0646.init_BaaMemIsol \uni0753.init_BaaMemIsol \uni0752.init_BaaMemIsol \uni062A.init_BaaMemIsol \uni0678.init_BaaMemIsol \uni063D.init_BaaMemIsol \uni062B.init_BaaMemIsol \uni0679.init_BaaMemIsol \uni06B9.init_BaaMemIsol \uni0769.init_BaaMemIsol \uni0649.init_BaaMemIsol \uni067C.init_BaaMemIsol \uni0754.init_BaaMemIsol \uni06D1.init_BaaMemIsol \uni06BA.init_BaaMemIsol \uni06CC.init_BaaMemIsol \uni0767.init_BaaMemIsol ] <anchor 603 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_BaaMemAlfFina \uni06BC.medi_BaaMemAlfFina \uni0756.medi_BaaMemAlfFina \uni0768.medi_BaaMemAlfFina \uni0626.medi_BaaMemAlfFina \uni066E.medi_BaaMemAlfFina \uni06BB.medi_BaaMemAlfFina \uni067F.medi_BaaMemAlfFina \uni067D.medi_BaaMemAlfFina \uni067A.medi_BaaMemAlfFina \uni0646.medi_BaaMemAlfFina \uni062A.medi_BaaMemAlfFina \uni0678.medi_BaaMemAlfFina \uni062B.medi_BaaMemAlfFina \uni0679.medi_BaaMemAlfFina \uni0769.medi_BaaMemAlfFina \uni0649.medi_BaaMemAlfFina \uni067C.medi_BaaMemAlfFina \uni06BA.medi_BaaMemAlfFina ] <anchor 383 -670> mark @TashkilBelow;
  pos base [\aMem.medi_BaaMemAlfFina \uni0645.medi_BaaMemAlfFina ] <anchor 20 -670> mark @TashkilBelow;
  pos base [\aBaa.init_BaaHehInit \uni0680.init_BaaHehInit \uni0776.init_BaaHehInit \uni06BC.init_BaaHehInit \uni0750.init_BaaHehInit \uni0756.init_BaaHehInit \uni0768.init_BaaHehInit \uni06CE.init_BaaHehInit \uni0775.init_BaaHehInit \uni06BD.init_BaaHehInit \uni0626.init_BaaHehInit \uni066E.init_BaaHehInit \uni0620.init_BaaHehInit \uni064A.init_BaaHehInit \uni06BB.init_BaaHehInit \uni067F.init_BaaHehInit \uni067D.init_BaaHehInit \uni067E.init_BaaHehInit \uni0628.init_BaaHehInit \uni067A.init_BaaHehInit \uni0751.init_BaaHehInit \uni0646.init_BaaHehInit \uni0753.init_BaaHehInit \uni0752.init_BaaHehInit \uni062A.init_BaaHehInit \uni0678.init_BaaHehInit \uni063D.init_BaaHehInit \uni062B.init_BaaHehInit \uni0679.init_BaaHehInit \uni06B9.init_BaaHehInit \uni0769.init_BaaHehInit \uni0649.init_BaaHehInit \uni067C.init_BaaHehInit \uni0754.init_BaaHehInit \uni06D1.init_BaaHehInit \uni06BA.init_BaaHehInit \uni06CC.init_BaaHehInit \uni0767.init_BaaHehInit ] <anchor 513 -670> mark @TashkilBelow;
  pos base [\aBaa.medi_BaaHehMedi \uni0680.medi_BaaHehMedi \uni0776.medi_BaaHehMedi \uni06BC.medi_BaaHehMedi \uni0750.medi_BaaHehMedi \uni0756.medi_BaaHehMedi \uni0768.medi_BaaHehMedi \uni06CE.medi_BaaHehMedi \uni0775.medi_BaaHehMedi \uni0626.medi_BaaHehMedi \uni066E.medi_BaaHehMedi \uni0620.medi_BaaHehMedi \uni064A.medi_BaaHehMedi \uni06BB.medi_BaaHehMedi \uni067F.medi_BaaHehMedi \uni067D.medi_BaaHehMedi \uni0628.medi_BaaHehMedi \uni067A.medi_BaaHehMedi \uni0751.medi_BaaHehMedi \uni0646.medi_BaaHehMedi \uni062A.medi_BaaHehMedi \uni0678.medi_BaaHehMedi \uni063D.medi_BaaHehMedi \uni062B.medi_BaaHehMedi \uni0679.medi_BaaHehMedi \uni06B9.medi_BaaHehMedi \uni0769.medi_BaaHehMedi \uni0649.medi_BaaHehMedi \uni067C.medi_BaaHehMedi \uni0754.medi_BaaHehMedi \uni06BA.medi_BaaHehMedi \uni06CC.medi_BaaHehMedi \uni0767.medi_BaaHehMedi ] <anchor 336 -670> mark @TashkilBelow;
  pos base [\aLam.medi_KafLamMemFina \uni06B5.medi_KafLamMemFina \uni06B7.medi_KafLamMemFina \uni0644.medi_KafLamMemFina \uni06B6.medi_KafLamMemFina \uni076A.medi_KafLamMemFina \aYaaBarree.fina_PostAscender \uni077B.fina_PostAscender \uni077A.fina_PostAscender \uni06D2.fina_PostAscender \aYaaBarree.fina_PostAyn \uni077B.fina_PostAyn \uni077A.fina_PostAyn \uni06D2.fina_PostAyn ] <anchor 350 -670> mark @TashkilBelow;
  pos base [\aLam.init_LamLamInit \uni06B5.init_LamLamInit \uni06B7.init_LamLamInit \uni0644.init_LamLamInit \uni06B8.init_LamLamInit \uni06B6.init_LamLamInit ] <anchor 205 -670> mark @TashkilBelow;
  pos base [\aLam.medi_LamLamAlfIsol \aLam.medi_LamLamAlefFina \uni06B5.medi_LamLamAlfIsol \uni06B7.medi_LamLamAlfIsol \uni0644.medi_LamLamAlfIsol \uni06B8.medi_LamLamAlfIsol \uni06B6.medi_LamLamAlfIsol \uni076A.medi_LamLamAlfIsol \uni06B5.medi_LamLamAlefFina \uni06B7.medi_LamLamAlefFina \uni0644.medi_LamLamAlefFina \uni06B8.medi_LamLamAlefFina \uni06B6.medi_LamLamAlefFina \uni076A.medi_LamLamAlefFina ] <anchor -70 -670> mark @TashkilBelow;
  pos base [\aKaf.fina_LamKafIsol \uni063B.fina_LamKafIsol \uni063C.fina_LamKafIsol \uni077F.fina_LamKafIsol \uni0764.fina_LamKafIsol \uni0643.fina_LamKafIsol \uni06B0.fina_LamKafIsol \uni06B3.fina_LamKafIsol \uni06B2.fina_LamKafIsol \uni06AB.fina_LamKafIsol \uni06AC.fina_LamKafIsol \uni06AD.fina_LamKafIsol \uni06AE.fina_LamKafIsol \uni06AF.fina_LamKafIsol \uni06A9.fina_LamKafIsol \uni06B4.fina_LamKafIsol \uni0763.fina_LamKafIsol \uni0762.fina_LamKafIsol \uni06B1.fina_LamKafIsol ] <anchor 720 -740> mark @TashkilBelow;
  pos base [\uni0625.fina \uni0673.fina \uni0625.fina_Narrow \uni0673.fina_Narrow ] <anchor 250 -730> mark @TashkilBelow;
  pos base [\uni0773.fina \uni0773.fina_Narrow ] <anchor 460 -670> mark @TashkilBelow;
  pos base [\uni0623.fina \uni0623.fina_Narrow \uni0623.fina_Wide ] <anchor 375 -670> mark @TashkilBelow;
  pos base [\uni0625 \uni0673 ] <anchor 330 -700> mark @TashkilBelow;
  pos base [\uni0627 \uni0675 \uni0672 ] <anchor 250 -190> mark @TashkilBelow;
  pos base [\uni0774 ] <anchor 351 -190> mark @TashkilBelow;
  pos base [\uni0773 ] <anchor 350 -190> mark @TashkilBelow;
  pos base [\uni0623 ] <anchor 275 -190> mark @TashkilBelow;
  pos base [\uni06FC.fina ] <anchor 630 -380> mark @TashkilBelow;
  pos base [\uni06FC ] <anchor 750 -80> mark @TashkilBelow;
  pos base [\uni0750.fina ] <anchor 870 -670> mark @TashkilBelow;
  pos base [\uni0753.fina \uni0752.fina ] <anchor 900 -801> mark @TashkilBelow;
  pos base [\uni0680.fina ] <anchor 930 -830> mark @TashkilBelow;
  pos base [\uni0755.fina ] <anchor 860 -860> mark @TashkilBelow;
  pos base [\uni067E.fina ] <anchor 984 -793> mark @TashkilBelow;
  pos base [\uni067B.fina ] <anchor 940 -820> mark @TashkilBelow;
  pos base [\uni0777.init ] <anchor 170 -960> mark @TashkilBelow;
  pos base [\uni0680.init ] <anchor 261 -920> mark @TashkilBelow;
  pos base [\uni06BD.init \uni0753.init \uni0752.init ] <anchor 231 -891> mark @TashkilBelow;
  pos base [\uni067E.init \uni06D1.init ] <anchor 315 -883> mark @TashkilBelow;
  pos base [\uni067B.init \uni06D0.init ] <anchor 210 -820> mark @TashkilBelow;
  pos base [\uni0753 \uni0752 ] <anchor 1011 -741> mark @TashkilBelow;
  pos base [\uni0680 ] <anchor 1041 -770> mark @TashkilBelow;
  pos base [\uni0755 ] <anchor 960 -810> mark @TashkilBelow;
  pos base [\uni067E ] <anchor 1095 -733> mark @TashkilBelow;
  pos base [\uni067B ] <anchor 1040 -770> mark @TashkilBelow;
  pos base [\uni0777.medi ] <anchor 260 -960> mark @TashkilBelow;
  pos base [\uni0755.medi ] <anchor 220 -760> mark @TashkilBelow;
  pos base [\uni067B.medi \uni06D0.medi ] <anchor 300 -720> mark @TashkilBelow;
  pos base [\uni075A ] <anchor 460 -610> mark @TashkilBelow;
  pos base [\uni0761.fina ] <anchor 1711 -741> mark @TashkilBelow;
  pos base [\uni06A5.fina ] <anchor 1795 -733> mark @TashkilBelow;
  pos base [\uni0761.init ] <anchor 311 -741> mark @TashkilBelow;
  pos base [\uni06A5.init ] <anchor 395 -733> mark @TashkilBelow;
  pos base [\uni0761.medi ] <anchor 381 -741> mark @TashkilBelow;
  pos base [\uni06A5.medi ] <anchor 465 -733> mark @TashkilBelow;
  pos base [\uni0687.fina \uni062C.fina ] <anchor 830 -1158> mark @TashkilBelow;
  pos base [\uni0684.fina ] <anchor 840 -1158> mark @TashkilBelow;
  pos base [\uni076F.fina ] <anchor 790 -1158> mark @TashkilBelow;
  pos base [\uni076E.fina ] <anchor 770 -1158> mark @TashkilBelow;
  pos base [\uni0683.fina ] <anchor 820 -1158> mark @TashkilBelow;
  pos base [\uni06BF.fina \uni0686.fina ] <anchor 884 -1158> mark @TashkilBelow;
  pos base [\uni077C.fina \uni0758.fina ] <anchor 800 -1158> mark @TashkilBelow;
  pos base [\uni062E.init \uni062D.init \uni0681.init \uni0687.init \uni0685.init \uni062C.init \uni0682.init \uni0757.init \uni0684.init \uni076E.init \uni0683.init \uni06BF.init \uni0758.init \uni0772.init \uni0686.init ] <anchor 610 -670> mark @TashkilBelow;
  pos base [\uni076F.init ] <anchor 700 -905> mark @TashkilBelow;
  pos base [\uni077C.init ] <anchor 610 -812> mark @TashkilBelow;
  pos base [\uni0687 \uni062C ] <anchor 830 -1154> mark @TashkilBelow;
  pos base [\uni0684 ] <anchor 840 -1154> mark @TashkilBelow;
  pos base [\uni076F ] <anchor 890 -1154> mark @TashkilBelow;
  pos base [\uni076E \uni0683 ] <anchor 870 -1154> mark @TashkilBelow;
  pos base [\uni06BF \uni0686 ] <anchor 884 -1154> mark @TashkilBelow;
  pos base [\uni077C \uni0758 ] <anchor 800 -1154> mark @TashkilBelow;
  pos base [\uni076F.medi ] <anchor 690 -893> mark @TashkilBelow;
  pos base [\uni077C.medi ] <anchor 600 -800> mark @TashkilBelow;
  pos base [\uni06B8.fina \uni06B8.fina_KafLam \uni06B8.fina_LamLamIsol \uni06B8.fina_LamLamFina ] <anchor 795 -1368> mark @TashkilBelow;
  pos base [\uni06B8 ] <anchor 780 -763> mark @TashkilBelow;
  pos base [\uni0767.fina ] <anchor 568 -1225> mark @TashkilBelow;
  pos base [\uni06B9.fina ] <anchor 577 -1175> mark @TashkilBelow;
  pos base [\uni0694.fina \uni0696.fina ] <anchor 461 -898> mark @TashkilBelow;
  pos base [\uni0695.fina ] <anchor 421 -28> mark @TashkilBelow;
  pos base [\uni0693 ] <anchor 194 -754> mark @TashkilBelow;
  pos base [\uni0694 \uni0696 ] <anchor 590 -821> mark @TashkilBelow;
  pos base [\uni069B.medi \uni069C.medi ] <anchor 645 -733> mark @TashkilBelow;
  pos base [\uni0677 ] <anchor 710 -670> mark @TashkilBelow;
  pos base [\uni0777.fina ] <anchor 749 -1607> mark @TashkilBelow;
  pos base [\uni06D1.fina ] <anchor 833 -1430> mark @TashkilBelow;
  pos base [\uni06D0.fina ] <anchor 789 -1367> mark @TashkilBelow;
  pos base [\uni0620.fina ] <anchor 749 -1197> mark @TashkilBelow;
  pos base [\uni064A.fina ] <anchor 769 -1227> mark @TashkilBelow;
  pos base [\uni0777 ] <anchor 727 -1319> mark @TashkilBelow;
  pos base [\uni06D1 ] <anchor 811 -1142> mark @TashkilBelow;
  pos base [\uni06D0 ] <anchor 767 -1079> mark @TashkilBelow;
  pos base [\uni0620 ] <anchor 727 -909> mark @TashkilBelow;
  pos base [\uni064A ] <anchor 747 -939> mark @TashkilBelow;
  pos base [\uni0777.init_BaaRaaIsol ] <anchor 194 -810> mark @TashkilBelow;
  pos base [\uni0694.fina_BaaRaaIsol \uni0696.fina_BaaRaaIsol ] <anchor 354 -834> mark @TashkilBelow;
  pos base [\uni0695.fina_BaaRaaIsol ] <anchor 314 36> mark @TashkilBelow;
  pos base [\uni0677.fina_LamWawFina ] <anchor 640 -670> mark @TashkilBelow;
  pos base [\uni0761.medi_FaaYaaFina ] <anchor 579 -1161> mark @TashkilBelow;
  pos base [\uni0760.medi_FaaYaaFina ] <anchor 599 -950> mark @TashkilBelow;
  pos base [\uni06A2.medi_FaaYaaFina \uni06A3.medi_FaaYaaFina ] <anchor 598 -900> mark @TashkilBelow;
  pos base [\uni06A5.medi_FaaYaaFina ] <anchor 663 -1153> mark @TashkilBelow;
  pos base [\uni0777.fina_FaaYaaFina ] <anchor 728 -1699> mark @TashkilBelow;
  pos base [\uni06D1.fina_FaaYaaFina ] <anchor 812 -1622> mark @TashkilBelow;
  pos base [\uni06D0.fina_FaaYaaFina ] <anchor 768 -1559> mark @TashkilBelow;
  pos base [\uni0620.fina_FaaYaaFina ] <anchor 600 -1300> mark @TashkilBelow;
  pos base [\uni064A.fina_FaaYaaFina ] <anchor 748 -1419> mark @TashkilBelow;
  pos base [\uni076F.medi_LamLamHaaInit ] <anchor 671 -893> mark @TashkilBelow;
  pos base [\uni077C.medi_LamLamHaaInit ] <anchor 581 -800> mark @TashkilBelow;
  pos base [\uni0777.medi_BaaMemFina ] <anchor 447 -1190> mark @TashkilBelow;
  pos base [\uni0680.medi_BaaMemFina ] <anchor 451 -1050> mark @TashkilBelow;
  pos base [\uni0776.medi_BaaMemFina \uni06CE.medi_BaaMemFina \uni0775.medi_BaaMemFina \uni064A.medi_BaaMemFina \uni063D.medi_BaaMemFina \uni0754.medi_BaaMemFina \uni06CC.medi_BaaMemFina \uni0767.medi_BaaMemFina ] <anchor 441 -810> mark @TashkilBelow;
  pos base [\uni0750.medi_BaaMemFina ] <anchor 350 -860> mark @TashkilBelow;
  pos base [\uni06BD.medi_BaaMemFina \uni0753.medi_BaaMemFina \uni0752.medi_BaaMemFina ] <anchor 421 -1021> mark @TashkilBelow;
  pos base [\uni0620.medi_BaaMemFina ] <anchor 447 -780> mark @TashkilBelow;
  pos base [\uni0755.medi_BaaMemFina ] <anchor 407 -990> mark @TashkilBelow;
  pos base [\uni067E.medi_BaaMemFina \uni06D1.medi_BaaMemFina ] <anchor 505 -1013> mark @TashkilBelow;
  pos base [\uni067B.medi_BaaMemFina \uni06D0.medi_BaaMemFina ] <anchor 487 -950> mark @TashkilBelow;
  pos base [\uni0628.medi_BaaMemFina \uni0751.medi_BaaMemFina \uni06B9.medi_BaaMemFina ] <anchor 477 -760> mark @TashkilBelow;
  pos base [\uni0777.medi_LamBaaMemInit ] <anchor 267 -960> mark @TashkilBelow;
  pos base [\uni0680.medi_LamBaaMemInit ] <anchor 298 -770> mark @TashkilBelow;
  pos base [\uni06BD.medi_LamBaaMemInit \uni0753.medi_LamBaaMemInit \uni0752.medi_LamBaaMemInit ] <anchor 268 -741> mark @TashkilBelow;
  pos base [\uni0755.medi_LamBaaMemInit ] <anchor 227 -760> mark @TashkilBelow;
  pos base [\uni067E.medi_LamBaaMemInit \uni06D1.medi_LamBaaMemInit ] <anchor 352 -733> mark @TashkilBelow;
  pos base [\uni067B.medi_LamBaaMemInit \uni06D0.medi_LamBaaMemInit ] <anchor 307 -720> mark @TashkilBelow;
  pos base [\uni0777.init_BaaDal ] <anchor 80 -960> mark @TashkilBelow;
  pos base [\uni0680.init_BaaDal \uni08A0.medi_BaaBaaInit ] <anchor 60 -770> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaDal \uni0753.init_BaaDal \uni0752.init_BaaDal ] <anchor 30 -741> mark @TashkilBelow;
  pos base [\uni0755.init_BaaDal ] <anchor 40 -760> mark @TashkilBelow;
  pos base [\uni067E.init_BaaDal \uni06D1.init_BaaDal ] <anchor 114 -733> mark @TashkilBelow;
  pos base [\uni067B.init_BaaDal \uni06D0.init_BaaDal ] <anchor 120 -720> mark @TashkilBelow;
  pos base [\uni075A.fina_BaaDal ] <anchor 502 -680> mark @TashkilBelow;
  pos base [\uni0777.init_BaaMemHaaInit ] <anchor 331 -1020> mark @TashkilBelow;
  pos base [\uni0755.init_BaaMemHaaInit ] <anchor 291 -820> mark @TashkilBelow;
  pos base [\uni067B.init_BaaMemHaaInit \uni06D0.init_BaaMemHaaInit ] <anchor 371 -780> mark @TashkilBelow;
  pos base [\uni076F.medi_BaaMemHaaInit ] <anchor 402 -933> mark @TashkilBelow;
  pos base [\uni077C.medi_BaaMemHaaInit ] <anchor 312 -840> mark @TashkilBelow;
  pos base [\uni0777.init_BaaBaaYaa ] <anchor 147 -894> mark @TashkilBelow;
  pos base [\uni0777.medi_BaaBaaYaa ] <anchor 437 -1229> mark @TashkilBelow;
  pos base [\uni0680.medi_BaaBaaYaa ] <anchor 578 -1089> mark @TashkilBelow;
  pos base [\uni0776.medi_BaaBaaYaa \uni06CE.medi_BaaBaaYaa \uni0775.medi_BaaBaaYaa \uni064A.medi_BaaBaaYaa \uni063D.medi_BaaBaaYaa \uni0754.medi_BaaBaaYaa \uni06CC.medi_BaaBaaYaa \uni0767.medi_BaaBaaYaa ] <anchor 568 -849> mark @TashkilBelow;
  pos base [\uni0750.medi_BaaBaaYaa ] <anchor 477 -899> mark @TashkilBelow;
  pos base [\uni06BD.medi_BaaBaaYaa \uni0753.medi_BaaBaaYaa \uni0752.medi_BaaBaaYaa ] <anchor 548 -1060> mark @TashkilBelow;
  pos base [\uni0620.medi_BaaBaaYaa ] <anchor 437 -819> mark @TashkilBelow;
  pos base [\uni0755.medi_BaaBaaYaa ] <anchor 397 -1029> mark @TashkilBelow;
  pos base [\uni067E.medi_BaaBaaYaa \uni06D1.medi_BaaBaaYaa ] <anchor 632 -1052> mark @TashkilBelow;
  pos base [\uni067B.medi_BaaBaaYaa \uni06D0.medi_BaaBaaYaa ] <anchor 477 -989> mark @TashkilBelow;
  pos base [\uni0777.fina_BaaBaaYaa ] <anchor 759 -1501> mark @TashkilBelow;
  pos base [\uni06D1.fina_BaaBaaYaa ] <anchor 843 -1324> mark @TashkilBelow;
  pos base [\uni06D0.fina_BaaBaaYaa ] <anchor 799 -1261> mark @TashkilBelow;
  pos base [\uni0620.fina_BaaBaaYaa ] <anchor 759 -1091> mark @TashkilBelow;
  pos base [\uni064A.fina_BaaBaaYaa ] <anchor 779 -1121> mark @TashkilBelow;
  pos base [\uni06B8.medi_LamYaaFina \uni067E.init_HighLD \uni06D1.init_HighLD ] <anchor 365 -1183> mark @TashkilBelow;
  pos base [\uni0777.fina_LamYaaFina ] <anchor 679 -1659> mark @TashkilBelow;
  pos base [\uni06D1.fina_LamYaaFina ] <anchor 753 -1482> mark @TashkilBelow;
  pos base [\uni06D0.fina_LamYaaFina ] <anchor 739 -1419> mark @TashkilBelow;
  pos base [\uni0620.fina_LamYaaFina ] <anchor 669 -1249> mark @TashkilBelow;
  pos base [\uni064A.fina_LamYaaFina ] <anchor 689 -1279> mark @TashkilBelow;
  pos base [\uni0777.medi_KafBaaInit ] <anchor 200 -960> mark @TashkilBelow;
  pos base [\uni067B.medi_KafBaaInit \uni06D0.medi_KafBaaInit ] <anchor 240 -820> mark @TashkilBelow;
  pos base [\uni0625.fina_LamAlfIsol \uni0673.fina_LamAlfIsol ] <anchor 553 -900> mark @TashkilBelow;
  pos base [\uni0773.fina_LamAlfIsol ] <anchor 389 -670> mark @TashkilBelow;
  pos base [\uni077C.medi_LamHaaMemInit ] <anchor 741 -600> mark @TashkilBelow;
  pos base [\uni0777.medi_BaaBaaInit ] <anchor 100 -910> mark @TashkilBelow;
  pos base [\uni0755.medi_BaaBaaInit ] <anchor 60 -710> mark @TashkilBelow;
  pos base [\uni0691.fina_MemRaaIsol \uni0692.fina_MemRaaIsol \uni0697.fina_MemRaaIsol \uni0698.fina_MemRaaIsol \uni0699.fina_MemRaaIsol \uni075B.fina_MemRaaIsol \uni06EF.fina_MemRaaIsol \uni0632.fina_MemRaaIsol \uni0771.fina_MemRaaIsol \uni0631.fina_MemRaaIsol \uni076B.fina_MemRaaIsol \uni076C.fina_MemRaaIsol \uni0691.fina_KafRaaFina \uni0692.fina_KafRaaFina \uni0693.fina_KafRaaFina \uni0697.fina_KafRaaFina \uni0698.fina_KafRaaFina \uni0699.fina_KafRaaFina \uni075B.fina_KafRaaFina \uni06EF.fina_KafRaaFina \uni0632.fina_KafRaaFina \uni0771.fina_KafRaaFina \uni0631.fina_KafRaaFina \uni076B.fina_KafRaaFina \uni076C.fina_KafRaaFina ] <anchor 430 -670> mark @TashkilBelow;
  pos base [\uni0693.fina_MemRaaIsol ] <anchor 268 -938> mark @TashkilBelow;
  pos base [\uni0694.fina_MemRaaIsol \uni0696.fina_MemRaaIsol ] <anchor 300 -1252> mark @TashkilBelow;
  pos base [\uni0695.fina_MemRaaIsol ] <anchor 230 -1442> mark @TashkilBelow;
  pos base [\uni076F.medi_FaaHaaInit ] <anchor 611 -963> mark @TashkilBelow;
  pos base [\uni077C.medi_FaaHaaInit ] <anchor 521 -870> mark @TashkilBelow;
  pos base [\uni076F.init_HaaHaaInit ] <anchor 1197 -952> mark @TashkilBelow;
  pos base [\uni077C.init_HaaHaaInit ] <anchor 1107 -859> mark @TashkilBelow;
  pos base [\uni069B.init_AboveHaa \uni069C.init_AboveHaa ] <anchor 1045 -353> mark @TashkilBelow;
  pos base [\uni0777.init_BaaNonIsol ] <anchor 212 -940> mark @TashkilBelow;
  pos base [\uni0755.init_BaaNonIsol ] <anchor 172 -740> mark @TashkilBelow;
  pos base [\uni0767.fina_BaaNonIsol ] <anchor 566 -860> mark @TashkilBelow;
  pos base [\uni06B9.fina_BaaNonIsol ] <anchor 575 -810> mark @TashkilBelow;
  pos base [\uni0777.init_BaaSenInit ] <anchor 296 -860> mark @TashkilBelow;
  pos base [\uni0777.medi_BaaRaaFina ] <anchor 190 -990> mark @TashkilBelow;
  pos base [\uni06BD.medi_BaaRaaFina \uni0753.medi_BaaRaaFina \uni0752.medi_BaaRaaFina ] <anchor 270 -721> mark @TashkilBelow;
  pos base [\uni0755.medi_BaaRaaFina ] <anchor 270 -790> mark @TashkilBelow;
  pos base [\uni067E.medi_BaaRaaFina \uni06D1.medi_BaaRaaFina ] <anchor 354 -713> mark @TashkilBelow;
  pos base [\uni067B.medi_BaaRaaFina \uni06D0.medi_BaaRaaFina ] <anchor 310 -750> mark @TashkilBelow;
  pos base [\uni0694.fina_BaaRaaFina \uni0696.fina_BaaRaaFina ] <anchor 280 -860> mark @TashkilBelow;
  pos base [\uni0695.fina_BaaRaaFina ] <anchor 210 -1090> mark @TashkilBelow;
  pos base [\uni0694.fina_KafRaaFina \uni0696.fina_KafRaaFina ] <anchor 488 -860> mark @TashkilBelow;
  pos base [\uni0695.fina_KafRaaFina ] <anchor 418 -1090> mark @TashkilBelow;
  pos base [\uni077C.medi_MemHaaMemInit ] <anchor 538 -609> mark @TashkilBelow;
  pos base [\uni0777.init_BaaMemInit ] <anchor 292 -960> mark @TashkilBelow;
  pos base [\uni0680.init_BaaMemInit ] <anchor 323 -770> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaMemInit \uni0753.init_BaaMemInit \uni0752.init_BaaMemInit ] <anchor 293 -741> mark @TashkilBelow;
  pos base [\uni0755.init_BaaMemInit ] <anchor 252 -760> mark @TashkilBelow;
  pos base [\uni067E.init_BaaMemInit \uni06D1.init_BaaMemInit ] <anchor 377 -733> mark @TashkilBelow;
  pos base [\uni067B.init_BaaMemInit \uni06D0.init_BaaMemInit ] <anchor 332 -720> mark @TashkilBelow;
  pos base [\uni0694.fina_KafRaaIsol \uni0696.fina_KafRaaIsol ] <anchor 518 -846> mark @TashkilBelow;
  pos base [\uni0695.fina_KafRaaIsol ] <anchor 448 -1076> mark @TashkilBelow;
  pos base [\uni0777.fina_KafYaaFina ] <anchor 739 -1553> mark @TashkilBelow;
  pos base [\uni06D1.fina_KafYaaFina ] <anchor 823 -1376> mark @TashkilBelow;
  pos base [\uni06D0.fina_KafYaaFina ] <anchor 779 -1313> mark @TashkilBelow;
  pos base [\uni0620.fina_KafYaaFina ] <anchor 739 -1143> mark @TashkilBelow;
  pos base [\uni064A.fina_KafYaaFina ] <anchor 759 -1173> mark @TashkilBelow;
  pos base [\uni076F.medi_LamMemHaaInit ] <anchor 723 -903> mark @TashkilBelow;
  pos base [\uni077C.medi_LamMemHaaInit ] <anchor 633 -810> mark @TashkilBelow;
  pos base [\uni0625.fina_LamAlfFina \uni0673.fina_LamAlfFina ] <anchor 221 -853> mark @TashkilBelow;
  pos base [\uni0774.fina_LamAlfFina ] <anchor 418 -670> mark @TashkilBelow;
  pos base [\uni0777.init_BaaBaaHaaInit ] <anchor 95 -710> mark @TashkilBelow;
  pos base [\uni0777.medi_BaaBaaHaaInit ] <anchor 699 -1020> mark @TashkilBelow;
  pos base [\uni0680.medi_BaaBaaHaaInit ] <anchor 729 -770> mark @TashkilBelow;
  pos base [\uni06BD.medi_BaaBaaHaaInit \uni0753.medi_BaaBaaHaaInit \uni0752.medi_BaaBaaHaaInit ] <anchor 699 -741> mark @TashkilBelow;
  pos base [\uni0755.medi_BaaBaaHaaInit ] <anchor 659 -820> mark @TashkilBelow;
  pos base [\uni067E.medi_BaaBaaHaaInit \uni06D1.medi_BaaBaaHaaInit ] <anchor 783 -733> mark @TashkilBelow;
  pos base [\uni067B.medi_BaaBaaHaaInit \uni06D0.medi_BaaBaaHaaInit ] <anchor 739 -780> mark @TashkilBelow;
  pos base [\uni076F.medi_BaaBaaHaaInit ] <anchor 436 -903> mark @TashkilBelow;
  pos base [\uni077C.medi_BaaBaaHaaInit ] <anchor 346 -810> mark @TashkilBelow;
  pos base [\uni0777.medi_SenBaaMemInit ] <anchor 478 -830> mark @TashkilBelow;
  pos base [\uni0777.init_BaaBaaIsol ] <anchor 230 -970> mark @TashkilBelow;
  pos base [\uni0680.init_BaaBaaIsol ] <anchor 271 -830> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaBaaIsol \uni0753.init_BaaBaaIsol \uni0752.init_BaaBaaIsol ] <anchor 241 -801> mark @TashkilBelow;
  pos base [\uni067E.init_BaaBaaIsol \uni06D1.init_BaaBaaIsol ] <anchor 325 -793> mark @TashkilBelow;
  pos base [\uni067B.init_BaaBaaIsol \uni06D0.init_BaaBaaIsol ] <anchor 270 -730> mark @TashkilBelow;
  pos base [\uni0753.fina_BaaBaaIsol \uni0752.fina_BaaBaaIsol ] <anchor 850 -761> mark @TashkilBelow;
  pos base [\uni0680.fina_BaaBaaIsol ] <anchor 880 -790> mark @TashkilBelow;
  pos base [\uni0755.fina_BaaBaaIsol ] <anchor 810 -830> mark @TashkilBelow;
  pos base [\uni067E.fina_BaaBaaIsol ] <anchor 934 -753> mark @TashkilBelow;
  pos base [\uni067B.fina_BaaBaaIsol ] <anchor 890 -790> mark @TashkilBelow;
  pos base [\uni0777.init_BaaBaaMemInit ] <anchor 177 -690> mark @TashkilBelow;
  pos base [\uni0777.medi_BaaBaaMemInit ] <anchor 76 -980> mark @TashkilBelow;
  pos base [\uni06BD.medi_BaaBaaMemInit \uni0753.medi_BaaBaaMemInit \uni0752.medi_BaaBaaMemInit ] <anchor 76 -731> mark @TashkilBelow;
  pos base [\uni0755.medi_BaaBaaMemInit ] <anchor 36 -780> mark @TashkilBelow;
  pos base [\uni067E.medi_BaaBaaMemInit \uni06D1.medi_BaaBaaMemInit ] <anchor 160 -723> mark @TashkilBelow;
  pos base [\uni067B.medi_BaaBaaMemInit \uni06D0.medi_BaaBaaMemInit ] <anchor 116 -740> mark @TashkilBelow;
  pos base [\uni0777.medi_KafBaaMedi ] <anchor 210 -960> mark @TashkilBelow;
  pos base [\uni067B.medi_KafBaaMedi \uni06D0.medi_KafBaaMedi ] <anchor 250 -820> mark @TashkilBelow;
  pos base [\uni0777.medi_BaaNonFina ] <anchor 159 -990> mark @TashkilBelow;
  pos base [\uni06BD.medi_BaaNonFina \uni0753.medi_BaaNonFina \uni0752.medi_BaaNonFina ] <anchor 159 -721> mark @TashkilBelow;
  pos base [\uni0755.medi_BaaNonFina ] <anchor 119 -790> mark @TashkilBelow;
  pos base [\uni067E.medi_BaaNonFina \uni06D1.medi_BaaNonFina ] <anchor 243 -713> mark @TashkilBelow;
  pos base [\uni067B.medi_BaaNonFina \uni06D0.medi_BaaNonFina ] <anchor 199 -750> mark @TashkilBelow;
  pos base [\uni0767.fina_BaaNonFina ] <anchor 582 -948> mark @TashkilBelow;
  pos base [\uni06B9.fina_BaaNonFina ] <anchor 591 -898> mark @TashkilBelow;
  pos base [\uni076F.init_HaaRaaIsol ] <anchor 679 -903> mark @TashkilBelow;
  pos base [\uni077C.init_HaaRaaIsol ] <anchor 589 -810> mark @TashkilBelow;
  pos base [\uni0694.fina_HaaRaaIsol \uni0696.fina_HaaRaaIsol ] <anchor 260 -860> mark @TashkilBelow;
  pos base [\uni0695.fina_HaaRaaIsol \uni0755.init_BaaBaaIsolLD ] <anchor 190 -1090> mark @TashkilBelow;
  pos base [\uni0694.fina_LamRaaIsol \uni0696.fina_LamRaaIsol ] <anchor 250 -850> mark @TashkilBelow;
  pos base [\uni0695.fina_LamRaaIsol ] <anchor 180 -1080> mark @TashkilBelow;
  pos base [\uni076F.medi_SadHaaInit ] <anchor 570 -963> mark @TashkilBelow;
  pos base [\uni077C.medi_SadHaaInit ] <anchor 480 -870> mark @TashkilBelow;
  pos base [\uni0777.medi_BaaYaaFina ] <anchor 400 -1410> mark @TashkilBelow;
  pos base [\uni0680.medi_BaaYaaFina ] <anchor 541 -1270> mark @TashkilBelow;
  pos base [\uni0776.medi_BaaYaaFina \uni06CE.medi_BaaYaaFina \uni0775.medi_BaaYaaFina \uni064A.medi_BaaYaaFina \uni063D.medi_BaaYaaFina \uni0754.medi_BaaYaaFina \uni06CC.medi_BaaYaaFina \uni0767.medi_BaaYaaFina ] <anchor 531 -1030> mark @TashkilBelow;
  pos base [\uni0750.medi_BaaYaaFina ] <anchor 440 -1080> mark @TashkilBelow;
  pos base [\uni06BD.medi_BaaYaaFina \uni0753.medi_BaaYaaFina \uni0752.medi_BaaYaaFina ] <anchor 511 -1241> mark @TashkilBelow;
  pos base [\uni0620.medi_BaaYaaFina ] <anchor 400 -1000> mark @TashkilBelow;
  pos base [\uni0755.medi_BaaYaaFina ] <anchor 360 -1210> mark @TashkilBelow;
  pos base [\uni067E.medi_BaaYaaFina \uni06D1.medi_BaaYaaFina ] <anchor 595 -1233> mark @TashkilBelow;
  pos base [\uni067B.medi_BaaYaaFina \uni06D0.medi_BaaYaaFina ] <anchor 440 -1170> mark @TashkilBelow;
  pos base [\uni0777.fina_BaaYaaFina \uni0777.fina_PostTooth ] <anchor 754 -1601> mark @TashkilBelow;
  pos base [\uni06D1.fina_BaaYaaFina \uni06D1.fina_PostTooth ] <anchor 837 -1424> mark @TashkilBelow;
  pos base [\uni06D0.fina_BaaYaaFina \uni06D0.fina_PostTooth ] <anchor 794 -1361> mark @TashkilBelow;
  pos base [\uni0620.fina_BaaYaaFina \uni0620.fina_PostTooth ] <anchor 754 -1191> mark @TashkilBelow;
  pos base [\uni064A.fina_BaaYaaFina \uni064A.fina_PostTooth ] <anchor 773 -1221> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaSenAltInit \uni0753.init_BaaSenAltInit \uni0752.init_BaaSenAltInit ] <anchor 307 -829> mark @TashkilBelow;
  pos base [\uni067E.init_BaaSenAltInit \uni06D1.init_BaaSenAltInit ] <anchor 391 -821> mark @TashkilBelow;
  pos base [\uni067B.init_BaaSenAltInit \uni06D0.init_BaaSenAltInit ] <anchor 336 -758> mark @TashkilBelow;
  pos base [\uni0694.fina_PostTooth \uni0696.fina_PostTooth ] <anchor 546 -934> mark @TashkilBelow;
  pos base [\uni0695.fina_PostTooth ] <anchor 476 -1164> mark @TashkilBelow;
  pos base [\uni0777.init_AboveHaa ] <anchor 944 -593> mark @TashkilBelow;
  pos base [\uni0777.init_BaaHaaInit ] <anchor 940 -1010> mark @TashkilBelow;
  pos base [\uni0680.init_BaaHaaInit \uni0776.init_BaaHaaInit \uni06BC.init_BaaHaaInit \uni0750.init_BaaHaaInit \uni0756.init_BaaHaaInit \uni0768.init_BaaHaaInit \uni06CE.init_BaaHaaInit \uni0775.init_BaaHaaInit \uni06BD.init_BaaHaaInit \uni0626.init_BaaHaaInit \uni066E.init_BaaHaaInit \uni0620.init_BaaHaaInit \uni064A.init_BaaHaaInit \uni06BB.init_BaaHaaInit \uni067F.init_BaaHaaInit \uni067D.init_BaaHaaInit \uni067E.init_BaaHaaInit \uni0628.init_BaaHaaInit \uni067A.init_BaaHaaInit \uni0751.init_BaaHaaInit \uni0646.init_BaaHaaInit \uni0753.init_BaaHaaInit \uni0752.init_BaaHaaInit \uni062A.init_BaaHaaInit \uni0678.init_BaaHaaInit \uni063D.init_BaaHaaInit \uni062B.init_BaaHaaInit \uni0679.init_BaaHaaInit \uni06B9.init_BaaHaaInit \uni0769.init_BaaHaaInit \uni0649.init_BaaHaaInit \uni067C.init_BaaHaaInit \uni0754.init_BaaHaaInit \uni06D1.init_BaaHaaInit \uni06BA.init_BaaHaaInit \uni06CC.init_BaaHaaInit \uni0767.init_BaaHaaInit ] <anchor 940 -670> mark @TashkilBelow;
  pos base [\uni0755.init_BaaHaaInit ] <anchor 900 -810> mark @TashkilBelow;
  pos base [\uni067B.init_BaaHaaInit \uni06D0.init_BaaHaaInit ] <anchor 980 -770> mark @TashkilBelow;
  pos base [\uni0777.init_BaaHaaMemInit ] <anchor 438 -845> mark @TashkilBelow;
  pos base [\uni076F.medi_BaaHaaMemInit ] <anchor 395 -870> mark @TashkilBelow;
  pos base [\uni077C.medi_BaaHaaMemInit ] <anchor 305 -777> mark @TashkilBelow;
  pos base [\uni0687.fina_AboveHaaIsol ] <anchor 778 -1139> mark @TashkilBelow;
  pos base [\uni062C.fina_AboveHaaIsol \uni062C.fina_AboveHaaIsol2 ] <anchor 847 -1139> mark @TashkilBelow;
  pos base [\uni0684.fina_AboveHaaIsol ] <anchor 767 -1139> mark @TashkilBelow;
  pos base [\uni076F.fina_AboveHaaIsol \uni076F.fina_AboveHaaIsol2 ] <anchor 790 -1139> mark @TashkilBelow;
  pos base [\uni076E.fina_AboveHaaIsol ] <anchor 810 -1139> mark @TashkilBelow;
  pos base [\uni0683.fina_AboveHaaIsol \uni0758.fina_AboveHaaIsol2 ] <anchor 818 -1139> mark @TashkilBelow;
  pos base [\uni06BF.fina_AboveHaaIsol ] <anchor 832 -1139> mark @TashkilBelow;
  pos base [\uni077C.fina_AboveHaaIsol \uni077C.fina_AboveHaaIsol2 ] <anchor 817 -1139> mark @TashkilBelow;
  pos base [\uni0758.fina_AboveHaaIsol ] <anchor 768 -1139> mark @TashkilBelow;
  pos base [\uni0686.fina_AboveHaaIsol ] <anchor 852 -1139> mark @TashkilBelow;
  pos base [\uni06B8.init_LamHaaHaaInit ] <anchor 345 -833> mark @TashkilBelow;
  pos base [\uni076F.init_Finjani ] <anchor 690 -990> mark @TashkilBelow;
  pos base [\uni06BF.init_Finjani \uni0686.init_Finjani ] <anchor 684 -720> mark @TashkilBelow;
  pos base [\uni077C.init_Finjani ] <anchor 600 -897> mark @TashkilBelow;
  pos base [\uni0758.init_Finjani ] <anchor 600 -728> mark @TashkilBelow;
  pos base [\uni076F.medi_Finjani ] <anchor 660 -958> mark @TashkilBelow;
  pos base [\uni077C.medi_Finjani ] <anchor 570 -865> mark @TashkilBelow;
  pos base [\uni0777.init_High ] <anchor 220 -960> mark @TashkilBelow;
  pos base [\uni0755.init_High ] <anchor 180 -760> mark @TashkilBelow;
  pos base [\uni067B.init_High \uni06D0.init_High ] <anchor 260 -720> mark @TashkilBelow;
  pos base [\uni0777.medi_High ] <anchor 277 -960> mark @TashkilBelow;
  pos base [\uni0680.medi_High ] <anchor 307 -770> mark @TashkilBelow;
  pos base [\uni06BD.medi_High \uni0753.medi_High \uni0752.medi_High ] <anchor 277 -741> mark @TashkilBelow;
  pos base [\uni0755.medi_High ] <anchor 237 -760> mark @TashkilBelow;
  pos base [\uni067E.medi_High \uni06D1.medi_High ] <anchor 361 -733> mark @TashkilBelow;
  pos base [\uni067B.medi_High \uni06D0.medi_High ] <anchor 317 -720> mark @TashkilBelow;
  pos base [\uni069B.fina_BaaSen \uni069C.fina_BaaSen ] <anchor 1739 -833> mark @TashkilBelow;
  pos base [\uni0777.init_Wide ] <anchor 265 -960> mark @TashkilBelow;
  pos base [\uni0680.init_Wide ] <anchor 351 -820> mark @TashkilBelow;
  pos base [\uni0776.init_Wide \uni06CE.init_Wide \uni0775.init_Wide \uni064A.init_Wide \uni063D.init_Wide \uni0754.init_Wide \uni06CC.init_Wide \uni0767.init_Wide ] <anchor 335 -670> mark @TashkilBelow;
  pos base [\uni06BC.init_Wide \uni0628.init_Wide \uni0751.init_Wide \uni06B9.init_Wide \uni067C.init_Wide ] <anchor 305 -670> mark @TashkilBelow;
  pos base [\uni06BD.init_Wide \uni0752.init_Wide ] <anchor 326 -791> mark @TashkilBelow;
  pos base [\uni0620.init_Wide ] <anchor 285 -670> mark @TashkilBelow;
  pos base [\uni0755.init_Wide ] <anchor 275 -760> mark @TashkilBelow;
  pos base [\uni067E.init_Wide \uni06D1.init_Wide ] <anchor 386 -783> mark @TashkilBelow;
  pos base [\uni067B.init_Wide \uni06D0.init_Wide ] <anchor 305 -720> mark @TashkilBelow;
  pos base [\uni0753.init_Wide ] <anchor 446 -791> mark @TashkilBelow;
  pos base [\uni0687.medi_HaaHaaInit \uni0683.medi_HaaHaaInit ] <anchor 472 -670> mark @TashkilBelow;
  pos base [\uni0684.medi_HaaHaaInit \uni076E.medi_HaaHaaInit ] <anchor 482 -670> mark @TashkilBelow;
  pos base [\uni076F.medi_HaaHaaInit ] <anchor 502 -983> mark @TashkilBelow;
  pos base [\uni06BF.medi_HaaHaaInit \uni0686.medi_HaaHaaInit ] <anchor 496 -713> mark @TashkilBelow;
  pos base [\uni077C.medi_HaaHaaInit ] <anchor 412 -890> mark @TashkilBelow;
  pos base [\uni0758.medi_HaaHaaInit ] <anchor 452 -721> mark @TashkilBelow;
  pos base [\uni076F.medi_AynHaaInit ] <anchor 530 -963> mark @TashkilBelow;
  pos base [\uni077C.medi_AynHaaInit ] <anchor 440 -870> mark @TashkilBelow;
  pos base [\uni0687.init_AboveHaa ] <anchor 1308 -590> mark @TashkilBelow;
  pos base [\uni0684.init_AboveHaa ] <anchor 1307 -490> mark @TashkilBelow;
  pos base [\uni076F.init_AboveHaa ] <anchor 1357 -823> mark @TashkilBelow;
  pos base [\uni076E.init_AboveHaa ] <anchor 1337 -470> mark @TashkilBelow;
  pos base [\uni0683.init_AboveHaa ] <anchor 1298 -350> mark @TashkilBelow;
  pos base [\uni06BF.init_AboveHaa \uni0686.init_AboveHaa ] <anchor 1362 -553> mark @TashkilBelow;
  pos base [\uni077C.init_AboveHaa ] <anchor 1267 -730> mark @TashkilBelow;
  pos base [\uni0758.init_AboveHaa ] <anchor 1278 -561> mark @TashkilBelow;
  pos base [\uni0687.fina_AboveHaaIsol2 ] <anchor 848 -1139> mark @TashkilBelow;
  pos base [\uni0684.fina_AboveHaaIsol2 ] <anchor 857 -1139> mark @TashkilBelow;
  pos base [\uni076E.fina_AboveHaaIsol2 ] <anchor 770 -1139> mark @TashkilBelow;
  pos base [\uni0683.fina_AboveHaaIsol2 ] <anchor 838 -1139> mark @TashkilBelow;
  pos base [\uni06BF.fina_AboveHaaIsol2 \uni0686.fina_AboveHaaIsol2 ] <anchor 902 -1139> mark @TashkilBelow;
  pos base [\uni063C.init_KafLam \uni06AE.init_KafLam ] <anchor 312 -853> mark @TashkilBelow;
  pos base [\uni0764.init_KafLam ] <anchor 228 -861> mark @TashkilBelow;
  pos base [\uni06B8.medi_KafLamHehIsol \uni06B8.medi_LamLamHehIsol \uni06B8.medi_LamLamHehFina ] <anchor 361 -709> mark @TashkilBelow;
  pos base [\uni06B8.medi_KafLamYaa \uni06B8.medi_LamLamYaaIsol \uni06B8.medi_LamLamYaaFina ] <anchor 445 -1123> mark @TashkilBelow;
  pos base [\uni0625.fina_KafAlf \uni0673.fina_KafAlf ] <anchor 350 -700> mark @TashkilBelow;
  pos base [\uni0623.fina_KafAlf ] <anchor 37 -670> mark @TashkilBelow;
  pos base [\uni06B8.fina_KafMemLam ] <anchor 760 -1433> mark @TashkilBelow;
  pos base [\uni0625.fina_KafMemAlf \uni0673.fina_KafMemAlf ] <anchor 260 -864> mark @TashkilBelow;
  pos base [\uni0774.fina_KafMemAlf \aBaa.init_BaaBaaHeh \uni0776.init_BaaBaaHeh \uni06BC.init_BaaBaaHeh \uni0750.init_BaaBaaHeh \uni0756.init_BaaBaaHeh \uni0768.init_BaaBaaHeh \uni06CE.init_BaaBaaHeh \uni0775.init_BaaBaaHeh \uni0626.init_BaaBaaHeh \uni066E.init_BaaBaaHeh \uni064A.init_BaaBaaHeh \uni06BB.init_BaaBaaHeh \uni067F.init_BaaBaaHeh \uni067D.init_BaaBaaHeh \uni0628.init_BaaBaaHeh \uni067A.init_BaaBaaHeh \uni0751.init_BaaBaaHeh \uni0646.init_BaaBaaHeh \uni062A.init_BaaBaaHeh \uni0678.init_BaaBaaHeh \uni063D.init_BaaBaaHeh \uni062B.init_BaaBaaHeh \uni0679.init_BaaBaaHeh \uni06B9.init_BaaBaaHeh \uni0769.init_BaaBaaHeh \uni0649.init_BaaBaaHeh \uni067C.init_BaaBaaHeh \uni0754.init_BaaBaaHeh \uni06BA.init_BaaBaaHeh \uni06CC.init_BaaBaaHeh \uni0767.init_BaaBaaHeh ] <anchor 197 -670> mark @TashkilBelow;
  pos base [\uni0773.fina_KafMemAlf ] <anchor 196 -670> mark @TashkilBelow;
  pos base [\uni0623.fina_KafMemAlf ] <anchor 189 -670> mark @TashkilBelow;
  pos base [\uni063C.init_KafHeh \uni06AE.init_KafHeh ] <anchor 310 -733> mark @TashkilBelow;
  pos base [\uni0764.init_KafHeh ] <anchor 226 -741> mark @TashkilBelow;
  pos base [\uni075A.fina_KafDal ] <anchor 433 -800> mark @TashkilBelow;
  pos base [\uni06B8.init_LamHeh ] <anchor 195 -764> mark @TashkilBelow;
  pos base [\uni06B8.medi_LamHeh ] <anchor 358 -783> mark @TashkilBelow;
  pos base [\uni075A.fina_LamDal ] <anchor 452 -736> mark @TashkilBelow;
  pos base [\uni076F.init_HaaMemInit ] <anchor 771 -723> mark @TashkilBelow;
  pos base [\uni077C.init_HaaMemInit ] <anchor 681 -630> mark @TashkilBelow;
  pos base [\uni063C.init_KafYaaIsol \uni06AE.init_KafYaaIsol ] <anchor 595 -733> mark @TashkilBelow;
  pos base [\uni0764.init_KafYaaIsol ] <anchor 511 -741> mark @TashkilBelow;
  pos base [\uni0777.init_BaaYaaIsol ] <anchor 343 -1200> mark @TashkilBelow;
  pos base [\uni0680.init_BaaYaaIsol ] <anchor 373 -910> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaYaaIsol \uni0753.init_BaaYaaIsol \uni0752.init_BaaYaaIsol ] <anchor 343 -881> mark @TashkilBelow;
  pos base [\uni0755.init_BaaYaaIsol ] <anchor 303 -1000> mark @TashkilBelow;
  pos base [\uni067E.init_BaaYaaIsol \uni06D1.init_BaaYaaIsol ] <anchor 427 -873> mark @TashkilBelow;
  pos base [\uni067B.init_BaaYaaIsol \uni06D0.init_BaaYaaIsol ] <anchor 383 -960> mark @TashkilBelow;
  pos base [\uni06B8.init_LamYaaIsol ] <anchor 295 -843> mark @TashkilBelow;
  pos base [\uni0777.fina_KafYaaIsol ] <anchor 728 -1349> mark @TashkilBelow;
  pos base [\uni06D1.fina_KafYaaIsol ] <anchor 812 -1172> mark @TashkilBelow;
  pos base [\uni06D0.fina_KafYaaIsol ] <anchor 768 -1109> mark @TashkilBelow;
  pos base [\uni0620.fina_KafYaaIsol ] <anchor 728 -939> mark @TashkilBelow;
  pos base [\uni064A.fina_KafYaaIsol ] <anchor 748 -969> mark @TashkilBelow;
  pos base [\uni06B8.init_LamMemIsol ] <anchor 265 -687> mark @TashkilBelow;
  pos base [\uni0777.init_BaaMemIsol ] <anchor 603 -957> mark @TashkilBelow;
  pos base [\uni0755.init_BaaMemIsol ] <anchor 563 -757> mark @TashkilBelow;
  pos base [\uni067B.init_BaaMemIsol \uni06D0.init_BaaMemIsol ] <anchor 643 -717> mark @TashkilBelow;
  pos base [\uni0777.medi_BaaMemAlfFina ] <anchor 291 -1210> mark @TashkilBelow;
  pos base [\uni0680.medi_BaaMemAlfFina ] <anchor 321 -1070> mark @TashkilBelow;
  pos base [\uni0776.medi_BaaMemAlfFina \uni06CE.medi_BaaMemAlfFina \uni0775.medi_BaaMemAlfFina \uni064A.medi_BaaMemAlfFina \uni063D.medi_BaaMemAlfFina \uni0754.medi_BaaMemAlfFina \uni06CC.medi_BaaMemAlfFina \uni0767.medi_BaaMemAlfFina ] <anchor 311 -830> mark @TashkilBelow;
  pos base [\uni0750.medi_BaaMemAlfFina ] <anchor 220 -880> mark @TashkilBelow;
  pos base [\uni06BD.medi_BaaMemAlfFina \uni0753.medi_BaaMemAlfFina \uni0752.medi_BaaMemAlfFina ] <anchor 291 -1041> mark @TashkilBelow;
  pos base [\uni0620.medi_BaaMemAlfFina ] <anchor 291 -800> mark @TashkilBelow;
  pos base [\uni0755.medi_BaaMemAlfFina ] <anchor 251 -1010> mark @TashkilBelow;
  pos base [\uni067E.medi_BaaMemAlfFina \uni06D1.medi_BaaMemAlfFina ] <anchor 375 -1033> mark @TashkilBelow;
  pos base [\uni067B.medi_BaaMemAlfFina \uni06D0.medi_BaaMemAlfFina ] <anchor 331 -970> mark @TashkilBelow;
  pos base [\uni0628.medi_BaaMemAlfFina \uni0751.medi_BaaMemAlfFina \uni06B9.medi_BaaMemAlfFina ] <anchor 321 -780> mark @TashkilBelow;
  pos base [\uni0625.fina_MemAlfFina \uni0673.fina_MemAlfFina ] <anchor 270 -1005> mark @TashkilBelow;
  pos base [\uni0773.fina_MemAlfFina ] <anchor 476 -670> mark @TashkilBelow;
  pos base [\uni0777.init_BaaHehInit ] <anchor 513 -1010> mark @TashkilBelow;
  pos base [\uni0755.init_BaaHehInit ] <anchor 473 -810> mark @TashkilBelow;
  pos base [\uni067B.init_BaaHehInit \uni06D0.init_BaaHehInit ] <anchor 553 -770> mark @TashkilBelow;
  pos base [\uni0777.medi_BaaHehMedi ] <anchor 386 -994> mark @TashkilBelow;
  pos base [\uni06BD.medi_BaaHehMedi \uni0753.medi_BaaHehMedi \uni0752.medi_BaaHehMedi ] <anchor 336 -725> mark @TashkilBelow;
  pos base [\uni0755.medi_BaaHehMedi ] <anchor 346 -794> mark @TashkilBelow;
  pos base [\uni067E.medi_BaaHehMedi \uni06D1.medi_BaaHehMedi ] <anchor 420 -717> mark @TashkilBelow;
  pos base [\uni067B.medi_BaaHehMedi \uni06D0.medi_BaaHehMedi ] <anchor 426 -754> mark @TashkilBelow;
  pos base [\uni06B8.medi_KafLamMemFina ] <anchor 525 -1223> mark @TashkilBelow;
  pos base [\uni076A.init_LamLamInit ] <anchor 225 -670> mark @TashkilBelow;
  pos base [\uni06B8.medi_LamLamMedi ] <anchor 245 -783> mark @TashkilBelow;
  pos base [\uni076F.medi_1LamHaaHaaInit ] <anchor 1101 -983> mark @TashkilBelow;
  pos base [\uni06BF.medi_1LamHaaHaaInit \uni0686.medi_1LamHaaHaaInit ] <anchor 1095 -713> mark @TashkilBelow;
  pos base [\uni077C.medi_1LamHaaHaaInit ] <anchor 1011 -890> mark @TashkilBelow;
  pos base [\uni0758.medi_1LamHaaHaaInit ] <anchor 1011 -721> mark @TashkilBelow;
  pos base [\uni076F.medi_2LamHaaHaaInit ] <anchor 250 -983> mark @TashkilBelow;
  pos base [\uni06BF.medi_2LamHaaHaaInit \uni0686.medi_2LamHaaHaaInit ] <anchor 244 -713> mark @TashkilBelow;
  pos base [\uni077C.medi_2LamHaaHaaInit ] <anchor 160 -890> mark @TashkilBelow;
  pos base [\uni0758.medi_2LamHaaHaaInit ] <anchor 160 -721> mark @TashkilBelow;
  pos base [\uni0625.LowHamza \uni0673.LowHamza ] <anchor 328 -1220> mark @TashkilBelow;
  pos base [\uni0680.init_LD ] <anchor 191 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_LD \uni0753.init_LD \uni0752.init_LD ] <anchor 161 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_LD \uni06D1.init_LD ] <anchor 245 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_LD \uni06D0.init_LD ] <anchor 140 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_LD \uni0751.init_LD \uni06B9.init_LD ] <anchor 130 -930> mark @TashkilBelow;
  pos base [\uni0767.init_LD \uni063D.init_LD \uni0776.init_LD \uni0775.init_LD \uni06CC.init_LD \uni064A.init_LD \uni06CE.init_LD \uni0754.init_LD ] <anchor 181 -980> mark @TashkilBelow;
  pos base [\uni0777.init_LD ] <anchor 100 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_LD ] <anchor 90 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_LD ] <anchor 60 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaRaaIsolLD ] <anchor 225 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaRaaIsolLD \uni0753.init_BaaRaaIsolLD \uni0752.init_BaaRaaIsolLD ] <anchor 195 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaRaaIsolLD \uni06D1.init_BaaRaaIsolLD ] <anchor 279 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaRaaIsolLD \uni06D0.init_BaaRaaIsolLD ] <anchor 234 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaRaaIsolLD \uni0751.init_BaaRaaIsolLD \uni06B9.init_BaaRaaIsolLD ] <anchor 224 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaRaaIsolLD \uni063D.init_BaaRaaIsolLD \uni0776.init_BaaRaaIsolLD \uni0775.init_BaaRaaIsolLD \uni06CC.init_BaaRaaIsolLD \uni064A.init_BaaRaaIsolLD \uni06CE.init_BaaRaaIsolLD \uni0754.init_BaaRaaIsolLD ] <anchor 215 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaRaaIsolLD ] <anchor 194 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaRaaIsolLD ] <anchor 124 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaRaaIsolLD ] <anchor 154 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaDalLD ] <anchor 110 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaDalLD \uni0753.init_BaaDalLD \uni0752.init_BaaDalLD ] <anchor 80 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaDalLD \uni06D1.init_BaaDalLD ] <anchor 164 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaDalLD \uni06D0.init_BaaDalLD ] <anchor 120 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaDalLD \uni0751.init_BaaDalLD \uni06B9.init_BaaDalLD ] <anchor 110 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaDalLD \uni063D.init_BaaDalLD \uni0776.init_BaaDalLD \uni0775.init_BaaDalLD \uni06CC.init_BaaDalLD \uni064A.init_BaaDalLD \uni06CE.init_BaaDalLD \uni0754.init_BaaDalLD ] <anchor 100 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaDalLD ] <anchor 80 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaDalLD ] <anchor 9 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaDalLD ] <anchor 40 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaMemHaaInitLD ] <anchor 361 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaMemHaaInitLD \uni0753.init_BaaMemHaaInitLD \uni0752.init_BaaMemHaaInitLD ] <anchor 331 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaMemHaaInitLD \uni06D1.init_BaaMemHaaInitLD ] <anchor 415 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaMemHaaInitLD \uni06D0.init_BaaMemHaaInitLD \uni067B.init_WideLD \uni06D0.init_WideLD ] <anchor 371 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaMemHaaInitLD \uni0751.init_BaaMemHaaInitLD \uni06B9.init_BaaMemHaaInitLD \uni0628.init_WideLD \uni0751.init_WideLD \uni06B9.init_WideLD ] <anchor 361 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaMemHaaInitLD \uni063D.init_BaaMemHaaInitLD \uni0776.init_BaaMemHaaInitLD \uni0775.init_BaaMemHaaInitLD \uni06CC.init_BaaMemHaaInitLD \uni064A.init_BaaMemHaaInitLD \uni06CE.init_BaaMemHaaInitLD \uni0754.init_BaaMemHaaInitLD ] <anchor 351 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaMemHaaInitLD \uni0777.init_WideLD ] <anchor 331 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaMemHaaInitLD ] <anchor 260 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaMemHaaInitLD \uni0755.init_WideLD ] <anchor 291 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaBaaYaaLD ] <anchor 153 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaBaaYaaLD \uni0753.init_BaaBaaYaaLD \uni0752.init_BaaBaaYaaLD ] <anchor 123 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaBaaYaaLD \uni06D1.init_BaaBaaYaaLD ] <anchor 207 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaBaaYaaLD \uni06D0.init_BaaBaaYaaLD ] <anchor 187 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaBaaYaaLD \uni0751.init_BaaBaaYaaLD \uni06B9.init_BaaBaaYaaLD ] <anchor 177 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaBaaYaaLD \uni063D.init_BaaBaaYaaLD \uni0776.init_BaaBaaYaaLD \uni0775.init_BaaBaaYaaLD \uni06CC.init_BaaBaaYaaLD \uni064A.init_BaaBaaYaaLD \uni06CE.init_BaaBaaYaaLD \uni0754.init_BaaBaaYaaLD ] <anchor 143 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaBaaYaaLD ] <anchor 147 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaBaaYaaLD ] <anchor 52 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaBaaYaaLD ] <anchor 107 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaNonIsolLD ] <anchor 242 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaNonIsolLD \uni0753.init_BaaNonIsolLD \uni0752.init_BaaNonIsolLD ] <anchor 212 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaNonIsolLD \uni06D1.init_BaaNonIsolLD ] <anchor 296 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaNonIsolLD \uni06D0.init_BaaNonIsolLD \uni0755.init_BaaMemInitLD ] <anchor 252 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaNonIsolLD \uni0751.init_BaaNonIsolLD \uni06B9.init_BaaNonIsolLD ] <anchor 242 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaNonIsolLD \uni063D.init_BaaNonIsolLD \uni0776.init_BaaNonIsolLD \uni0775.init_BaaNonIsolLD \uni06CC.init_BaaNonIsolLD \uni064A.init_BaaNonIsolLD \uni06CE.init_BaaNonIsolLD \uni0754.init_BaaNonIsolLD ] <anchor 232 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaNonIsolLD ] <anchor 212 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaNonIsolLD ] <anchor 141 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaNonIsolLD ] <anchor 172 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaSenInitLD ] <anchor 337 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaSenInitLD \uni0753.init_BaaSenInitLD \uni0752.init_BaaSenInitLD ] <anchor 307 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaSenInitLD \uni06D1.init_BaaSenInitLD ] <anchor 391 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaSenInitLD \uni06D0.init_BaaSenInitLD ] <anchor 336 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaSenInitLD \uni0751.init_BaaSenInitLD \uni06B9.init_BaaSenInitLD ] <anchor 326 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaSenInitLD \uni063D.init_BaaSenInitLD \uni0776.init_BaaSenInitLD \uni0775.init_BaaSenInitLD \uni06CC.init_BaaSenInitLD \uni064A.init_BaaSenInitLD \uni06CE.init_BaaSenInitLD \uni0754.init_BaaSenInitLD ] <anchor 327 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaSenInitLD ] <anchor 296 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaSenInitLD ] <anchor 236 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaSenInitLD ] <anchor 256 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaMemInitLD ] <anchor 323 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaMemInitLD \uni0753.init_BaaMemInitLD \uni0752.init_BaaMemInitLD ] <anchor 293 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaMemInitLD \uni06D1.init_BaaMemInitLD ] <anchor 377 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaMemInitLD \uni06D0.init_BaaMemInitLD ] <anchor 332 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaMemInitLD \uni0751.init_BaaMemInitLD \uni06B9.init_BaaMemInitLD ] <anchor 322 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaMemInitLD \uni063D.init_BaaMemInitLD \uni0776.init_BaaMemInitLD \uni0775.init_BaaMemInitLD \uni06CC.init_BaaMemInitLD \uni064A.init_BaaMemInitLD \uni06CE.init_BaaMemInitLD \uni0754.init_BaaMemInitLD ] <anchor 313 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaMemInitLD ] <anchor 292 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaMemInitLD ] <anchor 222 -1030> mark @TashkilBelow;
  pos base [\uni0680.init_BaaBaaHaaInitLD ] <anchor 236 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaBaaHaaInitLD \uni0753.init_BaaBaaHaaInitLD \uni0752.init_BaaBaaHaaInitLD ] <anchor 206 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaBaaHaaInitLD \uni06D1.init_BaaBaaHaaInitLD ] <anchor 290 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaBaaHaaInitLD \uni06D0.init_BaaBaaHaaInitLD ] <anchor 134 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaBaaHaaInitLD \uni0751.init_BaaBaaHaaInitLD \uni06B9.init_BaaBaaHaaInitLD ] <anchor 124 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaBaaHaaInitLD \uni063D.init_BaaBaaHaaInitLD \uni0776.init_BaaBaaHaaInitLD \uni0775.init_BaaBaaHaaInitLD \uni06CC.init_BaaBaaHaaInitLD \uni064A.init_BaaBaaHaaInitLD \uni06CE.init_BaaBaaHaaInitLD \uni0754.init_BaaBaaHaaInitLD ] <anchor 226 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaBaaHaaInitLD ] <anchor 94 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaBaaHaaInitLD ] <anchor 135 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaBaaHaaInitLD ] <anchor 54 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaBaaIsolLD ] <anchor 261 -1190> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaBaaIsolLD \uni0753.init_BaaBaaIsolLD \uni0752.init_BaaBaaIsolLD ] <anchor 231 -1161> mark @TashkilBelow;
  pos base [\uni067E.init_BaaBaaIsolLD \uni06D1.init_BaaBaaIsolLD ] <anchor 315 -1153> mark @TashkilBelow;
  pos base [\uni067B.init_BaaBaaIsolLD \uni06D0.init_BaaBaaIsolLD ] <anchor 270 -1090> mark @TashkilBelow;
  pos base [\uni0628.init_BaaBaaIsolLD \uni0751.init_BaaBaaIsolLD \uni06B9.init_BaaBaaIsolLD ] <anchor 260 -900> mark @TashkilBelow;
  pos base [\uni0767.init_BaaBaaIsolLD \uni063D.init_BaaBaaIsolLD \uni0776.init_BaaBaaIsolLD \uni0775.init_BaaBaaIsolLD \uni06CC.init_BaaBaaIsolLD \uni064A.init_BaaBaaIsolLD \uni06CE.init_BaaBaaIsolLD \uni0754.init_BaaBaaIsolLD ] <anchor 251 -950> mark @TashkilBelow;
  pos base [\uni0777.init_BaaBaaIsolLD ] <anchor 230 -1230> mark @TashkilBelow;
  pos base [\uni0750.init_BaaBaaIsolLD ] <anchor 160 -1000> mark @TashkilBelow;
  pos base [\uni0680.init_BaaBaaMemInitLD ] <anchor 207 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaBaaMemInitLD \uni0753.init_BaaBaaMemInitLD \uni0752.init_BaaBaaMemInitLD ] <anchor 177 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaBaaMemInitLD \uni06D1.init_BaaBaaMemInitLD ] <anchor 261 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaBaaMemInitLD \uni06D0.init_BaaBaaMemInitLD ] <anchor 217 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaBaaMemInitLD \uni0751.init_BaaBaaMemInitLD \uni06B9.init_BaaBaaMemInitLD ] <anchor 207 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaBaaMemInitLD \uni063D.init_BaaBaaMemInitLD \uni0776.init_BaaBaaMemInitLD \uni0775.init_BaaBaaMemInitLD \uni06CC.init_BaaBaaMemInitLD \uni064A.init_BaaBaaMemInitLD \uni06CE.init_BaaBaaMemInitLD \uni0754.init_BaaBaaMemInitLD ] <anchor 197 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaBaaMemInitLD ] <anchor 177 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaBaaMemInitLD ] <anchor 106 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaBaaMemInitLD ] <anchor 137 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaSenAltInitLD ] <anchor 293 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaSenAltInitLD \uni0753.init_BaaSenAltInitLD \uni0752.init_BaaSenAltInitLD ] <anchor 263 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaSenAltInitLD \uni06D1.init_BaaSenAltInitLD ] <anchor 347 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaSenAltInitLD \uni06D0.init_BaaSenAltInitLD ] <anchor 302 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaSenAltInitLD \uni0751.init_BaaSenAltInitLD \uni06B9.init_BaaSenAltInitLD ] <anchor 292 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaSenAltInitLD \uni063D.init_BaaSenAltInitLD \uni0776.init_BaaSenAltInitLD \uni0775.init_BaaSenAltInitLD \uni06CC.init_BaaSenAltInitLD \uni064A.init_BaaSenAltInitLD \uni06CE.init_BaaSenAltInitLD \uni0754.init_BaaSenAltInitLD ] <anchor 283 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaSenAltInitLD ] <anchor 262 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaSenAltInitLD ] <anchor 192 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaSenAltInitLD ] <anchor 222 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaHaaInitLD ] <anchor 981 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaHaaInitLD \uni0753.init_BaaHaaInitLD \uni0752.init_BaaHaaInitLD ] <anchor 951 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaHaaInitLD \uni06D1.init_BaaHaaInitLD ] <anchor 1035 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaHaaInitLD \uni06D0.init_BaaHaaInitLD ] <anchor 980 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaHaaInitLD \uni0751.init_BaaHaaInitLD \uni06B9.init_BaaHaaInitLD ] <anchor 970 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaHaaInitLD \uni063D.init_BaaHaaInitLD \uni0776.init_BaaHaaInitLD \uni0775.init_BaaHaaInitLD \uni06CC.init_BaaHaaInitLD \uni064A.init_BaaHaaInitLD \uni06CE.init_BaaHaaInitLD \uni0754.init_BaaHaaInitLD ] <anchor 971 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaHaaInitLD ] <anchor 940 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaHaaInitLD ] <anchor 880 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaHaaInitLD ] <anchor 900 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaHaaMemInitLD ] <anchor 468 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaHaaMemInitLD \uni0753.init_BaaHaaMemInitLD \uni0752.init_BaaHaaMemInitLD ] <anchor 438 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaHaaMemInitLD \uni06D1.init_BaaHaaMemInitLD ] <anchor 522 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaHaaMemInitLD \uni06D0.init_BaaHaaMemInitLD ] <anchor 478 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaHaaMemInitLD \uni0751.init_BaaHaaMemInitLD \uni06B9.init_BaaHaaMemInitLD ] <anchor 468 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaHaaMemInitLD \uni063D.init_BaaHaaMemInitLD \uni0776.init_BaaHaaMemInitLD \uni0775.init_BaaHaaMemInitLD \uni06CC.init_BaaHaaMemInitLD \uni064A.init_BaaHaaMemInitLD \uni06CE.init_BaaHaaMemInitLD \uni0754.init_BaaHaaMemInitLD ] <anchor 458 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaHaaMemInitLD ] <anchor 438 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaHaaMemInitLD ] <anchor 367 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaHaaMemInitLD ] <anchor 398 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_HighLD ] <anchor 311 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_HighLD \uni0753.init_HighLD \uni0752.init_HighLD ] <anchor 281 -1191> mark @TashkilBelow;
  pos base [\uni067B.init_HighLD \uni06D0.init_HighLD ] <anchor 260 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_HighLD \uni0751.init_HighLD \uni06B9.init_HighLD ] <anchor 250 -930> mark @TashkilBelow;
  pos base [\uni0767.init_HighLD \uni063D.init_HighLD \uni0776.init_HighLD \uni0775.init_HighLD \uni06CC.init_HighLD \uni064A.init_HighLD \uni06CE.init_HighLD \uni0754.init_HighLD ] <anchor 301 -980> mark @TashkilBelow;
  pos base [\uni0777.init_HighLD ] <anchor 220 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_HighLD ] <anchor 210 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_HighLD ] <anchor 180 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_WideLD ] <anchor 362 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_WideLD \uni0753.init_WideLD \uni0752.init_WideLD ] <anchor 332 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_WideLD \uni06D1.init_WideLD ] <anchor 416 -1183> mark @TashkilBelow;
  pos base [\uni0767.init_WideLD \uni063D.init_WideLD \uni0776.init_WideLD \uni0775.init_WideLD \uni06CC.init_WideLD \uni064A.init_WideLD \uni06CE.init_WideLD \uni0754.init_WideLD ] <anchor 352 -980> mark @TashkilBelow;
  pos base [\uni0750.init_WideLD ] <anchor 261 -1030> mark @TashkilBelow;
  pos base [\uni0680.init_BaaYaaIsolLD ] <anchor 373 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaYaaIsolLD \uni0753.init_BaaYaaIsolLD \uni0752.init_BaaYaaIsolLD ] <anchor 343 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaYaaIsolLD \uni06D1.init_BaaYaaIsolLD ] <anchor 427 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaYaaIsolLD \uni06D0.init_BaaYaaIsolLD ] <anchor 383 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaYaaIsolLD \uni0751.init_BaaYaaIsolLD \uni06B9.init_BaaYaaIsolLD ] <anchor 373 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaYaaIsolLD \uni063D.init_BaaYaaIsolLD \uni0776.init_BaaYaaIsolLD \uni0775.init_BaaYaaIsolLD \uni06CC.init_BaaYaaIsolLD \uni064A.init_BaaYaaIsolLD \uni06CE.init_BaaYaaIsolLD \uni0754.init_BaaYaaIsolLD ] <anchor 363 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaYaaIsolLD ] <anchor 343 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaYaaIsolLD ] <anchor 272 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaYaaIsolLD ] <anchor 303 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaMemIsolLD ] <anchor 633 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaMemIsolLD \uni0753.init_BaaMemIsolLD \uni0752.init_BaaMemIsolLD ] <anchor 603 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaMemIsolLD \uni06D1.init_BaaMemIsolLD ] <anchor 687 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaMemIsolLD \uni06D0.init_BaaMemIsolLD ] <anchor 643 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaMemIsolLD \uni0751.init_BaaMemIsolLD \uni06B9.init_BaaMemIsolLD ] <anchor 633 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaMemIsolLD \uni063D.init_BaaMemIsolLD \uni0776.init_BaaMemIsolLD \uni0775.init_BaaMemIsolLD \uni06CC.init_BaaMemIsolLD \uni064A.init_BaaMemIsolLD \uni06CE.init_BaaMemIsolLD \uni0754.init_BaaMemIsolLD ] <anchor 623 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaMemIsolLD ] <anchor 603 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaMemIsolLD ] <anchor 532 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaMemIsolLD ] <anchor 563 -1120> mark @TashkilBelow;
  pos base [\uni0680.init_BaaHehInitLD ] <anchor 544 -1220> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaHehInitLD \uni0753.init_BaaHehInitLD \uni0752.init_BaaHehInitLD ] <anchor 514 -1191> mark @TashkilBelow;
  pos base [\uni067E.init_BaaHehInitLD \uni06D1.init_BaaHehInitLD ] <anchor 598 -1183> mark @TashkilBelow;
  pos base [\uni067B.init_BaaHehInitLD \uni06D0.init_BaaHehInitLD ] <anchor 553 -1120> mark @TashkilBelow;
  pos base [\uni0628.init_BaaHehInitLD \uni0751.init_BaaHehInitLD \uni06B9.init_BaaHehInitLD ] <anchor 543 -930> mark @TashkilBelow;
  pos base [\uni0767.init_BaaHehInitLD \uni063D.init_BaaHehInitLD \uni0776.init_BaaHehInitLD \uni0775.init_BaaHehInitLD \uni06CC.init_BaaHehInitLD \uni064A.init_BaaHehInitLD \uni06CE.init_BaaHehInitLD \uni0754.init_BaaHehInitLD ] <anchor 534 -980> mark @TashkilBelow;
  pos base [\uni0777.init_BaaHehInitLD ] <anchor 513 -1260> mark @TashkilBelow;
  pos base [\uni0750.init_BaaHehInitLD ] <anchor 443 -1030> mark @TashkilBelow;
  pos base [\uni0755.init_BaaHehInitLD ] <anchor 473 -1120> mark @TashkilBelow;
  pos base [\uni0620.initLD ] <anchor 170 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaRaaIsolLD ] <anchor 194 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaDalLD ] <anchor 80 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaMemHaaInitLD \uni0620.init_WideLD ] <anchor 331 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaBaaYaaLD ] <anchor 147 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaNonIsolLD ] <anchor 212 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaSenInitLD ] <anchor 296 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaMemInitLD ] <anchor 292 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaBaaHaaInitLD ] <anchor 94 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaBaaIsolLD ] <anchor 294 -1164> mark @TashkilBelow;
  pos base [\uni0620.init_BaaBaaMemInitLD ] <anchor 177 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaSenAltInitLD ] <anchor 262 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaHaaInitLD ] <anchor 940 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaHaaMemInitLD ] <anchor 438 -950> mark @TashkilBelow;
  pos base [\uni0620.init_HighLD ] <anchor 220 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaYaaIsolLD ] <anchor 343 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaMemIsolLD ] <anchor 603 -950> mark @TashkilBelow;
  pos base [\uni0620.init_BaaHehInitLD ] <anchor 513 -950> mark @TashkilBelow;
  pos base [\aSen.init_YaaBarree \uni06FA.init_YaaBarree \uni076D.init_YaaBarree \uni0633.init_YaaBarree \uni077E.init_YaaBarree \uni077D.init_YaaBarree \uni0634.init_YaaBarree \uni0770.init_YaaBarree \uni075C.init_YaaBarree \uni069A.init_YaaBarree \uni069B.init_YaaBarree \uni069C.init_YaaBarree ] <anchor 469 -670> mark @TashkilBelow;
  pos base [\aSad.init_YaaBarree \uni069D.init_YaaBarree \uni06FB.init_YaaBarree \uni0636.init_YaaBarree \uni069E.init_YaaBarree \uni0635.init_YaaBarree ] <anchor 496 -670> mark @TashkilBelow;
  pos base [\aBaa.init_YaaBarree \aFaa.init_YaaBarree \aLam.init_YaaBarree \aKaf.init_YaaBarree \uni063B.init_YaaBarree \uni063C.init_YaaBarree \uni077F.init_YaaBarree \uni0764.init_YaaBarree \uni0643.init_YaaBarree \uni06B0.init_YaaBarree \uni06B3.init_YaaBarree \uni06B2.init_YaaBarree \uni06AB.init_YaaBarree \uni06AC.init_YaaBarree \uni06AD.init_YaaBarree \uni06AE.init_YaaBarree \uni06AF.init_YaaBarree \uni06A9.init_YaaBarree \uni06B4.init_YaaBarree \uni0763.init_YaaBarree \uni0762.init_YaaBarree \uni06B1.init_YaaBarree \uni0777.init_YaaBarree \uni0680.init_YaaBarree \uni0776.init_YaaBarree \uni06BC.init_YaaBarree \uni0750.init_YaaBarree \uni0756.init_YaaBarree \uni0768.init_YaaBarree \uni06CE.init_YaaBarree \uni0775.init_YaaBarree \uni06BD.init_YaaBarree \uni0626.init_YaaBarree \uni066E.init_YaaBarree \uni0620.init_YaaBarree \uni064A.init_YaaBarree \uni06BB.init_YaaBarree \uni067F.init_YaaBarree \uni0755.init_YaaBarree \uni067D.init_YaaBarree \uni067E.init_YaaBarree \uni067B.init_YaaBarree \uni0628.init_YaaBarree \uni067A.init_YaaBarree \uni0751.init_YaaBarree \uni0646.init_YaaBarree \uni0753.init_YaaBarree \uni0752.init_YaaBarree \uni062A.init_YaaBarree \uni0678.init_YaaBarree \uni063D.init_YaaBarree \uni062B.init_YaaBarree \uni0679.init_YaaBarree \uni06B9.init_YaaBarree \uni0769.init_YaaBarree \uni0649.init_YaaBarree \uni067C.init_YaaBarree \uni0754.init_YaaBarree \uni06D1.init_YaaBarree \uni06D0.init_YaaBarree \uni06BA.init_YaaBarree \uni06CC.init_YaaBarree \uni0767.init_YaaBarree \uni06B5.init_YaaBarree \uni06B7.init_YaaBarree \uni0644.init_YaaBarree \uni06B8.init_YaaBarree \uni06B6.init_YaaBarree \uni076A.init_YaaBarree \uni066F.init_YaaBarree \uni0761.init_YaaBarree \uni0760.init_YaaBarree \uni0642.init_YaaBarree \uni0641.init_YaaBarree \uni06A8.init_YaaBarree \uni06A1.init_YaaBarree \uni06A2.init_YaaBarree \uni06A3.init_YaaBarree \uni06A4.init_YaaBarree \uni06A5.init_YaaBarree \uni06A6.init_YaaBarree \uni06A7.init_YaaBarree \uni08A0.init_YaaBarree ] <anchor 201 -670> mark @TashkilBelow;
  pos base [\aHaa.init_YaaBarree \uni062E.init_YaaBarree \uni062D.init_YaaBarree \uni0681.init_YaaBarree \uni0687.init_YaaBarree \uni0685.init_YaaBarree \uni062C.init_YaaBarree \uni0682.init_YaaBarree \uni0757.init_YaaBarree \uni0684.init_YaaBarree \uni076E.init_YaaBarree \uni0683.init_YaaBarree \uni06BF.init_YaaBarree \uni077C.init_YaaBarree \uni0758.init_YaaBarree \uni0772.init_YaaBarree \uni0686.init_YaaBarree ] <anchor 911 -670> mark @TashkilBelow;
  pos base [\aAyn.init_YaaBarree \uni06FC.init_YaaBarree \uni063A.init_YaaBarree \uni075E.init_YaaBarree \uni075D.init_YaaBarree \uni075F.init_YaaBarree \uni06A0.init_YaaBarree \uni0639.init_YaaBarree ] <anchor 840 -670> mark @TashkilBelow;
  pos base [\uni076F.init_YaaBarree ] <anchor 1207 -1104> mark @TashkilBelow;
  pos base [\aHeh.medi_HehYaaFina \uni0647.medi_HehYaaFina \uni06C1.medi_HehYaaFina ] <anchor 758 -670> mark @TashkilBelow;
  pos base [\uni0647.medi_PostToothHehYaa \uni06C1.medi_PostToothHehYaa ] <anchor 902 -670> mark @TashkilBelow;
  pos base [\uni08A0.fina ] <anchor 860 -920> mark @TashkilBelow;
  pos base [\uni08A0.init ] <anchor 130 -820> mark @TashkilBelow;
  pos base [\uni08A0 ] <anchor 960 -870> mark @TashkilBelow;
  pos base [\uni08A0.medi ] <anchor 220 -820> mark @TashkilBelow;
  pos base [\uni08A0.medi_BaaMemFina ] <anchor 407 -1050> mark @TashkilBelow;
  pos base [\uni08A0.medi_LamBaaMemInit ] <anchor 227 -820> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaDal ] <anchor 40 -820> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaMemHaaInit ] <anchor 291 -880> mark @TashkilBelow;
  pos base [\uni08A0.medi_BaaBaaYaa ] <anchor 397 -1089> mark @TashkilBelow;
  pos base [\uni08A0.medi_KafBaaInit ] <anchor 160 -820> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaNonIsol ] <anchor 172 -800> mark @TashkilBelow;
  pos base [\uni08A0.medi_BaaRaaFina ] <anchor 230 -850> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaMemInit ] <anchor 252 -820> mark @TashkilBelow;
  pos base [\uni08A0.medi_BaaBaaHaaInit ] <anchor 659 -880> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaBaaIsol ] <anchor 190 -830> mark @TashkilBelow;
  pos base [\uni08A0.fina_BaaBaaIsol ] <anchor 810 -890> mark @TashkilBelow;
  pos base [\uni08A0.medi_BaaBaaMemInit ] <anchor 36 -840> mark @TashkilBelow;
  pos base [\uni08A0.medi_KafBaaMedi ] <anchor 170 -820> mark @TashkilBelow;
  pos base [\uni08A0.medi_BaaNonFina ] <anchor 119 -850> mark @TashkilBelow;
  pos base [\uni08A0.medi_BaaYaaFina ] <anchor 360 -1270> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaSenAltInit ] <anchor 256 -858> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaHaaInit ] <anchor 940 -870> mark @TashkilBelow;
  pos base [\uni08A0.init_High ] <anchor 180 -820> mark @TashkilBelow;
  pos base [\uni08A0.medi_High ] <anchor 237 -820> mark @TashkilBelow;
  pos base [\uni08A0.init_Wide ] <anchor 375 -820> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaYaaIsol ] <anchor 303 -1060> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaMemIsol ] <anchor 563 -817> mark @TashkilBelow;
  pos base [\uni08A0.medi_BaaMemAlfFina ] <anchor 251 -1070> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaHehInit ] <anchor 473 -870> mark @TashkilBelow;
  pos base [\uni08A0.medi_BaaHehMedi ] <anchor 346 -854> mark @TashkilBelow;
  pos base [\uni08A0.init_LD ] <anchor 130 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaRaaIsolLD ] <anchor 154 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaDalLD ] <anchor 40 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaMemHaaInitLD \uni08A0.init_WideLD ] <anchor 291 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaBaaYaaLD ] <anchor 107 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaNonIsolLD ] <anchor 172 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaSenInitLD ] <anchor 256 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaMemInitLD ] <anchor 252 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaBaaHaaInitLD ] <anchor 54 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaBaaIsolLD ] <anchor 254 -1434> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaBaaMemInitLD ] <anchor 137 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaSenAltInitLD ] <anchor 256 -1358> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaHaaInitLD ] <anchor 940 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaHaaMemInitLD ] <anchor 398 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_HighLD ] <anchor 180 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaYaaIsolLD ] <anchor 303 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaMemIsolLD ] <anchor 563 -1220> mark @TashkilBelow;
  pos base [\uni08A0.init_BaaHehInitLD ] <anchor 473 -1220> mark @TashkilBelow;
  pos base [\uni0777.init_BaaBaaHeh \uni0777.init_BaaBaaHehLD ] <anchor 197 -1020> mark @TashkilBelow;
  pos base [\uni0680.init_BaaBaaHeh ] <anchor 203 -980> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaBaaHeh \uni0753.init_BaaBaaHeh \uni0752.init_BaaBaaHeh ] <anchor 173 -951> mark @TashkilBelow;
  pos base [\uni0755.init_BaaBaaHeh ] <anchor 157 -880> mark @TashkilBelow;
  pos base [\uni067E.init_BaaBaaHeh \uni06D1.init_BaaBaaHeh ] <anchor 257 -943> mark @TashkilBelow;
  pos base [\uni067B.init_BaaBaaHeh \uni06D0.init_BaaBaaHeh ] <anchor 237 -880> mark @TashkilBelow;
  pos base [\uni0680.init_BaaBaaHehLD ] <anchor 203 -1320> mark @TashkilBelow;
  pos base [\uni06BD.init_BaaBaaHehLD \uni0753.init_BaaBaaHehLD \uni0752.init_BaaBaaHehLD ] <anchor 173 -1291> mark @TashkilBelow;
  pos base [\uni067E.init_BaaBaaHehLD \uni06D1.init_BaaBaaHehLD ] <anchor 257 -1283> mark @TashkilBelow;
  pos base [\uni067B.init_BaaBaaHehLD \uni06D0.init_BaaBaaHehLD ] <anchor 237 -1220> mark @TashkilBelow;
  pos base [\uni0628.init_BaaBaaHehLD \uni0751.init_BaaBaaHehLD \uni06B9.init_BaaBaaHehLD ] <anchor 227 -1030> mark @TashkilBelow;
  pos base [\uni0767.init_BaaBaaHehLD \uni063D.init_BaaBaaHehLD \uni0776.init_BaaBaaHehLD \uni0775.init_BaaBaaHehLD \uni06CC.init_BaaBaaHehLD \uni064A.init_BaaBaaHehLD \uni06CE.init_BaaBaaHehLD \uni0754.init_BaaBaaHehLD ] <anchor 193 -1080> mark @TashkilBelow;
  pos base [\uni0750.init_BaaBaaHehLD ] <anchor 102 -1130> mark @TashkilBelow;
  pos base [\uni0755.init_BaaBaaHehLD ] <anchor 157 -1220> mark @TashkilBelow;
} markTashkilBelowBase;

lookup markHamzaBelow {
  lookupflag 0;
  markClass [\uni0655 \uni065F ] <anchor 0 -500> @HamzaBelow;
  pos base [\aAlf.fina \aAlf.fina_Narrow ] <anchor 250 -110> mark @HamzaBelow;
  pos base [\aAlf.isol ] <anchor 330 -80> mark @HamzaBelow;
  pos base [\aBaa.fina ] <anchor 900 -450> mark @HamzaBelow;
  pos base [\aBaa.isol ] <anchor 1000 -300> mark @HamzaBelow;
  pos base [\aDal.fina ] <anchor 500 -250> mark @HamzaBelow;
  pos base [\aDal.isol ] <anchor 500 -300> mark @HamzaBelow;
  pos base [\aHaa.fina \aHaa.isol ] <anchor 800 -320> mark @HamzaBelow;
  pos base [\aHaa.init ] <anchor 620 -202> mark @HamzaBelow;
  pos base [\aHaa.medi ] <anchor 600 -190> mark @HamzaBelow;
  pos base [\aYaa.fina \uni0649.fina ] <anchor 749 -997> mark @HamzaBelow;
  pos base [\aYaa.isol \uni0649 ] <anchor 727 -709> mark @HamzaBelow;
  pos base [\aBaa.init_BaaRaaIsol \uni0649.init_BaaRaaIsol ] <anchor 194 -200> mark @HamzaBelow;
  pos base [\aYaa.fina_FaaYaaFina \uni0649.fina_FaaYaaFina ] <anchor 728 -1089> mark @HamzaBelow;
  pos base [\aHaa.medi_LamLamHaaInit ] <anchor 581 -190> mark @HamzaBelow;
  pos base [\aBaa.medi_BaaMemFina \uni0649.medi_BaaMemFina ] <anchor 447 -580> mark @HamzaBelow;
  pos base [\aBaa.medi_LamBaaMemInit \uni0649.medi_LamBaaMemInit ] <anchor 267 -200> mark @HamzaBelow;
  pos base [\aBaa.init_BaaDal \uni0649.init_BaaDal ] <anchor 80 -350> mark @HamzaBelow;
  pos base [\aDal.fina_BaaDal ] <anchor 542 -270> mark @HamzaBelow;
  pos base [\aBaa.init_BaaMemHaaInit \uni0649.init_BaaMemHaaInit ] <anchor 331 -410> mark @HamzaBelow;
  pos base [\aHaa.medi_BaaMemHaaInit ] <anchor 312 -230> mark @HamzaBelow;
  pos base [\aBaa.init_BaaBaaYaa \uni0649.init_BaaBaaYaa ] <anchor 147 -284> mark @HamzaBelow;
  pos base [\aBaa.medi_BaaBaaYaa \uni0649.medi_BaaBaaYaa ] <anchor 437 -619> mark @HamzaBelow;
  pos base [\aYaa.fina_BaaBaaYaa \uni0649.fina_BaaBaaYaa ] <anchor 759 -891> mark @HamzaBelow;
  pos base [\aYaa.fina_LamYaaFina \uni0649.fina_LamYaaFina ] <anchor 669 -1049> mark @HamzaBelow;
  pos base [\aBaa.medi_KafBaaInit \uni0649.medi_KafBaaInit ] <anchor 300 -200> mark @HamzaBelow;
  pos base [\aAlf.fina_LamAlfIsol ] <anchor 553 -280> mark @HamzaBelow;
  pos base [\aHaa.medi_LamHaaMemInit ] <anchor 741 10> mark @HamzaBelow;
  pos base [\aBaa.medi_BaaBaaInit \uni0649.medi_BaaBaaInit ] <anchor 200 -150> mark @HamzaBelow;
  pos base [\aHaa.medi_FaaHaaInit ] <anchor 521 -260> mark @HamzaBelow;
  pos base [\aHaa.init_HaaHaaInit ] <anchor 1107 -249> mark @HamzaBelow;
  pos base [\aBaa.init_BaaSenInit \uni0649.init_BaaSenInit ] <anchor 296 -250> mark @HamzaBelow;
  pos base [\aBaa.medi_BaaRaaFina \uni0649.medi_BaaRaaFina ] <anchor 270 -380> mark @HamzaBelow;
  pos base [\aHaa.medi_MemHaaMemInit ] <anchor 538 1> mark @HamzaBelow;
  pos base [\aBaa.init_BaaMemInit \uni0649.init_BaaMemInit ] <anchor 292 -200> mark @HamzaBelow;
  pos base [\aYaa.fina_KafYaaFina \uni0649.fina_KafYaaFina ] <anchor 739 -943> mark @HamzaBelow;
  pos base [\aHaa.medi_LamMemHaaInit ] <anchor 633 -200> mark @HamzaBelow;
  pos base [\aAlf.fina_LamAlfFina ] <anchor 221 -233> mark @HamzaBelow;
  pos base [\aBaa.init_BaaBaaHaaInit \uni0649.init_BaaBaaHaaInit ] <anchor 95 -100> mark @HamzaBelow;
  pos base [\aBaa.medi_BaaBaaHaaInit \uni0649.medi_BaaBaaHaaInit ] <anchor 699 -410> mark @HamzaBelow;
  pos base [\aHaa.medi_BaaBaaHaaInit ] <anchor 346 -200> mark @HamzaBelow;
  pos base [\aBaa.medi_SenBaaMemInit \uni0649.medi_SenBaaMemInit ] <anchor 478 -220> mark @HamzaBelow;
  pos base [\aBaa.init_BaaBaaIsol \uni0649.init_BaaBaaIsol ] <anchor 230 -360> mark @HamzaBelow;
  pos base [\aBaa.fina_BaaBaaIsol ] <anchor 850 -420> mark @HamzaBelow;
  pos base [\aBaa.init_BaaBaaMemInit \uni0649.init_BaaBaaMemInit ] <anchor 177 -80> mark @HamzaBelow;
  pos base [\aBaa.medi_BaaBaaMemInit \uni0649.medi_BaaBaaMemInit ] <anchor 76 -370> mark @HamzaBelow;
  pos base [\aBaa.medi_KafBaaMedi \uni0649.medi_KafBaaMedi ] <anchor 310 -200> mark @HamzaBelow;
  pos base [\aBaa.medi_BaaNonFina \uni0649.medi_BaaNonFina ] <anchor 159 -380> mark @HamzaBelow;
  pos base [\aHaa.init_HaaRaaIsol ] <anchor 589 -200> mark @HamzaBelow;
  pos base [\aHaa.medi_SadHaaInit ] <anchor 480 -260> mark @HamzaBelow;
  pos base [\aBaa.medi_BaaYaaFina \uni0649.medi_BaaYaaFina ] <anchor 400 -800> mark @HamzaBelow;
  pos base [\aYaa.fina_BaaYaaFina \aYaa.fina_PostTooth \uni0649.fina_BaaYaaFina \uni0649.fina_PostTooth ] <anchor 754 -991> mark @HamzaBelow;
  pos base [\aBaa.init_BaaSenAltInit \uni0649.init_BaaSenAltInit ] <anchor 296 -388> mark @HamzaBelow;
  pos base [\aBaa.init_AboveHaa \uni0649.init_AboveHaa ] <anchor 714 17> mark @HamzaBelow;
  pos base [\aBaa.init_BaaHaaInit \uni0649.init_BaaHaaInit ] <anchor 980 -200> mark @HamzaBelow;
  pos base [\aBaa.init_BaaHaaMemInit \uni0649.init_BaaHaaMemInit ] <anchor 438 -235> mark @HamzaBelow;
  pos base [\aHaa.medi_BaaHaaMemInit ] <anchor 305 -167> mark @HamzaBelow;
  pos base [\aHaa.fina_AboveHaaIsol \aHaa.fina_AboveHaaIsol2 ] <anchor 817 -90> mark @HamzaBelow;
  pos base [\aHaa.medi_1LamHaaHaaInit ] <anchor 1011 -280> mark @HamzaBelow;
  pos base [\aHaa.medi_2LamHaaHaaInit ] <anchor 160 -280> mark @HamzaBelow;
  pos base [\aHaa.init_Finjani ] <anchor 600 -287> mark @HamzaBelow;
  pos base [\aHaa.medi_Finjani ] <anchor 570 -255> mark @HamzaBelow;
  pos base [\aBaa.medi_High \uni0649.medi_High ] <anchor 166 -182> mark @HamzaBelow;
  pos base [\aHaa.medi_HaaHaaInit ] <anchor 362 -280> mark @HamzaBelow;
  pos base [\aHaa.medi_AynHaaInit ] <anchor 440 -260> mark @HamzaBelow;
  pos base [\aHaa.init_AboveHaa ] <anchor 1267 -120> mark @HamzaBelow;
  pos base [\aAlf.fina_KafAlf ] <anchor 350 -80> mark @HamzaBelow;
  pos base [\aAlf.fina_KafMemAlf ] <anchor 260 -244> mark @HamzaBelow;
  pos base [\aDal.fina_KafDal ] <anchor 396 -330> mark @HamzaBelow;
  pos base [\aDal.fina_LamDal ] <anchor 492 -326> mark @HamzaBelow;
  pos base [\aHaa.init_HaaMemInit ] <anchor 681 -20> mark @HamzaBelow;
  pos base [\aBaa.init_BaaYaaIsol \uni0649.init_BaaYaaIsol ] <anchor 343 -590> mark @HamzaBelow;
  pos base [\aHaa.init_HaaYaaIsol ] <anchor 759 0> mark @HamzaBelow;
  pos base [\aYaa.fina_KafYaaIsol \uni0649.fina_KafYaaIsol ] <anchor 728 -739> mark @HamzaBelow;
  pos base [\aBaa.init_BaaMemIsol \uni0649.init_BaaMemIsol ] <anchor 603 -347> mark @HamzaBelow;
  pos base [\aBaa.medi_BaaMemAlfFina \uni0649.medi_BaaMemAlfFina ] <anchor 291 -600> mark @HamzaBelow;
  pos base [\aAlf.fina_MemAlfFina ] <anchor 270 -385> mark @HamzaBelow;
  pos base [\aBaa.init_BaaHehInit \uni0649.init_BaaHehInit ] <anchor 513 -400> mark @HamzaBelow;
  pos base [\aBaa.medi_BaaHehMedi \uni0649.medi_BaaHehMedi ] <anchor 386 -384> mark @HamzaBelow;
  pos base [\uni0649.medi ] <anchor 270 -190> mark @HamzaBelow;
  pos base [\aBaa.init_YaaBarree \uni0649.init_YaaBarree ] <anchor 74 -263> mark @HamzaBelow;
  pos base [\aHaa.init_YaaBarree ] <anchor 1117 -221> mark @HamzaBelow;
  pos base [\aBaa.init_BaaBaaHeh \uni0649.init_BaaBaaHeh ] <anchor 197 -410> mark @HamzaBelow;
} markHamzaBelow;

lookup markSmallAlefAbove {
  lookupflag 0;
  markClass [\uni0670 ] <anchor 395 1640> @AlefAbove;
  markClass [\uni06E4 ] <anchor 0 1300> @AlefAbove;
  markClass [\uni06EB ] <anchor 179 250> @AlefAbove;
  pos base [\aWaw.isol ] <anchor 800 650> mark @AlefAbove;
  pos base [\aYaa.fina ] <anchor 600 -100> mark @AlefAbove;
  pos base [\aYaa.isol \uni0777 \uni06D1 \uni06D0 \uni0649 \uni06CD \uni06CC \uni0620 \uni064A ] <anchor 600 350> mark @AlefAbove;
  pos base [\aYaa.fina_FaaYaaFina \uni0777.fina_FaaYaaFina \uni06D1.fina_FaaYaaFina \uni0775.fina_FaaYaaFina \uni063F.fina_FaaYaaFina \uni0678.fina_FaaYaaFina \uni063D.fina_FaaYaaFina \uni063E.fina_FaaYaaFina \uni06D0.fina_FaaYaaFina \uni0649.fina_FaaYaaFina \uni0776.fina_FaaYaaFina \uni06CD.fina_FaaYaaFina \uni06CC.fina_FaaYaaFina \uni0626.fina_FaaYaaFina \uni0620.fina_FaaYaaFina \uni064A.fina_FaaYaaFina \uni06CE.fina_FaaYaaFina ] <anchor 570 0> mark @AlefAbove;
  pos base [\aYaa.fina_BaaBaaYaa \uni0777.fina_BaaBaaYaa \uni06D1.fina_BaaBaaYaa \uni06D0.fina_BaaBaaYaa \uni0649.fina_BaaBaaYaa \uni06CD.fina_BaaBaaYaa \uni06CC.fina_BaaBaaYaa \uni0620.fina_BaaBaaYaa \uni064A.fina_BaaBaaYaa ] <anchor 600 200> mark @AlefAbove;
  pos base [\aYaa.fina_LamYaaFina \aYaa.fina_BaaYaaFina \aYaa.fina_PostTooth \uni0777.fina \uni06D1.fina \uni06D0.fina \uni0649.fina \uni06CD.fina \uni06CC.fina \uni0620.fina \uni064A.fina \uni0777.fina_LamYaaFina \uni06D1.fina_LamYaaFina \uni06D0.fina_LamYaaFina \uni0649.fina_LamYaaFina \uni06CD.fina_LamYaaFina \uni06CC.fina_LamYaaFina \uni0620.fina_LamYaaFina \uni064A.fina_LamYaaFina \uni0777.fina_BaaYaaFina \uni06D1.fina_BaaYaaFina \uni06D0.fina_BaaYaaFina \uni0649.fina_BaaYaaFina \uni06CD.fina_BaaYaaFina \uni06CC.fina_BaaYaaFina \uni0620.fina_BaaYaaFina \uni064A.fina_BaaYaaFina \uni0777.fina_PostTooth \uni06D1.fina_PostTooth \uni06D0.fina_PostTooth \uni0649.fina_PostTooth \uni06CD.fina_PostTooth \uni06CC.fina_PostTooth \uni0620.fina_PostTooth \uni064A.fina_PostTooth ] <anchor 600 0> mark @AlefAbove;
  pos base [\aYaa.fina_KafYaaFina \uni0777.fina_KafYaaFina \uni06D1.fina_KafYaaFina \uni0775.fina_KafYaaFina \uni063F.fina_KafYaaFina \uni0678.fina_KafYaaFina \uni063D.fina_KafYaaFina \uni063E.fina_KafYaaFina \uni06D0.fina_KafYaaFina \uni0649.fina_KafYaaFina \uni0776.fina_KafYaaFina \uni06CD.fina_KafYaaFina \uni06CC.fina_KafYaaFina \uni0626.fina_KafYaaFina \uni0620.fina_KafYaaFina \uni064A.fina_KafYaaFina \uni06CE.fina_KafYaaFina ] <anchor 297 515> mark @AlefAbove;
  pos base [\aYaa.fina_KafYaaIsol \uni0777.fina_KafYaaIsol \uni06D1.fina_KafYaaIsol \uni0775.fina_KafYaaIsol \uni063F.fina_KafYaaIsol \uni0678.fina_KafYaaIsol \uni063D.fina_KafYaaIsol \uni063E.fina_KafYaaIsol \uni06D0.fina_KafYaaIsol \uni0649.fina_KafYaaIsol \uni0776.fina_KafYaaIsol \uni06CD.fina_KafYaaIsol \uni06CC.fina_KafYaaIsol \uni0626.fina_KafYaaIsol \uni0620.fina_KafYaaIsol \uni064A.fina_KafYaaIsol \uni06CE.fina_KafYaaIsol ] <anchor 270 730> mark @AlefAbove;
  pos base [\uni0649.init ] <anchor 190 600> mark @AlefAbove;
  pos base [\uni0649.medi ] <anchor 280 980> mark @AlefAbove;
  pos base [\uni0648.fina \uni0648 ] <anchor 500 650> mark @AlefAbove;
  pos base [\uni0775.fina \uni063F.fina \uni0678.fina \uni063D.fina \uni063E.fina \uni0776.fina \uni0626.fina \uni06CE.fina ] <anchor 330 500> mark @AlefAbove;
  pos base [\uni0775 \uni063F \uni0678 \uni063D \uni063E \uni0776 \uni0626 \uni06CE ] <anchor 385 828> mark @AlefAbove;
  pos base [\uni0649.init_BaaRaaIsol \uni0649.medi_LamBaaMemInit \uni0649.init_BaaBaaYaa \uni0649.medi_BaaRaaFina \uni0649.init_BaaBaaMemInit \uni0649.medi_BaaNonFina ] <anchor 150 980> mark @AlefAbove;
  pos base [\uni0648.fina_LamWawFina ] <anchor 400 650> mark @AlefAbove;
  pos base [\uni0649.medi_BaaMemFina ] <anchor 380 980> mark @AlefAbove;
  pos base [\uni0649.init_BaaDal \uni0649.init_BaaNonIsol ] <anchor 100 980> mark @AlefAbove;
  pos base [\uni0649.init_BaaMemHaaInit ] <anchor 102 1640> mark @AlefAbove;
  pos base [\uni0649.medi_BaaBaaYaa ] <anchor 390 980> mark @AlefAbove;
  pos base [\uni0775.fina_BaaBaaYaa \uni063F.fina_BaaBaaYaa \uni0678.fina_BaaBaaYaa \uni063D.fina_BaaBaaYaa \uni063E.fina_BaaBaaYaa \uni0776.fina_BaaBaaYaa \uni0626.fina_BaaBaaYaa \uni06CE.fina_BaaBaaYaa ] <anchor 326 795> mark @AlefAbove;
  pos base [\uni0775.fina_LamYaaFina \uni063F.fina_LamYaaFina \uni0678.fina_LamYaaFina \uni063D.fina_LamYaaFina \uni063E.fina_LamYaaFina \uni0776.fina_LamYaaFina \uni0626.fina_LamYaaFina \uni06CE.fina_LamYaaFina ] <anchor 382 535> mark @AlefAbove;
  pos base [\uni0649.medi_BaaBaaInit ] <anchor 180 980> mark @AlefAbove;
  pos base [\uni0649.init_BaaSenInit ] <anchor 164 1290> mark @AlefAbove;
  pos base [\uni0649.init_BaaMemInit ] <anchor 143 1249> mark @AlefAbove;
  pos base [\uni0649.init_BaaBaaHaaInit ] <anchor 143 1208> mark @AlefAbove;
  pos base [\uni0649.medi_BaaBaaHaaInit ] <anchor 655 1208> mark @AlefAbove;
  pos base [\uni0649.medi_SenBaaMemInit \uni0649.medi_BaaBaaMemInit \uni0649.init_BaaYaaIsol ] <anchor 150 1110> mark @AlefAbove;
  pos base [\uni0649.init_BaaBaaIsol ] <anchor 200 980> mark @AlefAbove;
  pos base [\uni0649.medi_BaaYaaFina ] <anchor 350 980> mark @AlefAbove;
  pos base [\uni0775.fina_BaaYaaFina \uni063F.fina_BaaYaaFina \uni0678.fina_BaaYaaFina \uni063D.fina_BaaYaaFina \uni063E.fina_BaaYaaFina \uni0776.fina_BaaYaaFina \uni0626.fina_BaaYaaFina \uni06CE.fina_BaaYaaFina ] <anchor 355 552> mark @AlefAbove;
  pos base [\uni0775.fina_PostTooth \uni063F.fina_PostTooth \uni0678.fina_PostTooth \uni063D.fina_PostTooth \uni063E.fina_PostTooth \uni0776.fina_PostTooth \uni0626.fina_PostTooth \uni06CE.fina_PostTooth ] <anchor 355 550> mark @AlefAbove;
  pos base [\uni0649.init_BaaHaaMemInit ] <anchor 276 1478> mark @AlefAbove;
  pos base [\uni0649.init_High ] <anchor 90 980> mark @AlefAbove;
  pos base [\uni0649.medi_High ] <anchor 230 980> mark @AlefAbove;
  pos base [\uni0649.init_Wide \uni0649.medi_BaaHehMedi ] <anchor 340 980> mark @AlefAbove;
  pos base [\uni0649.init_BaaMemIsol ] <anchor 510 980> mark @AlefAbove;
  pos base [\uni0649.medi_BaaMemAlfFina ] <anchor 220 980> mark @AlefAbove;
  pos base [\uni0649.init_BaaHehInit ] <anchor 440 980> mark @AlefAbove;
  pos base [\uni0640.1 ] <anchor 190 500> mark @AlefAbove;
} markSmallAlefAbove;

lookup mkmkTashkiltoTashkilAbove {
  lookupflag 0;
  markClass [\uni0670 ] <anchor 400 1776> @TashkilTashkilAbove;
  markClass [\uni064B.small \uni064E.small \uni08F0.small ] <anchor 0 1950> @TashkilTashkilAbove;
  markClass [\uni08F1.small ] <anchor 450 1750> @TashkilTashkilAbove;
  markClass [\uni064F.small ] <anchor 0 1870> @TashkilTashkilAbove;
  markClass [\uni0652.small2 ] <anchor -50 2206> @TashkilTashkilAbove;
  markClass [\uni064E.small2 ] <anchor -40 2185> @TashkilTashkilAbove;
  markClass [\uni064C.small ] <anchor 600 1750> @TashkilTashkilAbove;
  markClass [\uni06E4 ] <anchor 0 1658> @TashkilTashkilAbove;
  markClass [\uni06E2 ] <anchor 450 2150> @TashkilTashkilAbove;
  markClass [\uni06EC ] <anchor 228 1640> @TashkilTashkilAbove;
  markClass [\uni0657 ] <anchor 556 1640> @TashkilTashkilAbove;
  pos mark [\uni0654 \uni0674 \hamza.above.wavy \hamza.above ] <anchor -50 1470> mark @TashkilTashkilAbove;
  pos mark [\uni065A \uni065B \smallv.above \smallv.above.inverted ] <anchor 0 1500> mark @TashkilTashkilAbove;
  pos mark [\uni064B ] <anchor 287 2170> mark @TashkilTashkilAbove;
  pos mark [\uni064C ] <anchor 187 2170> mark @TashkilTashkilAbove;
  pos mark [\uni064E ] <anchor 297 1955> mark @TashkilTashkilAbove;
  pos mark [\uni064F ] <anchor 400 2200> mark @TashkilTashkilAbove;
  pos mark [\uni0651 ] <anchor 410 2054> mark @TashkilTashkilAbove;
  pos mark [\uni06E1 ] <anchor 366 2190> mark @TashkilTashkilAbove;
  pos mark [\uni08F0 ] <anchor 287 2200> mark @TashkilTashkilAbove;
  pos mark [\uni08F1 ] <anchor 170 2200> mark @TashkilTashkilAbove;
  pos mark [\uni06EC ] <anchor 190 2170> mark @TashkilTashkilAbove;
  pos mark [\uni0657 ] <anchor 615 2464> mark @TashkilTashkilAbove;
  pos mark [\uni030A ] <anchor 256 2214> mark @TashkilTashkilAbove;
} mkmkTashkiltoTashkilAbove;

lookup mkmkTashkiltoTashkilAbove2 {
  lookupflag 0;
  markClass [\uni06E4 ] <anchor 0 1658> @TashkilTashkilAbove2;
  markClass [\uni06E2 ] <anchor 450 2150> @TashkilTashkilAbove2;
  pos mark [\uni0670 ] <anchor 245 2520> mark @TashkilTashkilAbove2;
  pos mark [\uni064E.small ] <anchor 0 2400> mark @TashkilTashkilAbove2;
  pos mark [\uni064F.small ] <anchor -80 2350> mark @TashkilTashkilAbove2;
} mkmkTashkiltoTashkilAbove2;

lookup mkmkTashkiltoTashkilBelow {
  lookupflag 0;
  markClass [\uni0650.small2 ] <anchor 0 -555> @TashkilTashkilBelow;
  markClass [\uni08F2 ] <anchor 670 -700> @TashkilTashkilBelow;
  markClass [\uni06ED ] <anchor 746 -842> @TashkilTashkilBelow;
  pos mark [\uni0655 \uni065F ] <anchor 0 -1120> mark @TashkilTashkilBelow;
  pos mark [\uni064D ] <anchor 287 -889> mark @TashkilTashkilBelow;
  pos mark [\uni0650 ] <anchor 297 -819> mark @TashkilTashkilBelow;
  pos mark [\uni0650.small2 ] <anchor -170 -550> mark @TashkilTashkilBelow;
  pos mark [\uni08F2 ] <anchor 297 -889> mark @TashkilTashkilBelow;
  pos mark [\uni0325 ] <anchor 256 -1197> mark @TashkilTashkilBelow;
} mkmkTashkiltoTashkilBelow;

lookup markSeenAbove {
  lookupflag 0;
  markClass [\uni06DC ] <anchor 510 2700> @SeenAbove;
  pos base [\uni0627.fina \uni0627.fina_Narrow ] <anchor 100 1600> mark @SeenAbove;
  pos base [\uni0635.medi ] <anchor 740 800> mark @SeenAbove;
} markSeenAbove;

lookup markSeenBelow {
  lookupflag 0;
  markClass [\uni06E3 ] <anchor 510 -400> @SeenBelow;
  pos base [\uni0635.medi ] <anchor 740 -400> mark @SeenBelow;
} markSeenBelow;

lookup markNoonAbove {
  lookupflag 0;
  markClass [\uni06E8 ] <anchor 290 1640> @NoonAbove;
  pos base [\uni0646.init_BaaHaaInit ] <anchor 100 1000> mark @NoonAbove;
} markNoonAbove;

feature kern {

  script DFLT;
     language dflt ;
      lookup kernHorizontalKerninginLatinloo;

  script arab;
     language dflt ;
      lookup kernHorizontalKerninginLatinloo;
     language ARA  exclude_dflt;
      lookup kernHorizontalKerninginLatinloo;
     language SND  exclude_dflt;
      lookup kernHorizontalKerninginLatinloo;
     language URD  exclude_dflt;
      lookup kernHorizontalKerninginLatinloo;

  script latn;
     language dflt ;
      lookup kernHorizontalKerninginLatinloo;
     language TRK  exclude_dflt;
      lookup kernHorizontalKerninginLatinloo;
} kern;

feature curs {

  script DFLT;
     language dflt ;
      lookup cursCursiveAttachment;

  script arab;
     language dflt ;
      lookup cursCursiveAttachment;
     language ARA  exclude_dflt;
      lookup cursCursiveAttachment;
     language SND  exclude_dflt;
      lookup cursCursiveAttachment;
     language URD  exclude_dflt;
      lookup cursCursiveAttachment;
} curs;

feature mark {

  script DFLT;
     language dflt ;
      lookup markGenericMarkAnchor;
      lookup markHamzaAbove;
      lookup markTashkilAboveBase;
      lookup markTashkilBelowBase;
      lookup markHamzaBelow;
      lookup markSmallAlefAbove;
      lookup markSeenAbove;
      lookup markSeenBelow;
      lookup markNoonAbove;

  script arab;
     language dflt ;
      lookup markGenericMarkAnchor;
      lookup markHamzaAbove;
      lookup markTashkilAboveBase;
      lookup markTashkilBelowBase;
      lookup markHamzaBelow;
      lookup markSmallAlefAbove;
      lookup markSeenAbove;
      lookup markSeenBelow;
      lookup markNoonAbove;
     language ARA  exclude_dflt;
      lookup markGenericMarkAnchor;
      lookup markHamzaAbove;
      lookup markTashkilAboveBase;
      lookup markTashkilBelowBase;
      lookup markHamzaBelow;
      lookup markSmallAlefAbove;
      lookup markSeenAbove;
      lookup markSeenBelow;
      lookup markNoonAbove;
     language SND  exclude_dflt;
      lookup markGenericMarkAnchor;
      lookup markHamzaAbove;
      lookup markTashkilAboveBase;
      lookup markTashkilBelowBase;
      lookup markHamzaBelow;
      lookup markSmallAlefAbove;
      lookup markSeenAbove;
      lookup markSeenBelow;
      lookup markNoonAbove;
     language URD  exclude_dflt;
      lookup markGenericMarkAnchor;
      lookup markHamzaAbove;
      lookup markTashkilAboveBase;
      lookup markTashkilBelowBase;
      lookup markHamzaBelow;
      lookup markSmallAlefAbove;
      lookup markSeenAbove;
      lookup markSeenBelow;
      lookup markNoonAbove;

  script latn;
     language dflt ;
      lookup markGenericMarkAnchor;
     language TRK  exclude_dflt;
      lookup markGenericMarkAnchor;
} mark;

feature mkmk {

  script DFLT;
     language dflt ;
      lookup mkmkTashkiltoTashkilAbove;
      lookup mkmkTashkiltoTashkilAbove2;
      lookup mkmkTashkiltoTashkilBelow;

  script arab;
     language dflt ;
      lookup mkmkTashkiltoTashkilAbove;
      lookup mkmkTashkiltoTashkilAbove2;
      lookup mkmkTashkiltoTashkilBelow;
     language ARA  exclude_dflt;
      lookup mkmkTashkiltoTashkilAbove;
      lookup mkmkTashkiltoTashkilAbove2;
      lookup mkmkTashkiltoTashkilBelow;
     language SND  exclude_dflt;
      lookup mkmkTashkiltoTashkilAbove;
      lookup mkmkTashkiltoTashkilAbove2;
      lookup mkmkTashkiltoTashkilBelow;
     language URD  exclude_dflt;
      lookup mkmkTashkiltoTashkilAbove;
      lookup mkmkTashkiltoTashkilAbove2;
      lookup mkmkTashkiltoTashkilBelow;
} mkmk;
#Mark attachment classes (defined in GDEF, used in lookupflags)

@GDEF_Simple = [\aAlf.fina.alt \aAlf.fina \aAlf.isol \aAyn.fina \aAyn.init 
	\aAyn.isol \aAyn.medi \aBaa.fina \aBaa.init \aBaa.isol \aBaa.medi \aDal.fina 
	\aDal.isol \aFaa.fina \aFaa.init \aFaa.isol \aFaa.medi \aHaa.fina \aHaa.init 
	\aHaa.isol \aHaa.medi \aHeh.fina \aHeh.init \aHeh.isol \aHeh.medi \aKaf.fina 
	\aKaf.init.alt \aKaf.init \aKaf.isol \aKaf.medi \aLam.fina \aLam.init 
	\aLam.isol \aLam.medi \aMem.fina.alt \aMem.fina \aMem.init \aMem.isol 
	\aMem.medi \aNon.fina.alt \aNon.fina \aNon.isol.alt \aNon.isol \aQaf.fina 
	\aQaf.isol \aRaa.fina.alt2 \aRaa.fina \aRaa.isol \aSad.fina \aSad.init 
	\aSad.isol \aSad.medi \aSen.fina \aSen.init \aSen.isol \aSen.medi \aTaa.fina 
	\aTaa.init \aTaa.isol \aTaa.medi \aWaw.fina \aWaw.isol \aYaa.fina \aYaa.isol 
	\uni0621 \aLam.medi_LamMemFina \aMem.fina_LamMemFina \aBaa.init_BaaRaaIsol 
	\aRaa.fina_BaaRaaIsol \aLam.medi_LamWawFina \aWaw.fina_LamWawFina 
	\aLam.init_LamHaaInit \aFaa.medi_FaaYaaFina \aYaa.fina_FaaYaaFina 
	\aLam.init_LamLamHaaInit \aLam.medi_LamLamHaaInit \aHaa.medi_LamLamHaaInit 
	\aBaa.medi_BaaMemFina \aMem.fina_BaaMemFina \aSad.init_AboveHaa 
	\aLam.init_LamBaaMemInit \aBaa.medi_LamBaaMemInit \aMem.medi_LamBaaMemInit 
	\aBaa.init_BaaDal \aDal.fina_BaaDal \aBaa.init_BaaMemHaaInit 
	\aMem.medi_BaaMemHaaInit \aHaa.medi_BaaMemHaaInit \aBaa.init_BaaBaaYaa 
	\aBaa.medi_BaaBaaYaa \aYaa.fina_BaaBaaYaa \aLam.medi_LamYaaFina 
	\aYaa.fina_LamYaaFina \aKaf.init_KafBaaInit \aBaa.medi_KafBaaInit 
	\aLam.init_LamMemInit \aMem.medi_LamMemInit \aLam.init_LamAlfIsol 
	\aAlf.fina_LamAlfIsol \aLam.init_LamHaaMemInit \aHaa.medi_LamHaaMemInit 
	\aMem.medi_LamHaaMemInit \aBaa.medi_BaaBaaInit \aAyn.medi_AynYaaFina 
	\aMem.init_MemRaaIsol \aRaa.fina_MemRaaIsol \aFaa.init_FaaHaaInit 
	\aHaa.medi_FaaHaaInit \aHaa.init_HaaHaaInit \aLam.medi_LamQafFina 
	\aQaf.fina_LamQafFina \aSen.init_AboveHaa \aMem.init_MemHaaInit 
	\aBaa.init_BaaNonIsol \aNon.fina_BaaNonIsol \aKaf.medi_KafMemFina 
	\aMem.fina_KafMemFina \aBaa.init_BaaSenInit \aSen.medi_BaaSenInit 
	\aBaa.medi_BaaRaaFina \aRaa.fina_BaaRaaFina \aKaf.medi_KafRaaFina 
	\aRaa.fina_KafRaaFina \aLam.init_LamHehInit \aHeh.medi_LamHehInit 
	\aMem.init_MemHaaMemInit \aHaa.medi_MemHaaMemInit \aBaa.init_BaaMemInit 
	\aMem.medi_BaaMemInit \aSen.init_SenHaaInit \aKaf.init_KafRaaIsol 
	\aRaa.fina_KafRaaIsol \aAyn.init_AynHaaInit \aKaf.medi_KafYaaFina 
	\aYaa.fina_KafYaaFina \aLam.init_LamMemHaaInit \aMem.medi_LamMemHaaInit 
	\aHaa.medi_LamMemHaaInit \aLam.medi_LamAlfFina \aAlf.fina_LamAlfFina 
	\aLam.medi_LamMemMedi \aMem.medi_LamMemMedi \uni0644.init_Lellah 
	\uni0644.medi_Lellah \uni0647.fina_Lellah \aBaa.init_BaaBaaHaaInit 
	\aBaa.medi_BaaBaaHaaInit \aHaa.medi_BaaBaaHaaInit \aBaa.medi_SenBaaMemInit 
	\aMem.medi_SenBaaMemInit \aBaa.init_BaaBaaIsol \aBaa.fina_BaaBaaIsol 
	\aBaa.init_BaaBaaMemInit \aBaa.medi_BaaBaaMemInit \aMem.medi_BaaBaaMemInit 
	\aKaf.medi_KafBaaMedi \aBaa.medi_KafBaaMedi \aBaa.medi_BaaNonFina 
	\aNon.fina_BaaNonFina \aHaa.init_HaaRaaIsol \aRaa.fina_HaaRaaIsol 
	\aHeh.init_HehHaaInit \aLam.init_LamRaaIsol \aRaa.fina_LamRaaIsol 
	\aSad.init_SadHaaInit \aHaa.medi_SadHaaInit \uni061B \uni060C \uni061F 
	\aBaa.medi_BaaYaaFina \aYaa.fina_BaaYaaFina \aBaa.init_BaaSenAltInit 
	\aSen.medi_BaaSenAltInit \aRaa.fina_PostTooth \aYaa.fina_PostTooth 
	\aBaa.init_AboveHaa \aBaa.init_BaaHaaInit \aBaa.init_BaaHaaMemInit 
	\aHaa.medi_BaaHaaMemInit \aHaa.fina_AboveHaaIsol \aLam.init_LamHaaHaaInit 
	\aHaa.medi_1LamHaaHaaInit \aHaa.medi_2LamHaaHaaInit \aAyn.init_Finjani 
	\aHaa.init_Finjani \aHaa.medi_Finjani \aSen.init_PreYaa \aSen.medi_PreYaa 
	\aSad.init_PreYaa \aSad.medi_PreYaa \aBaa.init_High \space \aKaf.isol.alt 
	\aKaf.medi.alt \aKaf.fina.alt \uni0640 \aBaa.medi_High \aSen.fina_BaaSen 
	\aMem.fina_PostTooth \tatwil.one_LamKaf \aBaa.init_Wide 
	\aHaa.medi_HaaHaaInit \aHaa.medi_AynHaaInit \aMem.medi_LamMemInitTatweel 
	\aHeh.init_AboveHaa \aHaa.init_AboveHaa \aAyn.init_AboveHaa 
	\aHaa.fina_AboveHaaIsol2 \aMem.init_AboveHaa \aKaf.init_AboveHaa \uni06FD 
	\uni06FE \uni0661 \uni0662 \uni0663 \uni0664 \uni0665 \uni0660 \period.ara 
	\uni0666 \uni0667 \uni0668 \uni0669 \uni06F0 \uni06F1 \uni06F2 \uni06F3 \uni06F7 
	\uni06F8 \uni06F9 \uni06F4 \uni06F5 \uni06F6 \parenleft \parenright \dot.1 
	\dot.2 \dash.kaf_gaf \uni200B \uni200C \uni200D \uni200E \uni200F \uni202A 
	\uni202B \uni202C \uni202D \uni202E \uni2003 \uni2001 \uni2004 \uni2005 \uni2006 
	\uni2000 \uni2002 \uni2007 \uni2008 \uni2009 \uni200A \uni202F \uni2028 \uni2029 
	\uni066A \uni0609 \uni060A \uni066B \uni066C \guillemotright \guillemotleft 
	\uni066D \bracketleft \bracketright \braceleft \braceright \uni06F4.urd 
	\uni06F6.urd \uni06F7.urd \uni06D2 \uni06D3 \uni077A \uni077B \uni06BE \uni06FF 
	\aKaf.init_KafLam \aKaf.fina_KafKafFina \aLam.medi_KafLam 
	\aLam.medi_KafLamMemMedi \aKaf.medi_KafLam \aLam.medi_KafLamHehIsol 
	\aLam.medi_KafLamYaa \aLam.medi_KafLamAlf \aLam.fina_KafLam 
	\aAlf.fina_KafAlf \aKaf.init_KafMemAlf \aKaf.medi_KafMemAlf 
	\aMem.medi_KafMemAlf \aLam.medi_KafMemLam \aLam.fina_KafMemLam 
	\aAlf.fina_KafMemAlf \at.ara \aKaf.init_KafHeh \aKaf.medi_KafHeh 
	\aHeh.fina_KafHeh \aDal.fina_KafDal \aLam.init_LamHeh \aLam.medi_LamHeh 
	\aHeh.fina_LamHeh \aDal.fina_LamDal \aKaf.medi_KafMemMedi 
	\aKaf.init_KafMemInit \aAyn.init_AynMemInit \aFaa.init_FaaMemInit 
	\aHaa.init_HaaMemInit \aHeh.init_HehMemInit \aMem.medi_KafMemMedi 
	\aSen.init_SenMemInit \aSad.init_SadMemInit \aMem.init_MemMemInit 
	\aMem.medi_SenMemInit \aKaf.init_KafYaaIsol \aBaa.init_BaaYaaIsol 
	\aHaa.init_HaaYaaIsol \aMem.init_MemYaaIsol \aFaa.init_FaaYaaIsol 
	\aAyn.init_AynYaaIsol \aLam.init_LamYaaIsol \aHeh.init_HehYaaIsol 
	\aYaa.fina_KafYaaIsol \aKaf.init_KafMemIsol \aLam.init_LamMemIsol 
	\aBaa.init_BaaMemIsol \aMem.fina_KafMemIsol \aMem.medi_MemAlfFina 
	\aBaa.medi_BaaMemAlfFina \aMem.medi_BaaMemAlfFina \aMem.medi_AlfPostTooth 
	\aAlf.fina_MemAlfFina \aBaa.init_BaaHehInit \aBaa.medi_BaaHehMedi 
	\aHeh.medi_BaaHehMedi \aHeh.medi_PostTooth \aLam.medi_KafLamMemFina 
	\aLam.init_LamLamInit \aLam.medi_LamLamInit \aLam.medi_LamLamAlfIsol 
	\aKaf.fina_LamKafIsol \aLam.fina_LamLamIsol \uni061E \uni0644.medi_FaLellah 
	\aLam.medi_LamLamMedi \aLam.medi_LamLamAlefFina \aLam.medi_LamLamMedi2 
	\aKaf.fina_LamKafFina \aLam.fina_LamLamFina \aLam.medi_LamLamMemInit 
	\aLam.medi_LamLamHehIsol \aLam.medi_LamLamYaaIsol \aLam.medi_LamLamMemMedi 
	\aLam.medi_LamLamHehFina \aLam.medi_LamLamYaaFina \uni0625.fina 
	\uni0627.fina \uni0774.fina \uni0773.fina \uni0623.fina \uni0622.fina 
	\uni0675.fina \uni0672.fina \uni0673.fina \uni0671.fina \uni0625 \uni0627 
	\uni0774 \uni0773 \uni0623 \uni0622 \uni0675 \uni0672 \uni0673 \uni0671 
	\uni06FC.fina \uni063A.fina \uni075E.fina \uni075D.fina \uni075F.fina 
	\uni06A0.fina \uni0639.fina \uni06FC.init \uni063A.init \uni075E.init 
	\uni075D.init \uni075F.init \uni06A0.init \uni0639.init \uni06FC \uni063A 
	\uni075E \uni075D \uni075F \uni06A0 \uni0639 \uni06FC.medi \uni063A.medi 
	\uni075E.medi \uni075D.medi \uni075F.medi \uni06A0.medi \uni0639.medi 
	\uni0751.fina \uni0750.fina \uni0753.fina \uni0680.fina \uni062A.fina 
	\uni0754.fina \uni062B.fina \uni0679.fina \uni067C.fina \uni0756.fina 
	\uni0752.fina \uni066E.fina \uni067F.fina \uni0755.fina \uni067D.fina 
	\uni067E.fina \uni067B.fina \uni0628.fina \uni067A.fina \uni0777.init 
	\uni0680.init \uni0776.init \uni06BC.init \uni0750.init \uni0756.init 
	\uni0768.init \uni06CE.init \uni0775.init \uni06BD.init \uni0626.init 
	\uni066E.init \uni0620.init \uni064A.init \uni06BB.init \uni067F.init 
	\uni0755.init \uni067D.init \uni067E.init \uni067B.init \uni0628.init 
	\uni067A.init \uni0751.init \uni0646.init \uni0753.init \uni0752.init 
	\uni062A.init \uni0678.init \uni063D.init \uni062B.init \uni0679.init 
	\uni06B9.init \uni0769.init \uni0649.init \uni067C.init \uni0754.init 
	\uni06D1.init \uni06D0.init \uni06BA.init \uni06CC.init \uni0767.init 
	\uni0751 \uni0750 \uni0753 \uni0680 \uni062A \uni0754 \uni062B \uni0679 \uni067C 
	\uni0756 \uni0752 \uni066E \uni067F \uni0755 \uni067D \uni067E \uni067B \uni0628 
	\uni067A \uni0777.medi \uni0680.medi \uni0776.medi \uni06BC.medi 
	\uni0750.medi \uni0756.medi \uni0768.medi \uni06CE.medi \uni0775.medi 
	\uni06BD.medi \uni0626.medi \uni066E.medi \uni0620.medi \uni064A.medi 
	\uni06BB.medi \uni067F.medi \uni0755.medi \uni067D.medi \uni067E.medi 
	\uni067B.medi \uni0628.medi \uni067A.medi \uni0751.medi \uni0646.medi 
	\uni0753.medi \uni0752.medi \uni062A.medi \uni0678.medi \uni063D.medi 
	\uni062B.medi \uni0679.medi \uni06B9.medi \uni0769.medi \uni0649.medi 
	\uni067C.medi \uni0754.medi \uni06D1.medi \uni06D0.medi \uni06BA.medi 
	\uni06CC.medi \uni0767.medi \uni0690.fina \uni06EE.fina \uni0689.fina 
	\uni0688.fina \uni075A.fina \uni0630.fina \uni062F.fina \uni0759.fina 
	\uni068C.fina \uni068B.fina \uni068A.fina \uni068F.fina \uni068E.fina 
	\uni068D.fina \uni0690 \uni06EE \uni0689 \uni0688 \uni075A \uni0630 \uni062F 
	\uni0759 \uni068C \uni068B \uni068A \uni068F \uni068E \uni068D \uni0760.fina 
	\uni0761.fina \uni0641.fina \uni06A1.fina \uni06A2.fina \uni06A3.fina 
	\uni06A4.fina \uni06A5.fina \uni06A6.fina \uni066F.init \uni0761.init 
	\uni0760.init \uni0642.init \uni0641.init \uni06A8.init \uni06A1.init 
	\uni06A2.init \uni06A3.init \uni06A4.init \uni06A5.init \uni06A6.init 
	\uni06A7.init \uni0760 \uni0761 \uni0641 \uni06A1 \uni06A2 \uni06A3 \uni06A4 
	\uni06A5 \uni06A6 \uni066F.medi \uni0761.medi \uni0760.medi \uni0642.medi 
	\uni0641.medi \uni06A8.medi \uni06A1.medi \uni06A2.medi \uni06A3.medi 
	\uni06A4.medi \uni06A5.medi \uni06A6.medi \uni06A7.medi \uni062E.fina 
	\uni062D.fina \uni0681.fina \uni0687.fina \uni0685.fina \uni062C.fina 
	\uni0682.fina \uni0757.fina \uni0684.fina \uni076F.fina \uni076E.fina 
	\uni0683.fina \uni06BF.fina \uni077C.fina \uni0758.fina \uni0772.fina 
	\uni0686.fina \uni062E.init \uni062D.init \uni0681.init \uni0687.init 
	\uni0685.init \uni062C.init \uni0682.init \uni0757.init \uni0684.init 
	\uni076F.init \uni076E.init \uni0683.init \uni06BF.init \uni077C.init 
	\uni0758.init \uni0772.init \uni0686.init \uni062E \uni062D \uni0681 \uni0687 
	\uni0685 \uni062C \uni0682 \uni0757 \uni0684 \uni076F \uni076E \uni0683 \uni06BF 
	\uni077C \uni0758 \uni0772 \uni0686 \uni062E.medi \uni062D.medi \uni0681.medi 
	\uni0687.medi \uni0685.medi \uni062C.medi \uni0682.medi \uni0757.medi 
	\uni0684.medi \uni076F.medi \uni076E.medi \uni0683.medi \uni06BF.medi 
	\uni077C.medi \uni0758.medi \uni0772.medi \uni0686.medi \uni0647.fina 
	\uni06C1.fina \uni06C3.fina \uni06D5.fina \uni0629.fina \uni0647.init 
	\uni06C1.init \uni0647 \uni06C2 \uni06C0 \uni06C1 \uni06C3 \uni06D5 \uni0629 
	\uni0647.medi \uni06C1.medi \uni063B.fina \uni063C.fina \uni077F.fina 
	\uni0764.fina \uni0643.fina \uni06B0.fina \uni06B3.fina \uni06B2.fina 
	\uni06AB.fina \uni06AC.fina \uni06AD.fina \uni06AE.fina \uni06AF.fina 
	\uni06A9.fina \uni06B4.fina \uni0763.fina \uni0762.fina \uni06B1.fina 
	\uni063B.init \uni063C.init \uni077F.init \uni0764.init \uni0643.init 
	\uni06B0.init \uni06B3.init \uni06B2.init \uni06AB.init \uni06AC.init 
	\uni06AD.init \uni06AE.init \uni06AF.init \uni06A9.init \uni06B4.init 
	\uni0763.init \uni0762.init \uni06B1.init \uni063B \uni063C \uni077F \uni0764 
	\uni0643 \uni06B0 \uni06B3 \uni06B2 \uni06AB \uni06AC \uni06AD \uni06AE \uni06AF 
	\uni06A9 \uni06B4 \uni0763 \uni0762 \uni06B1 \uni063B.medi \uni063C.medi 
	\uni077F.medi \uni0764.medi \uni0643.medi \uni06B0.medi \uni06B3.medi 
	\uni06B2.medi \uni06AB.medi \uni06AC.medi \uni06AD.medi \uni06AE.medi 
	\uni06AF.medi \uni06A9.medi \uni06B4.medi \uni0763.medi \uni0762.medi 
	\uni06B1.medi \uni06B5.fina \uni06B7.fina \uni0644.fina \uni06B8.fina 
	\uni06B6.fina \uni076A.fina \uni06B5.init \uni06B7.init \uni0644.init 
	\uni06B8.init \uni06B6.init \uni076A.init \uni06B5 \uni06B7 \uni0644 \uni06B8 
	\uni06B6 \uni076A \uni06B5.medi \uni06B7.medi \uni0644.medi \uni06B8.medi 
	\uni06B6.medi \uni076A.medi \uni0765.fina \uni0645.fina \uni0766.fina 
	\uni0765.init \uni0645.init \uni0766.init \uni0765 \uni0645 \uni0766 
	\uni0765.medi \uni0645.medi \uni0766.medi \uni0646.fina \uni0767.fina 
	\uni06BA.fina \uni06BC.fina \uni06BB.fina \uni0768.fina \uni06B9.fina 
	\uni0769.fina \uni06BD.fina \uni0646 \uni0767 \uni06BA \uni06BC \uni06BB 
	\uni0768 \uni06B9 \uni0769 \uni06BD \uni06A8.fina \uni06A7.fina \uni0642.fina 
	\uni066F.fina \uni06A8 \uni06A7 \uni0642 \uni066F \uni0691.fina \uni0692.fina 
	\uni0693.fina \uni0694.fina \uni0695.fina \uni0696.fina \uni0697.fina 
	\uni0698.fina \uni0699.fina \uni075B.fina \uni06EF.fina \uni0632.fina 
	\uni0771.fina \uni0631.fina \uni076B.fina \uni076C.fina \uni0691 \uni0692 
	\uni0693 \uni0694 \uni0695 \uni0696 \uni0697 \uni0698 \uni0699 \uni075B \uni06EF 
	\uni0632 \uni0771 \uni0631 \uni076B \uni076C \uni069D.fina \uni06FB.fina 
	\uni0636.fina \uni069E.fina \uni0635.fina \uni069D.init \uni06FB.init 
	\uni0636.init \uni069E.init \uni0635.init \uni069D \uni06FB \uni0636 \uni069E 
	\uni0635 \uni069D.medi \uni06FB.medi \uni0636.medi \uni069E.medi 
	\uni0635.medi \uni06FA.fina \uni076D.fina \uni0633.fina \uni077E.fina 
	\uni077D.fina \uni0634.fina \uni0770.fina \uni075C.fina \uni069A.fina 
	\uni069B.fina \uni069C.fina \uni06FA.init \uni076D.init \uni0633.init 
	\uni077E.init \uni077D.init \uni0634.init \uni0770.init \uni075C.init 
	\uni069A.init \uni069B.init \uni069C.init \uni06FA \uni076D \uni0633 \uni077E 
	\uni077D \uni0634 \uni0770 \uni075C \uni069A \uni069B \uni069C \uni06FA.medi 
	\uni076D.medi \uni0633.medi \uni077E.medi \uni077D.medi \uni0634.medi 
	\uni0770.medi \uni075C.medi \uni069A.medi \uni069B.medi \uni069C.medi 
	\uni0638.fina \uni0637.fina \uni069F.fina \uni0638.init \uni0637.init 
	\uni069F.init \uni0638 \uni0637 \uni069F \uni0638.medi \uni0637.medi 
	\uni069F.medi \uni06CB.fina \uni0624.fina \uni06CA.fina \uni06CF.fina 
	\uni0778.fina \uni06C6.fina \uni06C7.fina \uni06C4.fina \uni06C5.fina 
	\uni0676.fina \uni0677.fina \uni06C8.fina \uni06C9.fina \uni0779.fina 
	\uni0648.fina \uni06CB \uni0624 \uni06CA \uni06CF \uni0778 \uni06C6 \uni06C7 
	\uni06C4 \uni06C5 \uni0676 \uni0677 \uni06C8 \uni06C9 \uni0779 \uni0648 
	\uni0777.fina \uni06D1.fina \uni0775.fina \uni063F.fina \uni0678.fina 
	\uni063D.fina \uni063E.fina \uni06D0.fina \uni0649.fina \uni0776.fina 
	\uni06CD.fina \uni06CC.fina \uni0626.fina \uni0620.fina \uni064A.fina 
	\uni06CE.fina \uni0777 \uni06D1 \uni0775 \uni063F \uni0678 \uni063D \uni063E 
	\uni06D0 \uni0649 \uni0776 \uni06CD \uni06CC \uni0626 \uni0620 \uni064A \uni06CE 
	\uni06B5.medi_LamMemFina \uni06B7.medi_LamMemFina \uni0644.medi_LamMemFina 
	\uni06B8.medi_LamMemFina \uni06B6.medi_LamMemFina \uni076A.medi_LamMemFina 
	\uni0645.fina_LamMemFina \uni0777.init_BaaRaaIsol \uni0680.init_BaaRaaIsol 
	\uni0776.init_BaaRaaIsol \uni06BC.init_BaaRaaIsol \uni0750.init_BaaRaaIsol 
	\uni0756.init_BaaRaaIsol \uni0768.init_BaaRaaIsol \uni06CE.init_BaaRaaIsol 
	\uni0775.init_BaaRaaIsol \uni06BD.init_BaaRaaIsol \uni0626.init_BaaRaaIsol 
	\uni066E.init_BaaRaaIsol \uni0620.init_BaaRaaIsol \uni064A.init_BaaRaaIsol 
	\uni06BB.init_BaaRaaIsol \uni067F.init_BaaRaaIsol \uni0755.init_BaaRaaIsol 
	\uni067D.init_BaaRaaIsol \uni067E.init_BaaRaaIsol \uni067B.init_BaaRaaIsol 
	\uni0628.init_BaaRaaIsol \uni067A.init_BaaRaaIsol \uni0751.init_BaaRaaIsol 
	\uni0646.init_BaaRaaIsol \uni0753.init_BaaRaaIsol \uni0752.init_BaaRaaIsol 
	\uni062A.init_BaaRaaIsol \uni0678.init_BaaRaaIsol \uni063D.init_BaaRaaIsol 
	\uni062B.init_BaaRaaIsol \uni0679.init_BaaRaaIsol \uni06B9.init_BaaRaaIsol 
	\uni0769.init_BaaRaaIsol \uni0649.init_BaaRaaIsol \uni067C.init_BaaRaaIsol 
	\uni0754.init_BaaRaaIsol \uni06D1.init_BaaRaaIsol \uni06D0.init_BaaRaaIsol 
	\uni06BA.init_BaaRaaIsol \uni06CC.init_BaaRaaIsol \uni0767.init_BaaRaaIsol 
	\uni0691.fina_BaaRaaIsol \uni0692.fina_BaaRaaIsol \uni0693.fina_BaaRaaIsol 
	\uni0694.fina_BaaRaaIsol \uni0695.fina_BaaRaaIsol \uni0696.fina_BaaRaaIsol 
	\uni0697.fina_BaaRaaIsol \uni0698.fina_BaaRaaIsol \uni0699.fina_BaaRaaIsol 
	\uni075B.fina_BaaRaaIsol \uni06EF.fina_BaaRaaIsol \uni0632.fina_BaaRaaIsol 
	\uni0771.fina_BaaRaaIsol \uni0631.fina_BaaRaaIsol \uni076B.fina_BaaRaaIsol 
	\uni076C.fina_BaaRaaIsol \uni06B5.medi_LamWawFina \uni06B7.medi_LamWawFina 
	\uni0644.medi_LamWawFina \uni06B8.medi_LamWawFina \uni06B6.medi_LamWawFina 
	\uni076A.medi_LamWawFina \uni06CB.fina_LamWawFina \uni0624.fina_LamWawFina 
	\uni06CA.fina_LamWawFina \uni06CF.fina_LamWawFina \uni0778.fina_LamWawFina 
	\uni06C6.fina_LamWawFina \uni06C7.fina_LamWawFina \uni06C4.fina_LamWawFina 
	\uni06C5.fina_LamWawFina \uni0676.fina_LamWawFina \uni0677.fina_LamWawFina 
	\uni06C8.fina_LamWawFina \uni06C9.fina_LamWawFina \uni0779.fina_LamWawFina 
	\uni0648.fina_LamWawFina \uni06B5.init_LamHaaInit \uni06B7.init_LamHaaInit 
	\uni0644.init_LamHaaInit \uni06B8.init_LamHaaInit \uni06B6.init_LamHaaInit 
	\uni076A.init_LamHaaInit \uni066F.medi_FaaYaaFina \uni0761.medi_FaaYaaFina 
	\uni0760.medi_FaaYaaFina \uni0642.medi_FaaYaaFina \uni0641.medi_FaaYaaFina 
	\uni06A8.medi_FaaYaaFina \uni06A1.medi_FaaYaaFina \uni06A2.medi_FaaYaaFina 
	\uni06A3.medi_FaaYaaFina \uni06A4.medi_FaaYaaFina \uni06A5.medi_FaaYaaFina 
	\uni06A6.medi_FaaYaaFina \uni06A7.medi_FaaYaaFina \uni0777.fina_FaaYaaFina 
	\uni06D1.fina_FaaYaaFina \uni0775.fina_FaaYaaFina \uni063F.fina_FaaYaaFina 
	\uni0678.fina_FaaYaaFina \uni063D.fina_FaaYaaFina \uni063E.fina_FaaYaaFina 
	\uni06D0.fina_FaaYaaFina \uni0649.fina_FaaYaaFina \uni0776.fina_FaaYaaFina 
	\uni06CD.fina_FaaYaaFina \uni06CC.fina_FaaYaaFina \uni0626.fina_FaaYaaFina 
	\uni0620.fina_FaaYaaFina \uni064A.fina_FaaYaaFina \uni06CE.fina_FaaYaaFina 
	\uni06B5.init_LamLamHaaInit \uni06B7.init_LamLamHaaInit 
	\uni0644.init_LamLamHaaInit \uni06B8.init_LamLamHaaInit 
	\uni06B6.init_LamLamHaaInit \uni076A.init_LamLamHaaInit 
	\uni06B5.medi_LamLamHaaInit \uni06B7.medi_LamLamHaaInit 
	\uni0644.medi_LamLamHaaInit \uni06B8.medi_LamLamHaaInit 
	\uni06B6.medi_LamLamHaaInit \uni076A.medi_LamLamHaaInit 
	\uni062E.medi_LamLamHaaInit \uni062D.medi_LamLamHaaInit 
	\uni0681.medi_LamLamHaaInit \uni0687.medi_LamLamHaaInit 
	\uni0685.medi_LamLamHaaInit \uni062C.medi_LamLamHaaInit 
	\uni0682.medi_LamLamHaaInit \uni0757.medi_LamLamHaaInit 
	\uni0684.medi_LamLamHaaInit \uni076F.medi_LamLamHaaInit 
	\uni076E.medi_LamLamHaaInit \uni0683.medi_LamLamHaaInit 
	\uni06BF.medi_LamLamHaaInit \uni077C.medi_LamLamHaaInit 
	\uni0758.medi_LamLamHaaInit \uni0772.medi_LamLamHaaInit 
	\uni0686.medi_LamLamHaaInit \uni0777.medi_BaaMemFina 
	\uni0680.medi_BaaMemFina \uni0776.medi_BaaMemFina \uni06BC.medi_BaaMemFina 
	\uni0750.medi_BaaMemFina \uni0756.medi_BaaMemFina \uni0768.medi_BaaMemFina 
	\uni06CE.medi_BaaMemFina \uni0775.medi_BaaMemFina \uni06BD.medi_BaaMemFina 
	\uni0626.medi_BaaMemFina \uni066E.medi_BaaMemFina \uni0620.medi_BaaMemFina 
	\uni064A.medi_BaaMemFina \uni06BB.medi_BaaMemFina \uni067F.medi_BaaMemFina 
	\uni0755.medi_BaaMemFina \uni067D.medi_BaaMemFina \uni067E.medi_BaaMemFina 
	\uni067B.medi_BaaMemFina \uni0628.medi_BaaMemFina \uni067A.medi_BaaMemFina 
	\uni0751.medi_BaaMemFina \uni0646.medi_BaaMemFina \uni0753.medi_BaaMemFina 
	\uni0752.medi_BaaMemFina \uni062A.medi_BaaMemFina \uni0678.medi_BaaMemFina 
	\uni063D.medi_BaaMemFina \uni062B.medi_BaaMemFina \uni0679.medi_BaaMemFina 
	\uni06B9.medi_BaaMemFina \uni0769.medi_BaaMemFina \uni0649.medi_BaaMemFina 
	\uni067C.medi_BaaMemFina \uni0754.medi_BaaMemFina \uni06D1.medi_BaaMemFina 
	\uni06D0.medi_BaaMemFina \uni06BA.medi_BaaMemFina \uni06CC.medi_BaaMemFina 
	\uni0767.medi_BaaMemFina \uni0645.fina_BaaMemFina \uni069D.init_AboveHaa 
	\uni06FB.init_AboveHaa \uni0636.init_AboveHaa \uni069E.init_AboveHaa 
	\uni0635.init_AboveHaa \uni06B5.init_LamBaaMemInit 
	\uni06B7.init_LamBaaMemInit \uni0644.init_LamBaaMemInit 
	\uni06B8.init_LamBaaMemInit \uni06B6.init_LamBaaMemInit 
	\uni076A.init_LamBaaMemInit \uni0777.medi_LamBaaMemInit 
	\uni0680.medi_LamBaaMemInit \uni0776.medi_LamBaaMemInit 
	\uni06BC.medi_LamBaaMemInit \uni0750.medi_LamBaaMemInit 
	\uni0756.medi_LamBaaMemInit \uni0768.medi_LamBaaMemInit 
	\uni06CE.medi_LamBaaMemInit \uni0775.medi_LamBaaMemInit 
	\uni06BD.medi_LamBaaMemInit \uni0626.medi_LamBaaMemInit 
	\uni066E.medi_LamBaaMemInit \uni0620.medi_LamBaaMemInit 
	\uni064A.medi_LamBaaMemInit \uni06BB.medi_LamBaaMemInit 
	\uni067F.medi_LamBaaMemInit \uni0755.medi_LamBaaMemInit 
	\uni067D.medi_LamBaaMemInit \uni067E.medi_LamBaaMemInit 
	\uni067B.medi_LamBaaMemInit \uni0628.medi_LamBaaMemInit 
	\uni067A.medi_LamBaaMemInit \uni0751.medi_LamBaaMemInit 
	\uni0646.medi_LamBaaMemInit \uni0753.medi_LamBaaMemInit 
	\uni0752.medi_LamBaaMemInit \uni062A.medi_LamBaaMemInit 
	\uni0678.medi_LamBaaMemInit \uni063D.medi_LamBaaMemInit 
	\uni062B.medi_LamBaaMemInit \uni0679.medi_LamBaaMemInit 
	\uni06B9.medi_LamBaaMemInit \uni0769.medi_LamBaaMemInit 
	\uni0649.medi_LamBaaMemInit \uni067C.medi_LamBaaMemInit 
	\uni0754.medi_LamBaaMemInit \uni06D1.medi_LamBaaMemInit 
	\uni06D0.medi_LamBaaMemInit \uni06BA.medi_LamBaaMemInit 
	\uni06CC.medi_LamBaaMemInit \uni0767.medi_LamBaaMemInit 
	\uni0645.medi_LamBaaMemInit \uni0777.init_BaaDal \uni0680.init_BaaDal 
	\uni0776.init_BaaDal \uni06BC.init_BaaDal \uni0750.init_BaaDal 
	\uni0756.init_BaaDal \uni0768.init_BaaDal \uni06CE.init_BaaDal 
	\uni0775.init_BaaDal \uni06BD.init_BaaDal \uni0626.init_BaaDal 
	\uni066E.init_BaaDal \uni0620.init_BaaDal \uni064A.init_BaaDal 
	\uni06BB.init_BaaDal \uni067F.init_BaaDal \uni0755.init_BaaDal 
	\uni067D.init_BaaDal \uni067E.init_BaaDal \uni067B.init_BaaDal 
	\uni0628.init_BaaDal \uni067A.init_BaaDal \uni0751.init_BaaDal 
	\uni0646.init_BaaDal \uni0753.init_BaaDal \uni0752.init_BaaDal 
	\uni062A.init_BaaDal \uni0678.init_BaaDal \uni063D.init_BaaDal 
	\uni062B.init_BaaDal \uni0679.init_BaaDal \uni06B9.init_BaaDal 
	\uni0769.init_BaaDal \uni0649.init_BaaDal \uni067C.init_BaaDal 
	\uni0754.init_BaaDal \uni06D1.init_BaaDal \uni06D0.init_BaaDal 
	\uni06BA.init_BaaDal \uni06CC.init_BaaDal \uni0767.init_BaaDal 
	\uni0690.fina_BaaDal \uni06EE.fina_BaaDal \uni0689.fina_BaaDal 
	\uni0688.fina_BaaDal \uni075A.fina_BaaDal \uni0630.fina_BaaDal 
	\uni062F.fina_BaaDal \uni0759.fina_BaaDal \uni068C.fina_BaaDal 
	\uni068B.fina_BaaDal \uni068A.fina_BaaDal \uni068F.fina_BaaDal 
	\uni068E.fina_BaaDal \uni068D.fina_BaaDal \uni0777.init_BaaMemHaaInit 
	\uni0680.init_BaaMemHaaInit \uni0776.init_BaaMemHaaInit 
	\uni06BC.init_BaaMemHaaInit \uni0750.init_BaaMemHaaInit 
	\uni0756.init_BaaMemHaaInit \uni0768.init_BaaMemHaaInit 
	\uni06CE.init_BaaMemHaaInit \uni0775.init_BaaMemHaaInit 
	\uni06BD.init_BaaMemHaaInit \uni0626.init_BaaMemHaaInit 
	\uni066E.init_BaaMemHaaInit \uni0620.init_BaaMemHaaInit 
	\uni064A.init_BaaMemHaaInit \uni06BB.init_BaaMemHaaInit 
	\uni067F.init_BaaMemHaaInit \uni0755.init_BaaMemHaaInit 
	\uni067D.init_BaaMemHaaInit \uni067E.init_BaaMemHaaInit 
	\uni067B.init_BaaMemHaaInit \uni0628.init_BaaMemHaaInit 
	\uni067A.init_BaaMemHaaInit \uni0751.init_BaaMemHaaInit 
	\uni0646.init_BaaMemHaaInit \uni0753.init_BaaMemHaaInit 
	\uni0752.init_BaaMemHaaInit \uni062A.init_BaaMemHaaInit 
	\uni0678.init_BaaMemHaaInit \uni063D.init_BaaMemHaaInit 
	\uni062B.init_BaaMemHaaInit \uni0679.init_BaaMemHaaInit 
	\uni06B9.init_BaaMemHaaInit \uni0769.init_BaaMemHaaInit 
	\uni0649.init_BaaMemHaaInit \uni067C.init_BaaMemHaaInit 
	\uni0754.init_BaaMemHaaInit \uni06D1.init_BaaMemHaaInit 
	\uni06D0.init_BaaMemHaaInit \uni06BA.init_BaaMemHaaInit 
	\uni06CC.init_BaaMemHaaInit \uni0767.init_BaaMemHaaInit 
	\uni0645.medi_BaaMemHaaInit \uni062E.medi_BaaMemHaaInit 
	\uni062D.medi_BaaMemHaaInit \uni0681.medi_BaaMemHaaInit 
	\uni0687.medi_BaaMemHaaInit \uni0685.medi_BaaMemHaaInit 
	\uni062C.medi_BaaMemHaaInit \uni0682.medi_BaaMemHaaInit 
	\uni0757.medi_BaaMemHaaInit \uni0684.medi_BaaMemHaaInit 
	\uni076F.medi_BaaMemHaaInit \uni076E.medi_BaaMemHaaInit 
	\uni0683.medi_BaaMemHaaInit \uni06BF.medi_BaaMemHaaInit 
	\uni077C.medi_BaaMemHaaInit \uni0758.medi_BaaMemHaaInit 
	\uni0772.medi_BaaMemHaaInit \uni0686.medi_BaaMemHaaInit 
	\uni0777.init_BaaBaaYaa \uni0680.init_BaaBaaYaa \uni0776.init_BaaBaaYaa 
	\uni06BC.init_BaaBaaYaa \uni0750.init_BaaBaaYaa \uni0756.init_BaaBaaYaa 
	\uni0768.init_BaaBaaYaa \uni06CE.init_BaaBaaYaa \uni0775.init_BaaBaaYaa 
	\uni06BD.init_BaaBaaYaa \uni0626.init_BaaBaaYaa \uni066E.init_BaaBaaYaa 
	\uni0620.init_BaaBaaYaa \uni064A.init_BaaBaaYaa \uni06BB.init_BaaBaaYaa 
	\uni067F.init_BaaBaaYaa \uni0755.init_BaaBaaYaa \uni067D.init_BaaBaaYaa 
	\uni067E.init_BaaBaaYaa \uni067B.init_BaaBaaYaa \uni0628.init_BaaBaaYaa 
	\uni067A.init_BaaBaaYaa \uni0751.init_BaaBaaYaa \uni0646.init_BaaBaaYaa 
	\uni0753.init_BaaBaaYaa \uni0752.init_BaaBaaYaa \uni062A.init_BaaBaaYaa 
	\uni0678.init_BaaBaaYaa \uni063D.init_BaaBaaYaa \uni062B.init_BaaBaaYaa 
	\uni0679.init_BaaBaaYaa \uni06B9.init_BaaBaaYaa \uni0769.init_BaaBaaYaa 
	\uni0649.init_BaaBaaYaa \uni067C.init_BaaBaaYaa \uni0754.init_BaaBaaYaa 
	\uni06D1.init_BaaBaaYaa \uni06D0.init_BaaBaaYaa \uni06BA.init_BaaBaaYaa 
	\uni06CC.init_BaaBaaYaa \uni0767.init_BaaBaaYaa \uni0777.medi_BaaBaaYaa 
	\uni0680.medi_BaaBaaYaa \uni0776.medi_BaaBaaYaa \uni06BC.medi_BaaBaaYaa 
	\uni0750.medi_BaaBaaYaa \uni0756.medi_BaaBaaYaa \uni0768.medi_BaaBaaYaa 
	\uni06CE.medi_BaaBaaYaa \uni0775.medi_BaaBaaYaa \uni06BD.medi_BaaBaaYaa 
	\uni0626.medi_BaaBaaYaa \uni066E.medi_BaaBaaYaa \uni0620.medi_BaaBaaYaa 
	\uni064A.medi_BaaBaaYaa \uni06BB.medi_BaaBaaYaa \uni067F.medi_BaaBaaYaa 
	\uni0755.medi_BaaBaaYaa \uni067D.medi_BaaBaaYaa \uni067E.medi_BaaBaaYaa 
	\uni067B.medi_BaaBaaYaa \uni0628.medi_BaaBaaYaa \uni067A.medi_BaaBaaYaa 
	\uni0751.medi_BaaBaaYaa \uni0646.medi_BaaBaaYaa \uni0753.medi_BaaBaaYaa 
	\uni0752.medi_BaaBaaYaa \uni062A.medi_BaaBaaYaa \uni0678.medi_BaaBaaYaa 
	\uni063D.medi_BaaBaaYaa \uni062B.medi_BaaBaaYaa \uni0679.medi_BaaBaaYaa 
	\uni06B9.medi_BaaBaaYaa \uni0769.medi_BaaBaaYaa \uni0649.medi_BaaBaaYaa 
	\uni067C.medi_BaaBaaYaa \uni0754.medi_BaaBaaYaa \uni06D1.medi_BaaBaaYaa 
	\uni06D0.medi_BaaBaaYaa \uni06BA.medi_BaaBaaYaa \uni06CC.medi_BaaBaaYaa 
	\uni0767.medi_BaaBaaYaa \uni0777.fina_BaaBaaYaa \uni06D1.fina_BaaBaaYaa 
	\uni0775.fina_BaaBaaYaa \uni063F.fina_BaaBaaYaa \uni0678.fina_BaaBaaYaa 
	\uni063D.fina_BaaBaaYaa \uni063E.fina_BaaBaaYaa \uni06D0.fina_BaaBaaYaa 
	\uni0649.fina_BaaBaaYaa \uni0776.fina_BaaBaaYaa \uni06CD.fina_BaaBaaYaa 
	\uni06CC.fina_BaaBaaYaa \uni0626.fina_BaaBaaYaa \uni0620.fina_BaaBaaYaa 
	\uni064A.fina_BaaBaaYaa \uni06CE.fina_BaaBaaYaa \uni06B5.medi_LamYaaFina 
	\uni06B7.medi_LamYaaFina \uni0644.medi_LamYaaFina \uni06B8.medi_LamYaaFina 
	\uni06B6.medi_LamYaaFina \uni076A.medi_LamYaaFina \uni0777.fina_LamYaaFina 
	\uni06D1.fina_LamYaaFina \uni0775.fina_LamYaaFina \uni063F.fina_LamYaaFina 
	\uni0678.fina_LamYaaFina \uni063D.fina_LamYaaFina \uni063E.fina_LamYaaFina 
	\uni06D0.fina_LamYaaFina \uni0649.fina_LamYaaFina \uni0776.fina_LamYaaFina 
	\uni06CD.fina_LamYaaFina \uni06CC.fina_LamYaaFina \uni0626.fina_LamYaaFina 
	\uni0620.fina_LamYaaFina \uni064A.fina_LamYaaFina \uni06CE.fina_LamYaaFina 
	\uni063B.init_KafBaaInit \uni063C.init_KafBaaInit \uni077F.init_KafBaaInit 
	\uni0764.init_KafBaaInit \uni0643.init_KafBaaInit \uni06B0.init_KafBaaInit 
	\uni06B3.init_KafBaaInit \uni06B2.init_KafBaaInit \uni06AB.init_KafBaaInit 
	\uni06AC.init_KafBaaInit \uni06AD.init_KafBaaInit \uni06AE.init_KafBaaInit 
	\uni06AF.init_KafBaaInit \uni06A9.init_KafBaaInit \uni06B4.init_KafBaaInit 
	\uni0763.init_KafBaaInit \uni0762.init_KafBaaInit \uni06B1.init_KafBaaInit 
	\uni0777.medi_KafBaaInit \uni0680.medi_KafBaaInit \uni0776.medi_KafBaaInit 
	\uni06BC.medi_KafBaaInit \uni0750.medi_KafBaaInit \uni0756.medi_KafBaaInit 
	\uni0768.medi_KafBaaInit \uni06CE.medi_KafBaaInit \uni0775.medi_KafBaaInit 
	\uni06BD.medi_KafBaaInit \uni0626.medi_KafBaaInit \uni066E.medi_KafBaaInit 
	\uni0620.medi_KafBaaInit \uni064A.medi_KafBaaInit \uni06BB.medi_KafBaaInit 
	\uni067F.medi_KafBaaInit \uni0755.medi_KafBaaInit \uni067D.medi_KafBaaInit 
	\uni067E.medi_KafBaaInit \uni067B.medi_KafBaaInit \uni0628.medi_KafBaaInit 
	\uni067A.medi_KafBaaInit \uni0751.medi_KafBaaInit \uni0646.medi_KafBaaInit 
	\uni0753.medi_KafBaaInit \uni0752.medi_KafBaaInit \uni062A.medi_KafBaaInit 
	\uni0678.medi_KafBaaInit \uni063D.medi_KafBaaInit \uni062B.medi_KafBaaInit 
	\uni0679.medi_KafBaaInit \uni06B9.medi_KafBaaInit \uni0769.medi_KafBaaInit 
	\uni0649.medi_KafBaaInit \uni067C.medi_KafBaaInit \uni0754.medi_KafBaaInit 
	\uni06D1.medi_KafBaaInit \uni06D0.medi_KafBaaInit \uni06BA.medi_KafBaaInit 
	\uni06CC.medi_KafBaaInit \uni0767.medi_KafBaaInit \uni06B5.init_LamMemInit 
	\uni06B7.init_LamMemInit \uni0644.init_LamMemInit \uni06B8.init_LamMemInit 
	\uni06B6.init_LamMemInit \uni076A.init_LamMemInit \uni0645.medi_LamMemInit 
	\uni06B5.init_LamAlfIsol \uni06B7.init_LamAlfIsol \uni0644.init_LamAlfIsol 
	\uni06B8.init_LamAlfIsol \uni06B6.init_LamAlfIsol \uni076A.init_LamAlfIsol 
	\uni0625.fina_LamAlfIsol \uni0627.fina_LamAlfIsol \uni0774.fina_LamAlfIsol 
	\uni0773.fina_LamAlfIsol \uni0623.fina_LamAlfIsol \uni0622.fina_LamAlfIsol 
	\uni0675.fina_LamAlfIsol \uni0672.fina_LamAlfIsol \uni0673.fina_LamAlfIsol 
	\uni0671.fina_LamAlfIsol \uni06B5.init_LamHaaMemInit 
	\uni06B7.init_LamHaaMemInit \uni0644.init_LamHaaMemInit 
	\uni06B8.init_LamHaaMemInit \uni06B6.init_LamHaaMemInit 
	\uni076A.init_LamHaaMemInit \uni062E.medi_LamHaaMemInit 
	\uni062D.medi_LamHaaMemInit \uni0681.medi_LamHaaMemInit 
	\uni0687.medi_LamHaaMemInit \uni0685.medi_LamHaaMemInit 
	\uni062C.medi_LamHaaMemInit \uni0682.medi_LamHaaMemInit 
	\uni0757.medi_LamHaaMemInit \uni0684.medi_LamHaaMemInit 
	\uni076F.medi_LamHaaMemInit \uni076E.medi_LamHaaMemInit 
	\uni0683.medi_LamHaaMemInit \uni06BF.medi_LamHaaMemInit 
	\uni077C.medi_LamHaaMemInit \uni0758.medi_LamHaaMemInit 
	\uni0772.medi_LamHaaMemInit \uni0686.medi_LamHaaMemInit 
	\uni0645.medi_LamHaaMemInit \uni0777.medi_BaaBaaInit 
	\uni0680.medi_BaaBaaInit \uni0776.medi_BaaBaaInit \uni06BC.medi_BaaBaaInit 
	\uni0750.medi_BaaBaaInit \uni0756.medi_BaaBaaInit \uni0768.medi_BaaBaaInit 
	\uni06CE.medi_BaaBaaInit \uni0775.medi_BaaBaaInit \uni06BD.medi_BaaBaaInit 
	\uni0626.medi_BaaBaaInit \uni066E.medi_BaaBaaInit \uni0620.medi_BaaBaaInit 
	\uni064A.medi_BaaBaaInit \uni06BB.medi_BaaBaaInit \uni067F.medi_BaaBaaInit 
	\uni0755.medi_BaaBaaInit \uni067D.medi_BaaBaaInit \uni067E.medi_BaaBaaInit 
	\uni067B.medi_BaaBaaInit \uni0628.medi_BaaBaaInit \uni067A.medi_BaaBaaInit 
	\uni0751.medi_BaaBaaInit \uni0646.medi_BaaBaaInit \uni0753.medi_BaaBaaInit 
	\uni0752.medi_BaaBaaInit \uni062A.medi_BaaBaaInit \uni0678.medi_BaaBaaInit 
	\uni063D.medi_BaaBaaInit \uni062B.medi_BaaBaaInit \uni0679.medi_BaaBaaInit 
	\uni06B9.medi_BaaBaaInit \uni0769.medi_BaaBaaInit \uni0649.medi_BaaBaaInit 
	\uni067C.medi_BaaBaaInit \uni0754.medi_BaaBaaInit \uni06D1.medi_BaaBaaInit 
	\uni06D0.medi_BaaBaaInit \uni06BA.medi_BaaBaaInit \uni06CC.medi_BaaBaaInit 
	\uni0767.medi_BaaBaaInit \uni06FC.medi_AynYaaFina \uni063A.medi_AynYaaFina 
	\uni075E.medi_AynYaaFina \uni075D.medi_AynYaaFina \uni075F.medi_AynYaaFina 
	\uni06A0.medi_AynYaaFina \uni0639.medi_AynYaaFina \uni0765.init_MemRaaIsol 
	\uni0645.init_MemRaaIsol \uni0766.init_MemRaaIsol \uni0691.fina_MemRaaIsol 
	\uni0692.fina_MemRaaIsol \uni0693.fina_MemRaaIsol \uni0694.fina_MemRaaIsol 
	\uni0695.fina_MemRaaIsol \uni0696.fina_MemRaaIsol \uni0697.fina_MemRaaIsol 
	\uni0698.fina_MemRaaIsol \uni0699.fina_MemRaaIsol \uni075B.fina_MemRaaIsol 
	\uni06EF.fina_MemRaaIsol \uni0632.fina_MemRaaIsol \uni0771.fina_MemRaaIsol 
	\uni0631.fina_MemRaaIsol \uni076B.fina_MemRaaIsol \uni076C.fina_MemRaaIsol 
	\uni066F.init_FaaHaaInit \uni0761.init_FaaHaaInit \uni0760.init_FaaHaaInit 
	\uni0642.init_FaaHaaInit \uni0641.init_FaaHaaInit \uni06A8.init_FaaHaaInit 
	\uni06A1.init_FaaHaaInit \uni06A2.init_FaaHaaInit \uni06A3.init_FaaHaaInit 
	\uni06A4.init_FaaHaaInit \uni06A5.init_FaaHaaInit \uni06A6.init_FaaHaaInit 
	\uni06A7.init_FaaHaaInit \uni062E.medi_FaaHaaInit \uni062D.medi_FaaHaaInit 
	\uni0681.medi_FaaHaaInit \uni0687.medi_FaaHaaInit \uni0685.medi_FaaHaaInit 
	\uni062C.medi_FaaHaaInit \uni0682.medi_FaaHaaInit \uni0757.medi_FaaHaaInit 
	\uni0684.medi_FaaHaaInit \uni076F.medi_FaaHaaInit \uni076E.medi_FaaHaaInit 
	\uni0683.medi_FaaHaaInit \uni06BF.medi_FaaHaaInit \uni077C.medi_FaaHaaInit 
	\uni0758.medi_FaaHaaInit \uni0772.medi_FaaHaaInit \uni0686.medi_FaaHaaInit 
	\uni062E.init_HaaHaaInit \uni062D.init_HaaHaaInit \uni0681.init_HaaHaaInit 
	\uni0687.init_HaaHaaInit \uni0685.init_HaaHaaInit \uni062C.init_HaaHaaInit 
	\uni0682.init_HaaHaaInit \uni0757.init_HaaHaaInit \uni0684.init_HaaHaaInit 
	\uni076F.init_HaaHaaInit \uni076E.init_HaaHaaInit \uni0683.init_HaaHaaInit 
	\uni06BF.init_HaaHaaInit \uni077C.init_HaaHaaInit \uni0758.init_HaaHaaInit 
	\uni0772.init_HaaHaaInit \uni0686.init_HaaHaaInit \uni06B5.medi_LamQafFina 
	\uni06B7.medi_LamQafFina \uni0644.medi_LamQafFina \uni06B8.medi_LamQafFina 
	\uni06B6.medi_LamQafFina \uni076A.medi_LamQafFina \uni06A8.fina_LamQafFina 
	\uni06A7.fina_LamQafFina \uni0642.fina_LamQafFina \uni066F.fina_LamQafFina 
	\uni06FA.init_AboveHaa \uni076D.init_AboveHaa \uni0633.init_AboveHaa 
	\uni077E.init_AboveHaa \uni077D.init_AboveHaa \uni0634.init_AboveHaa 
	\uni0770.init_AboveHaa \uni075C.init_AboveHaa \uni069A.init_AboveHaa 
	\uni069B.init_AboveHaa \uni069C.init_AboveHaa \uni0645.init_MemHaaInit 
	\uni0777.init_BaaNonIsol \uni0680.init_BaaNonIsol \uni0776.init_BaaNonIsol 
	\uni06BC.init_BaaNonIsol \uni0750.init_BaaNonIsol \uni0756.init_BaaNonIsol 
	\uni0768.init_BaaNonIsol \uni06CE.init_BaaNonIsol \uni0775.init_BaaNonIsol 
	\uni06BD.init_BaaNonIsol \uni0626.init_BaaNonIsol \uni066E.init_BaaNonIsol 
	\uni0620.init_BaaNonIsol \uni064A.init_BaaNonIsol \uni06BB.init_BaaNonIsol 
	\uni067F.init_BaaNonIsol \uni0755.init_BaaNonIsol \uni067D.init_BaaNonIsol 
	\uni067E.init_BaaNonIsol \uni067B.init_BaaNonIsol \uni0628.init_BaaNonIsol 
	\uni067A.init_BaaNonIsol \uni0751.init_BaaNonIsol \uni0646.init_BaaNonIsol 
	\uni0753.init_BaaNonIsol \uni0752.init_BaaNonIsol \uni062A.init_BaaNonIsol 
	\uni0678.init_BaaNonIsol \uni063D.init_BaaNonIsol \uni062B.init_BaaNonIsol 
	\uni0679.init_BaaNonIsol \uni06B9.init_BaaNonIsol \uni0769.init_BaaNonIsol 
	\uni0649.init_BaaNonIsol \uni067C.init_BaaNonIsol \uni0754.init_BaaNonIsol 
	\uni06D1.init_BaaNonIsol \uni06D0.init_BaaNonIsol \uni06BA.init_BaaNonIsol 
	\uni06CC.init_BaaNonIsol \uni0767.init_BaaNonIsol \uni0646.fina_BaaNonIsol 
	\uni0767.fina_BaaNonIsol \uni06BA.fina_BaaNonIsol \uni06BC.fina_BaaNonIsol 
	\uni06BB.fina_BaaNonIsol \uni0768.fina_BaaNonIsol \uni06B9.fina_BaaNonIsol 
	\uni0769.fina_BaaNonIsol \uni06BD.fina_BaaNonIsol \uni063B.medi_KafMemFina 
	\uni063C.medi_KafMemFina \uni077F.medi_KafMemFina \uni0764.medi_KafMemFina 
	\uni0643.medi_KafMemFina \uni06B0.medi_KafMemFina \uni06B3.medi_KafMemFina 
	\uni06B2.medi_KafMemFina \uni06AB.medi_KafMemFina \uni06AC.medi_KafMemFina 
	\uni06AD.medi_KafMemFina \uni06AE.medi_KafMemFina \uni06AF.medi_KafMemFina 
	\uni06A9.medi_KafMemFina \uni06B4.medi_KafMemFina \uni0763.medi_KafMemFina 
	\uni0762.medi_KafMemFina \uni06B1.medi_KafMemFina \uni0645.fina_KafMemFina 
	\uni0777.init_BaaSenInit \uni0680.init_BaaSenInit \uni0776.init_BaaSenInit 
	\uni06BC.init_BaaSenInit \uni0750.init_BaaSenInit \uni0756.init_BaaSenInit 
	\uni0768.init_BaaSenInit \uni06CE.init_BaaSenInit \uni0775.init_BaaSenInit 
	\uni06BD.init_BaaSenInit \uni0626.init_BaaSenInit \uni066E.init_BaaSenInit 
	\uni0620.init_BaaSenInit \uni064A.init_BaaSenInit \uni06BB.init_BaaSenInit 
	\uni067F.init_BaaSenInit \uni0755.init_BaaSenInit \uni067D.init_BaaSenInit 
	\uni067E.init_BaaSenInit \uni067B.init_BaaSenInit \uni0628.init_BaaSenInit 
	\uni067A.init_BaaSenInit \uni0751.init_BaaSenInit \uni0646.init_BaaSenInit 
	\uni0753.init_BaaSenInit \uni0752.init_BaaSenInit \uni062A.init_BaaSenInit 
	\uni0678.init_BaaSenInit \uni063D.init_BaaSenInit \uni062B.init_BaaSenInit 
	\uni0679.init_BaaSenInit \uni06B9.init_BaaSenInit \uni0769.init_BaaSenInit 
	\uni0649.init_BaaSenInit \uni067C.init_BaaSenInit \uni0754.init_BaaSenInit 
	\uni06D1.init_BaaSenInit \uni06D0.init_BaaSenInit \uni06BA.init_BaaSenInit 
	\uni06CC.init_BaaSenInit \uni0767.init_BaaSenInit \uni06FA.medi_BaaSenInit 
	\uni076D.medi_BaaSenInit \uni0633.medi_BaaSenInit \uni077E.medi_BaaSenInit 
	\uni077D.medi_BaaSenInit \uni0634.medi_BaaSenInit \uni0770.medi_BaaSenInit 
	\uni075C.medi_BaaSenInit \uni069A.medi_BaaSenInit \uni069B.medi_BaaSenInit 
	\uni069C.medi_BaaSenInit \uni0777.medi_BaaRaaFina \uni0680.medi_BaaRaaFina 
	\uni0776.medi_BaaRaaFina \uni06BC.medi_BaaRaaFina \uni0750.medi_BaaRaaFina 
	\uni0756.medi_BaaRaaFina \uni0768.medi_BaaRaaFina \uni06CE.medi_BaaRaaFina 
	\uni0775.medi_BaaRaaFina \uni06BD.medi_BaaRaaFina \uni0626.medi_BaaRaaFina 
	\uni066E.medi_BaaRaaFina \uni0620.medi_BaaRaaFina \uni064A.medi_BaaRaaFina 
	\uni06BB.medi_BaaRaaFina \uni067F.medi_BaaRaaFina \uni0755.medi_BaaRaaFina 
	\uni067D.medi_BaaRaaFina \uni067E.medi_BaaRaaFina \uni067B.medi_BaaRaaFina 
	\uni0628.medi_BaaRaaFina \uni067A.medi_BaaRaaFina \uni0751.medi_BaaRaaFina 
	\uni0646.medi_BaaRaaFina \uni0753.medi_BaaRaaFina \uni0752.medi_BaaRaaFina 
	\uni062A.medi_BaaRaaFina \uni0678.medi_BaaRaaFina \uni063D.medi_BaaRaaFina 
	\uni062B.medi_BaaRaaFina \uni0679.medi_BaaRaaFina \uni06B9.medi_BaaRaaFina 
	\uni0769.medi_BaaRaaFina \uni0649.medi_BaaRaaFina \uni067C.medi_BaaRaaFina 
	\uni0754.medi_BaaRaaFina \uni06D1.medi_BaaRaaFina \uni06D0.medi_BaaRaaFina 
	\uni06BA.medi_BaaRaaFina \uni06CC.medi_BaaRaaFina \uni0767.medi_BaaRaaFina 
	\uni0691.fina_BaaRaaFina \uni0692.fina_BaaRaaFina \uni0693.fina_BaaRaaFina 
	\uni0694.fina_BaaRaaFina \uni0695.fina_BaaRaaFina \uni0696.fina_BaaRaaFina 
	\uni0697.fina_BaaRaaFina \uni0698.fina_BaaRaaFina \uni0699.fina_BaaRaaFina 
	\uni075B.fina_BaaRaaFina \uni06EF.fina_BaaRaaFina \uni0632.fina_BaaRaaFina 
	\uni0771.fina_BaaRaaFina \uni0631.fina_BaaRaaFina \uni076B.fina_BaaRaaFina 
	\uni076C.fina_BaaRaaFina \uni063B.medi_KafRaaFina \uni063C.medi_KafRaaFina 
	\uni077F.medi_KafRaaFina \uni0764.medi_KafRaaFina \uni0643.medi_KafRaaFina 
	\uni06B0.medi_KafRaaFina \uni06B3.medi_KafRaaFina \uni06B2.medi_KafRaaFina 
	\uni06AB.medi_KafRaaFina \uni06AC.medi_KafRaaFina \uni06AD.medi_KafRaaFina 
	\uni06AE.medi_KafRaaFina \uni06AF.medi_KafRaaFina \uni06A9.medi_KafRaaFina 
	\uni06B4.medi_KafRaaFina \uni0763.medi_KafRaaFina \uni0762.medi_KafRaaFina 
	\uni06B1.medi_KafRaaFina \uni0691.fina_KafRaaFina \uni0692.fina_KafRaaFina 
	\uni0693.fina_KafRaaFina \uni0694.fina_KafRaaFina \uni0695.fina_KafRaaFina 
	\uni0696.fina_KafRaaFina \uni0697.fina_KafRaaFina \uni0698.fina_KafRaaFina 
	\uni0699.fina_KafRaaFina \uni075B.fina_KafRaaFina \uni06EF.fina_KafRaaFina 
	\uni0632.fina_KafRaaFina \uni0771.fina_KafRaaFina \uni0631.fina_KafRaaFina 
	\uni076B.fina_KafRaaFina \uni076C.fina_KafRaaFina \uni06B5.init_LamHehInit 
	\uni06B7.init_LamHehInit \uni0644.init_LamHehInit \uni06B8.init_LamHehInit 
	\uni06B6.init_LamHehInit \uni076A.init_LamHehInit \uni0647.medi_LamHehInit 
	\uni06C1.medi_LamHehInit \uni0645.init_MemHaaMemInit 
	\uni062E.medi_MemHaaMemInit \uni062D.medi_MemHaaMemInit 
	\uni0681.medi_MemHaaMemInit \uni0687.medi_MemHaaMemInit 
	\uni0685.medi_MemHaaMemInit \uni062C.medi_MemHaaMemInit 
	\uni0682.medi_MemHaaMemInit \uni0757.medi_MemHaaMemInit 
	\uni0684.medi_MemHaaMemInit \uni076F.medi_MemHaaMemInit 
	\uni076E.medi_MemHaaMemInit \uni0683.medi_MemHaaMemInit 
	\uni06BF.medi_MemHaaMemInit \uni077C.medi_MemHaaMemInit 
	\uni0758.medi_MemHaaMemInit \uni0772.medi_MemHaaMemInit 
	\uni0686.medi_MemHaaMemInit \uni0777.init_BaaMemInit 
	\uni0680.init_BaaMemInit \uni0776.init_BaaMemInit \uni06BC.init_BaaMemInit 
	\uni0750.init_BaaMemInit \uni0756.init_BaaMemInit \uni0768.init_BaaMemInit 
	\uni06CE.init_BaaMemInit \uni0775.init_BaaMemInit \uni06BD.init_BaaMemInit 
	\uni0626.init_BaaMemInit \uni066E.init_BaaMemInit \uni0620.init_BaaMemInit 
	\uni064A.init_BaaMemInit \uni06BB.init_BaaMemInit \uni067F.init_BaaMemInit 
	\uni0755.init_BaaMemInit \uni067D.init_BaaMemInit \uni067E.init_BaaMemInit 
	\uni067B.init_BaaMemInit \uni0628.init_BaaMemInit \uni067A.init_BaaMemInit 
	\uni0751.init_BaaMemInit \uni0646.init_BaaMemInit \uni0753.init_BaaMemInit 
	\uni0752.init_BaaMemInit \uni062A.init_BaaMemInit \uni0678.init_BaaMemInit 
	\uni063D.init_BaaMemInit \uni062B.init_BaaMemInit \uni0679.init_BaaMemInit 
	\uni06B9.init_BaaMemInit \uni0769.init_BaaMemInit \uni0649.init_BaaMemInit 
	\uni067C.init_BaaMemInit \uni0754.init_BaaMemInit \uni06D1.init_BaaMemInit 
	\uni06D0.init_BaaMemInit \uni06BA.init_BaaMemInit \uni06CC.init_BaaMemInit 
	\uni0767.init_BaaMemInit \uni0645.medi_BaaMemInit \uni06FA.init_SenHaaInit 
	\uni076D.init_SenHaaInit \uni0633.init_SenHaaInit \uni077E.init_SenHaaInit 
	\uni077D.init_SenHaaInit \uni0634.init_SenHaaInit \uni0770.init_SenHaaInit 
	\uni075C.init_SenHaaInit \uni069A.init_SenHaaInit \uni069B.init_SenHaaInit 
	\uni069C.init_SenHaaInit \uni063B.init_KafRaaIsol \uni063C.init_KafRaaIsol 
	\uni077F.init_KafRaaIsol \uni0764.init_KafRaaIsol \uni0643.init_KafRaaIsol 
	\uni06B0.init_KafRaaIsol \uni06B3.init_KafRaaIsol \uni06B2.init_KafRaaIsol 
	\uni06AB.init_KafRaaIsol \uni06AC.init_KafRaaIsol \uni06AD.init_KafRaaIsol 
	\uni06AE.init_KafRaaIsol \uni06AF.init_KafRaaIsol \uni06A9.init_KafRaaIsol 
	\uni06B4.init_KafRaaIsol \uni0763.init_KafRaaIsol \uni0762.init_KafRaaIsol 
	\uni06B1.init_KafRaaIsol \uni0691.fina_KafRaaIsol \uni0692.fina_KafRaaIsol 
	\uni0693.fina_KafRaaIsol \uni0694.fina_KafRaaIsol \uni0695.fina_KafRaaIsol 
	\uni0696.fina_KafRaaIsol \uni0697.fina_KafRaaIsol \uni0698.fina_KafRaaIsol 
	\uni0699.fina_KafRaaIsol \uni075B.fina_KafRaaIsol \uni06EF.fina_KafRaaIsol 
	\uni0632.fina_KafRaaIsol \uni0771.fina_KafRaaIsol \uni0631.fina_KafRaaIsol 
	\uni076B.fina_KafRaaIsol \uni076C.fina_KafRaaIsol \uni06FC.init_AynHaaInit 
	\uni063A.init_AynHaaInit \uni075E.init_AynHaaInit \uni075D.init_AynHaaInit 
	\uni075F.init_AynHaaInit \uni06A0.init_AynHaaInit \uni0639.init_AynHaaInit 
	\uni063B.medi_KafYaaFina \uni063C.medi_KafYaaFina \uni077F.medi_KafYaaFina 
	\uni0764.medi_KafYaaFina \uni0643.medi_KafYaaFina \uni06B0.medi_KafYaaFina 
	\uni06B3.medi_KafYaaFina \uni06B2.medi_KafYaaFina \uni06AB.medi_KafYaaFina 
	\uni06AC.medi_KafYaaFina \uni06AD.medi_KafYaaFina \uni06AE.medi_KafYaaFina 
	\uni06AF.medi_KafYaaFina \uni06A9.medi_KafYaaFina \uni06B4.medi_KafYaaFina 
	\uni0763.medi_KafYaaFina \uni0762.medi_KafYaaFina \uni06B1.medi_KafYaaFina 
	\uni0777.fina_KafYaaFina \uni06D1.fina_KafYaaFina \uni0775.fina_KafYaaFina 
	\uni063F.fina_KafYaaFina \uni0678.fina_KafYaaFina \uni063D.fina_KafYaaFina 
	\uni063E.fina_KafYaaFina \uni06D0.fina_KafYaaFina \uni0649.fina_KafYaaFina 
	\uni0776.fina_KafYaaFina \uni06CD.fina_KafYaaFina \uni06CC.fina_KafYaaFina 
	\uni0626.fina_KafYaaFina \uni0620.fina_KafYaaFina \uni064A.fina_KafYaaFina 
	\uni06CE.fina_KafYaaFina \uni06B5.init_LamMemHaaInit 
	\uni06B7.init_LamMemHaaInit \uni0644.init_LamMemHaaInit 
	\uni06B8.init_LamMemHaaInit \uni06B6.init_LamMemHaaInit 
	\uni076A.init_LamMemHaaInit \uni0645.medi_LamMemHaaInit 
	\uni062E.medi_LamMemHaaInit \uni062D.medi_LamMemHaaInit 
	\uni0681.medi_LamMemHaaInit \uni0687.medi_LamMemHaaInit 
	\uni0685.medi_LamMemHaaInit \uni062C.medi_LamMemHaaInit 
	\uni0682.medi_LamMemHaaInit \uni0757.medi_LamMemHaaInit 
	\uni0684.medi_LamMemHaaInit \uni076F.medi_LamMemHaaInit 
	\uni076E.medi_LamMemHaaInit \uni0683.medi_LamMemHaaInit 
	\uni06BF.medi_LamMemHaaInit \uni077C.medi_LamMemHaaInit 
	\uni0758.medi_LamMemHaaInit \uni0772.medi_LamMemHaaInit 
	\uni0686.medi_LamMemHaaInit \uni06B5.medi_LamAlfFina 
	\uni06B7.medi_LamAlfFina \uni0644.medi_LamAlfFina \uni06B8.medi_LamAlfFina 
	\uni06B6.medi_LamAlfFina \uni076A.medi_LamAlfFina \uni0625.fina_LamAlfFina 
	\uni0627.fina_LamAlfFina \uni0774.fina_LamAlfFina \uni0773.fina_LamAlfFina 
	\uni0623.fina_LamAlfFina \uni0622.fina_LamAlfFina \uni0675.fina_LamAlfFina 
	\uni0672.fina_LamAlfFina \uni0673.fina_LamAlfFina \uni0671.fina_LamAlfFina 
	\uni06B5.medi_LamMemMedi \uni06B7.medi_LamMemMedi \uni0644.medi_LamMemMedi 
	\uni06B8.medi_LamMemMedi \uni06B6.medi_LamMemMedi \uni076A.medi_LamMemMedi 
	\uni0765.medi_LamMemMedi \uni0645.medi_LamMemMedi \uni0766.medi_LamMemMedi 
	\uni0777.init_BaaBaaHaaInit \uni0680.init_BaaBaaHaaInit 
	\uni0776.init_BaaBaaHaaInit \uni06BC.init_BaaBaaHaaInit 
	\uni0750.init_BaaBaaHaaInit \uni0756.init_BaaBaaHaaInit 
	\uni0768.init_BaaBaaHaaInit \uni06CE.init_BaaBaaHaaInit 
	\uni0775.init_BaaBaaHaaInit \uni06BD.init_BaaBaaHaaInit 
	\uni0626.init_BaaBaaHaaInit \uni066E.init_BaaBaaHaaInit 
	\uni0620.init_BaaBaaHaaInit \uni064A.init_BaaBaaHaaInit 
	\uni06BB.init_BaaBaaHaaInit \uni067F.init_BaaBaaHaaInit 
	\uni0755.init_BaaBaaHaaInit \uni067D.init_BaaBaaHaaInit 
	\uni067E.init_BaaBaaHaaInit \uni067B.init_BaaBaaHaaInit 
	\uni0628.init_BaaBaaHaaInit \uni067A.init_BaaBaaHaaInit 
	\uni0751.init_BaaBaaHaaInit \uni0646.init_BaaBaaHaaInit 
	\uni0753.init_BaaBaaHaaInit \uni0752.init_BaaBaaHaaInit 
	\uni062A.init_BaaBaaHaaInit \uni0678.init_BaaBaaHaaInit 
	\uni063D.init_BaaBaaHaaInit \uni062B.init_BaaBaaHaaInit 
	\uni0679.init_BaaBaaHaaInit \uni06B9.init_BaaBaaHaaInit 
	\uni0769.init_BaaBaaHaaInit \uni0649.init_BaaBaaHaaInit 
	\uni067C.init_BaaBaaHaaInit \uni0754.init_BaaBaaHaaInit 
	\uni06D1.init_BaaBaaHaaInit \uni06D0.init_BaaBaaHaaInit 
	\uni06BA.init_BaaBaaHaaInit \uni06CC.init_BaaBaaHaaInit 
	\uni0767.init_BaaBaaHaaInit \uni0777.medi_BaaBaaHaaInit 
	\uni0680.medi_BaaBaaHaaInit \uni0776.medi_BaaBaaHaaInit 
	\uni06BC.medi_BaaBaaHaaInit \uni0750.medi_BaaBaaHaaInit 
	\uni0756.medi_BaaBaaHaaInit \uni0768.medi_BaaBaaHaaInit 
	\uni06CE.medi_BaaBaaHaaInit \uni0775.medi_BaaBaaHaaInit 
	\uni06BD.medi_BaaBaaHaaInit \uni0626.medi_BaaBaaHaaInit 
	\uni066E.medi_BaaBaaHaaInit \uni0620.medi_BaaBaaHaaInit 
	\uni064A.medi_BaaBaaHaaInit \uni06BB.medi_BaaBaaHaaInit 
	\uni067F.medi_BaaBaaHaaInit \uni0755.medi_BaaBaaHaaInit 
	\uni067D.medi_BaaBaaHaaInit \uni067E.medi_BaaBaaHaaInit 
	\uni067B.medi_BaaBaaHaaInit \uni0628.medi_BaaBaaHaaInit 
	\uni067A.medi_BaaBaaHaaInit \uni0751.medi_BaaBaaHaaInit 
	\uni0646.medi_BaaBaaHaaInit \uni0753.medi_BaaBaaHaaInit 
	\uni0752.medi_BaaBaaHaaInit \uni062A.medi_BaaBaaHaaInit 
	\uni0678.medi_BaaBaaHaaInit \uni063D.medi_BaaBaaHaaInit 
	\uni062B.medi_BaaBaaHaaInit \uni0679.medi_BaaBaaHaaInit 
	\uni06B9.medi_BaaBaaHaaInit \uni0769.medi_BaaBaaHaaInit 
	\uni0649.medi_BaaBaaHaaInit \uni067C.medi_BaaBaaHaaInit 
	\uni0754.medi_BaaBaaHaaInit \uni06D1.medi_BaaBaaHaaInit 
	\uni06D0.medi_BaaBaaHaaInit \uni06BA.medi_BaaBaaHaaInit 
	\uni06CC.medi_BaaBaaHaaInit \uni0767.medi_BaaBaaHaaInit 
	\uni062E.medi_BaaBaaHaaInit \uni062D.medi_BaaBaaHaaInit 
	\uni0681.medi_BaaBaaHaaInit \uni0687.medi_BaaBaaHaaInit 
	\uni0685.medi_BaaBaaHaaInit \uni062C.medi_BaaBaaHaaInit 
	\uni0682.medi_BaaBaaHaaInit \uni0757.medi_BaaBaaHaaInit 
	\uni0684.medi_BaaBaaHaaInit \uni076F.medi_BaaBaaHaaInit 
	\uni076E.medi_BaaBaaHaaInit \uni0683.medi_BaaBaaHaaInit 
	\uni06BF.medi_BaaBaaHaaInit \uni077C.medi_BaaBaaHaaInit 
	\uni0758.medi_BaaBaaHaaInit \uni0772.medi_BaaBaaHaaInit 
	\uni0686.medi_BaaBaaHaaInit \uni0777.medi_SenBaaMemInit 
	\uni0680.medi_SenBaaMemInit \uni0776.medi_SenBaaMemInit 
	\uni06BC.medi_SenBaaMemInit \uni0750.medi_SenBaaMemInit 
	\uni0756.medi_SenBaaMemInit \uni0768.medi_SenBaaMemInit 
	\uni06CE.medi_SenBaaMemInit \uni0775.medi_SenBaaMemInit 
	\uni06BD.medi_SenBaaMemInit \uni0626.medi_SenBaaMemInit 
	\uni066E.medi_SenBaaMemInit \uni0620.medi_SenBaaMemInit 
	\uni064A.medi_SenBaaMemInit \uni06BB.medi_SenBaaMemInit 
	\uni067F.medi_SenBaaMemInit \uni0755.medi_SenBaaMemInit 
	\uni067D.medi_SenBaaMemInit \uni067E.medi_SenBaaMemInit 
	\uni067B.medi_SenBaaMemInit \uni0628.medi_SenBaaMemInit 
	\uni067A.medi_SenBaaMemInit \uni0751.medi_SenBaaMemInit 
	\uni0646.medi_SenBaaMemInit \uni0753.medi_SenBaaMemInit 
	\uni0752.medi_SenBaaMemInit \uni062A.medi_SenBaaMemInit 
	\uni0678.medi_SenBaaMemInit \uni063D.medi_SenBaaMemInit 
	\uni062B.medi_SenBaaMemInit \uni0679.medi_SenBaaMemInit 
	\uni06B9.medi_SenBaaMemInit \uni0769.medi_SenBaaMemInit 
	\uni0649.medi_SenBaaMemInit \uni067C.medi_SenBaaMemInit 
	\uni0754.medi_SenBaaMemInit \uni06D1.medi_SenBaaMemInit 
	\uni06D0.medi_SenBaaMemInit \uni06BA.medi_SenBaaMemInit 
	\uni06CC.medi_SenBaaMemInit \uni0767.medi_SenBaaMemInit 
	\uni0645.medi_SenBaaMemInit \uni0777.init_BaaBaaIsol 
	\uni0680.init_BaaBaaIsol \uni0776.init_BaaBaaIsol \uni06BC.init_BaaBaaIsol 
	\uni0750.init_BaaBaaIsol \uni0756.init_BaaBaaIsol \uni0768.init_BaaBaaIsol 
	\uni06CE.init_BaaBaaIsol \uni0775.init_BaaBaaIsol \uni06BD.init_BaaBaaIsol 
	\uni0626.init_BaaBaaIsol \uni066E.init_BaaBaaIsol \uni0620.init_BaaBaaIsol 
	\uni064A.init_BaaBaaIsol \uni06BB.init_BaaBaaIsol \uni067F.init_BaaBaaIsol 
	\uni0755.init_BaaBaaIsol \uni067D.init_BaaBaaIsol \uni067E.init_BaaBaaIsol 
	\uni067B.init_BaaBaaIsol \uni0628.init_BaaBaaIsol \uni067A.init_BaaBaaIsol 
	\uni0751.init_BaaBaaIsol \uni0646.init_BaaBaaIsol \uni0753.init_BaaBaaIsol 
	\uni0752.init_BaaBaaIsol \uni062A.init_BaaBaaIsol \uni0678.init_BaaBaaIsol 
	\uni063D.init_BaaBaaIsol \uni062B.init_BaaBaaIsol \uni0679.init_BaaBaaIsol 
	\uni06B9.init_BaaBaaIsol \uni0769.init_BaaBaaIsol \uni0649.init_BaaBaaIsol 
	\uni067C.init_BaaBaaIsol \uni0754.init_BaaBaaIsol \uni06D1.init_BaaBaaIsol 
	\uni06D0.init_BaaBaaIsol \uni06BA.init_BaaBaaIsol \uni06CC.init_BaaBaaIsol 
	\uni0767.init_BaaBaaIsol \uni0751.fina_BaaBaaIsol \uni0750.fina_BaaBaaIsol 
	\uni0753.fina_BaaBaaIsol \uni0680.fina_BaaBaaIsol \uni062A.fina_BaaBaaIsol 
	\uni0754.fina_BaaBaaIsol \uni062B.fina_BaaBaaIsol \uni0679.fina_BaaBaaIsol 
	\uni067C.fina_BaaBaaIsol \uni0756.fina_BaaBaaIsol \uni0752.fina_BaaBaaIsol 
	\uni066E.fina_BaaBaaIsol \uni067F.fina_BaaBaaIsol \uni0755.fina_BaaBaaIsol 
	\uni067D.fina_BaaBaaIsol \uni067E.fina_BaaBaaIsol \uni067B.fina_BaaBaaIsol 
	\uni0628.fina_BaaBaaIsol \uni067A.fina_BaaBaaIsol 
	\uni0777.init_BaaBaaMemInit \uni0680.init_BaaBaaMemInit 
	\uni0776.init_BaaBaaMemInit \uni06BC.init_BaaBaaMemInit 
	\uni0750.init_BaaBaaMemInit \uni0756.init_BaaBaaMemInit 
	\uni0768.init_BaaBaaMemInit \uni06CE.init_BaaBaaMemInit 
	\uni0775.init_BaaBaaMemInit \uni06BD.init_BaaBaaMemInit 
	\uni0626.init_BaaBaaMemInit \uni066E.init_BaaBaaMemInit 
	\uni0620.init_BaaBaaMemInit \uni064A.init_BaaBaaMemInit 
	\uni06BB.init_BaaBaaMemInit \uni067F.init_BaaBaaMemInit 
	\uni0755.init_BaaBaaMemInit \uni067D.init_BaaBaaMemInit 
	\uni067E.init_BaaBaaMemInit \uni067B.init_BaaBaaMemInit 
	\uni0628.init_BaaBaaMemInit \uni067A.init_BaaBaaMemInit 
	\uni0751.init_BaaBaaMemInit \uni0646.init_BaaBaaMemInit 
	\uni0753.init_BaaBaaMemInit \uni0752.init_BaaBaaMemInit 
	\uni062A.init_BaaBaaMemInit \uni0678.init_BaaBaaMemInit 
	\uni063D.init_BaaBaaMemInit \uni062B.init_BaaBaaMemInit 
	\uni0679.init_BaaBaaMemInit \uni06B9.init_BaaBaaMemInit 
	\uni0769.init_BaaBaaMemInit \uni0649.init_BaaBaaMemInit 
	\uni067C.init_BaaBaaMemInit \uni0754.init_BaaBaaMemInit 
	\uni06D1.init_BaaBaaMemInit \uni06D0.init_BaaBaaMemInit 
	\uni06BA.init_BaaBaaMemInit \uni06CC.init_BaaBaaMemInit 
	\uni0767.init_BaaBaaMemInit \uni0777.medi_BaaBaaMemInit 
	\uni0680.medi_BaaBaaMemInit \uni0776.medi_BaaBaaMemInit 
	\uni06BC.medi_BaaBaaMemInit \uni0750.medi_BaaBaaMemInit 
	\uni0756.medi_BaaBaaMemInit \uni0768.medi_BaaBaaMemInit 
	\uni06CE.medi_BaaBaaMemInit \uni0775.medi_BaaBaaMemInit 
	\uni06BD.medi_BaaBaaMemInit \uni0626.medi_BaaBaaMemInit 
	\uni066E.medi_BaaBaaMemInit \uni0620.medi_BaaBaaMemInit 
	\uni064A.medi_BaaBaaMemInit \uni06BB.medi_BaaBaaMemInit 
	\uni067F.medi_BaaBaaMemInit \uni0755.medi_BaaBaaMemInit 
	\uni067D.medi_BaaBaaMemInit \uni067E.medi_BaaBaaMemInit 
	\uni067B.medi_BaaBaaMemInit \uni0628.medi_BaaBaaMemInit 
	\uni067A.medi_BaaBaaMemInit \uni0751.medi_BaaBaaMemInit 
	\uni0646.medi_BaaBaaMemInit \uni0753.medi_BaaBaaMemInit 
	\uni0752.medi_BaaBaaMemInit \uni062A.medi_BaaBaaMemInit 
	\uni0678.medi_BaaBaaMemInit \uni063D.medi_BaaBaaMemInit 
	\uni062B.medi_BaaBaaMemInit \uni0679.medi_BaaBaaMemInit 
	\uni06B9.medi_BaaBaaMemInit \uni0769.medi_BaaBaaMemInit 
	\uni0649.medi_BaaBaaMemInit \uni067C.medi_BaaBaaMemInit 
	\uni0754.medi_BaaBaaMemInit \uni06D1.medi_BaaBaaMemInit 
	\uni06D0.medi_BaaBaaMemInit \uni06BA.medi_BaaBaaMemInit 
	\uni06CC.medi_BaaBaaMemInit \uni0767.medi_BaaBaaMemInit 
	\uni0645.medi_BaaBaaMemInit \uni063B.medi_KafBaaMedi 
	\uni063C.medi_KafBaaMedi \uni077F.medi_KafBaaMedi \uni0764.medi_KafBaaMedi 
	\uni0643.medi_KafBaaMedi \uni06B0.medi_KafBaaMedi \uni06B3.medi_KafBaaMedi 
	\uni06B2.medi_KafBaaMedi \uni06AB.medi_KafBaaMedi \uni06AC.medi_KafBaaMedi 
	\uni06AD.medi_KafBaaMedi \uni06AE.medi_KafBaaMedi \uni06AF.medi_KafBaaMedi 
	\uni06A9.medi_KafBaaMedi \uni06B4.medi_KafBaaMedi \uni0763.medi_KafBaaMedi 
	\uni0762.medi_KafBaaMedi \uni06B1.medi_KafBaaMedi \uni0777.medi_KafBaaMedi 
	\uni0680.medi_KafBaaMedi \uni0776.medi_KafBaaMedi \uni06BC.medi_KafBaaMedi 
	\uni0750.medi_KafBaaMedi \uni0756.medi_KafBaaMedi \uni0768.medi_KafBaaMedi 
	\uni06CE.medi_KafBaaMedi \uni0775.medi_KafBaaMedi \uni06BD.medi_KafBaaMedi 
	\uni0626.medi_KafBaaMedi \uni066E.medi_KafBaaMedi \uni0620.medi_KafBaaMedi 
	\uni064A.medi_KafBaaMedi \uni06BB.medi_KafBaaMedi \uni067F.medi_KafBaaMedi 
	\uni0755.medi_KafBaaMedi \uni067D.medi_KafBaaMedi \uni067E.medi_KafBaaMedi 
	\uni067B.medi_KafBaaMedi \uni0628.medi_KafBaaMedi \uni067A.medi_KafBaaMedi 
	\uni0751.medi_KafBaaMedi \uni0646.medi_KafBaaMedi \uni0753.medi_KafBaaMedi 
	\uni0752.medi_KafBaaMedi \uni062A.medi_KafBaaMedi \uni0678.medi_KafBaaMedi 
	\uni063D.medi_KafBaaMedi \uni062B.medi_KafBaaMedi \uni0679.medi_KafBaaMedi 
	\uni06B9.medi_KafBaaMedi \uni0769.medi_KafBaaMedi \uni0649.medi_KafBaaMedi 
	\uni067C.medi_KafBaaMedi \uni0754.medi_KafBaaMedi \uni06D1.medi_KafBaaMedi 
	\uni06D0.medi_KafBaaMedi \uni06BA.medi_KafBaaMedi \uni06CC.medi_KafBaaMedi 
	\uni0767.medi_KafBaaMedi \uni0777.medi_BaaNonFina \uni0680.medi_BaaNonFina 
	\uni0776.medi_BaaNonFina \uni06BC.medi_BaaNonFina \uni0750.medi_BaaNonFina 
	\uni0756.medi_BaaNonFina \uni0768.medi_BaaNonFina \uni06CE.medi_BaaNonFina 
	\uni0775.medi_BaaNonFina \uni06BD.medi_BaaNonFina \uni0626.medi_BaaNonFina 
	\uni066E.medi_BaaNonFina \uni0620.medi_BaaNonFina \uni064A.medi_BaaNonFina 
	\uni06BB.medi_BaaNonFina \uni067F.medi_BaaNonFina \uni0755.medi_BaaNonFina 
	\uni067D.medi_BaaNonFina \uni067E.medi_BaaNonFina \uni067B.medi_BaaNonFina 
	\uni0628.medi_BaaNonFina \uni067A.medi_BaaNonFina \uni0751.medi_BaaNonFina 
	\uni0646.medi_BaaNonFina \uni0753.medi_BaaNonFina \uni0752.medi_BaaNonFina 
	\uni062A.medi_BaaNonFina \uni0678.medi_BaaNonFina \uni063D.medi_BaaNonFina 
	\uni062B.medi_BaaNonFina \uni0679.medi_BaaNonFina \uni06B9.medi_BaaNonFina 
	\uni0769.medi_BaaNonFina \uni0649.medi_BaaNonFina \uni067C.medi_BaaNonFina 
	\uni0754.medi_BaaNonFina \uni06D1.medi_BaaNonFina \uni06D0.medi_BaaNonFina 
	\uni06BA.medi_BaaNonFina \uni06CC.medi_BaaNonFina \uni0767.medi_BaaNonFina 
	\uni0646.fina_BaaNonFina \uni0767.fina_BaaNonFina \uni06BA.fina_BaaNonFina 
	\uni06BC.fina_BaaNonFina \uni06BB.fina_BaaNonFina \uni0768.fina_BaaNonFina 
	\uni06B9.fina_BaaNonFina \uni0769.fina_BaaNonFina \uni06BD.fina_BaaNonFina 
	\uni062E.init_HaaRaaIsol \uni062D.init_HaaRaaIsol \uni0681.init_HaaRaaIsol 
	\uni0687.init_HaaRaaIsol \uni0685.init_HaaRaaIsol \uni062C.init_HaaRaaIsol 
	\uni0682.init_HaaRaaIsol \uni0757.init_HaaRaaIsol \uni0684.init_HaaRaaIsol 
	\uni076F.init_HaaRaaIsol \uni076E.init_HaaRaaIsol \uni0683.init_HaaRaaIsol 
	\uni06BF.init_HaaRaaIsol \uni077C.init_HaaRaaIsol \uni0758.init_HaaRaaIsol 
	\uni0772.init_HaaRaaIsol \uni0686.init_HaaRaaIsol \uni0691.fina_HaaRaaIsol 
	\uni0692.fina_HaaRaaIsol \uni0693.fina_HaaRaaIsol \uni0694.fina_HaaRaaIsol 
	\uni0695.fina_HaaRaaIsol \uni0696.fina_HaaRaaIsol \uni0697.fina_HaaRaaIsol 
	\uni0698.fina_HaaRaaIsol \uni0699.fina_HaaRaaIsol \uni075B.fina_HaaRaaIsol 
	\uni06EF.fina_HaaRaaIsol \uni0632.fina_HaaRaaIsol \uni0771.fina_HaaRaaIsol 
	\uni0631.fina_HaaRaaIsol \uni076B.fina_HaaRaaIsol \uni076C.fina_HaaRaaIsol 
	\uni0647.init_HehHaaInit \uni06C1.init_HehHaaInit \uni06B5.init_LamRaaIsol 
	\uni06B7.init_LamRaaIsol \uni0644.init_LamRaaIsol \uni06B8.init_LamRaaIsol 
	\uni06B6.init_LamRaaIsol \uni076A.init_LamRaaIsol \uni0691.fina_LamRaaIsol 
	\uni0692.fina_LamRaaIsol \uni0693.fina_LamRaaIsol \uni0694.fina_LamRaaIsol 
	\uni0695.fina_LamRaaIsol \uni0696.fina_LamRaaIsol \uni0697.fina_LamRaaIsol 
	\uni0698.fina_LamRaaIsol \uni0699.fina_LamRaaIsol \uni075B.fina_LamRaaIsol 
	\uni06EF.fina_LamRaaIsol \uni0632.fina_LamRaaIsol \uni0771.fina_LamRaaIsol 
	\uni0631.fina_LamRaaIsol \uni076B.fina_LamRaaIsol \uni076C.fina_LamRaaIsol 
	\uni069D.init_SadHaaInit \uni06FB.init_SadHaaInit \uni0636.init_SadHaaInit 
	\uni069E.init_SadHaaInit \uni0635.init_SadHaaInit \uni062E.medi_SadHaaInit 
	\uni062D.medi_SadHaaInit \uni0681.medi_SadHaaInit \uni0687.medi_SadHaaInit 
	\uni0685.medi_SadHaaInit \uni062C.medi_SadHaaInit \uni0682.medi_SadHaaInit 
	\uni0757.medi_SadHaaInit \uni0684.medi_SadHaaInit \uni076F.medi_SadHaaInit 
	\uni076E.medi_SadHaaInit \uni0683.medi_SadHaaInit \uni06BF.medi_SadHaaInit 
	\uni077C.medi_SadHaaInit \uni0758.medi_SadHaaInit \uni0772.medi_SadHaaInit 
	\uni0686.medi_SadHaaInit \uni0777.medi_BaaYaaFina \uni0680.medi_BaaYaaFina 
	\uni0776.medi_BaaYaaFina \uni06BC.medi_BaaYaaFina \uni0750.medi_BaaYaaFina 
	\uni0756.medi_BaaYaaFina \uni0768.medi_BaaYaaFina \uni06CE.medi_BaaYaaFina 
	\uni0775.medi_BaaYaaFina \uni06BD.medi_BaaYaaFina \uni0626.medi_BaaYaaFina 
	\uni066E.medi_BaaYaaFina \uni0620.medi_BaaYaaFina \uni064A.medi_BaaYaaFina 
	\uni06BB.medi_BaaYaaFina \uni067F.medi_BaaYaaFina \uni0755.medi_BaaYaaFina 
	\uni067D.medi_BaaYaaFina \uni067E.medi_BaaYaaFina \uni067B.medi_BaaYaaFina 
	\uni0628.medi_BaaYaaFina \uni067A.medi_BaaYaaFina \uni0751.medi_BaaYaaFina 
	\uni0646.medi_BaaYaaFina \uni0753.medi_BaaYaaFina \uni0752.medi_BaaYaaFina 
	\uni062A.medi_BaaYaaFina \uni0678.medi_BaaYaaFina \uni063D.medi_BaaYaaFina 
	\uni062B.medi_BaaYaaFina \uni0679.medi_BaaYaaFina \uni06B9.medi_BaaYaaFina 
	\uni0769.medi_BaaYaaFina \uni0649.medi_BaaYaaFina \uni067C.medi_BaaYaaFina 
	\uni0754.medi_BaaYaaFina \uni06D1.medi_BaaYaaFina \uni06D0.medi_BaaYaaFina 
	\uni06BA.medi_BaaYaaFina \uni06CC.medi_BaaYaaFina \uni0767.medi_BaaYaaFina 
	\uni0777.fina_BaaYaaFina \uni06D1.fina_BaaYaaFina \uni0775.fina_BaaYaaFina 
	\uni063F.fina_BaaYaaFina \uni0678.fina_BaaYaaFina \uni063D.fina_BaaYaaFina 
	\uni063E.fina_BaaYaaFina \uni06D0.fina_BaaYaaFina \uni0649.fina_BaaYaaFina 
	\uni0776.fina_BaaYaaFina \uni06CD.fina_BaaYaaFina \uni06CC.fina_BaaYaaFina 
	\uni0626.fina_BaaYaaFina \uni0620.fina_BaaYaaFina \uni064A.fina_BaaYaaFina 
	\uni06CE.fina_BaaYaaFina \uni0777.init_BaaSenAltInit 
	\uni0680.init_BaaSenAltInit \uni0776.init_BaaSenAltInit 
	\uni06BC.init_BaaSenAltInit \uni0750.init_BaaSenAltInit 
	\uni0756.init_BaaSenAltInit \uni0768.init_BaaSenAltInit 
	\uni06CE.init_BaaSenAltInit \uni0775.init_BaaSenAltInit 
	\uni06BD.init_BaaSenAltInit \uni0626.init_BaaSenAltInit 
	\uni066E.init_BaaSenAltInit \uni0620.init_BaaSenAltInit 
	\uni064A.init_BaaSenAltInit \uni06BB.init_BaaSenAltInit 
	\uni067F.init_BaaSenAltInit \uni0755.init_BaaSenAltInit 
	\uni067D.init_BaaSenAltInit \uni067E.init_BaaSenAltInit 
	\uni067B.init_BaaSenAltInit \uni0628.init_BaaSenAltInit 
	\uni067A.init_BaaSenAltInit \uni0751.init_BaaSenAltInit 
	\uni0646.init_BaaSenAltInit \uni0753.init_BaaSenAltInit 
	\uni0752.init_BaaSenAltInit \uni062A.init_BaaSenAltInit 
	\uni0678.init_BaaSenAltInit \uni063D.init_BaaSenAltInit 
	\uni062B.init_BaaSenAltInit \uni0679.init_BaaSenAltInit 
	\uni06B9.init_BaaSenAltInit \uni0769.init_BaaSenAltInit 
	\uni0649.init_BaaSenAltInit \uni067C.init_BaaSenAltInit 
	\uni0754.init_BaaSenAltInit \uni06D1.init_BaaSenAltInit 
	\uni06D0.init_BaaSenAltInit \uni06BA.init_BaaSenAltInit 
	\uni06CC.init_BaaSenAltInit \uni0767.init_BaaSenAltInit 
	\uni06FA.medi_BaaSenAltInit \uni076D.medi_BaaSenAltInit 
	\uni0633.medi_BaaSenAltInit \uni077E.medi_BaaSenAltInit 
	\uni077D.medi_BaaSenAltInit \uni0634.medi_BaaSenAltInit 
	\uni0770.medi_BaaSenAltInit \uni075C.medi_BaaSenAltInit 
	\uni069A.medi_BaaSenAltInit \uni069B.medi_BaaSenAltInit 
	\uni069C.medi_BaaSenAltInit \uni0691.fina_PostTooth 
	\uni0692.fina_PostTooth \uni0693.fina_PostTooth \uni0694.fina_PostTooth 
	\uni0695.fina_PostTooth \uni0696.fina_PostTooth \uni0697.fina_PostTooth 
	\uni0698.fina_PostTooth \uni0699.fina_PostTooth \uni075B.fina_PostTooth 
	\uni06EF.fina_PostTooth \uni0632.fina_PostTooth \uni0771.fina_PostTooth 
	\uni0631.fina_PostTooth \uni076B.fina_PostTooth \uni076C.fina_PostTooth 
	\uni0777.fina_PostTooth \uni06D1.fina_PostTooth \uni0775.fina_PostTooth 
	\uni063F.fina_PostTooth \uni0678.fina_PostTooth \uni063D.fina_PostTooth 
	\uni063E.fina_PostTooth \uni06D0.fina_PostTooth \uni0649.fina_PostTooth 
	\uni0776.fina_PostTooth \uni06CD.fina_PostTooth \uni06CC.fina_PostTooth 
	\uni0626.fina_PostTooth \uni0620.fina_PostTooth \uni064A.fina_PostTooth 
	\uni06CE.fina_PostTooth \uni0777.init_AboveHaa \uni0680.init_AboveHaa 
	\uni0776.init_AboveHaa \uni06BC.init_AboveHaa \uni0750.init_AboveHaa 
	\uni0756.init_AboveHaa \uni0768.init_AboveHaa \uni06CE.init_AboveHaa 
	\uni0775.init_AboveHaa \uni06BD.init_AboveHaa \uni0626.init_AboveHaa 
	\uni066E.init_AboveHaa \uni0620.init_AboveHaa \uni064A.init_AboveHaa 
	\uni06BB.init_AboveHaa \uni067F.init_AboveHaa \uni0755.init_AboveHaa 
	\uni067D.init_AboveHaa \uni067E.init_AboveHaa \uni067B.init_AboveHaa 
	\uni0628.init_AboveHaa \uni067A.init_AboveHaa \uni0751.init_AboveHaa 
	\uni0646.init_AboveHaa \uni0753.init_AboveHaa \uni0752.init_AboveHaa 
	\uni062A.init_AboveHaa \uni0678.init_AboveHaa \uni063D.init_AboveHaa 
	\uni062B.init_AboveHaa \uni0679.init_AboveHaa \uni06B9.init_AboveHaa 
	\uni0769.init_AboveHaa \uni0649.init_AboveHaa \uni067C.init_AboveHaa 
	\uni0754.init_AboveHaa \uni06D1.init_AboveHaa \uni06D0.init_AboveHaa 
	\uni06BA.init_AboveHaa \uni06CC.init_AboveHaa \uni0767.init_AboveHaa 
	\uni0777.init_BaaHaaInit \uni0680.init_BaaHaaInit \uni0776.init_BaaHaaInit 
	\uni06BC.init_BaaHaaInit \uni0750.init_BaaHaaInit \uni0756.init_BaaHaaInit 
	\uni0768.init_BaaHaaInit \uni06CE.init_BaaHaaInit \uni0775.init_BaaHaaInit 
	\uni06BD.init_BaaHaaInit \uni0626.init_BaaHaaInit \uni066E.init_BaaHaaInit 
	\uni0620.init_BaaHaaInit \uni064A.init_BaaHaaInit \uni06BB.init_BaaHaaInit 
	\uni067F.init_BaaHaaInit \uni0755.init_BaaHaaInit \uni067D.init_BaaHaaInit 
	\uni067E.init_BaaHaaInit \uni067B.init_BaaHaaInit \uni0628.init_BaaHaaInit 
	\uni067A.init_BaaHaaInit \uni0751.init_BaaHaaInit \uni0646.init_BaaHaaInit 
	\uni0753.init_BaaHaaInit \uni0752.init_BaaHaaInit \uni062A.init_BaaHaaInit 
	\uni0678.init_BaaHaaInit \uni063D.init_BaaHaaInit \uni062B.init_BaaHaaInit 
	\uni0679.init_BaaHaaInit \uni06B9.init_BaaHaaInit \uni0769.init_BaaHaaInit 
	\uni0649.init_BaaHaaInit \uni067C.init_BaaHaaInit \uni0754.init_BaaHaaInit 
	\uni06D1.init_BaaHaaInit \uni06D0.init_BaaHaaInit \uni06BA.init_BaaHaaInit 
	\uni06CC.init_BaaHaaInit \uni0767.init_BaaHaaInit 
	\uni0777.init_BaaHaaMemInit \uni0680.init_BaaHaaMemInit 
	\uni0776.init_BaaHaaMemInit \uni06BC.init_BaaHaaMemInit 
	\uni0750.init_BaaHaaMemInit \uni0756.init_BaaHaaMemInit 
	\uni0768.init_BaaHaaMemInit \uni06CE.init_BaaHaaMemInit 
	\uni0775.init_BaaHaaMemInit \uni06BD.init_BaaHaaMemInit 
	\uni0626.init_BaaHaaMemInit \uni066E.init_BaaHaaMemInit 
	\uni0620.init_BaaHaaMemInit \uni064A.init_BaaHaaMemInit 
	\uni06BB.init_BaaHaaMemInit \uni067F.init_BaaHaaMemInit 
	\uni0755.init_BaaHaaMemInit \uni067D.init_BaaHaaMemInit 
	\uni067E.init_BaaHaaMemInit \uni067B.init_BaaHaaMemInit 
	\uni0628.init_BaaHaaMemInit \uni067A.init_BaaHaaMemInit 
	\uni0751.init_BaaHaaMemInit \uni0646.init_BaaHaaMemInit 
	\uni0753.init_BaaHaaMemInit \uni0752.init_BaaHaaMemInit 
	\uni062A.init_BaaHaaMemInit \uni0678.init_BaaHaaMemInit 
	\uni063D.init_BaaHaaMemInit \uni062B.init_BaaHaaMemInit 
	\uni0679.init_BaaHaaMemInit \uni06B9.init_BaaHaaMemInit 
	\uni0769.init_BaaHaaMemInit \uni0649.init_BaaHaaMemInit 
	\uni067C.init_BaaHaaMemInit \uni0754.init_BaaHaaMemInit 
	\uni06D1.init_BaaHaaMemInit \uni06D0.init_BaaHaaMemInit 
	\uni06BA.init_BaaHaaMemInit \uni06CC.init_BaaHaaMemInit 
	\uni0767.init_BaaHaaMemInit \uni062E.medi_BaaHaaMemInit 
	\uni062D.medi_BaaHaaMemInit \uni0681.medi_BaaHaaMemInit 
	\uni0687.medi_BaaHaaMemInit \uni0685.medi_BaaHaaMemInit 
	\uni062C.medi_BaaHaaMemInit \uni0682.medi_BaaHaaMemInit 
	\uni0757.medi_BaaHaaMemInit \uni0684.medi_BaaHaaMemInit 
	\uni076F.medi_BaaHaaMemInit \uni076E.medi_BaaHaaMemInit 
	\uni0683.medi_BaaHaaMemInit \uni06BF.medi_BaaHaaMemInit 
	\uni077C.medi_BaaHaaMemInit \uni0758.medi_BaaHaaMemInit 
	\uni0772.medi_BaaHaaMemInit \uni0686.medi_BaaHaaMemInit 
	\uni062E.fina_AboveHaaIsol \uni062D.fina_AboveHaaIsol 
	\uni0681.fina_AboveHaaIsol \uni0687.fina_AboveHaaIsol 
	\uni0685.fina_AboveHaaIsol \uni062C.fina_AboveHaaIsol 
	\uni0682.fina_AboveHaaIsol \uni0757.fina_AboveHaaIsol 
	\uni0684.fina_AboveHaaIsol \uni076F.fina_AboveHaaIsol 
	\uni076E.fina_AboveHaaIsol \uni0683.fina_AboveHaaIsol 
	\uni06BF.fina_AboveHaaIsol \uni077C.fina_AboveHaaIsol 
	\uni0758.fina_AboveHaaIsol \uni0772.fina_AboveHaaIsol 
	\uni0686.fina_AboveHaaIsol \uni06B5.init_LamHaaHaaInit 
	\uni06B7.init_LamHaaHaaInit \uni0644.init_LamHaaHaaInit 
	\uni06B8.init_LamHaaHaaInit \uni06B6.init_LamHaaHaaInit 
	\uni076A.init_LamHaaHaaInit \uni06FC.init_Finjani \uni063A.init_Finjani 
	\uni075E.init_Finjani \uni075D.init_Finjani \uni075F.init_Finjani 
	\uni06A0.init_Finjani \uni0639.init_Finjani \uni062E.init_Finjani 
	\uni062D.init_Finjani \uni0681.init_Finjani \uni0687.init_Finjani 
	\uni0685.init_Finjani \uni062C.init_Finjani \uni0682.init_Finjani 
	\uni0757.init_Finjani \uni0684.init_Finjani \uni076F.init_Finjani 
	\uni076E.init_Finjani \uni0683.init_Finjani \uni06BF.init_Finjani 
	\uni077C.init_Finjani \uni0758.init_Finjani \uni0772.init_Finjani 
	\uni0686.init_Finjani \uni062E.medi_Finjani \uni062D.medi_Finjani 
	\uni0681.medi_Finjani \uni0687.medi_Finjani \uni0685.medi_Finjani 
	\uni062C.medi_Finjani \uni0682.medi_Finjani \uni0757.medi_Finjani 
	\uni0684.medi_Finjani \uni076F.medi_Finjani \uni076E.medi_Finjani 
	\uni0683.medi_Finjani \uni06BF.medi_Finjani \uni077C.medi_Finjani 
	\uni0758.medi_Finjani \uni0772.medi_Finjani \uni0686.medi_Finjani 
	\uni06FA.init_PreYaa \uni076D.init_PreYaa \uni0633.init_PreYaa 
	\uni077E.init_PreYaa \uni077D.init_PreYaa \uni0634.init_PreYaa 
	\uni0770.init_PreYaa \uni075C.init_PreYaa \uni069A.init_PreYaa 
	\uni069B.init_PreYaa \uni069C.init_PreYaa \uni06FA.medi_PreYaa 
	\uni076D.medi_PreYaa \uni0633.medi_PreYaa \uni077E.medi_PreYaa 
	\uni077D.medi_PreYaa \uni0634.medi_PreYaa \uni0770.medi_PreYaa 
	\uni075C.medi_PreYaa \uni069A.medi_PreYaa \uni069B.medi_PreYaa 
	\uni069C.medi_PreYaa \uni069D.init_PreYaa \uni06FB.init_PreYaa 
	\uni0636.init_PreYaa \uni069E.init_PreYaa \uni0635.init_PreYaa 
	\uni069D.medi_PreYaa \uni06FB.medi_PreYaa \uni0636.medi_PreYaa 
	\uni069E.medi_PreYaa \uni0635.medi_PreYaa \uni0777.init_High 
	\uni0680.init_High \uni0776.init_High \uni06BC.init_High \uni0750.init_High 
	\uni0756.init_High \uni0768.init_High \uni06CE.init_High \uni0775.init_High 
	\uni06BD.init_High \uni0626.init_High \uni066E.init_High \uni0620.init_High 
	\uni064A.init_High \uni06BB.init_High \uni067F.init_High \uni0755.init_High 
	\uni067D.init_High \uni067E.init_High \uni067B.init_High \uni0628.init_High 
	\uni067A.init_High \uni0751.init_High \uni0646.init_High \uni0753.init_High 
	\uni0752.init_High \uni062A.init_High \uni0678.init_High \uni063D.init_High 
	\uni062B.init_High \uni0679.init_High \uni06B9.init_High \uni0769.init_High 
	\uni0649.init_High \uni067C.init_High \uni0754.init_High \uni06D1.init_High 
	\uni06D0.init_High \uni06BA.init_High \uni06CC.init_High \uni0767.init_High 
	\uni0777.medi_High \uni0680.medi_High \uni0776.medi_High \uni06BC.medi_High 
	\uni0750.medi_High \uni0756.medi_High \uni0768.medi_High \uni06CE.medi_High 
	\uni0775.medi_High \uni06BD.medi_High \uni0626.medi_High \uni066E.medi_High 
	\uni0620.medi_High \uni064A.medi_High \uni06BB.medi_High \uni067F.medi_High 
	\uni0755.medi_High \uni067D.medi_High \uni067E.medi_High \uni067B.medi_High 
	\uni0628.medi_High \uni067A.medi_High \uni0751.medi_High \uni0646.medi_High 
	\uni0753.medi_High \uni0752.medi_High \uni062A.medi_High \uni0678.medi_High 
	\uni063D.medi_High \uni062B.medi_High \uni0679.medi_High \uni06B9.medi_High 
	\uni0769.medi_High \uni0649.medi_High \uni067C.medi_High \uni0754.medi_High 
	\uni06D1.medi_High \uni06D0.medi_High \uni06BA.medi_High \uni06CC.medi_High 
	\uni0767.medi_High \uni06FA.fina_BaaSen \uni076D.fina_BaaSen 
	\uni0633.fina_BaaSen \uni077E.fina_BaaSen \uni077D.fina_BaaSen 
	\uni0634.fina_BaaSen \uni0770.fina_BaaSen \uni075C.fina_BaaSen 
	\uni069A.fina_BaaSen \uni069B.fina_BaaSen \uni069C.fina_BaaSen 
	\uni0645.fina_PostTooth \uni0777.init_Wide \uni0680.init_Wide 
	\uni0776.init_Wide \uni06BC.init_Wide \uni0750.init_Wide \uni0756.init_Wide 
	\uni0768.init_Wide \uni06CE.init_Wide \uni0775.init_Wide \uni06BD.init_Wide 
	\uni0626.init_Wide \uni066E.init_Wide \uni0620.init_Wide \uni064A.init_Wide 
	\uni06BB.init_Wide \uni067F.init_Wide \uni0755.init_Wide \uni067D.init_Wide 
	\uni067E.init_Wide \uni067B.init_Wide \uni0628.init_Wide \uni067A.init_Wide 
	\uni0751.init_Wide \uni0646.init_Wide \uni0753.init_Wide \uni0752.init_Wide 
	\uni062A.init_Wide \uni0678.init_Wide \uni063D.init_Wide \uni062B.init_Wide 
	\uni0679.init_Wide \uni06B9.init_Wide \uni0769.init_Wide \uni0649.init_Wide 
	\uni067C.init_Wide \uni0754.init_Wide \uni06D1.init_Wide \uni06D0.init_Wide 
	\uni06BA.init_Wide \uni06CC.init_Wide \uni0767.init_Wide 
	\uni062E.medi_HaaHaaInit \uni062D.medi_HaaHaaInit \uni0681.medi_HaaHaaInit 
	\uni0687.medi_HaaHaaInit \uni0685.medi_HaaHaaInit \uni062C.medi_HaaHaaInit 
	\uni0682.medi_HaaHaaInit \uni0757.medi_HaaHaaInit \uni0684.medi_HaaHaaInit 
	\uni076F.medi_HaaHaaInit \uni076E.medi_HaaHaaInit \uni0683.medi_HaaHaaInit 
	\uni06BF.medi_HaaHaaInit \uni077C.medi_HaaHaaInit \uni0758.medi_HaaHaaInit 
	\uni0772.medi_HaaHaaInit \uni0686.medi_HaaHaaInit \uni062E.medi_AynHaaInit 
	\uni062D.medi_AynHaaInit \uni0681.medi_AynHaaInit \uni0687.medi_AynHaaInit 
	\uni0685.medi_AynHaaInit \uni062C.medi_AynHaaInit \uni0682.medi_AynHaaInit 
	\uni0757.medi_AynHaaInit \uni0684.medi_AynHaaInit \uni076F.medi_AynHaaInit 
	\uni076E.medi_AynHaaInit \uni0683.medi_AynHaaInit \uni06BF.medi_AynHaaInit 
	\uni077C.medi_AynHaaInit \uni0758.medi_AynHaaInit \uni0772.medi_AynHaaInit 
	\uni0686.medi_AynHaaInit \uni0645.medi_LamMemInitTatweel 
	\uni0647.init_AboveHaa \uni06C1.init_AboveHaa \uni062E.init_AboveHaa 
	\uni062D.init_AboveHaa \uni0681.init_AboveHaa \uni0687.init_AboveHaa 
	\uni0685.init_AboveHaa \uni062C.init_AboveHaa \uni0682.init_AboveHaa 
	\uni0757.init_AboveHaa \uni0684.init_AboveHaa \uni076F.init_AboveHaa 
	\uni076E.init_AboveHaa \uni0683.init_AboveHaa \uni06BF.init_AboveHaa 
	\uni077C.init_AboveHaa \uni0758.init_AboveHaa \uni0772.init_AboveHaa 
	\uni0686.init_AboveHaa \uni06FC.init_AboveHaa \uni063A.init_AboveHaa 
	\uni075E.init_AboveHaa \uni075D.init_AboveHaa \uni075F.init_AboveHaa 
	\uni06A0.init_AboveHaa \uni0639.init_AboveHaa \uni062E.fina_AboveHaaIsol2 
	\uni062D.fina_AboveHaaIsol2 \uni0681.fina_AboveHaaIsol2 
	\uni0687.fina_AboveHaaIsol2 \uni0685.fina_AboveHaaIsol2 
	\uni062C.fina_AboveHaaIsol2 \uni0682.fina_AboveHaaIsol2 
	\uni0757.fina_AboveHaaIsol2 \uni0684.fina_AboveHaaIsol2 
	\uni076F.fina_AboveHaaIsol2 \uni076E.fina_AboveHaaIsol2 
	\uni0683.fina_AboveHaaIsol2 \uni06BF.fina_AboveHaaIsol2 
	\uni077C.fina_AboveHaaIsol2 \uni0758.fina_AboveHaaIsol2 
	\uni0772.fina_AboveHaaIsol2 \uni0686.fina_AboveHaaIsol2 
	\uni0645.init_AboveHaa \uni063B.init_AboveHaa \uni063C.init_AboveHaa 
	\uni077F.init_AboveHaa \uni0764.init_AboveHaa \uni0643.init_AboveHaa 
	\uni06B0.init_AboveHaa \uni06B3.init_AboveHaa \uni06B2.init_AboveHaa 
	\uni06AB.init_AboveHaa \uni06AC.init_AboveHaa \uni06AD.init_AboveHaa 
	\uni06AE.init_AboveHaa \uni06AF.init_AboveHaa \uni06A9.init_AboveHaa 
	\uni06B4.init_AboveHaa \uni0763.init_AboveHaa \uni0762.init_AboveHaa 
	\uni06B1.init_AboveHaa \uni063B.init_KafLam \uni063C.init_KafLam 
	\uni077F.init_KafLam \uni0764.init_KafLam \uni0643.init_KafLam 
	\uni06B0.init_KafLam \uni06B3.init_KafLam \uni06B2.init_KafLam 
	\uni06AB.init_KafLam \uni06AC.init_KafLam \uni06AD.init_KafLam 
	\uni06AE.init_KafLam \uni06AF.init_KafLam \uni06A9.init_KafLam 
	\uni06B4.init_KafLam \uni0763.init_KafLam \uni0762.init_KafLam 
	\uni06B1.init_KafLam \uni063B.fina_KafKafFina \uni063C.fina_KafKafFina 
	\uni077F.fina_KafKafFina \uni0764.fina_KafKafFina \uni0643.fina_KafKafFina 
	\uni06B0.fina_KafKafFina \uni06B3.fina_KafKafFina \uni06B2.fina_KafKafFina 
	\uni06AB.fina_KafKafFina \uni06AC.fina_KafKafFina \uni06AD.fina_KafKafFina 
	\uni06AE.fina_KafKafFina \uni06AF.fina_KafKafFina \uni06A9.fina_KafKafFina 
	\uni06B4.fina_KafKafFina \uni0763.fina_KafKafFina \uni0762.fina_KafKafFina 
	\uni06B1.fina_KafKafFina \uni06B5.medi_KafLam \uni06B7.medi_KafLam 
	\uni0644.medi_KafLam \uni06B8.medi_KafLam \uni06B6.medi_KafLam 
	\uni076A.medi_KafLam \uni06B5.medi_KafLamMemMedi 
	\uni06B7.medi_KafLamMemMedi \uni0644.medi_KafLamMemMedi 
	\uni06B8.medi_KafLamMemMedi \uni06B6.medi_KafLamMemMedi 
	\uni076A.medi_KafLamMemMedi \uni063B.medi_KafLam \uni063C.medi_KafLam 
	\uni077F.medi_KafLam \uni0764.medi_KafLam \uni0643.medi_KafLam 
	\uni06B0.medi_KafLam \uni06B3.medi_KafLam \uni06B2.medi_KafLam 
	\uni06AB.medi_KafLam \uni06AC.medi_KafLam \uni06AD.medi_KafLam 
	\uni06AE.medi_KafLam \uni06AF.medi_KafLam \uni06A9.medi_KafLam 
	\uni06B4.medi_KafLam \uni0763.medi_KafLam \uni0762.medi_KafLam 
	\uni06B1.medi_KafLam \uni06B5.medi_KafLamHehIsol 
	\uni06B7.medi_KafLamHehIsol \uni0644.medi_KafLamHehIsol 
	\uni06B8.medi_KafLamHehIsol \uni06B6.medi_KafLamHehIsol 
	\uni076A.medi_KafLamHehIsol \uni06B5.medi_KafLamYaa 
	\uni06B7.medi_KafLamYaa \uni0644.medi_KafLamYaa \uni06B8.medi_KafLamYaa 
	\uni06B6.medi_KafLamYaa \uni076A.medi_KafLamYaa \uni06B5.medi_KafLamAlf 
	\uni06B7.medi_KafLamAlf \uni0644.medi_KafLamAlf \uni06B8.medi_KafLamAlf 
	\uni06B6.medi_KafLamAlf \uni076A.medi_KafLamAlf \uni06B5.fina_KafLam 
	\uni06B7.fina_KafLam \uni0644.fina_KafLam \uni06B8.fina_KafLam 
	\uni06B6.fina_KafLam \uni076A.fina_KafLam \uni0625.fina_KafAlf 
	\uni0627.fina_KafAlf \uni0774.fina_KafAlf \uni0773.fina_KafAlf 
	\uni0623.fina_KafAlf \uni0622.fina_KafAlf \uni0675.fina_KafAlf 
	\uni0672.fina_KafAlf \uni0673.fina_KafAlf \uni0671.fina_KafAlf 
	\uni063B.init_KafMemAlf \uni063C.init_KafMemAlf \uni077F.init_KafMemAlf 
	\uni0764.init_KafMemAlf \uni0643.init_KafMemAlf \uni06B0.init_KafMemAlf 
	\uni06B3.init_KafMemAlf \uni06B2.init_KafMemAlf \uni06AB.init_KafMemAlf 
	\uni06AC.init_KafMemAlf \uni06AD.init_KafMemAlf \uni06AE.init_KafMemAlf 
	\uni06AF.init_KafMemAlf \uni06A9.init_KafMemAlf \uni06B4.init_KafMemAlf 
	\uni0763.init_KafMemAlf \uni0762.init_KafMemAlf \uni06B1.init_KafMemAlf 
	\uni063B.medi_KafMemAlf \uni063C.medi_KafMemAlf \uni077F.medi_KafMemAlf 
	\uni0764.medi_KafMemAlf \uni0643.medi_KafMemAlf \uni06B0.medi_KafMemAlf 
	\uni06B3.medi_KafMemAlf \uni06B2.medi_KafMemAlf \uni06AB.medi_KafMemAlf 
	\uni06AC.medi_KafMemAlf \uni06AD.medi_KafMemAlf \uni06AE.medi_KafMemAlf 
	\uni06AF.medi_KafMemAlf \uni06A9.medi_KafMemAlf \uni06B4.medi_KafMemAlf 
	\uni0763.medi_KafMemAlf \uni0762.medi_KafMemAlf \uni06B1.medi_KafMemAlf 
	\uni0645.medi_KafMemAlf \uni06B5.medi_KafMemLam \uni06B7.medi_KafMemLam 
	\uni0644.medi_KafMemLam \uni06B8.medi_KafMemLam \uni06B6.medi_KafMemLam 
	\uni076A.medi_KafMemLam \uni06B5.fina_KafMemLam \uni06B7.fina_KafMemLam 
	\uni0644.fina_KafMemLam \uni06B8.fina_KafMemLam \uni06B6.fina_KafMemLam 
	\uni076A.fina_KafMemLam \uni0625.fina_KafMemAlf \uni0627.fina_KafMemAlf 
	\uni0774.fina_KafMemAlf \uni0773.fina_KafMemAlf \uni0623.fina_KafMemAlf 
	\uni0622.fina_KafMemAlf \uni0675.fina_KafMemAlf \uni0672.fina_KafMemAlf 
	\uni0673.fina_KafMemAlf \uni0671.fina_KafMemAlf \uni063B.init_KafHeh 
	\uni063C.init_KafHeh \uni077F.init_KafHeh \uni0764.init_KafHeh 
	\uni0643.init_KafHeh \uni06B0.init_KafHeh \uni06B3.init_KafHeh 
	\uni06B2.init_KafHeh \uni06AB.init_KafHeh \uni06AC.init_KafHeh 
	\uni06AD.init_KafHeh \uni06AE.init_KafHeh \uni06AF.init_KafHeh 
	\uni06A9.init_KafHeh \uni06B4.init_KafHeh \uni0763.init_KafHeh 
	\uni0762.init_KafHeh \uni06B1.init_KafHeh \uni063B.medi_KafHeh 
	\uni063C.medi_KafHeh \uni077F.medi_KafHeh \uni0764.medi_KafHeh 
	\uni0643.medi_KafHeh \uni06B0.medi_KafHeh \uni06B3.medi_KafHeh 
	\uni06B2.medi_KafHeh \uni06AB.medi_KafHeh \uni06AC.medi_KafHeh 
	\uni06AD.medi_KafHeh \uni06AE.medi_KafHeh \uni06AF.medi_KafHeh 
	\uni06A9.medi_KafHeh \uni06B4.medi_KafHeh \uni0763.medi_KafHeh 
	\uni0762.medi_KafHeh \uni06B1.medi_KafHeh \uni0647.fina_KafHeh 
	\uni06C1.fina_KafHeh \uni06C3.fina_KafHeh \uni06D5.fina_KafHeh 
	\uni0629.fina_KafHeh \uni0690.fina_KafDal \uni06EE.fina_KafDal 
	\uni0689.fina_KafDal \uni0688.fina_KafDal \uni075A.fina_KafDal 
	\uni0630.fina_KafDal \uni062F.fina_KafDal \uni0759.fina_KafDal 
	\uni068C.fina_KafDal \uni068B.fina_KafDal \uni068A.fina_KafDal 
	\uni068F.fina_KafDal \uni068E.fina_KafDal \uni068D.fina_KafDal 
	\uni06B5.init_LamHeh \uni06B7.init_LamHeh \uni0644.init_LamHeh 
	\uni06B8.init_LamHeh \uni06B6.init_LamHeh \uni076A.init_LamHeh 
	\uni06B5.medi_LamHeh \uni06B7.medi_LamHeh \uni0644.medi_LamHeh 
	\uni06B8.medi_LamHeh \uni06B6.medi_LamHeh \uni076A.medi_LamHeh 
	\uni0647.fina_LamHeh \uni06C1.fina_LamHeh \uni06C3.fina_LamHeh 
	\uni06D5.fina_LamHeh \uni0629.fina_LamHeh \uni0690.fina_LamDal 
	\uni06EE.fina_LamDal \uni0689.fina_LamDal \uni0688.fina_LamDal 
	\uni075A.fina_LamDal \uni0630.fina_LamDal \uni062F.fina_LamDal 
	\uni0759.fina_LamDal \uni068C.fina_LamDal \uni068B.fina_LamDal 
	\uni068A.fina_LamDal \uni068F.fina_LamDal \uni068E.fina_LamDal 
	\uni068D.fina_LamDal \uni063B.medi_KafMemMedi \uni063C.medi_KafMemMedi 
	\uni077F.medi_KafMemMedi \uni0764.medi_KafMemMedi \uni0643.medi_KafMemMedi 
	\uni06B0.medi_KafMemMedi \uni06B3.medi_KafMemMedi \uni06B2.medi_KafMemMedi 
	\uni06AB.medi_KafMemMedi \uni06AC.medi_KafMemMedi \uni06AD.medi_KafMemMedi 
	\uni06AE.medi_KafMemMedi \uni06AF.medi_KafMemMedi \uni06A9.medi_KafMemMedi 
	\uni06B4.medi_KafMemMedi \uni0763.medi_KafMemMedi \uni0762.medi_KafMemMedi 
	\uni06B1.medi_KafMemMedi \uni063B.init_KafMemInit \uni063C.init_KafMemInit 
	\uni077F.init_KafMemInit \uni0764.init_KafMemInit \uni0643.init_KafMemInit 
	\uni06B0.init_KafMemInit \uni06B3.init_KafMemInit \uni06B2.init_KafMemInit 
	\uni06AB.init_KafMemInit \uni06AC.init_KafMemInit \uni06AD.init_KafMemInit 
	\uni06AE.init_KafMemInit \uni06AF.init_KafMemInit \uni06A9.init_KafMemInit 
	\uni06B4.init_KafMemInit \uni0763.init_KafMemInit \uni0762.init_KafMemInit 
	\uni06B1.init_KafMemInit \uni06FC.init_AynMemInit \uni063A.init_AynMemInit 
	\uni075E.init_AynMemInit \uni075D.init_AynMemInit \uni075F.init_AynMemInit 
	\uni06A0.init_AynMemInit \uni0639.init_AynMemInit \uni066F.init_FaaMemInit 
	\uni0761.init_FaaMemInit \uni0760.init_FaaMemInit \uni0642.init_FaaMemInit 
	\uni0641.init_FaaMemInit \uni06A8.init_FaaMemInit \uni06A1.init_FaaMemInit 
	\uni06A2.init_FaaMemInit \uni06A3.init_FaaMemInit \uni06A4.init_FaaMemInit 
	\uni06A5.init_FaaMemInit \uni06A6.init_FaaMemInit \uni06A7.init_FaaMemInit 
	\uni062E.init_HaaMemInit \uni062D.init_HaaMemInit \uni0681.init_HaaMemInit 
	\uni0687.init_HaaMemInit \uni0685.init_HaaMemInit \uni062C.init_HaaMemInit 
	\uni0682.init_HaaMemInit \uni0757.init_HaaMemInit \uni0684.init_HaaMemInit 
	\uni076F.init_HaaMemInit \uni076E.init_HaaMemInit \uni0683.init_HaaMemInit 
	\uni06BF.init_HaaMemInit \uni077C.init_HaaMemInit \uni0758.init_HaaMemInit 
	\uni0772.init_HaaMemInit \uni0686.init_HaaMemInit \uni0647.init_HehMemInit 
	\uni06C1.init_HehMemInit \uni0645.medi_KafMemMedi \uni06FA.init_SenMemInit 
	\uni076D.init_SenMemInit \uni0633.init_SenMemInit \uni077E.init_SenMemInit 
	\uni077D.init_SenMemInit \uni0634.init_SenMemInit \uni0770.init_SenMemInit 
	\uni075C.init_SenMemInit \uni069A.init_SenMemInit \uni069B.init_SenMemInit 
	\uni069C.init_SenMemInit \uni069D.init_SadMemInit \uni06FB.init_SadMemInit 
	\uni0636.init_SadMemInit \uni069E.init_SadMemInit \uni0635.init_SadMemInit 
	\uni0645.init_MemMemInit \uni0645.medi_SenMemInit \uni063B.init_KafYaaIsol 
	\uni063C.init_KafYaaIsol \uni077F.init_KafYaaIsol \uni0764.init_KafYaaIsol 
	\uni0643.init_KafYaaIsol \uni06B0.init_KafYaaIsol \uni06B3.init_KafYaaIsol 
	\uni06B2.init_KafYaaIsol \uni06AB.init_KafYaaIsol \uni06AC.init_KafYaaIsol 
	\uni06AD.init_KafYaaIsol \uni06AE.init_KafYaaIsol \uni06AF.init_KafYaaIsol 
	\uni06A9.init_KafYaaIsol \uni06B4.init_KafYaaIsol \uni0763.init_KafYaaIsol 
	\uni0762.init_KafYaaIsol \uni06B1.init_KafYaaIsol \uni0777.init_BaaYaaIsol 
	\uni0680.init_BaaYaaIsol \uni0776.init_BaaYaaIsol \uni06BC.init_BaaYaaIsol 
	\uni0750.init_BaaYaaIsol \uni0756.init_BaaYaaIsol \uni0768.init_BaaYaaIsol 
	\uni06CE.init_BaaYaaIsol \uni0775.init_BaaYaaIsol \uni06BD.init_BaaYaaIsol 
	\uni0626.init_BaaYaaIsol \uni066E.init_BaaYaaIsol \uni0620.init_BaaYaaIsol 
	\uni064A.init_BaaYaaIsol \uni06BB.init_BaaYaaIsol \uni067F.init_BaaYaaIsol 
	\uni0755.init_BaaYaaIsol \uni067D.init_BaaYaaIsol \uni067E.init_BaaYaaIsol 
	\uni067B.init_BaaYaaIsol \uni0628.init_BaaYaaIsol \uni067A.init_BaaYaaIsol 
	\uni0751.init_BaaYaaIsol \uni0646.init_BaaYaaIsol \uni0753.init_BaaYaaIsol 
	\uni0752.init_BaaYaaIsol \uni062A.init_BaaYaaIsol \uni0678.init_BaaYaaIsol 
	\uni063D.init_BaaYaaIsol \uni062B.init_BaaYaaIsol \uni0679.init_BaaYaaIsol 
	\uni06B9.init_BaaYaaIsol \uni0769.init_BaaYaaIsol \uni0649.init_BaaYaaIsol 
	\uni067C.init_BaaYaaIsol \uni0754.init_BaaYaaIsol \uni06D1.init_BaaYaaIsol 
	\uni06D0.init_BaaYaaIsol \uni06BA.init_BaaYaaIsol \uni06CC.init_BaaYaaIsol 
	\uni0767.init_BaaYaaIsol \uni062E.init_HaaYaaIsol \uni062D.init_HaaYaaIsol 
	\uni0681.init_HaaYaaIsol \uni0687.init_HaaYaaIsol \uni0685.init_HaaYaaIsol 
	\uni062C.init_HaaYaaIsol \uni0682.init_HaaYaaIsol \uni0757.init_HaaYaaIsol 
	\uni0684.init_HaaYaaIsol \uni076F.init_HaaYaaIsol \uni076E.init_HaaYaaIsol 
	\uni0683.init_HaaYaaIsol \uni06BF.init_HaaYaaIsol \uni077C.init_HaaYaaIsol 
	\uni0758.init_HaaYaaIsol \uni0772.init_HaaYaaIsol \uni0686.init_HaaYaaIsol 
	\uni0765.init_MemYaaIsol \uni0645.init_MemYaaIsol \uni0766.init_MemYaaIsol 
	\uni066F.init_FaaYaaIsol \uni0761.init_FaaYaaIsol \uni0760.init_FaaYaaIsol 
	\uni0642.init_FaaYaaIsol \uni0641.init_FaaYaaIsol \uni06A8.init_FaaYaaIsol 
	\uni06A1.init_FaaYaaIsol \uni06A2.init_FaaYaaIsol \uni06A3.init_FaaYaaIsol 
	\uni06A4.init_FaaYaaIsol \uni06A5.init_FaaYaaIsol \uni06A6.init_FaaYaaIsol 
	\uni06A7.init_FaaYaaIsol \uni06FC.init_AynYaaIsol \uni063A.init_AynYaaIsol 
	\uni075E.init_AynYaaIsol \uni075D.init_AynYaaIsol \uni075F.init_AynYaaIsol 
	\uni06A0.init_AynYaaIsol \uni0639.init_AynYaaIsol \uni06B5.init_LamYaaIsol 
	\uni06B7.init_LamYaaIsol \uni0644.init_LamYaaIsol \uni06B8.init_LamYaaIsol 
	\uni06B6.init_LamYaaIsol \uni076A.init_LamYaaIsol \uni0647.init_HehYaaIsol 
	\uni06C1.init_HehYaaIsol \uni0777.fina_KafYaaIsol \uni06D1.fina_KafYaaIsol 
	\uni0775.fina_KafYaaIsol \uni063F.fina_KafYaaIsol \uni0678.fina_KafYaaIsol 
	\uni063D.fina_KafYaaIsol \uni063E.fina_KafYaaIsol \uni06D0.fina_KafYaaIsol 
	\uni0649.fina_KafYaaIsol \uni0776.fina_KafYaaIsol \uni06CD.fina_KafYaaIsol 
	\uni06CC.fina_KafYaaIsol \uni0626.fina_KafYaaIsol \uni0620.fina_KafYaaIsol 
	\uni064A.fina_KafYaaIsol \uni06CE.fina_KafYaaIsol \uni063B.init_KafMemIsol 
	\uni063C.init_KafMemIsol \uni077F.init_KafMemIsol \uni0764.init_KafMemIsol 
	\uni0643.init_KafMemIsol \uni06B0.init_KafMemIsol \uni06B3.init_KafMemIsol 
	\uni06B2.init_KafMemIsol \uni06AB.init_KafMemIsol \uni06AC.init_KafMemIsol 
	\uni06AD.init_KafMemIsol \uni06AE.init_KafMemIsol \uni06AF.init_KafMemIsol 
	\uni06A9.init_KafMemIsol \uni06B4.init_KafMemIsol \uni0763.init_KafMemIsol 
	\uni0762.init_KafMemIsol \uni06B1.init_KafMemIsol \uni06B5.init_LamMemIsol 
	\uni06B7.init_LamMemIsol \uni0644.init_LamMemIsol \uni06B8.init_LamMemIsol 
	\uni06B6.init_LamMemIsol \uni076A.init_LamMemIsol \uni0777.init_BaaMemIsol 
	\uni0680.init_BaaMemIsol \uni0776.init_BaaMemIsol \uni06BC.init_BaaMemIsol 
	\uni0750.init_BaaMemIsol \uni0756.init_BaaMemIsol \uni0768.init_BaaMemIsol 
	\uni06CE.init_BaaMemIsol \uni0775.init_BaaMemIsol \uni06BD.init_BaaMemIsol 
	\uni0626.init_BaaMemIsol \uni066E.init_BaaMemIsol \uni0620.init_BaaMemIsol 
	\uni064A.init_BaaMemIsol \uni06BB.init_BaaMemIsol \uni067F.init_BaaMemIsol 
	\uni0755.init_BaaMemIsol \uni067D.init_BaaMemIsol \uni067E.init_BaaMemIsol 
	\uni067B.init_BaaMemIsol \uni0628.init_BaaMemIsol \uni067A.init_BaaMemIsol 
	\uni0751.init_BaaMemIsol \uni0646.init_BaaMemIsol \uni0753.init_BaaMemIsol 
	\uni0752.init_BaaMemIsol \uni062A.init_BaaMemIsol \uni0678.init_BaaMemIsol 
	\uni063D.init_BaaMemIsol \uni062B.init_BaaMemIsol \uni0679.init_BaaMemIsol 
	\uni06B9.init_BaaMemIsol \uni0769.init_BaaMemIsol \uni0649.init_BaaMemIsol 
	\uni067C.init_BaaMemIsol \uni0754.init_BaaMemIsol \uni06D1.init_BaaMemIsol 
	\uni06D0.init_BaaMemIsol \uni06BA.init_BaaMemIsol \uni06CC.init_BaaMemIsol 
	\uni0767.init_BaaMemIsol \uni0645.fina_KafMemIsol \uni0645.medi_MemAlfFina 
	\uni0777.medi_BaaMemAlfFina \uni0680.medi_BaaMemAlfFina 
	\uni0776.medi_BaaMemAlfFina \uni06BC.medi_BaaMemAlfFina 
	\uni0750.medi_BaaMemAlfFina \uni0756.medi_BaaMemAlfFina 
	\uni0768.medi_BaaMemAlfFina \uni06CE.medi_BaaMemAlfFina 
	\uni0775.medi_BaaMemAlfFina \uni06BD.medi_BaaMemAlfFina 
	\uni0626.medi_BaaMemAlfFina \uni066E.medi_BaaMemAlfFina 
	\uni0620.medi_BaaMemAlfFina \uni064A.medi_BaaMemAlfFina 
	\uni06BB.medi_BaaMemAlfFina \uni067F.medi_BaaMemAlfFina 
	\uni0755.medi_BaaMemAlfFina \uni067D.medi_BaaMemAlfFina 
	\uni067E.medi_BaaMemAlfFina \uni067B.medi_BaaMemAlfFina 
	\uni0628.medi_BaaMemAlfFina \uni067A.medi_BaaMemAlfFina 
	\uni0751.medi_BaaMemAlfFina \uni0646.medi_BaaMemAlfFina 
	\uni0753.medi_BaaMemAlfFina \uni0752.medi_BaaMemAlfFina 
	\uni062A.medi_BaaMemAlfFina \uni0678.medi_BaaMemAlfFina 
	\uni063D.medi_BaaMemAlfFina \uni062B.medi_BaaMemAlfFina 
	\uni0679.medi_BaaMemAlfFina \uni06B9.medi_BaaMemAlfFina 
	\uni0769.medi_BaaMemAlfFina \uni0649.medi_BaaMemAlfFina 
	\uni067C.medi_BaaMemAlfFina \uni0754.medi_BaaMemAlfFina 
	\uni06D1.medi_BaaMemAlfFina \uni06D0.medi_BaaMemAlfFina 
	\uni06BA.medi_BaaMemAlfFina \uni06CC.medi_BaaMemAlfFina 
	\uni0767.medi_BaaMemAlfFina \uni0645.medi_BaaMemAlfFina 
	\uni0645.medi_AlfPostTooth \uni0625.fina_MemAlfFina 
	\uni0627.fina_MemAlfFina \uni0774.fina_MemAlfFina \uni0773.fina_MemAlfFina 
	\uni0623.fina_MemAlfFina \uni0622.fina_MemAlfFina \uni0675.fina_MemAlfFina 
	\uni0672.fina_MemAlfFina \uni0673.fina_MemAlfFina \uni0671.fina_MemAlfFina 
	\uni0777.init_BaaHehInit \uni0680.init_BaaHehInit \uni0776.init_BaaHehInit 
	\uni06BC.init_BaaHehInit \uni0750.init_BaaHehInit \uni0756.init_BaaHehInit 
	\uni0768.init_BaaHehInit \uni06CE.init_BaaHehInit \uni0775.init_BaaHehInit 
	\uni06BD.init_BaaHehInit \uni0626.init_BaaHehInit \uni066E.init_BaaHehInit 
	\uni0620.init_BaaHehInit \uni064A.init_BaaHehInit \uni06BB.init_BaaHehInit 
	\uni067F.init_BaaHehInit \uni0755.init_BaaHehInit \uni067D.init_BaaHehInit 
	\uni067E.init_BaaHehInit \uni067B.init_BaaHehInit \uni0628.init_BaaHehInit 
	\uni067A.init_BaaHehInit \uni0751.init_BaaHehInit \uni0646.init_BaaHehInit 
	\uni0753.init_BaaHehInit \uni0752.init_BaaHehInit \uni062A.init_BaaHehInit 
	\uni0678.init_BaaHehInit \uni063D.init_BaaHehInit \uni062B.init_BaaHehInit 
	\uni0679.init_BaaHehInit \uni06B9.init_BaaHehInit \uni0769.init_BaaHehInit 
	\uni0649.init_BaaHehInit \uni067C.init_BaaHehInit \uni0754.init_BaaHehInit 
	\uni06D1.init_BaaHehInit \uni06D0.init_BaaHehInit \uni06BA.init_BaaHehInit 
	\uni06CC.init_BaaHehInit \uni0767.init_BaaHehInit \uni0777.medi_BaaHehMedi 
	\uni0680.medi_BaaHehMedi \uni0776.medi_BaaHehMedi \uni06BC.medi_BaaHehMedi 
	\uni0750.medi_BaaHehMedi \uni0756.medi_BaaHehMedi \uni0768.medi_BaaHehMedi 
	\uni06CE.medi_BaaHehMedi \uni0775.medi_BaaHehMedi \uni06BD.medi_BaaHehMedi 
	\uni0626.medi_BaaHehMedi \uni066E.medi_BaaHehMedi \uni0620.medi_BaaHehMedi 
	\uni064A.medi_BaaHehMedi \uni06BB.medi_BaaHehMedi \uni067F.medi_BaaHehMedi 
	\uni0755.medi_BaaHehMedi \uni067D.medi_BaaHehMedi \uni067E.medi_BaaHehMedi 
	\uni067B.medi_BaaHehMedi \uni0628.medi_BaaHehMedi \uni067A.medi_BaaHehMedi 
	\uni0751.medi_BaaHehMedi \uni0646.medi_BaaHehMedi \uni0753.medi_BaaHehMedi 
	\uni0752.medi_BaaHehMedi \uni062A.medi_BaaHehMedi \uni0678.medi_BaaHehMedi 
	\uni063D.medi_BaaHehMedi \uni062B.medi_BaaHehMedi \uni0679.medi_BaaHehMedi 
	\uni06B9.medi_BaaHehMedi \uni0769.medi_BaaHehMedi \uni0649.medi_BaaHehMedi 
	\uni067C.medi_BaaHehMedi \uni0754.medi_BaaHehMedi \uni06D1.medi_BaaHehMedi 
	\uni06D0.medi_BaaHehMedi \uni06BA.medi_BaaHehMedi \uni06CC.medi_BaaHehMedi 
	\uni0767.medi_BaaHehMedi \uni0647.medi_BaaHehMedi \uni06C1.medi_BaaHehMedi 
	\uni0647.medi_PostTooth \uni06C1.medi_PostTooth 
	\uni06B5.medi_KafLamMemFina \uni06B7.medi_KafLamMemFina 
	\uni0644.medi_KafLamMemFina \uni06B8.medi_KafLamMemFina 
	\uni06B6.medi_KafLamMemFina \uni076A.medi_KafLamMemFina 
	\uni06B5.init_LamLamInit \uni06B7.init_LamLamInit \uni0644.init_LamLamInit 
	\uni06B8.init_LamLamInit \uni06B6.init_LamLamInit \uni076A.init_LamLamInit 
	\uni06B5.medi_LamLamInit \uni06B7.medi_LamLamInit \uni0644.medi_LamLamInit 
	\uni06B8.medi_LamLamInit \uni06B6.medi_LamLamInit \uni076A.medi_LamLamInit 
	\uni06B5.medi_LamLamAlfIsol \uni06B7.medi_LamLamAlfIsol 
	\uni0644.medi_LamLamAlfIsol \uni06B8.medi_LamLamAlfIsol 
	\uni06B6.medi_LamLamAlfIsol \uni076A.medi_LamLamAlfIsol 
	\uni063B.fina_LamKafIsol \uni063C.fina_LamKafIsol \uni077F.fina_LamKafIsol 
	\uni0764.fina_LamKafIsol \uni0643.fina_LamKafIsol \uni06B0.fina_LamKafIsol 
	\uni06B3.fina_LamKafIsol \uni06B2.fina_LamKafIsol \uni06AB.fina_LamKafIsol 
	\uni06AC.fina_LamKafIsol \uni06AD.fina_LamKafIsol \uni06AE.fina_LamKafIsol 
	\uni06AF.fina_LamKafIsol \uni06A9.fina_LamKafIsol \uni06B4.fina_LamKafIsol 
	\uni0763.fina_LamKafIsol \uni0762.fina_LamKafIsol \uni06B1.fina_LamKafIsol 
	\uni06B5.fina_LamLamIsol \uni06B7.fina_LamLamIsol \uni0644.fina_LamLamIsol 
	\uni06B8.fina_LamLamIsol \uni06B6.fina_LamLamIsol \uni076A.fina_LamLamIsol 
	\uni06B5.medi_LamLamMedi \uni06B7.medi_LamLamMedi \uni0644.medi_LamLamMedi 
	\uni06B8.medi_LamLamMedi \uni06B6.medi_LamLamMedi \uni076A.medi_LamLamMedi 
	\uni06B5.medi_LamLamAlefFina \uni06B7.medi_LamLamAlefFina 
	\uni0644.medi_LamLamAlefFina \uni06B8.medi_LamLamAlefFina 
	\uni06B6.medi_LamLamAlefFina \uni076A.medi_LamLamAlefFina 
	\uni06B5.medi_LamLamMedi2 \uni06B7.medi_LamLamMedi2 
	\uni0644.medi_LamLamMedi2 \uni06B8.medi_LamLamMedi2 
	\uni06B6.medi_LamLamMedi2 \uni076A.medi_LamLamMedi2 
	\uni063B.fina_LamKafFina \uni063C.fina_LamKafFina \uni077F.fina_LamKafFina 
	\uni0764.fina_LamKafFina \uni0643.fina_LamKafFina \uni06B0.fina_LamKafFina 
	\uni06B3.fina_LamKafFina \uni06B2.fina_LamKafFina \uni06AB.fina_LamKafFina 
	\uni06AC.fina_LamKafFina \uni06AD.fina_LamKafFina \uni06AE.fina_LamKafFina 
	\uni06AF.fina_LamKafFina \uni06A9.fina_LamKafFina \uni06B4.fina_LamKafFina 
	\uni0763.fina_LamKafFina \uni0762.fina_LamKafFina \uni06B1.fina_LamKafFina 
	\uni06B5.fina_LamLamFina \uni06B7.fina_LamLamFina \uni0644.fina_LamLamFina 
	\uni06B8.fina_LamLamFina \uni06B6.fina_LamLamFina \uni076A.fina_LamLamFina 
	\uni06B5.medi_LamLamMemInit \uni06B7.medi_LamLamMemInit 
	\uni0644.medi_LamLamMemInit \uni06B8.medi_LamLamMemInit 
	\uni06B6.medi_LamLamMemInit \uni076A.medi_LamLamMemInit 
	\uni06B5.medi_LamLamHehIsol \uni06B7.medi_LamLamHehIsol 
	\uni0644.medi_LamLamHehIsol \uni06B8.medi_LamLamHehIsol 
	\uni06B6.medi_LamLamHehIsol \uni076A.medi_LamLamHehIsol 
	\uni06B5.medi_LamLamYaaIsol \uni06B7.medi_LamLamYaaIsol 
	\uni0644.medi_LamLamYaaIsol \uni06B8.medi_LamLamYaaIsol 
	\uni06B6.medi_LamLamYaaIsol \uni076A.medi_LamLamYaaIsol 
	\uni06B5.medi_LamLamMemMedi \uni06B7.medi_LamLamMemMedi 
	\uni0644.medi_LamLamMemMedi \uni06B8.medi_LamLamMemMedi 
	\uni06B6.medi_LamLamMemMedi \uni076A.medi_LamLamMemMedi 
	\uni06B5.medi_LamLamHehFina \uni06B7.medi_LamLamHehFina 
	\uni0644.medi_LamLamHehFina \uni06B8.medi_LamLamHehFina 
	\uni06B6.medi_LamLamHehFina \uni076A.medi_LamLamHehFina 
	\uni06B5.medi_LamLamYaaFina \uni06B7.medi_LamLamYaaFina 
	\uni0644.medi_LamLamYaaFina \uni06B8.medi_LamLamYaaFina 
	\uni06B6.medi_LamLamYaaFina \uni076A.medi_LamLamYaaFina 
	\uni062E.medi_1LamHaaHaaInit \uni062D.medi_1LamHaaHaaInit 
	\uni0681.medi_1LamHaaHaaInit \uni0687.medi_1LamHaaHaaInit 
	\uni0685.medi_1LamHaaHaaInit \uni062C.medi_1LamHaaHaaInit 
	\uni0682.medi_1LamHaaHaaInit \uni0757.medi_1LamHaaHaaInit 
	\uni0684.medi_1LamHaaHaaInit \uni076F.medi_1LamHaaHaaInit 
	\uni076E.medi_1LamHaaHaaInit \uni0683.medi_1LamHaaHaaInit 
	\uni06BF.medi_1LamHaaHaaInit \uni077C.medi_1LamHaaHaaInit 
	\uni0758.medi_1LamHaaHaaInit \uni0772.medi_1LamHaaHaaInit 
	\uni0686.medi_1LamHaaHaaInit \uni062E.medi_2LamHaaHaaInit 
	\uni062D.medi_2LamHaaHaaInit \uni0681.medi_2LamHaaHaaInit 
	\uni0687.medi_2LamHaaHaaInit \uni0685.medi_2LamHaaHaaInit 
	\uni062C.medi_2LamHaaHaaInit \uni0682.medi_2LamHaaHaaInit 
	\uni0757.medi_2LamHaaHaaInit \uni0684.medi_2LamHaaHaaInit 
	\uni076F.medi_2LamHaaHaaInit \uni076E.medi_2LamHaaHaaInit 
	\uni0683.medi_2LamHaaHaaInit \uni06BF.medi_2LamHaaHaaInit 
	\uni077C.medi_2LamHaaHaaInit \uni0758.medi_2LamHaaHaaInit 
	\uni0772.medi_2LamHaaHaaInit \uni0686.medi_2LamHaaHaaInit \uni06AA.init 
	\uni06AA.medi \uni06AA.fina \uni06AA \uni0625.LowHamza \uni0673.LowHamza 
	\uni0680.init_LD \uni06BD.init_LD \uni067E.init_LD \uni067B.init_LD 
	\uni0628.init_LD \uni0767.init_LD \uni063D.init_LD \uni0777.init_LD 
	\uni0776.init_LD \uni0775.init_LD \uni06CC.init_LD \uni064A.init_LD 
	\uni06CE.init_LD \uni0751.init_LD \uni0750.init_LD \uni0753.init_LD 
	\uni0752.init_LD \uni0755.init_LD \uni0754.init_LD \uni06B9.init_LD 
	\uni06D1.init_LD \uni06D0.init_LD \uni0680.init_BaaRaaIsolLD 
	\uni06BD.init_BaaRaaIsolLD \uni067E.init_BaaRaaIsolLD 
	\uni067B.init_BaaRaaIsolLD \uni0628.init_BaaRaaIsolLD 
	\uni0767.init_BaaRaaIsolLD \uni063D.init_BaaRaaIsolLD 
	\uni0777.init_BaaRaaIsolLD \uni0776.init_BaaRaaIsolLD 
	\uni0775.init_BaaRaaIsolLD \uni06CC.init_BaaRaaIsolLD 
	\uni064A.init_BaaRaaIsolLD \uni06CE.init_BaaRaaIsolLD 
	\uni0751.init_BaaRaaIsolLD \uni0750.init_BaaRaaIsolLD 
	\uni0753.init_BaaRaaIsolLD \uni0752.init_BaaRaaIsolLD 
	\uni0755.init_BaaRaaIsolLD \uni0754.init_BaaRaaIsolLD 
	\uni06B9.init_BaaRaaIsolLD \uni06D1.init_BaaRaaIsolLD 
	\uni06D0.init_BaaRaaIsolLD \uni0680.init_BaaDalLD \uni06BD.init_BaaDalLD 
	\uni067E.init_BaaDalLD \uni067B.init_BaaDalLD \uni0628.init_BaaDalLD 
	\uni0767.init_BaaDalLD \uni063D.init_BaaDalLD \uni0777.init_BaaDalLD 
	\uni0776.init_BaaDalLD \uni0775.init_BaaDalLD \uni06CC.init_BaaDalLD 
	\uni064A.init_BaaDalLD \uni06CE.init_BaaDalLD \uni0751.init_BaaDalLD 
	\uni0750.init_BaaDalLD \uni0753.init_BaaDalLD \uni0752.init_BaaDalLD 
	\uni0755.init_BaaDalLD \uni0754.init_BaaDalLD \uni06B9.init_BaaDalLD 
	\uni06D1.init_BaaDalLD \uni06D0.init_BaaDalLD 
	\uni0680.init_BaaMemHaaInitLD \uni06BD.init_BaaMemHaaInitLD 
	\uni067E.init_BaaMemHaaInitLD \uni067B.init_BaaMemHaaInitLD 
	\uni0628.init_BaaMemHaaInitLD \uni0767.init_BaaMemHaaInitLD 
	\uni063D.init_BaaMemHaaInitLD \uni0777.init_BaaMemHaaInitLD 
	\uni0776.init_BaaMemHaaInitLD \uni0775.init_BaaMemHaaInitLD 
	\uni06CC.init_BaaMemHaaInitLD \uni064A.init_BaaMemHaaInitLD 
	\uni06CE.init_BaaMemHaaInitLD \uni0751.init_BaaMemHaaInitLD 
	\uni0750.init_BaaMemHaaInitLD \uni0753.init_BaaMemHaaInitLD 
	\uni0752.init_BaaMemHaaInitLD \uni0755.init_BaaMemHaaInitLD 
	\uni0754.init_BaaMemHaaInitLD \uni06B9.init_BaaMemHaaInitLD 
	\uni06D1.init_BaaMemHaaInitLD \uni06D0.init_BaaMemHaaInitLD 
	\uni0680.init_BaaBaaYaaLD \uni06BD.init_BaaBaaYaaLD 
	\uni067E.init_BaaBaaYaaLD \uni067B.init_BaaBaaYaaLD 
	\uni0628.init_BaaBaaYaaLD \uni0767.init_BaaBaaYaaLD 
	\uni063D.init_BaaBaaYaaLD \uni0777.init_BaaBaaYaaLD 
	\uni0776.init_BaaBaaYaaLD \uni0775.init_BaaBaaYaaLD 
	\uni06CC.init_BaaBaaYaaLD \uni064A.init_BaaBaaYaaLD 
	\uni06CE.init_BaaBaaYaaLD \uni0751.init_BaaBaaYaaLD 
	\uni0750.init_BaaBaaYaaLD \uni0753.init_BaaBaaYaaLD 
	\uni0752.init_BaaBaaYaaLD \uni0755.init_BaaBaaYaaLD 
	\uni0754.init_BaaBaaYaaLD \uni06B9.init_BaaBaaYaaLD 
	\uni06D1.init_BaaBaaYaaLD \uni06D0.init_BaaBaaYaaLD 
	\uni0680.init_BaaNonIsolLD \uni06BD.init_BaaNonIsolLD 
	\uni067E.init_BaaNonIsolLD \uni067B.init_BaaNonIsolLD 
	\uni0628.init_BaaNonIsolLD \uni0767.init_BaaNonIsolLD 
	\uni063D.init_BaaNonIsolLD \uni0777.init_BaaNonIsolLD 
	\uni0776.init_BaaNonIsolLD \uni0775.init_BaaNonIsolLD 
	\uni06CC.init_BaaNonIsolLD \uni064A.init_BaaNonIsolLD 
	\uni06CE.init_BaaNonIsolLD \uni0751.init_BaaNonIsolLD 
	\uni0750.init_BaaNonIsolLD \uni0753.init_BaaNonIsolLD 
	\uni0752.init_BaaNonIsolLD \uni0755.init_BaaNonIsolLD 
	\uni0754.init_BaaNonIsolLD \uni06B9.init_BaaNonIsolLD 
	\uni06D1.init_BaaNonIsolLD \uni06D0.init_BaaNonIsolLD 
	\uni0680.init_BaaSenInitLD \uni06BD.init_BaaSenInitLD 
	\uni067E.init_BaaSenInitLD \uni067B.init_BaaSenInitLD 
	\uni0628.init_BaaSenInitLD \uni0767.init_BaaSenInitLD 
	\uni063D.init_BaaSenInitLD \uni0777.init_BaaSenInitLD 
	\uni0776.init_BaaSenInitLD \uni0775.init_BaaSenInitLD 
	\uni06CC.init_BaaSenInitLD \uni064A.init_BaaSenInitLD 
	\uni06CE.init_BaaSenInitLD \uni0751.init_BaaSenInitLD 
	\uni0750.init_BaaSenInitLD \uni0753.init_BaaSenInitLD 
	\uni0752.init_BaaSenInitLD \uni0755.init_BaaSenInitLD 
	\uni0754.init_BaaSenInitLD \uni06B9.init_BaaSenInitLD 
	\uni06D1.init_BaaSenInitLD \uni06D0.init_BaaSenInitLD 
	\uni0680.init_BaaMemInitLD \uni06BD.init_BaaMemInitLD 
	\uni067E.init_BaaMemInitLD \uni067B.init_BaaMemInitLD 
	\uni0628.init_BaaMemInitLD \uni0767.init_BaaMemInitLD 
	\uni063D.init_BaaMemInitLD \uni0777.init_BaaMemInitLD 
	\uni0776.init_BaaMemInitLD \uni0775.init_BaaMemInitLD 
	\uni06CC.init_BaaMemInitLD \uni064A.init_BaaMemInitLD 
	\uni06CE.init_BaaMemInitLD \uni0751.init_BaaMemInitLD 
	\uni0750.init_BaaMemInitLD \uni0753.init_BaaMemInitLD 
	\uni0752.init_BaaMemInitLD \uni0755.init_BaaMemInitLD 
	\uni0754.init_BaaMemInitLD \uni06B9.init_BaaMemInitLD 
	\uni06D1.init_BaaMemInitLD \uni06D0.init_BaaMemInitLD 
	\uni0680.init_BaaBaaHaaInitLD \uni06BD.init_BaaBaaHaaInitLD 
	\uni067E.init_BaaBaaHaaInitLD \uni067B.init_BaaBaaHaaInitLD 
	\uni0628.init_BaaBaaHaaInitLD \uni0767.init_BaaBaaHaaInitLD 
	\uni063D.init_BaaBaaHaaInitLD \uni0777.init_BaaBaaHaaInitLD 
	\uni0776.init_BaaBaaHaaInitLD \uni0775.init_BaaBaaHaaInitLD 
	\uni06CC.init_BaaBaaHaaInitLD \uni064A.init_BaaBaaHaaInitLD 
	\uni06CE.init_BaaBaaHaaInitLD \uni0751.init_BaaBaaHaaInitLD 
	\uni0750.init_BaaBaaHaaInitLD \uni0753.init_BaaBaaHaaInitLD 
	\uni0752.init_BaaBaaHaaInitLD \uni0755.init_BaaBaaHaaInitLD 
	\uni0754.init_BaaBaaHaaInitLD \uni06B9.init_BaaBaaHaaInitLD 
	\uni06D1.init_BaaBaaHaaInitLD \uni06D0.init_BaaBaaHaaInitLD 
	\uni0680.init_BaaBaaIsolLD \uni06BD.init_BaaBaaIsolLD 
	\uni067E.init_BaaBaaIsolLD \uni067B.init_BaaBaaIsolLD 
	\uni0628.init_BaaBaaIsolLD \uni0767.init_BaaBaaIsolLD 
	\uni063D.init_BaaBaaIsolLD \uni0777.init_BaaBaaIsolLD 
	\uni0776.init_BaaBaaIsolLD \uni0775.init_BaaBaaIsolLD 
	\uni06CC.init_BaaBaaIsolLD \uni064A.init_BaaBaaIsolLD 
	\uni06CE.init_BaaBaaIsolLD \uni0751.init_BaaBaaIsolLD 
	\uni0750.init_BaaBaaIsolLD \uni0753.init_BaaBaaIsolLD 
	\uni0752.init_BaaBaaIsolLD \uni0755.init_BaaBaaIsolLD 
	\uni0754.init_BaaBaaIsolLD \uni06B9.init_BaaBaaIsolLD 
	\uni06D1.init_BaaBaaIsolLD \uni06D0.init_BaaBaaIsolLD 
	\uni0680.init_BaaBaaMemInitLD \uni06BD.init_BaaBaaMemInitLD 
	\uni067E.init_BaaBaaMemInitLD \uni067B.init_BaaBaaMemInitLD 
	\uni0628.init_BaaBaaMemInitLD \uni0767.init_BaaBaaMemInitLD 
	\uni063D.init_BaaBaaMemInitLD \uni0777.init_BaaBaaMemInitLD 
	\uni0776.init_BaaBaaMemInitLD \uni0775.init_BaaBaaMemInitLD 
	\uni06CC.init_BaaBaaMemInitLD \uni064A.init_BaaBaaMemInitLD 
	\uni06CE.init_BaaBaaMemInitLD \uni0751.init_BaaBaaMemInitLD 
	\uni0750.init_BaaBaaMemInitLD \uni0753.init_BaaBaaMemInitLD 
	\uni0752.init_BaaBaaMemInitLD \uni0755.init_BaaBaaMemInitLD 
	\uni0754.init_BaaBaaMemInitLD \uni06B9.init_BaaBaaMemInitLD 
	\uni06D1.init_BaaBaaMemInitLD \uni06D0.init_BaaBaaMemInitLD 
	\uni0680.init_BaaSenAltInitLD \uni06BD.init_BaaSenAltInitLD 
	\uni067E.init_BaaSenAltInitLD \uni067B.init_BaaSenAltInitLD 
	\uni0628.init_BaaSenAltInitLD \uni0767.init_BaaSenAltInitLD 
	\uni063D.init_BaaSenAltInitLD \uni0777.init_BaaSenAltInitLD 
	\uni0776.init_BaaSenAltInitLD \uni0775.init_BaaSenAltInitLD 
	\uni06CC.init_BaaSenAltInitLD \uni064A.init_BaaSenAltInitLD 
	\uni06CE.init_BaaSenAltInitLD \uni0751.init_BaaSenAltInitLD 
	\uni0750.init_BaaSenAltInitLD \uni0753.init_BaaSenAltInitLD 
	\uni0752.init_BaaSenAltInitLD \uni0755.init_BaaSenAltInitLD 
	\uni0754.init_BaaSenAltInitLD \uni06B9.init_BaaSenAltInitLD 
	\uni06D1.init_BaaSenAltInitLD \uni06D0.init_BaaSenAltInitLD 
	\uni0680.init_BaaHaaInitLD \uni06BD.init_BaaHaaInitLD 
	\uni067E.init_BaaHaaInitLD \uni067B.init_BaaHaaInitLD 
	\uni0628.init_BaaHaaInitLD \uni0767.init_BaaHaaInitLD 
	\uni063D.init_BaaHaaInitLD \uni0777.init_BaaHaaInitLD 
	\uni0776.init_BaaHaaInitLD \uni0775.init_BaaHaaInitLD 
	\uni06CC.init_BaaHaaInitLD \uni064A.init_BaaHaaInitLD 
	\uni06CE.init_BaaHaaInitLD \uni0751.init_BaaHaaInitLD 
	\uni0750.init_BaaHaaInitLD \uni0753.init_BaaHaaInitLD 
	\uni0752.init_BaaHaaInitLD \uni0755.init_BaaHaaInitLD 
	\uni0754.init_BaaHaaInitLD \uni06B9.init_BaaHaaInitLD 
	\uni06D1.init_BaaHaaInitLD \uni06D0.init_BaaHaaInitLD 
	\uni0680.init_BaaHaaMemInitLD \uni06BD.init_BaaHaaMemInitLD 
	\uni067E.init_BaaHaaMemInitLD \uni067B.init_BaaHaaMemInitLD 
	\uni0628.init_BaaHaaMemInitLD \uni0767.init_BaaHaaMemInitLD 
	\uni063D.init_BaaHaaMemInitLD \uni0777.init_BaaHaaMemInitLD 
	\uni0776.init_BaaHaaMemInitLD \uni0775.init_BaaHaaMemInitLD 
	\uni06CC.init_BaaHaaMemInitLD \uni064A.init_BaaHaaMemInitLD 
	\uni06CE.init_BaaHaaMemInitLD \uni0751.init_BaaHaaMemInitLD 
	\uni0750.init_BaaHaaMemInitLD \uni0753.init_BaaHaaMemInitLD 
	\uni0752.init_BaaHaaMemInitLD \uni0755.init_BaaHaaMemInitLD 
	\uni0754.init_BaaHaaMemInitLD \uni06B9.init_BaaHaaMemInitLD 
	\uni06D1.init_BaaHaaMemInitLD \uni06D0.init_BaaHaaMemInitLD 
	\uni0680.init_HighLD \uni06BD.init_HighLD \uni067E.init_HighLD 
	\uni067B.init_HighLD \uni0628.init_HighLD \uni0767.init_HighLD 
	\uni063D.init_HighLD \uni0777.init_HighLD \uni0776.init_HighLD 
	\uni0775.init_HighLD \uni06CC.init_HighLD \uni064A.init_HighLD 
	\uni06CE.init_HighLD \uni0751.init_HighLD \uni0750.init_HighLD 
	\uni0753.init_HighLD \uni0752.init_HighLD \uni0755.init_HighLD 
	\uni0754.init_HighLD \uni06B9.init_HighLD \uni06D1.init_HighLD 
	\uni06D0.init_HighLD \uni0680.init_WideLD \uni06BD.init_WideLD 
	\uni067E.init_WideLD \uni067B.init_WideLD \uni0628.init_WideLD 
	\uni0767.init_WideLD \uni063D.init_WideLD \uni0777.init_WideLD 
	\uni0776.init_WideLD \uni0775.init_WideLD \uni06CC.init_WideLD 
	\uni064A.init_WideLD \uni06CE.init_WideLD \uni0751.init_WideLD 
	\uni0750.init_WideLD \uni0753.init_WideLD \uni0752.init_WideLD 
	\uni0755.init_WideLD \uni0754.init_WideLD \uni06B9.init_WideLD 
	\uni06D1.init_WideLD \uni06D0.init_WideLD \uni0680.init_BaaYaaIsolLD 
	\uni06BD.init_BaaYaaIsolLD \uni067E.init_BaaYaaIsolLD 
	\uni067B.init_BaaYaaIsolLD \uni0628.init_BaaYaaIsolLD 
	\uni0767.init_BaaYaaIsolLD \uni063D.init_BaaYaaIsolLD 
	\uni0777.init_BaaYaaIsolLD \uni0776.init_BaaYaaIsolLD 
	\uni0775.init_BaaYaaIsolLD \uni06CC.init_BaaYaaIsolLD 
	\uni064A.init_BaaYaaIsolLD \uni06CE.init_BaaYaaIsolLD 
	\uni0751.init_BaaYaaIsolLD \uni0750.init_BaaYaaIsolLD 
	\uni0753.init_BaaYaaIsolLD \uni0752.init_BaaYaaIsolLD 
	\uni0755.init_BaaYaaIsolLD \uni0754.init_BaaYaaIsolLD 
	\uni06B9.init_BaaYaaIsolLD \uni06D1.init_BaaYaaIsolLD 
	\uni06D0.init_BaaYaaIsolLD \uni0680.init_BaaMemIsolLD 
	\uni06BD.init_BaaMemIsolLD \uni067E.init_BaaMemIsolLD 
	\uni067B.init_BaaMemIsolLD \uni0628.init_BaaMemIsolLD 
	\uni0767.init_BaaMemIsolLD \uni063D.init_BaaMemIsolLD 
	\uni0777.init_BaaMemIsolLD \uni0776.init_BaaMemIsolLD 
	\uni0775.init_BaaMemIsolLD \uni06CC.init_BaaMemIsolLD 
	\uni064A.init_BaaMemIsolLD \uni06CE.init_BaaMemIsolLD 
	\uni0751.init_BaaMemIsolLD \uni0750.init_BaaMemIsolLD 
	\uni0753.init_BaaMemIsolLD \uni0752.init_BaaMemIsolLD 
	\uni0755.init_BaaMemIsolLD \uni0754.init_BaaMemIsolLD 
	\uni06B9.init_BaaMemIsolLD \uni06D1.init_BaaMemIsolLD 
	\uni06D0.init_BaaMemIsolLD \uni0680.init_BaaHehInitLD 
	\uni06BD.init_BaaHehInitLD \uni067E.init_BaaHehInitLD 
	\uni067B.init_BaaHehInitLD \uni0628.init_BaaHehInitLD 
	\uni0767.init_BaaHehInitLD \uni063D.init_BaaHehInitLD 
	\uni0777.init_BaaHehInitLD \uni0776.init_BaaHehInitLD 
	\uni0775.init_BaaHehInitLD \uni06CC.init_BaaHehInitLD 
	\uni064A.init_BaaHehInitLD \uni06CE.init_BaaHehInitLD 
	\uni0751.init_BaaHehInitLD \uni0750.init_BaaHehInitLD 
	\uni0753.init_BaaHehInitLD \uni0752.init_BaaHehInitLD 
	\uni0755.init_BaaHehInitLD \uni0754.init_BaaHehInitLD 
	\uni06B9.init_BaaHehInitLD \uni06D1.init_BaaHehInitLD 
	\uni06D0.init_BaaHehInitLD \uni0620.initLD \uni0620.init_BaaRaaIsolLD 
	\uni0620.init_BaaDalLD \uni0620.init_BaaMemHaaInitLD 
	\uni0620.init_BaaBaaYaaLD \uni0620.init_BaaNonIsolLD 
	\uni0620.init_BaaSenInitLD \uni0620.init_BaaMemInitLD 
	\uni0620.init_BaaBaaHaaInitLD \uni0620.init_BaaBaaIsolLD 
	\uni0620.init_BaaBaaMemInitLD \uni0620.init_BaaSenAltInitLD 
	\uni0620.init_BaaHaaInitLD \uni0620.init_BaaHaaMemInitLD 
	\uni0620.init_HighLD \uni0620.init_WideLD \uni0620.init_BaaYaaIsolLD 
	\uni0620.init_BaaMemIsolLD \uni0620.init_BaaHehInitLD 
	\uni0765.init_MemHehInit \aMem.init_MemHehInit \uni0645.init_MemHehInit 
	\uni0766.init_MemHehInit \uni0670.isol \uni0670.medi \uni0621.medi 
	\uni0621.float \uni0640.long1 \uni06DD \uni0660.small \uni0661.small 
	\uni0662.small \uni0663.small \uni0664.small \uni0665.small \uni0666.small 
	\uni0667.small \uni0668.small \uni0669.small \uni06F0.small \uni06F1.small 
	\uni06F2.small \uni06F3.small \uni06F4.small \uni06F5.small \uni06F6.small 
	\uni06F7.small \uni06F8.small \uni06F9.small \uni06F4.urd.small 
	\uni06F6.urd.small \uni06F7.urd.small \uni06DE \uni06E5 \uni06E6 
	\uni06E5.medi \uni06E6.medi \uni0606 \uni0607 \radical.rtlm \dot.percent 
	\uni06E9 \uni060F \uni0608 \uni060B \uni06D4 \uni25CC 
	\uni0645.medi_KafMemMediTatweel \uni0645.fina_LamMemFinaExtended 
	\uni0645.fina_KafMemFinaExtended \aMem.fina_Extended 
	\uni0645.fina_KafMemIsolExtended \uni0640.1 \uni0627.fina_Tatweel 
	\uni0640.2 \uni0640.3 \uni0640.4 \uni06FA.init_SenBaaMemInit 
	\uni076D.init_SenBaaMemInit \uni0633.init_SenBaaMemInit 
	\uni077E.init_SenBaaMemInit \uni077D.init_SenBaaMemInit 
	\uni0634.init_SenBaaMemInit \uni0770.init_SenBaaMemInit 
	\uni075C.init_SenBaaMemInit \uni069A.init_SenBaaMemInit 
	\uni069B.init_SenBaaMemInit \uni069C.init_SenBaaMemInit 
	\uni069D.init_SenBaaMemInit \uni06FB.init_SenBaaMemInit 
	\uni0636.init_SenBaaMemInit \uni069E.init_SenBaaMemInit 
	\uni0635.init_SenBaaMemInit \aYaaBarree.isol \aYaaBarree.fina 
	\aYaaBarree.fina_PostTooth \aSen.init_YaaBarree \aSad.init_YaaBarree 
	\uni077B.fina \uni077B.fina_PostTooth \uni077A.fina \uni077A.fina_PostTooth 
	\uni06D2.fina \uni06D2.fina_PostTooth \uni06FA.init_YaaBarree 
	\uni076D.init_YaaBarree \uni0633.init_YaaBarree \uni077E.init_YaaBarree 
	\uni077D.init_YaaBarree \uni0634.init_YaaBarree \uni0770.init_YaaBarree 
	\uni075C.init_YaaBarree \uni069A.init_YaaBarree \uni069B.init_YaaBarree 
	\uni069C.init_YaaBarree \uni069D.init_YaaBarree \uni06FB.init_YaaBarree 
	\uni0636.init_YaaBarree \uni069E.init_YaaBarree \uni0635.init_YaaBarree 
	\aYaaBarree.fina_PostAscender \uni0627.fina_Wide \aBaa.init_YaaBarree 
	\aFaa.init_YaaBarree \uni06E5.low \aLam.init_YaaBarree \aKaf.init_YaaBarree 
	\uni063B.init_YaaBarree \uni063C.init_YaaBarree \uni077F.init_YaaBarree 
	\uni0764.init_YaaBarree \uni0643.init_YaaBarree \uni06B0.init_YaaBarree 
	\uni06B3.init_YaaBarree \uni06B2.init_YaaBarree \uni06AB.init_YaaBarree 
	\uni06AC.init_YaaBarree \uni06AD.init_YaaBarree \uni06AE.init_YaaBarree 
	\uni06AF.init_YaaBarree \uni06A9.init_YaaBarree \uni06B4.init_YaaBarree 
	\uni0763.init_YaaBarree \uni0762.init_YaaBarree \uni06B1.init_YaaBarree 
	\uni0777.init_YaaBarree \uni0680.init_YaaBarree \uni0776.init_YaaBarree 
	\uni06BC.init_YaaBarree \uni0750.init_YaaBarree \uni0756.init_YaaBarree 
	\uni0768.init_YaaBarree \uni06CE.init_YaaBarree \uni0775.init_YaaBarree 
	\uni06BD.init_YaaBarree \uni0626.init_YaaBarree \uni066E.init_YaaBarree 
	\uni0620.init_YaaBarree \uni064A.init_YaaBarree \uni06BB.init_YaaBarree 
	\uni067F.init_YaaBarree \uni0755.init_YaaBarree \uni067D.init_YaaBarree 
	\uni067E.init_YaaBarree \uni067B.init_YaaBarree \uni0628.init_YaaBarree 
	\uni067A.init_YaaBarree \uni0751.init_YaaBarree \uni0646.init_YaaBarree 
	\uni0753.init_YaaBarree \uni0752.init_YaaBarree \uni062A.init_YaaBarree 
	\uni0678.init_YaaBarree \uni063D.init_YaaBarree \uni062B.init_YaaBarree 
	\uni0679.init_YaaBarree \uni06B9.init_YaaBarree \uni0769.init_YaaBarree 
	\uni0649.init_YaaBarree \uni067C.init_YaaBarree \uni0754.init_YaaBarree 
	\uni06D1.init_YaaBarree \uni06D0.init_YaaBarree \uni06BA.init_YaaBarree 
	\uni06CC.init_YaaBarree \uni0767.init_YaaBarree \uni077B.fina_PostAscender 
	\uni077A.fina_PostAscender \uni06D2.fina_PostAscender 
	\uni06B5.init_YaaBarree \uni06B7.init_YaaBarree \uni0644.init_YaaBarree 
	\uni06B8.init_YaaBarree \uni06B6.init_YaaBarree \uni076A.init_YaaBarree 
	\uni066F.init_YaaBarree \uni0761.init_YaaBarree \uni0760.init_YaaBarree 
	\uni0642.init_YaaBarree \uni0641.init_YaaBarree \uni06A8.init_YaaBarree 
	\uni06A1.init_YaaBarree \uni06A2.init_YaaBarree \uni06A3.init_YaaBarree 
	\uni06A4.init_YaaBarree \uni06A5.init_YaaBarree \uni06A6.init_YaaBarree 
	\uni06A7.init_YaaBarree \aYaaBarree.fina_PostAyn \aHaa.init_YaaBarree 
	\aAyn.init_YaaBarree \aMem.init_YaaBarree \uni077B.fina_PostAyn 
	\uni077A.fina_PostAyn \uni06D2.fina_PostAyn \uni0765.init_YaaBarree 
	\uni0645.init_YaaBarree \uni0766.init_YaaBarree \uni062E.init_YaaBarree 
	\uni062D.init_YaaBarree \uni0681.init_YaaBarree \uni0687.init_YaaBarree 
	\uni0685.init_YaaBarree \uni062C.init_YaaBarree \uni0682.init_YaaBarree 
	\uni0757.init_YaaBarree \uni0684.init_YaaBarree \uni076F.init_YaaBarree 
	\uni076E.init_YaaBarree \uni0683.init_YaaBarree \uni06BF.init_YaaBarree 
	\uni077C.init_YaaBarree \uni0758.init_YaaBarree \uni0772.init_YaaBarree 
	\uni0686.init_YaaBarree \uni06FC.init_YaaBarree \uni063A.init_YaaBarree 
	\uni075E.init_YaaBarree \uni075D.init_YaaBarree \uni075F.init_YaaBarree 
	\uni06A0.init_YaaBarree \uni0639.init_YaaBarree \aHeh.init_YaaBarree 
	\uni0647.init_YaaBarree \uni06C1.init_YaaBarree \aTaa.init_YaaBaree 
	\uni0638.init_YaaBarree \uni0637.init_YaaBarree \uni069F.init_YaaBarree 
	\aHehKnotted.isol \uni06BE.init \uni06FF.init \uni06BE.fina \uni06FF.fina 
	\uni06BE.medi \uni06FF.medi \aHehKnotted.fina \uni060D \uni060E \uni0602 
	\uni0603 \uni0600 \uni0601 \aHeh.medi_HehYaaFina \uni0647.medi_HehYaaFina 
	\uni06C1.medi_HehYaaFina \uni0647.medi_PostToothHehYaa 
	\uni06C1.medi_PostToothHehYaa \uniFD3E \uniFD3F \uniFB50 \uniFB51 \uniFB52 
	\uniFB53 \uniFB54 \uniFB55 \uniFB56 \uniFB57 \uniFB58 \uniFB59 \uniFB5A \uniFB5B 
	\uniFB5C \uniFB5D \uniFB5E \uniFB5F \uniFB60 \uniFB61 \uniFB62 \uniFB63 \uniFB64 
	\uniFB65 \uniFB66 \uniFB67 \uniFB68 \uniFB69 \uniFB6A \uniFB6B \uniFB6C \uniFB6D 
	\uniFB6E \uniFB6F \uniFB70 \uniFB71 \uniFB72 \uniFB73 \uniFB74 \uniFB75 \uniFB76 
	\uniFB77 \uniFB78 \uniFB79 \uniFB7A \uniFB7B \uniFB7C \uniFB7D \uniFB7E \uniFB7F 
	\uniFB80 \uniFB81 \uniFB82 \uniFB83 \uniFB84 \uniFB85 \uniFB86 \uniFB87 \uniFB88 
	\uniFB89 \uniFB8A \uniFB8B \uniFB8C \uniFB8D \uniFB8E \uniFB8F \uniFB90 \uniFB91 
	\uniFB92 \uniFB93 \uniFB94 \uniFB95 \uniFB96 \uniFB97 \uniFB98 \uniFB99 \uniFB9A 
	\uniFB9B \uniFB9C \uniFB9D \uniFB9E \uniFB9F \uniFBA0 \uniFBA1 \uniFBA2 \uniFBA3 
	\uniFBA4 \uniFBA5 \uniFBA6 \uniFBA7 \uniFBA8 \uniFBA9 \uniFBAA \uniFBAB \uniFBAC 
	\uniFBAD \uniFBAE \uniFBAF \uniFBB0 \uniFBB1 \uniFBD3 \uniFBD4 \uniFBD5 \uniFBD6 
	\uniFBD7 \uniFBD8 \uniFBD9 \uniFBDA \uniFBDB \uniFBDC \uniFBDD \uniFBDE \uniFBDF 
	\uniFBE0 \uniFBE1 \uniFBE2 \uniFBE3 \uniFBE4 \uniFBE5 \uniFBE6 \uniFBE7 \uniFBE8 
	\uniFBE9 \uniFBEA \uniFBEB \uniFBEC \uniFBED \uniFBEE \uniFBEF \uniFBF0 \uniFBF1 
	\uniFBF2 \uniFBF3 \uniFBF4 \uniFBF5 \uniFBF6 \uniFBF7 \uniFBF8 \uniFBF9 \uniFBFA 
	\uniFBFB \uniFBFC \uniFBFD \uniFBFE \uniFBFF \uniFC00 \uniFC01 \uniFC02 \uniFC03 
	\uniFC04 \uniFC05 \uniFC06 \uniFC07 \uniFC08 \uniFC09 \uniFC0A \uniFC0B \uniFC0C 
	\uniFC0D \uniFC0E \uniFC0F \uniFC10 \uniFC11 \uniFC12 \uniFC13 \uniFC14 \uniFC15 
	\uniFC16 \uniFC17 \uniFC18 \uniFC19 \uniFC1A \uniFC1B \uniFC1C \uniFC1D \uniFC1E 
	\uniFC1F \uniFC20 \uniFC21 \uniFC22 \uniFC23 \uniFC24 \uniFC25 \uniFC26 \uniFC27 
	\uniFC28 \uniFC29 \uniFC2A \uniFC2B \uniFC2C \uniFC2D \uniFC2E \uniFC2F \uniFC30 
	\uniFC31 \uniFC32 \uniFC33 \uniFC34 \uniFC35 \uniFC36 \uniFC37 \uniFC38 \uniFC39 
	\uniFC3A \uniFC3B \uniFC3C \uniFC3D \uniFC3E \uniFC3F \uniFC40 \uniFC41 \uniFC42 
	\uniFC43 \uniFC44 \uniFC45 \uniFC46 \uniFC47 \uniFC48 \uniFC49 \uniFC4A \uniFC4B 
	\uniFC4C \uniFC4D \uniFC4E \uniFC4F \uniFC50 \uniFC51 \uniFC52 \uniFC53 \uniFC54 
	\uniFC55 \uniFC56 \uniFC57 \uniFC58 \uniFC59 \uniFC5A \uniFC5B \uniFC5C \uniFC5D 
	\uniFC5E \uniFC5F \uniFC60 \uniFC61 \uniFC62 \uniFC63 \uniFC64 \uniFC65 \uniFC66 
	\uniFC67 \uniFC68 \uniFC69 \uniFC6A \uniFC6B \uniFC6C \uniFC6D \uniFC6E \uniFC6F 
	\uniFC70 \uniFC71 \uniFC72 \uniFC73 \uniFC74 \uniFC75 \uniFC76 \uniFC77 \uniFC78 
	\uniFC79 \uniFC7A \uniFC7B \uniFC7C \uniFC7D \uniFC7E \uniFC7F \uniFC80 \uniFC81 
	\uniFC82 \uniFC83 \uniFC84 \uniFC85 \uniFC86 \uniFC87 \uniFC88 \uniFC89 \uniFC8A 
	\uniFC8B \uniFC8C \uniFC8D \uniFC8E \uniFC8F \uniFC90 \uniFC91 \uniFC92 \uniFC93 
	\uniFC94 \uniFC95 \uniFC96 \uniFC97 \uniFC98 \uniFC99 \uniFC9A \uniFC9B \uniFC9C 
	\uniFC9D \uniFC9E \uniFC9F \uniFCA0 \uniFCA1 \uniFCA2 \uniFCA3 \uniFCA4 \uniFCA5 
	\uniFCA6 \uniFCA7 \uniFCA8 \uniFCA9 \uniFCAA \uniFCAB \uniFCAC \uniFCAD \uniFCAE 
	\uniFCAF \uniFCB0 \uniFCB1 \uniFCB2 \uniFCB3 \uniFCB4 \uniFCB5 \uniFCB6 \uniFCB7 
	\uniFCB8 \uniFCB9 \uniFCBA \uniFCBB \uniFCBC \uniFCBD \uniFCBE \uniFCBF \uniFCC0 
	\uniFCC1 \uniFCC2 \uniFCC3 \uniFCC4 \uniFCC5 \uniFCC6 \uniFCC7 \uniFCC8 \uniFCC9 
	\uniFCCA \uniFCCB \uniFCCC \uniFCCD \uniFCCE \uniFCCF \uniFCD0 \uniFCD1 \uniFCD2 
	\uniFCD3 \uniFCD4 \uniFCD5 \uniFCD6 \uniFCD7 \uniFCD8 \uniFCD9 \uniFCDA \uniFCDB 
	\uniFCDC \uniFCDD \uniFCDE \uniFCDF \uniFCE0 \uniFCE1 \uniFCE2 \uniFCE3 \uniFCE4 
	\uniFCE5 \uniFCE6 \uniFCE7 \uniFCE8 \uniFCE9 \uniFCEA \uniFCEB \uniFCEC \uniFCED 
	\uniFCEE \uniFCEF \uniFCF0 \uniFCF1 \uniFCF2 \uniFCF3 \uniFCF4 \uniFCF5 \uniFCF6 
	\uniFCF7 \uniFCF8 \uniFCF9 \uniFCFA \uniFCFB \uniFCFC \uniFCFD \uniFCFE \uniFCFF 
	\uniFD00 \uniFD01 \uniFD02 \uniFD03 \uniFD04 \uniFD05 \uniFD06 \uniFD07 \uniFD08 
	\uniFD09 \uniFD0A \uniFD0B \uniFD0C \uniFD0D \uniFD0E \uniFD0F \uniFD10 \uniFD11 
	\uniFD12 \uniFD13 \uniFD14 \uniFD15 \uniFD16 \uniFD17 \uniFD18 \uniFD19 \uniFD1A 
	\uniFD1B \uniFD1C \uniFD1D \uniFD1E \uniFD1F \uniFD20 \uniFD21 \uniFD22 \uniFD23 
	\uniFD24 \uniFD25 \uniFD26 \uniFD27 \uniFD28 \uniFD29 \uniFD2A \uniFD2B \uniFD2C 
	\uniFD2D \uniFD2E \uniFD2F \uniFD30 \uniFD31 \uniFD32 \uniFD33 \uniFD34 \uniFD35 
	\uniFD36 \uniFD37 \uniFD38 \uniFD39 \uniFD3A \uniFD3B \uniFD3C \uniFD3D \uniFD50 
	\uniFD51 \uniFD52 \uniFD53 \uniFD54 \uniFD55 \uniFD56 \uniFD57 \uniFD58 \uniFD59 
	\uniFD5A \uniFD5B \uniFD5C \uniFD5D \uniFD5E \uniFD5F \uniFD60 \uniFD61 \uniFD62 
	\uniFD63 \uniFD64 \uniFD65 \uniFD66 \uniFD67 \uniFD68 \uniFD69 \uniFD6A \uniFD6B 
	\uniFD6C \uniFD6D \uniFD6E \uniFD6F \uniFD70 \uniFD71 \uniFD72 \uniFD73 \uniFD74 
	\uniFD75 \uniFD76 \uniFD77 \uniFD78 \uniFD79 \uniFD7A \uniFD7B \uniFD7C \uniFD7D 
	\uniFD7E \uniFD7F \uniFD80 \uniFD81 \uniFD82 \uniFD83 \uniFD84 \uniFD85 \uniFD86 
	\uniFD87 \uniFD88 \uniFD89 \uniFD8A \uniFD8B \uniFD8C \uniFD8D \uniFD8E \uniFD8F 
	\uniFD92 \uniFD93 \uniFD94 \uniFD95 \uniFD96 \uniFD97 \uniFD98 \uniFD99 \uniFD9A 
	\uniFD9B \uniFD9C \uniFD9D \uniFD9E \uniFD9F \uniFDA0 \uniFDA1 \uniFDA2 \uniFDA3 
	\uniFDA4 \uniFDA5 \uniFDA6 \uniFDA7 \uniFDA8 \uniFDA9 \uniFDAA \uniFDAB \uniFDAC 
	\uniFDAD \uniFDAE \uniFDAF \uniFDB0 \uniFDB1 \uniFDB2 \uniFDB3 \uniFDB4 \uniFDB5 
	\uniFDB6 \uniFDB7 \uniFDB8 \uniFDB9 \uniFDBA \uniFDBB \uniFDBC \uniFDBD \uniFDBE 
	\uniFDBF \uniFDC0 \uniFDC1 \uniFDC2 \uniFDC3 \uniFDC4 \uniFDC5 \uniFDC6 \uniFDC7 
	\uniFDF0 \uniFDF1 \uniFDF2 \uniFDF3 \uniFDF4 \uniFDF5 \uniFDF6 \uniFDF7 \uniFDF8 
	\uniFDF9 \uniFDFC \uniFE70 \uniFE71 \uniFE72 \uniFE74 \uniFE76 \uniFE77 \uniFE78 
	\uniFE79 \uniFE7A \uniFE7B \uniFE7C \uniFE7D \uniFE7E \uniFE7F \uniFE80 \uniFE81 
	\uniFE82 \uniFE83 \uniFE84 \uniFE85 \uniFE86 \uniFE87 \uniFE88 \uniFE89 \uniFE8A 
	\uniFE8B \uniFE8C \uniFE8D \uniFE8E \uniFE8F \uniFE90 \uniFE91 \uniFE92 \uniFE93 
	\uniFE94 \uniFE95 \uniFE96 \uniFE97 \uniFE98 \uniFE99 \uniFE9A \uniFE9B \uniFE9C 
	\uniFE9D \uniFE9E \uniFE9F \uniFEA0 \uniFEA1 \uniFEA2 \uniFEA3 \uniFEA4 \uniFEA5 
	\uniFEA6 \uniFEA7 \uniFEA8 \uniFEA9 \uniFEAA \uniFEAB \uniFEAC \uniFEAD \uniFEAE 
	\uniFEAF \uniFEB0 \uniFEB1 \uniFEB2 \uniFEB3 \uniFEB4 \uniFEB5 \uniFEB6 \uniFEB7 
	\uniFEB8 \uniFEB9 \uniFEBA \uniFEBB \uniFEBC \uniFEBD \uniFEBE \uniFEBF \uniFEC0 
	\uniFEC1 \uniFEC2 \uniFEC3 \uniFEC4 \uniFEC5 \uniFEC6 \uniFEC7 \uniFEC8 \uniFEC9 
	\uniFECA \uniFECB \uniFECC \uniFECD \uniFECE \uniFECF \uniFED0 \uniFED1 \uniFED2 
	\uniFED3 \uniFED4 \uniFED5 \uniFED6 \uniFED7 \uniFED8 \uniFED9 \uniFEDA \uniFEDB 
	\uniFEDC \uniFEDD \uniFEDE \uniFEDF \uniFEE0 \uniFEE1 \uniFEE2 \uniFEE3 \uniFEE4 
	\uniFEE5 \uniFEE6 \uniFEE7 \uniFEE8 \uniFEE9 \uniFEEA \uniFEEB \uniFEEC \uniFEED 
	\uniFEEE \uniFEEF \uniFEF0 \uniFEF1 \uniFEF2 \uniFEF3 \uniFEF4 \uniFEF5 \uniFEF6 
	\uniFEF7 \uniFEF8 \uniFEF9 \uniFEFA \uniFEFB \uniFEFC \uniFBB2 \uniFBB3 \uniFBB4 
	\uniFBB5 \uniFBB6 \uniFBB7 \uniFBB8 \uniFBB9 \uniFBBD \uniFBBE \uniFBBF \uniFBC0 
	\uniFBC1 \uniFBBA \uniFBBB \uniFBBC \uni0660.medium \uni0661.medium 
	\uni0662.medium \uni0663.medium \uni0664.medium \uni0665.medium 
	\uni0666.medium \uni0667.medium \uni0668.medium \uni0669.medium 
	\uni06F0.medium \uni06F1.medium \uni06F2.medium \uni06F3.medium 
	\uni06F4.medium \uni06F5.medium \uni06F6.medium \uni06F7.medium 
	\uni06F8.medium \uni06F9.medium \uni06F4.urd.medium \uni06F6.urd.medium 
	\uni06F7.urd.medium \uniFDFA \uniFDFD \aAlf.fina_Narrow \uni0622.fina_Narrow 
	\uni0623.fina_Narrow \uni0625.fina_Narrow \uni0627.fina_Narrow 
	\uni0671.fina_Narrow \uni0672.fina_Narrow \uni0673.fina_Narrow 
	\uni0675.fina_Narrow \uni0773.fina_Narrow \uni0774.fina_Narrow \slash 
	\aHehKnotted.init_YaaBarree \uni06BE.init_YaaBarree \uni08A0.fina 
	\uni08A0.init \uni08A0 \uni08A0.medi \uni08A0.init_BaaRaaIsol 
	\uni08A0.medi_BaaMemFina \uni08A0.medi_LamBaaMemInit \uni08A0.init_BaaDal 
	\uni08A0.init_BaaMemHaaInit \uni08A0.init_BaaBaaYaa 
	\uni08A0.medi_BaaBaaYaa \uni08A0.medi_KafBaaInit \uni08A0.medi_BaaBaaInit 
	\uni08A0.init_BaaNonIsol \uni08A0.init_BaaSenInit \uni08A0.medi_BaaRaaFina 
	\uni08A0.init_BaaMemInit \uni08A0.init_BaaBaaHaaInit 
	\uni08A0.medi_BaaBaaHaaInit \uni08A0.medi_SenBaaMemInit 
	\uni08A0.init_BaaBaaIsol \uni08A0.fina_BaaBaaIsol 
	\uni08A0.init_BaaBaaMemInit \uni08A0.medi_BaaBaaMemInit 
	\uni08A0.medi_KafBaaMedi \uni08A0.medi_BaaNonFina \uni08A0.medi_BaaYaaFina 
	\uni08A0.init_BaaSenAltInit \uni08A0.init_AboveHaa 
	\uni08A0.init_BaaHaaInit \uni08A0.init_BaaHaaMemInit \uni08A0.init_High 
	\uni08A0.medi_High \uni08A0.init_Wide \uni08A0.init_BaaYaaIsol 
	\uni08A0.init_BaaMemIsol \uni08A0.medi_BaaMemAlfFina 
	\uni08A0.init_BaaHehInit \uni08A0.medi_BaaHehMedi \uni08A0.init_LD 
	\uni08A0.init_BaaRaaIsolLD \uni08A0.init_BaaDalLD 
	\uni08A0.init_BaaMemHaaInitLD \uni08A0.init_BaaBaaYaaLD 
	\uni08A0.init_BaaNonIsolLD \uni08A0.init_BaaSenInitLD 
	\uni08A0.init_BaaMemInitLD \uni08A0.init_BaaBaaHaaInitLD 
	\uni08A0.init_BaaBaaIsolLD \uni08A0.init_BaaBaaMemInitLD 
	\uni08A0.init_BaaSenAltInitLD \uni08A0.init_BaaHaaInitLD 
	\uni08A0.init_BaaHaaMemInitLD \uni08A0.init_HighLD \uni08A0.init_WideLD 
	\uni08A0.init_BaaYaaIsolLD \uni08A0.init_BaaMemIsolLD 
	\uni08A0.init_BaaHehInitLD \uni08A0.init_YaaBarree \bar \uni0600.alt 
	\uni0660.prop \uni0661.prop \uni0662.prop \uni0663.prop \uni0664.prop 
	\uni0665.prop \uni0666.prop \uni0667.prop \uni0668.prop \uni0669.prop 
	\uni06F0.prop \uni06F1.prop \uni06F2.prop \uni06F3.prop \uni06F4.prop 
	\uni06F5.prop \uni06F6.prop \uni06F7.prop \uni06F8.prop \uni06F9.prop 
	\uni06F4.urd.prop \uni06F6.urd.prop \uni06F7.urd.prop \uni0623.fina_Wide 
	\uni0671.fina_Wide \aBaa.init_BaaBaaHeh \uni0777.init_BaaBaaHeh 
	\uni0680.init_BaaBaaHeh \uni0776.init_BaaBaaHeh \uni06BC.init_BaaBaaHeh 
	\uni0750.init_BaaBaaHeh \uni0756.init_BaaBaaHeh \uni0768.init_BaaBaaHeh 
	\uni06CE.init_BaaBaaHeh \uni0775.init_BaaBaaHeh \uni06BD.init_BaaBaaHeh 
	\uni0626.init_BaaBaaHeh \uni066E.init_BaaBaaHeh \uni0620.init_BaaBaaHeh 
	\uni064A.init_BaaBaaHeh \uni06BB.init_BaaBaaHeh \uni067F.init_BaaBaaHeh 
	\uni0755.init_BaaBaaHeh \uni067D.init_BaaBaaHeh \uni067E.init_BaaBaaHeh 
	\uni067B.init_BaaBaaHeh \uni0628.init_BaaBaaHeh \uni067A.init_BaaBaaHeh 
	\uni0751.init_BaaBaaHeh \uni0646.init_BaaBaaHeh \uni0753.init_BaaBaaHeh 
	\uni0752.init_BaaBaaHeh \uni062A.init_BaaBaaHeh \uni0678.init_BaaBaaHeh 
	\uni063D.init_BaaBaaHeh \uni062B.init_BaaBaaHeh \uni0679.init_BaaBaaHeh 
	\uni06B9.init_BaaBaaHeh \uni0769.init_BaaBaaHeh \uni0649.init_BaaBaaHeh 
	\uni067C.init_BaaBaaHeh \uni0754.init_BaaBaaHeh \uni06D1.init_BaaBaaHeh 
	\uni06D0.init_BaaBaaHeh \uni06BA.init_BaaBaaHeh \uni06CC.init_BaaBaaHeh 
	\uni0767.init_BaaBaaHeh \uni0680.init_BaaBaaHehLD 
	\uni06BD.init_BaaBaaHehLD \uni067E.init_BaaBaaHehLD 
	\uni067B.init_BaaBaaHehLD \uni0628.init_BaaBaaHehLD 
	\uni0767.init_BaaBaaHehLD \uni063D.init_BaaBaaHehLD 
	\uni0777.init_BaaBaaHehLD \uni0776.init_BaaBaaHehLD 
	\uni0775.init_BaaBaaHehLD \uni06CC.init_BaaBaaHehLD 
	\uni064A.init_BaaBaaHehLD \uni06CE.init_BaaBaaHehLD 
	\uni0751.init_BaaBaaHehLD \uni0750.init_BaaBaaHehLD 
	\uni0753.init_BaaBaaHehLD \uni0752.init_BaaBaaHehLD 
	\uni0755.init_BaaBaaHehLD \uni0754.init_BaaBaaHehLD 
	\uni06B9.init_BaaBaaHehLD \uni06D1.init_BaaBaaHehLD 
	\uni06D0.init_BaaBaaHehLD \backslash \brokenbar \exclam \quotedbl \numbersign 
	\dollar \percent \ampersand \quotesingle \asterisk \plus \comma \hyphen \zero 
	\one \two \three \four \five \six \seven \eight \nine \colon \semicolon \less \equal 
	\greater \question \at \A \B \C \D \E \F \G \H \I \J \K \L \M \N \O \P \Q \R \S \T \U \V \W \X \Y \Z 
	\asciicircum \underscore \grave \a \b \c \d \e \f \g \h \i \j \k \l \m \n \o \p \q \r \s \t \u 
	\v \w \x \y \z \asciitilde \exclamdown \cent \sterling \currency \yen \section 
	\dieresis \copyright \ordfeminine \logicalnot \registered \macron \degree 
	\plusminus \uni00B2 \uni00B3 \acute \mu \paragraph \periodcentered \cedilla 
	\uni00B9 \ordmasculine \onequarter \onehalf \threequarters \questiondown \AE 
	\Eth \multiply \Oslash \Thorn \germandbls \ae \eth \divide \oslash \thorn \Dcroat 
	\dcroat \Hbar \hbar \dotlessi \IJ \ij \kgreenlandic \Ldot \ldot \Lslash \lslash 
	\Eng \eng \OE \oe \Tbar \tbar \uni0237 \uni02BB \uni02BC \caron \breve \dotaccent 
	\ogonek \hungarumlaut \uni2010 \uni2011 \figuredash \endash \emdash \uni2015 
	\quoteleft \quoteright \quotesinglbase \quotedblleft \quotedblright 
	\quotedblbase \dagger \daggerdbl \bullet \onedotenleader \ellipsis 
	\perthousand \minute \second \uni2038 \guilsinglleft \guilsinglright \uni203E 
	\Euro \minus \uni2213 \radical \f_f \f_i \f_f_i \f_l \f_f_l \f_b \f_f_b \f_k \f_f_k 
	\f_h \f_f_h \f_j \f_f_j \iogonek.dotless \fraction \zero.prop \one.prop 
	\two.prop \three.prop \four.prop \five.prop \six.prop \seven.prop \eight.prop 
	\nine.prop \longs \period \uni02BE \uni02BF \quotereversed \uni201F \i.TRK 
	\napostrophe \uni2042 \uni00AD \uni0331 \uni032E \zero.small \zero.medium 
	\one.small \one.medium \two.small \two.medium \three.small \three.medium 
	\four.small \four.medium \five.small \five.medium \six.small \six.medium 
	\seven.small \seven.medium \eight.small \eight.medium \nine.small 
	\nine.medium ];
@GDEF_Ligature = [\Agrave \Aacute \Acircumflex \Atilde \Adieresis \Aring \Ccedilla 
	\Egrave \Eacute \Ecircumflex \Edieresis \Igrave \Iacute \Icircumflex \Idieresis 
	\Ntilde \Ograve \Oacute \Ocircumflex \Otilde \Odieresis \Ugrave \Uacute 
	\Ucircumflex \Udieresis \Yacute \agrave \aacute \acircumflex \atilde \adieresis 
	\aring \ccedilla \egrave \eacute \ecircumflex \edieresis \igrave \iacute 
	\icircumflex \idieresis \ntilde \ograve \oacute \ocircumflex \otilde \odieresis 
	\ugrave \uacute \ucircumflex \udieresis \yacute \ydieresis \Amacron \amacron 
	\Abreve \abreve \Aogonek \aogonek \Cacute \cacute \Ccircumflex \ccircumflex 
	\Cdotaccent \cdotaccent \Ccaron \ccaron \Dcaron \dcaron \Emacron \emacron 
	\Ebreve \ebreve \Edotaccent \edotaccent \Eogonek \eogonek \Ecaron \ecaron 
	\Gcircumflex \gcircumflex \Gbreve \gbreve \Gdotaccent \gdotaccent \uni0122 
	\uni0123 \Hcircumflex \hcircumflex \Itilde \itilde \Imacron \imacron \Ibreve 
	\ibreve \Iogonek \iogonek \Idotaccent \Jcircumflex \jcircumflex \uni0136 
	\uni0137 \Lacute \lacute \uni013B \uni013C \Lcaron \lcaron \Nacute \nacute 
	\uni0145 \uni0146 \Ncaron \ncaron \Omacron \omacron \Obreve \obreve 
	\Ohungarumlaut \ohungarumlaut \Racute \racute \uni0156 \uni0157 \Rcaron \rcaron 
	\Sacute \sacute \Scircumflex \scircumflex \Scedilla \scedilla \Scaron \scaron 
	\uni0162 \uni0163 \Tcaron \tcaron \Utilde \utilde \Umacron \umacron \Ubreve 
	\ubreve \Uring \uring \Uhungarumlaut \uhungarumlaut \Uogonek \uogonek 
	\Wcircumflex \wcircumflex \Ycircumflex \ycircumflex \Ydieresis \Zacute \zacute 
	\Zdotaccent \zdotaccent \Zcaron \zcaron \uni1E02 \uni1E03 \uni1E0A \uni1E0B 
	\uni1E1E \uni1E1F \uni1E40 \uni1E41 \uni1E56 \uni1E57 \uni1E60 \uni1E61 \uni1E6A 
	\uni1E6B \uni1E0C \uni1E0D \uni1E0E \uni1E0F \uni1E10 \uni1E11 \uni1E24 \uni1E25 
	\uni1E28 \uni1E29 \uni1E2A \uni1E2B \uni1E62 \uni1E63 \uni1E6C \uni1E6D \uni1E6E 
	\uni1E6F \Wgrave \wgrave \Wacute \wacute \Wdieresis \wdieresis \uni1E92 \uni1E93 
	\uni1E96 \uni1E97 \Ygrave \ygrave \Gcaron \gcaron ];
@GDEF_Mark = [\Dot.a \TwoDots.a \ThreeDots.a \Dot.b \TwoDots.b \ThreeDots.b 
	\uni0654 \uni0655 \uni0653 \hamza.kaf \dash.kaf \iThreeDots.a \iThreeDots.b 
	\uni065A \dash.gaf \smalltaa.above \vTwoDots.a \vTwoDots.b \ring.below 
	\dot.alt1 \dash.gaf.alt2 \dash.gaf.alt1 \uni064B \uni064C \uni064D \uni064E 
	\uni064F \uni0650 \uni0651 \uni0652 \FourDots.a \FourDots.b \hThreeDots.a 
	\hThreeDots.b \uni065B \uni0674 \aYaa.tail \uni065F \hamza.above.wavy 
	\smallv.below \smallv.below.inverted \smalltaa.below \twostrokes.below 
	\uni0670 \uni0656 \hamza.wasl \uni064B.small \uni064E.small \uni08F1.small 
	\uni064F.small \uni0652.small2 \uni0650.small2 \damma.mark \hamzadamma.mark 
	\aAlf.dagger \uni0615 \uni065D \uni061A \uni0619 \uni0618 \Dot.b.l \TwoDots.b.l 
	\ThreeDots.b.l \iThreeDots.b.l \vTwoDots.b.l \FourDots.b.l \hThreeDots.b.l 
	\smallv.below.inverted.low \stroke \aTwo.above \aThree.above \aFour.above 
	\aFour.below \uni06E1 \uni06DF \uni06E0 \uni08F0 \uni08F1 \uni08F2 
	\uni08F0.small \uni064E.small2 \uni064C.small \uni06E4 \uni06E2 \uni06ED 
	\uni06D6 \uni06D7 \uni06E7 \uni06DB \uni06E3 \uni06DC \uni06DA \uni06D9 \uni06E8 
	\uni06EC \uni06D8 \uni06EB \uni06EA \uni0616 \uni0617 \uni0659 \uni065E \uni0657 
	\uni0658 \uni065C \uni0610 \uni0611 \uni0612 \uni0613 \uni0614 \hamza.above 
	\smallv.above \smallv.below.low \smallv.above.inverted \uni030A \uni0325 
	\gravecomb \acutecomb \uni0302 \tildecomb \uni0304 \uni0305 \uni0306 \uni0307 
	\uni0308 \uni030B \uni030C \uni0312 \uni0315 \dotbelowcomb \uni0326 \uni0327 
	\uni0328 \gravecomb.cap \acutecomb.cap \uni0302.cap \uni030A.cap 
	\tildecomb.cap \uni0304.cap \uni0306.cap \uni0327.cap \uni0308.narrow 
	\uni0308.cap \uni030C.cap ];

table GDEF {
  GlyphClassDef @GDEF_Simple, @GDEF_Ligature, @GDEF_Mark, ;

  LigatureCaret \f_f <caret 530>;
  LigatureCaret \f_i <caret 550>;
  LigatureCaret \f_f_i <caret 550> <caret 1100>;
  LigatureCaret \f_l <caret 560>;
  LigatureCaret \f_f_l <caret 560> <caret 1120>;
  LigatureCaret \f_b <caret 680>;
  LigatureCaret \f_f_b <caret 560> <caret 1200>;
  LigatureCaret \f_k <caret 600>;
  LigatureCaret \f_f_k <caret 550> <caret 1160>;
  LigatureCaret \f_h <caret 600>;
  LigatureCaret \f_f_h <caret 550> <caret 1180>;
  LigatureCaret \f_j <caret 570>;
  LigatureCaret \f_f_j <caret 550> <caret 1130>;
} GDEF;




feature locl {
  script latn;
  language TRK exclude_dflt;
  sub i by i.TRK;
} locl;

feature liga {
  sub f f  by f_f;
  sub f i  by f_i;
  sub f f i  by f_f_i;
  sub f l  by f_l;
  sub f f l  by f_f_l;
  sub f b  by f_b;
  sub f f b  by f_f_b;
  sub f k  by f_k;
  sub f f k  by f_f_k;
  sub f h  by f_h;
  sub f f h  by f_f_h;
  sub f j  by f_j;
  sub f f j  by f_f_j;
} liga;
