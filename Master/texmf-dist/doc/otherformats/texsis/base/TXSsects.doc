%% TXSsects.doc - Chapters and Sections - TeXsis version 2.18
%% @(#) $Id: TXSsects.doc,v 18.3 2001/04/06 21:54:45 myers Exp $
%======================================================================*
% (C) Copyright 1989-1998 by Eric Myers and Frank E. Paige
% This file is a part of TeXsis.  Distribution and/or modifications
% are allowed under the terms of the LaTeX Project Public License (LPPL).
% See the file COPYING or ftp://ftp.texsis.org/texsis/LPPL
%======================================================================*
\ifx\undefined\bs \texsis\input TXSdocM.doc\draft\input Manual.aux\fi

\section{Typesetting Complete Papers\label{sect.papers}} 

The commands described so far are useful for creating the body of a
simple physics paper, but they are not enough to typeset a complete
paper.  A physics paper generally has a title, one or more authors and
their addresses or professional affiliations, and perhaps an abstract.
Longer documents may be divided into sections, or even chapters, and
there might even be a table of contents or an index.  This section
describes the commands for creating the basic structural elements of a
complete paper: the title material and divisions into chapters,
sections, and subsections.  It also includes a discussion of more
advanced topics, such as how to create a table of contents or an index,
and how to control the running headlines at the top of the page.  Later,
\Sect{fmts} discusses more specialized document layouts for conference
proceedings or for papers submitted to specific journals, while
\Sect{letr} describes the commands for producing letters, memos, referee
reports, and similar specialized documents.

It is often convenient to be able to typeset the same document in more
than one format.  Therefore, all complete papers in \TeXsis\  use the
same general structure, and one can select the particular document
format to be used by changing only one or two commands at the beginning
of the manuscript file.  The commands to typeset the title page and
to break the paper up into sections are still the same, even if they 
behave slightly differently
according to the particular format selected.  For example, if you give
the \cs{paper} command then the title page material will appear at the
top of the first page, and the text of the paper will also begin on that
page.  If you simply change \cs{paper} to \cs{preprint} the title page
material will appear on a page by itself, with a banner (defined by
\cs{banner}) across the top of the page, and the text will begin on the
next page.  Using any of the other document formats described in
\Sect{fmts} will cause the title page material to be formatted
slightly differently.

At this point an example may be more instructive than more discussion.
Here is how a famous paper\reference{BCS} J.~Bardeen, L.~N.~Cooper, and
J.~R.~Schrieffer,
\journal Phys. Rev.;108,1175(1957) 
\endreference 
would have begun had it been typeset with \TeXsis:
\TeXexample
|smalltt\texsis
\preprint
\pubdate{July 8, 1957}
\pubcode{hep-cats/yymmnnn}
\titlepage
\title
Theory of Superconductivity
\endtitle
\author
J.~Bardeen
Department of Physics
University of Illinois
Urbana, Illinois 61801
\endauthor
\and
\author
L.~N.~Cooper
Department of Physics and Astronomy
The Ohio State University
Columbus, OH 43210
\endauthor
\and
\author
J.~R.~Schrieffer
Department of Theoretical Physics
University of Birmingham
Birmingham, England B15 2TT
\endauthor
\abstract
A theory of superconductivity is presented, based on
the fact that...

\endabstract
\toappear{Physical Review}
\endtitlepage

\section{Introduction}
The main facts which a theory of superconductivity
must explain are...
|endTeXexample
\noindent 
A more complete example paper is given in the file |Example.tex|, which
is included in the standard distribution of \TeXsis.
In the rest of this section each of the elements in these examples and
several others related to the overall structure of a complete paper will be
explained.


%==================================================*
\subsection{Types of Documents \label{sect.types}}

A complete \TeXsis\  paper should start with the \cs{texsis} macro
described in \Sect{start}.  (While this is not required if you also use
one of the macros listed below, it does serve to indicate unambiguously
that the document is
to be typeset with \TeXsis.)  The \cs{texsis} macro can be followed by a
command to specify whether the document should be typeset as a simple
paper or conference proceeding, a preprint, a book, etc.  This macro
will provide sensible defaults for the layout, although additional
commands can be given to override them.  The following basic document
types are included in \TeXsis:

\description{XManuscript\quad}\clump
%
\itm{\CS{paper}}
Produces a paper with the title and abstract at the top of the first
page, such as for submission to a conference proceeding.
The text begins on the first page, below the title and abstract.
%
\itm{\CS{preprint}}
Produces a paper in a preprint format, with the title material by
itself on the first page.   A \cs{banner} is put across the top of the
title page, and the text begins on the page following.
%
\itm{\CS{eprint}}
Produces a paper in a format suitable for submission to the Los Alamos
preprint archives (the ``e-print bulletin boards,'' such as hep-ph,
hep-th, hep-lat, or qc-gr).
%
\itm{\CS{Manuscript}}
Similar to \CS{preprint}, but with true double spacing for manuscripts
submitted for publication in journals such as the {\sl Physical
Review}, which require this.
%
\itm{\CS{book}}
A book or other long document with a separate title page, running
headlines, a table of contents, etc.  Page numbers are printed in
opposite corners on left and right handed pages.
%
\itm{\CS{thesis}}
Similar to \CS{book}, this format is for typesetting a doctoral
dissertation or similar document.
It may be customized for your own university.
%
\enddescription

After the command defining the type of document you can add optional
commands to modify the default layout. For example, you might want to
use
\example
|\texsis| \n
|\paper| \n
|\hsize=|\meta{dimen} \n
|\vsize=|\meta{dimen} \n
|\singlespaced|
\endexample
to produce a single-spaced conference proceeding of a specified
size.  Macros to create more specialized documents in some of the common
formats used by physics journals and conferences are described in
\Sect{fmts}.  Since \TeXsis\  obviously cannot include all
the possible formats, that section also contains some hints about how
to modify a format to fit your needs.

%==================================================*
\subsection{The Title ``Page''              \label{sect.title}}

A complete paper almost always starts with a title.  Most probably it
will also include the names of the authors, and often it will include
an abstract.  Sometimes the title, author's name(s) and abstract
appear on a separate page, while sometimes they simply appear at the
top of the first page of the paper.  In any case we will refer to all
this material together as the ``title page'' material.

\TeXsis\  has a standardized method for entering the title page
material, whether it will actually appear on a page by itself or not.
You begin the title page material with \CS{titlepage} and end it with
\CS{endtitlepage}.
Between these two control sequences you can use any of the commands
listed below to enter the various parts of the title page material.
(An example appears at the beginning of this section).

\description{Xendauthor}\clump\tolerance=1700
%
\itm{\CS{title}}
Begins the title of the paper.  All lines up to a following
\cs{endtitle} are centered and printed in \cs{Tbf} type. 
Line endings in the input file {\sl are} respected, just as with
\cs{obeylines}. 
%
\itm{\CS{author}}
Begins an entry for an author's name and address, which should
appear on the lines that follow.  Line endings in the input file
{\it are} respected.  The first line to follow is the author's
name, which is printed centered in |\bf| type.  All subsequent
lines are centered in normal type.  End with |\endauthor|.
%
\itm{\CS{and}}
Prints the word "and," centered and with appropriate vertical
separation.  It can be used either between sets of |\author|
\dots\  |\endauthor| or between multiple addresses for the same
author.
%
\itm{\CS{authors}}
An alternative to |\author| for papers with many authors. Here
line endings are not respected; instead, the list of authors is
divided into lines as evenly as possible, and each line is
centered and printed in Roman type.  End with \cs{endauthors}.
%
\itm{\CS{institution}\arg{symbol}\arg{address}} Prints the symbol and
the institution address centered on a line.  A |\medskip| is placed
before the first address to separate the names and addresses.
|\institution| should be placed after the author list but before
|\endauthors| to work correctly.
%
\itm{\CS{abstract}}
Begins the abstract.  The word "ABSTRACT" is centered above the
abstract.  The left and right margins are brought in relative to
the text of the paper.  End with |\endabstract|.
%
\itm{\CS{pacs}\arg{codes}}
Prints the PACS (Physics and Astronomy Classification Scheme) codes
on the title page.
%
\itm{\CS{submitted}\arg{Journal}}
Prints the phrase ``Submitted to {\sl Journal}'' centered on the page,
with the name of the journal printed in |\sl| type.
%
\itm{\CS{toappear}\arg{Journal}}
Similar to |\submitted|, this prints the phrase 
``To appear in {\sl Journal}'' on the title page, 
with the name of the journal printed in |\sl| type.
%
\itm{\CS{disclaimer}\arg{contract}}
Prints a standard DOE disclaimer for contract number
\meta{contract}, printed in 10~pt.\  type, as a footnote.
%
\enddescription

The advantage to entering the title page material this way is that if
you change the overall document format, either from \cs{paper} to
\CS{preprint} or to one of the more specialized formats  described in
\Sect{fmts},  then the title page material will automatically be changed 
to match the document format selected.  

\bigskip

The \cs{banner} macro is called in the \CS{preprint} document format to
put the date, a preprint code number, and the name of your institution
or organization at the top of the title page.  The name of the
institution can be changed by saying
\example
|\def\ORGANIZATION{|\meta{name}|}|
\endexample
and this change can be made permanent by putting it into the file
|TXSsite.tex| when \TeXsis\  is being installed. 
\def\tmp{}\ifx\ORGANIZATION\tmp
(When this manual was printed, the \CS{ORGANIZATION} macro was 
empty.)
\else
When this manual was printed, the \CS{ORGANIZATION} macro was:
\example
\ORGANIZATION
\endexample
\fi
The preprint date and document number can be specified by putting
\example\obeylines
\CS{pubdate}\arg{date}
\CS{pubcode}\arg{code-number}
\endexample
before the \cs{titlepage}. If these are omitted, the date defaults to
the current month and year and the document number defaults either to
something set as the default when \TeXsis\  was installed or to the
current \TeXsis\  version number.  The default at this installation is
\ATunlock   "\@DOCcode".   
\ATlock
If you really want to customize the look of your paper you can create
your own \cs{banner} macro.

\bigskip

If you are submitting your paper to one of the \idx{preprint archives}
at Los Alamos, such as |hep-lat|, |hep-ph|, or |hep-th|, you can have
the archive server automatically insert the preprint number it assigns
your document when it is submitted.
To do this put
% Important: putting \relax here and below inhibits any such substitutions
% in this manual if it gets put out on such a server.
\example
\CS{pubcode}|{hep-lat|\relax|/yymmnnn}|
\endexample
near the top of the document (change ``\idx{hep-lat}'' as appropriate
to the archive you are using).
When you submit a paper to an archive with the ``put'' command the
server will translate any occurrence in the input file of the sequence
``|hep-lat|\relax|/yymmnnn|'' to ``|hep-lat/|'' followed by the 7
digit paper number.
You must use the exact string of characters ``|yymmnnn|'' for this to
work.
When someone else gets your paper from the server and prints a copy it
will have the assigned document code in the banner at the top of the
title page.

\bigbreak

The standard \cs{footnote} macro cannot be used to make footnotes to
titles and authors's names, because these special fields 
use internal vertical mode, where footnotes are not allowed. You
should instead use the \cs{vfootnote} macro of plain \TeX\  for such
purposes.  In this case you must put the reference mark in the title or
author list yourself (for example, with |${}^*$|), and then say
|\vfootnote*{The footnote}| somewhere else on the page, outside of the
internal vertical mode material (the author or title blocks).

``Footnotes'' identifying institutions in a long list of authors
are often centered on lines immediately below the author list.
\CS{authors} and \CS{institution} can be used for this. For example,
the author list of one well known paper\reference{DIS} E.D. Bloom, et al.,
Phys.\  Rev.\  Letters \vol{23}, 930 (1969) \endreference
could be typed as follows:
\TeXexample
|smalltt\authors 
E.D.~Bloom,$^a$ M.~Breidenback,$^b$ D.H.~Coward,$^a$
H.~DeStaebler,$^a$ J.~Drees,$^a$ J.I.~Friedman,$^b$
G.C.~Hartmann,$^{b,c}$ H.W.~Kendall,$^b$ G.~Miller,$^a$ 
L.W.~Mo,$^a$ R.E.~Taylor$^a$
\institution{a}{Stanford Linear Accelerator Center,
   Stanford, CA 94305}
\institution{b}{Department of Physics and Laboratory for Nuclear
Science, Massachusetts Institute of Technology, 
   Cambridge MA 02139}
\institution{c}{Xerox Corporation, Rochester, NY}
\endauthors
|endTeXexample
This produces
\authors 
E.D.~Bloom,$^a$ M.~Breidenback,$^b$ D.H.~Coward,$^a$
H.~DeStaebler,$^a$ J.~Drees,$^a$ J.I.~Friedman,$^b$
G.C.~Hartmann,$^{b,c}$ H.W.~Kendall,$^b$ G.~Miller,$^a$ 
L.W.~Mo,$^a$ R.E.~Taylor$^a$
\institution{a}{Stanford Linear Accelerator Center,
   Stanford, CA 94305}
\institution{b}{Department of Physics and Laboratory for Nuclear
Science, Massachusetts Institute of Technology, 
   Cambridge MA 02139}
\institution{c}{Xerox Corporation, Rochester, NY}
\endauthors
\noindent Note that \cs{institution} must come {\sl before} the
\cs{endauthors}. 

%==================================================*

\subsection{Chapters and Sections \label{sect.sects}}

The following commands can be used to create and automatically number
the divisions of a document, including chapters, sections, and
appendices:

\singlelinetrue                         % make label overhang
\description{\hskip 3cm}\clump

\itm{\CS{chapter}\arg{title}\quad} 
Begins a chapter with \meta{title} printed in |\Tbf|
type on a new page.

\itm{\CS{section}\arg{title}\quad} 
Begins a section with the \meta{title} printed in |\tbf|
type.  This does {\it not} begin on a new page (although you 
certainly can start a section on a new page by saying
|\vfill\supereject| first.)

\itm{\CS{subsection}\arg{title}\quad} 
Begins a subsection with the \meta{title} printed in |\bf|
type.

\itm{\CS{subsubsection}\arg{title}\quad} 
Begins a sub-subsection entitled \meta{title}.  This is a lower
level than even a subsection and should only be used when truly appropriate.

\itm{\CS{Appendix}\arg{label}\arg{title}} 
Begins a chapter-like appendix labeled by the letter \meta{label}
(generally A, B, C, \dots or I, II, III, \dots).  The \meta{title}
is printed in |\Tbf| type at the top of a new page.

\itm{\CS{appendix}\arg{label}\arg{title}} 
Begins a section-like appendix labeled by the letter \meta{label}
(generally A, B, C, \dots or I, II, III, \dots).  The \meta{title}
is printed in |\tbf| type.

\itm{\CS{nosechead}\arg{title}} 
The \meta{title} is typeset like a subsection headline, but without a
subsection number.  This is suitable for acknowledgments, lists of
references and such.

\enddescription
The title appears at the top of the section or subsection, left
justified in the appropriate typestyle for the document type and with
an appropriate amount of vertical spacing below.  These and other
details of the layout vary with the document format.
In general \CS{chapter} is appropriate mainly
for books or theses; the manual you are reading now uses only
\CS{section} and \CS{subsection} divisions.

As you may have noticed from this manual, the first paragraph of a
section or subsection is not indented.
This is the usual practice for most well-designed books, though it may
seem odd at first.
It isn't that strange if you think of the indentation of a paragraph
as a way to mark a {\sl separation between} paragraphs, rather than as
a way to mark the beginning of a paragraph.
But if you really want to indent that first paragraph, just begin it
with \cs{par}.
On the other hand, if you put any kind of commands or macro
definitions between the \CS{section} or \CS{subsection} and the first
paragraph then this will interfere with the suppression of the
indentation.
You can easily fix this by either moving your macro definitions
elsewhere or putting in a \cs{noindent} by hand.



   By default chapter, section, and subsection numbers are printed with
the titles, and the chapter and section numbers are also attached to
equation, figure and table numbers in the form ``$cc.ss.nn$'', where
$cc$ is the chapter number, $ss$ the section number, and $nn$ the
equation number.  The inclusion of the chapter and section numbers can
be turned on or off with the commands
\description{~showchaptIDfalse\qquad} 
\itm{\CS{showchaptIDtrue}} Display chapter numbers.
\itm{\CS{showchaptIDfalse}} Do not display chapter numbers.
\itm{\CS{showsectIDtrue}} Display section numbers.
\itm{\CS{showsectIDfalse}} Do not display section numbers.
\enddescription

\bigskip

        \CS{Appendix} and \CS{appendix} give chapter-like and
section-like appendices, respectively. The chapter or section number is
replaced by a letter, which is given as the first argument, and the
title, which is the second argument, is printed in the same format as a
chapter or section title.  For example,
\TeXexample
\appendix{A}{Review of Functional Integration}
|endTeXexample
\noindent
The letter labeling the appendix is attached to the equation
numbers if \CS{showchaptIDtrue} or \CS{showsectIDtrue} is used.  You can 
omit the letter by using an empty first argument, as in
\TeXexample
\appendix{}{Leading logarithm approximation}
|endTeXexample
\noindent
in which case the result is the same as setting
\CS{showchaptIDfalse} or \CS{showsectIDfalse}.

\bigskip

       A skip called \CS{sectionskip} is used before the title of any
section-like division (i.e., a |\section| or an |\appendix)|; a skip
called \CS{subsectionskip} is used for any lower-level division, that is, a
\CS{subsection}, \CS{subsubsection}, or a \CS{nosechead}. Of course these
are all removed by \TeX\  if the section starts on a new page.
Furthermore, any of these divisions will start on a new page if less
than a minimum length given by \CS{sectionminspace} remains on the
current page. 
The defaults for these parameters are
\TeXexample
\sectionskip=2cm plus 8pt minus 8pt     % skip before \section
\subsectionskip=1cm plus 4pt minus 4pt  % skip before \subsection
\sectionminspace=0.25\vsize             % new page if less left
|endTeXexample
but they can be changed to any legal \TeX\  skip or dimension either
throughout the document or in a local group. For example, you could
use
\TeXexample
\begingroup
\subsectionskip=\sectionskip
\fourteenpoint
\nosechead{Acknowledgments}
\endgroup
|endTeXexample
to make a \CS{nosechead} which looks like a |\section|.

\bigskip

        Ordinarily the order of appearance of the sections of a document
are not changed, so there is no automatic mechanism for labeling the
sections.  Any of the divisions described above can be labeled by
inserting \cs{label}\arg{label} into the title text.  The \meta{label}
should have the form \meta{type.name}, where \meta{type} is |chap|,
|sect|, etc., to avoid possible conflicts with other kinds of labels.
The corresponding numbers can then be referred to with
|\use{|\meta{type.name}|}|, as described in \Sect{labels}.

        If the title of a chapter or section is too long, a break in
the line can be inserted with the \cs{n} command.  This will also
break the line in the title when it appears in the table of contents.
If running headlines are being used (see \Sect{headline}) then only the
first part of the title, up to the \cs{n}, will be used for the running
headline.  
You can always reset the headline by hand with |\setHeadline|
(see  \Sect{headline}).

        You can customize the behavior of the section-making macros by
adding your own definitions for the macros
\index{customization!chapters and sections}%
\example
\CS{everychapter}, \CS{everysection}, \CS{everysubsection},
\CS{everysubsubsection},
\endexample
which are called immediately before making the corresponding
division of the document, or for the macros
\example
\CS{afterchapter}, \CS{aftersection}, \CS{aftersubsection},
\CS{aftersubsubsection},
\endexample
which are called immediately afterwards.  By default all of these macros
do nothing.  As an example, if you wanted every section to begin on a
new page, and you wanted numbered footnotes created by \cs{NFootnote} to
start over counting from one at the begining of each section,  you
could define: 
\TeXexample
\def\everysection{\vfill\supereject \footnum=0}
|endTeXexample

The full text of the title of the current chapter, section, and
subsection is saved in \CS{ChapterTitle}, \CS{SectionTitle}, and
\CS{SubsectionTitle}, so that you may do whatever you want with these
later in the chapter or section.  An example of how these might be
used with \CS{aftersubsection} to change the running headlines is
given in \Sect{headline}.


Another way you can control the results produced by \CS{chapter} and
\CS{section} is to change the way the chapter or section number is
printed, by changing the definitions of the macros \CS{ChapterStyle},
\CS{SectionStyle}, or \CS{SubsectionStyle}.  These macros take
one argument, the chapter or section number, and by default they simply
return that argument.    By substituting your own definition you can do
almost anything you like to the section number.  For example, it is
often desired that the section number printed as a Roman numeral
rather than as an Arabic numeral.  The following definition will convert
the section number to an upper-case Roman numeral:
\TeXexample
\def\SectionStyle#1{\uppercase\expandafter{\romannumeral #1}}
|endTeXexample

Even more customized behavior of the section-making macros is possible.
For example, \cs{Tbf} and \cs{tbf} can also be defined as macros which
take a single argument, which is arranged to be the title of the
section or chapter. 
To see how this works it may be best to look at the definitions of
\CS{chapter} and \CS{section} and the like in the  source file
|TXSsects.tex|. For very specialized applications you could even copy
the definitions in this file to the TXSmods.tex file or your own
personal style file and then alter them however you like.


%==================================================*

\subsection{The Master File}                          
                                                     
   An useful concept for typesetting a long document is that of
the\index{master file} {\em Master File}.
The idea is to create one small file which initializes \TeXsis\  with
\cs{texsis}, makes any special definitions or sets parameters, and
then reads in the actual text of the document in sections using the
|\input| command.
As a simple example consider a manuscript called MUONS.
The master file is called |MUONS.tex|, and contains the following:
\TeXexample 
|smalltt\texsis                % initialize TeXsis
\input MyMacros        % special macros for this paper
\input MUONSr          % references for the paper
\input MUONS0          % title page for the paper
\input MUONS1          % first section of manuscript
\input MUONS2          % second section of manuscript
\ListReferences        % print the reference list
\end
|endTeXexample
The file |MyMacros.tex| contains any special macro definitions used
in this paper.  (If there are none, leave this line out.)  The
file |MUONSr.tex| contains the references used in this paper,
beginning with |\referencelist| and ending with
|\endreferencelist|, as described in \Sect{refs}.  The
file |MUONS0.tex| contains the commands for producing the title
page of the paper, as described in \Sect{fmts}.  The
actual text of the manuscript is divided into two parts, which
are kept in the files |MUONS1.tex| and |MUONS2.tex|.  To process the
entire document one simply runs \TeXsis\  on the master file with
the command
\TeXexample
texsis MUONS
|endTeXexample
\noindent
If you are familiar with computer programming you may think of
the master file as the main program and the other files as
subroutines called by the main program.

   One of the great advantages of using a master file, aside from
keeping the manuscript organized, is that it lets you work on a single
section of the paper at a time.  For example, suppose you want to
run off a draft of just the first section of the paper called MUONS
above.  You don't need to process the references, the title page,
or the second section of the paper.  Recall that the percent sign
(|%|) is the comment character in \TeX\  --- anything appearing on
a line after a |%| is ignored by \TeX.  Thus we simply edit the
master file and put a |%| in front of certain lines, like this:
\TeXexample 
|smalltt\texsis                    % initialize TeXsis
\draft                     % optional draft mode
\input MyMacros            % special macros for this paper
% \input MUONSr            % references for the paper
% \input MUONS0            % title page for the paper
\input MUONS1              % first section of manuscript
% \input MUONS2            % section section of manuscript
% \ListReferences          % print the references
\end        
|endTeXexample
Now when you run \TeXsis\  on this file only the first section of
the paper will be processed, but it will use the macros defined
in |MyMacros.tex|, as well as any other definitions that you might
add to the master file. Once the first section is error free you
can repeat the process on the second section of the paper,
without rerunning the first section. After both sections are
correct you can reinstate all lines in the master file and
process the whole document.

%==================================================*

\subsection{Table of Contents \label{sect.conts}}

The section-making macros just described automatically make an entry in
a \idx{table of contents} for the document being prepared.  The title of
the chapter or section and the page number are stored in a table of
contents file (which ends with the extension ``|.toc|'') until needed,
then the table of contents is created simply
by saying \CS{Contents}.  
If your document is divided into chapters,
like a book, the table of contents should appear on a page with a Roman
numeral page number (which is obtained by setting \cs{pageno} to a
negative value).  For example:
\TeXexample
\book                     % document format for a book
\input Chapt1             % input chapters...
...
\vfill\supereject         % eject last page, including figures
\pageno=-3                % print on pages iii, iv, ...
\nosechead{Contents}      % label TOC without section #
\Contents                 % generate table of contents
|endTeXexample

The table of contents is usually printed at the {\sl end\/} of the
document so that all the page numbers are correct.  You then
simply move these pages to the front of the document by hand.
Alternately, you can say \CS{Contents} at the beginning of your document
to read in the table of contents file produced by a previous run of
\TeXsis, although then you must process the document twice to be sure
that the page numbers are correct. 

It is important to note that nothing is written to the table of contents
file unless \CS{Contentstrue} is set.  The default for most documents
is \CS{Contentsfalse}, since one usually does not want a table of
contents for a short paper or a memo or a letter.  The exceptions are
\cs{book} and \cs{thesis}, which automatically set
\CS{Contentstrue}.  In any case, if you want a table of contents
you can explicitly state \CS{Contentstrue}.
 
        While an entry in the table of contents is created by
\cs{chapter} or \cs{section}, no entry is made for \cs{nosechead}.  In
this case, or in any case, you can insert an entry into the table of
contents ``by hand'' by saying
\example
\CS{addTOC}\arg{level}\arg{title}\arg{page}
\endexample
where \meta{level} is the level of division for the entry being made,
with 0 for chapters, 1 for sections and 2 for subsections.  This
controls how much vertical space is put around the table of contents
entry and how much the entry is indented in the list.
%
The \meta{title} is the text of the title as it should appear in the
table of contents.  Text which is too long for one line will be split
over several lines, but you can force a line break where you want one
with \cs{n}. The \meta{page} is the page number for the entry you are
making.  Generally you'll use \cs{folio} here to just use the current
page number, whatever that may be.  Thus to make a section level entry
in the table of contents having the title ``Acknowledgments'' you would
simply say
\TeXexample
\addTOC{1}{Acknowledgments}{\folio}
|endTeXexample

Entries made in the table of contents by \cs{chapter}, \cs{section},
etc. will optionally begin with the chapter or section number, depending
upon whether \cs{showchaptIDtrue} and \cs{showsectIDtrue} are selected.
The decision whether or not to show the chapter or section number is
made when the table of contents is printed by \CS{Contents}, not when
the \cs{chapter} or \cs{section} is begun.  Thus it is possible to have
the section numbers showing at the beginning of the section, but to then
have no section numbers in the table of contents by saying
\cs{showsectIDfalse} immediately before saying \CS{Contents}.

\bigbreak

It is possible to write commands or other text to the table of contents
file while it is being created, using the command
\CS{TOCwrite}\arg{stuff}.
For example, the heading separating the appendices from the rest of
the entries in the table of contents of this Manual was created by
saying
\TeXexample
\TOCwrite{\bigskip\noexpand\nosechead{Appendices:}}
|endTeXexample
\noindent
before the appendices were created.  The \cs{noexpand} prevents the
|\nosechead| from being expanded until later, after the table of contents
file is read back in by \CS{Contents}


\bigbreak

        What if you want the table of contents to appear in the table
of contents itself!? Naturally it should appear first (even though it
may be printed at the end of the document).  You can use the tagging
mechanism described in \Sect{labels} to make a ``forward reference''
to the page number.  At the beginning of the document you can create
the table of contents entry by saying
\TeXexample
\addTOC{0}{Table of Contents}{\noexpand\use{page.TOC}}
|endTeXexample 
\noindent
The \cs{noexpand} prevents the \cs{use} from being expanded until later,
after the table of contents file is read back in.  Later, when you actually
print the table of contents you would first tag the page number, then
invoke \CS{Contents}, like so:
\TeXexample
\nosechead{Table of Contents}
\tag{page.TOC}{\folio}
\Contents
|endTeXexample 

\Ignore         % NO, WE DON'T DO IT
\noindent
As a demonstration of this we have done this for the table of contents
for this manual.  For more information about \cs{tag} and \cs{use} see
\Sect{labels}.\relax 
\endIgnore

%======================================================================*

\subsection{Running Headlines and Footlines     \label{sect.headline}}

        In Plain \TeX,  whenever a page is output an optional headline
and footline are added at the top and bottom of the page.  In \TeXsis\
the footline is normally blank, while the headline contains the page
number in the upper right corner. The \cs{book} and \cs{thesis} formats
also display a running headline, centered in 10~pt.\  italic type,
containing the chapter or section title.  
Running headlines and page numbers are {\bf not} printed on any
page containing a \cs{title} or a new \cs{chapter}.  
For an example of running headlines look at the top of
any page in this Manual.

        \CS{nopagenumbers} turns off the running headlines and
footlines, just as it does in Plain \TeX. In \TeXsis\  saying
\CS{pagenumbers} restores the standard headlines and footlines.
Saying \CS{bookpagenumbers} puts odd page numbers in the upper right
corner and  even page numbers in the upper left corner, the normal
convention for books.  This is the default for \cs{book}.

        \CS{RunningHeadstrue}, the default for \cs{book} (and 
\cs{thesis}), displays the running headline text \cs{HeadText}, as well
as the page number.  |\RunningHeadsfalse| turns off the running
headlines but leaves the page numbers. \cs{HeadText} is defined by
\cs{chapter} to be the chapter title in 10~pt.\  italic type; if no
chapters are used, then it is defined by \cs{section} to be the section
title. You can change the running headline yourself with the command
\CS{setHeadline}\arg{text}.  For example, the headline at the top of
this page could have been created by saying
\example\tt
\CS{setHeadline}|{|\HeadText|}|
\endexample
The headline text appears in typestyle \CS{HeadFont}, which 
you can change to suit your tastes.  The default is
|\tenit|,
but for this manual we used |\def\HeadFont{\tensl}|.


Running headlines are not normally affected by \cs{subsection}, but it
is easy to have each subsection change the runing headlines by
defining \cs{aftersubsection} to include a \CS{setHeadline} command.
As a useful example, suppose that each subsection is to have a running
headline composed of both the title of the section and the title of
the subsection.  You could have this done automatically by defining
something like:
\TeXexample
\def\aftersubsection{\setHeadline{\SectionTitle: \SubsectionTitle}}
|endTeXexample
The rest of this section will have such running headlines as an
example. 
\def\aftersubsection{\setHeadline{\SectionTitle: \SubsectionTitle}}
\bigskip  %<-- needed to force page break before next section!


%======================================================================*

\subsection{How to Make an Index        \label{sect.index}}

The traditional way to make an index for a book was to write key~words
on 3$\times$5 ``index'' cards, and to then write on these card the
numbers of the pages on which the key words occurred.
The cards were then sorted by hand and then their contents also sorted
and typed in order to make the index.
This was a tedious and time consuming task.
Fortunately this whole process has been modernized so that an index
can be made almost automatically.
Still, it takes a certain amount of care to create a useful, well
constructed index.
An important part of designing a good index is deciding which
key~words will be useful in the index and which key~words will not.
A computer cannot make these decisions for you.

Making an index with \TeX\  still involves collecting the index entries,
sorting them (and removing duplicate references), and then producing the
final index.  The first and last steps are accomplished with a set of
plain \TeX\  macros known as |index.tex|, while the sorting and collating
of the index entries is done by a separate computer program called
|makeindex|.\index{makeindex}\  This is how we made the index for this
Manual, which starts on page~\use{pg.index}.
 
To create an entry in the index you can say \CS{index}\arg{key}, where
\meta{key} is the key~word to be entered into the index, or more simply
you can say \CS{idx}\arg{word}, which both includes the word in the
text and makes an index entry for it; saying ``\CS{idx}|{Frisbee}|''
is the same as saying ``|Frisbee|\CS{index}|{Frisbee}|''.
Either of these indexing commands writes the index entry to a file
with the name extension ``|.idx|''.
Once your document has been processed by \TeX\  this file will contain
all of the ``raw'' index entries.

The entries in the raw index file are not sorted, and there may be many
duplications.  The |makeindex|\index{makeindex} program will sort the
index entries and reformat them in a form appropriate for printing the
index.  The output from the |makeindex| program is a file with the
extension ``|.ind|''.  So if your 
manuscript file is called |myfile.tex| and you have run it through
\TeXsis\  you should find a file called |myfile.idx| containing the raw
index entries.  You would run the |makeindex| program like so,
\example
|makeindex myfile.idx|
\endexample
to produce the processed index file |myfile.ind|.
(You can leave off the |.idx| extension for the raw file and |makeindex|
will assume it.)  

To print the index you can either include it in another pass of the full
document, by saying
\TeXexample
\input \jobname.ind
|endTeXexample
\noindent
in the manuscript file, or you can print it separately by adding
``|\input index|'' to the begining of the |myfile.ind| file and running
it through \TeX\  or \TeXsis.

If you set \CS{markindextrue} then indexed words selected with \CS{idx}
will appear in the printed output inside a ruled box 
like so:
\begingroup
\markindextrue \idx{Frisbee}. 
\endgroup
The box does not interfere with or change the word or line spacing. 
These ``proof marks'' are useful for draft copies when you are putting
together the index, but obviously you do not want them to appear in the
final copy, so the default is |\markindexfalse|.

The |index.tex| macros are distributed with \TeXsis, but |makeindex|
is not.
If you do not have the |makeindex| program you can get the C source
code for it from the \TeXsis\  anonymous ftp site,
|ftp.texsis.org|.
It is a well-known program, and is also available from CTAN in the
directory {\tt indexing/makeindex}.
Note that the |index.tex| macros work with plain \TeX\  as well as
\TeXsis, and they work with |makeindex| in its default mode, even
though that default mode was originally intended for \LaTeX\  code.

For more information about how to make full use of |makeindex| see the
file |index.tex|, the Unix man page for makeindex(1), and the original
paper by Chen and Harrison\reference{makeindex} Pehong Chen and
Michael A. Harrison, ``Index Preparation and Processing'', in {\sl
Software: Practice and Experience}, \vol{19}, 897-915 (1988)
\endreference
describing how they designed the |makeindex| program.

\def\aftersubsection{\relax}

%>>> EOF TXSsects.doc <<<
