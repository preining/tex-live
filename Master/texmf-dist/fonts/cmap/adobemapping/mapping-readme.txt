The links above are for the mapping resources for PDF, divided into two categories, ToUnicode and everything else:

*mappingresources4pdf_2unicode_20091116.tar.Z* (MD5 = 59267a7d0b818267c8ce85d1dbc4479a): ToUnicode Mapping Resources (for Adobe-GB1, Adobe-CNS1, Adobe-Japan1, and Adobe-Korea1)  
*mappingresources4pdf_other_20091116.tar.Z* (MD5 = 202fa0a291fc1f5b50cfa239106c1b92): All Other Mapping Resources

The reason to separate the ToUnicode mapping resources from the other mapping resources is related to the static nature of the other mapping mapping resources. The ToUnicode mapping resources are subject to change when a new Supplement is defined, or when a new version of Unicode triggers additional mappings in CMap resources that in turn influence these mapping resources.

That is all.
