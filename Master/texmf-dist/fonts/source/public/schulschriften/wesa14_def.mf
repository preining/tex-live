% Schulausgangsschrift (SAS)  wesa
% Parameter und Makrodefinitionen
% 23.10.2011

% Parameter:

% kl. Abstand in der Groesse der Strichstaerke
eta:=1ut;
% Steigung des Verbindungsstrichs
m:=1.5;
% Superellipse
b=ht/2;
k:=1.5; % SAS
a*k=b;
sigma:=0.73345;

% Delta, delta
  z1=(0,b); z2=(-a,0);
  z5=(-sigma*a,sigma*b);
  p := z1{left}..z5{z2-z1}..{down}z2;
  x0:=whatever;y0:=whatever;
    z0=(directionpoint(1,m) of (reverse (subpath (0,2) of p)));
    delta:=a+x0;Delta:=b-y0;

% typische kleine Masse
dd:=2.5ut; 
D:=5ut; 
s:=7.5ut; 
% Radius z.B. bei "a"
rho:=2ut;

% Unterschleife, Schnittpunkt bei (0,0)
% Steigung m
sb:=9ut;
sbf:=0.35; % sb*sbf
sbfh:=0.23; % dt*sbfh 
% vor lat. e, Steigung m'=1
m':=0.87;
sb':=9ut;
sbf':=sbf;
sbfh':=0.28;
% Oberschleife, Schnittpunkt bei (0,ht)
% Steigung m
sb'':=9ut;
sbf'':=0.35; % sb''*sbf''
sbfh'':=0.23; % dt*sbfh''
% Breite von "h" etc (breite=ht/kh)
kh:=1.66;
% Breite der Fahne bei r
rfahne:=12ut;
% Einrollmass
einroll:=ht/9;
% Breite der Fahne bei o, v, w
ofahne:=rfahne+einroll;
% Breite der Fahne bei b
bfahne:=ofahne;
% Steigung der Fahne bei b, v, w
mfahne:=2;
% Breite von "s"
%sbreite:=7.5ut;
sbreite:=6ut;
% gekroepftes e
me:=0.8;
ebreite:=8ut;
ef:=0.2;
he:=ht/2-2eta;
%Steigung im Beruehrpunkt
mt:=m;
%
s_kurzstrichbreite:=(Delta-ht+(ht))/m;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter fuer Grossbuchstaben:
% Radius bei "Flanke"
R:=3.5ut;
% Abstand der Umlautstriche bei Grossbuchstaben:
dumlaut:=5ut;
% Breitenmasse:
Sporn:=D; % Horn bei B, D, etc.
sporn:=10ut;
Abreite:=25ut;
Bbreite:=20ut;
Bbreite':=15ut; % oberer Bauch
Hbreite:=17.5ut;
Nbreite:=18ut;
Lbreite:=22.5ut;
Obreite:=27.5ut;
% Oval
A:=Obreite/2; B:=ht;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter fuer Ziffern:
s':=Hz/8;    % seitlicher Abstand
w':= Wz-2s';  % effektive Breite der Ziffern
r:=w'/2;     % Radius bei "2"
% Zeichen:
hmath:=ht/2;
hdash:=ht/2;
% Kerning:
skern#:=-6ut#;
sskern#:=-7.5ut#;
Bkern#:=-15ut#;
Dkern#:=-17.5ut#;
Ikern#:=-8.75ut#; 
Nkern#:=-18ut#;  
Okern#:=-13.75ut#; % auch Ö
Pkern#:=-15ut#;
Skern#:=-7.5ut#;
Tkern#:=-15ut#;
Vkern#:=-17.5ut#;   % auch W

%%%%%%%%%%%%%% Makros %%%%%
Kern:=0;
% Nur fuer Wartungszwecke
% Makro aktivieren, um die 'width' der Zeichen auszugeben
def zeigen=
%  weite:=20*xpos/ht;
%   if Kern>0:
%    kernwert:=20*Kern/ht;
%showvariable kernwert;
%    fi 
%  showvariable weite;
%  Kern:=0;
enddef;

% Fuer Kleinbuchstaben:
% Makros fuer Kleinbuchstaben
def oval(expr sx)=
	         x1:=whatever;y1:=whatever;
    x2:=whatever;y2:=whatever;
    x3:=whatever;y3:=whatever;
    x4:=whatever;y4:=whatever;
    x5:=whatever;y5:=whatever;
    x6:=whatever;y6:=whatever;
    x7:=whatever;y7:=whatever;
    x8:=whatever;y8:=whatever; 
  z1=(0,b); z2=(-a,0); z3=(0,-b); z4=(a,0);
  z5=(-sigma*a,sigma*b);
  z6=(-sigma*a,-sigma*b);
  z7=(sigma*a,-sigma*b);
  z8=(sigma*a,sigma*b);
  p:= ( z1{left}.. z5{z2-z1}..z2{down}.. z6{z3-z2}..
    {right} z3.. z7{z4-z3}..z4{up}.. z8{z1-z4}..{left} z1);
  draw p shifted (sx+a-delta,b);
  x0:=whatever; y0:=whatever;
    z0=(directionpoint (1,mt) of (subpath (4,6) of p)) shifted (sx+a-delta,b);
    x1:=whatever;y1:=whatever;
  z1=(directionpoint (-1,mfahne) of (subpath (6,8) of p)) shifted (sx+a-delta,b);
    xpos:=sx+2a-delta;
  enddef;

  \def stock(expr sx)=
    if not wortende:
    draw ((0,rho){down}..(rho,0){right}..tension2.5 and 4..{1,m}(2*rho+((ht)-Delta)/m,ht-Delta)) shifted (sx,0);
    xpos:=sx+2*rho+((ht)-Delta)/m;
  else:
    draw ((0,rho){down}..{right}(rho,0)) shifted (sx,0);
    xpos:=sx+rho;
    fi
  enddef;

  def stamm(expr ha, hb, sx)=
  draw ((0,ha)--(0,hb)) shifted (sx,0);
  xpos:=sx;
enddef;

def strich (expr sx)=
  if not wortende:
    draw ((0,0)--((ht-Delta)/m,ht-Delta)) shifted (sx,0);
    xpos:=sx+(ht-Delta)/m;
  else:
    xpos:=sx;
    fi
  enddef;

  def uschl(expr sx)=
draw ((0,0){down}..tension 1.5..(-(1-sbf)*sb,-dt){left}..(-sb,-dt*(1-sbfh)){up}..tension1.5..{1,m}(0,0))  shifted (sx,0);
xpos:=sx;
enddef;

def uschl_e (expr sx) =
  draw ((0,0){down}..tension 1.5..(-(1-sbf')*sb',-dt){left}..(-sb',-dt*(1-sbfh')){up}..tension1.5..{1,m'}(0,0))  shifted (sx,0);
xpos:=sx;
  enddef;

  def oschl(expr sx)=
  draw ((0,ht){1,m}..tension2..(sb'',Ht-dt*sbfh''){up}..((1-sbf'')*sb'',Ht){left}..tension 1.5..{down}(0,ht)) shifted (sx,0);
  xpos:=sx;
enddef;

   def bogen(expr hl,hm,hr,bt,fx,phil,phir,tensl,tensr,sx)=
     p:=((0,hl){dir phil}..tension tensl..(bt*fx,hm){bt,0}..tension tensr..{dir phir}(bt,hr));
     draw p shifted (sx,0);
     if (hl>hm) and (hr>hm):
x0:=whatever; y0:=whatever;
z0=(directionpoint (1,mt) of if bt>0:(subpath (1,2) of p)else: reverse(subpath (0,1) of p)fi) shifted (sx,0);
fi
     xpos:=sx+bt;
   enddef;

   def schweif (expr hl, hr, bt, phil, phir, tens, sx) =
    p := (0,hl){dir phil}..tension tens..{dir phir}(bt,hr);
    draw p shifted (sx,0);
    if ((bt>0) and (phil<angle(1,mt)) and (angle(1,mt)<phir)) or ((bt<0) and (phir<angle(-1,-mt)) and (angle(-1,-mt)<phil)):
         x0:=whatever; y0:=whatever;
	 z0=(directionpoint (1,mt) of if bt>0:(subpath (0,1) of p)else: reverse(subpath (0,1) of p)fi ) shifted (sx,0);
	 fi
  xpos:=sx+bt;
enddef;

def fahne (expr hl, hr, bt, phil, phir, tens, sx) =
  if not wortende:
 schweif(hl, hr, bt, phil, phir, tens, sx);
else:
  xpos:=sx;
fi  
enddef;

def Fahne=
fahne((0),ht-Delta,rho+((ht)-Delta)/m,0,angle(1,m),2,xpos);
enddef;

def Schulter=
bogen(ht/4,(ht),(5ht/6),ht/kh,0.8,90,-90,1.2,1,xpos);
  enddef;

      def fuss(expr sx)=
     draw ((0,2rho)..{right}(2*rho,0)) shifted (sx,0);
     xpos:=sx+2*rho;
   enddef;

   def kurzstrich(expr sx)=
  draw ((0,ht-Delta)--(Delta/m,ht)) shifted (sx,0);
  xpos:=sx+Delta/m;
enddef;

   def s_kurzstrich(expr sx)=
  draw ((0,ht-Delta)--(s_kurzstrichbreite,ht)) shifted (sx,0);
  xpos:=sx+s_kurzstrichbreite;
enddef;

   def spitz(expr sx)=
%     draw ((0,ht-Delta){1,m}..{up}(eta,ht)) shifted (sx,0);
%     xpos:=sx+eta;
     xpos:=sx;
   enddef;

   def ipunkt(expr sx)=
  draw ((0,dd)--(0,-dd))  shifted (sx,ht+dt/2);
     enddef;

   def umlaut(expr dx,sx)=
  draw ((-dd,dd)--(-dd,-dd))  shifted (sx+dx,ht+dt/2);
  draw ((dd,dd)--(dd,-dd))  shifted (sx+dx,ht+dt/2);
  xpos:=sx;
enddef;

def latin_eb (expr sx)=
schweif((ht),ht/2,12.5ut,angle(1,-mfahne),0,1,sx);
latin_e_lig(xpos);
enddef;

def latin_e_form (expr sx)=
  draw ((0,he){1,me}..(ebreite,ht-ebreite*ef){up}..(ebreite*(1-ef),ht){left}..(0,ht/2){down}..{right}(ebreite*(1-ef),0)) shifted (sx,0);
  xpos:=sx+ebreite*(1-ef);
enddef;

% lat. e nach r
def latin_er (expr sx)=
schweif((ht),ht/2,10ut,-90,0,1,sx);
latin_e_lig(xpos);
enddef;

def a_char=
oval(xpos);
stamm((ht),(rho),xpos);
stock(xpos);
enddef;

def o_char=
  oval(xpos);
  ohr;
  fahne((ht),ht-Delta,ofahne,angle(1,-mfahne),angle(1,m),1,xpos);
enddef;

def u_char=
spitz(xpos);
usack;
stamm((ht),(rho),xpos);
stock(xpos);
enddef;

def usack=
  stamm((ht),ht/4,xpos);
bogen(ht/4,(0),(5ht/6),ht/kh,0.3,-90,90,1,1.2,xpos);
enddef;

def vsack=
  stamm((ht),ht/4,xpos);
  bogen(ht/4,(0),2ht/3,ht/kh,0.3,-90,90,1,1.3,xpos);
  schweif(2ht/3,(ht),-einroll,90,180,2,xpos);
enddef;

       def c_form (expr sx)=
	         x1:=whatever;y1:=whatever;
    x2:=whatever;y2:=whatever;
    x3:=whatever;y3:=whatever;
    x4:=whatever;y4:=whatever;
    x5:=whatever;y5:=whatever;
    x6:=whatever;y6:=whatever;
    x7:=whatever;y7:=whatever;
    x8:=whatever;y8:=whatever; 
  z1=(0,b); z2=(-a,0); z3=(0,-b); z4=(a,0);
  z5=(-sigma*a,sigma*b);
  z6=(-sigma*a,-sigma*b);
  z7=(sigma*a,-sigma*b);
  z8=(sigma*a,sigma*b);
  draw ( z8{z1-z4}.. z1{left}.. z5{z2-z1}..z2{down}.. z6{z3-z2}..
    {right} z3) shifted (sx+a-delta,b);
    xpos:=sx+a-delta;
  enddef;

    def ohr=
  draw (z1--(x1-(ht-y1)/mfahne,ht));
  xpos:=x1-(ht-y1)/mfahne;
enddef;

  def spazierstock (expr sx)=
    draw ((0,ht-Delta){1,m}..(Delta+rho,ht){right}..{down}(Delta+2*rho,ht-rho)) shifted (sx,0);
    xpos:=sx+Delta+2*rho;
  enddef;

    def s_form(expr sx)=
    p:= ((0,ht){down}..(sbreite,sbreite){down}..(0,0){left}..(-sbreite+1ut,2ut));
    draw p shifted (sx,0);
    x0:=whatever;y0:=whatever;
    z0=(directionpoint(1,mt) of (reverse (subpath (1,2) of p))) shifted (sx,0);
      xpos:=sx+sbreite;
    Kern:=sbreite;
  enddef;

    def tangente (expr ddx)=
    if not wortende:
      draw ((x0,y0){1,mt}..{1,m}(x0+ddx+(ht-Delta-y0)/m,ht-Delta));
      mt:=m;
      xpos:=x0+ddx+(ht-Delta-y0)/m;
    fi
  enddef;

  def t_schlinge(expr sx)=
    draw ((0,0){up}..(-6.5ut,4ut){down}..{right}(0,0)) shifted (sx,0);
    xpos:=sx;
  enddef;

         def gerade (expr ha, hb, bt, sx)=
	 draw ((0,ha)--(bt,hb)) shifted (sx,0);
	 xpos:=sx+bt;
       enddef;

         def k_form(expr sx)=
    draw ((0,ht/3){up}..(1.5*a,ht){right}..(2*a,0.8*ht){down}..tension1.2..{left}(dd,ht/2)) shifted (sx,0);
        draw ((dd,ht/2){1,-1.13}..tension2.5..{right}(2*a-rho,0)) shifted (sx,0);
xpos:=sx+2*a-rho;
enddef;

def ss_form (expr sx)=
      bogen(ht,(Ht-D),ht+dt/2,11ut,0.6,90,-90,1,1,xpos);
      schweif(ht+dt/2,ht,-11ut+dd,-90,-180,1,xpos);
      schweif(ht,ht/2,15ut-dd,0,-90,0.95,xpos);
      bogen(ht/2,(0),(dd),-15ut+dd,0.5,-90,135,1,1,xpos);
  xpos:=sx+15ut;
  Kern:=7.5ut;
enddef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Makros fuer Grossbuchstaben
   def V_kurzstrich(expr sx)=
  draw ((0,Ht-D)--(D/m,Ht)) shifted (sx,0);
  xpos:=sx+D/m;
enddef;

def Umlaut (expr dx,sx)=
  draw ((-dd,dd)--(-dd,-dd))  shifted (sx+dx,Ht+dumlaut);
  draw ((dd,dd)--(dd,-dd))  shifted (sx+dx,Ht+dumlaut);
  xpos:=sx;
enddef;

     def Flanke(expr sp,bt,sx)=
  mA:=(Ht)/bt;
  draw ((0,R)..(0.6sp,0){right}..tension1..{1,mA}(sp+R/mA,R)--(sp+bt,Ht)) shifted (sx,0);
  xpos:=sx+sp+bt;
enddef;

def Horn(expr sx)=
  draw ((0,Ht)--(Sporn,Ht)) shifted (sx,0);
  xpos:=sx+Sporn;
enddef;

def Bauch(expr bt,ha,hb,flach,sx)=
  p:= ((0,ha)--(flach,ha){bt/abs(bt),0}..(bt,(ha+hb)/2){down}..{-bt/abs(bt),0}(flach,hb)--(0,hb));
  draw p shifted (sx,0);
  if bt>0:
    x0:=whatever; y0:=whatever;
    z0 = (directionpoint  (-1,-mt) of (subpath (2,3) of p)) shifted (sx,0);
    fi
  xpos:=sx;
enddef;



       def C_form (expr sx)=
	         x1:=whatever;y1:=whatever;
    x2:=whatever;y2:=whatever;
    x3:=whatever;y3:=whatever;
    x4:=whatever;y4:=whatever;
    x5:=whatever;y5:=whatever;
    x6:=whatever;y6:=whatever;
    x7:=whatever;y7:=whatever;
    x8:=whatever;y8:=whatever; 
  z1=(0,B); z2=(-A,0); z3=(0,-B); z4=(A,0);
  z5=(-sigma*A,sigma*B);
  z6=(-sigma*A,-sigma*B);
  z7=(sigma*A,-sigma*B);
  z8=(sigma*A,sigma*B);
  draw ((s,B-dd).. z1{left}.. z5{z2-z1}..z2{down}.. z6{z3-z2}..
    {right} z3) shifted (sx+A,B);
    xpos:=sx+A;
  enddef;

  def A_char=
     Flanke(sporn,Abreite,xpos);
     stamm((Ht),(0),xpos);
     gerade((ht-Delta-eta),(ht-Delta-eta),22.5ut,xpos-22.5ut);
%     schweif((ht-Delta),(ht-Delta-eta),22.5ut,-45,0,1.8,xpos-22.5ut);
     fahne((ht-Delta-eta),ht-Delta,11ut,0,angle(1,m),2.5,xpos);
   enddef;

   def O_char=
     aalt:=a;balt:=b;
     a:=A; b:=B;
     mt:=0.1;
     oval(xpos+delta);
     a:=aalt;b:=balt;
tangente(10.5ut);
     enddef;

   def U_char=
     V_kurzstrich(xpos);
     stamm((Ht),ht/2,xpos);
     bogen(ht/2,(0),ht,Hbreite,0.4,-90,90,1,1,xpos);
     stamm((Ht),(rho),xpos);
     stock(xpos);
   enddef;

   def S_form(expr sx)=
     p:=((Bbreite'/2,Ht-D)..(0,Ht){left}..(-Bbreite'/2,ht+dt/2){down}..(Bbreite'/2,ht/2){down}..(0,0){left}..(-Bbreite'/2,D));
     draw p shifted (sx+Bbreite'/2,0);
     x0:=whatever; y0:=whatever;
     z0 = (directionpoint (-1,-mt) of (subpath (3,4) of p)) shifted (sx+Bbreite'/2,0);
     xpos:=sx+Bbreite';
   enddef;

      def latin_e_lig (expr sx)=
 draw ((0,ht/2){right}..(ebreite,ht-ebreite*ef){up}..(ebreite*(1-ef),ht){left}..(0,ht/2){down}..{right}(ebreite*(1-ef),0)) shifted (sx,0);
  xpos:=sx+ebreite*(1-ef);
enddef;

   def latin_eA (expr sx)=
     Flanke(sporn,Abreite,sx);
     stamm((Ht),(0),xpos);
     schweif(ht,ht/2,32ut,angle(1,-3/4),0,1,xpos-sporn-Abreite+15ut);
     latin_e_lig(xpos);
   enddef;

         % echte Ligatur "Fe" auf oct"014"
   def latin_eF (expr sx)=
stamm((Ht),(0),xpos+7.5ut);
gerade((Ht),(Ht),22.5ut,xpos-7.5ut);
     schweif(ht,ht/2,21ut,angle(1,-1),0,1,xpos-20ut);
     latin_e_lig(xpos);
   enddef;

            % echte Ligatur "He" auf oct"015"
   def latin_eH (expr sx)=
     stamm((Ht),(0),xpos+Sporn);
          stamm((Ht),(0),xpos+Hbreite);
     schweif(ht,ht/2,34.5ut,angle(1,-1),0,1,xpos-Sporn-Hbreite);
     latin_e_lig(xpos);
   enddef;
%%%%%%%%%%%%%%%%%%%%%%%%%%
% Makros fuer Ziffern und Sonderzeichen
def komma (expr ha,sx)=
draw ((0,0){down}--(0,-s)) shifted (sx,ha);
enddef;

          def quer(expr ha, sx)=
       draw ((-s+dd/2,0)--(s-dd/2,0)) shifted (sx,ha);
     enddef;
%%%%%%%%%%%%%%%%%%%%%% Ende Makros %%%%%%%%%%%%%%%%%%%%%%%%%%%
