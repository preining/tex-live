%\iffalse        This is file `cuisine.dtx'
%%
%% Recipe typesetting package `cuisine' by Ben Cohen.
%% cuisine.dtx
%% Copyright 2000 Ben Cohen
%
% This program may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.2
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.2 or later is part of all distributions of LaTeX 
% version 1999/12/01 or later.
%
% This program consists of the files cuisine.dtx and cuisine.ins
%
%  $Id: cuisine.dtx,v 1.73 2000/08/01 00:16:15 bjc23 Exp bjc23 $
% ------------------------------------------------ 
%
%<*driver>
\documentclass{ltxdoc}
\usepackage{cuisine}
\GetFileInfo{cuisine.sty} % defines \filename, \filedate, \fileversion, 
                          % \fileinfo from cuisine.sty
\begin{document}
  \def\myemailaddress{\texttt{benc@cus.org.uk}}
  \title{The \texttt{cuisine} package}
  \author{Ben Cohen}
  \date{\filedate, \fileversion}
  \maketitle
  \DocInput{cuisine.dtx}
\end{document}
%</driver> 
%\fi
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% \CheckSum{549} 
%
% \begin{abstract}
% This package provides an environment for typesetting recipes in steps
% in which each ingredient is on the left of the page next to the
% method step where it is used.
% \end{abstract}
% 
% \MakeShortVerb{\|}
% \section{Introduction}
% There appear to be two styles of typesetting recipes in general use.  The
% more common (at least in recipe books in the UK)
% is where the ingredients appear at the top above the method, 
% like in the class |macros/latex/contrib/other/recipe|. Another
% way is to have each ingredient next to the method step in which it appears, 
% as in the package |macros/latex/contrib/supported/cooking|; 
% the |cuisine| package also uses this style, but closer to the format in 
% \textit{Practical Professional Cookery} by H.~L.\ Cracknell and R.~J.\
% Kaufmann.
%
% \section{Usage}
% The package is loaded using the header |\usepackage{cuisine}|;  the
% available options are:
% \begin{itemize}
% \item{|number|}\ \ Recipes will be numbered sequentially to the left of the 
% recipe title.  (This is the
% default.) The recipe \emph{steps} will always be numbered.
% \item{|nonumber|}\ \ Recipes will not be numbered.
% \item{|index|}\ \ Recipe titles will be written to the contents for the
% document.
% \item{|noindex|}\ \ Recipe titles will not be written to the document contents.
% (This is the default.)
% \end{itemize}
% You will need the package |nicefrac|; 
% it comes
% with current versions of te\TeX\ and is also 
% available from CTAN as part of the |units| package in 
% |macros/latex/|\hskip0cm|contrib/supported/|\hskip0cm|units|.
% 
%
% \medskip
% \DescribeEnv{recipe}
% Each recipe is set out in a |recipe| environment.  The three
% parameters are the title, and two recipe description fields---it is assumed
% that they are used for the number of servings and the preparation time, for
% example:
% \begin{quote}
%   |\begin{recipe}{|\textit{Title}|}|%
%               |{|\textit{Number of servings}|}|%
%               |{|\textit{Preparation time}|}|
%               \\\dots\\|\end{recipe}|
% \end{quote}
% There is no reason why the descriptive fields cannot be used for other
% things (such as the number of calories).
% 
% \medskip
% \DescribeMacro{\ingredient}
% \DescribeMacro{\ing}
% Within the environment, you type in method details directly, and you
% type the ingredients using |\ingredient| or the shorter version |\ing|
% \textit{before} the method step beside which you want the ingredient to
% appear.  A new method step is started whenever there is method text before
% an |\ingredient|.  Two consecutive |\ingredient|
% declarations will appear next to the same method step.
%
% The |\ingredient| command is used as follows:
% \begin{quote}
%   |\ingredient{|\textit{Quantity}|}|%
%               |{|\textit{Ingredient}|}|
% \end{quote}
% There is an optional first parameter which allows you to separate the 
% numerical quantity from the name of the unit of measurement:
% \begin{quote}
%   |\ingredient[|\textit{Numerical quantity}|]|%
%               |{|\textit{Unit of measurement}|}|%
%               |{|\textit{Ingredient}|}|
% \end{quote}
% Examples include:
% \begin{quote}
%   |\ingredient{a pinch}{salt}|\\
%   |\ingredient[4]{oz}{sugar}|
% \end{quote}
%
% \medskip
% \DescribeMacro{\fr}
% Within the recipe you can use |\fr| to typeset fractions using
% |\fr12| to get \nicefrac12, or |\fr{11}{12}| to typeset \nicefrac{11}{12}.
% If the numerator or denominator of the fraction is more than one digit long
% then you will need to enclose it in braces otherwise they are optional.
%
% \DescribeMacro{\degrees}
% \DescribeMacro{\0}
% A degree symbol can be obtained using |\degrees| or the shorter form |\0|;
% for example |120\0| gives 120${}^\circ$.
% 
% \section{More advanced usage}
% The |cuisine| package has been designed so that it is easy to vary the
% widths of the columns.  \LaTeX\ has very wide margins by default 
% so the text width is very narrow which may be unsuitable for
% typesetting recipes using this package.  (It might be worth looking at the
% KOMA-Script classes in 
% |macros/latex/|\hskip0pt|contrib/supported/|\hskip0pt|koma-script/| as an
% alternative to the standard \LaTeX\ classes.)
%
% \DescribeMacro{\RecipeWidths}
% The |\RecipeWidths| command is designed to be used in conjunction with the
% \LaTeX\ commands for changing the page layout.  It is used as follows:
% \begin{quote}
%   |\RecipeWidths{|\textit{Total recipe width}|}|%
%               |{|\textit{Recipe number width}|}|%
%               |{|\textit{Number of servings width}|}|%
%               |{|\textit{Ingredient width}|}|%
%               |{|\textit{Quantity width}|}|%
%               |{|\textit{Units width}|}|
% \end{quote}
%
% \medskip
% \DescribeMacro{\ResetRecipeCounter}
% The |\ResetRecipeCounter| command will reset the recipe counter so that 
% subsequent recipes are numbered from 1.
%
% \medskip
% \DescribeMacro{\newstep}
% Within the recipe environment, to force a new step if you do not want 
% to declare any ingredients, use the
% |\newstep| command.  (A |\newstep| command with no preceding ingredient or
% method text will be ignored to prevent completely empty steps from
% appearing.)
%
% \DescribeMacro{\newpage}
% |\newpage| ends the current step (as for |\newstep|) but also tells the 
% environment that the next step will appear on
% the next page. 
% Page breaks can only
% occur between method steps; method steps will not be split across pages.
% It is not normally necessary to use this command since page breaking after
% each step will be done automatically by \LaTeX\ as necessary.
%
% \medskip
% \DescribeMacro{\freeform}
% To typeset text across the whole width of the recipe instead of just the
% width of the method, use the |\freeform| command.  This is equivalent to 
% |\noalign| in tables.  Freeform text is not given a step number.
%
% \section{Fonts}
% The fonts used in the different parts of a recipe can be altered by
% changing the definition of the following macros; this should be done
% using, for example, \verb|\renewcommand*{\recipetitlefont}{\sffamily}|.
%
% \begin{tabular}{ll}
% \verb|\recipefont|&The default font used for the fonts below\\
% \verb|\recipetitlefont|&The font for the recipe title\\
% \verb|\recipeservingsfont|&The font for the number of servings\\
% \verb|\recipetimefont|&The font for the preparation time\\
% \verb|\recipenumberfont|&The font for the recipe number\\
% \verb|\recipestepnumberfont|&The font for each step number\\
% \verb|\recipequantityfont|&The font for the numerical quantity\\
% \verb|\recipeunitfont|&The font for the unit of measurement\\
%   & (or descriptive quantity)\\
% \verb|\recipeingredientfont|&The font for the ingredient\\
% \verb|\recipemethodfont|&The font for the method details\\
% \verb|\recipefreeformfont|&The font for freeform descriptions\\
% \end{tabular}
%
% \section{Examples}
% \subsection{A simple example}
% There is only whitespace between the first three 
% ingredients, so they are used for the first step with
% the method text which follows.  (Had
% there been text before the first ingredient, that text would become the first
% step and the heating instruction would be the second step.)  The fourth
% |\ingredient| signals the end of the method text and starts a new step.  The
% final |\end{recipe}| finishes that step.
% \medskip
%
% \begin{small}\noindent
% |\begin{recipe}{Yorkshire Pudding}{4 portions}{1\fr12 hours}|\\
% |\ingredient[\fr12]{pt}{milk}|\\
% |\ingredient[2]{oz}{butter}|\\
% |\ingredient[5]{oz}{self-raising flour}|\\
% |Heat the milk and butter until nearly boiling.  Add flour and allow to|\\
% |seeth over.|\\
% |\ingredient[3]{}{eggs}|\\
% |\ingredient{to taste}{salt and pepper}|\\
% |Add the remaining eggs and whisk again.  Cook at 220\0C for about 1 hour.|\\
% |\end{recipe}|
% \end{small}
% \medskip
%
% \begin{recipe}{Yorkshire Pudding}{4 portions}{1\fr12 hours}
% \ingredient[\fr12]{pt}{milk}
% \ingredient[2]{oz}{butter}
% \ingredient[5]{oz}{self-raising flour}
% Heat the milk and butter until nearly boiling.  Add flour and
% allow to seeth over.
% \ingredient[3]{}{eggs}
% \ingredient{to taste}{salt and pepper}
% Add the remaining eggs and whisk again.  Cook at 220\0C
% for about 1 hour.
% \end{recipe}
% 
% \subsection{A more complex example}
% We can change the widths of the columns using, for example,
% \begin{quote}
% |\RecipeWidths{.75\textwidth}{0.5cm}{3cm}{1.75cm}{.75cm}{.75cm}|
% \end{quote}
% \noindent and the |small| environment to produce a different layout.
% We can also change the fonts, for example:
% \begin{quote}
% |\renewcommand*{\recipetitlefont}{\large\bfseries\sffamily}|\\
% |\renewcommand*{\recipequantityfont}{\sffamily\bfseries}|\\
% |\renewcommand*{\recipeunitfont}{\sffamily}|\\
% |\renewcommand*{\recipeingredientfont}{\sffamily}|\\
% |\renewcommand*{\recipefreeformfont}{\itshape}|
% \end{quote}
%
% The example below demostrates the |\newstep| and |\newpage| commands, and
% also how to use |\freeform| to create freeform text.  |\freeform| can also
% be used to create rules in the same way as when using |\noalign| in tables.
% In this case, recipe numbering has been turned off, and the |small|
% environment is being used.
%
% \medskip
% \begin{small}\noindent
% |\begin{recipe}{Zabaglione alla Marsala}{4 Portions}{\fr12 hour}|\\
% |\freeform This is a well-known Italian recipe which is |\\
% |great for piling on the calories.|\\
% |\ingredient[6]{}{egg yolks}|\\
% |\ingredient[2]{oz}{granulated sugar}|\\
% |\ingredient[6--8]{tbsp}{Marsala (or sherry)}|\\
% |\ingredient[\fr14]{oz}{gelatine}|\\
% |\ingredient[2]{tbsp}{cold water}|\\
% |In the top of a double boiler, combine the egg yolks with sugar and|\\
% |Marsala, and whip the mixture over hot, but not boiling, water until it|\\
% |thickens. Stir in gelatine, softened in cold water and dissolved over hot|\\
% |water.|\\
% |\ingredient[3]{tbsp}{brandy}|\\
% |\ingredient[\fr38]{pt}{double cream}|\\
% |Put the pan in a bowl of ice and stir the |\\
% |zabaglione well until it is thick and free of bubbles.  When it is almost |\\
% |cold, fold in brandy and whipped cream and pour into individual moulds.|\\
% |\freeform\rule{\textwidth}{0.05pt}\newpage|\\
% |\freeform\rule{\textwidth}{0.05pt}|\\
% |\ingredient[3]{}{egg yolks}|\\
% |\ingredient[1]{oz}{granulated sugar}|\\
% |\ingredient[3--4]{tbsp}{Marsala (or sherry)}|\\
% |\ingredient[1\fr12]{tbsp}{brandy}|\\
% |To make the sauce, combine egg yolks and sugar in the top of a double |\\
% |saucepan.  Whisk mixture over hot, but not boiling, water until the|\\
% |sauce coats |\\
% |the back of a spoon.  Stir in the Marsala and brandy.|\\
% |\newstep|\\
% |Chill the zabaglione, unmould it, and serve with the sauce immediately.|\\
% |\freeform\hrulefill|\\
% |\end{recipe}|
% \medskip
% \begin{center}
% \renewcommand*{\recipetitlefont}{\large\bfseries\sffamily}
% \renewcommand*{\recipequantityfont}{\sffamily\bfseries}
% \renewcommand*{\recipeunitfont}{\sffamily}
% \renewcommand*{\recipeingredientfont}{\sffamily}
% \renewcommand*{\recipefreeformfont}{\itshape}
% \RecipeWidths{.75\textwidth}{0.5cm}{3cm}{1.75cm}{.75cm}{.75cm}
% \makeatletter\numb@ringfalse\makeatother ^^A Cheap trick
%
% \begin{recipe}{Zabaglione alla Marsala}{4 Portions}{\fr12 hour}
% \freeform This is a well-known Italian recipe which is 
% great for piling on the calories.
% \ingredient[6]{}{egg yolks}
% \ingredient[2]{oz}{granulated sugar}
% \ingredient[6--8]{tbsp}{Marsala (or sherry)}
% \ingredient[\fr14]{oz}{gelatine}
% \ingredient[2]{tbsp}{cold water}
% In the top of a double boiler, combine the egg yolks 
% with sugar and Marsala, and whip the mixture over hot, but not 
% boiling, water until it thickens.  Stir in gelatine, softened in cold water 
% and dissolved over hot water.
% \ingredient[3]{tbsp}{brandy}
% \ingredient[\fr38]{pt}{double cream}
% Put the pan in a bowl of ice and stir the 
% zabaglione well until it is thick and free of bubbles.  When it is almost 
% cold, fold in brandy and whipped cream and pour into individual moulds.
% \freeform\rule{\textwidth}{0.05pt}\newpage
% \freeform\rule{\textwidth}{0.05pt}
% \ingredient[3]{}{egg yolks}
% \ingredient[1]{oz}{granulated sugar}
% \ingredient[3--4]{tbsp}{Marsala (or sherry)}
% \ingredient[1\fr12]{tbsp}{brandy}
% To make the sauce, combine egg yolks and sugar in the top of a double 
% saucepan.  Whisk mixture over hot, but not boiling, water until the
% sauce coats 
% the back of a spoon.  Stir in the Marsala and brandy.
% \newstep
% Chill the zabaglione, unmould it, and serve with the sauce immediately.
% \freeform\hrulefill
% \end{recipe}
% \end{center}\end{small}
%
% \section{Bugs, Issues, Features, \dots}
% \begin{itemize}
% \item{Vertical spacing of the boxes:  the ingredients are not uniformly
% spaced and nor are the method steps.  I currently
% have no idea how to solve this problem.  (It is better than it was before I 
% added a few extra |\strut|s but if you compare the baselines of the
% ingedients to the adjacent method baselines, they are still not all properly
% aligned.)}
% \item{Vertical alignment of the recipe title: see |\r@cipetitle| on page
% \pageref{rectitlealign}.}
% \item{Support for dual sets of quantities, for example using a command like
% \verb|\ingredient{1}{oz}{25}{g}{butter}|.  This could be used where two
% systems of measurement are used or for different numbers of servings.  The
% display would then have to have two extra columns or else (more likely) 
% there would have to be an option to select which one to display.}
% \end{itemize}
% Please e-mail me at \myemailaddress\ if you can help find or solve any 
% problems with this package.
% 
% \StopEventually{}
% \newpage
% \setlength{\parindent}{0cm}
%
% \section{Identification section and initialisation}
% The package declaration follows the standard form.
%    \begin{macrocode}
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{cuisine}[2000/08/01 v0.5 recipe typesetting]
%    \end{macrocode}
%
% We use the |nicefrac| package for ``nice'' fractions.
%    \begin{macrocode}
\RequirePackage{nicefrac}
%    \end{macrocode}
%
%^^A ------------------------------------------------
% \section{Counters, booleans and lengths}
%
% |r@cipenumber| is the recipe number counter.
%                  This could be made dependent on, say, the section number.
%    \begin{macrocode}
\newcounter{r@cipenumber}
%    \end{macrocode}
%
% \begin{macro}{\ResetRecipeCounter}
% The |\ResetRecipeCounter| macro resets the recipe number counter to 0.
%    \begin{macrocode}
\DeclareRobustCommand{\ResetRecipeCounter}{%
  \setcounter{r@cipenumber}{0}%
}
%    \end{macrocode}
% \end{macro}
%
% |st@pnumber| is the step counter within recipes.
%                  It is zeroed whenever the recipe counter is incremented.
%    \begin{macrocode}
\newcounter{st@pnumber}[r@cipenumber]  
%    \end{macrocode}
%
% |ingr@dnumber| is the ingredient counter within recipes and steps.
%                  It is zeroed whenever the step counter is incremented.
%    \begin{macrocode}
\newcounter{ingr@dnumber}[st@pnumber]  
%    \end{macrocode}
%
%\medskip
% |ifnumb@ring| determines whether or not recipe numbers are printed. 
%                  (Step numbers will always be printed.)
%    \begin{macrocode}
\newif\ifnumb@ring
%    \end{macrocode}
%
% |ifind@xing| determines whether we are 
% writing the recipes to an index file or not.
%    \begin{macrocode}
\newif\ifind@xing
%    \end{macrocode}
%
% |iffr@eforming| determines whether or not we are in freeform mode.
%    \begin{macrocode}
\newif\iffr@eforming
%    \end{macrocode}
%
% |ifn@wpaging| determines whether or not the next step will be on a new
% page.
%    \begin{macrocode}
\newif\ifn@wpaging
%    \end{macrocode}
%
% |\R@cipeWidth| is the total width of the recipe.
%    \begin{macrocode}
\newlength{\R@cipeWidth}
%    \end{macrocode}
%
% The following are, respectively, the widths of the recipe number box, the 
% recipe title box, the recipe number-of-servings box, and the total of the
% recipe title and number-of-servings boxes.  
%    \begin{macrocode}
\newlength{\R@cipeNumberWidth}
\newlength{\R@cipeTitleWidth}
\newlength{\R@cipeServingsWidth}
\newlength{\R@cipeTandSWidth}
%    \end{macrocode}
%
% This is the distance ingredient (etc.) paragraphs are outdented.
%    \begin{macrocode}
\newlength{\R@cipeOutdent}
%    \end{macrocode}
%
% These are the vertical distances for the recipe title-to-rule adjustments 
% (usually negative).
%    \begin{macrocode}
\newlength{\R@cipeTitleVerticalAdjustTop}
\newlength{\R@cipeTitleVerticalAdjustBot}
%    \end{macrocode}
%
% These are the widths of the ingredient descriptors and combinations of
% them.
%    \begin{macrocode}
\newlength{\R@cipeIngredientWidth}
\newlength{\R@cipeQuantityWidth}
\newlength{\R@cipeUnitsWidth}
\newlength{\R@cipeQandUWidth}
\newlength{\R@cipeIandUWidth}
\newlength{\R@cipeIQUWidth}
%    \end{macrocode}
%
% |\R@cipeMethodWidth| is the width of the method text.
%    \begin{macrocode}
\newlength{\R@cipeMethodWidth}
%    \end{macrocode}
%
% |\R@cipeStepWidth| is the width of everything but the step number.
%    \begin{macrocode}
\newlength{\R@cipeStepWidth}
%    \end{macrocode}
%
%\medskip
% The following macros are to set the lengths defined above.
%
% \begin{macro}{\R@cipeMethodWidths}
% |\R@cipeMethodWidths| is to set the widths of the boxes/columns used in the
% recipe steps.
% There are three parameters:  |#1| is the ingredient width, 
% |#2| the quantity width, |#3| the units width.  
% The other lengths below are deduced
% from these and |\R@cipeMethodWidth|.  
% The |0.5cm| values are to leave a little space between the columns.
%    \begin{macrocode}
\DeclareRobustCommand*{\R@cipeMethodWidths}[3]{%
  \setlength{\R@cipeIngredientWidth}{#1}%
  \setlength{\R@cipeQuantityWidth}{#2}%
  \setlength{\R@cipeUnitsWidth}{#3}%
  \setlength{\R@cipeQandUWidth}{\R@cipeQuantityWidth}%
  \addtolength{\R@cipeQandUWidth}{\R@cipeUnitsWidth}%
  \setlength{\R@cipeIQUWidth}{\R@cipeQandUWidth}%
  \addtolength{\R@cipeIQUWidth}{\R@cipeIngredientWidth}%
  \addtolength{\R@cipeIQUWidth}{0.5cm}%
  \setlength{\R@cipeIandUWidth}{\R@cipeIQUWidth}%
  \addtolength{\R@cipeIandUWidth}{-\R@cipeQuantityWidth}%
  \setlength{\R@cipeStepWidth}{\R@cipeWidth}%
  \addtolength{\R@cipeStepWidth}{-\R@cipeNumberWidth}%
  \setlength{\R@cipeMethodWidth}{\R@cipeStepWidth}%
  \addtolength{\R@cipeMethodWidth}{-\R@cipeIngredientWidth}%
  \addtolength{\R@cipeMethodWidth}{-\R@cipeQandUWidth}%
  \addtolength{\R@cipeMethodWidth}{-0.5cm}%
%    \end{macrocode}
%  Allow for the ingredient paragraph outdenting.
%    \begin{macrocode}
  \addtolength{\R@cipeIngredientWidth}{-\R@cipeOutdent}
  \addtolength{\R@cipeUnitsWidth}{-\R@cipeOutdent}
  \addtolength{\R@cipeQandUWidth}{-\R@cipeOutdent}
}%
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{\R@cipeTitleWidths}
% |\R@cipeTitleWidths| is to set the widths of the boxes/columns used in
% the recipe title.
%   There are two parameters:  |#1| is the recipe number width, 
% |#2| the number of servings width.
% The title width is deduced from there.  The method widths are also updated
% with the new recipe number width.
%    \begin{macrocode}
\DeclareRobustCommand*{\R@cipeTitleWidths}[2]{%
  \setlength{\R@cipeNumberWidth}{#1}%
  \setlength{\R@cipeServingsWidth}{#2}%
  \setlength{\R@cipeTitleWidth}{\R@cipeWidth}%
  \addtolength{\R@cipeTitleWidth}{-\R@cipeNumberWidth}%
  \addtolength{\R@cipeTitleWidth}{-\R@cipeServingsWidth}%
  \setlength{\R@cipeTandSWidth}{\R@cipeServingsWidth}%
  \addtolength{\R@cipeTandSWidth}{\R@cipeTitleWidth}%
  \R@cipeMethodWidths{\R@cipeIngredientWidth}{\R@cipeQuantityWidth}%
                     {\R@cipeUnitsWidth}%
}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{\RecipeWidths}
% |\RecipeWidths| combines the above macros in a useable form.  There are six
% parameters:
% |#1| is the recipe width, |#2| the recipe number width, |#3| the number
%of servings width, 
% |#4| the ingredient width, |#5| the quantity width, |#6| the units width.
% The order of the three commands here is very important.
%    \begin{macrocode}
\DeclareRobustCommand*{\RecipeWidths}[6]{%
  \setlength{\R@cipeWidth}{#1}%
  \R@cipeTitleWidths{#2}{#3}%
  \R@cipeMethodWidths{#4}{#5}{#6}%
}%
%    \end{macrocode}
% \end{macro}
%
%\medskip
% Now we set up the default values.
%     These appear to be reasonable for the LaTeX default page layout
%     (A4, portrait, one column, etc.)
%     I don't think the LaTeX default width is good for a recipe book though!
%    \begin{macrocode}
\setlength{\R@cipeOutdent}{0.3cm}%
\setlength{\R@cipeTitleVerticalAdjustTop}{-0.25cm}
\setlength{\R@cipeTitleVerticalAdjustBot}{-0.04cm}
\RecipeWidths{\textwidth}{0.8cm}{3cm}{3.5cm}{1cm}{1.7cm}
%    \end{macrocode}
%
%^^A ------------------------------------------------
%
% \section{Fonts}
% This is where the default fonts are set up.
% \verb|\recipefont| is the default font for the other recipe fonts; thus
% the other fonts inherit from this but can be altered individually.
%    \begin{macrocode}
\newcommand*\recipefont{\normalfont}
\newcommand*\recipetitlefont{\recipefont}
\newcommand*\recipenumberfont{\recipefont}
\newcommand*\recipestepnumberfont{\recipefont}
\newcommand*\recipequantityfont{\recipefont}
\newcommand*\recipeunitfont{\recipefont}
\newcommand*\recipeingredientfont{\recipefont}
\newcommand*\recipemethodfont{\recipefont}
\newcommand*\recipeservingsfont{\recipefont}
\newcommand*\recipetimefont{\recipefont}
\newcommand*\recipefreeformfont{\recipefont}
%    \end{macrocode}
%
%
%^^A ------------------------------------------------
% \section{Boxes}
% |\st@pingrbox| is to hold the ingredients cumulatively as each
% |\ingredient| command is processed; |\st@pingrtmpbox| is used as a
% temporary transfer box.
%    \begin{macrocode}
\newsavebox{\st@pingrbox}
\newsavebox{\st@pingrtmpbox}
%    \end{macrocode}
% This is used to hold a single ingredient as it is being processed:
%    \begin{macrocode}
\newsavebox{\st@pIQUbox}
%    \end{macrocode}
% This is to hold the method (or freeform) text for the step:
%    \begin{macrocode}
\newsavebox{\st@pmethodbox}
%    \end{macrocode}
%
% ^^A------------------------------------------------
% \section{Options}
%
% |nonumber| --- Should recipes be numbered?     (The default is yes.)
%    \begin{macrocode}
\DeclareOption{number}{\numb@ringtrue}
\DeclareOption{nonumber}{\numb@ringfalse}
%    \end{macrocode}
%
% |index|  --- Should recipes be indexed to a file?      (The default is no.)
%    \begin{macrocode}
\DeclareOption{index}{\ind@xingtrue}
\DeclareOption{noindex}{\ind@xingfalse}
%    \end{macrocode}
%
% Process the options, using the defaults if necessary.
%    \begin{macrocode}
\ExecuteOptions{number,noindex}
\ProcessOptions\relax
%    \end{macrocode}
%
%^^A ------------------------------------------------
% \section{The main macros}
% \subsection{Macros for displaying the steps and title}
%
%\begin{macro}{\Displ@ySt@p}
% |\Displ@ySt@p| displays the ingredients and method for the current step
%                  and resets things ready for the next step.
%    \begin{macrocode}
\DeclareRobustCommand{\Displ@ySt@p}{%
%    \end{macrocode}
% First, increment the step counter and put extra vertical space between steps.
% (But it isn't uniform yet, unfortunately.)
%    \begin{macrocode}
  \stepcounter{st@pnumber}%
%    \end{macrocode}
% Display this step.
%    \begin{macrocode}
  \makebox[\R@cipeWidth]{%
    \makebox[\R@cipeNumberWidth][l]{\recipestepnumberfont\arabic{st@pnumber}}%
    \usebox{\st@pingrbox}%
    \usebox{\st@pmethodbox}%
  }%
%    \end{macrocode}
% Finally, clear the step storage boxes for the next step.
%    \begin{macrocode}
  \savebox{\st@pingrbox}[\R@cipeIQUWidth]{}
}%
%    \end{macrocode}
%\end{macro}
%
%\begin{macro}{\Fr@eFormStep}
% |\Fr@eFormStep| displays a free form description; it works in the same way
% as |\Displ@ySt@p|.
%    \begin{macrocode}
\DeclareRobustCommand{\Fr@eFormStep}{%
  \usebox{\st@pmethodbox}%
  \savebox{\st@pmethodbox}[\R@cipeMethodWidth]{}%
}%
%    \end{macrocode}
%\end{macro}
%
%\begin{macro}{\DisplaySt@p}
%  This calls one of the two display routines above,
% depending on whether we are freeforming 
%  or not.
%    \begin{macrocode}
\DeclareRobustCommand{\DisplaySt@p}{%
  \iffr@eforming%
    \Fr@eFormStep%
  \else%
    \Displ@ySt@p%
  \fi%
  \ifn@wpaging%
    \recipen@wpage%
  \else%
    \vskip0.2cm%
  \fi%
  \n@wpagingfalse%
}%
%    \end{macrocode}
%\end{macro}
%
%\begin{macro}{\r@cipetitle}
%  |\r@cipetitle| displays the recipe title constrution, given the parameters
%  |#1| the title, |#2| the number of servings and |#3| the preparation time.
%
%  \label{rectitlealign}
%  Note: I want to get the recipe number aligned with the TOP of the title
%  in the case of multi-line titles.
%    \begin{macrocode}
\DeclareRobustCommand{\r@cipetitle}[3]{

  \bigskip
  \pagebreak[0]
%    \end{macrocode}
% The following displays the text line above the rule:
%    \begin{macrocode}
  \mbox{%
    \ifnumb@ring%
      \makebox[\R@cipeNumberWidth][l]{\recipenumberfont\arabic{r@cipenumber}}%
      \parbox[b]{\R@cipeTitleWidth}{\recipetitlefont #1}%
    \else%
      \parbox[b]{\R@cipeTitleWidth}{\recipetitlefont #1}%
      \makebox[\R@cipeNumberWidth]{}%
    \fi%
    \parbox[b]{\R@cipeServingsWidth}{\hfill\recipeservingsfont #2}%
  }\par%
%    \end{macrocode}
% Next we display the rule, being careful not to allow 
% page breaks during the title, using the adjustments as specified above.
%    \begin{macrocode}
  \nopagebreak   
  \vspace{\R@cipeTitleVerticalAdjustTop}%
  \nopagebreak
  \rule{\R@cipeWidth}{0.4pt}\par%
  \nopagebreak
  \vspace{\R@cipeTitleVerticalAdjustBot}%
  \nopagebreak
%    \end{macrocode}
% Finally we display the text line below the rule:
%    \begin{macrocode}
  \makebox[\R@cipeWidth][r]{\recipetimefont #3}\par%
  \nopagebreak
}%
%    \end{macrocode}
%\end{macro}
%
%
% \subsection{Macros for the method box}
% These macros are to input the method box |\st@pmethodbox|, and are hence
% called at the start and end of the method box, or equivalently the
% \emph{end} and \emph{start} of the ingredient (etc.) macros.
%
%\begin{macro}{\r@cipesloppy}
%  This is a version of \sloppy which doesn't cause problems with the 
%  definition!  (The \LaTeX\ definition has a problem where the empty box
%  detection in |\pr@ingred| fails.)
%    \begin{macrocode}
\def\r@cipesloppy{%
  \tolerance 9999%
  \emergencystretch 3em%
  \hfuzz.5pt%
  \vfuzz.5pt%
%    \end{macrocode}
%  This is not in |\sloppy|:  avoid warning on underfull boxes, which are
%  more common since the columns are very narrow.
%  This value may need to be adjusted...
%    \begin{macrocode}
  \hbadness 1500%   
}%
%    \end{macrocode}
%\end{macro}
%
%\begin{macro}{\pr@ingred}
%  This macro is called at the start of ingredients (that is, the 
% end of the method box).  It first ends the minipage--box which was started
% in the previous |\p@stingred|.
%    \begin{macrocode}
\DeclareRobustCommand{\pr@ingred}{%
  \endminipage\end{lrbox}%
%    \end{macrocode}
% Here we compare the width of the |\st@pmethodbox| with zero to determine
% whether anything was entered into it.
% (The idea for determining whether box is empty was from
% Donald Arseneau, |comp.text.tex|.)
%    \begin{macrocode}
  \ifdim\wd\st@pmethodbox=0in%
%    \PackageWarning{cuisine}{No text}%  % use for testing
  \else%
%    \PackageWarning{cuisine}{Method text}%
    \DisplaySt@p%
  \fi%
}%
%    \end{macrocode}
%\end{macro}
%
%\begin{macro}{\p@stingred}
%  And this macro is called at the end of ingredients (the start of the method
%  box).
%
%  This is very odd.  Using |\begin{lrbox}|...|\end{lrbox}| isn't
%  supposed to work properly over an environment definition (and doesn't
%  seem to, either).  So we use |\lrbox|...|\endlrbox|.  But we want to nest
%  a |\minipage| inside this, which fails.  But it does work with 
%  |\begin{lrbox}\minipage|...|\endminipage\end{lrbox}|.
%
%  \centerline{WHY?!}
%
%  What happens, anyway, is that a new minipage--box is started.  Provided
%  the user doesn't put anything other than whitespace and comments between
%  this and the next |\pr@ingred| then the box will be empty.  This is how
%  |\p@stingred| knows whether to display the step or not when called by
%  |\ingr@dient|.
%    \begin{macrocode}
\DeclareRobustCommand{\p@stingred}{%
  \fr@eformingfalse%
  \begin{lrbox}{\st@pmethodbox}\minipage[t]{\R@cipeMethodWidth}%
  \recipemethodfont%
  \noindent%
  \ignorespaces%
  \r@cipesloppy%
}%
%    \end{macrocode}
%\end{macro}
%
%\subsection{Step terminating macros}
% These macros call |\pr@ingred| and |\p@stingred|, and 
% can (through |\pr@ingred|) cause the next step to be started.
%
%\begin{macro}{\m@thodend}
%  The |\m@thodend| command (which is |\newstep| in the environment) forces
%  a new step unless there is no method \textbf{and} no ingredient.
%    \begin{macrocode}
\DeclareRobustCommand{\m@thodend}{%
  \endminipage\end{lrbox}%
  \ifdim\wd\st@pmethodbox=0in%
    \ifnum\value{ingr@dnumber}>0%
%    \end{macrocode}
% In this case, the method box is empty so we put something in it to avoid
% problems with alignment.
%    \begin{macrocode}
      \savebox{\st@pmethodbox}[\R@cipeMethodWidth]{\mbox{}}%
      \DisplaySt@p%
    \fi%
  \else%  
    \DisplaySt@p%
  \fi%
  \p@stingred%
}%
%    \end{macrocode}
%\end{macro}
%
%\begin{macro}{\r@cipen@wpage}
% |\r@cipen@wpage| is a macro which sets |\n@wpagingtrue|---so that the next
% step will appear on the next page---and then forces the next step.
%    \begin{macrocode}
\DeclareRobustCommand\r@cipen@wpage{\global\n@wpagingtrue\m@thodend}
%    \end{macrocode}
%\end{macro}
%
%\begin{macro}{\fr@eform}
%  |\fr@eform| is for freeform text,  like |\noalign| in tables.
% Again, as for |\m@thodend|, we want to force the new step unless there is 
% no method and no ingredient.
%    \begin{macrocode}
\DeclareRobustCommand{\fr@eform}{%
  \endminipage\end{lrbox}%
  \ifdim\wd\st@pmethodbox=0in%
    \ifnum\value{ingr@dnumber}>0%
      \savebox{\st@pmethodbox}[\R@cipeMethodWidth]{\mbox{}}%
       \DisplaySt@p%
    \fi%
  \else%  
    \DisplaySt@p%
  \fi% 
%    \end{macrocode}
% The rest is like |\p@stingred|:
%    \begin{macrocode}
  \fr@eformingtrue% 
  \begin{lrbox}{\st@pmethodbox}\minipage[t]{\R@cipeWidth}%
  \recipefreeformfont%
  \noindent%
  \ignorespaces%
  \r@cipesloppy%
}%
%    \end{macrocode}
% \end{macro}
%
%\begin{macro}{\ingr@dient}
%  The |\ingr@dient| command takes 3 parameters, one optional.  |#3| is the
%  ingredient.  If |#1| is non-blank then |#1| is the numerical quantity and 
% |#2| the unit of measure, otherwise |#2| is the quantity.  Start by ending the
% method box.
%    \begin{macrocode}
\DeclareRobustCommand{\ingr@dient}[3][]{%
  \pr@ingred%
%    \end{macrocode}
%
% The following is the main bit to typeset the ingredients list.
% (Thanks to Ulrike Fischer, |comp.text.tex|, for help with aligning box 
%  baselines.)
%
% This is a hack to see if |#1| is empty (Patrick Guio, |comp.text.tex|).
%    \begin{macrocode}
  \ifx\relax#1\relax%
%    \end{macrocode}
% If it is empty we typeset |#2| in the whole QandU width.  We align the top of
% the ingredient box with the bottom of the quantity box but have the
% baseline as the top line of the quantity box.  Use ragged right because the
% column is too narrow for flush right.
%    \begin{macrocode}
    \savebox{\st@pIQUbox}[\R@cipeIQUWidth][t]{%
      \parbox[t]{\R@cipeIQUWidth}{%
        \lineskip0pt\mbox{}\\[-\baselineskip]%
        \rule{\R@cipeOutdent}{0cm}%
        \parbox[b]{\R@cipeQandUWidth}{%
          \raggedright\recipeunitfont%
          \setlength{\parindent}{-\R@cipeOutdent}%
%    \end{macrocode}
% This sees whether the text is large enough to fit on one line, in which case
% it centres it; otherwise it formats it sensibly.
%
% Note: the |\hfill| matches another one elsewhere to centre the line.
%    \begin{macrocode}
          \savebox{\st@pingrtmpbox}{#2}%
          \ifdim\wd\st@pingrtmpbox>\R@cipeQandUWidth%
            \rule{0pt}{\baselineskip}%
            \strut #2\strut%
          \else%
            \noindent%
            \rule{0pt}{\baselineskip}%
            \strut #2\hfill\strut%
          \fi%
        }%
        \rule{0.2cm}{0cm}%
        \rule{\R@cipeOutdent}{0cm}%
        \parbox[t]{\R@cipeIngredientWidth}{\raggedright%
          \recipeingredientfont%
          \setlength{\parindent}{-\R@cipeOutdent}%
          \strut #3\strut}%
      }%
    }%
  \else
%    \end{macrocode}
% If |#1| was not empty, typeset |#1| and |#2| separately.  First align the top of
% the ingredient box with the bottom of the unit box but have the
% baseline as the top line of the unit box; then repeat with that box below
% the quantity box.
%    \begin{macrocode}
    \savebox{\st@pingrtmpbox}[\R@cipeIandUWidth][t]{%
      \parbox[t]{\R@cipeIandUWidth}{%
        \lineskip0pt\mbox{}\\[-\baselineskip]%
        \rule{\R@cipeOutdent}{0cm}%
        \parbox[b]{\R@cipeUnitsWidth}{%
          \raggedright\recipeunitfont%
          \setlength{\parindent}{-\R@cipeOutdent}%
          \rule{0pt}{\baselineskip}%
          \strut #2\strut\hfill}%
        \rule{0.2cm}{0cm}%
        \rule{\R@cipeOutdent}{0cm}%
        \parbox[t]{\R@cipeIngredientWidth}{%
          \raggedright\recipeingredientfont%
          \setlength{\parindent}{-\R@cipeOutdent}%
          \strut #3\strut}%
      }%
    }% 
    \savebox{\st@pIQUbox}[\R@cipeIQUWidth][t]{%
      \parbox[t]{\R@cipeIQUWidth}{%
        \lineskip0pt\mbox{}\\[-\baselineskip]%
        \parbox[b]{\R@cipeQuantityWidth}{\rule{0pt}{\baselineskip}%
          \hfill %
          \raggedright\recipequantityfont%
          \strut #1\strut%
          \rule{0.1cm}{0cm}}%
        \usebox{\st@pingrtmpbox}%
      }%
    }%
  \fi%
%    \end{macrocode}
% Use |\st@pingrtmpbox| as a temporary holding box:
%    \begin{macrocode}
  \savebox{\st@pingrtmpbox}[\R@cipeIQUWidth][t]{\usebox{\st@pingrbox}}%
  \savebox{\st@pingrbox}[\R@cipeIQUWidth][t]{%
    \begin{minipage}[t]{\R@cipeIQUWidth}%
        \ifnum\value{ingr@dnumber}>0%
          \usebox{\st@pingrtmpbox}\par%
        \fi%        
        \usebox{\st@pIQUbox}\strut%
    \end{minipage}%
  }%
%  \usebox{\st@pingrbox}    % For testing: show cumulative boxes.
  \stepcounter{ingr@dnumber}%
  \p@stingred%
}%
%    \end{macrocode}
%\end{macro}
%
%^^A ------------------------------------------------
%\section{The recipe environment}
%\begin{environment}{recipe}
%This is the main environment in the package.  Its 3 parameters are 
%|#1| is the recipe title, |#2| is the number of portions, and |#3| the preparation
%time.  Of course, |#2| and |#3| can be whatever you like but that is one way of
%using them.
%    \begin{macrocode}
\newenvironment{recipe}[3]{%
%    \end{macrocode}
% These are the things that are put at the start of the recipe environment.
% First, set things up.  Increment the recipe counter, make command aliases
% and empty the boxes.
%    \begin{macrocode}
  \stepcounter{r@cipenumber}
  \let\newstep\m@thodend
  \let\recipen@wpage\newpage
  \let\newpage\r@cipen@wpage
  \let\0\d@grees
  \let\degrees\d@grees
  \let\fr\fr@ction
  \let\ing\ingr@dient
  \let\ingredient\ingr@dient
  \let\freeform\fr@eform
  \n@wpagingfalse%
  \setlength{\parindent}{0pt}
  \savebox{\st@pingrbox}[\R@cipeIQUWidth]{}
  \savebox{\st@pmethodbox}[\R@cipeMethodWidth]{}
%    \end{macrocode}
% Next deal with the index entry if we need to.
%    \begin{macrocode}
  \ifind@xing
     \addcontentsline{toc}{subsection}{#1}     
  \fi
%    \end{macrocode}
% Display the title and start the method box.
%    \begin{macrocode}
  \r@cipetitle{#1}{#2}{#3}
  \vskip0.2cm%
  \p@stingred%
}%
{% 
%    \end{macrocode}
% These are the things put at the end of recipes.  
% End the method box and deal with the last step.  Give a warning if the
% recipe is empty.
%  It is apparently not necessary to reset the earlier assignments and
%  |\parindent|.
%    \begin{macrocode}
  \pr@ingred%
  \ifnum\value{st@pnumber}=0%  then complain!
    \PackageWarning{cuisine}{The recipe did not have any steps}%
  \fi%
  
  \pagebreak[0]%  
  \medskip%
%    \end{macrocode}
% This prevents indentation problems after the end of the environment.
%    \begin{macrocode}
  \@endpetrue%
}%
%    \end{macrocode}
%\end{environment}
%
%
%^^A ------------------------------------------------
%\section{Miscellaneous useful macros}
%
% \begin{macro}{\d@grees}
%This is to typeset a degrees symbol.
%    \begin{macrocode}
\DeclareRobustCommand{\d@grees}{%
  ${}^\circ$%
}%
%    \end{macrocode}
%\end{macro}
%
%\begin{macro}{\fr@ction}
% This is to typeset fractions, currently using package |nicefrac|.
%    \begin{macrocode}
\DeclareRobustCommand{\fr@ction}[2]{%
  \nicefrac#1#2%
}%
%    \end{macrocode}
%\end{macro}
%
%^^A ------------------------------------------------
%^^A That's it...
%
% \Finale
\endinput%
%^^A END OF FILE
%^^A ------------------------------------------------
