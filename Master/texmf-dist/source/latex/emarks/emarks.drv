%%
%% This is file `emarks.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% emarks.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% 
%% emarks : 2011/03/26 v1.0 - e-TeX named marks registers (FC)
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% 
%% This work consists of the main source file emarks.dtx
%% and the derived files:
%%                 emarks.sty, emarks.ins, emarks.drv,
%%         and:                emarks.pdf
%% 
%% emarks : 2011/03/26 v1.0 - e-TeX named marks registers (FC)
%% Copyright (C) 2011 by FC <florent.chervet @t free.fr>
%% 
\def\thisinfo {e-TeX named marks registers (FC)}
\def\thisversion {1.0}
\PassOptionsToPackage {full}{tabu}
\RequirePackage [\detokenize{��},hyperlistings]{fcltxdoc}
\AtBeginDocument{\embedfile{README}}
%%\CheckDates{interfaces=2011/02/12,tabu=2011/02/25}
\documentclass[a4paper,11pt,twoside,american,latin1,T1]{ltxdoc}     \usetikz{full}
\usepackage [latin1]{inputenc}
\usepackage [T1]{fontenc}
\usepackage {numprint}
\usepackage {pdfcomment}
\usepackage {ragged2e}   % general tools
\usepackage {arial,bbding,relsize,moresize,manfnt,pifont,upgreek} % fonts
\csname endofdump\endcsname
\usepackage {emarks}
\RequirePackage [full]{tabu}
\usepackage {geometry}
\AtBeginDocument {\let\setkeys \kvsetkeys }
\let\microtypeYN=n
\ifx y\microtypeYN                                                          %
   \usepackage[expansion=all,stretch=20,shrink=60]{microtype}\fi            % font (microtype)
\CodelineNumbered\lastlinefit999
\lstset{backgroundcolor=\color{LightYellow},
texcsstyle=\color{blue},
moretexcs=[1]{
    lstdefinestyle,
    lstinputlisting,lstset,tikzlabel,tikzrefXY,
    color,
    geometry,lasthline,firsthline,
    cmidrule,toprule,bottomrule,tabusetup*,tabusetup,
    everyrow,tabulinestyle,tabureset,savetabu,usetabu,preamble,
    taburulecolor,taburowcolors},
keywordstyle=[3]{\color{black}\bfseries},
morekeywords=[3]{&},
keywordstyle=[4]{\color{red}\bfseries},
morekeywords=[4]{\linegoal,$},
keywordstyle=[5]{\color{blue}\bfseries},
keywordstyle=[6]{\color{green}\bfseries},
keywordstyle=[7]{\color{yellow}\bfseries},
alsoletter={&},alsoletter={*},alsoletter={$},
morekeywords=[5]{blue},
morekeywords=[6]{green},
morekeywords=[7]{yellow},
}
\hypersetup {%
  pdfauthor=Florent CHERVET,
  pdfkeywords={TeX, LaTeX, e-TeX, marks, firstmarks, botmarks, topmarks, package },
}
\geometry {top=0mm,headheight=8mm,includehead,reversemarginpar,asymmetric,headsep=3mm,bottom=14mm,footskip=5mm,inner=35mm,outer=20mm }
\begin{document}
   \DocInput{\jobname.dtx}
\end{document}
\endinput
%%
%% End of file `emarks.drv'.
