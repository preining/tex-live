%%
%% This is file `mylatexformat.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% mylatexformat.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% 
%% mylatexformat : 2011/02/12 v3.4 - Use mylatexformat.ltx to make a format based on the preamble of any LaTeX file
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% 
%% This work consists of the main source file mylatexformat.dtx
%% and the derived files
%%    mylatexformat.ltx, mylatexformat.pdf, mylatexformat.ins
%% 
%% mylatexformat - Use mylatexformat.ltx to make a format based on the preamble of any LaTeX file
%% Copyright (C) 2010-2011 by Florent Chervet <florent.chervet@free.fr>
%% 
\edef\thisfile{\jobname}
\def\thisinfo{Make a format based on the preamble of any \texorpdfstring{\LaTeX{}}{LaTeX} file}
\def\thisdate{2011/02/12}
\def\thisversion{3.4}
\let\loadclass\LoadClass
\def\LoadClass#1{\loadclass[abstracton]{scrartcl}\let\scrmaketitle\maketitle\AtEndOfClass{\let\maketitle\scrmaketitle}}
{\makeatletter{\endlinechar`\^^J\obeyspaces
 \gdef\ErrorUpdate#1=#2,{\@ifpackagelater{#1}{#2}{}{\let\CheckDate\errmessage\toks@\expandafter{\the\toks@
        \thisfile-documentation: updates required !
              package #1 must be later than #2
              to compile this documentation.}}}}%
 \gdef\CheckDate#1{{\let\CheckDate\relax\toks@{}\@for\x:=#1\do{\expandafter\ErrorUpdate\x,}\CheckDate\expandafter{\the\toks@}}}}
\AtBeginDocument{\DeleteShortVerb{\|}\CheckDate{interfaces=2011/01/26,tabu=2011/01/26}}
\PassOptionsToPackage{svgnames}{xcolor}
\PassOptionsToPackage{hyperfootnotes}{hyperref}
\documentclass[a4paper,oneside]{ltxdoc}
\usepackage[latin1]{inputenc}
\usepackage[american]{babel}
\usepackage[T1]{fontenc}
\usepackage{etex,etoolbox,geometry,lastpage,xspace,xcolor,bbding,txfonts,atveryend,moresize,relsize}
\usepackage{tocloft,titlesec,graphicx,fancyhdr,framed,multirow,makecell,tabu,enumitem,needspace}
\usepackage{holtxdoc,bookmark,embedfile,tabu,keycommand}
\usepackage{microtype}
\usepackage{interfaces}
\usetikz{basic}
\csname endofdump\endcsname
\CodelineNumbered
\usepackage{fancyvrb}\fvset{gobble=1,listparameters={\topsep=0pt}}
\lastlinefit999
\geometry{top=0pt,includeheadfoot,headheight=7mm,headsep=.6cm,bottom=.6cm,footskip=.5cm,left=4cm,right=14mm}
\hypersetup{%
  pdftitle={The mylatexformat package},
  pdfsubject={Use mylatexformat.ltx to make a format based on the preamble of any LaTeX file},
  pdfauthor={Florent CHERVET},
  colorlinks,linkcolor=reflink,
  pdfstartview={FitH},
  pdfkeywords={tex, e-tex, latex, package, format, formats, pdfTeX},
  bookmarksopen=true,bookmarksopenlevel=2}
\embedfile{\thisfile.dtx}
\begin{document}
   \DocInput{\thisfile.dtx}
\end{document}
\endinput
%%
%% End of file `mylatexformat.drv'.
