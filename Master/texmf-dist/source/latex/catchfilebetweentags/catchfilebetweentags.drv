%%
%% This is file `catchfilebetweentags.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% catchfilebetweentags.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% 
%% catchfilebetweentags : 2011/02/19 v1.1 - catchfilebetweentags : catch file between delimiters or tags
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% 
%% This work consists of the main source file catchfilebetweentags.dtx
%% and the derived files
%%    catchfilebetweentags.sty, catchfilebetweentags.pdf, catchfilebetweentags.ins,
%% 
%% catchfilebetweentags : catchfilebetweentags : a new dimen corresponding to the remainder of the line
%% Copyright (C) 2010 by Florent Chervet <florent.chervet@free.fr>
%% 
\edef\thisfile{\jobname}
\def\thisinfo{catch file between delimiters or tags}
\def\thisdate{2011/02/19}
\def\thisversion{1.1}
\def\CTANbaseurl{http://www.ctan.org/tex-archive/macros/latex}
\def\CTANdisplay{CTAN:macros/latex}
\makeatletter\protected\def\CTANhref{\@ifstar\CTANhrefstar\CTANhrefnost}\makeatother
\newcommand*\CTANhrefstar[3][/contrib/]{\href{\CTANbaseurl#1#2}{#3}}
\newcommand*\CTANhrefnost[2][/contrib/]{\href{\CTANbaseurl#1#2}{\nolinkurl{\CTANdisplay#1#2}}}
\let\loadclass\LoadClass
\def\LoadClass#1{\loadclass[abstracton]{scrartcl}\let\scrmaketitle\maketitle\AtEndOfClass{\let\maketitle\scrmaketitle}}
{\makeatletter{\endlinechar`\^^J\obeyspaces
 \gdef\ErrorUpdate#1=#2,{\@ifpackagelater{#1}{#2}{}{\let\CheckDate\errmessage\toks@\expandafter{\the\toks@
        \thisfile-documentation: updates required !
              package #1 must be later than #2
              to compile this documentation.}}}}%
 \gdef\CheckDate#1{{\let\CheckDate\relax\toks@{}\@for\x:=\thisfile=\thisdate,#1\do{\expandafter\ErrorUpdate\x,}\CheckDate\expandafter{\the\toks@}}}}
\AtBeginDocument{\CheckDate{interfaces=2011/02/19,tabu=2011/02/19}}
\PassOptionsToPackage{svgnames}{xcolor}
\PassOptionsToPackage{hyperfootnotes=true}{hyperref}
\documentclass[a4paper,oneside]{ltxdoc}
\AtBeginDocument{\DeleteShortVerb{\|}}
\usepackage[latin1]{inputenc}
\usepackage[american]{babel}
\usepackage[T1]{fontenc}
\usepackage{ltxnew,etoolbox,geometry,graphicx,xcolor,needspace,ragged2e}   % general tools
\usepackage{lmodern,bbding,hologo,relsize,moresize,manfnt,pifont,upgreek}  % fonts
\usepackage[official]{eurosym}                                             % font
\usepackage{xspace,tocloft,titlesec,fancyhdr,lastpage,enumitem,marginnote} % paragraphs & pages management
\usepackage{holtxdoc,bookmark,hypbmsec,enumitem-zref}                      % hyper-links
\usepackage{array,delarray,longtable,colortbl,multirow,makecell,booktabs}  % tabulars
\usepackage{bbding,embedfile,framed,txfonts,catchfile}
\usepackage{interfaces}
\usepackage{tabu}
\csname endofdump\endcsname
\CodelineNumbered
\usepackage{fancyvrb}
\usepackage{catchfilebetweentags}
\lastlinefit999
\geometry{top=0pt,includeheadfoot,headheight=.6cm,headsep=.6cm,bottom=.6cm,footskip=.5cm,left=4cm,right=1.5cm}
\hypersetup{%
  pdftitle={The catchfilebetweentags package},
  pdfsubject={catch file between delimiters or tags},
  pdfauthor={Florent CHERVET},
  colorlinks,linkcolor=reflink,
  pdfstartview={FitH},
  hyperindex=true,
  pdfkeywords={tex, e-tex, latex, package, catchfilebetweentags, catchfile, docstrip},
  bookmarksopen=true,bookmarksopenlevel=2}
\usepackage{bookmark}
\embedfile{\thisfile.dtx}
\begin{document}
   \DocInput{\thisfile.dtx}
\end{document}
\endinput
%%
%% End of file `catchfilebetweentags.drv'.
