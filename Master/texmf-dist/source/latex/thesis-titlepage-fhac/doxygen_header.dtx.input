% ^^A -*- latex -*-
% \def\docdate {2006/04/01} ^^A not style'able!!
%
% \section{Doxygen-Header}
%
% Falls jemand die Beschreibung der Programmierschnittstelle mit
% \texttt{doxygen} generiert, hat er mit diesem Beispiel-Dateikopf die
% M�glichkeit dort auch die gleiche Titelseite zu verwenden.
%
% 
%    \begin{macrocode}
%<*doxyheader>
% doxygen_header.tex
\documentclass[a4paper,
11pt,titlepage,abstracton,german,twoside,bibtotocnumbered,idxtotoc,%glototoc
liststotoc, pointlessnumbers,openright]{scrreprt}

%TimesNewRoman Schrift, dazu braucht man auch
%den entsprechenden Zeichensatz f�r den Math-Mode:
\usepackage{mathptmx}
\usepackage[scaled = .9]{helvet}
\usepackage{courier}

%Einstellung der Textma�e zu den R�ndern
\usepackage[left=20mm,bottom=30mm]{geometry}
%Deutsche Trennungen, Anf�hrungsstriche und mehr:
\usepackage{ngerman}
%\usepackage[german]{babel}

%Eingabe von �,�,�,� erlaubt:
\usepackage[latin1]{inputenc}

\usepackage{float}
\restylefloat{figure}
\restylefloat{table}

%Liefert viele Tools f�r Math-Mode: 
\usepackage{amsmath}

%Erg�nzung zu "amsmath":
\usepackage{amsthm}

%Zum Einbinden von Grafiken:
\usepackage{graphicx}
\usepackage[dvips]{epsfig}
\graphicspath{{./bilder}}

%F�r TeX Zeichen in beliebigen eingebundenen Grafiken:
\usepackage{psfrag}

%F�r sch�ne Darstellung von Algorithmen:
\usepackage[german, vlined, boxed]{algorithm2e}

%Ein Paket, das die Darstellung von "Text, wie
%er eingegeben wird"erlaubt: Also
%\begin{verbatim} \end{document} \end{verbatim}
%erzeugt die Ausgabe von 
%\end{document} im Typewriter-Style, und
%beendet nicht das Dokument 
\usepackage{makeidx,verbatim,array}
\makeindex
\usepackage{url}

\usepackage{scrpage}
\deftripstyle{Kopfzeile}{\rightmark}{}{\pagemark}{}{}{}
%Erm�glicht frei gestaltbare Kopf- und Fu�zeilen
\pagestyle{Kopfzeile}

%Nachweis der Abbildungen
\usepackage[
center, % Bilder grundsaetzlich zentrieren 
nocaptionlist
]{figbib_add}
% figbib = Style zum verwalten von Bildern in BibTeX-Dateien (_add Erg�nzungen durch J�rgen A.Lamers)
\fbTheFigs
\def\fbDirectory{bilder}

%Glossar erstellen
\usepackage{gloss}
\makegloss{}

%Definitionen f�r die "theorem" Umgebung von AMSTeX,
%die Umgebungen f�r Definition, Satz, Lemma etc.
%bereitstellen. 
%Bei dieser Setzung wird in jedem Kapitel
%neu mit der Numerierung begonnen, jedoch
%gibt es keine einzelnen counter f�r
%SDefinition, Satz, Lemma, etc., sondern es
%wird stets derselbe counter (der von "defi") verwendet:
\newtheorem{defi}{Definition}[chapter]
\newtheorem{theo}[defi]{Satz}
\newtheorem{expl}[defi]{Beispiel}
\newtheorem{lem}[defi]{Lemma}
\newtheorem{kor}[defi]{Korollar}
\newtheorem{bem}[defi]{Bemerkung}
\newtheorem{folg}[defi]{Folgerung}
\newtheorem{prop}[defi]{Proposition}

\usepackage[colored]{fhACtitlepage}
\subject{Beschreibung der Programmierschnittstelle}

\usepackage{fancyhdr} % fuer doxygen
\usepackage{doxygen}

% um links und refs auch als clickable zu machen
\usepackage{hyperref}


%Eine Setzung f�r die Proof-Umgebung von "amsthm",
%die leider nicht automatisch von "german" gemacht wird.
\renewcommand{\proofname}{Beweis}

\setcounter{tocdepth}{1}
\setcounter{secnumdepth}{2}
%\setlength{\footrulewidth}{0.4pt}
\makeindex
%Hiergeht's los:
\begin{document}
\maketitle
\clearemptydoublepage
\pagenumbering{roman}
\tableofcontents
\clearemptydoublepage
\pagenumbering{arabic}
%</doxyheader>
%    \end{macrocode}
\endinput
% End of file 'doxygen_header.dtx.input'
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End:
