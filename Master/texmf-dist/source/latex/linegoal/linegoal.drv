%%
%% This is file `linegoal.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% linegoal.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% 
%% linegoal : 2011/02/25 v2.9 - linegoal : a new dimen corresponding to the remainder of the line
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% 
%% This work consists of the main source file linegoal.dtx
%% and the derived files
%%    linegoal.sty, linegoal.pdf, linegoal.ins
%% 
%% linegoal : linegoal : a new dimen corresponding to the remainder of the line
%% Copyright (C) 2010 by Florent Chervet <florent.chervet@free.fr>
%% 
\let\microtypeYN=y
\edef\thisfile{\jobname}
\def\thisinfo{Measuring the remaining width of the line}
\def\thisdate{2011/02/25}
\def\thisversion{2.9}
\def\CTANbaseurl{http://www.ctan.org/tex-archive/}
\def\CTANhref#1#2{\href{\CTANbaseurl/help/Catalogue/entries/#1.html}{\nolinkurl{CTAN:help/Catalogue/entries/#1.html}}}
\let\loadclass\LoadClass
\def\LoadClass#1{\loadclass[abstracton]{scrartcl}\let\scrmaketitle\maketitle\AtEndOfClass{\let\maketitle\scrmaketitle}}
\PassOptionsToPackage{svgnames}{xcolor}
{\makeatletter{\endlinechar`\^^J\obeyspaces
 \gdef\ErrorUpdate#1=#2,{\@ifpackagelater{#1}{#2}{}{\let\CheckDate\errmessage\toks@\expandafter{\the\toks@
        \thisfile-documentation: updates required !
              package #1 must be later than #2
              to compile this documentation.}}}}%
 \gdef\CheckDate#1{{\let\CheckDate\relax\toks@{}\@for\x:=\thisfile=\thisdate,#1\do{\expandafter\ErrorUpdate\x,}\CheckDate\expandafter{\the\toks@}}}}
\AtBeginDocument{\CheckDate{interfaces=2011/02/12,tabu=2011/02/25}}
\documentclass[a4paper,oneside,american,latin1,T1]{ltxdoc}
\AtBeginDocument{\DeleteShortVerb{\|}}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hologo} % bug: must be loaded before graphicx...
\usepackage{ltxnew,etoolbox,geometry,graphicx,xcolor,needspace,ragged2e}   % general tools
\usepackage{lmodern,bbding,hologo,relsize,moresize,manfnt,pifont,upgreek}  % fonts
\usepackage[official]{eurosym}                                             % font
\ifx y\microtypeYN                                                         %
   \usepackage[expansion=all,stretch=20,shrink=60]{microtype}\fi           % font (microtype)
\usepackage{xspace,tocloft,titlesec,fancyhdr,lastpage,enumitem,marginnote} % paragraphs & pages management
\usepackage{holtxdoc,bookmark,hypbmsec,enumitem-zref}                      % hyper-links
\usepackage{array,delarray,longtable,colortbl,multirow,makecell,booktabs}  % tabulars
\usepackage{tabularx}\tracingtabularx                                      % tabularx
\usepackage{txfonts,framed}
\usepackage{interfaces}
\usepackage{nccfoots}
\usepackage[linegoal,delarray]{tabu}
\CodelineNumbered\lastlinefit999
\usepackage{embedfile}
\usepackage{fancyvrb}\fvset{gobble=1,listparameters={\topsep=0pt}}
\usepackage{listings}
\lstset{
    gobble=1,
    language=[LaTeX]TeX,
    basicstyle=\ttfamily,
    breaklines=true,
    upquote=true,
    backgroundcolor=\color[gray]{0.90},
    keywordstyle=\color{blue}\bfseries,
    keywordstyle=[2]{\color{ForestGreen}},
    commentstyle=\ttfamily\color{violet},
    keywordstyle=[3]{\color{black}\bfseries},
    keywordstyle=[4]{\color{red}\bfseries},
    keywordstyle=[5]{\color{blue}\bfseries},
    keywordstyle=[6]{\color{green}\bfseries},
    keywordstyle=[7]{\color{yellow}\bfseries},
    %extendedchars={true},
    alsoletter={&},
morekeywords=[1]{
    \lstdefinestyle,
    \lstinputlisting,\lstset,
    \color,
    \geometry,\lasthline,\firsthline,
    \cmidrule,\toprule,\bottomrule,
    \everyrow,\tabulinestyle,\tabureset,\savetabu,\usetabu,\preamble,
    \taburulecolor,\taburowcolors},
morekeywords=[2]{
    tabular,
    caption,
    table,
    tabu},
morekeywords=[3]{
  &},
morekeywords=[4]{\linegoal},
morekeywords=[5]{blue},
morekeywords=[6]{green},
morekeywords=[7]{yellow},
}
\csname endofdump\endcsname
\hypersetup{%
  pdftitle={The linegoal package},
  pdfsubject={A new dimen corresponding to the remainder of the line},
  pdfauthor={Florent CHERVET},
  colorlinks,linkcolor=reflink,
  pdfstartview=FitH,
  pdfkeywords={TeX, LaTeX, e-TeX, pdfTeX, package, zref, linegoal}}
\embedfile{\thisfile.dtx}
\geometry{top=0pt,headheight=.6cm,includehead,headsep=.6cm,bottom=1.4cm,footskip=.5cm,left=2.5cm,right=1cm}
\begin{document}
   \DocInput{\thisfile.dtx}
\end{document}
\endinput
%%
%% End of file `linegoal.drv'.
