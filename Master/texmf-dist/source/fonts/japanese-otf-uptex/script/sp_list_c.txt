%
% This file is generated from the data of UniGB-UTF32
% in cid2code.txt (Version 10/29/2010)
% for Adobe-GB1-5
%
% Reference:
%   http://sourceforge.net/adobe/cmap/home/Home/
%   cmapresources_gb1-5.tar.z
%
% A newer CMap may be required for some code points.
%


%Adobe-GB1-2
20087,20089,200CC,215D7,2298F,20509,2099D,241FE

% end
