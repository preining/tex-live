#!/bin/bash

THIS_FOLDER=$(pwd -P)
ROOT_FOLDER=$(cd -P -- "$(dirname -- "$THIS_FOLDER/../../../../")" && pwd -P)

cd $ROOT_FOLDER

export TEMP_FOLDER=$ROOT_FOLDER/tmp

mkdir -p $TEMP_FOLDER

cd $TEMP_FOLDER
rm *

# cp $ROOT_FOLDER/fonts/truetype/public/gentium/*.ttf $TEMP_FOLDER
cp $ROOT_FOLDER/fonts/afm/public/gentium/*.afm $TEMP_FOLDER
cp $ROOT_FOLDER/fonts/enc/dvips/gentium/*.enc $TEMP_FOLDER
# replace the file that is used during typesetting
cp $THIS_FOLDER/gentium-ec-source.enc $TEMP_FOLDER/gentium-ec.enc
cp $THIS_FOLDER/gentium-t5-source.enc $TEMP_FOLDER/gentium-t5.enc

for enc in ec texnansi l7x qx t5 t2a agr ; do
	echo "Generating files for encoding $enc"

	for f in *.afm ; do
		echo "> afm2pl -p gentium-${enc}.enc $f"
		afm2pl -p gentium-${enc}.enc $f
	done ;

	encoding_unicode=false
	case $enc in
		agr|t2a) encoding_unicode=true ;;
		*) encoding_unicode=false ;;
	esac

	mv GenI102.pl ${enc}-gentium-italic.pl
	mv GenR102.pl ${enc}-gentium-roman.pl
	if [ $encoding_unicode != true ]; then
		mv GenBasB.pl  ${enc}-gentiumbasic-bold.pl
		mv GenBasBI.pl ${enc}-gentiumbasic-bolditalic.pl
	else
		rm GenBasB.pl
		rm GenBasBI.pl
	fi

	for f in ${enc}-gentium*.pl ; do
		echo "> pltotf $f"
		pltotf $f
	done
	echo
done;

rm $ROOT_FOLDER/fonts/tfm/public/gentium/*.tfm
cp $TEMP_FOLDER/*.tfm $ROOT_FOLDER/fonts/tfm/public/gentium

cd $ROOT_FOLDER
