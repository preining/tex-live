% textalpha: Greek symbols in text
% ********************************
%
% :Copyright: © 2010 Günter Milde
% :Licence:   This work may be distributed and/or modified under the
%             conditions of the `LaTeX Project Public License`_, either
%             version 1.3 of this license or any later version.
%
% :Abstract:  Provide a set of ``\text*`` macros for Greek letters
%             outside math.
%
% :Identification:
%  ::

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{textalpha}
[2012/07/03 v0.2 macros for Greek letters in text]

%  This package is part of the lgrx_ bundle.
%
% .. note::
%
%    The package was renamed from `textgreek` to `textalpha` to prevent
%    confusion with the textgreek_ package by Leonard Michlmayr.
%
%
% Changelog:
%   .. class:: borderless
%
%   ==========  ===== =========================================================
%   2010-06-16  0.1   initial version
%   2012-06-27  0.2   support for compound Unicode definitions outside LGR
%   ==========  ===== =========================================================
%
% Motivation
% ==========
%
% By default, macros for Greek letters
%
% * are only valid in mathematical mode,
% * do not change shape (default shape is italic).
%
% In Greek text fonts with the LGR font encoding, letters are
% accessible by a Latin transcription.
%
% This package provides a set of macros that can be used to access Greek
% symbols from a font in LGR encoding (e.g. the `CB fonts`_) without the
% need for an explicite font-encoding switch.
%
% The macro names follow the established scheme to prepend ``\text*`` to
% the Adobe glyph name (if it exists) or the corresponding math-macro, so
% ``\textalpha`` -- ``\textOmega`` prints α – Ω.
%
% * In combination with the ``\text`` macro from amsmath, this can be
%   used to get matching upright and bold upright Greek symbols in math
%   as well.
%
% * In combination with ``\usepackage[utf8]{inputenc}`` and the
%   accompanying ``lgrenc.dfu`` file, Unicode input of Greek characters
%   is supported.
%
% This is done using NFSS default definitions as explained in the
% fntguide_ (section 5.3).
%
% Attention
%   While the macros work reasonably well for single letters, the automatic
%   font-encoding switches behind the doors interferes with the kerning
%   between the letters and replacement of accent+character with a
%   pre-composed character.
%
%   Therefore, Greek text should be written with the help of babel_, setting
%   the language to either ``greek`` or ``polutonikogreek`` or wrapped in the
%   provided ``\TextGreek`` macro.
%
%
% Implementation
% ==============
%
% Requirements
% ------------
%
% This package requires the extended font encoding definitions ::

\input{lgrxenc.def}

% TextGreek
% ---------
%
% The ``\TextGreek`` command can be used to ensure that its argument is set in
% a font encoding with support for Greek. We define LGR as default encoding for
% Greek text::

\DeclareTextCommandDefault{\TextGreek}[1]{{\fontencoding{LGR}\selectfont #1}}

% .. The [fntguide_] writes
%
%      Note that ``\DeclareTextAccentDefault`` can be used on any
%      one-argument encoding-specific command, not just those defined with
%      ``\DeclareTextAccent``.
%
%    However, the simpler alternative:
%    ``\DeclareTextAccentDefault{\TextGreek}{LGR}`` this fails with the table
%    of Unicode characters in the test document `<textalpha-test.tex>`__:
%
%      ``! You can't use a prefix with `end-group character }'.``
%
% Font encodings supporting Greek script (e.g. LGI) may declare a specific
% text command that passes the argument unchanged. This way, kerning and
% selection of precomposed glyphs work also for cases like
% ``\TextGreek{\'A}U``.
%
% .. Do this for PU here? ``\DeclareTextCommand{\TextGreek}{PU}[1]{#1}``
%
%    Rather not: Not required, as hyperref's PDF string preparation
%    ignores unknown commands.  Can lead to infinite recursion (``TeX capacity
%    exeeded``) without the ``unicode=true`` hyperref option.:
%
%
% Greek Alphabet
% --------------
%
% Define the ``text*`` marcos as default for all font encodings::

\DeclareTextSymbolDefault{\textAlpha}{LGR}
\DeclareTextSymbolDefault{\textBeta}{LGR}
\DeclareTextSymbolDefault{\textGamma}{LGR}
\DeclareTextSymbolDefault{\textDelta}{LGR}
\DeclareTextSymbolDefault{\textEpsilon}{LGR}
\DeclareTextSymbolDefault{\textZeta}{LGR}
\DeclareTextSymbolDefault{\textEta}{LGR}
\DeclareTextSymbolDefault{\textTheta}{LGR}
\DeclareTextSymbolDefault{\textIota}{LGR}
\DeclareTextSymbolDefault{\textKappa}{LGR}
\DeclareTextSymbolDefault{\textLambda}{LGR}
\DeclareTextSymbolDefault{\textMu}{LGR}
\DeclareTextSymbolDefault{\textNu}{LGR}
\DeclareTextSymbolDefault{\textXi}{LGR}
\DeclareTextSymbolDefault{\textOmicron}{LGR}
\DeclareTextSymbolDefault{\textPi}{LGR}
\DeclareTextSymbolDefault{\textRho}{LGR}
\DeclareTextSymbolDefault{\textSigma}{LGR}
\DeclareTextSymbolDefault{\textTau}{LGR}
\DeclareTextSymbolDefault{\textUpsilon}{LGR}
\DeclareTextSymbolDefault{\textPhi}{LGR}
\DeclareTextSymbolDefault{\textChi}{LGR}
\DeclareTextSymbolDefault{\textPsi}{LGR}
\DeclareTextSymbolDefault{\textOmega}{LGR}
%
\DeclareTextSymbolDefault{\textalpha}{LGR}
\DeclareTextSymbolDefault{\textbeta}{LGR}
\DeclareTextSymbolDefault{\textgamma}{LGR}
\DeclareTextSymbolDefault{\textdelta}{LGR}
\DeclareTextSymbolDefault{\textepsilon}{LGR}
\DeclareTextSymbolDefault{\textzeta}{LGR}
\DeclareTextSymbolDefault{\texteta}{LGR}
\DeclareTextSymbolDefault{\texttheta}{LGR}
\DeclareTextSymbolDefault{\textiota}{LGR}
\DeclareTextSymbolDefault{\textkappa}{LGR}
\DeclareTextSymbolDefault{\textlambda}{LGR}
\DeclareTextSymbolDefault{\textmu}{LGR}
\DeclareTextSymbolDefault{\textnu}{LGR}
\DeclareTextSymbolDefault{\textxi}{LGR}
\DeclareTextSymbolDefault{\textomicron}{LGR}
\DeclareTextSymbolDefault{\textpi}{LGR}
\DeclareTextSymbolDefault{\textrho}{LGR}
\DeclareTextSymbolDefault{\textsigma}{LGR}
\DeclareTextSymbolDefault{\textvarsigma}{LGR}
\DeclareTextSymbolDefault{\texttau}{LGR}
\DeclareTextSymbolDefault{\textupsilon}{LGR}
\DeclareTextSymbolDefault{\textphi}{LGR}
\DeclareTextSymbolDefault{\textchi}{LGR}
\DeclareTextSymbolDefault{\textpsi}{LGR}
\DeclareTextSymbolDefault{\textomega}{LGR}

% Additional Greek symbols
% """"""""""""""""""""""""
%
% Ancient Greek Numbers (Athenian Numerals)::

\DeclareTextSymbolDefault{\PiDelta}{LGR} % GREEK ACROPHONIC ATTIC FIFTY
\DeclareTextSymbolDefault{\PiEta}{LGR}   % GREEK ACROPHONIC ATTIC FIVE HUNDRED
\DeclareTextSymbolDefault{\PiChi}{LGR}   % GREEK ACROPHONIC ATTIC FIVE THOUSAND
\DeclareTextSymbolDefault{\PiMu}{LGR}    % GREEK ACROPHONIC ATTIC FIFTY THOUSAND
% \DeclareTextSymbolDefault{\pentedeka}{LGR}    % GREEK ACROPHONIC ATTIC FIFTY
% \DeclareTextSymbolDefault{\pentehekaton}{LGR} % GREEK ACROPHONIC ATTIC FIVE HUNDRED
% \DeclareTextSymbolDefault{\penteqilioi}{LGR}  % GREEK ACROPHONIC ATTIC FIVE THOUSAND
% \DeclareTextSymbolDefault{\pentemurioi}{LGR}  % GREEK ACROPHONIC ATTIC FIFTY THOUSAND

\DeclareTextSymbolDefault{\stigma}{LGR}        % ϛ
\DeclareTextSymbolDefault{\textstigmagreek}{LGR} % ϛ (puenc.def)
\DeclareTextSymbolDefault{\varstigma}{LGR}     % stigma variant (CB.enc, teubner)
% \DeclareTextSymbolDefault{\vardigamma}{LGR}  % digamma variant (greek.ldf)
\DeclareTextSymbolDefault{\koppa}{LGR}         % ϟ (greek small letter koppa)
\DeclareTextSymbolDefault{\textkoppagreek}{LGR}  % ϟ (puenc.def)
\DeclareTextSymbolDefault{\qoppa}{LGR}           % ϙ (archaic koppa)
\DeclareTextSymbolDefault{\Qoppa}{LGR}           % Ϙ (archaic Koppa)
\DeclareTextSymbolDefault{\Stigma}{LGR}          % ϹΤ ligature (teubner)
\DeclareTextSymbolDefault{\textStigmagreek}{LGR} % ϹΤ ligature (puenc.def)
\DeclareTextSymbolDefault{\Sampi}{LGR}           % Ϡ
\DeclareTextSymbolDefault{\textSampigreek}{LGR}  % Ϡ (puenc.def)
\DeclareTextSymbolDefault{\sampi}{LGR}           % ϡ
\DeclareTextSymbolDefault{\textsampigreek}{LGR}  % ϡ (puenc.def)
\DeclareTextSymbolDefault{\anoteleia}{LGR}     % ·
\DeclareTextSymbolDefault{\erotimatiko}{LGR}   % ;
% \digamma used by amsmath!
\DeclareTextSymbolDefault{\ddigamma}{LGR}      % ϝ (greek.ldf)
\DeclareTextSymbolDefault{\Digamma}{LGR}       % Ϝ
\DeclareTextSymbolDefault{\textdigamma}{LGR}   % ϝ alias using "text" prefix
\DeclareTextSymbolDefault{\textDigamma}{LGR}   % Ϝ alias using "text" prefix
\DeclareTextSymbolDefault{\textdigammagreek}{LGR} % ϝ (puenc.def)
\DeclareTextSymbolDefault{\textDigammagreek}{LGR} % Ϝ (puenc.def)

% numeral signs: http://en.wikipedia.org/wiki/Greek_numerals
\DeclareTextSymbolDefault{\anwtonos}{LGR}     % ʹ (Dexia keraia)
\DeclareTextSymbolDefault{\katwtonos}{LGR}    % ͵ (Aristeri keraia)
% alias names for compatibility with hyperref's puenc.def:
\DeclareTextSymbolDefault{\textnumeralsigngreek}{LGR}
\DeclareTextSymbolDefault{\textnumeralsignlowergreek}{LGR}

% Diacritics
% ----------
%
% Diacritics are defined via "named" macros in ``lgrxenc.def``. Make them
% default in any encoding::

\DeclareTextAccentDefault{\Dasia}{LGR}
\DeclareTextAccentDefault{\Psili}{LGR}
\DeclareTextAccentDefault{\Tonos}{LGR}
\DeclareTextAccentDefault{\Varia}{LGR}
\DeclareTextAccentDefault{\Perispomeni}{LGR}
\DeclareTextAccentDefault{\Dialytika}{LGR}
\DeclareTextAccentDefault{\<}{LGR}
\DeclareTextAccentDefault{\>}{LGR}
%
\DeclareTextAccentDefault{\DasiaOxia}{LGR}
\DeclareTextAccentDefault{\DasiaVaria}{LGR}
\DeclareTextAccentDefault{\DasiaPerispomeni}{LGR}
\DeclareTextAccentDefault{\PsiliOxia}{LGR}
\DeclareTextAccentDefault{\PsiliVaria}{LGR}
\DeclareTextAccentDefault{\PsiliPerispomeni}{LGR}
\DeclareTextAccentDefault{\DialytikaTonos}{LGR}
\DeclareTextAccentDefault{\DialytikaVaria}{LGR}
\DeclareTextAccentDefault{\DialytikaPerispomeni}{LGR}

% Postfix accents::

\DeclareTextSymbolDefault{\Ypogegrammeni}{LGR} % "small" sub-iota
\DeclareTextSymbolDefault{\Prosgegrammeni}{LGR}  % "capital" sub-iota

% Auxiliary commands
% ------------------
%
% ``\MakeUppercase`` requires some internal commands::

\DeclareTextAccentDefault{\@hiatus}{LGR}
\DeclareTextAccentDefault{\up@Dialytika}{LGR}

% .. References
%    ----------
% .. _LaTeX Project Public License: http://www.latex-project.org/lppl.txt
% .. _lgrx: http://www.ctan.org/pkg/lgrx
% .. _babel: http://www.ctan.org/cgi-bin/ctanPackageInformation.py?id=babel
% .. _fntguide: http://www.ctan.org/tex-archive/macros/latex/doc/fntguide.pdf
% .. _CB fonts: http://www.ctan.org/pkg/cbgreek-complete
