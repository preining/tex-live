% alphabeta: Greek symbols in text and math
% *****************************************
%
% :Copyright: © 2010 Günter Milde
% :Licence:   This work may be distributed and/or modified under the
%             conditions of the `LaTeX Project Public License`_, either
%             version 1.3 of this license or any later version.
%
% :Abstract: Use ``\alpha, \beta, ...`` for Greek letters in text and math.
%
% :Identification:
%  ::

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{alphabeta}
[2012/06/26 v0.1 macros for Greek letters in text and math]

%  This package is part of the lgrx_ bundle.
%
% Changelog:
%   .. class:: borderless
%
%   ==========  ===== =========================================================
%   2012-06-26  0.1   initial version
%   ==========  ===== =========================================================
%
% Motivation
% ==========
%
% By default, the ``\alpha, \beta, ...`` macros for Greek letters
% are only valid in mathematical mode.
%
% The `textalpha` package provides a set of macros that can be used to
% access Greek symbols from a font in LGR encoding (e.g. the `CB fonts`_)
% without the need for an explicite font-encoding switch under the names
% ``\textalpha, \textbeta``, ..., ``\textOmega``.
%
% With this package, the math macros are redefined to work both, text and
% math, so that single Greek letters or simple words can be written with the
% established command names.
%
% Attention
%   While the macros work reasonably well for single letters, the automatic
%   font-encoding switches behind the doors interferes with the kerning
%   between the letters and replacement of accent+character with a
%   pre-composed character.
%
%   Therefore, Greek text should be written with the help of babel_, setting
%   the language to either ``greek`` or ``polutonikogreek`` or wrapped in the
%   provided ``\TextGreek`` macro.
%
%
% Implementation
% ==============
%
% This package extends the `textalpha` package::

\RequirePackage{textalpha}

% It uses the ``\TextOrMath`` test from the fixltx2e_ standard LaTeX
% package::

\RequirePackage{fixltx2e}

% .. _fixltx2e: http://www.ctan.org/pkg/fixltx2e
%
%
% Save math macros
% ----------------
%
% Provide an alias for the standard math commands::

\let\mathGamma\Gamma
\let\mathDelta\Delta
\let\mathTheta\Theta
\let\mathLambda\Lambda
\let\mathXi\Xi
\let\mathPi\Pi
\let\mathSigma\Sigma
\let\mathUpsilon\Upsilon
\let\mathPhi\Phi
\let\mathPsi\Psi
\let\mathOmega\Omega
%
\let\mathalpha\alpha
\let\mathbeta\beta
\let\mathgamma\gamma
\let\mathdelta\delta
\let\mathepsilon\epsilon
\let\mathzeta\zeta
\let\matheta\eta
\let\maththeta\theta
\let\mathiota\iota
\let\mathkappa\kappa
\let\mathlambda\lambda
\let\mathmu\mu
\let\mathnu\nu
\let\mathxi\xi
\let\mathpi\pi
\let\mathrho\rho
\let\mathsigma\sigma
\let\mathvarsigma\varsigma
\let\mathtau\tau
\let\mathupsilon\upsilon
\let\mathphi\phi
\let\mathchi\chi
\let\mathpsi\psi
\let\mathomega\omega


% Commands to access Greek letters by name
% ----------------------------------------
%
% For letters defined in math mode, the commands work in both, text and math.
%
% Some Greek letters look identic to Latin letters and can therefore not be
% used as variable symbols in math formulas. These letters are not defined in
% TeX's math mode, but we define an alias to the corrsponding ``\text...``
% command.
% ::

\newcommand{\Alpha}{\textAlpha}
\newcommand{\Beta}{\textBeta}
\renewcommand{\Gamma}{\TextOrMath{\textGamma}{\mathGamma}}
\renewcommand{\Delta}{\TextOrMath{\textDelta}{\mathDelta}}
\newcommand{\Epsilon}{\textEpsilon}
\newcommand{\Zeta}{\textZeta}
\newcommand{\Eta}{\textEta}
\renewcommand{\Theta}{\TextOrMath{\textTheta}{\mathTheta}}
\newcommand{\Iota}{\textIota}
\newcommand{\Kappa}{\textKappa}
\renewcommand{\Lambda}{\TextOrMath{\textLambda}{\mathLambda}}
\newcommand{\Mu}{\textMu}
\newcommand{\Nu}{\textNu}
\renewcommand{\Xi}{\TextOrMath{\textXi}{\mathXi}}
\newcommand{\Omicron}{\textOmicron}
\renewcommand{\Pi}{\TextOrMath{\textPi}{\mathPi}}
\newcommand{\Rho}{\textRho}
\renewcommand{\Sigma}{\TextOrMath{\textSigma}{\mathSigma}}
\newcommand{\Tau}{\textTau}
\renewcommand{\Upsilon}{\TextOrMath{\textUpsilon}{\mathUpsilon}}
\renewcommand{\Phi}{\TextOrMath{\textPhi}{\mathPhi}}
\newcommand{\Chi}{\textChi}
\renewcommand{\Psi}{\TextOrMath{\textPsi}{\mathPsi}}
\renewcommand{\Omega}{\TextOrMath{\textOmega}{\mathOmega}}
%
\renewcommand{\alpha}{\TextOrMath{\textalpha}{\mathalpha}}
\renewcommand{\beta}{\TextOrMath{\textbeta}{\mathbeta}}
\renewcommand{\gamma}{\TextOrMath{\textgamma}{\mathgamma}}
\renewcommand{\delta}{\TextOrMath{\textdelta}{\mathdelta}}
\renewcommand{\epsilon}{\TextOrMath{\textepsilon}{\mathepsilon}}
\renewcommand{\zeta}{\TextOrMath{\textzeta}{\mathzeta}}
\renewcommand{\eta}{\TextOrMath{\texteta}{\matheta}}
\renewcommand{\theta}{\TextOrMath{\texttheta}{\maththeta}}
\renewcommand{\iota}{\TextOrMath{\textiota}{\mathiota}}
\renewcommand{\kappa}{\TextOrMath{\textkappa}{\mathkappa}}
\renewcommand{\lambda}{\TextOrMath{\textlambda}{\mathlambda}}
\renewcommand{\mu}{\TextOrMath{\textmu}{\mathmu}}
\renewcommand{\nu}{\TextOrMath{\textnu}{\mathnu}}
\renewcommand{\xi}{\TextOrMath{\textxi}{\mathxi}}
\newcommand{\omicron}{\textomicron}
\renewcommand{\pi}{\TextOrMath{\textpi}{\mathpi}}
\renewcommand{\rho}{\TextOrMath{\textrho}{\mathrho}}
\renewcommand{\sigma}{\TextOrMath{\textsigma}{\mathsigma}}
\renewcommand{\varsigma}{\TextOrMath{\textvarsigma}{\mathvarsigma}}
\renewcommand{\tau}{\TextOrMath{\texttau}{\mathtau}}
\renewcommand{\upsilon}{\TextOrMath{\textupsilon}{\mathupsilon}}
\renewcommand{\phi}{\TextOrMath{\textphi}{\mathphi}}
\renewcommand{\chi}{\TextOrMath{\textchi}{\mathchi}}
\renewcommand{\psi}{\TextOrMath{\textpsi}{\mathpsi}}
\renewcommand{\omega}{\TextOrMath{\textomega}{\mathomega}}

% TextCompositeCommands for the generic macros
% --------------------------------------------
%
% The NFSS TextComposite mechanism looks for the next token without expanding
% it. In order to let compositions like ``\TextGreek{\'\Alpha}`` or
% ``\TextGreek{\>"\alpha}`` work as expected we define TextComposites with the
% `letter name commands` (even so composition only works if the font encoding
% is LGR). ::

\DeclareTextComposite{\Varia}{LGR}{\alpha}{128}
\DeclareTextComposite{\Dasia}{LGR}{\alpha}{129}
\DeclareTextComposite{\Psili}{LGR}{\alpha}{130}
\DeclareTextComposite{\DasiaVaria}{LGR}{\alpha}{131}
\DeclareTextComposite{\Tonos}{LGR}{\alpha}{136}
\DeclareTextComposite{\DasiaOxia}{LGR}{\alpha}{137}
\DeclareTextComposite{\PsiliOxia}{LGR}{\alpha}{138}
\DeclareTextComposite{\PsiliVaria}{LGR}{\alpha}{139}
\DeclareTextComposite{\Perispomeni}{LGR}{\alpha}{144}
\DeclareTextComposite{\DasiaPerispomeni}{LGR}{\alpha}{145}
\DeclareTextComposite{\PsiliPerispomeni}{LGR}{\alpha}{146}
\DeclareTextComposite{\Varia}{LGR}{\eta}{152}
\DeclareTextComposite{\Dasia}{LGR}{\eta}{153}
\DeclareTextComposite{\Psili}{LGR}{\eta}{154}
\DeclareTextComposite{\Tonos}{LGR}{\eta}{160}
\DeclareTextComposite{\DasiaOxia}{LGR}{\eta}{161}
\DeclareTextComposite{\PsiliOxia}{LGR}{\eta}{162}
\DeclareTextComposite{\DasiaVaria}{LGR}{\eta}{163}
\DeclareTextComposite{\Perispomeni}{LGR}{\eta}{168}
\DeclareTextComposite{\DasiaPerispomeni}{LGR}{\eta}{169}
\DeclareTextComposite{\PsiliPerispomeni}{LGR}{\eta}{170}
\DeclareTextComposite{\PsiliVaria}{LGR}{\eta}{171}
\DeclareTextComposite{\Varia}{LGR}{\omega}{176}
\DeclareTextComposite{\Dasia}{LGR}{\omega}{177}
\DeclareTextComposite{\Psili}{LGR}{\omega}{178}
\DeclareTextComposite{\DasiaVaria}{LGR}{\omega}{179}
\DeclareTextComposite{\Tonos}{LGR}{\omega}{184}
\DeclareTextComposite{\DasiaOxia}{LGR}{\omega}{185}
\DeclareTextComposite{\PsiliOxia}{LGR}{\omega}{186}
\DeclareTextComposite{\PsiliVaria}{LGR}{\omega}{187}
\DeclareTextComposite{\Perispomeni}{LGR}{\omega}{192}
\DeclareTextComposite{\DasiaPerispomeni}{LGR}{\omega}{193}
\DeclareTextComposite{\PsiliPerispomeni}{LGR}{\omega}{194}
\DeclareTextComposite{\Varia}{LGR}{\iota}{200}
\DeclareTextComposite{\Dasia}{LGR}{\iota}{201}
\DeclareTextComposite{\Psili}{LGR}{\iota}{202}
\DeclareTextComposite{\DasiaVaria}{LGR}{\iota}{203}
\DeclareTextComposite{\Tonos}{LGR}{\iota}{208}
\DeclareTextComposite{\DasiaOxia}{LGR}{\iota}{209}
\DeclareTextComposite{\PsiliOxia}{LGR}{\iota}{210}
\DeclareTextComposite{\PsiliVaria}{LGR}{\iota}{211}
\DeclareTextComposite{\Perispomeni}{LGR}{\iota}{216}
\DeclareTextComposite{\DasiaPerispomeni}{LGR}{\iota}{217}
\DeclareTextComposite{\PsiliPerispomeni}{LGR}{\iota}{218}
\DeclareTextComposite{\Dialytika}{LGR}{\iota}{240}
\DeclareTextComposite{\DialytikaVaria}{LGR}{\iota}{241}
\DeclareTextComposite{\DialytikaTonos}{LGR}{\iota}{242}
\DeclareTextComposite{\DialytikaPerispomeni}{LGR}{\iota}{243}
\DeclareTextComposite{\Varia}{LGR}{\upsilon}{204}
\DeclareTextComposite{\Dasia}{LGR}{\upsilon}{205}
\DeclareTextComposite{\Psili}{LGR}{\upsilon}{206}
\DeclareTextComposite{\DasiaVaria}{LGR}{\upsilon}{207}
\DeclareTextComposite{\Tonos}{LGR}{\upsilon}{212}
\DeclareTextComposite{\DasiaOxia}{LGR}{\upsilon}{213}
\DeclareTextComposite{\PsiliOxia}{LGR}{\upsilon}{214}
\DeclareTextComposite{\PsiliVaria}{LGR}{\upsilon}{215}
\DeclareTextComposite{\Perispomeni}{LGR}{\upsilon}{220}
\DeclareTextComposite{\DasiaPerispomeni}{LGR}{\upsilon}{221}
\DeclareTextComposite{\PsiliPerispomeni}{LGR}{\upsilon}{222}
\DeclareTextComposite{\Dialytika}{LGR}{\upsilon}{244}
\DeclareTextComposite{\DialytikaVaria}{LGR}{\upsilon}{245}
\DeclareTextComposite{\DialytikaTonos}{LGR}{\upsilon}{246}
\DeclareTextComposite{\DialytikaPerispomeni}{LGR}{\upsilon}{247}
\DeclareTextComposite{\Varia}{LGR}{\epsilon}{224}
\DeclareTextComposite{\Dasia}{LGR}{\epsilon}{225}
\DeclareTextComposite{\Psili}{LGR}{\epsilon}{226}
\DeclareTextComposite{\DasiaVaria}{LGR}{\epsilon}{227}
\DeclareTextComposite{\Tonos}{LGR}{\epsilon}{232}
\DeclareTextComposite{\DasiaOxia}{LGR}{\epsilon}{233}
\DeclareTextComposite{\PsiliOxia}{LGR}{\epsilon}{234}
\DeclareTextComposite{\PsiliVaria}{LGR}{\epsilon}{235}
\DeclareTextComposite{\Varia}{LGR}{\omicron}{228}
\DeclareTextComposite{\Dasia}{LGR}{\omicron}{229}
\DeclareTextComposite{\Psili}{LGR}{\omicron}{230}
\DeclareTextComposite{\DasiaVaria}{LGR}{\omicron}{231}
\DeclareTextComposite{\Tonos}{LGR}{\omicron}{236}
\DeclareTextComposite{\DasiaOxia}{LGR}{\omicron}{237}
\DeclareTextComposite{\PsiliOxia}{LGR}{\omicron}{238}
\DeclareTextComposite{\PsiliVaria}{LGR}{\omicron}{239}
\DeclareTextCompositeCommand{\Dasia}{LGR}{\Alpha}{<A}
\DeclareTextCompositeCommand{\DasiaVaria}{LGR}{\Alpha}{<`A}
\DeclareTextCompositeCommand{\DasiaOxia}{LGR}{\Alpha}{<'A}
\DeclareTextCompositeCommand{\DasiaPerispomeni}{LGR}{\Alpha}{<\char126A}
\DeclareTextCompositeCommand{\Psili}{LGR}{\Alpha}{>A}
\DeclareTextCompositeCommand{\PsiliVaria}{LGR}{\Alpha}{>`A}
\DeclareTextCompositeCommand{\PsiliOxia}{LGR}{\Alpha}{>'A}
\DeclareTextCompositeCommand{\PsiliPerispomeni}{LGR}{\Alpha}{>\char126A}
\DeclareTextCompositeCommand{\Tonos}{LGR}{\Alpha}{'A}
\DeclareTextCompositeCommand{\Varia}{LGR}{\Alpha}{`A}
\DeclareTextCompositeCommand{\Dasia}{LGR}{\Epsilon}{<E}
\DeclareTextCompositeCommand{\DasiaOxia}{LGR}{\Epsilon}{<'E}
\DeclareTextCompositeCommand{\DasiaVaria}{LGR}{\Epsilon}{<`E}
\DeclareTextCompositeCommand{\Psili}{LGR}{\Epsilon}{>E}
\DeclareTextCompositeCommand{\PsiliOxia}{LGR}{\Epsilon}{>'E}
\DeclareTextCompositeCommand{\PsiliVaria}{LGR}{\Epsilon}{>`E}
\DeclareTextCompositeCommand{\Tonos}{LGR}{\Epsilon}{'E}
\DeclareTextCompositeCommand{\Varia}{LGR}{\Epsilon}{`E}
\DeclareTextCompositeCommand{\Dasia}{LGR}{\Eta}{<H}
\DeclareTextCompositeCommand{\DasiaVaria}{LGR}{\Eta}{<`H}
\DeclareTextCompositeCommand{\DasiaOxia}{LGR}{\Eta}{<'H}
\DeclareTextCompositeCommand{\DasiaPerispomeni}{LGR}{\Eta}{<\char126H}
\DeclareTextCompositeCommand{\Psili}{LGR}{\Eta}{>H}
\DeclareTextCompositeCommand{\PsiliVaria}{LGR}{\Eta}{>`H}
\DeclareTextCompositeCommand{\PsiliOxia}{LGR}{\Eta}{>'H}
\DeclareTextCompositeCommand{\PsiliPerispomeni}{LGR}{\Eta}{>\char126H}
\DeclareTextCompositeCommand{\Tonos}{LGR}{\Eta}{'H}
\DeclareTextCompositeCommand{\Varia}{LGR}{\Eta}{`H}
\DeclareTextCompositeCommand{\Dasia}{LGR}{\Iota}{<I}
\DeclareTextCompositeCommand{\DasiaVaria}{LGR}{\Iota}{<`I}
\DeclareTextCompositeCommand{\DasiaOxia}{LGR}{\Iota}{<'I}
\DeclareTextCompositeCommand{\DasiaPerispomeni}{LGR}{\Iota}{<\char126I}
\DeclareTextCompositeCommand{\Psili}{LGR}{\Iota}{>I}
\DeclareTextCompositeCommand{\PsiliVaria}{LGR}{\Iota}{>`I}
\DeclareTextCompositeCommand{\PsiliOxia}{LGR}{\Iota}{>'I}
\DeclareTextCompositeCommand{\PsiliPerispomeni}{LGR}{\Iota}{>\char126I}
\DeclareTextCompositeCommand{\Tonos}{LGR}{\Iota}{'I}
\DeclareTextCompositeCommand{\Varia}{LGR}{\Iota}{`I}
\DeclareTextCompositeCommand{\Dasia}{LGR}{\Omicron}{<O}
\DeclareTextCompositeCommand{\DasiaVaria}{LGR}{\Omicron}{<`O}
\DeclareTextCompositeCommand{\DasiaOxia}{LGR}{\Omicron}{<'O}
\DeclareTextCompositeCommand{\Psili}{LGR}{\Omicron}{>O}
\DeclareTextCompositeCommand{\PsiliVaria}{LGR}{\Omicron}{>`O}
\DeclareTextCompositeCommand{\PsiliOxia}{LGR}{\Omicron}{>'O}
\DeclareTextCompositeCommand{\Tonos}{LGR}{\Omicron}{'O}
\DeclareTextCompositeCommand{\Varia}{LGR}{\Omicron}{`O}
\DeclareTextCompositeCommand{\Dasia}{LGR}{\Upsilon}{<U}
\DeclareTextCompositeCommand{\DasiaVaria}{LGR}{\Upsilon}{<`U}
\DeclareTextCompositeCommand{\DasiaOxia}{LGR}{\Upsilon}{<'U}
\DeclareTextCompositeCommand{\DasiaPerispomeni}{LGR}{\Upsilon}{<\char126U}
\DeclareTextCompositeCommand{\Tonos}{LGR}{\Upsilon}{'U}
\DeclareTextCompositeCommand{\Varia}{LGR}{\Upsilon}{`U}
\DeclareTextCompositeCommand{\Dasia}{LGR}{\Omega}{<W}
\DeclareTextCompositeCommand{\DasiaVaria}{LGR}{\Omega}{<`W}
\DeclareTextCompositeCommand{\DasiaOxia}{LGR}{\Omega}{<'W}
\DeclareTextCompositeCommand{\DasiaPerispomeni}{LGR}{\Omega}{<\char126W}
\DeclareTextCompositeCommand{\Psili}{LGR}{\Omega}{>W}
\DeclareTextCompositeCommand{\PsiliVaria}{LGR}{\Omega}{>`W}
\DeclareTextCompositeCommand{\PsiliOxia}{LGR}{\Omega}{>'W}
\DeclareTextCompositeCommand{\PsiliPerispomeni}{LGR}{\Omega}{>\char126W}
\DeclareTextCompositeCommand{\Tonos}{LGR}{\Omega}{'W}
\DeclareTextCompositeCommand{\Varia}{LGR}{\Omega}{`W}
\DeclareTextComposite{\Dialytika}{LGR}{\Iota}{219}
\DeclareTextComposite{\Dialytika}{LGR}{\Upsilon}{223}
\DeclareTextCompositeCommand{\@hiatus}{LGR}{\Alpha}{\A@hiatus}
\DeclareTextCompositeCommand{\@hiatus}{LGR}{\Epsilon}{\E@hiatus}


% .. References
%    ----------
% .. _LaTeX Project Public License: http://www.latex-project.org/lppl.txt
% .. _CB Fonts: http://www.ctan.org/pkg/cbgreek-complete
% .. _babel: http://www.ctan.org/cgi-bin/ctanPackageInformation.py?id=babel
% .. _lgrx: http://www.ctan.org/pkg/lgrx
% .. _fixltx2e: http://www.ctan.org/pkg/fixltx2e
