% ctex-xecjk-engine.def: for XeTeX engine with xeCJK
% vim:ft=tex

% compatibility with amssymb.sty etc and xunicode.sty v0.95+
\RequirePackage{savesym}
\def\CTEX@save@symlist{hbar,Finv,aleph,beth,gimel,daleth,Game}

\@for \reversed@a:=\CTEX@save@symlist \do{%
  \savesymbol{\reversed@a}}

\RequirePackage[BoldFont,normalindentfirst]{xeCJK}
\defaultfontfeatures{Ligatures=TeX}

\@for \reversed@a:=\CTEX@save@symlist \do{%
  \restoresymbol{UTF}{\reversed@a}%
  \expandafter\ifx \csname\reversed@a\endcsname \relax
    \expandafter\let \csname\reversed@a\endcsname \CTEX@undefined
  \fi}
\let\CTEX@save@symlist\CTEX@undefined

\ifCTEX@punct
  \punctstyle{quanjiao}
\else
  \punctstyle{plain}
\fi

\input{ctex-cjk-common.def}

\ifCTEX@nofonts\else
  \ifCTEX@winfonts
    \input{ctex-xecjk-winfonts.def}
  \else\ifCTEX@adobefonts
    \input{ctex-xecjk-adobefonts.def}
  \fi\fi
\fi

\endinput
