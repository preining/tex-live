% File hyphen.lan
%
%  Created by Oldrich Ulrych,            January 5, 1993
%  General changes by Petr Olsak         November 2012
%
% See the end of this file for more inforamtion 

\def\loadpatterns #1#2 #3 #4#5#6#7#8#9{\ifx#1\undefined \else \chardef#1=#2
      \begingroup \language=#2 \def\doaccents{\def\csaccentsmessage{}\csaccents}
                  \let\global=\relax
                  \message{Loading #4 encoding, \string#1=#2 (#3).} #5
      \endgroup
      \expandafter \def \csname lan:#2\endcsname {#3}
      \expandafter \def \csname #3lang\endcsname {%
         \language=#9\relax \initlanguage{#3}\frenchspacing 
         \lefthyphenmin=#6\righthyphenmin=#7%
         \message{#3 hyphenation used (\string\language=\the\language).
                    \string\frenchspacing\space is set on.}}
      \ifx#8\relax \else
         \expandafter\def\expandafter#8\expandafter{#8\let#9#1}\fi
      \edef\pattlist{\pattlist #1=#2 }
   \fi
}
\def\pattlist{} \def\initlanguage#1{}

\def\robustpatterns{\let\oripatterns=\patterns \def\patterns##1{\scanpatterns##1{}{##1}}}
\def\scanpatterns#1{\if$#1$\expandafter\oripatterns\else 
    \ifnum`#1<128 \else \lccode`#1=`#1 \fi \expandafter\scanpatterns\fi}

\def\tmp#1#2\relax{\def\tmp{#2}}\tmp ^^^^abcd\relax               % 16bit TeX engine?
\ifx\allpatterns\undefined \catcode`\:=14 \else \catcode`\:=9 \fi % comment or ignored

\def\iltwolangs{} \def\corklangs{} \def\unicodelangs{}

%% \corklangs (etc.) cummulates the commands 
%%       \let\czPatt=\czCork \let\skPatt=\skCork (etc.)
%% You can run \corklangs before \language=\czPatt or \language=\skPatt 
%% if you want to work in T1 encoding. 

%% You can remove the colon character if you want to use 
%% the language mentioned below.  Or you can say:  
%%    pdftex -ini "\let\deCork=\patterns \input csplain.ini"
%% or
%%    pdftex -ini "\let\allpaterns=y \input csplain.ini"

%% ASCII (these patterns work independently on choosen encoding):

\let\USenglish=\patterns  % Default plainTeX US English
:\let\enusPatt=\patterns  % US English extended
:\let\engbPatt=\patterns  % UK English
:\let\itPatt=\patterns    % Italian
:\let\iaPatt=\patterns    % Interlingua
:\let\idPatt=\patterns    % Indonesian

%% ISO-8859-2 (default patterns in csplain):

\let\czILtwo=\patterns
\let\skILtwo=\patterns

%% You can say
%%   pdftex -ini "\let\Cork=\relax \input csplain.ini"
%% if you want to suppress the Cork (alias T1) encoded hyphenation patterns.

\ifx\Cork\undefined

\let\czCork=\patterns    % Czech
\let\skCork=\patterns    % Slovak

\ifx\tmp\empty\else

:\let\deCork=\patterns   % German
:\let\frCork=\patterns   % French
:\let\plCork=\patterns   % Polish
:\let\cyCork=\patterns   % Welsh
:\let\daCork=\patterns   % Danish
:\let\esCork=\patterns   % Spanish
:\let\slCork=\patterns   % Slovenian
:\let\svCork=\patterns   % Swedish
:\let\fiCork=\patterns   % Finnish
:\let\huCork=\patterns   % Hungarian
:\let\trCork=\patterns   % Turkish
:\let\etCork=\patterns   % Estonian
:\let\euCork=\patterns   % Basque
:\let\gaCork=\patterns   % Irish
:\let\nbCork=\patterns   % Norwegian Bokmal
:\let\nnCork=\patterns   % Norwegian Nynorsk
:\let\nlCork=\patterns   % Dutch
:\let\huCork=\patterns   % Hungarian
:\let\ptCork=\patterns   % Portuguese
:\let\roCork=\patterns   % Romanian
:\let\hrCork=\patterns   % Croatian
:\let\zhCork=\patterns   % Unaccented Pinyin Syllables
:\let\isCork=\patterns   % Icelandic
:\let\hsbCork=\patterns  % EC Upper Sorbian
:\let\afCork=\patterns   % Afrikaans
:\let\glCork=\patterns   % Galician
:\let\kmrCork=\patterns  % Kurmanji
:\let\tkCork=\patterns   % Turkmen
:\let\laCork=\patterns   % Latin

\fi\fi

%% test of Unicoded TeX engine

\ifx\tmp\empty 

\let\czUnicode=\patterns    % Czech
\let\skUnicode=\patterns    % Slovak
:\let\deUnicode=\patterns   % German
:\let\frUnicode=\patterns   % French
:\let\plUnicode=\patterns   % Polish
:\let\cyUnicode=\patterns   % Welsh
:\let\daUnicode=\patterns   % Danish
:\let\esUnicode=\patterns   % Spanish
:\let\eoUnicode=\patterns   % Esperanto
:\let\slUnicode=\patterns   % Slovenian
:\let\svUnicode=\patterns   % Swedish
:\let\fiUnicode=\patterns   % Finnish
:\let\huUnicode=\patterns   % Hungarian
:\let\trUnicode=\patterns   % Turkish
:\let\etUnicode=\patterns   % Estonian
:\let\euUnicode=\patterns   % Basque
:\let\gaUnicode=\patterns   % Irish
:\let\nbUnicode=\patterns   % Norwegian Bokmal
:\let\nnUnicode=\patterns   % Norwegian Nynorsk
:\let\nlUnicode=\patterns   % Dutch
:\let\huUnicode=\patterns   % Hungarian
:\let\ptUnicode=\patterns   % Portuguese
:\let\roUnicode=\patterns   % Romanian
:\let\hrUnicode=\patterns   % Croatian
:\let\zhUnicode=\patterns   % Unaccented Pinyin Syllables
:\let\isUnicode=\patterns   % Icelandic
:\let\hsbUnicode=\patterns  % EC Upper Sorbian
:\let\afUnicode=\patterns   % Afrikaans
:\let\glUnicode=\patterns   % Galician
:\let\kmrUnicode=\patterns  % Kurmanji
:\let\tkUnicode=\patterns   % Turkmen
:\let\laUnicode=\patterns   % Latin
:\let\elmUnicode=\patterns  % Modern Monotonic Greek
:\let\elpUnicode=\patterns  % Modern Polytonic Greek
:\let\grcUnicode=\patterns  % Ancient Greek
:\let\caUnicode=\patterns   % Catalan
:\let\copUnicode=\patterns  % Coptic
:\let\mnUnicode=\patterns   % Mongolian
:\let\saUnicode=\patterns   % Sanskrit
:\let\ruUnicode=\patterns   % Russian
:\let\ukUnicode=\patterns   % Ukrainian
:\let\hyUnicode=\patterns   % Armenian
:\let\asUnicode=\patterns   % Assamese
:\let\hiUnicode=\patterns   % Hindi
:\let\knUnicode=\patterns   % Kannada
:\let\loUnicode=\patterns   % Lao
:\let\lvUnicode=\patterns   % Latvian
:\let\ltUnicode=\patterns   % Lithuanian
:\let\mlUnicode=\patterns   % Malayalam
:\let\mrUnicode=\patterns   % Marathi
:\let\orUnicode=\patterns   % Oriya
:\let\paUnicode=\patterns   % Panjabi
:\let\taUnicode=\patterns   % Tamil
:\let\teUnicode=\patterns   % Telugu

\fi

\catcode58=12 % colon has normal meaning now
\def\patt{hyphenation patterns } 

\loadpatterns \USenglish 0 us {US English \patt in ASCII}
              {\input hyphen.ex }23 \relax \USenglish

\loadpatterns \enusPatt 100 enus {US English extended \patt in ASCII}
              {\input hyph-en-us }23 \relax \enusPatt

\loadpatterns \engbPatt 101 engb {UK English \patt in ASCII}
              {\input hyph-en-gb }23 \relax \engbPatt

\loadpatterns \itPatt 102 it {Italian \patt in ASCII}
              {\lccode`\'=`\' \input hyph-it }22 \relax \itPatt

\loadpatterns \iaPatt 103 ia {Interlingua \patt in ASCII}
              {\input hyph-ia }22 \relax \iaPatt

\loadpatterns \idPatt 104 id {Indonesian \patt in ASCII}
              {\input hyph-id }23 \relax \idPatt

\loadpatterns \czILtwo 5 cz {Czech \patt in ISO-8859-2}
              {\doaccents \input czhyphen.tex \input czhyphen.ex }23 \iltwolangs \czPatt

\loadpatterns \skILtwo 6 sk {Slovak \patt in ISO-8859-2}
              {\doaccents \input skhyphen.tex \input skhyphen.ex }23 \iltwolangs \skPatt

\loadpatterns \czCork 15 cz {Czech \patt in Cork}
              {\input t1code \doaccents \input czhyphen.tex \input czhyphen.ex }23
              \corklangs \czPatt

\loadpatterns \skCork 16 sk {Slovak \patt in Cork}
              {\input t1code \doaccents \input skhyphen.tex \input skhyphen.ex }23
              \corklangs \skPatt

\loadpatterns \deCork 21 de {German \patt in Cork}
              {\input conv-utf8-ec \input hyph-de-1996 }22 \corklangs \dePatt

\loadpatterns \frCork 22 fr {French \patt in Cork}
              {\lccode`\'=`\' \input conv-utf8-ec \input hyph-fr }23 \corklangs \frPatt

\loadpatterns \plCork 23 pl {Polish \patt in Cork}
              {\input conv-utf8-ec \input hyph-pl }22 \corklangs \plPatt

\loadpatterns \cyCork 24 cy {Welsh \patt in Cork}
              {\input conv-utf8-ec \input hyph-cy }23 \corklangs \cyPatt

\loadpatterns \daCork 25 da {Danish \patt in Cork}
              {\input conv-utf8-ec \input hyph-da }22 \corklangs \daPatt

\loadpatterns \esCork 26 es {Spanish \patt in Cork}
              {\input conv-utf8-ec \input hyph-es }22 \corklangs \esPatt

\loadpatterns \slCork 28 sl {Slovenian \patt in Cork}
              {\input conv-utf8-ec \input hyph-sl }22 \corklangs \slPatt

\loadpatterns \fiCork 29 fi {Finnish \patt in Cork}
              {\input conv-utf8-ec \input hyph-fi }22 \corklangs \fiPatt

\loadpatterns \huCork 30 hu {Hungarian \patt in Cork}
              {\input conv-utf8-ec \input hyph-hu }22 \corklangs \huPatt

\loadpatterns \trCork 31 tr {Turkish \patt in Cork}
              {\input conv-utf8-ec \input hyph-tr }22 \corklangs \trPatt

\loadpatterns \etCork 32 et {Estonian \patt in Cork}
              {\input conv-utf8-ec \input hyph-et }23 \corklangs \etPatt

\loadpatterns \euCork 33 eu {Basque \patt in Cork}
              {\input conv-utf8-ec \input hyph-eu }22 \corklangs \euPatt

\loadpatterns \gaCork 34 ga {Irish \patt in Cork}
              {\input conv-utf8-ec \input hyph-ga }23 \corklangs \gaPatt

\loadpatterns \nbCork 35 nb {Norwegian Bokmal \patt in Cork}
              {\input conv-utf8-ec \input hyph-nb }22 \corklangs \nbPatt

\loadpatterns \nnCork 36 nn {Norwegian Nynorsk \patt in Cork}
              {\input conv-utf8-ec \input hyph-nn }22 \corklangs \nnPatt

\loadpatterns \nlCork 37 nl {Dutch \patt in Cork}
              {\input conv-utf8-ec \input hyph-nl }22 \corklangs \nlPatt

\loadpatterns \ptCork 38 pt {Portugesse \patt in Cork}
              {\lccode`\-=`\- \input conv-utf8-ec \input hyph-pt }23 \corklangs \ptPatt

\loadpatterns \roCork 39 ro {Romanian \patt in Cork}
              {\input conv-utf8-ec \input hyph-ro }22 \corklangs \roPatt

\loadpatterns \hrCork 40 hr {Croatian \patt in Cork}
              {\input conv-utf8-ec \input hyph-hr }22 \corklangs \hrPatt

\loadpatterns \zhCork 41 zh {Unaccented Pinyin Syllables \patt in Cork}
              {\lccode`\'=`\' \input hyph-zh-latn-pinyin.ec }11 \corklangs \zhPatt

\loadpatterns \isCork 42 is {Icelandic \patt in Cork}
              {\input conv-utf8-ec \input hyph-is }22 \corklangs \isPatt

\loadpatterns \hsbCork 43 hsb {EC Upper Sorbian \patt in Cork}
              {\input conv-utf8-ec \input hyph-hsb }22 \corklangs \hsbPatt

\loadpatterns \afCork 44 af {Afrikaans \patt in Cork}
              {\lccode`\-=`\- \lccode`\'=`\'
               \input conv-utf8-ec \input hyph-af }22 \corklangs \afPatt

\loadpatterns \glCork 45 gl {Galician \patt in Cork}
              {\input conv-utf8-ec \input hyph-gl }22 \corklangs \glPatt

\loadpatterns \kmrCork 46 kmr {Kurmanji \patt in Cork}
              {\input conv-utf8-ec \input hyph-kmr }22 \corklangs \kmrPatt

\loadpatterns \tkCork 47 tk {Turkmen \patt in Cork}
              {\lccode`\-=`\- \input conv-utf8-ec \input hyph-tk }22 \corklangs \tkPatt

\loadpatterns \laCork 48 la {Latin \patt in Cork}
              {\lccode`\'=`\' \input conv-utf8-ec \input hyph-la }22 \corklangs \laPatt

\loadpatterns \czUnicode 115 cz {Czech \patt in Unicode}
              {\input ucode \doaccents \input czhyphen.tex \input czhyphen.ex }23
              \unicodelangs \czPatt

\loadpatterns \skUnicode 116 sk {Slovak \patt in Unicode}
              {\input ucode \doaccents \input skhyphen.tex \input skhyphen.ex }23
              \unicodelangs \skPatt

\loadpatterns \deUnicode 121 de {German \patt in Unicode}
              {\robustpatterns \input hyph-de-1996 }22 \unicodelangs \dePatt

\loadpatterns \frUnicode 122 fr {French \patt in Unicode}
              {\robustpatterns \lccode`\'=`\' \input hyph-fr }23 \unicodelangs \frPatt

\loadpatterns \plUnicode 123 pl {Polish \patt in Unicode}
              {\robustpatterns \input hyph-pl }22 \unicodelangs \plPatt

\loadpatterns \cyUnicode 124 cy {Welsh \patt in Unicode}
              {\robustpatterns \input hyph-cy }23 \unicodelangs \cyPatt

\loadpatterns \daUnicode 125 da {Danish \patt in Unicode}
              {\robustpatterns \input hyph-da }22 \unicodelangs \daPatt

\loadpatterns \esUnicode 126 es {Spanish \patt in Unicode}
              {\robustpatterns \input hyph-es }22 \unicodelangs \esPatt

\loadpatterns \slUnicode 128 sl {Slovenian \patt in Unicode}
              {\robustpatterns \input hyph-sl }22 \unicodelangs \slPatt

\loadpatterns \fiUnicode 129 fi {Finnish \patt in Unicode}
              {\robustpatterns \input hyph-fi }22 \unicodelangs \fiPatt

\loadpatterns \huUnicode 130 hu {Hungarian \patt in Unicode}
              {\robustpatterns input hyph-hu }22 \unicodelangs \huPatt

\loadpatterns \trUnicode 131 tr {Turkish \patt in Unicode}
              {\robustpatterns \input hyph-tr }22 \unicodelangs \trPatt

\loadpatterns \etUnicode 132 et {Estonian \patt in Unicode}
              {\robustpatterns \input hyph-et }23 \unicodelangs \etPatt

\loadpatterns \euUnicode 133 eu {Basque \patt in Unicode}
              {\robustpatterns \input hyph-eu }22 \unicodelangs \euPatt

\loadpatterns \gaUnicode 134 ga {Irish \patt in Unicode}
              {\robustpatterns \input hyph-ga }23 \unicodelangs \gaPatt

\loadpatterns \nbUnicode 135 nb {Norwegian Bokmal \patt in Unicode}
              {\robustpatterns \input hyph-nb }22 \unicodelangs \nbPatt

\loadpatterns \nnUnicode 136 nn {Norwegian Nynorsk \patt in Unicode}
              {\robustpatterns \input hyph-nn }22 \unicodelangs \nnPatt

\loadpatterns \nlUnicode 137 nl {Dutch \patt in Unicode}
              {\robustpatterns \input hyph-nl }22 \unicodelangs \nlPatt

\loadpatterns \ptUnicode 138 pt {Portugesse \patt in Unicode}
              {\robustpatterns \lccode`\-=`\- \input hyph-pt }23 \unicodelangs \ptPatt

\loadpatterns \roUnicode 139 ro {Romanian \patt in Unicode}
              {\robustpatterns \input hyph-ro }22 \unicodelangs \roPatt

\loadpatterns \hrUnicode 140 hr {Croatian \patt in Unicode}
              {\robustpatterns \input hyph-hr }22 \unicodelangs \hrPatt

\loadpatterns \zhUnicode 141 zh {Unaccented Pinyin Syllables \patt in Unicode}
              {\robustpatterns \lccode`\'=`\' \input hyph-zh-latn-pinyin }11 \unicodelangs \zhPatt

\loadpatterns \isUnicode 142 is {Icelandic \patt in Unicode}
              {\robustpatterns \input hyph-is }22 \unicodelangs \isPatt

\loadpatterns \hsbUnicode 143 hsb {EC Upper Sorbian \patt in Unicode}
              {\robustpatterns \input hyph-hsb }22 \unicodelangs \hsbPatt

\loadpatterns \afUnicode 144 af {Afrikaans \patt in Unicode}
              {\robustpatterns \lccode`\-=`\- \lccode`\'=`\'
               \input hyph-af }22 \corklangs \afPatt

\loadpatterns \glUnicode 145 gl {Galician \patt in Unicode}
              {\robustpatterns \input hyph-gl }22 \corklangs \glPatt

\loadpatterns \kmrUnicode 146 kmr {Kurmanji \patt in Unicode}
              {\robustpatterns \input hyph-kmr }22 \corklangs \kmrPatt

\loadpatterns \tkUnicode 147 tk {Turkmen \patt in Unicode}
              {\robustpatterns \lccode`\-=`\- \input hyph-tk }22 \corklangs \tkPatt

\loadpatterns \laUnicode 148 la {Latin \patt in Unicode}
              {\robustpatterns \lccode`\'=`\' \input hyph-la }22 \unicodelangs \laPatt

\loadpatterns \elmUnicode 201 elm {Modern Monotonic Greek \patt in Unicode}
              {\lccode`\'=`\' \lccode"2019="2019 \lccode"02BC="02BC \lccode"1FBF="1FBF
               \robustpatterns \input hyph-el-monoton }11 \unicodelangs \elmPatt

\loadpatterns \elpUnicode 202 elp {Modern Polytonic Greek \patt in Unicode}
              {\lccode`\'=`\' \lccode"2019="2019 \lccode"02BC="02BC \lccode"1FBF="1FBF
               \robustpatterns \input hyph-el-polyton }11 \unicodelangs \elpPatt

\loadpatterns \grcUnicode 203 grc {Ancient Greek \patt in Unicode}
              {\lccode`\'=`\' \lccode"2019="2019 \lccode"02BC="02BC \lccode"1FBF="1FBF
               \robustpatterns \input hyph-grc }11 \unicodelangs \grcPatt

\loadpatterns \caUnicode 204 ca {Catalan \patt in Unicode}
              {\robustpatterns \lccode`\'=`\' \input hyph-ca }22 \unicodelangs \caPatt

\loadpatterns \copUnicode 205 cop {Coptic \patt in Unicode}
              {\robustpatterns \lccode`\'=`\' \input hyph-cop }11 \unicodelangs \copPatt

\loadpatterns \mnUnicode 206 mn {Mongolian \patt in Unicode}
              {\robustpatterns \lccode`\'=`\' \input hyph-mn-cyrl }22 \unicodelangs \mnPatt

\loadpatterns \saUnicode 207 sa {Sanskrit \patt in Unicode}
              {\robustpatterns \lccode"200C="200C \lccode"200D="200D 
               \input hyph-sa }15 \unicodelangs \saPatt

\loadpatterns \ruUnicode 208 ru {Russian \patt in Unicode}
              {\robustpatterns \lccode`\-=`\- \input hyph-ru }22 \unicodelangs \ruPatt

\loadpatterns \ukUnicode 209 uk {Ukrainian \patt in Unicode}
              {\robustpatterns \lccode`\'=`\' \lccode`\-=`\-
               \input hyph-uk }22 \unicodelangs \ukPatt

\loadpatterns \hyUnicode 210 hy {Armenian \patt in Unicode}
              {\robustpatterns \input hyph-hy }12 \unicodelangs \hyPatt

\loadpatterns \asUnicode 211 as {Assamesse \patt in Unicode}
              {\robustpatterns \input hyph-as }11 \unicodelangs \asPatt

\loadpatterns \hiUnicode 212 hi {Hindi \patt in Unicode}
              {\robustpatterns \input hyph-hi }11 \unicodelangs \hiPatt

\loadpatterns \knUnicode 213 kn {Kannada \patt in Unicode}
              {\robustpatterns \input hyph-kn }11 \unicodelangs \knPatt

\loadpatterns \loUnicode 214 lo {Lao \patt in Unicode}
              {\robustpatterns \input hyph-lo }11 \unicodelangs \loPatt

\loadpatterns \lvUnicode 215 lv {Latvian \patt in Unicode}
              {\robustpatterns \input hyph-lv }22 \unicodelangs \lvPatt

\loadpatterns \ltUnicode 216 lt {Lithuanian \patt in Unicode}
              {\robustpatterns \input hyph-lt }22 \unicodelangs \ltPatt

\loadpatterns \mlUnicode 217 ml {Malayalam \patt in Unicode}
              {\robustpatterns \input hyph-ml }11 \unicodelangs \mlPatt

\loadpatterns \mrUnicode 218 mr {Marathi \patt in Unicode}
              {\robustpatterns \input hyph-mr }11 \unicodelangs \mrPatt

\loadpatterns \orUnicode 219 or {Oriya \patt in Unicode}
              {\robustpatterns \input hyph-or }11 \unicodelangs \orPatt

\loadpatterns \paUnicode 220 pa {Panjabi \patt in Unicode}
              {\robustpatterns \input hyph-pa }11 \unicodelangs \paPatt

\loadpatterns \taUnicode 221 ta {Tamil \patt in Unicode}
              {\robustpatterns \input hyph-ta }11 \unicodelangs \taPatt

\loadpatterns \teUnicode 222 te {Telgu \patt in Unicode}
              {\robustpatterns \input hyph-te }11 \unicodelangs \tePatt


\message{Patterns: \pattlist}
\message{Defaults: \string\language=\the\language,
        \string\cmaccents, \string\nofrenchspacing,
        ISO-8859-2 font encoding}

% ========= for backward compatibility ========

\newcount\czech  \newcount\slovak

\let\iltwoczech=\czILtwo \let\iltwoslovak=\skILtwo
\let\toneczech=\czCork   \let\toneslovak=\skCork

\expandafter \def\expandafter\iltwolangs \expandafter {\iltwolangs
    \czech\czILtwo  \slovak\skILtwo}

\ifx\czCork\undefined \else
\expandafter \def\expandafter\corklangs \expandafter {\corklangs
    \czech\czCork  \slovak\skCork}
\fi

\ifx\czUnicode\undefined \else
\expandafter \def\expandafter\unicodelangs \expandafter {\unicodelangs
    \czech\czUnicode  \slovak\skUnicode}
\fi

\def\uslang{\language=\USenglish \initlanguage{us}\nonfrenchspacing
        \lefthyphenmin=2       \righthyphenmin=3
        \message{English hyphenation used (\string\language=\the\language).
                 \string\nonfrenchspacing\space is set on.}}

\let\ehyph=\uslang  \let\chyph=\czlang  \let\shyph=\sklang

\iltwolangs  % csplain default is ISO-8859-2 encoding

\endinput

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

If \<lang>Code (for example \czUnicode, \deCork) is undefined, the patterns
are not loaded. If user sets \<lang>Code=\patterns before this file is
loaded, then the patterns are loaded.

You can remove colons in this file at lines 48-160 or you can say, for
example:

    pdftex -ini "\let\plCork=\patterns \input csplain.ini"
or
    pdftex -ini "\let\allpatterns=y \input csplain.ini"

in order to load the additional patterns.

By default, only \USenglish=0 \czILtwo=5, \skILtwo=6, \czCork=15, \skCork=16
are loaded in 8bit TeX. The additional \czUnicode=115, \skUnicode=116 are
loaded in 16bit TeX.

The macro \loadpatterns \langCode <num> <lang> {note}
                        {action}<lhm><rhm> \codelist \switch
does following:
- If \langCode is undefined, does nothing.
- Declares \chadref\langCode=<num>.
- Loads new pattern with the number <num> by {action} in the group,
- Defines \"<lang>lang" macro (for example \czlang), which sets 
  \language=\switch \lefthyphenmin=<lhm> \righthypgenmin=<rhm>
  and runs \initlanguage{<lang>}. The \initlanguage is dumy by default 
  but macro programmer can define it.
- Adds \let\switch=\langCode" to \codelist. The codelist includes 
  such commands dependent on the encoding. If user switches to 
  another encoding, the \codelist have to be run and then 
  the command \language=\switch works properly.
- Defines \csname lan:<num>\endcsname as <lang>. This may be usable 
  for macro programes.
- Adds info \langCode=<num> to \pattlist. User can expand this 
  macro in order to see which languages are preloaded.

The \robustpatterns macro performs two steps of \patterns reading:
Fisrt step: \lccodes of all characters in the patterns are set to code. 
Second step: the \patterns are read.


% ======= end of hyphen.lan


