NOTES ON BUILDING TLPERL 2012 November update

BUILD PLATFORM

32-bits Windows XP Professional SP3 running under VirtualBox on
Linux. The user is not an administrator.

GETTING THE PERL SOURCE

Release version:
I downloaded from CPAN: the latest stable Perl source 5.16.2

COLLECTING A NATIVE BUILD ENVIRONMENT

I found in the root of the distribution a file README.win32
containing compilation instructions, which listed both mingw32 from
mingw.org and the newer mingw64 from mingw-w64.sf.net as possible
compilers.

I downloaded and ran the automated downloader/installer
x86-mingw32-build-1.0-sh.tar.bz2. I selected gcc and g++. I added
the mingw32 bin subdirectory to the searchpath.

Based on the same README.win32 from the Perl source, I downloaded
dmake-4.12-2009-SHAY.zip from
http://search.cpan.org/dist/dmake. Following the file readme.txt in
the root of this archive, I added the file dmake.exe and the startup
subdirectory to the bin subdirectory of mingw.

BUILDING

I went to the win32 subdirectory of the Perl source. In makefile.mk I
adjusted relevant settings to my build environment. Hoping to avoid
some intractable failures I turned off optimization:

*** makefile.mk.orig	2012-10-26 05:07:28.000000000 +0200
--- makefile.mk	2012-11-16 17:50:27.947856481 +0100
*** 428,434 ****
  OPTIMIZE	= -g -O2 -DDEBUGGING
  LINK_DBG	= -g
  .ELSE
! OPTIMIZE	= -s -O2
  LINK_DBG	= -s
  .ENDIF
  
--- 428,434 ----
  OPTIMIZE	= -g -O2 -DDEBUGGING
  LINK_DBG	= -g
  .ELSE
! OPTIMIZE	= -s -O0
  LINK_DBG	= -s
  .ENDIF

Then I typed

  dmake

and

  dmake install

There were hard-coded paths in lib/Config.pm and
lib/Config_heavy.pl, which were likely to cause problems.

I edited lib/Config.pm and lib/Config_heavy.pl to compute Perl's
location dynamically. To both I add the code

  my $rootdir = __FILE__;
  $rootdir =~ s![\\/][^\\/]*[\\/][^\\/]*$!!;
  $rootdir =~ s!/!\\!g;

and used this in Config.pm for the definition of %Config, taking
care to replace single-quoted strings with double-quoted ones and
escaping backslashes and other characters where necessary. I also
parameterized the location of the mingw compiler.

Config_heavy.pl got a similar treatment: in single-quoted
here-documents with markers !END! (very long) and EOVIRTUAL I
replaced the hard-coded Perl paths. Because the $rootdir variable
needed to be interpolated, I shifted from single- to double-quoting,
which required escaping various characters.

The original versions are still present under the names Config.pm.orig
and Config_heavy.pl.orig. Diffs are here.

For adding modules, I added the new Perl bin directory to the
searchpath. I downloaded them all from CPAN.

Windows-specific:

  Win32-API-0.68.tar.gz
  Win32-Shortcut-0.07.tar.gz
  Win32API-Registry-0.32.tar.gz
  Win32-TieRegistry-0.26.tar.gz
  Win32-Process-Info-1.019.tar.gz

Perl/Tk:

  Tk-804.030.tar.gz

LWP and dependencies, in order of compilation/installation:

  HTML-Tagset-3.20.tar.gz
  HTML-Parser-3.69.tar.gz
  URI-1.60.tar.gz
  Encode-Locale-1.03.tar.gz
  HTTP-Date-6.02.tar.gz
  File-Listing-6.04.tar.gz
  LWP-MediaTypes-6.02.tar.gz
  HTTP-Message-6.03.tar.gz
  HTTP-Cookies-6.01.tar.gz
  HTTP-Daemon-6.01.tar.gz
  HTTP-Negotiate-6.01.tar.gz
  Net-HTTP-6.03.tar.gz
  WWW-RobotRules-6.02.tar.gz
  libwww-perl-6.04.tar.gz

Other:

  IO-String-1.08.tar.gz
  Digest-SHA1-2.13.tar.gz (for 3rd-party scripts)

They were all configured, compiled and installed with

  perl Makefile.PL
  dmake
  dmake install

Compared to 2011, two modules have been dropped because of
additional dependencies:

  Carp-Always-0.11 (for backtracing)
  File-Which-1.09 (for a 3rd-party script)

MODIFICATIONS FOR TEXLIVE

- lib/Config.pm and lib/Config_heavy.pl now compute the location of
  tlperl dynamically; see above.
- The html directory has been removed.
- The site/lib subdirectory is folded into lib.
- The bin subdirectory now only contains exe- and dll files;
  Batch files and Perl scripts have been removed.
- Copied from mingw/bin to tlperl/bin:
  libgcc_s_dw2_1.dll and libstdc++-6.dll.

TESTING WITH TEXLIVE

For testing the new Perl with the installer, I unpacked the net
installer and replaced its tlperl with the new one.

For testing the new Perl with the installed TeX Live, I copied it
into the fresh installation.

tlmgr appeared to work as intended.  I also successfully ran
the uninstaller.

Siep Kroonenberg

Last revision: December 7 2012
