#ifndef _TKCONFIG
#define _TKCONFIG
#define ANSI_SIGNED_CHAR 1
#define HAS_STDARG 1
#define HAVE_LIMITS_H 1
#define LSEEK_TYPE long long
#define SELECT_MASK Perl_fd_set
#define USE_NEWSTYLE_REGEXP_STRUCT 1
#define USE_PREGCOMP_31027 1
#define USE_PROTOTYPE 1
#define USE_REGEXP_511 1
#endif
