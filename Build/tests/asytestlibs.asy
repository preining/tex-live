// $Id$
// Check Asymptote shared libraries.
// From Philipp Stephani, posted to tlbuild 28 Jun 2009 18:34:19.
// public domain.
size(4cm, 0);

// Test GSL
access gsl;
write(gsl.sinc(0));
write();

// Test FFTW
int n = 4;
pair[] f = sequence(n);
write(f);
pair[] g = fft(f, -1);
write();
write(g);
f=fft(g, 1);
write();
write(f/n);
write();

// Test OpenGL
access settings;
access three;
settings.render = -1;
settings.outformat = "png";
three.draw(three.unitsphere);

