# $Id$
# Written 2004, Karl Berry.  Public domain.

# maybe we'll need to set paths or something?
#run =

# we run in subdirectories.
dvicheck = ../checkdvi.pl
pdfcheck = ../checkpdf.pl

# running context is complicated.
context = texexec --batch --pdf --result=$(test).pdf $(paper)
ctxpaper = --paper=letter\*letter --mode=letter
ctxclean = $(test).tmp $(test).tui $(test).tuo mpgraph.* texexec.*
pdfclean = $(test).pdf checkpdf-*.ppm

rm = rm -f

%.dvitype: %.dvi
	$(run) dvitype $< >$@ || { rm -f $@; exit 1; }

# compress the ppm just for the sake of saving depot space/transfer time.
# used pdftoppm 3.00.
# only useful for one-page tests.
%.ppz: %.pdf
	pdftoppm -cfg /dev/null $< $*
	gzip -1 <$*-000001.ppm >$@
	$(rm) $*-000001.ppm

.PRECIOUS: %.dvitype
